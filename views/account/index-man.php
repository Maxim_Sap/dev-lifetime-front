<?php
use app\controllers\BaseController;

$session = Yii::$app->session;

$userType = (isset($session['user_data']['userType']) &&  $session['user_data']['userType'] == BaseController::USER_FEMALE) ? BaseController::USER_FEMALE : BaseController::USER_MALE;

if (!empty($userData->personalInfo->birthday)) {
    $birthday = new \DateTime($userData->personalInfo->birthday);
    $tz  = new \DateTimeZone('Europe/Brussels');
    $age = \DateTime::createFromFormat('Y-m-d', $userData->personalInfo->birthday, $tz)
         ->diff(new \DateTime('now', $tz))
         ->y;
    // zodiak
    $signs = [ 
                "Capricorn", 
                "Aquarius", 
                "Pisces", 
                "Aries", 
                "Taurus", 
                "Gemini", 
                "Cancer", 
                "Leo", 
                "Virgo", 
                "Libra", 
                "Scorpio", 
                "Sagittarius"
            ];
    $signsstart = array(1=>21, 2=>20, 3=>20, 4=>20, 5=>20, 6=>20, 7=>21, 8=>22, 9=>23, 10=>23, 11=>23, 12=>23);
    $zodiak = (int)$birthday->format('d') < $signsstart[(int)$birthday->format('m')] ? $signs[(int)$birthday->format('m') - 1] : $signs[(int)$birthday->format('m') % 12];
} else {
    $age = "Not defined";
    $zodiak = "Not defined";
}
if (!empty($userData->personalInfo->weight)) {
    $weight = $userData->personalInfo->weight." kg (" . round($userData->personalInfo->weight/0.45359237)." lbs)";
} else {
    $weight = "Not defined";
}

if (!empty($userData->personalInfo->height)) {
    $heightInMeters = sprintf("%.2f", $userData->personalInfo->height/100);
    $heightInFeet = $userData->personalInfo->height*0.03280839895013123;
    $heightFeet = (int)$heightInFeet; // truncate the float to an integer
    $heightInches = round(($heightInFeet-$heightFeet)*12); 
    
    if ($heightInches == 0) {
        $height = "$heightInMeters m ($heightFeet')";
    } else {
        $height = "$heightInMeters m ($heightFeet' $heightInches'')";
    }
} else {
    $height = "Not defined";
}

if (!empty($userData->location)) {
    $user_city = $userData->city;
    foreach($countryList as $country) {
        if (isset($userData->location->country_id) && $userData->location->country_id == $country->id) {
            $user_country = $country->name;
        }        
    }    
} else {
    $user_city = "Not defined";
    $user_country = "Not defined";
}

$maritalArray = ['Single','Married','Divorced','Widowed','Complicated'];
if ($userData->personalInfo->marital != null && isset($maritalArray[$userData->personalInfo->marital-1])) {
    $marital = $maritalArray[$userData->personalInfo->marital-1];
} else {
    $marital = 'Not defined';
}

if ($userData->personalInfo->eyes != null) {
    foreach ($eyesColors as $eyesColor) {
        if ($userData->personalInfo->eyes == $eyesColor['id'])
            $eyes = $eyesColor['name'];
    }
} else {
    $eyes = 'Not defined';
}

if ($userData->personalInfo->hair != null) {
    foreach ($hairColors as $hairColor) {
        if ($userData->personalInfo->hair == $hairColor['id'])
            $hair = $hairColor['name'];
    }    
} else {
    $hair = 'Not defined';
}

if (isset($userData->personalInfo->kids)) {    
    $children = $userData->personalInfo->kids;
    if ($children == 0) {
        $children = "no";
    }
} else {
    $children = 'Not defined';
}

$smokingArray = ['Yes','No','Casual','Heavy'];
if ($userData->personalInfo->smoking != null && isset($smokingArray[$userData->personalInfo->smoking-1])) {
    $smoking = $smokingArray[$userData->personalInfo->smoking-1];
} else {
    $smoking = 'Not defined';
}

$englishLevelArray = ['Beginner','Intermediate','Advanced','Fluent'];
if ($userData->personalInfo->english_level != null && isset($englishLevelArray[$userData->personalInfo->english_level-1])) {
    $englishLevel = $englishLevelArray[$userData->personalInfo->english_level-1];
} else {
    $englishLevel = 'Not defined';
}

if ($userData->userType == BaseController::USER_MALE) {
	$baseUrl = 'men';
	$text = 'Men';
	$baseUrl2 = 'girls';
	$text2 = 'Ladies';
} else {
	$baseUrl = 'girls';
	$text = 'Ladies';
	$baseUrl2 = 'men';
	$text2 = 'Men';
}


?>

<div class="global-content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-sm-push-3">
                <div class="profile-container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="profile-title">Welcome, <?= $userData->personalInfo->first_name ?>!</h3>
                        </div>
                    </div>
                    <div class="profile-info clearfix">                    
                        <div class="avatar-wrapper">
                            <div class="avatar edit_profile_avatar" style="background-image: url('<?= $userData->avatar->normal ?>')"></div>
                            <div class="change_avatar_block fl-l">                        
                                <span>                                    
                                    <a id="change_avatar" class="change-avatar" href="javascript:void(0);">Change Avatar</a>
                                    <form id="avatarform" method="post" enctype="multipart/form-data">
                                        <input name="MAX_FILE_SIZE" value="12345" type="hidden">
                                        <input name="file" multiple="" accept="image/*" id="avatar_file" data-album-title="letters" style="display: none;" type="file">
                                    </form>
                                </span>
                            </div>
                        </div>

                        <div class="description-wrapper">

                            <div class="description-item clearfix">
                                <div class="left-side">
                                    <ul class="info-list">
                                        <li>
                                            <div class="part">
                                                <span class="title">Messages:</span>
                                            </div>
                                            <div class="part">
                                                <span class="quantity"><?= $messagesCount ?></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="part">
                                                <span class="title">Favorites:</span>
                                            </div>
                                            <div class="part">
                                                <span class="quantity"><?= $favoritesCount ?></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="part">
                                                <span class="title">Admirers:</span>
                                            </div>
                                            <div class="part">
                                                <span class="quantity"><?= $admirersCount ?></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="part">
                                                <span class="title">Matches:</span>
                                            </div>
                                            <div class="part">
                                                <span class="quantity"><?= $matchesCount ?></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="right-side">
                                    <ul class="info-list">
                                        <li>
                                            <div class="part">
                                                <span class="title">Notifications:</span>
                                            </div>
                                            <div class="part">
                                                <span class="quantity"><?= $notificationsCount ?></span>
                                            </div>  
                                        </li> 
                                        <li>
                                            <div class="part">
                                                <span class="title">Photo Likes:</span>
                                            </div>
                                            <div class="part">
                                                <span class="quantity"><?= $photoLikes ?></span>
                                            </div>
                                        </li>
                                    </ul>
                                    <a href="/account/gallery" class="add-photos">Add photos to your profile</a>
                                </div>
                            </div>

                            <div class="description-item clearfix">
                                <div class="left-side">
                                    <div class="info-line">
                                        <span>Membership Level: <span class="high"><?= $userData->status ?></span></span>
                                        
                                    </div>
                                    <div class="info-line">

                                    </div>
                                </div>
                                <div class="right-side">
                                    <div class="info-line">
                                        <span>Credits Balance:</span>
                                        <span class="high"><?php if ($session['user_data']['balance'] != null) {echo $session['user_data']['balance']; } else { echo '0.00'; } ?> <a href="/payments" class="info-link buy-credits">Buy credits</a></span>
                                    </div>
                                    <div class="info-line">
                                        <span>Connect:</span>
                                        <ul class="socials list-inline">
                                            <li><a href="javascript:void(0);"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="javascript:void(0);"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="javascript:void(0);"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                            <li><a href="javascript:void(0);"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="description-item clearfix">
                                <div class="left-side">
                                    <span>Member Since: <span class="high"><?= date('F d, Y', strtotime($userData->created_at)); ?></span></span>
                                    <a href="/account/edit/<?= $userData->userID ?>" class="info-link">My Account Options</a>
                                </div>
                                <div class="right-side">

                                </div>
                            </div>
    

                        </div>

                    </div>
                    
                    <div class="persons-carousel-wrapper">
                        <h2 class="title">girls available to chat</h2>
                        <?php if (!empty($availableGirls)) { ?>
                        <div class="basic-slider persons-carousel">
                            <?php foreach ($availableGirls as $girl) { ?>
                                <div class="slide">
                                    <div class="slide-content">
                                        <div class="person-box clearfix">
                                            <div class="person-image" style="background-image: url('<?= (!empty($girl->medium_thumb)) ? $this->context->serverUrl . '/' . $girl->medium_thumb : $this->context->serverUrl . '/img/no_avatar_normal_girl.jpg'; ?>')">
                                                <span class="status online">Online</span>
                                            </div>
                                            <div class="person-description">
                                                <a href="/<?= $baseUrl2 . '/' . $girl->id  ?>" class="person-name"><?= $girl->first_name ?></a>
                                                <span class="person-id">ID: <?= $girl->id ?></span>
                                                <?php if (!empty($girl->birthday)) { 
                                                    $girlBirthday = new \DateTime($girl->birthday);
                                                    $timezone  = new \DateTimeZone('Europe/Brussels');
                                                    $girlAge = \DateTime::createFromFormat('Y-m-d', $girl->birthday, $timezone)
                                                         ->diff(new \DateTime('now', $timezone))
                                                         ->y;
                                                ?>
                                                <span class="person-age"><?= $girlAge ?> years</span>
                                                <?php } else {?>
                                                <span class="person-age" style="min-height: 20px;"> </span>
                                                <?php } ?>
                                                <a href="/account/chat/<?= $girl->id ?>" class="action">Chat Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <?php } else {?>
                            <p>There are no girls available to chat right now.</p>
                        <?php } ?>
                    </div>
                    <?php if (!empty($waitingAdmirers) || !empty($girlsLikedPhoto)) {?>
                    <div class="persons-carousel-wrapper">
                        <h2 class="title">waiting for your reaction</h2>
                        <div class="basic-slider persons-carousel">
                            <?php foreach ($girlsLikedPhoto as $girl) { ?>
                            <div class="slide">
                                <div class="slide-content">
                                    <div class="person-box clearfix">
                                        <div class="person-image" style="background-image: url('<?= (!empty($girl->medium_thumb)) ? $this->context->serverUrl . '/' . $girl->medium_thumb : $this->context->serverUrl . '/img/no_avatar_normal_girl.jpg' ?>')">
                                            <span class="status <?= ($girl->online == 'Online') ? 'online' : 'offline' ?>"><?= $girl->online ?></span>
                                        </div>
                                        <div class="person-description">
                                            <a href="/<?= $baseUrl2 ?>/<?= $girl->id ?>" class="person-name"><?= $girl->first_name ?></a>
                                            <p>Liked your photo</p>
                                            <a href="/<?= $baseUrl2 ?>/<?= $girl->id ?>" class="action">View Profile</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php foreach ($waitingAdmirers as $girl) { ?>
                            <div class="slide">
                                <div class="slide-content">
                                    <div class="person-box clearfix">
                                        <div class="person-image" style="background-image: url('<?= (!empty($girl->medium_thumb)) ? $this->context->serverUrl . '/' . $girl->medium_thumb : $this->context->serverUrl . '/img/no_avatar_normal_girl.jpg' ?>')">
                                            <span class="status <?= ($girl->online == 1) ? 'online' : 'offline' ?>"><?= ($girl->online == 1) ? 'Online' : 'Offline' ?></span>
                                        </div>
                                        <div class="person-description">
                                            <a href="/<?= $baseUrl2 ?>/<?= $girl->id ?>" class="person-name"><?= $girl->first_name ?></a>
                                            <p>Added you to her favorites</p>
                                            <a href="/<?= $baseUrl2 ?>/<?= $girl->id ?>" class="action">View Profile</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3 col-sm-pull-9 hidden-xs">
                <aside class="side-bar side-bar-left">
                    <?php echo $this->render('//layouts/parts/message-vertical-menu-man.php'); ?>                    
                </aside>
            </div>
        </div>
    </div>
</div>
<?php
	$session = Yii::$app->session;
	if (isset($session['message']) && is_string($session['message'])) {
		echo " " . $session['message'];
		unset($session['message']);
	}	
?>


