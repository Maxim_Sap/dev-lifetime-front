<?php
use app\controllers\BaseController;
$session = Yii::$app->session;
$userType = isset($session['user_data']['userType']) && $session['user_data']['userType'] == BaseController::USER_FEMALE ? BaseController::USER_FEMALE : BaseController::USER_MALE;    
$prefix = (isset($userType) && $userType == BaseController::USER_FEMALE) ? 'men' : 'girls';
$tz  = new \DateTimeZone('Europe/Brussels');
$maritalStatus = 'Not set';
$age = null;
if($user['birthday']){
  $age = \DateTime::createFromFormat('Y-m-d', $user['birthday'], $tz)
  ->diff(new \DateTime('now', $tz))
  ->y;
}

if (isset($user['inFavourite']) && $user['inFavourite'] == 1) {
  $showStatus = 'liked';
} else {
  $showStatus = '';    
}

$maritalArray = ['Single','Married','Divorced','Widowed','Complicated'];
$i = 0;    
foreach ($maritalArray as $marital) {
  $i++;        
  if ($user['marital'] == $i) {
    $maritalStatus = $maritalArray[$i-1];
  }
}

if (!empty($user['weight'])) {
    $weight = round($user['weight']/0.45359237)." lbs";
} else {
    $weight = null;
}

if (!empty($user['height'])) {
    $heightInMeters = sprintf("%.2f", $user['height']/100);
    $heightInFeet = $user['height']*0.03280839895013123;
    $heightFeet = (int)$heightInFeet; // truncate the float to an integer
    $heightInches = round(($heightInFeet-$heightFeet)*12); 
    
    if ($heightInches == 0) {
        $height = "$heightFeet'";
    } else {
        $height = "$heightFeet' $heightInches''";
    }
} else {
    $height = null;
}

$usersOnPage = 4;
$this->title = "Chat";    

?>

  <div class="main sub-main chat">

    <div class="global-content-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="chat-content-wrapper">
              <div class="row">

                <div class="col-xs-12 col-sm-8 col-md-9">

                  <div class="chat-box">

                    <div ss-container class="chat-box-messages-wrapper">
                      
                    </div>
                    <div class="translate-line clearfix">
                        <div class="translate-buttons">Translate to:
                          <span class="item" data-lang="ru-en">EN</span> or 
                          <span class="item" data-lang="en-ru">RU</span>
                          <span class="preloader"><img src="/img/loading.gif" /></span>
                        </div>
                        <?php if ($prefix == 'men') {?>
                        <a href="#mailing-list" class="btn mailing-list" data-toggle="modal" data-target="#mailing-list">My mailing list</a>
                        <?php } ?>
                      </div>
                    <div class="print-message-container">
                        <form class="print-message-form" method="post" enctype="multipart/form-data">
                          <input type="hidden" id="otherUserID" value="<?= $otherUserID ?>">
                          <div class="chat-message-wrapper">
                            <textarea name="chat-message" id="chat-message" placeholder="Type your message here"></textarea>
                          </div>
                          <ul class="print-message-form-action-list list-inline">
                            <?php if (false && $userType == BaseController::USER_MALE) { ?>
                            <li>
                              <span><a href="javascript:void(0);" class="turn-web">Turn on</a> your webcam.</span><br/>
                              <span>It is free!</span>
                            </li>
                            <?php } else {?>
                            <li>
                              
                            </li>
                            <?php } ?>
                            <li>
                              <div class="file-type-wrapper">
                                <button class="btn btn-reset btn-file">Attach image</button>
                                <input type="file" name="image" id="attach-image" style="width: 100%">
                              </div>
                            </li>
                            <li>
                              <button <?php if ($fakeUser) {echo "disabled";} ?> type="submit" class="btn <?php if ($fakeUser) {echo "btn-disabled";} ?>">Send Message</button>
                            </li>
                          </ul>
                        </form>
                    </div>
                  </div>

                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                  <div class="sidebar-right-wrapper">

                    <button class="right-toggle-button">
                      <span class="show-text">Show contact list</span>
                      <span class="hide-text">Hide contact list</span>
                    </button>
                    
                    <div class="content-container">
                      <div class="video-chat-wrapper">
                        <div class="video-window <?= ($prefix == 'girls' && !$user['cam_online']) ? 'no-video' : '' ?>">
                          <video class="video-container" src=""></video>
                          <div class="preview" style="background-image: url('/img/video-stub.jpg')"></div>
                          <a id="start-video" href="javascript:void(0);" class="play-video-chat"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>
                        <ul class="video-options-list list-inline">
                          <li>
                            
                          </li>
                          <li>
                            
                          </li>
                        </ul>
                      </div>

                      <ul class="interlocutor-cta-list chat-list list-inline">
                        <li>
                          <div class="start">
                            <div class="interlocutor-cta-list-popup-info">
                              <span>Call</span>
                            </div>
                            <a href="javascript:void(0);" class="play-video-chat" onclick="$('#start-video').click()">
                              <img src="/img/phone-icon.png" alt="">
                            </a>
                          </div>
                          <div class="stop-video hidden">
                            <div class="interlocutor-cta-list-popup-info">
                              <span>Stop Call</span>
                            </div>
                            <a id="stop-video" href="javascript:void(0);">
                              <img src="/img/phone-hangdown-icon.png" alt="">
                            </a>
                          </div>                          
                        </li>
                          <li>
                            <div class="interlocutor-cta-list-popup-info">
                              <span>Send Message</span>
                            </div>
                            <a class="new-message" href="<?= (!empty($user['id'])) ? '/newmessage/'. $user['id'] : 'javascript:void(0);'; ?>">
                              <svg width="512pt" height="512pt" viewBox="0 0 512 512" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <g id="#000000ff">
                                  <path fill="#eb3758" opacity="1.00" d=" M 109.36 0.00 L 404.85 0.00 C 413.73 1.22 422.40 4.72 429.07 10.81 C 437.97 18.59 443.58 30.12 443.70 41.98 C 443.72 102.02 443.69 162.06 443.72 222.09 C 450.68 216.91 457.48 211.50 464.49 206.37 C 468.62 203.19 475.21 205.14 477.15 209.92 C 478.82 213.52 477.48 218.03 474.25 220.29 C 415.64 264.96 356.97 309.58 298.34 354.24 C 289.24 361.01 280.58 368.83 269.72 372.69 C 261.21 376.07 251.51 376.40 242.94 373.06 C 232.24 369.44 223.80 361.71 214.90 355.10 C 156.02 310.31 97.15 265.50 38.26 220.72 C 34.74 218.46 33.02 213.68 34.90 209.82 C 36.92 205.10 43.49 203.22 47.58 206.42 C 54.56 211.54 61.34 216.91 68.28 222.08 C 68.33 161.73 68.24 101.37 68.33 41.02 C 68.88 19.51 87.88 0.61 109.36 0.00 M 105.08 17.84 C 93.95 20.60 85.19 31.35 85.35 42.92 C 85.34 105.94 85.39 168.96 85.32 231.98 C 85.50 233.20 84.87 234.96 86.20 235.74 C 136.56 274.06 186.92 312.38 237.27 350.70 C 243.04 354.73 249.66 358.66 256.94 358.34 C 265.11 357.38 272.29 352.79 278.63 347.81 C 327.96 310.28 377.22 272.67 426.59 235.20 C 426.73 171.13 426.60 107.05 426.65 42.97 C 426.91 29.33 414.65 16.97 401.02 17.08 C 306.03 17.05 211.03 17.08 116.04 17.06 C 112.38 17.08 108.64 16.84 105.08 17.84 Z" />
                                  <path fill="#eb3758" opacity="1.00" d=" M 160.42 119.65 C 193.91 119.21 227.44 119.61 260.95 119.45 C 263.94 119.43 267.27 119.19 269.73 121.24 C 273.68 124.07 274.17 130.36 270.78 133.82 C 268.84 136.00 265.83 136.71 263.01 136.54 C 229.30 136.50 195.60 136.58 161.89 136.50 C 156.30 136.58 151.97 130.24 154.13 125.06 C 155.13 122.35 157.58 120.21 160.42 119.65 Z" />
                                  <path fill="#eb3758" opacity="1.00" d=" M 30.35 137.33 C 34.92 134.79 39.16 129.58 44.95 131.14 C 50.88 132.50 53.29 140.74 48.98 145.06 C 43.78 149.60 37.23 152.39 32.27 157.26 C 24.43 164.86 19.89 175.35 18.00 185.99 C 16.80 192.25 17.08 198.64 17.06 204.98 C 17.06 292.99 17.05 381.00 17.06 469.01 C 16.96 476.78 20.45 484.59 26.62 489.40 C 31.47 493.33 37.80 495.12 44.00 494.94 C 185.68 494.91 327.36 494.95 469.04 494.92 C 478.81 495.16 488.49 489.34 492.50 480.36 C 495.81 473.72 494.80 466.14 494.94 458.99 C 494.94 374.01 494.94 289.02 494.94 204.03 C 494.93 198.02 495.13 191.96 494.15 186.00 C 492.61 175.68 488.25 165.57 480.77 158.16 C 475.87 153.19 469.49 150.15 463.99 145.95 C 460.24 143.21 459.77 137.28 462.82 133.85 C 465.60 130.46 470.97 129.88 474.46 132.53 C 481.34 137.24 488.76 141.37 494.41 147.62 C 505.93 159.89 511.41 176.78 512.00 193.38 L 512.00 472.42 C 511.04 483.96 505.61 495.21 496.49 502.46 C 489.74 508.08 481.18 511.16 472.50 512.00 L 39.58 512.00 C 29.70 511.13 20.04 507.14 12.91 500.17 C 5.14 492.75 0.61 482.29 0.00 471.62 L 0.00 193.36 C 0.89 171.49 11.04 148.83 30.35 137.33 Z" />
                                  <path fill="#eb3758" opacity="1.00" d=" M 159.15 188.28 C 160.70 187.67 162.41 187.76 164.04 187.71 C 180.34 187.77 196.65 187.71 212.95 187.74 C 216.43 187.57 219.83 189.73 221.19 192.93 C 223.78 198.36 219.02 205.21 213.06 204.79 C 196.02 204.80 178.98 204.83 161.94 204.77 C 157.70 204.80 153.86 201.14 153.64 196.92 C 153.26 193.22 155.70 189.55 159.15 188.28 Z" />
                                  <path fill="#eb3758" opacity="1.00" d=" M 244.28 188.36 C 245.75 187.71 247.39 187.76 248.97 187.72 C 281.98 187.74 314.98 187.75 347.98 187.71 C 352.01 187.23 356.38 189.24 357.82 193.20 C 360.08 198.38 355.76 204.87 350.12 204.75 C 316.08 204.85 282.04 204.79 248.00 204.79 C 243.93 205.07 239.90 202.12 239.14 198.08 C 238.18 194.14 240.50 189.80 244.28 188.36 Z" />
                                  <path fill="#eb3758" opacity="1.00" d=" M 159.34 239.41 C 160.52 239.05 161.74 238.89 162.99 238.93 C 202.32 238.94 241.66 238.92 280.99 238.94 C 284.58 238.70 288.20 240.88 289.51 244.25 C 291.13 248.05 289.50 252.90 285.84 254.86 C 283.51 256.32 280.64 255.95 278.03 256.02 C 239.69 255.98 201.35 256.01 163.01 256.01 C 158.70 256.41 154.38 253.24 153.74 248.92 C 152.93 244.92 155.51 240.70 159.34 239.41 Z" />
                                  <path fill="#eb3758" opacity="1.00" d=" M 312.47 239.59 C 314.21 238.82 316.17 238.96 318.04 238.91 C 328.71 238.99 339.39 238.86 350.07 238.97 C 354.70 238.91 358.77 243.31 358.38 247.93 C 358.25 252.21 354.39 256.01 350.08 255.95 C 338.71 256.05 327.33 255.99 315.95 255.99 C 311.87 256.17 307.98 253.02 307.36 248.98 C 306.56 245.14 308.86 241.05 312.47 239.59 Z" />
                                  <path fill="#eb3758" opacity="1.00" d=" M 212.20 290.23 C 240.77 290.00 269.37 290.20 297.95 290.13 C 302.14 289.78 306.25 292.81 307.02 296.96 C 308.18 301.90 304.07 307.21 298.96 307.16 C 270.63 307.25 242.30 307.17 213.97 307.20 C 209.73 307.52 205.51 304.35 204.94 300.09 C 203.98 295.51 207.57 290.70 212.20 290.23 Z" />
                                  <path fill="#eb3758" opacity="1.00" d=" M 159.20 350.28 C 163.27 348.92 168.15 351.14 169.67 355.19 C 171.26 358.83 169.79 363.27 166.56 365.50 C 127.03 396.63 87.54 427.82 47.98 458.92 C 44.55 461.79 38.95 461.27 36.16 457.76 C 33.01 454.25 33.66 448.25 37.50 445.49 C 76.06 415.04 114.69 384.67 153.26 354.23 C 155.14 352.79 156.92 351.08 159.20 350.28 Z" />
                                  <path fill="#eb3758" opacity="1.00" d=" M 346.42 350.61 C 349.52 349.18 353.25 349.95 355.77 352.18 C 395.35 383.31 434.97 414.39 474.54 445.53 C 479.06 448.75 478.85 456.22 474.24 459.25 C 471.03 461.60 466.38 461.15 463.47 458.52 C 423.91 427.40 384.30 396.34 344.74 365.21 C 339.60 361.66 340.56 352.87 346.42 350.61 Z" />
                                </g>
                              </svg>

                            </a>
                          </li>
                        <li>
                          <div class="interlocutor-cta-list-popup-info">
                            <span>Watch Video</span>
                          </div>
                          <a class="video-gallery" href="/<?= $prefix . '/' . $user['id']; ?>/video-gallery">
                            <img src="/img/movie-icon.png" alt="">
                          </a>
                        </li>
                        <?php if ($userType == BaseController::USER_MALE) { ?>
                            <li>
                              <div class="interlocutor-cta-list-popup-info">
                                <span>Send Present</span>
                              </div>
                              <a target="_blank" href="/account/shop/<?= $user['id']; ?>">
                                <svg width="609pt" height="612pt" viewBox="0 0 609 612" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                  <g id="#000000ff">
                                    <path fill="#eb4161" opacity="1.00" d=" M 154.69 0.00 L 162.40 0.00 C 177.89 1.23 192.65 6.76 206.45 13.62 C 224.99 22.94 242.24 34.64 258.49 47.50 C 275.56 61.42 292.29 76.18 305.49 93.94 C 313.82 82.62 323.79 72.61 334.15 63.17 C 349.61 49.06 366.29 36.26 384.04 25.16 C 403.75 13.09 425.12 2.06 448.51 0.00 L 455.41 0.00 C 466.88 0.76 478.19 5.39 486.19 13.78 C 494.82 22.57 499.86 34.24 502.86 46.04 C 507.72 65.59 507.76 85.93 507.10 105.95 C 506.86 111.55 507.19 117.24 505.86 122.74 C 531.24 122.73 556.63 122.72 582.01 122.75 C 594.29 122.53 605.85 131.42 609.00 143.25 L 609.00 275.51 C 605.94 286.56 595.61 295.23 584.09 295.83 C 575.93 296.08 567.76 295.85 559.59 295.94 C 559.57 394.62 559.62 493.31 559.57 591.99 C 559.77 601.86 551.79 610.56 542.16 612.00 L 69.57 612.00 C 62.71 611.21 56.31 606.93 53.40 600.61 C 51.07 596.08 51.39 590.86 51.40 585.93 C 51.42 489.27 51.41 392.61 51.41 295.94 C 42.95 295.81 34.48 296.16 26.03 295.77 C 12.60 294.66 1.27 282.51 1.05 269.04 C 0.92 230.02 1.05 190.99 0.99 151.97 C 0.89 146.91 1.95 141.79 4.42 137.35 C 9.14 128.49 18.92 122.65 28.97 122.74 C 54.35 122.73 79.72 122.73 105.10 122.74 C 103.73 116.59 104.16 110.25 103.85 104.01 C 103.23 83.36 103.42 62.31 109.18 42.31 C 112.97 29.19 119.85 16.28 131.27 8.30 C 138.10 3.34 146.39 0.95 154.69 0.00 M 146.81 26.82 C 138.23 31.80 134.25 41.66 131.75 50.79 C 126.48 71.89 127.48 93.86 128.22 115.40 C 170.79 117.43 213.39 118.85 256.00 119.72 C 267.60 119.59 280.23 121.28 290.75 115.30 C 286.39 107.32 280.22 100.54 274.07 93.93 C 259.97 79.23 244.28 66.09 227.66 54.33 C 211.64 43.18 194.68 32.89 175.98 26.94 C 166.63 24.19 155.86 22.01 146.81 26.82 M 412.37 36.47 C 394.67 45.70 378.31 57.30 362.89 69.93 C 351.32 79.61 340.11 89.82 330.27 101.30 C 326.58 105.74 322.85 110.22 320.25 115.41 C 331.85 121.58 345.44 119.47 358.05 119.68 C 399.65 118.72 441.25 117.44 482.81 115.37 C 483.47 93.85 484.56 71.89 479.25 50.81 C 476.99 42.49 473.56 33.83 466.56 28.37 C 460.50 23.72 452.35 23.40 445.09 24.56 C 433.57 26.49 422.71 31.17 412.37 36.47 M 25.02 150.86 C 24.97 189.59 25.02 228.32 25.00 267.05 C 24.45 269.94 27.12 272.47 29.97 271.99 C 104.09 272.00 178.22 272.00 252.35 272.00 C 252.35 230.22 252.35 188.45 252.35 146.67 C 178.26 146.66 104.18 146.67 30.09 146.67 C 27.59 146.29 24.77 148.13 25.02 150.86 M 276.30 147.40 C 276.28 188.93 276.31 230.46 276.29 271.99 C 295.76 272.01 315.24 272.01 334.71 271.99 C 334.69 230.46 334.72 188.93 334.70 147.40 C 315.23 147.40 295.77 147.40 276.30 147.40 M 358.65 146.68 C 358.65 188.46 358.64 230.23 358.65 272.00 C 432.76 272.00 506.87 272.00 580.99 272.00 C 583.84 272.48 586.55 269.98 586.00 267.06 C 585.98 228.35 586.03 189.62 585.98 150.91 C 586.25 148.18 583.47 146.30 580.96 146.67 C 506.86 146.67 432.75 146.65 358.65 146.68 M 75.39 296.01 C 75.43 393.33 75.42 490.65 75.39 587.97 C 134.38 588.04 193.36 587.98 252.35 588.00 C 252.35 490.65 252.36 393.29 252.35 295.94 C 193.36 295.98 134.37 295.83 75.39 296.01 M 276.30 295.94 C 276.29 393.29 276.31 490.64 276.29 587.99 C 295.76 588.01 315.24 588.01 334.71 587.99 C 334.70 490.65 334.71 393.30 334.71 295.96 C 315.24 295.90 295.77 295.94 276.30 295.94 M 358.65 295.94 C 358.64 393.29 358.65 490.64 358.65 588.00 C 417.64 587.98 476.62 588.04 535.61 587.97 C 535.58 490.65 535.56 393.34 535.62 296.03 C 476.64 295.81 417.64 295.99 358.65 295.94 Z" />
                                  </g>
                                </svg>

                              </a>
                            </li>
                            <?php } else { ?>
                            <li>
                              <div class="interlocutor-cta-list-popup-info">
                                <span>Wink</span>
                              </div>
                              <a href="javascript:void(0);" class="wink-link" data-id="<?= $user['id'] ?>">
                                <img style="margin:0" src="/img/wink.svg" alt="web">                                      
                              </a>
                            </li>
                        <?php } ?>
                        <li>
                          <div class="interlocutor-cta-list-popup-info">
                            <span>Like</span>
                          </div>
                          <a href="javascript:void(0);" class="send-like like <?= $showStatus ?>" data-id="<?= $user['id']; ?>">
                            <svg width="702pt" height="653pt" viewBox="0 0 702 653" version="1.1" xmlns="http://www.w3.org/2000/svg">
                              <g id="outer">
                                <path fill="#fcfcfc" opacity="1.00" d=" M 84.16 37.19 C 113.49 14.69 150.00 1.52 187.01 0.93 C 216.59 0.01 246.35 6.77 272.65 20.35 C 304.59 36.68 331.02 62.90 349.07 93.78 C 373.22 53.16 412.69 21.71 458.15 8.40 C 498.41 -3.57 543.00 -1.47 581.48 15.69 C 617.31 31.40 647.14 59.30 667.25 92.67 C 688.17 127.03 698.85 166.98 700.99 206.97 C 701.88 229.59 698.33 252.13 692.89 274.03 C 680.00 325.26 653.40 372.79 617.28 411.27 C 604.73 425.09 590.40 437.10 576.57 449.60 C 500.69 517.30 424.94 585.15 349.01 652.80 L 349.89 653.00 L 348.96 653.00 C 267.04 578.61 185.15 504.18 103.27 429.74 C 56.59 387.08 22.17 330.68 7.85 268.92 C 2.14 244.11 -1.17 218.32 2.18 192.92 C 4.49 170.07 9.70 147.46 18.18 126.10 C 31.87 91.38 54.36 59.85 84.16 37.19 M 158.45 31.59 C 126.38 38.70 97.38 57.23 75.99 81.97 C 52.01 109.50 37.18 144.25 31.16 180.09 C 28.92 194.76 26.92 209.62 28.22 224.49 C 31.02 260.97 42.04 296.64 58.98 328.99 C 75.20 359.89 97.18 387.63 122.97 411.08 C 198.32 479.58 273.69 548.07 349.06 616.54 C 417.82 555.14 486.55 493.71 555.30 432.30 C 567.53 421.20 580.20 410.56 591.82 398.80 C 626.52 363.67 652.57 319.80 665.45 272.05 C 670.93 251.53 674.58 230.34 674.10 209.05 C 672.10 172.91 662.43 136.75 643.29 105.82 C 627.65 80.15 605.38 58.29 578.61 44.42 C 555.75 32.54 529.74 26.87 504.01 28.03 C 476.59 29.10 449.56 37.63 426.36 52.26 C 398.24 69.86 375.83 96.24 362.55 126.60 C 358.05 136.98 353.74 147.43 349.34 157.85 C 349.15 157.82 348.79 157.77 348.61 157.75 C 340.84 139.98 334.43 121.49 324.17 104.93 C 308.19 78.52 284.90 56.32 257.02 42.87 C 226.71 28.07 191.31 24.04 158.45 31.59 Z"/>
                                <path fill="#fcfcfc" opacity="1.00" d=" M 200.54 83.78 C 206.35 83.15 212.21 82.71 218.05 82.97 C 225.22 83.70 230.83 91.00 229.67 98.12 C 228.93 104.72 222.72 110.07 216.08 109.83 C 188.43 109.76 161.04 121.01 141.33 140.37 C 122.20 158.82 110.32 184.56 108.82 211.11 C 108.45 215.86 109.30 221.15 106.29 225.26 C 102.50 231.17 93.86 233.00 87.99 229.14 C 84.20 226.79 81.70 222.45 81.70 217.97 C 81.55 190.82 89.93 163.70 105.39 141.38 C 126.94 109.74 162.46 88.07 200.54 83.78 Z"/>
                              </g>
                              <g id="inner">
                                <path fill="#f13e4d" opacity="1.00" d=" M 158.45 31.59 C 191.31 24.04 226.71 28.07 257.02 42.87 C 284.90 56.32 308.19 78.52 324.17 104.93 C 334.43 121.49 340.84 139.98 348.61 157.75 C 348.79 157.77 349.15 157.82 349.34 157.85 C 353.74 147.43 358.05 136.98 362.55 126.60 C 375.83 96.24 398.24 69.86 426.36 52.26 C 449.56 37.63 476.59 29.10 504.01 28.03 C 529.74 26.87 555.75 32.54 578.61 44.42 C 605.38 58.29 627.65 80.15 643.29 105.82 C 662.43 136.75 672.10 172.91 674.10 209.05 C 674.58 230.34 670.93 251.53 665.45 272.05 C 652.57 319.80 626.52 363.67 591.82 398.80 C 580.20 410.56 567.53 421.20 555.30 432.30 C 486.55 493.71 417.82 555.14 349.06 616.54 C 273.69 548.07 198.32 479.58 122.97 411.08 C 97.18 387.63 75.20 359.89 58.98 328.99 C 42.04 296.64 31.02 260.97 28.22 224.49 C 26.92 209.62 28.92 194.76 31.16 180.09 C 37.18 144.25 52.01 109.50 75.99 81.97 C 97.38 57.23 126.38 38.70 158.45 31.59 M 200.54 83.78 C 162.46 88.07 126.94 109.74 105.39 141.38 C 89.93 163.70 81.55 190.82 81.70 217.97 C 81.70 222.45 84.20 226.79 87.99 229.14 C 93.86 233.00 102.50 231.17 106.29 225.26 C 109.30 221.15 108.45 215.86 108.82 211.11 C 110.32 184.56 122.20 158.82 141.33 140.37 C 161.04 121.01 188.43 109.76 216.08 109.83 C 222.72 110.07 228.93 104.72 229.67 98.12 C 230.83 91.00 225.22 83.70 218.05 82.97 C 212.21 82.71 206.35 83.15 200.54 83.78 Z"/>
                              </g>
                            </svg>
                          </a>
                        </li>
                      </ul>
                      
                      <div class="contact-list-box">
                        <h3 class="contact-list-title">My contact list</h3>
                        <div class="divider"></div>
                        <?php if (!empty($contactList)) {?>
                        <div ss-container class="contact-list-wrapper">
                          <ul class="contact-list" data-page="1">
                          <?php foreach ($contactList as $contact) { ?>
                              <li>
                                <div class="person-contact <?= ($contact->id == $user['id']) ? 'active' : '' ?>" data-id="<?= $contact->id ?>"> 
                                  <a href="javascript:void(0);" class="person-contact-link">
                                    <span class="person-name"><?= $contact->first_name ?>,</span>
                                    <span class="person-id">ID: <span><?= $contact->id ?></span></span><?php 
                                    if (!empty($contact->unreaded_messages)) {?><span class="item-count"><?= $contact->unreaded_messages ?></span><?php } ?>
                                  </a>
                                </div>
                              </li>                            
                          <?php } ?>                            
                          </ul>
                        </div>
                        <?php } else { ?>
                          <div class="empty-contact-list">Your contact list is empty.</div>
                        <?php } ?>
                        <ul class="contact-cta-list list-inline">
                          <li><a href="javascript:void(0);" class="cta-btn remove">Remove contact</a></li>
                          <li><a href="javascript:void(0);" class="cta-btn remove-all">Remove All</a></li>
                        </ul>
                      </div>                      
                      <div class="chat-active-person-box <?php if ($fakeUser) {echo "not-visible";} ?>">
                        <div class="active-person-info-wrapper clearfix">
                          <div class="active-person-image" style="background-image: url('<?= (!empty($user['medium_avatar'])) ? $this->context->serverUrl . '/'.$user['medium_avatar'] :  '/img/no_avatar_normal.jpg' ?>')">
                            <span class="status <?= (!empty($user['online'])) ? 'online' : 'offline' ?>"><?= (!empty($user['online'])) ? 'Online' : 'Offline' ?></span>
                          </div>
                          <div class="active-person-description">
                            <a href="/<?= $prefix .'/'. $user['id'] ?>" class="person-name"><?= $user['first_name'] ?></a>
                            <span class="person-id">ID: <span><?= $user['id'] ?></span></span>
                            <?php if (!empty($age)) {?><span class="person-age">Age: <span><?= $age ?></span></span><?php } ?>
                            <?php if (!empty($height)) {?><span class="person-height">Height: <span><?= $height ?></span></span><?php } ?>
                            <?php if (!empty($weight)) {?><span class="person-weight">Weight: <span><?= $weight ?></span></span><?php } ?>
                          </div>
                        </div>
                        <ul class="active-person-cta-list list-inline">
                          <?php if ($userType == BaseController::USER_MALE) { ?>
                            <li><a target="_blank" href="/account/shop/<?= $user['id']; ?>" class="send">Send Gift</a></li>
                          <?php } else {?>
                            <li></li>
                          <?php } ?>
                          <li><a href="javascript:void(0);" class="end">End Chat</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

    <?php if ($prefix == 'men') {?>
    <div class="modal fade fullscreen-modal" id="mailing-list" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span></span>
          <span></span>
        </button>
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-header-logo rounded-block">
              <img src="/img/logo.png" alt="Logo">
            </div>
          </div>
          <div class="modal-body">
            <div class="mailing-container">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12">
                    <h2 class="mailing-title">My mailing list</h2>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-sm-7">
                    <div class="left-side">
                      <div class="mailing-tabs">
                        <ul class="list-inline tab-list">
                          <li class="tab active">
                           My Templates 
                         </li>
                         <li class="tab">
                          New Template
                        </li>
                      </ul>
                      <div class="tab-panel-container">
                        <div class="tab-panel active">
                          <div class="tab-panel-content">
                            <form class="existing-template-form">
                              <select name="templates" id="mailing-templates">
                                <option value="Template 1">Template 1</option>
                                <option value="Template 2">Template 2</option>
                                <option value="Template 3">Template 3</option>
                                <option value="Template 4">Template 4</option>
                              </select>
                              <textarea name="letter-text" id="" cols="" rows="" placeholder="Letter text"></textarea>
                              <div class="checkbox-group">
                                <input type="checkbox" name="black-exclude" id="black-exclude">
                                <span class="theme-checkbox"></span>
                                <label for="black-exclude">except black letter</label>
                              </div>
                              <button type="submit" class="btn">start a newsletter</button>
                            </form>
                          </div>
                        </div>
                        <div class="tab-panel">
                          <div class="tab-panel-content">
                            <form class="new-mailing-template-form">
                              <input type="text" name="template-name" placeholder="Template name">
                              <textarea name="letter-text" placeholder="Template text"></textarea>
                              <p>Теги, которые можно использовать:</p>
                              <p>%username% - имя получателя</p>
                              <button type="submit" class="btn add-template">Add Template</button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-5">
                  <div class="right-side">
                    <div class="mailing-proggress">
                      <h4 class="mailing-proggress-title">mailing progress</h4>
                      <div class="mailing-proggress-info-item">
                        <span class="title">Begin:</span>
                        <span class="description"><span>12-10-2017</span> <span>11:45</span></span>
                      </div>
                      <div class="mailing-proggress-info-item">
                        <span class="title">Received:</span>
                        <span class="description"><span>45</span> of <span>185</span> people</span>
                      </div>
                      <a href="javascript:void(0);" class="btn play-pause">Pause / Resume</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">

        </div>
        </div>
      </div>
    </div>

    <?php } ?>

    <div id="messages-feed">

    </div>      

    <style>
    .ss-scroll {
          width: 5px;
          -webkit-border-radius: 0;
          border-radius: 0;
          background: #f34148;
      }
      .like {
        position: static;
      }
      .translate-buttons {
        top: 15px;
        <?php if ($prefix == 'girls') {?>
          margin-bottom: 20px;
        <?php } ?>
      }
    </style>