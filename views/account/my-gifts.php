<?php
use app\controllers\BaseController;
use yii\helpers\Url;
$session = Yii::$app->session;
if($session['user_type'] == BaseController::USER_FEMALE){
    $prefix = 'men';
    $prefix2 = 'Men';
    $otherUserID = 'user_from_id';
    $otherUserName = 'user_from_name';
    $otherUserLastName = 'user_from_last_name';
}else{
    $prefix = 'girls';
    $prefix2 = 'Girls';
    $otherUserID = 'user_to_id';
    $otherUserName = 'user_to_name';
    $otherUserLastName = 'user_to_last_name';
}
//var_dump($giftsArray); die;
?>

<div class="main sub-main">
    <div class="global-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <?php echo $this->render('//layouts/parts/profile-vertical-menu-man.php'); ?>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="my-gifts-container">
                        <h2>My Gifts</h2>
                        <?php if (!empty($giftsArray)) {?>
                        <table class="my-gifts-table hidden-xs hidden-sm">
                            <tr>
                                <th>N/n</th>
                                <th><?=$prefix2?> name</th>
                                <th>Gifts title</th>
                                <th>Gifts image</th>
                                <th>Count</th>
                                <?php if($session['user_type'] == BaseController::USER_MALE){?>
                                    <th>Price</th>
                                <?php } ?>
                                <th>Date of purchase</th>
                                <?php if($session['user_type'] == BaseController::USER_MALE){?>
                                    <th>Status</th>
                                <?php } ?>                                
                            </tr>
                            <?php foreach ($giftsArray as $gift) { ?>
                            <tr class="gift-line">
                                <td><span><?=$gift->action_id;?></span></td>
                                <td><a href="/<?=$prefix.'/'.$gift->$otherUserID;?>" class="who"><?=$gift->$otherUserName . ' '. $gift->$otherUserLastName; ?></a></td>
                                <td><span><?=$gift->gift_name;?></span></td>
                                <td><img src="<?=$this->context->serverUrl.'/uploads/gifts/thumb/'.$gift->gift_img;?>" alt="<?=$gift->gift_name;?>"></td>
                                <td><span><?=$gift->quantity;?></span></td>
                                <?php if($session['user_type'] == BaseController::USER_MALE){?>
                                <td><?=$gift->gift_price;?></td>
                                <?php } ?>
                                <?php 
                                
                                $timestamp = strtotime($gift->created_at);

                                if ($timestamp == false) {
                                    $date = 'Not defined';
                                    $time = 'Not defined';
                                } else {
                                    $date = date('Y-m-d', $timestamp);
                                    $time = date('H:i:s', $timestamp);
                                }

                                ?>
                                <td><span class="date"><?= $date; ?></span><span class="time"><?= $time; ?></span></td>
                                <?php if($session['user_type'] == BaseController::USER_MALE){?>
                                    <td>
                                        <?php if($gift->order_status_id == 5){?>
                                            delivered<br/>
                                            <a class="gift-btn" href="<?= Url::toRoute(['account/my-gifts-detail', 'giftID' => $gift->action_id]); ?>">view comment</a>
                                        <?php }elseif($gift->order_status_id == 3){?>
                                            cancelled<br/>
                                            <a class="gift-btn" href="<?= Url::toRoute(['account/my-gifts-detail', 'giftID' => $gift->action_id]); ?>">view comment</a>
                                        <?php }else{
                                            echo $gift->status_title;
                                        }?>
                                    </td>
                                <?php } ?>
                            </tr>
                            <?php } ?>
                        </table>
                        <?php foreach ($giftsArray as $gift) { ?>
                        <table class="my-gifts-table my-gifts-table-mobile visible-xs visible-sm">
                            <tr>
                                <td>N/n</td>
                                <td><span><?=$gift->action_id;?></span></td>
                            </tr>
                            <tr>
                                <td><?=$prefix2?> Name</td>
                                <td><a href="/<?=$prefix.'/'.$gift->$otherUserID;?>" class="who"><?=$gift->$otherUserName . ' '. $gift->$otherUserLastName;?></a></td>
                            </tr>
                            <tr>
                                <td>Gifts title</td>
                                <td><span><?=$gift->gift_name;?></span></td>
                            </tr>
                            <tr>
                                <td>Gifts image</td>
                                <td><img src="<?=$this->context->serverUrl.'/uploads/gifts/thumb/'.$gift->gift_img;?>" alt="<?=$gift->gift_name;?>"></td>
                            </tr>
                            <tr>
                                <td>Count</td>
                                <td><span><?=$gift->quantity;?></span></td>
                            </tr>
                            <?php if($session['user_type'] == BaseController::USER_MALE){?>
                                <tr>
                                    <td>Price</td>
                                    <td><?=$gift->gift_price;?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php 
                                
                                $timestamp = strtotime($gift->created_at);

                                if ($timestamp == false) {
                                    $date = 'Not defined';
                                    $time = 'Not defined';
                                } else {
                                    $date = date('Y-m-d', $timestamp);
                                    $time = date('H:i:s', $timestamp);
                                }

                                ?>
                                <td>Date of purchase</td>
                                <td><span class="date"><?= $date; ?></span><span class="time"><?= $time; ?></span></td>
                            </tr>
                            <?php if($session['user_type'] == BaseController::USER_MALE){?>
                                <tr>
                                    <td>Status</td>                                    
                                    <td class="status">
                                        <?php if($gift->order_status_id == 5){?>
                                            delivered<br/>
                                            <a class="gift-btn" href="<?= Url::toRoute(['account/my-gifts-detail', 'giftID' => $gift->action_id]); ?>">view comment</a>
                                        <?php }elseif($gift->order_status_id == 3){?>
                                            cancelled<br/>
                                            <a class="gift-btn" href="<?= Url::toRoute(['account/my-gifts-detail', 'giftID' => $gift->action_id]); ?>">view comment</a>
                                        <?php }else{
                                            echo $gift->status_title;
                                        }?>
                                    </td>
                                </tr>
                            <?php } ?>  
                        </table>
                        <?php } ?>
                    <?php }else{?>
                        <p>No result found</p>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

