<?php
    $apiServer = $this->context->serverUrl;
?>

<div class="main sub-main">
    
    <div class="global-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <?php echo $this->render('//layouts/parts/profile-vertical-menu-man.php'); ?>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="my-gallery-container">
                        <h2>My video</h2>

                        <div class="added-videos-container">
                            <?php if (!empty($videoList)) {
                                foreach ($videoList as $video) {?>
                                    <div class="added-video">
                                        <?php if($video->status == 4){?>
                                            <div class="sticker approved-status yes">
                                                <span>video approved</span>
                                            </div>
                                        <?php }else{ ?>
                                            <div class="sticker approved-status no">
                                                <span>video not approved</span>
                                            </div>
                                        <?php }?>
                                        <div class="video-container">
                                            <div class="video-inner">
                                                <video id="player1" width="" height="" style="max-width:100%;" poster="" preload="none" controls playsinline webkit-playsinline src="<?= $apiServer . '/' . $video->path ?>">
                                            </video>
                                            </div>
                                        </div>
                                        <div class="description-container video-data">
                                            <form action="" class="submit-video-form">
                                                <textarea name="" id="" cols="30" rows="10" placeholder="description"><?=$video->description?></textarea>
                                                <button type="submit" class="btn save-video" data-video-id="<?=$video->id?>">Save</button>
                                            </form>
                                        </div>
                                    </div>
                                <?php }
                            }else{?>
                                <p>No result found</p>
                            <?php } ?>
                            <div class="attach_foto_block">
                                <div id="in_uploaded_img" style="display:none;">0</div>
                                <div id="document-attach-photo"></div>
                                <div class="row uploader" style="position: relative;">
                                    <a style="display: none" href="javascript:void(0);" id="save_order">save</a>
                                    <div class="col-md-12">
                                        <div class="drag-and-drop-area drag_and_drop_area inner-padding">
                                            <p class="title">Click here or drag&amp;drop video to upload them to your gallery</p>
                                            <div class="cab_main_photo" id="div_foto_add">
                                                <span for="choose" id="link" class="choose"></span>
                                                <form id="choosevideoform" method="post" enctype="multipart/form-data">
                                                    <input type="hidden" name="MAX_FILE_SIZE" value="12345"/>
                                                    <input type="file" name="file" multiple accept="video/*" id="choosevideo"
                                                           style="display: none;"/>
                                                </form>
                                            </div>
                                            <span>Select a file in this format: avi, mp4, flv</span>
                                            <span>Should not exceed 20 Mb!</span>
                                        </div>
                                    </div>
                                    <span style="height: 183px;    position: absolute;    top: 0px;    width: 100%;    left: 0;"
                                          class="video_drop_here"></span>
                                    <div class="errors"></div>
                                    <div id="status1"></div>
                                </div>
                            </div>

                            <p class="private-note">Private gallery videos are not available for public viewing on the website. Thus you can use the private gallery for storing your special videos. You can send a video from this gallery to ladies you want. They will be seen only by ladies you have chosen. We maintain the confidentiality of your information and not share it!</p>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

