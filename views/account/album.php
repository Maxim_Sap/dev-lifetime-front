<?php 
use app\controllers\BaseController;

$session = Yii::$app->session;
use yii\helpers\Html;
//var_dump($session['user_data']); die;

?>
<div class="main sub-main">
	
	<div class="global-content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-3">
					<aside class="side-bar side-bar-left">						
						<?php echo $this->render('//layouts/parts/profile-vertical-menu-man.php'); ?>
					</aside>
				</div>
				<div class="col-xs-12 col-sm-9">
					<div class="my-gallery-container">
					<h2>My Albums</h2>

					<div class="created-albums-container">
							<div class="row">
				                <div class="drag_and_drop_area drag-and-drop-area pos-rel inner-padding">
					                <a href="javascript:void(0);" class="main-col-text">Click here or drag&amp;drop photos to upload them to your album</a>
					                
					                <div class="cab_main_photo" id="div_foto_add">
									    <span for="choose" id="link" class="choose"></span>
									    <form id="chooseform" method="post" enctype="multipart/form-data">
										    <input type="hidden" name="MAX_FILE_SIZE" value="12345"/>
										    <input type="file" name="file" multiple accept="image/*" id="choose" data-album-title="<?=$albumInfo->title;?>" data-place="albums" style="display: none;" />
									    </form>
								    </div>
					                <div>Select a file in this format: gif, jpg, jpeg, png</div>
					                <div>Should not exceed 2 Mb!</div>
					        	    <span class="drop_here pos-abs" data-album-title="<?=$albumInfo->title;?>" data-place="albums"></span>
					            </div> 
					            <?php if(!empty($photos)){
									foreach($photos as $photo){
										$img = empty($photo->medium_thumb) ? '/img/no_image.jpg' : $this->context->serverUrl."/".$photo->medium_thumb;?>
										<div class="col-xs-12 col-sm-4 col-md-4 photo-box">
											<?php if($photo->approve_status == BaseController::STATUS_NOT_APPROVED){?>
												<div class="approved-status no margin-left-14"><span>not approved</span></div>
											<?php }elseif($photo->approve_status == BaseController::STATUS_APPROVED){?>
												<div class="approved-status yes margin-left-14"><span>approved</span></div>
											<?php }elseif($photo->approve_status == BaseController::STATUS_DECLINED){?>
												<div class="approved-status decline margin-left-14"><span>decline</span></div>
											<?php }?>
											<div class="created-album">
												<div class="created-album-image" style="background-image: url('<?= $img ?>')">
													<a href="/account/photo/<?=$photo->id;?>" class="created-album-main-link"></a>									
												</div>
												<div class="created-album-description">
													<input id="radio_<?=$photo->id?>"
														   type="radio"
														   name="avatar"
														   value="<?=$photo->id?>"
														   style="position: relative;top: 3px;"
														   <?php if(isset($session['user_data']['avatarPhotoID']) && $session['user_data']['avatarPhotoID'] == $photo->id){echo 'checked';}?>
													>
													<label for="radio_<?=$photo->id?>">set as avatar</label>
													<?php if($photo->title){?>
													<a href="/account/photo/<?=$photo->id;?>" class="created-album-title-link"><?= Html::decode($photo->title); ?></a>	
													<?php } ?>												
												</div>
												<div class="like-icons">
													<span style="color: #10b510"><i class="fa fa-thumbs-up"></i><?= $photo->likes ?></span>
													<span style="color: red"><i class="fa fa-thumbs-down"></i><?= $photo->dislikes ?></span>
												</div>
											</div>
										</div>
										<?php }
					                    }else{?>
											<p>No photos</p>
					                    <?php } ?>								
							</div>
							<input id="set_photo_as_avatar" type="submit" value="SAVE" class="text-white main-col-bg btn uppercase fl-l" style="width: 140px;">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	


<style>
	.photo-box {
		position: relative;
	}
	.approved-status {
		position: absolute;
	    top: 5px;
	    left: 20px;
	    z-index: 10;
	    padding: 5px;
	}
	.approved-status.yes {
		background-color: green;
	}
	.approved-status.no {
		background-color: red;
	}

</style>