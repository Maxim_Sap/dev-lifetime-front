<?php 
use app\controllers\BaseController;

if($user_type == BaseController::USER_MALE){
	$outher_prefix = 'Girls';
}else{
	$outher_prefix = 'Mens';
} ?>
<div class="main sub-main">
	<div class="global-content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-3">
					<aside class="side-bar side-bar-left">
						<?= $this->render('//layouts/parts/profile-vertical-menu-man.php'); ?>
					</aside>
				</div>
				<div class="col-xs-12 col-sm-9">
					<?php
						if(!empty($girls)){?>
						<div class="">
							<h3><span class="text-pink">Blacklist</span> <?=$outher_prefix?></h3>
						</div>
						<?php
							foreach($girls as $user){?>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<?=$this->render('//layouts/parts/girls.php',['girl'=>$user,'userType'=>$user_type, 'blacklist' => 1])?>
							</div>
						<?php }?>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>