<div class="main sub-main">
    
    <div class="global-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3 hidden-xs">
                <?php echo $this->render('//layouts/parts/profile-vertical-menu-man.php'); ?>
                </div> 
                <div class="col-xs-12 col-sm-9">
                        <div class="my-gallery-container">                            
                            <h2>My albums</h2>                            
                                <div class="created-albums-container">
                                    <div class="row">
                                        <?php if(!empty($albums)){
                                            foreach($albums as $album){
                                                $img = empty($album->medium_thumb) ? '/img/no_image.jpg' : $this->context->serverUrl."/".$album->medium_thumb;
                                                ?>

                                                <div class="col-xs-12 col-sm-4 col-md-2">
                                                    <div class="created-album">
                                                        <div class="created-album-image" style="background-image: url('<?=$img;?>')">
                                                            <a href="/account/album/<?=$album->id;?>" class="created-album-main-link"></a>
                                                        </div>
                                                        <div class="created-album-description">
                                                            <a href="/account/album/<?=$album->id;?>" class="created-album-title-link"><?php if($album->title == 'default'){echo "avatar album (invisible)";}else{ echo $album->title;}?></a>
                                                            <span class="photos-quantity"><span><?php
                                                        $album->count_photos = !empty($album->count_photos) ? $album->count_photos : 0;
                                                        echo $album->count_photos;?></span> photos</span>
                                                        </div>
                                                    </div>
                                                </div>   
                                            <?php }
                                        }else{?>
                                            <p>No albums</p>
                                        <?php } ?>
                                    </div>  
                                </div>
                        </div>
                </div>
		    </div>
        </div>
	</div>
</div>

<?php $this->registerJsFile('/js/crop_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
