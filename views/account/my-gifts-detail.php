<?php
use app\controllers\BaseController;
$session = Yii::$app->session;
if($session['user_type'] == BaseController::USER_FEMALE){
    $prefix = 'men';
    $prefix2 = 'Men';
    $otherUserID = 'user_from_id';
    $otherUserName = 'user_from_name';
    $otherUserLastName = 'user_from_last_name';
}else{
    $prefix = 'girls';
    $prefix2 = 'Girls';
    $otherUserID = 'user_to_id';
    $otherUserName = 'user_to_name';
    $otherUserLastName = 'user_to_last_name';
}
?>
<div class="main sub-main">
    
    <div class="global-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <aside class="side-bar side-bar-left">
                        <?php echo $this->render('//layouts/parts/profile-vertical-menu.php'); ?>
                    </aside>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="gift-detail">
                        <?php if (!empty($giftsArray)) {
                        $gift = $giftsArray[0]; ?>
                        <table class="table custom-border detail-description">
                            <tr>
                                <td>
                                    <?=$prefix2?> name
                                </td>
                                <td>
                                    <a class="gift-recipient" href="/<?=$prefix.'/'.$gift->$otherUserID;?>"><?=$gift->$otherUserName . ' ' . $gift->$otherUserLastName;?></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Gifts title
                                </td>
                                <td>
                                    <span class="gift-detail-name"><?=$gift->gift_name;?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Gifts image
                                </td>
                                <td>
                                    <div class="gift-detail-thumb" style="background-image: url(<?=$this->context->serverUrl.'/uploads/gifts/thumb/'.$gift->gift_img;?>);">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Count
                                </td>
                                <td>
                                    <?=$gift->quantity;?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Price
                                </td>
                                <td>
                                    <?=$gift->gift_price;?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Date of purchase
                                </td>
                                <td>
                                    <?=$gift->created_at;?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Status
                                </td>
                                <td>
                                    <?php if($gift->order_status_id == 5){
                                        echo "delivered";
                                    }else{
                                        echo $gift->status_title;
                                    }?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="comment-title">
                                    Comment
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="comment-text">
                                    <?=(trim($gift->comment) != ''? trim($gift->comment) : 'no comments')?>
                                </td>
                            </tr>
                        </table>
                    <?php }else{?>
                        <p>No result found</p>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
