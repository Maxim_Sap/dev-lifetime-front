<?php
use app\controllers\BaseController;

$session = Yii::$app->session;

$userType = (isset($session['user_data']['userType']) &&  $session['user_data']['userType'] == BaseController::USER_FEMALE) ? BaseController::USER_FEMALE : BaseController::USER_MALE;

if (!empty($userData->personalInfo->birthday)) {
    $birthday = new \DateTime($userData->personalInfo->birthday);
    $tz  = new \DateTimeZone('Europe/Brussels');
    $age = \DateTime::createFromFormat('Y-m-d', $userData->personalInfo->birthday, $tz)
         ->diff(new \DateTime('now', $tz))
         ->y;
    // zodiak
    $signs = [ 
                "Capricorn", 
                "Aquarius", 
                "Pisces", 
                "Aries", 
                "Taurus", 
                "Gemini", 
                "Cancer", 
                "Leo", 
                "Virgo", 
                "Libra", 
                "Scorpio", 
                "Sagittarius"
            ];
    $signsstart = array(1=>21, 2=>20, 3=>20, 4=>20, 5=>20, 6=>20, 7=>21, 8=>22, 9=>23, 10=>23, 11=>23, 12=>23);
    $zodiak = (int)$birthday->format('d') < $signsstart[(int)$birthday->format('m')] ? $signs[(int)$birthday->format('m') - 1] : $signs[(int)$birthday->format('m') % 12];
} else {
    $age = "Not defined";
    $zodiak = "Not defined";
}
if (!empty($userData->personalInfo->weight)) {
    $weight = $userData->personalInfo->weight." kg (" . round($userData->personalInfo->weight/0.45359237)." lbs)";
} else {
    $weight = "Not defined";
}

if (!empty($userData->personalInfo->height)) {
    $heightInMeters = sprintf("%.2f", $userData->personalInfo->height/100);
    $heightInFeet = $userData->personalInfo->height*0.03280839895013123;
    $heightFeet = (int)$heightInFeet; // truncate the float to an integer
    $heightInches = round(($heightInFeet-$heightFeet)*12); 
    
    if ($heightInches == 0) {
        $height = "$heightInMeters m ($heightFeet')";
    } else {
        $height = "$heightInMeters m ($heightFeet' $heightInches'')";
    }
} else {
    $height = "Not defined";
}

if (!empty($userData->location)) {
    $user_city = $userData->city;
    foreach($countryList as $country) {
        if (isset($userData->location->country_id) && $userData->location->country_id == $country->id) {
            $user_country = $country->name;
        }        
    }    
} else {
    $user_city = "Not defined";
    $user_country = "Not defined";
}

$maritalArray = ['Single','Married','Divorced','Widowed','Complicated'];
if ($userData->personalInfo->marital != null && isset($maritalArray[$userData->personalInfo->marital-1])) {
    $marital = $maritalArray[$userData->personalInfo->marital-1];
} else {
    $marital = 'Not defined';
}

if ($userData->personalInfo->eyes != null) {
    foreach ($eyesColors as $eyesColor) {
        if ($userData->personalInfo->eyes == $eyesColor['id'])
            $eyes = $eyesColor['name'];
    }
} else {
    $eyes = 'Not defined';
}

if ($userData->personalInfo->hair != null) {
    foreach ($hairColors as $hairColor) {
        if ($userData->personalInfo->hair == $hairColor['id'])
            $hair = $hairColor['name'];
    }    
} else {
    $hair = 'Not defined';
}

if (isset($userData->personalInfo->kids)) {    
    $children = $userData->personalInfo->kids;
    if ($children == 0) {
        $children = "no";
    }
} else {
    $children = 'Not defined';
}

$smokingArray = ['Yes','No','Casual','Heavy'];
if ($userData->personalInfo->smoking != null && isset($smokingArray[$userData->personalInfo->smoking-1])) {
    $smoking = $smokingArray[$userData->personalInfo->smoking-1];
} else {
    $smoking = 'Not defined';
}

$englishLevelArray = ['Beginner','Intermediate','Advanced','Fluent'];
if ($userData->personalInfo->english_level != null && isset($englishLevelArray[$userData->personalInfo->english_level-1])) {
    $englishLevel = $englishLevelArray[$userData->personalInfo->english_level-1];
} else {
    $englishLevel = 'Not defined';
}

if ($userData->userType == BaseController::USER_MALE) {
	$baseUrl = 'men';
	$text = 'Men';
	$baseUrl2 = 'girls';
	$text2 = 'Ladies';
} else {
	$baseUrl = 'girls';
	$text = 'Ladies';
	$baseUrl2 = 'men';
	$text2 = 'Men';

}
?>
<div class="main sub-main">
    <div class="global-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <aside class="side-bar side-bar-left">                        
                        <?php echo $this->render('//layouts/parts/message-vertical-menu-man.php'); ?> 
                    </aside>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="profile-content-container">
                        <div class="row">

                            <div class="col-xs-12 col-sm-4 col-md-3">
                                <div class="profile-image-container">
                                    <img src="<?= $userData->avatar->normal ?>" alt="" class="profile-image">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-8 col-md-9">
                                <div class="profile-description-container">
                                    <div class="person-profile-info-block clearfix">
                                        <h5 class="person-profile-info-block-title"><span>personal info</span></h5>

                                        <ul class="person-profile-info-list">
                                            <li>
                                                <span class="person-profile-info-list-title"><span>Age</span></span>
                                                <span class="person-profile-info-list-description"><?= $age ?></span>
                                            </li>
                                            <li>
                                                <span class="person-profile-info-list-title"><span>Zodiac</span></span>
                                                <span class="person-profile-info-list-description"><?= $zodiak ?></span>
                                            </li>
                                            <li>
                                                <span class="person-profile-info-list-title"><span>Eyes color</span></span>
                                                <span class="person-profile-info-list-description"><?= $eyes ?></span>
                                            </li>
                                            <li>
                                                <span class="person-profile-info-list-title"><span>Hair color</span></span>
                                                <span class="person-profile-info-list-description"><?= $hair ?></span>
                                            </li>
                                            <li>
                                                <span class="person-profile-info-list-title"><span>Height</span></span>
                                                <span class="person-profile-info-list-description"><?= $height ?></span>
                                            </li>
                                            <li>
                                                <span class="person-profile-info-list-title"><span>Weight</span></span>
                                                <span class="person-profile-info-list-description"><?= $weight ?></span>
                                            </li>
                                        </ul>

                                        <ul class="person-profile-info-list">
                                            <li>
                                                <span class="person-profile-info-list-title"><span>Country</span></span>
                                                <span class="person-profile-info-list-description"><?= $user_country ?></span>
                                            </li>
                                            <li>
                                                <span class="person-profile-info-list-title"><span>City</span></span>
                                                <span class="person-profile-info-list-description"><?= $user_city ?></span>
                                            </li>
                                            <li>
                                                <span class="person-profile-info-list-title"><span>Marital status</span></span>
                                                <span class="person-profile-info-list-description"><?= $marital ?></span>
                                            </li>
                                            <li>
                                                <span class="person-profile-info-list-title"><span>Children</span></span>
                                                <span class="person-profile-info-list-description"><?= $children ?></span>
                                            </li>
                                            <li>
                                                <span class="person-profile-info-list-title"><span>Smoker</span></span>
                                                <span class="person-profile-info-list-description"><?= $smoking ?></span>
                                            </li>
                                            <li>
                                                <span class="person-profile-info-list-title"><span>English</span></span>
                                                <span class="person-profile-info-list-description"><?= $englishLevel ?></span>
                                            </li>
                                        </ul>
                                        <?php if ($userType == BaseController::USER_MALE) {?>
                                        <div class="edit-profile-button-container">
                                            <a href="/account/edit/<?= $userData->userID ?>" class="edit-profile">Edit Profile</a>
                                        </div>
                                        <?php } ?>

                                    </div>

                                    <div class="person-profile-info-block clearfix">
                                        <h5 class="person-profile-info-block-title"><span>About me</span></h5>
                                        <?=  $userData->personalInfo->about_me ?>
                                    </div>

                                    <div class="person-profile-info-block clearfix">
                                        <h5 class="person-profile-info-block-title"><span>My Desire</span></h5>
                                        <?= $userData->personalInfo->my_ideal ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<?php
	$session = Yii::$app->session;
	if (isset($session['message']) && is_string($session['message'])) {
		echo " " . $session['message'];
		unset($session['message']);
	}	
?>


