<div class="main sub-main">
    <div class="global-content-wrapper">
        <?php //echo $this->render('//layouts/parts/profile-horizontal-menu.php'); ?>
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-3 profile-left-menu hidden-xs">
                    <?php echo $this->render('//layouts/parts/message-vertical-menu-man.php'); ?>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="edit_profile_block">
                            <div class="col-md-12">
                                <h2>Edit password</h2>
                            </div>
                        </div>
                    </div>
                    <div class="edit_profile_fields">
                        <form class="edit-password">
                            <div class="field_column fl-l">
                                <div class="sm_left fl">Your old password:</div>
                                <input type="password" name="oldPassword" class="form-control" required>
                            </div>
                            <div class="field_column fl-l">
                                <span class="sm_left fl-l">New password:</span>
                                <input type="password" name="newPassword" class="form-control fl-l" required>
                            </div>
                            <div class="clear"></div>
                            <div class="field_column fl-l">
                                <span class="sm_left fl-l">Confirm password:</span>
                                <input type="password" name="confirmPassword" class="form-control fl-l" required>
                            </div>
                            <div class="clear"></div>
                            <div class="field_column fl-l">
                                <span class="sm_left fl-l"></span>
                                <button class="btn">save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
	                