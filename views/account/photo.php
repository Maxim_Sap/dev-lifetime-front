<?php use yii\helpers\Html; ?>
<style>
    .delete_link {
        position: absolute;
        top: 10px;
        right: 10px;
    }
</style>
<div class="main sub-main">
    
    <div class="global-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <aside class="side-bar side-bar-left">
                        <?php echo $this->render('//layouts/parts/profile-vertical-menu-man.php'); ?>
                    </aside>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="my-gallery-container">
                        <h2>Edit Photo</h2>
                        <div class="">
                            <div class="row">
                                <?php if(!empty($photo)){
                                $img_original = empty($photo->original_image) ? '/img/no_image.jpg' : $this->context->serverUrl."/".$photo->original_image;
                                $img = empty($photo->medium_thumb) ? '/img/no_image.jpg' : $this->context->serverUrl."/".$photo->medium_thumb;
                                ?>
                                <div class="col-md-6 col-sm-6 col-xs-6 album_wrap">
                                    <div class="album_wrap">
                                        <a href="<?=$img_original;?>" target="_blank" class="foto_bg">
                                            <img class="img-responsive" src="<?=$img;?>" alt="">            
                                        </a>
                                        <a onclick="if(confirm('Are you sure you want delete this photo?')){return true}else{return false;};" href="/account/photo-delete/<?=$photo->id;?>" class="delete_link fl-r"><i class="fa fa-trash-o text-red" aria-hidden="true"></i> Delete</a>
                                        <div class="foto_foot">
                                            <form action="" class="photo-title-form">        
                                                <input id="photo-id" type="hidden" value="<?=$photo->id;?>">
                                                <label>Photo title: <input id="photo-title" class="custom-border fl-l" placeholder="photo title" type="text" value="<?= Html::decode($photo->title); ?>"></label>
                                                <input type="submit" value="update" class="text-white main-col-bg btn uppercase">
                                            </form>
                                            
                                            <a id="set-album-cover" class="btn pull-left" data-id="<?=$photo->id;?>" data-album-title="<?=$photo->album_id;?>" href="#">Set as album cover</a>
                                        </div>
                                    </div>
                                </div>
                                <?php }else{ ?>
                                    <p>No photo</p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
