<style>
    .gift-tabs .tab-list-wrapper .tab-list .tab{
        margin-right: 16px;
    }
    .gift-item .gift-item-title{
        min-height: 40px;

    }
</style>
<?php
$serverUrl  = $this->context->serverUrl . '/';
$category   = isset($search_params['giftsCategory'][0]) ? $search_params['giftsCategory'][0] : '';
$sort       = isset($search_params['sort']) && $search_params['sort'] == 'asc' ? 'asc' : 'desc';
$ajax       = Yii::$app->request->getIsAjax();
$gift_types = ['accessory' => 1, 'cellphone' => 2, 'notebook' => 3, 'perfume' => 4, 'food' => 5, 'other' => 6, 'individual gifts' => 7, 'flowers' => 8, 'lingerie' => 9, 'sport' => 11, 'plush toys' => 10,];
if (!$ajax){
    $tz = new \DateTimeZone('Europe/Brussels');
    if ($user_info->personalInfo->birthday) {
        $age = \DateTime::createFromFormat('Y-m-d', $user_info->personalInfo->birthday, $tz)
            ->diff(new \DateTime('now', $tz))
            ->y;
    } else {
        $age = 'No set';
    }

    //var_dump($user_info);die;
    $label_new    = (time() - strtotime($user_info->created_at) < 60 * 60 * 24 * 30) ? 1 : 0;
    $label_online = (time() - $user_info->last_activity <= 30) ? 1 : 0;
    $session      = Yii::$app->session;
    $count        = 0;
    $amount       = 0;
    if (isset($session['basket']) && is_array($session['basket'])) {
        foreach ($session['basket'] as $gift_id => $one) {
            $count += $one['count'];
            $amount += $one['price'];
        }
    }
    ?>
    <div class="main sub-main">
        <div class="global-content-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="gifts">
                            <div class="row">
                                <div class="col-xs-12 col-sm-3 col-md-2">
                                    <?=$this->render('/layouts/parts/shop-gifts-sidebar.php', [
                                        'avatar'        => $avatar,
                                        'label_new'     => $label_new,
                                        'label_online'  => $label_online,
                                        'cam_online'    => $user_info->cam_online,
                                        'first_name'    => $user_info->personalInfo->first_name,
                                        'in_favorite'   => $user_info->in_favorite,
                                        'age'           => $age,
                                        'count'         => $count,
                                        'amount'        => $amount,
                                        'other_user_id' => $other_user_id,
                                    ])?>
                                </div>
                                <div class="col-xs-12 col-sm-9 col-md-10">
                                    <div id="gifts_content" class="gifts-content tabs gift-tabs">
                                        <?php } //end not ajax?>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="tab-list-wrapper">
                                                    <ul class="list-inline tab-list gifts_categories">
                                                        <li class="tab <?=($category == '') ? 'active' : '';?>">All
                                                            Gifts
                                                        </li>
                                                        <li class="tab <?=($category == $gift_types['perfume']) ? 'active' : '';?>"
                                                            data-category="<?=$gift_types['perfume']?>">Perfume
                                                        </li>
                                                        <li class="tab <?=($category == $gift_types['flowers']) ? 'active' : '';?>"
                                                            data-category="<?=$gift_types['flowers']?>">Flowers
                                                        </li>
                                                        <li class="tab <?=($category == $gift_types['food']) ? 'active' : '';?>"
                                                            data-category="<?=$gift_types['food']?>">Food
                                                        </li>
                                                        <li class="tab <?=($category == $gift_types['sport']) ? 'active' : '';?>"
                                                            data-category="<?=$gift_types['sport']?>">Sport
                                                        </li>
                                                        <li class="tab <?=($category == $gift_types['plush toys']) ? 'active' : '';?>"
                                                            data-category="<?=$gift_types['plush toys']?>">Plush toys
                                                        </li>
                                                        <li class="tab <?=($category == $gift_types['other']) ? 'active' : '';?>"
                                                            data-category="<?=$gift_types['other']?>">Other
                                                        </li>
                                                    </ul>
                                                    <div class="sort-price">
                                                        <span class="text">Sort by price:</span>
                                                        <a href="javascript:void(0);"
                                                           class="price-up <?=($sort == 'asc' ? 'active' : '');?>"
                                                           data-value="asc">
                                                            <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                                        </a>
                                                        <a href="javascript:void(0);"
                                                           class="price-down <?=($sort == 'desc' ? 'active' : '');?>"
                                                           data-value="desc">
                                                            <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-panel-container">
                                            <div class="tab-panel active">
                                                <div class="tab-panel-content">
                                                    <div class="row">
                                                        <?php foreach ($giftsArray as $gift) { ?>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <div class="gift-item">
                                                                    <h3 class="gift-item-title"><?=$gift->name?></h3>
                                                                    <div class="gift-item-preview">
                                                                        <img
                                                                            src="<?=$serverUrl . 'uploads/gifts/thumb/' . $gift->image?>"
                                                                            alt="" class="gift-item-preview-image">
                                                                        <div class="gift-item-cost">
                                                                            <p><span><?=$gift->price?></span> credit</p>
                                                                        </div>
                                                                        <div class="gift-item-gallery">
                                                                            <a href="<?=$serverUrl . 'uploads/gifts/large/' . $gift->image?>"
                                                                               data-fancybox="food-1"
                                                                               title="<?=$gift->description?>"></a>
                                                                        </div>
                                                                    </div>
                                                                    <a href="#"
                                                                       onclick="window['accountLibrary']['addGiftToCart'](<?=$gift->id?>, <?=$gift->price?>);return false;"
                                                                       class="btn btn-gift-item">Add to Card</a>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <?php echo $this->render('//layouts/parts/shop-pagination.php', ['count' => $total_count, 'page' => $page]); ?>
                                            </div>
                                        </div>
                                        <?php if (!$ajax){ ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <script type="text/javascript">
        gift_optimization();
    </script>
<?php } ?>