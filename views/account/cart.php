<?php
$serverUrl = $this->context->serverUrl . '/';
$tz  = new \DateTimeZone('Europe/Brussels');
if($user_info->personalInfo->birthday){
    $age = \DateTime::createFromFormat('Y-m-d', $user_info->personalInfo->birthday, $tz)
        ->diff(new \DateTime('now', $tz))
        ->y;
}else{
    $age = 'No set';
}
//var_dump($user_info);die;
$label_new = (time() - strtotime($user_info->created_at) < 60*60*24*30) ? 1 : 0;
$label_online = (time() - $user_info->last_activity <= 30) ? 1 : 0;
$session = Yii::$app->session;
$count = 0;
$amount = 0;
if(isset($session['basket']) && is_array($session['basket'])){
    foreach($session['basket'] as $gift_id => $one){
        $count += $one['count'];
        $amount += $one['price'];
    }
}
?>
<div class="main sub-main">

    <div class="global-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <div class="gifts">

                        <div class="row">
                            <div class="col-xs-12 col-sm-3 col-md-2">
                                <?= $this->render('/layouts/parts/shop-gifts-sidebar.php', [
                                    'avatar' => $avatar,
                                    'label_new' => $label_new,
                                    'label_online' => $label_online,
                                    'cam_online' => $user_info->cam_online,
                                    'first_name' => $user_info->personalInfo->first_name,
                                    'in_favorite' => $user_info->in_favorite,
                                    'age' => $age,
                                    'other_user_id' => $other_user_id,
                                    'cart'=>'yes'
                                ]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-9 col-md-10">
                                <div class="gifts-add-container">
                                    <h2 class="gifts-add-title">Confirm gifts</h2>

                                    <div class="gifts-add-block desktop hidden-xs">
                                        <table class="gifts-add-block-table">
                                            <tr class="gifts-add-block-table-title-line">
                                                <th colspan="2">Gift</th>
                                                <th>Quantity</th>
                                                <th>Price</th>
                                                <th></th>
                                            </tr>
                                            <?php if(!empty($giftsArray)){
                                                foreach ($giftsArray as $gift){?>
                                                    <tr class="gift-row gifts-add-block-table-content-line" data-gift-id="<?=$gift->id;?>">
                                                        <td colspan="2">
                                                            <span class="gift-image"><img src="<?=$serverUrl."uploads/gifts/thumb/".$gift->image?>" alt="<?=$gift->description?>"></span>
                                                            <span class="gift-description"><?=$gift->name?></span>
                                                        </td>
                                                        <td>
                                                            <ul class="gift-quantity list-inline">
                                                                <li class="less" data-price="<?=$gift->price?>">
                                                                    <span>-</span>
                                                                </li>
                                                                <li class="quantity-counter"><?=$basket[$gift->id]['count']?></li>
                                                                <li class="more" data-price="<?=$gift->price?>">
                                                                    <span>+</span>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                        <td>
                                                            <span class="total-price"><span><?=$gift->price?> </span>cr</span>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0);" onclick="var place=$(this).parents('.gifts-add-block');$(this).parents('.gift-row').remove();reCalcTotalAmount(place);" class="remove-gift"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</a>
                                                        </td>
                                                    </tr>
                                                <?php }
                                            } ?>
                                            <tr class="gifts-add-block-table-payment-line">
                                                <!--                                                <td>-->
                                                <!--                                                    <a href="javascript:void(0);" class="btn">Add postcard</a>-->
                                                <!--                                                </td>-->
                                                <td colspan="3">
                                                    <span class="total-text">You ordered <span class="total-quantity"><?=$count?></span> gifts for <span class="total-credits"><?=$amount?></span> credits ( <span class="total-price"><?=$amount?> USD</span> )</span>
                                                </td>
                                                <td colspan="2">
                                                    <a href="javascript:void(0);" class="confirm_order btn">Make an Order</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="gifts-add-block mobile visible-xs">

                                        <?php if(!empty($giftsArray)){
                                            foreach ($giftsArray as $gift){?>
                                                <div class="gift-row gifts-add-block-item" data-gift-id="<?=$gift->id;?>">

                                                    <div class="gifts-add-block-item-image">
                                                        <img src="<?=$serverUrl."uploads/gifts/thumb/".$gift->image?>" alt="<?=$gift->description?>">
                                                    </div>

                                                    <div class="gifts-add-block-item-description">
                                                        <span><?=$gift->name?></span>
                                                    </div>

                                                    <div class="gifts-add-block-item-quantity">
                                                        <div class="gifts-add-block-item-quantity-left">
                                                            <ul class="gift-quantity list-inline">
                                                                <li class="less" data-price="<?=$gift->price?>">
                                                                    <span>-</span>
                                                                </li>
                                                                <li class="quantity-counter"><?=$basket[$gift->id]['count']?></li>
                                                                <li class="more" data-price="<?=$gift->price?>">
                                                                    <span>+</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="gifts-add-block-item-quantity-right">
                                                            <span class="total-price"><span><?=$gift->price?> </span>cr</span>
                                                        </div>
                                                    </div>

                                                    <a href="javascript:void(0);" class="remove-gift" onclick="var place=$(this).parents('.gifts-add-block');$(this).parents('.gift-row').remove();reCalcTotalAmount(place);"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</a>

                                                </div>
                                            <?php }
                                        } ?>

                                        <div class="gifts-add-block-payment">
                                            <!--                                            <a href="javascript:void(0);" class="btn">Add postcard</a>-->
                                            <span class="total-text">You ordered <span class="total-quantity"><?=$count?></span> gifts for <span class="total-credits"><?=round($amount,2)?></span> credits ( <span class="total-price"><?=round($amount,2)?> USD</span> )</span>
                                            <a href="javascript:void(0);" class="btn confirm_order">Make an Order</a>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>

</script>