<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */
use yii\helpers\Html;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="profile-container">
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>Please choose your new password:</p>

        <div class="row">
            <div class="col-lg-3">
                <form class="reset-password-block-form" method="post">
                <input class="form-control" name="password" type="password" placeholder="Password" required=""> <br>
                <input class="form-control" name="confirmPassword" type="password" placeholder="Confirm password" required=""> <br>
                <div><input type="submit" value="Reset Password" class="btn btn-info form-control"></div>
                <p></p>
            </form>
            </div>
        </div>
    </div>
</section>