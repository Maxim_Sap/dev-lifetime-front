<?php
use app\controllers\BaseController;
use yii\helpers\Html;
if (!empty($userData->personalInfo->looking_age_from) && !empty($userData->personalInfo->looking_age_to)) {
	$looking_age_from = $userData->personalInfo->looking_age_from;
	$looking_age_to = $userData->personalInfo->looking_age_to;
}else{
	$looking_age_from = "";
	$looking_age_to = "";
}

?>
<div class="main sub-main">
	<div class="global-content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-3 hidden-xs">
					<aside class="side-bar side-bar-left">
						<?php echo $this->render('//layouts/parts/message-vertical-menu-man.php'); ?>
					</aside>
				</div>
				<div class="col-xs-12 col-sm-9">
					<div class="edit-profile-container">
						<div class="row">
							<div class="col-xs-12">
								<h2>Edit profile</h2>
							</div>
						</div>
						<div class="edit_profile_block clearfix">
							<div class="row">
								<div class="col-xs-12 col-md-5">
								<div class="edit_profile_avatar img_bg rounded-block fl-l" style="background-image: url(<?=$userData->avatar->normal;?>);"></div> 
									<div class="change_avatar_block fl-l">                        
										<span>
											To change your avatar
											<a id="change_avatar" href="javascript:void(0);"><span class="change_avatar_btn main-col-text custom-border">click here</span></a>
											<form id="avatarform" method="post" enctype="multipart/form-data">
												<input name="MAX_FILE_SIZE" value="12345" type="hidden">
												<input name="file" multiple="" accept="image/*" id="avatar_file" data-album-title="letters" style="display: none;" type="file">
											</form>
										</span>
									</div>    
								</div>
								<div class="col-xs-12 col-md-7">
									<div class="edit_pass_block">
										<span>
											If you need edit your password account, please press:
											<a href="/account/edit-password"><span class="edit_pass_btn custom-border">edit password</span></a>
										</span>
									</div>
								</div>
							</div>                           
						</div>
		                <div class="edit_profile_fields">
			                <form class="edit-profile-form clearfix">
			                    <div class="field_column fl-l field-left" id="field-id">
			                        <span class="sm_left fl-l">Your ID:</span>
			                        <span><?=$userData->personalInfo->id;?></span> 
			                    </div>
		                        <div class="field_column fl-l field-right">
			                        <span class="sm_left fl-l">Weight (Kg)</span>
			                        <input type="number" name="weight" class="custom-border fl-l" <?=(!empty($userData->personalInfo->weight)) ? 'value="'.Html::decode($userData->personalInfo->weight).'"' : "placeholder='---'";?>>
			                    </div>
			                    <div class="field_column fl-l field-left">
			                        <span class="sm_left fl-l">First Name</span>
			                        <input type="text" name="first_name" class="custom-border fl-l" value="<?= Html::decode($userData->personalInfo->first_name);?>" required> 
			                    </div>
			                    <div class="field_column fl-l field-right">
			                        <span class="sm_left fl-l">Height (Cm)</span>
			                        <input type="number" name="height" class="custom-border fl-l" <?=(!empty($userData->personalInfo->height)) ? 'value="'.Html::decode($userData->personalInfo->height).'"' : "placeholder='---'";?>>
			                    </div>
			                    <div class="field_column fl-l field-left">
			                        <span class="sm_left fl-l">Last Name</span>
			                        <input type="text" name="last_name" class="custom-border fl-l" <?=(!empty($userData->personalInfo->last_name)) ? 'value="'.Html::decode($userData->personalInfo->last_name).'"' : "placeholder='---'";?>> 
			                    </div>
		                        <div class="field_column fl-l field-right">
			                        <span class="sm_left fl-l">Hair</span>
			                        <div class="custom-select active">
				                        <select class="selectpicker custom-border" name="hair" data-actions-box="true" data-title="--- please select ---">
											<option value="">--- please select ---</option>
											<?php 
											foreach ($hairColors as $hairColor) { ?>
												<option
													value="<?=$hairColor['id'];?>" <?=($userData->personalInfo->hair == $hairColor['id']) ? "selected" : "";?>>
													<?=$hairColor['name']?>
												</option>
											<?php }?>
										</select>
										<a href="javascript:void(0);" class="clear-select-button"></a>
									</div>
			                    </div>
		                        <div class="field_column fl-l field-left">
			                        <span class="sm_left fl-l">Your email</span>  
			                        <input type="email" name="email" class="custom-border fl-l" value="<?=$userData->email;?>">
			                    </div>
			                    <div class="field_column fl-l field-right">
			                        <span class="sm_left fl-l">Date of birth</span>  
			                        <input type="text" name="birthday" class="custom-border fl-l datepicker" data-date-format="dd-mm-yyyy" value="<?=($userData->personalInfo->birthday != null && strtotime($userData->personalInfo->birthday)) ? date("d-m-Y",strtotime($userData->personalInfo->birthday)): '';?>">
			                    </div>

		                        <div class="field_column fl-l field-left">
			                        <span class="sm_left fl-l">Country</span>
			                        <div class="custom-select">
				                        <select class="selectpicker custom-border" name="country" data-live-search="true" data-actions-box="true" data-title="--- please select ---" data-size="8">
											<option value="">--- please select ---</option>
											<?php if(!empty($countryList)) {
												foreach($countryList as $country) {?>										
													<option value="<?=$country->id;?>" <?=(isset($userData->location->country_id) && $userData->location->country_id == $country->id)? "selected" : "";?>><?=$country->name;?></option>
												<?php }
											} ?>
										</select>
										<a href="javascript:void(0);" class="clear-select-button"></a>
									</div>
			                    </div>
			                    
			                    <div class="field_column fl-l field-right">
			                        <span class="sm_left fl-l">Eyes</span>
			                        <div class="custom-select">
				                        <select class="selectpicker custom-border" name="eyes" data-actions-box="true" data-title="--- please select ---">
											<option value="">--- please select ---</option>
											<?php 
											foreach ($eyesColors as $eyesColor){ ?>
												<option
													value="<?= $eyesColor['id'];?>" <?=($userData->personalInfo->eyes == $eyesColor['id']) ? "selected" : "";?>>
													<?= $eyesColor['name'] ?>
												</option>
											<?php }?>
										</select>
										<a href="javascript:void(0);" class="clear-select-button"></a>
									</div>
			                    </div>
			                    <div class="field_column fl-l field-left">
			                        <span class="sm_left fl-l">Region</span>
			                        <input type="text" name="region" class="custom-border fl-l" value="<?=(isset($userData->city)) ? Html::decode($userData->city) : "";?>" placeholder="<?=(isset($userData->city)) ? "" : "---";?>">
			                    </div>
			                    <div class="field_column fl-l field-right">
			                        <span class="sm_left fl-l">Education</span>
			                        <div class="custom-select">
				                        <select class="selectpicker custom-border" name="education" data-actions-box="true" data-title="--- please select ---">
											<option value="">--- please select ---</option>
											<?php $educationArray = ['None','Primary School','Hight School','College','University Degree'];
											$i = 0;
											foreach ($educationArray as $edication) { $i++?>
												<option
													value="<?=$i;?>" <?=($userData->personalInfo->education == $i) ? "selected" : "";?>>
													<?=$educationArray[$i-1]?>
												</option>
											<?php }?>
										</select>
										<a href="javascript:void(0);" class="clear-select-button"></a>
									</div>
			                    </div>
			                    <div class="field_column fl-l field-left">
			                        <span class="sm_left fl-l">Religion</span>
			                         <div class="custom-select">
				                        <select class="selectpicker custom-border" name="religion" data-actions-box="true" data-title="--- please select ---">
											<option value="">--- please select ---</option>
											<?php $religionArray = ['Catholicity','Christianity','Ateism','Islam','Hinduism','Buddhism','Folk','Judaism','Orthodox','Other'];
											$i = 0;
											foreach ($religionArray as $religion){ $i++?>
												<option
													value="<?=$i;?>" <?=($userData->personalInfo->religion == $i) ? "selected" : "";?>>
													<?=$religionArray[$i-1]?>
												</option>
											<?php }?>
										</select>
										<a href="javascript:void(0);" class="clear-select-button"></a>
									</div>
			                    </div>
			                    <div class="field_column fl-l field-right">
			                        <span class="sm_left fl-l">Ocupation</span>
			                        <input type="text" name="occupation" class="custom-border fl-l" <?=(!empty($userData->personalInfo->occupation)) ? 'value="'.Html::decode($userData->personalInfo->occupation).'"' : "placeholder='---'";?>>
			                    </div>
								<div class="field_column fl-l field-left">
									<span class="sm_left fl-l">Ethnicity</span>
									<div class="custom-select">
										<select class="selectpicker custom-border" name="ethnos" data-actions-box="true" data-title="--- please select ---">
											<option value="">--- please select ---</option>
											<?php $ethnosArray = ['European','African','American','Asian','East Asian','Spanish','Indian','Latin American','Mediterranean','Caucasian','Middle Eastern','Mixed'];
											$i = 0;
											foreach ($ethnosArray as $ethnos){ $i++?>
												<option
													value="<?=$i;?>" <?=($userData->personalInfo->ethnos == $i) ? "selected" : "";?>>
													<?=$ethnosArray[$i-1]?>
												</option>
											<?php }?>
										</select>
										<a href="javascript:void(0);" class="clear-select-button"></a>
									</div>
								</div>
								<div class="field_column fl-l field-right">
									<span class="sm_left fl-l">English level</span>
									<div class="custom-select">
										<select class="selectpicker custom-border" name="english_level" data-actions-box="true" data-title="--- please select ---">
											<option value="">--- please select ---</option>
											<?php $englishLevelArray = ['Beginner','Intermediate','Advanced','Fluent'];
											$i = 0;
											foreach ($englishLevelArray as $level){ $i++?>
												<option
													value="<?=$i;?>" <?=($userData->personalInfo->english_level == $i) ? "selected" : "";?>>
													<?=$englishLevelArray[$i-1]?>
												</option>
											<?php }?>
										</select>
										<a href="javascript:void(0);" class="clear-select-button"></a>
									</div>
								</div>
								<div class="field_column fl-l field-left">
									<span class="sm_left fl-l">Address</span>
									<input type="text" name="address" class="custom-border fl-l" <?=(!empty($userData->personalInfo->address)) ? 'value="'.Html::decode($userData->personalInfo->address).'"' : "placeholder='---'";?>>
								</div>
								<div class="field_column fl-l field-right">
									<span class="sm_left fl-l">Other languages</span>
									<input type="text" name="other_language" class="custom-border fl-l" <?=(!empty($userData->personalInfo->other_language)) ? 'value="'.Html::decode($userData->personalInfo->other_language).'"' : "placeholder='---'";?>>
								</div>
			                    <!--<div class="field_column">
			                        <span class="sm_left">City</span>
			                        <input type="text" class="form-control" id="usr">
			                    </div>-->
			                    <div class="field_column fl-l field-right">
			                        <span class="sm_left fl-l">Marital status</span>
			                        <div class="custom-select">
				                        <select class="selectpicker custom-border" name="marital" data-actions-box="true" data-title="--- please select ---">
											<option value="">--- please select ---</option>
											<?php $maritalArray = ['Single','Married','Divorced','Widowed','Complicated'];
											$i = 0;
											foreach ($maritalArray as $marital){ $i++?>
												<option
													value="<?=$i;?>" <?=($userData->personalInfo->marital == $i) ? "selected" : "";?>>
													<?=$maritalArray[$i-1]?>
												</option>
											<?php }?>
										</select>
										<a href="javascript:void(0);" class="clear-select-button"></a>
									</div>
			                    </div>
			                    <div class="field_column fl-l field-left">
			                        <span class="sm_left fl-l">Smoking</span>
			                        <div class="custom-select">
				                        <select class="selectpicker custom-border" name="smoking" data-actions-box="true" data-title="--- please select ---">
											<option value="">--- please select ---</option>
											<?php $smokingArray = ['Yes','No','Casual','Heavy'];
											$i = 0;
											foreach ($smokingArray as $smoking){ $i++?>
												<option
													value="<?=$i;?>" <?=($userData->personalInfo->smoking == $i) ? "selected" : "";?>>
													<?=$smokingArray[$i-1]?>
												</option>
											<?php }?>
										</select>
										<a href="javascript:void(0);" class="clear-select-button"></a>
									</div>
			                    </div>
			                    <div class="field_column fl-l field-right">
			                        <span class="sm_left fl-l">Number of children</span>
			                        <input type="number" name="kids" class="custom-border fl-l" <?=($userData->personalInfo->kids !== null) ? 'value="'.Html::decode($userData->personalInfo->kids).'"' : "placeholder='---'";?>>
			                    </div>
			                    <div class="field_column fl-l field-left">
			                        <span class="sm_left fl-l">Drinking</span>
			                        <div class="custom-select">
				                        <select class="selectpicker custom-border" name="alcohol" data-actions-box="true" data-title="--- please select ---">
											<option value="">--- please select ---</option>
											<?php $alcoholArray = ['Yes','No','Casual','Heavy'];
											$i = 0;
											foreach ($alcoholArray as $alcohol){ $i++?>
												<option
													value="<?=$i;?>" <?=($userData->personalInfo->alcohol == $i) ? "selected" : "";?>>
													<?=$alcoholArray[$i-1]?>
												</option>
											<?php }?>
										</select>
										<a href="javascript:void(0);" class="clear-select-button"></a>
									</div>
			                    </div>
			                    <div class="field_column fl-l field-left">
			                        <span class="sm_left fl-l" style="line-height: 1.2">Looking for an age group</span>
			                        <div class="custom-select small fl-l">
				                        <select class="selectpicker custom-border" name="looking_age_from"  data-actions-box="true" data-title="---">
											<option value="">--- please select ---</option>
											<?php for($i = 18; $i<=65; $i++){?>
												<option value="<?=$i;?>" <?=($looking_age_from == $i)? "selected" : "";?>><?=$i;?></option>
											<?php } ?>
										</select>
										<a href="javascript:void(0);" class="clear-select-button"></a>
									</div>
									<div class="delimiter fl-l">-</div>
									<div class="custom-select small fl-l">
				                        <select class="selectpicker custom-border" name="looking_age_to" data-actions-box="true" data-title="---">
				                        <option value="">--- please select ---</option>
											<?php for($i = 18;$i<=65;$i++){?>
												<option value="<?=$i;?>" <?=($looking_age_to == $i)? "selected" : "";?>><?=$i;?></option>
											<?php } ?>
										</select>
										<a href="javascript:void(0);" class="clear-select-button"></a>
									</div>
			                    </div>
			                    <div class="field_column fl-l field-right">
			                        <span class="sm_left fl-l">Physique</span>
			                        <div class="custom-select active">
				                        <select class="selectpicker custom-border" name="physique" data-actions-box="true" data-title="--- please select ---">
											<option value="">--- please select ---</option>
											<?php 
											foreach ($physiques as $physique) { ?>
												<option
													value="<?=$physique['id'];?>" <?=($userData->personalInfo->physique == $physique['id']) ? "selected" : "";?>>
													<?=$physique['name']?>
												</option>
											<?php }?>
										</select>
										<a href="javascript:void(0);" class="clear-select-button"></a>
									</div>
			                    </div>
			                    <div class="field_column fl-l field-left">
			                        <span class="sm_left fl-l">Phone</span>
			                        <input type="text" name="phone" class="custom-border fl-l" <?=(!empty($userData->personalInfo->phone)) ? 'value="'.Html::decode($userData->personalInfo->phone).'"' : "placeholder='---'";?>> 
			                    </div>
								<?php if ($userData->userType == BaseController::USER_FEMALE){?>
									<div class="field_column fl-l field-right">
										<span class="sm_left fl-l">Passport S/N</span>
										<input type="text" name="pasport" class="custom-border fl-l" <?=($userData->personalInfo->passport !== null) ? 'value="'.Html::decode($userData->personalInfo->passport).'"' : "placeholder='SSNNNNNN'";?>>
									</div>
								<?php } ?>
								<div class="clearfix"></div>
			                    <div class="text_areas">
			                        <span class="sm_left fl-l">About Me</span>
			                        <textarea name="about_me" class="custom-border fl-l" rows="5" ><?=Html::decode($userData->personalInfo->about_me);?></textarea>
			                    </div>
			                    <div class="text_areas">
			                        <span class="sm_left fl-l">My hobbies</span>
			                        <textarea name="hobbies" class="custom-border fl-l" rows="5" ><?=Html::decode($userData->personalInfo->hobbies);?></textarea>
			                    </div>
			                    <div class="text_areas">
			                        <span class="sm_left fl-l">Ideal relationships</span>
			                        <textarea name="my_ideal" class="custom-border fl-l" rows="5" ><?=Html::decode($userData->personalInfo->my_ideal);?></textarea>
			                    </div>
			                    <div class="text_areas">
			                        <span class="sm_left fl-l"></span>
			                        <button  class="btn save_button uppercase text-white">save</button>
			                    </div>
		                    </form>
		                </div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
