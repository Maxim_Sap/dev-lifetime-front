<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\ChatAsset;
use app\controllers\BaseController;

$session = Yii::$app->session;

ChatAsset::register($this);
?>
<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?=Yii::$app->language?>" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="<?=Yii::$app->charset?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0"/>
        <?=Html::csrfMetaTags()?>
        <title><?=Html::encode($this->title)?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody(); ?>

    <audio class="audio_new_message hidden" controls>
        <source src="/sounds/new_message.mp3" type="audio/mpeg">
    </audio>
    <?php 
        if (isset($session['user_token']) && isset($session['user_data']['userType']) && $session['user_data']['userType'] == BaseController::USER_MALE) {
            echo $this->render('parts/header-man.php', ['pageName' => 'chat']);
        } else {
            echo $this->render('parts/header.php', ['pageName' => 'chat']);
        } 
    ?>
    <?php echo $content; ?>
    <!-- Modal MESSAGE-->
    <div class="modal fade" id="modalMessage" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span></span>
                <span></span>
            </button>
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-header-logo rounded-block">
                        <img src="/img/logo.png" alt="Logo">
                    </div>
                </div>
                <div class="modal-body">
                    <span></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-close" data-dismiss="modal" aria-label="Close">Close Modal
                    </button>
                </div>
            </div>
        </div>
    </div>
    <?php
    $session = Yii::$app->session;
    if (isset($session['message'])) {
        $mess = $session['message'];
        $js_getNewMessageCount = "$('#modalMessage').modal('show');$('#modalMessage .modal-body span').text('" . $mess . "');";
        $this->registerJs($js_getNewMessageCount, yii\web\View::POS_READY);
        unset($session['message']);
    } ?>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>