<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

use app\controllers\BaseController;
$session = Yii::$app->session;
$userType = (isset($session['user_token']) && isset($session['user_data']['userType']) && $session['user_data']['userType'] == BaseController::USER_FEMALE) ? BaseController::USER_FEMALE : BaseController::USER_MALE;

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>" xmlns="http://www.w3.org/1999/xhtml">
<head>    
    <meta charset="<?=Yii::$app->charset?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
    <?=Html::csrfMetaTags()?>
    <title><?=Html::encode($this->title)?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody();
    if (isset($session['user_token']) && isset($session['user_data']['userType']) && $userType == BaseController::USER_MALE) {
        echo '<div class="main sub-main">';
        echo $this->render('parts/header-man.php');
    } else {
        echo $this->render('parts/header.php');
    } 
    
    echo $content;
    if (isset($session['user_token']) && isset($session['user_data']['userType']) && $userType == BaseController::USER_MALE) {
        echo '</div>';        
    }
    $footer_posts = $this->params['footer_post'];
    $footer_history = $this->params['footer_history'];
    echo $this->render('parts/footer.php',['footer_posts'=>$footer_posts,'footer_history'=>$footer_history->description]);
    
	$session = Yii::$app->session;
	if(isset($session['message']) && is_string($session['message'])){
        $mess = $session['message'];	
		$js_getNewMessageCount = "$('#modalMessage').modal('show');$('#modalMessage .modal-body span').text('".$mess."');";
		$this->registerJs($js_getNewMessageCount, yii\web\View::POS_READY);
		unset($session['message']);
	}
    
    $this->endBody() ?>
    
</body>
</html>
<?php $this->endPage() ?>