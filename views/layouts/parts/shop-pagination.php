<?php
if(Yii::$app->params['numberOfGiftItemsOnFrontPage'] < $count){
	$giftsOnPage = (Yii::$app->params['numberOfGiftItemsOnFrontPage'] > $count) ? $count : Yii::$app->params['numberOfGiftItemsOnFrontPage'];

	$total = (($count - 1) / $giftsOnPage) + 1;

	$total =  intval($total);

	$pagePrev = $page - 1;
	$pageNext = $page + 1;

	$currentPage = '<li class="active"><a href="javascript:void(0);">'.$page.'</a></li>';
	if ($page > 1) {
		//$prevPage = "<a href='#' class='custom-border' onclick=\"window['adminAccountLibrary']['giftPagination'](".($page-1).");return false;\"><span> <i class='fa fa-angle-left' aria-hidden='true'></i></span></a>";
		$prevPage = '<li class="prev"><a href="#" onclick="window[\'accountLibrary\'][\'giftPagination\']('.($page-1).');return false;"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>';
	} else {
		$prevPage = '<li class="prev"><a href="javascript:void(0);"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>';
	}
	if ($page < $total) {
		//$nextPage = "<a href='#' class='custom-border' onclick=\"window['adminAccountLibrary']['giftPagination'](".($page+1).");return false;\"><span> <i class='fa fa-angle-right' aria-hidden='true'></i></span></a>";
		$nextPage = '<li class="next"><a href="#" onclick="window[\'accountLibrary\'][\'giftPagination\']('.($page+1).');return false;"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>';
	} else {
		$nextPage = '<li class="next"><a href="javascript:void(0);"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>';
	}
	if ($page < 4 && $page - 3 > 0 || ($page == $total && $page-2 > 0)) {
		//$page2left = "<a href='#' class='custom-border' onclick=\"window['adminAccountLibrary']['giftPagination'](".($page-2).");return false;\"><span>". ($page - 2) .'</span></a>';
		$page2left = '<li><a href="#" onclick="window[\'accountLibrary\'][\'giftPagination\']('.($page-2).');return false;">'.($page-2).'</a></li>';
	} else {
		$page2left = "";
	}
	if ($page - 2 >= 0) {
		//$page1left = "<a href='#' class='custom-border' onclick=\"window['adminAccountLibrary']['giftPagination'](".($page-1).");return false;\"><span>". ($page - 1) .'</span></a>';
		$page1left = '<li><a href="#" onclick="window[\'accountLibrary\'][\'giftPagination\']('.($page-1).');return false;">'.($page-1).'</a></li>';
	} else {
		$page1left = "";
	}
	if (($page < 2 && $page+2 <= $total) || $page > 3 && $page + 2 <= $total) {
		//$page2right = "<a class='custom-border' href='#' onclick=\"window['adminAccountLibrary']['giftPagination'](".($page + 2).");return false;\"><span>". ($page + 2) .'</span></a>';
		$page2right = '<li><a href="#" onclick="window[\'accountLibrary\'][\'giftPagination\']('.($page+2).');return false;">'.($page+2).'</a></li>';
	} else {
		$page2right = "";
	}
	if ($page + 1 <= $total) {
		//$page1right = "<a class='custom-border' href='#' onclick=\"window['adminAccountLibrary']['giftPagination'](".($page + 1).");return false;\"><span>". ($page + 1) .'</span></a>';
		$page1right = '<li><a href="#" onclick="window[\'accountLibrary\'][\'giftPagination\']('.($page+1).');return false;">'.($page+1).'</a></li>';
	} else {
		$page1right = "";
	}
	$navigationLine = $prevPage.$page2left.$page1left.$currentPage.$page1right.$page2right.$nextPage;
	?>

	<ul class="gifts-pagination list-inline">
		<?=$navigationLine?>
	</ul>
<?php } ?>