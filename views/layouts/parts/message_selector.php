<?php 
    $session = Yii::$app->session;
    $page_name = (!empty($pageName)) ? $pageName : $this->context->verticalMenuName;

    if ($page_name == 'deleted') {
    	$linkName = 'Restore';
    	$class = 'undo';
    	$aLink = 'restore_link';
    } else {
    	$linkName = 'Delete';
    	$class = 'trash-o';
    	$aLink = 'delete_link';
    }
?>
<ul class="control-list list-inline">
	<li>
		<div class="type-list-box">
			<span class="select-tap select-all"></span>
			<span class="select-type"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
			<ul class="message-option-list type-list">
				<li class="check-all"><a href="javascript:void(0);">Check All</a></li>
				<li class="uncheck-all"><a href="javascript:void(0);">Uncheck All</a></li>
				<li class="check-new"><a href="javascript:void(0);">New messages</a></li>
				<li class="check-answered"><a href="javascript:void(0);">Answered messages</a></li>
			</ul>
		</div>
	</li>
	<li>
		<a href="javascript:void(0);" class="delete-message <?= $aLink; ?> <?= $page_name ?>"><i class="fa fa-<?=$class?>" aria-hidden="true"></i><?= $linkName ?></a>
	</li>
	<?php if( $page_name != 'system' && $page_name != 'deleted'){?>
	<li>
		<?php if ($page_name != 'featured') { ?>
		<a href="javascript:void(0);" class="add-featured"><i class="fa fa-bookmark-o" aria-hidden="true"></i>Add to favourite</a>
		<?php } else { ?>
		<a href="javascript:void(0);" class="add-featured"><i class="fa fa-bookmark-o" aria-hidden="true"></i>Delete from favourite</a>
		<?php } ?>
	</li>
	<?php if (false) {?>
	<li>
		<a href="javascript:void(0);" class="blacklist"><i class="fa fa-times" aria-hidden="true"></i>Add to blacklist</a>
	</li>
	<?php } ?>
	<?php } ?>
</ul>
