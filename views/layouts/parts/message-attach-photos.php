<span class="attach_manual">For adding  photo to message - just click on it</span>
<div class="row">
    <?php if(!empty($photos)){
    	foreach($photos as $photo){;?>
			<div class="col-md-3" style="height: 325px;">
		        <img data-original="<?=$this->context->serverUrl."/".$photo->original_image;?>" src="<?=$this->context->serverUrl."/".$photo->medium_thumb;?>">
		        <div class="remove_foto">
		            <a class='delete_link' href="javascript:void(0);" onclick="window['messageLibrary']['removePhotoFromLetter'](<?=$photo->id;?>)">
		                <i class="fa fa-trash-o text-red" aria-hidden="true"></i>
		                Remove foto
		            </a>
		        </div>
		    </div>
    	<?php }
		}?>
</div>