<?php
    $page_name = $this->context->verticalMenuName;
    $session = Yii::$app->session;
?>
<ul class="custom-border">
    <li>
        <a href="/account/ladies/favorite" <?php if($page_name == "favorite"){echo 'class="active"';}?>>My favorites</a>
    </li>
    <li>
        <a href="/account/ladies/winks" <?php if($page_name == "winks"){echo 'class="active"';}?>>My winks</a>
    </li>
    <li>
        <a href="/account/ladies/admires" <?php if($page_name == "admires"){echo 'class="active"';}?>>My admires</a>
    </li>
    <li>
        <a href="/account/ladies/matches" <?php if($page_name == "matches"){echo 'class="active"';}?>>My matches</a>
    </li>
    <li>
        <a href="/account/ladies/guests" <?php if($page_name == "guests"){echo 'class="active"';}?>>My guests</a>
    </li>
    <li>
        <a href="/account/ladies/blacklist" <?php if($page_name == "blacklist"){echo 'class="active"';}?>>My blacklist</a>
    </li>
    <li>
        <a href="/account/ladies/users-news" <?php if($page_name == "users_news"){echo 'class="active"';}?>>News from users</a>
    </li>
</ul>