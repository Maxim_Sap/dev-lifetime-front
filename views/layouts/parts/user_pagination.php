<?php
    if ($pageController == 'search') {
        $numberOfItems = Yii::$app->params['numberOfItemsOnSearchPage'];       
    } else {
        $numberOfItems = Yii::$app->params['numberOfItemsOnFrontPage'];       
    }
	$itemsOnPage = ($numberOfItems > $count) ? $count : $numberOfItems;
	$total = (($count - 1) / $itemsOnPage) + 1;
	$total =  intval($total);
	$pagePrev = $page - 1;
	$pageNext = $page + 1;
	if ($pageName == 'generate-girls-list') {
		$functionName = 'commonLibrary[\'searchPaginationResult\']';
	} else {
		$functionName = 'commonLibrary[\'userPagination\']';
	}
?>
<div class="page_pagination fl-r">
    <div class="page_nav fl-r">
        <span class="fl-r <?php if($pageNext <= 0 || $pageNext > $total) { echo "disabled"; }?>" <?php 
        	if ($pageNext > 0 && $pageNext <= $total) { echo "onclick=\"$functionName($pageNext,'$pageName','$pageController');\"";}?>>
        	<i class="fa fa-angle-right custom-border" aria-hidden="true"></i>
        </span>
        <span class="fl-r <?php if($pagePrev <= 0 || $pagePrev >$total) { echo "disabled";} ?>" <?php 
        	if ($pagePrev > 0 && $pagePrev <= $total) { echo "onclick=\"$functionName($pagePrev,'$pageName','$pageController');\"";}?>>
        	<i class="fa fa-angle-left custom-border" aria-hidden="true"></i>
        </span>
        <span id="page" data-page="<?= $page; ?>" data-page-name="<?= $pageName;?>" class="pages_counter fl-r">
        	(<?= $count ?> <?= !empty($items) ? $items : 'items' ?>) <?= " Pages ". $page ." of ". $total ?></span>                 
    </div>
    <div class="clearfix"></div>
</div> 
<div class="clearfix"></div>