<?php
use yii\helpers\HtmlPurifier;
$session = Yii::$app->session;
//var_dump($message); die;
$createdAt = strtotime($message->created_at);
$formatDateCreate = date("H:i d M, Y", $createdAt);
if($message->reply_status == 1){
	$message_type = "answer-msg";
	$message_type_text = "anwsered message";
	$messageClass = "read answered";	
}elseif($message->readed_at == null) {
	$message_type = "new-msg";
	$message_type_text = "new message";
	$messageClass = "unread";
}else{
	$message_type = "read-msg";
	$message_type_text = "read message";
	$messageClass = "read";		
}
$page_name = $this->context->verticalMenuName;

if($message->another_user_avatar != null){
	$userAvatar = $this->context->serverUrl."/".$message->another_user_avatar;
}else{
	$userAvatar = $this->context->serverUrl."/img/no_avatar_normal.jpg";
}
$message_block_type = ($message->message_creator == $session['user_id']) ? 'outbox' : 'inbox';

?>
<div class="message-item message <?= $messageClass ?> <?= $last ?>">
	<ul class="message-item-list list-inline"  data-id="<?= $message->id ?>">
		<li class="check">
			<span id="check_mail_<?=$message->id;?>" data-other-user-id="<?=$message->message_receiver;?>" class="select-tap"></span>
		</li>
		<li class="avatar">
			<div class="person-avatar" style="background-image: url('<?= $userAvatar ?>')">
				<span class="status <?= $online ?>"><?= $online ?></span>
			</div>
		</li>
		<li class="text">
			<div class="message-item-description">
				<a href="/message/system/<?= $message->id ?>" class="person-name"><?=$message->another_user_name;?></a> 				
				<div class="date-wrapper">
					<span class="date"><?=date('Y-m-d', $createdAt) ?></span>
				</div>
				<a href="/message/system/<?= $message->id ?>" class="text-link">
					<div class="message-text">
						<?php
							$description = HtmlPurifier::process(strip_tags($message->description));
							$description = mb_substr($description, 0, 200);
							/*$description = rtrim($description, "!,.-");
							$description = substr($description, 0, strrpos($description, ' '));*/
							echo $description;?>
						<?php if ($messageClass == "unread") {?>	
						<span class="item-label">unread</span>
						<?php } ?>
					</div>
				</a>
			</div>
		</li>
	</ul>
</div>