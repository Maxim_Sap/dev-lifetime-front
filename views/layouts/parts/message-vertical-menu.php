<?php
    use yii\helpers\Url;
    $page_name = $this->context->verticalMenuName;
    $session = Yii::$app->session;

?>
<div class="sidebar-block profile-main-action-block">
    <ul class="profile-menu-list messages-list">        
        <li <?php if (Yii::$app->controller->action->id == 'inbox') {echo 'class="active"';} ?>><a href="<?= Url::toRoute('message/inbox'); ?>">Inbox message</a></li>
        <li <?php if (Yii::$app->controller->action->id == 'outbox') {echo 'class="active"';} ?>><a href="<?= Url::toRoute('message/outbox'); ?>">Outbox message</a></li>
        <li <?php if (Yii::$app->controller->action->id == 'new') {echo 'class="active"';} ?>><a href="<?= Url::toRoute('message/new'); ?>">New message</a></li>
        <li <?php if (Yii::$app->controller->action->id == 'featured') {echo 'class="active"';} ?>><a href="<?= Url::toRoute('message/featured'); ?>">Favorite message</a></li>
        <li <?php if (Yii::$app->controller->action->id == 'deleted') {echo 'class="active"';} ?>><a href="<?= Url::toRoute('message/deleted'); ?>">Deleted message</a></li>                
        <li <?php if (Yii::$app->controller->action->id == 'system') {echo 'class="active"';} ?>><a href="<?= Url::toRoute('message/system'); ?>">System message</a></li>            
    </ul>
</div>