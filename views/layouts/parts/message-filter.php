<?php
	$session = Yii::$app->session;
	
	$otherUserID = (isset($session['letters_search_params']['otherUserID']) && is_numeric($session['letters_search_params']['otherUserID'])) ? $session['letters_search_params']['otherUserID'] : "";
	$created_at = (isset($session['letters_search_params']['created_at']) && strtotime($session['letters_search_params']['created_at'])) ? $session['letters_search_params']['created_at'] : "";
	$is_favourite = (isset($session['letters_search_params']['is_favourite']) && is_numeric($session['letters_search_params']['is_favourite'])) ? $session['letters_search_params']['is_favourite'] : "";
	$answered = (isset($session['letters_search_params']['answered']) && is_numeric($session['letters_search_params']['answered'])) ? $session['letters_search_params']['answered'] : "";
	?>

<input type="hidden" name="otherUserID" class="custom-border custom-input fl-l" value="<?=$otherUserID?>" />
<ul class="filters-list-wrapper list-inline">	
	<li class="date-b">		
		<input name="created_at" data-provide="datepicker" data-date-format="dd-mm-yyyy" class="datepicker drop-button date-button" placeholder="Date from" value="<?=$created_at?>">
	</li>
	<li>
		<select name="is_favourite" class="selectpicker favourites drop-button filter-list-button" data-title="Favourites">
			<option hidden value="">Favorite</option>
	        <option value="">All</option>
	        <option value="1" <?=($is_favourite !="" && $is_favourite == 1) ? 'selected':"";?>>Yes</option>         
	        <option value="0" <?=($is_favourite !="" && $is_favourite == 0) ? 'selected':"";?>>No</option>
	    </select>
	</li>
	<li>		
	    <select name="answered" class="selectpicker answered drop-button filter-list-button" data-title="All">
	    	<option hidden value="">Status</option>
	        <option value="">All</option>
	        <option value="1" <?=($answered !="" && $answered == 1) ? 'selected':"";?>>Answered</option>
	        <option value="2" <?=($answered !="" && $answered == 2) ? 'selected':"";?>>Read</option>
	        <option value="3" <?=($answered !="" && $answered == 3) ? 'selected':"";?>>New</option>
	    </select>
	</li>
	<li>
		<input type="submit" class="btn show-results" onclick="filterClick()" value="Start Search" /> 		
	</li>
</ul>

<script>
	function filterClick() {
		var pageName = null;
		if ($('.tab.active').hasClass('inbox')) {
			var	pageName = 'inbox';
		} else if ($('.tab.active').hasClass('outbox')) {
			var	pageName = 'outbox';
		} else if ($('.tab.active').hasClass('trash')) {
			var pageName = 'deleted';
		} else if ($('.tab.active').hasClass('notifications')) {
			var pageName = 'system';
		}
		if (pageName != null) {
			window['messageLibrary']['messagePagination'](1, pageName);
		} else {
			$('#modalMessage').modal('show');
			$('#modalMessage .modal-body span').text('General Error happend during handling your request.');
		}
		
	}
</script>

<?php

	$js = "$('input.datepicker').change(); $('.for-clear-button input[type=\"text\"]').keyup();";
	$this->registerJs(
	    $js,
	    \yii\web\View::POS_READY	    
	);

?>

