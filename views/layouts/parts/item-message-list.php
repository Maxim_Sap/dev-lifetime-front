<?php
use yii\helpers\HtmlPurifier;
$createdAt = strtotime($letter->created_at);

//var_dump($letter);
$page_name = $this->context->verticalMenuName;
if($letter->reply_status == 1){
	$messageType = "answer-msg";
	$messageTypeText = "answered message";
	$messageClass = "read answered";
	
	if($page_name == 'outbox'){
		$messageType = "read-msg";
		$messageTypeText = "read message";
	}
	
}elseif($letter->readed_at == null) {
	$messageType = "new-msg";
	$messageTypeText = "new message";
	$messageClass = "unread";
}else{
	$messageType = "read-msg";
	$messageTypeText = "read message";
	$messageClass = "read";	
}
//var_dump($letter);
if(isset($letter->user_in_favorite) && $letter->user_in_favorite == 1){
	$show_status = 'hidden';
	$show_status2 = '';
}else{
	$show_status = '';
	$show_status2 = 'hidden';
}
$userAvatar = (!empty($letter->another_user_avatar)) ? $this->context->serverUrl."/".$letter->another_user_avatar : '/img/no_avatar_normal.jpg';

$userPrefix = ($letter->user_type == 1) ? 'men' : 'girls';

$online = ($letter->last_activity > time() - 30) ? 'online' : 'offline';

if (!empty($letter->birthday)) {
	$birthday = new \DateTime($letter->birthday);
	$tz  = new \DateTimeZone('Europe/Brussels');
	$userAge = \DateTime::createFromFormat('Y-m-d', $letter->birthday, $tz)
	     ->diff(new \DateTime('now', $tz))
	     ->y;
} else {
	$userAge = null;
}


?>
<div class="message-item message <?= $messageClass ?>">
	<ul class="message-item-list list-inline" data-id="<?= $letter->id ?>">
		<li class="check">
			<span id="check_mail_<?=$letter->id;?>" data-other-user-id="<?=$letter->another_user_id;?>" class="select-tap"></span>
		</li>
		<li class="avatar">
			<div class="person-avatar" style="background-image: url('<?= $userAvatar ?>')">
				<span class="status <?= $online ?>"><?= $online ?></span>
			</div>
		</li>
		<li class="text">
			<div class="message-item-description">
				<a href="/message/<?= $letter->id ?>" class="person-name"><?=$letter->another_user_name;?></a> 
				<?php if ($userAge) {?><span class="person-age"><?= $userAge ?> years</span><?php } ?>
				<div class="date-wrapper">
					<span class="date"><?=date('Y-m-d', $createdAt) ?></span>
				</div>
				<a href="/message/<?= $letter->id ?>" class="text-link">
					<div class="message-text">
						<?=$letter->theme;?>
						<?php
							$description = HtmlPurifier::process(strip_tags($letter->description));
							$description = mb_substr($description, 0, 200);
							/*$description = rtrim($description, "!,.-");
							$description = substr($description, 0, strrpos($description, ' '));*/
							echo $description;?>
						<?php if ($messageClass == "unread") {?>	
						<span class="item-label">unread</span>
						<?php } ?>
					</div>
				</a>
			</div>
		</li>
	</ul>
</div>

<style>
	.text-link {
		text-decoration: none;
	}
</style>

<?php

$this->registerJs(
    "$('.message ul.message-body').on('click', function() {
    	if ($(this).data('id') != null && $(this).data('id') != '') {
			location.href = '/message/' + $(this).data('id');
    	}    	
    }); "
);