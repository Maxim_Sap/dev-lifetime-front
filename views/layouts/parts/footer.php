<?php
    use app\controllers\BaseController;
    use yii\helpers\Html;
    $session = Yii::$app->session;
    $userType = (isset($session['user_data']['userType']) &&  $session['user_data']['userType'] == BaseController::USER_FEMALE) ? BaseController::USER_FEMALE : BaseController::USER_MALE;
    $suffix = ($userType == BaseController::USER_MALE) ? 'girls' : 'men';
?>
<footer class="main-footer">
    <div class="footer-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 footer-column footer-column-1">
                    <div class="footer-content-item">
                        <h5 class="footer-content-item-title">Company history</h5>
                        <p class="history-text"><?=Html::decode($footer_history) ?></p>
                    </div>
                    <div class="footer-content-item">
                        <h5 class="footer-content-item-title">Sign up for our newsletter</h5>
                        <form action="/subscribe" class="news-form" method="post">
                            <input type="email" name="email" placeholder="Never miss a deal again" required>
                            <button type="submit">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            </button>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 footer-column footer-column-2">
                    <div class="footer-content-item footer-socials-wrapper">
                        <h5 class="footer-content-item-title">Social media:</h5>
                        <ul class="socials list-inline">
                            <li><a href="javascript:void(0);"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                        <a href="/search" class="btn">Find Single <?= $suffix ?></a>
                        <div class="card-block">
	                        <h5 class="footer-content-item-title">We accept:</h5> 
	                        <ul class="socials list-inline">
	                        	<li class="credit-card"><i class="fa fa-cc-visa" aria-hidden="true"></i> </li>
	                        	<li class="credit-card"><i class="fa fa-cc-mastercard" aria-hidden="true"></i></li>
	                    	</ul>
                        </div>                        
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 footer-column footer-column-3">
                    <div class="footer-content-item">
                        <h5 class="footer-content-item-title">Recents posts:</h5>
                        <?php if(!empty($footer_posts)) {
                            foreach ($footer_posts as $one) {?>
                                <div class="footer-post">
                                    <h6 class="footer-post-date"><span class="date"><?=date("M d Y",strtotime($one->created_at))?></span> | <span class="post-autor">By Admin</span></h6>
                                    <a href="/blog/<?=$one->id?>" class="footer-post-title"><?=$one->title?></a>
                                    <p class="footer-post-description"><?=mb_substr($one->description,0,100,'utf8')?>...</p>
                                </div>
                            <?php }
                        } ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 footer-column footer-column-4">
                    <div class="footer-content-item">
                        <h5 class="footer-content-item-title">Navigation</h5>
                        <ul class="footer-menu arrows-list">
                            <?php if ($userType == BaseController::USER_MALE) {?>
                            <li class="with-online"><a href="/girls/online">Girls Online<span class="online">0</span></a></li>
                            <li><a href="/girls">All Girls</a></li>
                            <li><a href="/girls/new">New Girls</a></li>
                            <?php } else {?>
                            <li class="with-online"><a href="/men/online">Men Online<span class="online">0</span></a></li>
                            <li><a href="/men">All Men</a></li>
                            <li><a href="/men/new">New Men</a></li>            
                            <?php } ?>
                            <li><a href="/about">About us</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#modalContact">Contact</a></li>                            
                        </ul>
                        <p>Customer Support: <a href="mailto:support@test.com">support@test.com</a></p>
                    </div>
                </div>
        </div>
    </div>
</div>
    <div class="footer-bottom-line">
        <p class="copy">Copyright &copy; LIFETIME.DATING <?= date('Y'); ?>. All rights reserved.</p>
        <p class="copy"><a href="/privacy-policy">Privacy policy</a></p>
    </div>
    <?= \bizley\cookiemonster\CookieMonster::widget([
            'box' => [
                'view' => '@app/views/layouts/cookie',
                'classButton' => 'cookie-button',
                'classInner' => 'cookie-inner',
                'classOuter' => 'cookie-block',
                'addOuterStyle' => [
                    'display' => 'none'
                ]
            ],
            'content' => [
                //'buttonMessage' => 'OK', // instead of default 'I understand'
            ],
            'mode' => 'custom'
        ]);
    ?>
</footer>


