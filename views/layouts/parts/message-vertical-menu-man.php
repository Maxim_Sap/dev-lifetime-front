<?php
    use app\controllers\BaseController;
    use yii\helpers\Url;
    $pageName = $this->context->verticalMenuName;
    $session = Yii::$app->session;    
    $userType = (isset($session['user_data']['userType']) &&  $session['user_data']['userType'] == BaseController::USER_FEMALE) ? BaseController::USER_FEMALE : BaseController::USER_MALE;
    if ($userType == BaseController::USER_FEMALE) {
        $baseUrl = 'men';
        $text = 'Men';
        $baseUrl2 = 'girls';
        $text2 = 'Ladies';
    } else {
        $baseUrl = 'girls';
        $text = 'Ladies';
        $baseUrl2 = 'men';
        $text2 = 'Men';
    }   
?>
<a href="<?= Yii::$app->homeUrl; ?>" class="home-link"><img src="/img/home-icon.png" alt=""> Home</a>
<div class="side-menu-block members-area">
    <a href="javascript:void(0);" class="side-menu-button">Members Area</a>
    <ul class="side-menu">
        <li class="messages-link"><a href="<?= Url::toRoute('message/index'); ?>">My Messages</a></li>
        <li><a href="/account/edit/<?= $session['user_data']['userID'] ?>">My Account Options</a></li>
        <li><a href="/account/favorite">My Favorites</a></li>
        <li><a href="/account/gallery">My Gallery</a></li>
        <li><a href="/account/video">My Video Gallery</a></li>
        <li><a href="/account/my-gifts">My Gifts</a></li>
        <li><a id="logout_btn" href="javascript:void(0);" onclick="window['commonLibrary']['logout']()">Logout</a></li>
    </ul>
</div>

<div class="side-menu-block profiles">
    <a href="javascript:void(0);" class="side-menu-button">Profiles</a>
    <ul class="side-menu">
        <li><a href="/search">Search</a></li>
        <li><a href="/<?= $baseUrl ?>/online">Online Now <span class="online"></span></a></li>
        <li><a href="/<?= $baseUrl ?>/new">New Profiles</a></li>
        <li><a href="/account/chat">Video Chat</a></li>
        <li class="chat-link"><a href="/account/chat">Text Chat</a></li>
        <!-- <li><a href="javascript:void(0);">Intro Videos</a></li> -->
    </ul>
</div>

<!-- <div class="side-menu-block services">
    <a href="javascript:void(0);" class="side-menu-button">Services</a>
    <ul class="side-menu">
        <li><a href="javascript:void(0);">Live Video Chat</a></li>
        <li><a href="javascript:void(0);">Serious Dater Program</a></li>
        <li><a href="javascript:void(0);">Member Rewards</a></li>
        <li><a href="javascript:void(0);">Apartment Rental</a></li>
        <li><a href="javascript:void(0);">Singles Tours</a></li>
    </ul>
</div> -->

<div class="side-menu-block info">
    <a href="javascript:void(0);" class="side-menu-button">Information</a>
    <ul class="side-menu">
        <li><a href="/blog">Love Stories</a></li>
        <li><a href="/contact">Contact Us</a></li>
        <li><a href="/about">About Us</a></li>
    </ul>
</div>