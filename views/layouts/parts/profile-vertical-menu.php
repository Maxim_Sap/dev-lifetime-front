<?php
    use app\controllers\BaseController;
    use yii\helpers\Url;
    $pageName = $this->context->verticalMenuName;
    $session = Yii::$app->session;    
?>
<div class="sidebar-block profile-main-action-block">
    <ul class="profile-menu-list main-action-menu">
        <li><a href="/account/chat" class="live-chat-link"><span>Live Chat</span> </a></li>
        <li class="has-action-submenu">
            <a href="javascript:void(0);" class="messages-link"><span>Messages</span> </a>
            <ul class="main-action-submenu">
                <li <?php if (Yii::$app->controller->action->id == 'inbox') {echo 'class="active"';} ?>><a href="<?= Url::toRoute('message/inbox'); ?>"><span class="inbox">Inbox message</span></a></li>
                <li <?php if (Yii::$app->controller->action->id == 'outbox') {echo 'class="active"';} ?>><a href="<?= Url::toRoute('message/outbox'); ?>">Outbox message</a></li>
                <li <?php if (Yii::$app->controller->action->id == 'new') {echo 'class="active"';} ?>><a href="<?= Url::toRoute('message/new'); ?>">New message</a></li>
                <li <?php if (Yii::$app->controller->action->id == 'featured') {echo 'class="active"';} ?>><a href="<?= Url::toRoute('message/featured'); ?>">Favorite message</a></li>
                <li <?php if (Yii::$app->controller->action->id == 'deleted') {echo 'class="active"';} ?>><a href="<?= Url::toRoute('message/deleted'); ?>">Deleted message</a></li>                
                <li <?php if (Yii::$app->controller->action->id == 'system') {echo 'class="active"';} ?>><a href="<?= Url::toRoute('message/system'); ?>"><span class="system">System message</span></a></li>
            </ul>
        </li>
        <li><a href="/account/my-gifts" class="gifts-link">Gifts</a></li>
    </ul>
</div>

<div class="sidebar-block profile-categories-block">
    <ul class="profile-menu-list profile-categories-list">
        <li <?php if($pageName == "favorite"){echo 'class="active"';}?>><a href="/account/favorite">My favorites</a></li>
        <li <?php if($pageName == "guests"){echo 'class="active"';}?>><a href="/account/guests">My Guests</a></li>
        <li <?php if($pageName == "gallery"){echo 'class="active"';}?>><a href="/account/gallery">My gallery</a></li>
        <li <?php if($pageName == "video"){echo 'class="active"';}?>><a href="/account/video">My video</a></li>
        <li <?php if($pageName == "winks"){echo 'class="active"';}?>><a href="/account/winks">Winks</a></li>
        <li <?php if($pageName == "blacklist"){echo 'class="active"';}?>><a href="/account/blacklist">Blacklist</a></li>
    </ul>
</div>
