<?php
    $serverUrl = $this->context->serverUrl;
    $blog_category = ['story','post'];
?>
<div class="main sub-main">
    <div class="global-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <aside class="side-bar side-bar-left">
                        <form action="" class="search-categories-form">
                            <input type="search" name="Search" id="b-search" placeholder="Search post">
                            <button type="submit">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </form>
                        <div class="sidebar-block profile-main-action-block">
                            <h4 class="profile-menu-list-title">Categories</h4>
                            <ul class="profile-menu-list blog-categories-list">
                                <?php foreach($blog_category as $one){?>
                                    <li class="<?=($post->category == $one) ? 'active' : '';?>"><a href="/blog?category=<?=$one?>" class="gifts-link"><?=$one?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <!--<div class="sidebar-block profile-main-action-block">
                            <h4 class="profile-menu-list-title">Archives</h4>
                            <ul class="profile-menu-list blog-categories-list">
                                <li><a href="javascript:void(0);" class="gifts-link">Category 1</a></li>
                                <li><a href="javascript:void(0);" class="gifts-link">Category 2</a></li>
                                <li><a href="javascript:void(0);" class="gifts-link">Category 3</a></li>
                                <li><a href="javascript:void(0);" class="gifts-link">Category 4</a></li>
                                <li><a href="javascript:void(0);" class="gifts-link">Category 5</a></li>
                            </ul>
                        </div>-->
                    </aside>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="blog-posts-container">
                        <?php if (!empty($post)) { ?>
                            <div class="single-blog-post">
                                <h2 class="single-blog-post-name"><?=$post->title?></h2>
                                <div class="single-blog-post-image" style="background-image: url('<?=$serverUrl . "/" . $post->image?>')"></div>
                                <div class="single-blog-post-description">
                                    <p class="single-blog-post-autor-date">
                                        <span class="single-blog-post-date"><?=$post->created_at?></span> | <span class="blog-post-autor">Site admin</span>
                                    </p>
                                    <?=$post->description?>
                                </div>
                            </div>
                        <?php } else { ?>
                            <p>Post not found</p>
                        <?php } ?>
                        <a href="<?=(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "/blog")?>" class="btn back">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>