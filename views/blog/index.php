<?php

    $serverUrl = $this->context->serverUrl;

    $ajax    = Yii::$app->request->getIsAjax();

    $blog_category = ['story','post'];

    $category = (isset($_GET['category']) && in_array($_GET['category'],$blog_category)? $_GET['category'] : '');

    $limit = Yii::$app->params['numberOfBlogItems'];

?>
<?php if (!$ajax) { ?>
<div class="main sub-main">

    <div class="global-content-wrapper">

        <div class="container">

            <div class="row">

                <div class="col-xs-12 col-sm-3">

                    <aside class="side-bar side-bar-left">

                        <form action="" class="search-categories-form">

                            <input type="search" name="search" id="b-search" placeholder="Search post">

                            <button type="submit">

                                <i class="fa fa-search" aria-hidden="true"></i>

                            </button>

                        </form>



                        <div class="sidebar-block profile-main-action-block">

                            <h4 class="profile-menu-list-title">Categories</h4>

                            <ul class="profile-menu-list blog-categories-list">

                                <?php foreach($blog_category as $one){?>

                                    <li class="<?=($category == $one) ? 'active' : '';?>"><a href="/blog?category=<?=$one?>" class="gifts-link"><?=$one?></a></li>

                                <?php } ?>

                            </ul>

                        </div>



                        <!--<div class="sidebar-block profile-main-action-block">

                            <h4 class="profile-menu-list-title">Archives</h4>

                            <ul class="profile-menu-list blog-categories-list">

                                <li><a href="javascript:void(0);" class="gifts-link">Category 1</a></li>

                                <li><a href="javascript:void(0);" class="gifts-link">Category 2</a></li>

                                <li><a href="javascript:void(0);" class="gifts-link">Category 3</a></li>

                                <li><a href="javascript:void(0);" class="gifts-link">Category 4</a></li>

                                <li><a href="javascript:void(0);" class="gifts-link">Category 5</a></li>

                            </ul>

                        </div>-->

                    </aside>

                </div>

                <div class="col-xs-12 col-sm-9"> 
                

                    <div class="blog-posts-container">
                    <?php } ?>

                        <?php if (!empty($postList)) { ?>

                            <?php foreach ($postList as $post) { ?>

                                <div class="blog-post">

                                    <a href="/blog/<?=$post->id?>" class="blog-post-link"></a>

                                    <div class="blog-post-image-wrapper">

                                        <div class="blog-post-image" style="background-image: url('<?=$serverUrl . "/" . $post->image?>')"></div>

                                    </div>

                                    <div class="blog-post-description">

                                        <h3 class="blog-post-name"><?=$post->title?></h3>

                                        <p class="blog-post-autor-date">

                                            <span class="blog-post-date"><?=$post->created_at?></span> | <span class="blog-post-autor">Site admin</span>

                                        </p>

                                        <p class="blog-post-small-description"><?=$post->description?></p>

                                    </div>

                                </div>

                            <?php }?>
                            
                            <?php if ($count > $limit) {echo $this->render('/layouts/parts/blog_pagination.php', ['count' => $count, 'currentpage'=>$page, 'limit' => $limit, 'pageName' => 'blog']);} ?>

                        <?php } else { ?>
                            <p>No results</p>
                        <?php } ?>
                <?php if (!$ajax) {?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>