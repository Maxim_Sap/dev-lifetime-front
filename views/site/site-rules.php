<?php 
    $this->title = $pageContent->meta_title;    
    use yii\helpers\Html;
?>
<div class="main sub-main">
    
    <div class="global-content-wrapper">
        <div class="container">
            <div class="row">
                <?php echo Html::decode($pageContent->description);?>
            </div>
        </div>
    </div>

</div>