<?php

use yii\helpers\Html;

$this->title                   = 'Login';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-login" xmlns="http://www.w3.org/1999/html">
    <h1><?=Html::encode($this->title)?></h1>
    <select id="user_type">
        <option value="1">Мужик</option>
        <option value="3">Агенство</option>
        <option value="5">Аффилейт</option>
    </select>
    <br/>
    <button id="fb_login" onclick="FB_login()">FB login</button>
    <button id="fb_logout" hidden onclick="Fb_Logout()">FB logout</button>
    <button><a href="<?=$google_href?>">Google login</a></button>
    <button onclick="google_logout();">Google logout</button>
    <div id="status"></div>
</div>