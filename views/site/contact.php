<div class="main sub-main">
	
	<div class="global-content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-sm-push-6">
					<div class="contact-block right">
						<h2 class="contact-block-title">Drop us a line</h2>
						<form action="/site/feedback" method="post" class="contact-form clearfix">
							<label for="name">First Name</label>
							<input type="text" name="name" id="name" placeholder="Name" value=<?= $name; ?>>
							<label for="email">E-mail</label>
							<input type="email" name="email" id="email" placeholder="Email" value=<?= $email; ?>>
							<label>Subject</label>
							<select name="options" id="options" data-type="options">
								<option>Choose one</option>
								<option value="1" <?= ((isset($options) && $options == 1) ? "selected='selected'" : '') ?>>Browser can't connect to chat server</option>
								<option value="2" <?= ((isset($options) && $options == 2) ? "selected='selected'" : '') ?>>Problems with chat/videochat</option>
								<option value="3" <?= ((isset($options) && $options == 3) ? "selected='selected'" : '') ?>>Can't login to site</option>
								<option value="4" <?= ((isset($options) && $options == 4) ? "selected='selected'" : '') ?>>Bug report</option>
								<option value="5" <?= ((isset($options) && $options == 5) ? "selected='selected'" : '') ?>>Other problem</option>
							</select>
							<label for="message">Your Message</label>
							<textarea name="message" id="message" placeholder="Type here" data-type="message"><?= $message ?></textarea>
							<?php if(isset($session['user_id'])){?>
								<input type="hidden" name="fromUserID" value="<?=$session['user_id']?>">
							<?php } ?>
							<button type="reset" class="btn btn-reset">Reset</button>
							<button type="submit" class="btn btn-submit">Send</button>
						</form>
					</div>
				</div>

				<div class="col-xs-12 col-sm-6 col-sm-pull-6">
					<div class="contact-block left">
						<h2 class="contact-block-title">main office</h2>
						<div class="contact-block-item">
							<p class="contact-block-item-title">Address:</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, quaerat, odit cumque quia nemo fuga soluta sunt dolor numquam voluptas? Eos animi consequuntur in fugit hic aperiam commodi quo architecto illo vitae. Temporibus, praesentium, at omnis ducimus maxime aperiam deserunt ipsa consequuntur voluptas eius laborum delectus quis laboriosam provident accusamus.</p>
						</div>
						<div class="contact-block-item">
							<p class="contact-block-item-title">Email:</p>
							<a href="mailto:support@ukrainianbrides.com">support@ukrainianbrides.com</a>
						</div>
						<div class="contact-block-item">
							<p class="contact-block-item-title">Phone:</p>
							<span>(+1) 07123456789</span>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>

</div>