<?php
$session = Yii::$app->session;
?>
<div class="main sub-main">

    <div class="global-content-wrapper">

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <h2 class="payment-title">Payments page</h2>
                </div>

                <div class="col-xs-12 col-sm-6 col-sm-push-6">

                    <form action="https://secure.multicards.com/cgi-bin/order2/processorder1.pl" method="post" class="account-form">

                        <label for="price">I want to fund my account</label>

                        <input name="mer_id" value="<?=$merchant_id;?>" type="hidden">

                        <input name="mer_url_idx" value="<?=$merchant_url_idx;?>" type="hidden">

                        <input name="user_id" value="<?=$session['user_id'];?>" type="hidden">
                        <input name="user_token" value="<?=$session['user_token'];?>" type="hidden">
                        <input name="random_key" value="<?=$payment_key;?>" type="hidden">

                        <input name="item1_desc" value="fund user account <?= (!empty($session['user_data']['name'])) ? $session['user_data']['name'] : 'Noname' ?> [id: <?=$session['user_id'];?>]" type="hidden"></br>

                        <input id="price" name="item1_price" value="50.00" type="number" style="width: 100px">

                        <span style="margin-left: 10px">USD</span>

                        <input name="item1_qty" type="hidden" size="3" maxlength="2" value="1"></br>

                        <input class="btn" name="submit" type="Submit" value="Proceed to Secure Payment Server">

                    </form>

                </div>

                <div class="col-xs-12 col-sm-6 col-sm-pull-6">

                    <div class="contact-block left">

                        <div class="contact-block-item">

                            <p class="contact-block-item-title">Instructions:</p>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, quaerat, odit cumque quia nemo fuga soluta sunt dolor numquam voluptas? Eos animi consequuntur in fugit hic aperiam commodi quo architecto illo vitae. Temporibus, praesentium, at omnis ducimus maxime aperiam deserunt ipsa consequuntur voluptas eius laborum delectus quis laboriosam provident accusamus.</p>

                            <a href="/license-agreement" class="la-link">License agreement</a>

                            <a href="/privacy-policy" class="pp-link">Privacy policy</a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>