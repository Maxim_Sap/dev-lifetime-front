<?php
    use yii\helpers\Url;
$online = ((!empty($userData->last_activity)) && (time() - $userData->last_activity <= 60)) ? 1 : 0;

if (!empty($userData->personalInfo->birthday)) {
    $birthday = new \DateTime($userData->personalInfo->birthday);
    $tz  = new \DateTimeZone('Europe/Brussels');
    $age = \DateTime::createFromFormat('Y-m-d', $userData->personalInfo->birthday, $tz)
         ->diff(new \DateTime('now', $tz))
         ->y;
	// zodiak
	$signs = [ 
                "Capricorn", 
                "Aquarius", 
                "Pisces", 
                "Aries", 
                "Taurus", 
                "Gemini", 
                "Cancer", 
                "Leo", 
                "Virgo", 
                "Libra", 
                "Scorpio", 
                "Sagittarius"
            ];
    $signsstart = array(1=>21, 2=>20, 3=>20, 4=>20, 5=>20, 6=>20, 7=>21, 8=>22, 9=>23, 10=>23, 11=>23, 12=>23);
    $zodiak = (int)$birthday->format('d') < $signsstart[(int)$birthday->format('m')] ? $signs[(int)$birthday->format('m') - 1] : $signs[(int)$birthday->format('m') % 12];
} else {
	$age = "Not defined";
	$zodiak = "Not defined";
}
if (!empty($userData->personalInfo->weight)) {
	$weight = $userData->personalInfo->weight." kg (" . round($userData->personalInfo->weight/0.45359237)." lbs)";
} else {
	$weight = "Not defined";
}

if (!empty($userData->personalInfo->height)) {
    $heightInMeters = sprintf("%.2f", $userData->personalInfo->height/100);
    $heightInFeet = $userData->personalInfo->height*0.03280839895013123;
    $heightFeet = (int)$heightInFeet; // truncate the float to an integer
    $heightInches = round(($heightInFeet-$heightFeet)*12); 
	
	if ($heightInches == 0) {
		$height = "$heightInMeters m ($heightFeet')";
	} else {
		$height = "$heightInMeters m ($heightFeet' $heightInches'')";
	}
} else {
	$height = "Not defined";
}

if (!empty($userData->location)) {
	$city = $userData->city;
	$country = $userData->location;
} else {
	$user_city = "Not defined";
	$user_country = "Not defined";
}


$maritalArray = ['Single','Married','Divorced','Widowed','Complicated'];
if ($userData->personalInfo->marital != null && isset($maritalArray[$userData->personalInfo->marital-1])) {
    $marital = $maritalArray[$userData->personalInfo->marital-1];
} else {
    $marital = 'Not defined';
}

if ($userData->personalInfo->eyes != null) {
    $eyes = $userData->personalInfo->eyes;
} else {
    $eyes = 'Not defined';
}

if ($userData->personalInfo->hair != null) {
    $hair = $userData->personalInfo->hair;
} else {
    $hair = 'Not defined';
}

if (isset($userData->personalInfo->children)) {    
    $children = $userData->personalInfo->children;
    if ($children == 0) {
        $children = "no";
    }
} else {
    $children = 'Not defined';
}

$smokingArray = ['Yes','No','Casual','Heavy'];
if ($userData->personalInfo->smoking != null && isset($smokingArray[$userData->personalInfo->smoking-1])) {
    $smoking = $smokingArray[$userData->personalInfo->smoking-1];
} else {
    $smoking = 'Not defined';
}

$englishLevelArray = ['Beginner','Intermediate','Advanced','Fluent'];
if ($userData->personalInfo->english_level != null && isset($englishLevelArray[$userData->personalInfo->english_level-1])) {
    $englishLevel = $englishLevelArray[$userData->personalInfo->english_level-1];
} else {
    $englishLevel = 'Not defined';
}

if (isset($userData->in_favorite) && $userData->in_favorite == 1) {
	$showStatus = 'liked';	
} else {
	$showStatus = '';	
}
?>

<div class="main sub-main">    
    <div class="global-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-4">
                    <div class="person-profile-gallery-wrapper">
                        <div class="person-profile-gallery-intro" style="background-image: url('<?= $userData->avatar->normal ?>')">
                            <a href="javascript:void(0);" class="like send-like <?= $showStatus ?>" data-id="<?= $userData->personalInfo->id; ?>">
                                <svg width="702pt" height="653pt" viewBox="0 0 702 653" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                    <g id="outer">
                                        <path fill="#fcfcfc" opacity="1.00" d=" M 84.16 37.19 C 113.49 14.69 150.00 1.52 187.01 0.93 C 216.59 0.01 246.35 6.77 272.65 20.35 C 304.59 36.68 331.02 62.90 349.07 93.78 C 373.22 53.16 412.69 21.71 458.15 8.40 C 498.41 -3.57 543.00 -1.47 581.48 15.69 C 617.31 31.40 647.14 59.30 667.25 92.67 C 688.17 127.03 698.85 166.98 700.99 206.97 C 701.88 229.59 698.33 252.13 692.89 274.03 C 680.00 325.26 653.40 372.79 617.28 411.27 C 604.73 425.09 590.40 437.10 576.57 449.60 C 500.69 517.30 424.94 585.15 349.01 652.80 L 349.89 653.00 L 348.96 653.00 C 267.04 578.61 185.15 504.18 103.27 429.74 C 56.59 387.08 22.17 330.68 7.85 268.92 C 2.14 244.11 -1.17 218.32 2.18 192.92 C 4.49 170.07 9.70 147.46 18.18 126.10 C 31.87 91.38 54.36 59.85 84.16 37.19 M 158.45 31.59 C 126.38 38.70 97.38 57.23 75.99 81.97 C 52.01 109.50 37.18 144.25 31.16 180.09 C 28.92 194.76 26.92 209.62 28.22 224.49 C 31.02 260.97 42.04 296.64 58.98 328.99 C 75.20 359.89 97.18 387.63 122.97 411.08 C 198.32 479.58 273.69 548.07 349.06 616.54 C 417.82 555.14 486.55 493.71 555.30 432.30 C 567.53 421.20 580.20 410.56 591.82 398.80 C 626.52 363.67 652.57 319.80 665.45 272.05 C 670.93 251.53 674.58 230.34 674.10 209.05 C 672.10 172.91 662.43 136.75 643.29 105.82 C 627.65 80.15 605.38 58.29 578.61 44.42 C 555.75 32.54 529.74 26.87 504.01 28.03 C 476.59 29.10 449.56 37.63 426.36 52.26 C 398.24 69.86 375.83 96.24 362.55 126.60 C 358.05 136.98 353.74 147.43 349.34 157.85 C 349.15 157.82 348.79 157.77 348.61 157.75 C 340.84 139.98 334.43 121.49 324.17 104.93 C 308.19 78.52 284.90 56.32 257.02 42.87 C 226.71 28.07 191.31 24.04 158.45 31.59 Z" />
                                        <path fill="#fcfcfc" opacity="1.00" d=" M 200.54 83.78 C 206.35 83.15 212.21 82.71 218.05 82.97 C 225.22 83.70 230.83 91.00 229.67 98.12 C 228.93 104.72 222.72 110.07 216.08 109.83 C 188.43 109.76 161.04 121.01 141.33 140.37 C 122.20 158.82 110.32 184.56 108.82 211.11 C 108.45 215.86 109.30 221.15 106.29 225.26 C 102.50 231.17 93.86 233.00 87.99 229.14 C 84.20 226.79 81.70 222.45 81.70 217.97 C 81.55 190.82 89.93 163.70 105.39 141.38 C 126.94 109.74 162.46 88.07 200.54 83.78 Z" />
                                    </g>
                                    <g id="inner">
                                        <path fill="#f13e4d" opacity="1.00" d=" M 158.45 31.59 C 191.31 24.04 226.71 28.07 257.02 42.87 C 284.90 56.32 308.19 78.52 324.17 104.93 C 334.43 121.49 340.84 139.98 348.61 157.75 C 348.79 157.77 349.15 157.82 349.34 157.85 C 353.74 147.43 358.05 136.98 362.55 126.60 C 375.83 96.24 398.24 69.86 426.36 52.26 C 449.56 37.63 476.59 29.10 504.01 28.03 C 529.74 26.87 555.75 32.54 578.61 44.42 C 605.38 58.29 627.65 80.15 643.29 105.82 C 662.43 136.75 672.10 172.91 674.10 209.05 C 674.58 230.34 670.93 251.53 665.45 272.05 C 652.57 319.80 626.52 363.67 591.82 398.80 C 580.20 410.56 567.53 421.20 555.30 432.30 C 486.55 493.71 417.82 555.14 349.06 616.54 C 273.69 548.07 198.32 479.58 122.97 411.08 C 97.18 387.63 75.20 359.89 58.98 328.99 C 42.04 296.64 31.02 260.97 28.22 224.49 C 26.92 209.62 28.92 194.76 31.16 180.09 C 37.18 144.25 52.01 109.50 75.99 81.97 C 97.38 57.23 126.38 38.70 158.45 31.59 M 200.54 83.78 C 162.46 88.07 126.94 109.74 105.39 141.38 C 89.93 163.70 81.55 190.82 81.70 217.97 C 81.70 222.45 84.20 226.79 87.99 229.14 C 93.86 233.00 102.50 231.17 106.29 225.26 C 109.30 221.15 108.45 215.86 108.82 211.11 C 110.32 184.56 122.20 158.82 141.33 140.37 C 161.04 121.01 188.43 109.76 216.08 109.83 C 222.72 110.07 228.93 104.72 229.67 98.12 C 230.83 91.00 225.22 83.70 218.05 82.97 C 212.21 82.71 206.35 83.15 200.54 83.78 Z" />
                                    </g>
                                </svg>
                            </a>
                        </div>

                        <div class="tabs person-profile-gallery-tabs">
                            <ul class="list-inline tab-list">
                                <li class="tab active">Photo</li>
                                <li class="tab">Video</li>
                            </ul>
                            <div class="tab-panel-container">
                                <div class="tab-panel active">
                                    <?php if (!empty($photos)) { $i = 1; ?>
                                    <div class="tab-panel-content">                                                          
                                        <div class="person-profile-gallery clearfix">
                                            <?php foreach ($photos as $photo) { ?>
                                                <?php if ($i>9) { break; } ?>
                                                <?php if (strpos($photo->original_image, 'premium') === false) {?>
                                                <div class="person-profile-gallery-item photo-item">
                                                    <a href="#photo-<?= $i ?>" class="person-profile-gallery-item-image" style="background-image: url('<?= $this->context->serverUrl . '/' . $photo->medium_thumb ?>')" data-fancybox="photo"></a>
                                                </div>
                                                <?php } else {?>
                                                <div class="person-profile-gallery-item photo-item">
                                                    <a href="#photo-<?= $i ?>" class="person-profile-gallery-item-image" style="background-image: url('/img/premium_small.png')" data-fancybox>
                                                        <span class="premium">
                                                            <span class="premium-content">
                                                                <img src="/img/premium.svg" alt="premium">
                                                                <span>Premium only</span>
                                                            </span>
                                                        </span>
                                                    </a>
                                                </div>
                                                <?php } ?>
                                            <?php $i++; } ?>             
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if (count($photos) > 9) {?>
                                    <a href="<?= Url::toRoute(['girls/gallery', 'otherUserID' => $userData->personalInfo->id]); ?>" class="btn btn-view">View More</a>
                                    <?php } ?> 
                                </div>
                                <div class="tab-panel">
                                    <div class="tab-panel-content">
                                        <?php if (!empty($videos)) { $i = 1; ?>
                                        <div class="person-profile-gallery clearfix">
                                            <?php foreach ($videos as $video) { 
                                                $video_poster = !empty($video->medium_thumb) ? $this->context->serverUrl . '/' . $video->medium_thumb : '/img/no_video_poster.jpg';
                                                 if ($i>9) { break; } ?>                                                
                                                    <?php if (strpos($video->path, 'premium') === false) {?>
                                                        <div class="person-profile-gallery-item video-item">
                                                            <a href="#video-<?= $i ?>" class="person-profile-gallery-item-video" style="background-image: url('<?= $video_poster ?>')"></a>
                                                        </div>
                                                    <?php } else {?>
                                                        <div class="person-profile-gallery-item video-item premium-item">
                                                        <a href="#video-<?= $i ?>" class="person-profile-gallery-item-video" style="background-image: url('/img/premium_small.png')" data-fancybox>
                                                        <span class="premium">
                                                            <span class="premium-content">
                                                                <img src="/img/premium.svg" alt="premium">
                                                                <span>Premium only</span>
                                                            </span>
                                                        </span>
                                                        </a>
                                                        </div>
                                                    <?php } ?>                                                
                                            <?php $i++; } ?>                                              
                                        </div>
                                        <?php } ?>
                                        <?php if (count($videos) > 9) {?>
                                        <a href="<?= Url::toRoute(['girls/video-gallery', 'otherUserID' => $userData->personalInfo->id]); ?>" class="btn btn-view">View More</a>
                                        <?php } ?> 
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>

                <div class="col-xs-12 col-sm-7 col-md-8">
                    <div class="person-profile-description">
                        <div class="person-profile-description-top">
                            <h2 class="person-profile-name"><?= $userData->personalInfo->first_name ?></h2>
                            <p class="person-profile-id"><span>ID</span> <?= $userData->personalInfo->id ?></p>
                            <div class="person-profile-status">
                                <a href="" class="btn-status btn-status-<?= ($online) ? "online" : "offline" ?>"><?= ($online) ? "online" : "offline" ?></a>
                                <?php if (!$online) {?>
                                <p class="last-visit">Last visited <span><?= date('d.m.Y', $userData->last_activity) ?></span></p>
                                <?php } ?>
                            </div>
                        </div>

                        <ul class="person-profile-action-list list-inline">
                            <li><a href="/account/chat/<?= $userData->personalInfo->id; ?>" class="web-link">
                                <img src="/img/webcam.svg" alt="web">
                                Start videochat
                            </a></li>
                            <li><a href="/account/chat/<?= $userData->personalInfo->id; ?>" class="chat-link">
                                <img src="/img/chat.svg" alt="web">
                                Chat now
                            </a></li>
                            <li><a href="/newmessage/<?= $userData->personalInfo->id; ?>" class="message-link">
                                <img src="/img/note.svg" alt="web">
                                Message
                            </a></li>
                            <li><a href="/account/shop/<?= $userData->personalInfo->id; ?>" class="gift-link">
                                <img src="/img/gifbox.svg" alt="web">
                                Send a gift
                            </a></li>
                            <li><a href="javascript:void(0);" class="wink-link" data-id="<?= $userData->personalInfo->id ?>">
                                <img src="/img/wink.svg" alt="web">
                                Wink
                            </a>
                            </li>
                            <li>
                                <?php if (empty($userData->in_blacklist)) { ?>
                                <a href="javascript:void(0);" class="blacklist-add" data-id="<?= $userData->personalInfo->id ?>">
                                    <i class="fa fa-plus"></i>
                                    Add to blacklist
                                </a>
                                <?php } else { ?>
                                <a href="javascript:void(0);" class="blacklist-remove" data-id="<?= $userData->personalInfo->id ?>">
                                    <i class="fa fa-times"></i>
                                    Remove from blacklist
                                </a>                                
                                <?php } ?>
                            </li>
                        </ul>

                        <div class="person-profile-info-block clearfix">
                            <h5 class="person-profile-info-block-title"><span>personal info</span></h5>

                            <ul class="person-profile-info-list">
                                <li>
                                    <span class="person-profile-info-list-title"><span>Age</span></span>
                                    <span class="person-profile-info-list-description"><?= $age ?></span>
                                </li>
                                <li>
                                    <span class="person-profile-info-list-title"><span>Zodiac</span></span>
                                    <span class="person-profile-info-list-description"><?= $zodiak ?></span>
                                </li>
                                <li>
                                    <span class="person-profile-info-list-title"><span>Eyes color</span></span>
                                    <span class="person-profile-info-list-description"><?= $eyes ?></span>
                                </li>
                                <li>
                                    <span class="person-profile-info-list-title"><span>Hair color</span></span>
                                    <span class="person-profile-info-list-description"><?= $hair ?></span>
                                </li>
                                <li>
                                    <span class="person-profile-info-list-title"><span>Height</span></span>
                                    <span class="person-profile-info-list-description"><?= $height ?></span>
                                </li>
                                <li>
                                    <span class="person-profile-info-list-title"><span>Weight</span></span>
                                    <span class="person-profile-info-list-description"><?= $weight ?></span>
                                </li>
                            </ul>

                            <ul class="person-profile-info-list">
                                <li>
                                    <span class="person-profile-info-list-title"><span>Country</span></span>
                                    <span class="person-profile-info-list-description"><?= $country ?></span>
                                </li>
                                <li>
                                    <span class="person-profile-info-list-title"><span>City</span></span>
                                    <span class="person-profile-info-list-description"><?= $city ?></span>
                                </li>
                                <li>
                                    <span class="person-profile-info-list-title"><span>Marital status</span></span>
                                    <span class="person-profile-info-list-description"><?= $marital ?></span>
                                </li>
                                <li>
                                    <span class="person-profile-info-list-title"><span>Children</span></span>
                                    <span class="person-profile-info-list-description"><?= $children ?></span>
                                </li>
                                <li>
                                    <span class="person-profile-info-list-title"><span>Smoker</span></span>
                                    <span class="person-profile-info-list-description"><?= $smoking ?></span>
                                </li>
                                <li>
                                    <span class="person-profile-info-list-title"><span>English</span></span>
                                    <span class="person-profile-info-list-description"><?= $englishLevel ?></span>
                                </li>
                            </ul>

                        </div>

                        <div class="person-profile-info-block clearfix">
                            <h5 class="person-profile-info-block-title"><span>About me</span></h5>
                            <div><?= $userData->personalInfo->about_me ?></div>
                        </div>

                        <div class="person-profile-info-block clearfix">
                            <h5 class="person-profile-info-block-title"><span>My Desire</span></h5>
                            <div><?= $userData->personalInfo->my_ideal ?></div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
 
</div>
    <section class="more-girls-online">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="more-girls-online-title">More single girls online</h2>
                </div>
            </div>
        </div>
        <?php if (!empty($singleOnlineGirls)) { ?>
        <div class="slider-wrapper gallery-slider-wrapper">
            <div class="slider-preloader">

                <svg viewBox="0 0 320 320" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="" height=""><g><path id="line" stroke="#111" stroke-width="30" stroke-linecap="round" d="M15 160h50"/><use xlink:href="#line" transform="rotate(30 160 160)" opacity=".083"/><use xlink:href="#line" transform="rotate(60 160 160)" opacity=".166"/><use xlink:href="#line" transform="rotate(90 160 160)" opacity=".25"/><use xlink:href="#line" transform="rotate(120 160 160)" opacity=".333"/><use xlink:href="#line" transform="rotate(150 160 160)" opacity=".417"/><use xlink:href="#line" transform="rotate(180 160 160)" opacity=".5"/><use xlink:href="#line" transform="rotate(210 160 160)" opacity=".583"/><use xlink:href="#line" transform="rotate(240 160 160)" opacity=".667"/><use xlink:href="#line" transform="rotate(270 160 160)" opacity=".75"/><use xlink:href="#line" transform="rotate(300 160 160)" opacity=".833"/><use xlink:href="#line" transform="rotate(330 160 160)" opacity=".917"/><animateTransform attributeName="transform" attributeType="XML" type="rotate" begin="0s" dur="1s" repeatCount="indefinite" calcMode="discrete" keyTimes="0;.0833;.166;.25;.3333;.4166;.5;.5833;.6666;.75;.8333;.9166;1" values="0,160,160;30,160,160;60,160,160;90,160,160;120,160,160;150,160,160;180,160,160;210,160,160;240,160,160;270,160,160;300,160,160;330,160,160;360,160,160"/></g></svg>

            </div>
            <div class="basic-slider gallery-slider">
                <?php 
                foreach ($singleOnlineGirls as $girl) { ?>
                    <?php if ($girl->id != $userData->personalInfo->id) {?>
                    <div class="slide">
                        <?=$this->render('//layouts/parts/girl-new.php', ['girl' => $girl, 'user_type' => Yii::$app->session['user_data']['userType']])?>
                    </div>
                    <?php } ?>
                    <?php } 
                ?>
            </div>
        </div>
        <?php } else { ?>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        No results                        
                    </div>
                </div>
            </div>
        <?php } ?>
    </section>
    <?php if (!empty($photos)) { $i = 1; ?>
    <section class="gallery-photos">
        <?php foreach ($photos as $photo) { ?>
        <?php if ($i>9) { break; } ?>
        <div id="photo-<?= $i ?>" class="">
            <?php if (strpos($photo->original_image, 'premium') === false) {?>
            <div>
                <img src="<?= $this->context->serverUrl . '/' . $photo->original_image ?>" alt="">
            </div>
            <?php } else {?>
            <div>
                <img src="<?= $photo->original_image ?>" alt="">
                <button class="btn pull-right get-access-photo" onclick="window['accountLibrary']['getAccess']($(this))" data-photo-id="<?= $photo->id ?>">Get access for <?= $photoPrice; ?> credits</button>
            </div>
            <?php } ?>
        </div>
        <?php $i++; } ?>      
    </section>
    <?php } ?>
    <?php if (!empty($videos)) { $i = 1; ?>
    <section class="gallery-videos">
        <?php foreach ($videos as $video) {
            $video_poster = !empty($video->medium_thumb) ? $this->context->serverUrl . '/' . $video->medium_thumb : '/img/no_video_poster.jpg';
            if ($i>9) { break; } ?>
        <div id="video-<?= $i ?>" class="gallery-video">
            <?php if (strpos($video->path, 'premium') === false) {?>
            <video class="video" loop controls width="100%" height="auto" poster="<?=$video_poster?>">
                <source src="<?= $this->context->serverUrl . '/' . $video->path ?>" type="video/mp4">
            </video>
            <?php } else {?>
            <div style="margin-top: calc(-50% + 60px);">
                <img src="<?= $video->path ?>" alt="premium image">
                <button class="btn pull-right get-access-video" data-video-id="<?= $video->id ?>">Get access for <?= $videoPrice; ?> credits</button>
            </div>
            <?php } ?>
        </div>
        <?php $i++; } ?>      
    </section>
    <?php } ?>

</div>

<style>
    .person-profile-action-list li {
        margin-right: 15px;
    }
</style>