<?php
use app\controllers\BaseController;
$session = Yii::$app->session;

if($session['user_type'] == BaseController::USER_FEMALE){
	$outher_prefix = 'men';
}else{
	$outher_prefix = 'SINGLE WOMEN';
}

?>
<div class="main sub-main">	
	<div class="global-content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-3">
					<aside class="side-bar side-bar-left">
						<h4 class="side-bar-title orange-color">Search <?=$outher_prefix?></h4>
						<div class="divider orange-bg"></div>
						<?php echo $this->render('//layouts/parts/advanced-search.php',['page_name'=>'search', 'hairColors' => $hairColors, 'eyesColors' => $eyesColors, 'physiques' => $physiques]); ?>
					</aside>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-9">
					<div class="search-results">
						<h4 class="search-results-title">Search Results</h4>
						<input type="hidden" id="limit" value="<?=$limit;?>">
						<input type="hidden" id="page" value="<?=$page;?>">						
						<div class="row">
							<div id="girls_block"></div>
							<div class="search pagination col-xs-12"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>