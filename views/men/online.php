<?php $ajax = Yii::$app->request->getIsAjax();
	if(!$ajax){	?>
<section class="girls-row gray-bg">
	<div class="container">
		<div class="row item-content">
			<?php }
				if(!empty($girls)){?>
				<div class="col-xs-12">
					<h3><span class="text-pink">Online</span> Men</h3>
				</div>
				<?php
					foreach($girls as $girl){?>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<?=$this->render('//layouts/parts/girl-online.php',['girl'=>$girl,'userType'=>2])?>
					</div>
					<?php }?>
					<div class="clear"></div>
					<?=$this->render('/layouts/parts/user_pagination.php',['count'=>$count,'pageName'=>$pageName,'pageController'=>'men','page'=>$page]);
			}else{echo '<div class="col-xs-12"><h3>No results</h3></div>';}
			if(!$ajax){ ?>
		</div>
	</div>
</section>
<?php } ?>