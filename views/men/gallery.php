<div class="main sub-main">
	
	<div class="global-content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-3">
					<aside class="side-bar side-bar-left">
						<?php echo $this->render('//layouts/parts/profile-vertical-menu.php'); ?>
					</aside>
				</div>
				<div class="col-xs-12 col-sm-9">
					<div class="my-gallery-container">
						<h2>Gallery</h2>
						<div class="created-albums-container">
							<div class="row">
                            	<?php if (!empty($photos)) { $i = 1; ?>
                                <div class="tab-panel-content">                                                          
                                    <div class="person-profile-gallery clearfix">
                                        <?php foreach ($photos as $photo) { ?>
                                            <?php if (strpos($photo->original_image, 'premium') === false) {?>
                                            <div class="person-profile-gallery-item photo-item">
                                                <a href="#photo-<?= $i ?>" class="person-profile-gallery-item-image" style="background-image: url('<?= $this->context->serverUrl . '/' . $photo->medium_thumb ?>')" data-fancybox="photo"></a>
                                            <div class="like-icons" data-photo-id="<?= $photo->id ?>">
                                            	<span class="like-icon"><i class="fa fa-thumbs-up"></i><span class="counter"><?= $photo->likes ?></span></span>
                                            	<span class="dislike-icon"><i class="fa fa-thumbs-down"></i><span class="counter"><?= $photo->dislikes ?></span></span>
                                            </div>                                                
                                            </div>
                                            <?php } else {?>
                                            <div class="person-profile-gallery-item photo-item">
                                                <a href="#photo-<?= $i ?>" class="person-profile-gallery-item-image" style="background-image: url('/img/premium_small.png')" data-fancybox>
                                                    <span class="premium">
                                                        <span class="premium-content">
                                                            <img src="/img/premium.svg" alt="premium">
                                                            <span>Premium only</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </div>
                                            <?php } ?>
                                        <?php $i++; } ?>             
                                    </div>
                                </div>
                                <?php } ?>
							</div>
						</div>
					</div>
				</div>
			    <?php if (!empty($photos)) { $i = 1; ?>
			    <section class="gallery-photos">
			        <?php foreach ($photos as $photo) { ?>
			        <div id="photo-<?= $i ?>" class="gallery-video">
			            <?php if (strpos($photo->original_image, 'premium') === false) {?>
			            <div>
			                <img src="<?= $this->context->serverUrl . '/' . $photo->original_image ?>" alt="">
			            </div>
			            <?php } else {?>
			            <div>
			                <img src="<?= $photo->original_image ?>" alt="">
			                <button class="btn pull-right get-access-photo" onclick="window['accountLibrary']['getAccess']($(this))" data-photo-id="<?= $photo->id ?>">Get access for <?= $photoPrice; ?> credits</button>
			            </div>
			            <?php } ?>
			        </div>
			        <?php $i++; } ?>      
			    </section>
			    <?php } ?>
			</div>
		</div>
	</div>
</div>