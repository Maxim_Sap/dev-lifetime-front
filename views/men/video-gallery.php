<div class="main sub-main">
    
    <div class="global-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <aside class="side-bar side-bar-left">
                        <?php echo $this->render('//layouts/parts/profile-vertical-menu.php'); ?>
                    </aside>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="my-gallery-container">
                        <h2>Video Gallery</h2>
                        <div class="created-albums-container">
                            <div class="row">
                                <?php if (!empty($videos)) { $i = 1; ?>
                                <div class="person-profile-gallery clearfix">
                                    <?php foreach ($videos as $video) { 
                                        $video_poster = !empty($video->medium_thumb) ? $this->context->serverUrl . '/' . $video->medium_thumb : '/img/no_video_poster.jpg';  ?>                                            
                                            <?php if (strpos($video->path, 'premium') === false) {?>
                                                <div class="person-profile-gallery-item video-item">
                                                    <a href="#video-<?= $i ?>" class="person-profile-gallery-item-video" style="background-image: url('<?= $video_poster ?>')"></a>
                                                </div>
                                            <?php } else {?>
                                                <div class="person-profile-gallery-item video-item premium-item">
                                                <a href="#video-<?= $i ?>" class="person-profile-gallery-item-video" style="background-image: url('/img/premium_small.png')" data-fancybox>
                                                <span class="premium">
                                                    <span class="premium-content">
                                                        <img src="/img/premium.svg" alt="premium">
                                                        <span>Premium only</span>
                                                    </span>
                                                </span>
                                                </a>
                                                </div>
                                            <?php } ?>                                                
                                    <?php $i++; } ?>                                              
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if (!empty($videos)) { $i = 1; ?>
                <section class="gallery-videos">
                    <?php foreach ($videos as $video) {
                        $video_poster = !empty($video->medium_thumb) ? $this->context->serverUrl . '/' . $video->medium_thumb : '/img/no_video_poster.jpg';
                        ?>
                    <div id="video-<?= $i ?>" class="gallery-video">
                        <?php if (strpos($video->path, 'premium') === false) {?>
                        <video class="video" loop controls width="100%" height="auto" poster="<?=$video_poster?>">
                            <source src="<?= $this->context->serverUrl . '/' . $video->path ?>" type="video/mp4">
                        </video>
                        <?php } else {?>
                        <div style="margin-top: calc(-50% + 60px);">
                            <img src="<?= $video->path ?>" alt="premium image">
                            <button class="btn pull-right get-access-video" data-video-id="<?= $video->id ?>">Get access for <?= $videoPrice; ?> credits</button>
                        </div>
                        <?php } ?>
                    </div>
                    <?php $i++; } ?>      
                </section>
                <?php } ?>
            </div>
        </div>
    </div>
</div>