<?php

use app\controllers\BaseController;

$session = Yii::$app->session;
if (isset($session['user_token']) && isset($session['user_data']['userType']) && $session['user_data']['userType'] == BaseController::USER_FEMALE) {
    $userTypeID = BaseController::USER_FEMALE;
    $userType   = 'Men';
} else {
    $userTypeID = BaseController::USER_MALE;
    $userType   = 'Girls';

}
$serverUrl = $this->context->serverUrl;
?>

<div class="main">
    <section class="home-intro">
        <div class="slider-wrapper">
            <div class="slider-preloader">
                <svg viewBox="0 0 320 320" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                     version="1.1" width="" height="">
                    <g>
                        <path id="line" stroke="#111" stroke-width="30" stroke-linecap="round" d="M15 160h50"/>
                        <use xlink:href="#line" transform="rotate(30 160 160)" opacity=".083"/>
                        <use xlink:href="#line" transform="rotate(60 160 160)" opacity=".166"/>
                        <use xlink:href="#line" transform="rotate(90 160 160)" opacity=".25"/>
                        <use xlink:href="#line" transform="rotate(120 160 160)" opacity=".333"/>
                        <use xlink:href="#line" transform="rotate(150 160 160)" opacity=".417"/>
                        <use xlink:href="#line" transform="rotate(180 160 160)" opacity=".5"/>
                        <use xlink:href="#line" transform="rotate(210 160 160)" opacity=".583"/>
                        <use xlink:href="#line" transform="rotate(240 160 160)" opacity=".667"/>
                        <use xlink:href="#line" transform="rotate(270 160 160)" opacity=".75"/>
                        <use xlink:href="#line" transform="rotate(300 160 160)" opacity=".833"/>
                        <use xlink:href="#line" transform="rotate(330 160 160)" opacity=".917"/>
                        <animateTransform attributeName="transform" attributeType="XML" type="rotate" begin="0s"
                                          dur="1s" repeatCount="indefinite" calcMode="discrete"
                                          keyTimes="0;.0833;.166;.25;.3333;.4166;.5;.5833;.6666;.75;.8333;.9166;1"
                                          values="0,160,160;30,160,160;60,160,160;90,160,160;120,160,160;150,160,160;180,160,160;210,160,160;240,160,160;270,160,160;300,160,160;330,160,160;360,160,160"/>
                    </g>
                </svg>
            </div>
            <div class="basic-slider intro-slider">
                <div class="slide" style="background-image: url('img/slide-img.jpg')"></div>
                <div class="slide" style="background-image: url('img/slide-img-2.jpg')"></div>
                <div class="slide" style="background-image: url('img/slide-img-3.jpg')"></div>
                <div class="slide" style="background-image: url('img/slide-img-4.jpg')"></div>
                <div class="slide" style="background-image: url('img/slide-img-5.jpg')"></div>
            </div>
        </div>
        <div class="home-intro-content-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="home-intro-content clearfix">
                            <?php
                            $session = Yii::$app->session;
                            if (!isset($session['user_token'])) { ?>
                                <?=$this->render('sign-up.php')?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="basic-section gallery">
        <div class="tabs gallery-tabs">
            <div class="text-center">
                <ul class="list-inline tab-list">
                    <li class="tab active">Now Online</li>
                    <li class="tab">Last added</li>
                    <li class="tab">Top <?=$userType?></li>
                    <li class="tab">All <?=$userType?></li>
                </ul>
            </div>
            <div class="tab-panel-container">
                <div class="tab-panel active">
                    <div class="tab-panel-content users-online">
                        <?php if (!empty($girls->online)) { ?>
                            <div class="slider-wrapper gallery-slider-wrapper">
                                <div class="slider-preloader">
                                    <svg viewBox="0 0 320 320" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="" height="">
                                        <g>
                                            <path id="line" stroke="#111" stroke-width="30" stroke-linecap="round"
                                                  d="M15 160h50"/>
                                            <use xlink:href="#line" transform="rotate(30 160 160)" opacity=".083"/>
                                            <use xlink:href="#line" transform="rotate(60 160 160)" opacity=".166"/>
                                            <use xlink:href="#line" transform="rotate(90 160 160)" opacity=".25"/>
                                            <use xlink:href="#line" transform="rotate(120 160 160)" opacity=".333"/>
                                            <use xlink:href="#line" transform="rotate(150 160 160)" opacity=".417"/>
                                            <use xlink:href="#line" transform="rotate(180 160 160)" opacity=".5"/>
                                            <use xlink:href="#line" transform="rotate(210 160 160)" opacity=".583"/>
                                            <use xlink:href="#line" transform="rotate(240 160 160)" opacity=".667"/>
                                            <use xlink:href="#line" transform="rotate(270 160 160)" opacity=".75"/>
                                            <use xlink:href="#line" transform="rotate(300 160 160)" opacity=".833"/>
                                            <use xlink:href="#line" transform="rotate(330 160 160)" opacity=".917"/>
                                            <animateTransform attributeName="transform" attributeType="XML"
                                                              type="rotate" begin="0s" dur="1s" repeatCount="indefinite"
                                                              calcMode="discrete"
                                                              keyTimes="0;.0833;.166;.25;.3333;.4166;.5;.5833;.6666;.75;.8333;.9166;1"
                                                              values="0,160,160;30,160,160;60,160,160;90,160,160;120,160,160;150,160,160;180,160,160;210,160,160;240,160,160;270,160,160;300,160,160;330,160,160;360,160,160"/>
                                        </g>
                                    </svg>
                                </div>
                                <div class="basic-slider gallery-slider">
                                    <?php foreach ($girls->online as $girl) { ?>
                                        <div class="slide">
                                            <?=$this->render('girl-online.php', ['girl' => $girl, 'userType' => $userTypeID])?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="tab-panel">
                    <div class="tab-panel-content users-new">
                        <?php if (isset($girls->new) && !empty($girls->new)) { ?>
                            <div class="slider-wrapper gallery-slider-wrapper">
                                <div class="slider-preloader">
                                    <svg viewBox="0 0 320 320" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="" height="">
                                        <g>
                                            <path id="line" stroke="#111" stroke-width="30" stroke-linecap="round"
                                                  d="M15 160h50"/>
                                            <use xlink:href="#line" transform="rotate(30 160 160)" opacity=".083"/>
                                            <use xlink:href="#line" transform="rotate(60 160 160)" opacity=".166"/>
                                            <use xlink:href="#line" transform="rotate(90 160 160)" opacity=".25"/>
                                            <use xlink:href="#line" transform="rotate(120 160 160)" opacity=".333"/>
                                            <use xlink:href="#line" transform="rotate(150 160 160)" opacity=".417"/>
                                            <use xlink:href="#line" transform="rotate(180 160 160)" opacity=".5"/>
                                            <use xlink:href="#line" transform="rotate(210 160 160)" opacity=".583"/>
                                            <use xlink:href="#line" transform="rotate(240 160 160)" opacity=".667"/>
                                            <use xlink:href="#line" transform="rotate(270 160 160)" opacity=".75"/>
                                            <use xlink:href="#line" transform="rotate(300 160 160)" opacity=".833"/>
                                            <use xlink:href="#line" transform="rotate(330 160 160)" opacity=".917"/>
                                            <animateTransform attributeName="transform" attributeType="XML"
                                                              type="rotate" begin="0s" dur="1s" repeatCount="indefinite"
                                                              calcMode="discrete"
                                                              keyTimes="0;.0833;.166;.25;.3333;.4166;.5;.5833;.6666;.75;.8333;.9166;1"
                                                              values="0,160,160;30,160,160;60,160,160;90,160,160;120,160,160;150,160,160;180,160,160;210,160,160;240,160,160;270,160,160;300,160,160;330,160,160;360,160,160"/>
                                        </g>
                                    </svg>
                                </div>
                                <div class="basic-slider gallery-slider">
                                    <?php
                                    foreach ($girls->new as $girl) { ?>
                                        <div class="slide">
                                            <?=$this->render('girl-new.php', ['girl' => $girl, 'user_type' => $userTypeID])?>
                                        </div>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="tab-panel">
                    <div class="tab-panel-content users-top">
                        <?php if (isset($girls->top) && !empty($girls->top)) { ?>
                            <div class="slider-wrapper gallery-slider-wrapper">
                                <div class="slider-preloader">
                                    <svg viewBox="0 0 320 320" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="" height="">
                                        <g>
                                            <path id="line" stroke="#111" stroke-width="30" stroke-linecap="round"
                                                  d="M15 160h50"/>
                                            <use xlink:href="#line" transform="rotate(30 160 160)" opacity=".083"/>
                                            <use xlink:href="#line" transform="rotate(60 160 160)" opacity=".166"/>
                                            <use xlink:href="#line" transform="rotate(90 160 160)" opacity=".25"/>
                                            <use xlink:href="#line" transform="rotate(120 160 160)" opacity=".333"/>
                                            <use xlink:href="#line" transform="rotate(150 160 160)" opacity=".417"/>
                                            <use xlink:href="#line" transform="rotate(180 160 160)" opacity=".5"/>
                                            <use xlink:href="#line" transform="rotate(210 160 160)" opacity=".583"/>
                                            <use xlink:href="#line" transform="rotate(240 160 160)" opacity=".667"/>
                                            <use xlink:href="#line" transform="rotate(270 160 160)" opacity=".75"/>
                                            <use xlink:href="#line" transform="rotate(300 160 160)" opacity=".833"/>
                                            <use xlink:href="#line" transform="rotate(330 160 160)" opacity=".917"/>
                                            <animateTransform attributeName="transform" attributeType="XML"
                                                              type="rotate" begin="0s" dur="1s" repeatCount="indefinite"
                                                              calcMode="discrete"
                                                              keyTimes="0;.0833;.166;.25;.3333;.4166;.5;.5833;.6666;.75;.8333;.9166;1"
                                                              values="0,160,160;30,160,160;60,160,160;90,160,160;120,160,160;150,160,160;180,160,160;210,160,160;240,160,160;270,160,160;300,160,160;330,160,160;360,160,160"/>
                                        </g>
                                    </svg>

                                </div>
                                <div class="basic-slider gallery-slider">
                                    <?php
                                    foreach ($girls->top as $girl) { ?>
                                        <div class="slide">
                                            <?=$this->render('girl-top.php', ['girl' => $girl, 'userType' => $userTypeID])?>
                                        </div>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="tab-panel">
                    <div class="tab-panel-content users-all">
                        <?php if (!empty($girls->all)) { ?>
                            <div class="slider-wrapper gallery-slider-wrapper">
                                <div class="slider-preloader">

                                    <svg viewBox="0 0 320 320" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="" height="">
                                        <g>
                                            <path id="line" stroke="#111" stroke-width="30" stroke-linecap="round"
                                                  d="M15 160h50"/>
                                            <use xlink:href="#line" transform="rotate(30 160 160)" opacity=".083"/>
                                            <use xlink:href="#line" transform="rotate(60 160 160)" opacity=".166"/>
                                            <use xlink:href="#line" transform="rotate(90 160 160)" opacity=".25"/>
                                            <use xlink:href="#line" transform="rotate(120 160 160)" opacity=".333"/>
                                            <use xlink:href="#line" transform="rotate(150 160 160)" opacity=".417"/>
                                            <use xlink:href="#line" transform="rotate(180 160 160)" opacity=".5"/>
                                            <use xlink:href="#line" transform="rotate(210 160 160)" opacity=".583"/>
                                            <use xlink:href="#line" transform="rotate(240 160 160)" opacity=".667"/>
                                            <use xlink:href="#line" transform="rotate(270 160 160)" opacity=".75"/>
                                            <use xlink:href="#line" transform="rotate(300 160 160)" opacity=".833"/>
                                            <use xlink:href="#line" transform="rotate(330 160 160)" opacity=".917"/>
                                            <animateTransform attributeName="transform" attributeType="XML"
                                                              type="rotate" begin="0s" dur="1s" repeatCount="indefinite"
                                                              calcMode="discrete"
                                                              keyTimes="0;.0833;.166;.25;.3333;.4166;.5;.5833;.6666;.75;.8333;.9166;1"
                                                              values="0,160,160;30,160,160;60,160,160;90,160,160;120,160,160;150,160,160;180,160,160;210,160,160;240,160,160;270,160,160;300,160,160;330,160,160;360,160,160"/>
                                        </g>
                                    </svg>

                                </div>
                                <div class="basic-slider gallery-slider">
                                    <?php foreach ($girls->all as $girl) { ?>
                                        <div class="slide">
                                            <?=$this->render('girl-new.php', ['girl' => $girl, 'userType' => $userTypeID])?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="text-center">
                <?php if (!isset($session['user_token'])) { ?>
                    <a href="#" class="btn btn-big scrollup" data-toggle="modal">Join Now <i
                            class="fa fa-angle-right"></i></a>
                <?php } ?>
            </div>

        </div>

    </section>

    <?php if (!empty($posts)) { ?>
        <section class="basic-section cta">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="section-title">latest love stories</h2>
                        <div class="cta-content-wrapper">
                            <div class="row">
                                <?php foreach ($posts as $one) { ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <figure class="cta-block">
                                            <a href="/blog/<?=$one->id;?>" class="cta-block-link"></a>
                                            <div class="cta-block-image-wrapper">
                                                <div class="cta-block-image"
                                                     style="background-image: url('<?=$serverUrl . "/" . $one->image?>')"></div>
                                            </div>
                                            <figcaption class="cta-block-content">
                                                <h4 class="cta-block-title"><?=$one->title?></h4>
                                            </figcaption>
                                        </figure>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="text-center">
                                        <a href="/blog" class="btn view-posts">View all stories</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php }
    if (!empty($about_page)) {
        ?>
        <section class="basic-section home-about">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="section-title">About Us</h2>
                        <?=html_entity_decode($about_page->description)?>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
</div>

