<div class="sort_by fl-l">
    <span>Sort by price:</span>
    <a href="javascript:void(0);" <?=isset($sort) && $sort == 'asc' ? 'class="active"':'';?> data-value="asc" title="Asc">
        <span class="to_up_price custom-border"> <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
        </span>
    </a>
    <a href="javascript:void(0);" <?=isset($sort) && $sort == 'desc' ? 'class="active"':'';?> data-value="desc" title="Desc">
        <span class="to_up_price custom-border"> <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
        </span>
    </a>
</div>