<?php 
$server_url = $this->context->server_url;
$date_create = strtotime($womans->date_create);
//если пользоватлеь добавлен мение месяца назад, то ему добавляется значек NEW
$new_label = ((time() - $date_create) <= 60*60*24*30) ? '<li class="is-new fl-r rounded-block">NEW</li>': "";
$avater = !empty($womans->thumb_normal) ? $server_url.'/'.$womans->thumb_normal : '/img/no_avatar_normal.jpg';

if(isset($womans->in_favorite) && $womans->in_favorite == 1){
	$show_status = 'hidden';
	$show_status2 = '';
}else{
	$show_status = '';
	$show_status2 = 'hidden';
}
$prefix = (isset($user_type) && $user_type == 2) ? 'men' : 'girls';
?>
<div class="girl-tile pos-rel" data-user-id="<?=$womans->user_id;?>">
    <div class="girl-photo pos-rel" style="background-image:url(<?=$avater;?>);">
        <ul class="status text-white pos-abs">
            <!--<li class="is-webcam">live<br/>Cam</li>-->
            <?=$new_label;?>
            <!--<li class="is-new">NEW</li>-->
            <!--<li class="is-online">online</li>-->

        </ul>
    </div>
    <div class="girl-meta">
        <div class="meta-left fl-l">
            <div class="girl-name"><?=$womans->first_name;?></div>
            <div class="girl-id"><?=$womans->user_id;?></div>  
        </div>
        <div class="meta-right fl-l black-list-control">
			<a class="link_blacklist fl-r pos-abs" href="/account/delete-form-blacklist/<?=$womans->user_id;?>">
				<i class="fa fa-times text-black" aria-hidden="true"></i>
				<span data-atr="dell">Delete from blacklist</span>
			</a>
        </div>
    </div>
    <a href="<?="/".$prefix."/".$womans->user_id;?>" class="girl-link pos-abs"></a>
</div>