<?php
use app\controllers\BaseController;
use yii\helpers\Url;
use app\models\ContactForm;
use yii\widgets\ActiveForm;    
use yii\helpers\Html;    


$session = Yii::$app->session;
$userType = (isset($session['user_token']) && isset($session['user_data']['userType']) && $session['user_data']['userType'] == BaseController::USER_FEMALE) ? BaseController::USER_FEMALE : BaseController::USER_MALE;
$numberOfHtmlMessages = (isset($session['new_mess_count']['message']) &&  is_numeric($session['new_mess_count']['message'])) ? $session['new_mess_count']['message'] : 0;
$numberOfHtmlLetters = (isset($session['new_mess_count']['letters']) &&  is_numeric($session['new_mess_count']['letters'])) ? $session['new_mess_count']['letters'] : 0;
$numberOfHtmlChatSms = (isset($session['new_mess_count']['chat']) &&  is_numeric($session['new_mess_count']['chat'])) ? $session['new_mess_count']['chat'] : 0; 

$suffix = ($userType == BaseController::USER_MALE) ? 'girls' : 'men';

$this->registerCssFile("@web/css/simple-scrollbar.css", [
    'depends' => [app\assets\AppAsset::className()],    
], 'css-scroll');

$this->registerJsFile(
    '@web/js/simple-scrollbar.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>

<div class="new-topline hidden-xs">
    <input type="hidden" id="userID" value="<?=$session['user_data']['userID']?>">
    <input type="hidden" id="userType" value="<?=$session['user_data']['userType']?>">
    <input type="hidden" id="userToken" value="<?=$session['user_token']?>">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <a href="<?= Yii::$app->homeUrl; ?>" class="top-logo">
                    <img src="/img/logo.png" alt="">
                </a>
                <ul class="action-list list-inline">
                    <li class="total-credits">
                        <div class="credits-icon">
                            <img src="/img/cr.svg" alt="credits" class="credits-icon-img">
                            <span class="credits-icon-text">Cr</span>
                        </div>
                        <span><?php if ($session['user_data']['balance'] != null) {echo $session['user_data']['balance']; } else { echo '0.00'; } ?></span>
                    </li>
                    <li class="buy-credits">
                        <a href="/payments" class="buy-credits-link">Buy Credits</a>
                    </li>
                    <li class="notification" data-message-count="<?= $numberOfHtmlMessages ?>" data-letters-count="<?= $numberOfHtmlLetters ?>" data-chat-sms-count="<?= $numberOfHtmlChatSms ?>">
                        <img src="/img/bell.svg" class="notification-icon" alt="bell">
                        <?php $notifications = $numberOfHtmlMessages + $numberOfHtmlLetters + $numberOfHtmlChatSms; ?>
                        <?php if ($notifications > 0) { ?>
                        <a href="javascript:void(0);" onclick="new_message_link($(this));" class="notification-count">                    
                        <span class="plus">+</span><?= $notifications ?>                            
                        </a>
                        <?php } ?>
                    </li>
                    <li class="loudspeaker">
                        <a href="javascript:void(0);" class="loudspeaker-link">
                            <img src="/img/loudspeaker.svg" class="loudspeaker-icon" alt="loudspeaker">
                        </a>
                    </li>
                    <li class="operator">
                        <div class="operator-photo" style="background-image: url(<?=$session['user_data']['avatar']?>);"><span class="status status-online"></span></div>
                        <span class="operator-choose"><?= (!empty($session['user_data']['name'])) ? $session['user_data']['name'] : 'Noname' ?></span>
                        <ul class="operator-list">
                            <li><a href="/account/<?=$session['user_data']['userID']?>">My profile</a></li>
                            <?php if ($session['user_type'] != BaseController::USER_FEMALE) {?>
                            <li><a href="/account/edit/<?=$session['user_data']['userID']?>">Edit profile</a></li>
                            <?php } ?>
                            <li><a id="logout_btn" href="javascript:void(0);" onclick="window['commonLibrary']['logout']()">Log out</a></li>                                
                        </ul>
                    </li>
                    <li class="chat-online">
                        <a href="javascript:void(0);" class="chat-button">
                            <img src="/img/chat-icon.png" alt="">chat online
                        </a>
<div class="chat-persons-box">
                                    <div class="top">
                                        <span class="top-text">These ladies are inviting you to chat!<span class="close-box"></span></span>
                                    </div>
                                    <div ss-container class="content">

                                        <div class="chat-person-item clearfix">
                                            <div class="item-person-image" style="background-image: url('img/chat-person.png')">
                                                <span class="status online">Online</span>
                                            </div>
                                            <div class="item-person-description">
                                                <ul class="item-name-years list-inline">
                                                    <li>
                                                        <a href="javascript:void(0);" class="person-name">Zlata</a>
                                                    </li>
                                                    <li>
                                                        <span class="person-age">24 years</span>
                                                    </li>
                                                </ul>
                                                <div class="message-box">
                                                    <div ss-container class="content">
                                                        <p>When you look into my eyes you will see the eternal desire to love.</p>
                                                    </div>
                                                </div>
                                                <ul class="item-action-list list-inline">
                                                    <li>
                                                        <a href="javascript:void(0);" class="chat-now">Chat Now</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);" class="no-chat">No Thanks</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="chat-person-item clearfix">
                                            <div class="item-person-image" style="background-image: url('img/chat-person.png')">
                                                <span class="status online">Online</span>
                                            </div>
                                            <div class="item-person-description">
                                                <ul class="item-name-years list-inline">
                                                    <li>
                                                        <a href="javascript:void(0);" class="person-name">Zlata</a>
                                                    </li>
                                                    <li>
                                                        <span class="person-age">24 years</span>
                                                    </li>
                                                </ul>
                                                <div class="message-box">
                                                    <div ss-container class="content">
                                                        <p>When you look into my eyes you will see the eternal desire to love.</p>
                                                    </div>
                                                </div>
                                                <ul class="item-action-list list-inline">
                                                    <li>
                                                        <a href="javascript:void(0);" class="chat-now">Chat Now</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);" class="no-chat">No Thanks</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="chat-person-item clearfix">
                                            <div class="item-person-image" style="background-image: url('img/chat-person.png')">
                                                <span class="status online">Online</span>
                                            </div>
                                            <div class="item-person-description">
                                                <ul class="item-name-years list-inline">
                                                    <li>
                                                        <a href="javascript:void(0);" class="person-name">Zlata</a>
                                                    </li>
                                                    <li>
                                                        <span class="person-age">24 years</span>
                                                    </li>
                                                </ul>
                                                <div class="message-box">
                                                    <div ss-container class="content">
                                                        <p>When you look into my eyes you will see the eternal desire to love.</p>
                                                    </div>
                                                </div>
                                                <ul class="item-action-list list-inline">
                                                    <li>
                                                        <a href="javascript:void(0);" class="chat-now">Chat Now</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);" class="no-chat">No Thanks</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- Modal Info block-->
<div id="modal-info-new-message" class="hidden" data-message-count="<?= $numberOfHtmlMessages ?>" data-letters-count="<?= $numberOfHtmlLetters ?>" data-chat-sms-count="<?= $numberOfHtmlChatSms ?>">
    <div class="image"></div>
    <div class="text">
        <a href="javascript:void(0);" data-href="" onclick="new_message_link($(this));">
            <span class="caption"></span>
            <span class="name"></span>
        </a>
    </div>
    <a href="javascript:void(0);" class="my-close" onclick="close_new_message_popup();">x</a>
</div>
<audio class="audio_new_message hidden" controls preload="none"> 
   <source src="/sounds/new_message.mp3" type="audio/mpeg">
</audio>
<!-- Modal MESSAGE-->
<div class="modal fade" id="modalMessage" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">                
                <div class="modal-header-logo rounded-block">
                    <img src="/img/logo.png" alt="Logo">
                </div>
            </div>
            <div class="modal-body">
                <span></span>
            </div>
        </div>
    </div>
</div>
            <!-- Modal CONTACT-->
            <div class="modal fade" id="modalContact" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span></span>
                  <span></span>
                </button>
                <div class="modal-content">
                  <div class="modal-header">                
                    <div class="modal-header-logo rounded-block">
                      <img src="/img/logo.png" alt="Logo">
                    </div>
                  </div>
                  <div class="modal-body">
                    <h2 class="contact-block-title">Contact Us</h2>
                    <?php $model = new ContactForm(); ?>

                    <?php $form = ActiveForm::begin([
                      'method' => 'post',
                      'class' => 'contact-form clearfix',
                    // URL to Contact Action
                      'action' => Url::to(['//site/contact-us']),
                      ]);?>

                      <?= $form->field($model, 'name', [])->textInput(['placeholder' => 'Name']);  ?>

                      <?= $form->field($model, 'email', [])->textInput(['placeholder' => $model->getAttributeLabel('email')]);  ?>

                      <div class="form-item-wrapper options">
                        <?php echo $form->field($model, 'subject')->dropDownList([
                         '1' => 'Browser can\'t connect to chat server',
                         '2' => 'Problems with chat/videochat',
                         '3'=>'Can\'t login to site',
                         '4' => 'Bug report',
                         '5' => 'Other problem'
                         ], [
                         'prompt' => 'Choose one'
                         ]); ?>                        
                       </div>

                       <?= $form->field($model, 'body')->textArea(['rows' => 3, 'placeholder' => 'Type here']) ?>
                       <div class="container">
                        <div class="row">
                          <div class="col-xs-6 centered">
                            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-reset']) ?>
                          </div>
                          <div class="col-xs-6 centered">
                            <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-submit']) ?>
                          </div>
                        </div>                                
                      </div>
                      <?php ActiveForm::end(); ?>
                    </div>            
                  </div>
                </div>
              </div>
              <?php if (!isset($session['user_token']) && Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') {?>
                <!-- Modal FORGOT PASWORD-->
                <div class="modal fade" id="modalForgotPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span></span>
                        <span></span>
                      </button>
                      <div class="modal-header">
                        <div class="modal-header-logo rounded-block">
                          <img src="/img/logo.png" alt="Logo">
                        </div>
                      </div>
                      <div class="modal-body">
                        <span class="modal-message-text">Enter your e-mail below and we will send you reset instructions!</span>
                        <span class="error-message-text"></span>
                        <form action="">
                          <input type="email" placeholder="E-mail" name="email" required="required">                    
                          <input type="submit" value="Send" class="btn btn-wide"> 
                        </form>                
                      </div>
                    </div>
                  </div>
                </div>
                <?php } ?>


                <div id="messages-feed">
                  
                </div>