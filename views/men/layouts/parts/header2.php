<?php
    use app\controllers\BaseController;
    $session = Yii::$app->session;
    $userType = (isset($session['user_token']) && isset($session['user_data']['userType']) &&  $session['user_data']['userType'] == BaseController::USER_MALE) ? BaseController::USER_MALE : BaseController::USER_FEMALE;
    $apiServer = $this->context->serverUrl;
?>
<style>
    .mess_text_wrap .mess_text span.name {
        color: #005aff;
        font-weight: 600;
    }

    .mess_text_wrap .mess_text span.text {
        font-weight: 500;
        margin-left: 4px;
    }
    a{
        outline: none!important;
    }
    .start_video_chat .info-block{
        display: none;
    }
</style>
<!-- Modal MESSAGE-->
<div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <div class="modal-header-logo rounded-block"><svg><use xlink:href="#logo"/></svg></div>
            </div>
            <div class="modal-body">
                <span></span>
            </div>
        </div>
    </div>
</div>

<header id="header" class="main-col-bg text-white chat_header">   <!--отличается классом chat_header-->
    <div class="container">
        <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3">
                <a href="/" class="logo"><svg><use xlink:href="#logo"/></svg>myrussian<span class="text-pink">brides</span></a>
            </div>
            <?php if(isset($session['user_token'])){?>
            <!--<div class="col-xs-12 col-md-3">
            </div>  -->
            <?php } ?>            
            
            <?php if(!isset($session['user_token'])){?>
                <div class="col-xs-9 col-sm-9 col-md-9">
                    <div class="login-block">
                        <form action="" class="login-form">
                            <input type="text" name="login" placeholder="Login" required>
                            <input type="password" name="password" placeholder="Password" required>
                            <button type="submit" class="btn btn-info">Login</button>
                            <div class="clearfix"></div>
                            <div class="remember">
                                <input type="checkbox" id="remember">
                                <label for="remember" class="pos-rel">Remember me</label>
                            </div>
                            <div class="forgotpass">
                                <a href="#" data-toggle="modal" data-target="#modalForgotPassword">Forgot Password?</a>
                            </div>
            <?php } else {?>                
                <div class="col-xs-9 col-sm-9 col-md-9">
	                <?php if ($users_type == BaseController::USER_FEMALE) {?>
			            <div class="cam_online fl-l">
			                <input type="checkbox" id="check_cam_online" <?php if(isset($session['user_data']['cam_online']) && $session['user_data']['cam_online'] == 1){echo "checked";}?>>
			                <label for="check_cam_online" class="pos-rel" >WEBCAM ONLINE</label>
			            </div>
		            <?php } ?>
		            <button id="close_cam" hidden></button>
                    <div class="user-block">
                        <!--<div class="user-credits text-green">
                            <?=$session['user_data']['balance']?> cr
                        </div>
                        <a href="/account/payments" class="buy-credits text-green">
                            BUY CREDITS
                        </a> -->
                        <br>
            <?php } ?> 
                        <div class="photo_lang_wrap ">     
                            <?//=$this->render('language-select.php')?>                            
            <?php if(!isset($session['user_token'])){?> 
                        </div>               
                        </form>
                    </div>
                    <a href="javascript:void(0);" class="feed-toggle">Show message feed</a>
                </div>
            <?php } else {?>  
                            <div class="user-photo pos-rel">                                                                                      
                                <div class="user-photo-wrap img_bg rounded-block" data-src="<?=$session['user_data']['avatar']?>" style="background-image: url(<?=$session['user_data']['avatar']?>);"></div>
                                <span class="user-name-chat text-white"><?=$session['user_data']['name']?></span>    <!--этот спанчик добавить-->
                                <i class="fa fa-chevron-down pos-abs" aria-hidden="true"></i> 
                                <ul class="user-dropdown hidden">
                                    <li>
                                        <a href="/account/<?=$session['user_data']['user_id']?>">
                                            <div class="user-dropdown-photo-wrap" style="background-image: url(<?=$session['user_data']['avatar']?>);">
                                                <!--<img src="<?=$session['user_data']['avatar']?>" title="<?=$session['user_data']['name']?>" />-->
                                            </div>
                                            <span class="user-name"> <?=$session['user_data']['name']?></span><br/>
                                            <span class="user-id">ID: <?=$session['user_data']['user_id']?></span>
                                        </a>
                                    </li>
                                    <li class="settings">
                                        <a href="/account/edit/<?=$session['user_data']['user_id']?>" class="">
                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                            Edit profile
                                        </a>
                                    </li>
                                    <li class="exit">                                      
                                        <a href="<?= Yii::$app->homeUrl; ?>" class="btn_exit" id="logout_btn">
                                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                                            Logout
                                        </a>
                                    </li>
                                </ul>   
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="userID" value="<?=$session['user_data']['user_id']?>">
                <input type="hidden" id="userType" value="<?=$session['user_data']['user_types']?>">
                <input type="hidden" id="userToken" value="<?=$session['user_token']?>">
                <audio class="start_vidio_chat" controls preload="none">
                    <source src="/sounds/ContactOn.wav" type="audio/mpeg">
                </audio>
            <?php } ?>
		</div>
    </div>
</header>

<div id="messages-feed" class="not-display">
  <div class="close-line clearfix">
    <button type="button" class="close">
      <span></span>
      <span></span>
  </button>
</div>
<div class="messages-feed-inner">
    <div class="messages-feed-container">
      <div class="message-item">
        <a href="javascript:void(0);" class="message-item-link"></a>
        <span class="message-title">New message</span>
        <div class="message-body clearfix">
          <div class="avatar" style="background-image: url('')"></div>
          <div class="text-container">
            <span class="message-autor">Autor</span>
            <span class="message-text">Message from autor</span>
        </div>
    </div>
</div>
<div class="message-item">
    <a href="javascript:void(0);" class="message-item-link"></a>
    <span class="message-title">New message</span>
    <div class="message-body clearfix">
      <div class="avatar" style="background-image: url('')"></div>
      <div class="text-container">
        <span class="message-autor">Autor</span>
        <span class="message-text">Message from autor</span>
    </div>
</div>
</div>
<div class="message-item">
    <a href="javascript:void(0);" class="message-item-link"></a>
    <span class="message-title">New message</span>
    <div class="message-body clearfix">
      <div class="avatar" style="background-image: url('')"></div>
      <div class="text-container">
        <span class="message-autor">Autor</span>
        <span class="message-text">Message from autor</span>
    </div>
</div>
</div>
<div class="message-item">
    <a href="javascript:void(0);" class="message-item-link"></a>
    <span class="message-title">New message</span>
    <div class="message-body clearfix">
      <div class="avatar" style="background-image: url('')"></div>
      <div class="text-container">
        <span class="message-autor">Autor</span>
        <span class="message-text">Message from autor</span>
    </div>
</div>
</div>
<div class="message-item">
    <a href="javascript:void(0);" class="message-item-link"></a>
    <span class="message-title">New message</span>
    <div class="message-body clearfix">
      <div class="avatar" style="background-image: url('')"></div>
      <div class="text-container">
        <span class="message-autor">Autor</span>
        <span class="message-text">Message from autor</span>
    </div>
</div>
</div>
<div class="message-item">
    <a href="javascript:void(0);" class="message-item-link"></a>
    <span class="message-title">New message</span>
    <div class="message-body clearfix">
      <div class="avatar" style="background-image: url('')"></div>
      <div class="text-container">
        <span class="message-autor">Autor</span>
        <span class="message-text">Message from autor</span>
    </div>
</div>
</div>
</div>
</div> 
</div>
