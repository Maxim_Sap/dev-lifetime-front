<?php
    $session = Yii::$app->session;
?>
<div class="home-intro-content-tabs">

    <ul class="list-inline tab-list">
        <li class="tab active">Sign Up Free</li>
        <li class="tab">Log in</li>
    </ul>
    <div class="tab-panel-container">
        <div class="tab-panel active">
            <div class="tab-panel-content">
                <form class="sign-form sign-up-block-form">
                    <label for="sign-mail">E-Mail</label>
                    <input type="email" name="email" id="sign-mail" data-type="email" required>
                    <label for="sign-password">Password</label>
                    <input name="password" type="password" id="sign-password" data-type="password" required>
                    <label for="conf-password">Confirm Password</label>
                    <input name="conf_password" type="password" id="conf-password" required>
                    <input name="referral_link" type="hidden" value="<?=(isset($session['referral_link']) && $session['referral_link'] != '') ? $session['referral_link'] : "";?>">
                    <input name="ip" type="hidden" value="<?=$_SERVER['REMOTE_ADDR']?>">
                    <label class="agree"><input type="checkbox" name="sign-agree">I agree with <a href="/license-agreement">License agreement</a></label>
                    <button type="submit" class="btn btn-submit">Sign Up Now</button>
                    <label for="sign-name">Or log in by social networks:</label>
                    <ul class="social-buttons-list list-inline clearfix">
                        <li><a href="javascript:void(0);" class="btn-social btn-social-facebook" id="fb_login" scope="public_profile,email" onlogin="window['commonLibrary']['checkLoginState']();" onclick="window['commonLibrary']['FB_login']()">Facebook</a></li>
                        <li><a href="<?=$this->context->googleHref;?>" class="btn-social btn-social-google">Google+</a></li>
                    </ul>
                </form>
            </div>
        </div>
        <div class="tab-panel">
            <div class="tab-panel-content">
				<form class="login-form">                    
                    <label for="login-mail">E-Mail</label>
                    <input type="email" name="login" id="login-mail" data-type="email" required>
                    <label for="login-password">Password</label>
                    <input name="password" type="password" id="login-password" data-type="password" required>
                    <label class="remember"><input type="checkbox" id="remember">Remember Me</label>
					<div class="forgotpass">
                        <a href="#" data-toggle="modal" data-target="#modalForgotPassword">Forgot Password?</a>
                    </div>
                    <button type="submit" class="btn btn-submit">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>