<?php
	$countOnPage = (Yii::$app->params['numberOfMessagesOnPage'] > $lettersCount) ? $lettersCount : Yii::$app->params['numberOfMessagesOnPage'];
	$total = (($lettersCount - 1) / $countOnPage) + 1;
	$total =  intval($total);
	$pagePrev = $page - 1;
	$pageNext = $page + 1;
	$page_name = (!empty($pageName)) ? $pageName : $this->context->verticalMenuName;
?>
<ul class="message-page-navigation-list list-inline hidden-xs">
	<li>
		<span data-page="<?=$page;?>" data-page-name="<?=$page_name;?>" class="page current-page-messages-quantity"><?=$page?></span> of <span class="global-messages-quantity"><?=$total?></span>
	</li>
	<li>
		<ul class="message-page-navigation-buttons-list">
			<li class="prev">
				<a href="javascript:void(0);" <?php if($pagePrev <= 0 || $pagePrev >$total){ echo 'class="disabled"';}?> <?php if($pagePrev > 0 && $pagePrev <= $total){echo "onclick=\"window['messageLibrary']['messagePagination']($pagePrev,'$page_name');\"";}?>><i class="fa fa-angle-left" aria-hidden="true"></i></a>
			</li>
			<li class="next">
				<a href="javascript:void(0);" <?php if($pageNext <= 0 || $pageNext > $total){ echo 'class="disabled"';}?> <?php if($pageNext > 0 && $pageNext <= $total){echo "onclick=\"window['messageLibrary']['messagePagination']($pageNext,'$page_name');\"";}?>><i class="fa fa-angle-right" aria-hidden="true"></i></a>
			</li>
		</ul>
	</li>
</ul>