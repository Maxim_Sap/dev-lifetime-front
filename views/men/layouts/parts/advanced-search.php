
<form action="" class="search_girl search-form">
	<div class="field-group">
		<div class="switch-field">
	      <div class="switch-title">Newcomer</div>
	      <input type="radio" id="switch_left" name="new-girls" value="1" />
	      <label for="switch_left" class="btn btn-disabled">LAST-ADDED</label>
	      <input type="radio" id="switch_right" name="new-girls" value="0" checked/>
	      <label for="switch_right" class="btn btn-disabled">VIEW ALL</label>
	    </div>
	</div>
	<div class="field-group">
		<div class="switch-field">
	      <div class="switch-title">Status</div>
	      <input type="radio" id="switch_1" name="girls-online" value="1" />
	      <label for="switch_1" class="btn btn-disabled">ONLINE</label>
	      <input type="radio" id="switch_2" name="girls-online" value="0" checked/>
	      <label for="switch_2" class="btn btn-disabled">VIEW ALL</label>
	    </div>
	</div>
	<div class="field-group">
		<div class="switch-field">
	      <div class="switch-title">Webcam</div>
	      <input type="radio" id="switch_3" name="cam-online" value="1" />
	      <label for="switch_3" class="btn btn-disabled">WITH CAM</label>
	      <input type="radio" id="switch_4" name="cam-online" value="0" checked/>
	      <label for="switch_4" class="btn btn-disabled">VIEW ALL</label>
	    </div>
	</div>

	<label class="field-group-title">Age</label>
	<input type="range" class="range range-age">

	<label class="field-group-title">Height</label>
	<input type="range" class="range range-height">

	<div class="field-group">
		<label class="field-group-title">Eyes color</label>
		<div class="color-palette">
			<?php foreach ($eyesColors as $eyeColor) { ?>
				<input type="checkbox" name="eyes" data-color="<?=$eyeColor['color'];?>" value="<?= $eyeColor['id']; ?>">		
			<?php } ?>
		</div>
	</div>

	<div class="field-group">
		<label class="field-group-title">Physique</label>
		<div class="checkbox-group">
			<?php foreach ($physiques as $physique) { ?>
				<label for="physique<?= $physique['id']; ?>"><input type="checkbox" name="physique" id="physique<?= $physique['id']; ?>" value="<?= $physique['id']; ?>"><?= $physique['name']; ?></label><br/>
			<?php } ?>
		</div>
	</div>

	<div class="field-group">
		<label class="field-group-title">Hair color</label>
		<div class="color-palette">
			<?php foreach ($hairColors as $hairColor) { ?>
				<input type="checkbox" name="hair" data-color="<?= $hairColor['color']; ?>" value="<?= $hairColor['id']; ?>">
			<?php } ?>
		</div>
	</div>

	<div class="field-group">
		<label class="field-group-title">Country</label>
		<div class="country-select">
			<select name="options" id="country" data-placeholder="Select" multiple>
				<option value="120">Ukraine</option>
			</select>
		</div>
		<label class="field-group-title">ID</label>
		<input type="text" name="id">
		<label class="field-group-title">Name</label>
		<input type="text" name="name">
		<button type="reset" class="btn btn-reset btn-reset-with-icon reset-all"><span class="reset-icon"></span>Reset to default</button>
<?php		
	$this->registerJs(
	    "$('.reset-all').click();",
	    \yii\web\View::POS_READY	    
	);
?>
	</div>
</form>
