<?php
	if (!function_exists('links')) {
		function links($currentpage, $from, $to, $function) 
		{
			$pages = array();
			// loop to show links to range of pages around current page
			for ($x = $from; $x <= $to; $x++) {
			   // if it's a valid page number...
			   if (($x > 0) && ($x <= $to)) {
			      // if we're on current page...
			      if ($x == $currentpage) {
			         // 'highlight' it but don't make a link
			         $pages[] = "<li class=\"active\"><a href=\"javascript:void(0);\" onclick=\"$function($x);return false;\">$x</a></li>";
			      // if not current page...
			      } else {
			         // make it a link
			         $pages[] = "<li><a href=\"javascript:void(0);\" onclick=\"$function($x);return false;\">$x</a></li>";
			      } // end else
			   } // end if 
			} // end for
			//var_dump($pages);
			return implode('', $pages);
		}
	}

switch($pageName){
	case 'chat':
		$function = "window['accountLibrary']['chatPagination']";
		break;
	case 'contact':
		$function = "window['accountLibrary']['contactPagination']";
		break;
	case 'activity-page':
		$function = 'activityPagination';
		break;
	case 'letters':
		$function = 'finance_pagination';
		break;
	case 'chats':
		$function = 'finance_pagination';
		break;
	case 'video-chat':
		$function = 'finance_pagination';
		break;
	case 'gifts':
		$function = 'finance_pagination';
		break;
	case 'tasks':
		$function = 'tasks_pagination';
		break;
}
	
	$totalpages = (int) (ceil($userCount / $limit));
	//var_dump($totalpages);
?>
<?php

	/******  build the pagination links ******/
	// range of num links to show
	$range = 1;
	$window = $range * 2;
	$dots = "<li><a href=\"javascript:void(0);\">...</a></li>";
	
	$paginationResult = '';
	// if not on page 1, don't show prev links
	if ($currentpage > 1) {
	   // get previous page num
	   $prevpage = $currentpage - 1;
	   // show < link to go back to prev page
	   $paginationResult .= "<li class=\"prev\"><a href=\"javascript:void(0);\" onclick=\"$function('$prevpage');return false;\"> &lt; Prev</a></li>";
	} // end if 

    // Example: 1 2 3 4
    if ($totalpages <= 5 + $range*2) {
    	$from = 1;
    	$to = $totalpages;    	
		$paginationResult .= links($currentpage, $from, $to, $function);				
    }
    // Example: 1 [2] 3 4 5 6 ... 23 24
	elseif ($currentpage <= $window + 2)
	{
		$from = 1;
		$to = $window + 2;
		$paginationResult .= links($currentpage, $from, $to, $function);
		$paginationResult .= $dots . ' '. links($currentpage, $totalpages-1, $totalpages, $function);
	}
	// Example: 1 2 ... 32 33 34 35 [36] 37
	elseif ($currentpage >= $totalpages - $window)
	{
		$from = $totalpages - $window - 1;
		$to = $totalpages;
		$paginationResult .= links($currentpage, 1, 2, $function) . ' ' . $dots;
		$paginationResult .= links($currentpage, $from, $to, $function);
	} else if (($currentpage > $window) && ($currentpage < $totalpages - $window)) {
		// Example: 1 2 ... 23 24 25 [26] 27 28 29 ... 51 52
		$paginationResult .= links($currentpage, 1, 2, $function) . ' ' . $dots;
		$paginationResult .= links($currentpage, $currentpage - $range, $currentpage + $range, $function);
		$paginationResult .= $dots . ' '. links($currentpage, $totalpages-1, $totalpages, $function);
	}
		
	// if not on last page, show forward link
	if ($currentpage != $totalpages) {
	   // get next page
	   $nextpage = $currentpage + 1;
	    // echo forward link for next page 
	   $paginationResult .= "<li class=\"next\"><a href=\"javascript:void(0);\" onclick=\"$function('$nextpage');return false;\">Next &gt;</a></li>";	   	  
	} // end if
	/****** end build pagination links ******/


?>
<ul class="pagination list-inline"><?= $paginationResult; ?></ul>
