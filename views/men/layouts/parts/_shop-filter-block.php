<span data-value="all" class="fl-l custom-border main-col-text <?=(empty($giftsCategories)) ? 'active':'';?>">all gifts</span>
<?php foreach ($allGiftsCategories as $category) { ?>
	<span data-value="<?= $category['id'] ?>" class="fl-l custom-border main-col-text <?=(in_array($category['id'], $giftsCategories)) ? 'active':'';?>"><?= $category['title'] ?></span>
<?php } ?>
