<?php 
    use app\controllers\BaseController;

    $session = Yii::$app->session;
    $pageName = $this->context->horizontalMenuName;
    $ladiesPageName = isset($session['user_data']['userType']) && $session['user_data']['userType'] == BaseController::USER_FEMALE ? 'Men' : 'Ladies' ;
?>
<div class="container hidden-xs">
    <div class="profile-menu">
        <div class="row">
            <ul>
                <li class="col-md-2 col-xs-3"><a href="/account/<?=$session['user_data']['userID']?>" <?php if ($pageName == "index") {echo 'class="active"';} ?>>Home</a></li>
                <li class="col-md-2 col-xs-3"><a href="/message/new" <?php if ($pageName == "message") {echo 'class="active"';}?>>Messages<span class="count pos-abs text-white rounded-block message">0</span></a></li>
                <li class="col-md-2 col-xs-3"><a href="/account/ladies" <?php if ($pageName == "ladies"){echo 'class="active"';}?>><?= $ladiesPageName;?></a></li>
                <li class="col-md-2 col-xs-3 live_chat"><a href="/account/chat" <?php if ($pageName == "chat") {echo 'class="active"';}?>>Live chat<span class="count pos-abs text-white rounded-block chat">0</span></a></li>
            </ul>
        </div>    
    </div>        
</div>