<?php
use app\controllers\BaseController;
use yii\helpers\HtmlPurifier;
use yii\helpers\Html;

$session = Yii::$app->session;
$user1_name = $user2_name = '';
if(!empty($lettersArray)){

    if ($session['user_id'] == $lettersArray[0]->from_user_id) {
        $anotherUserID = $lettersArray[0]->to_user_id;
        $anotherUserName = $lettersArray[0]->user_to_name;
        $anotherUserInFavorite = $lettersArray[0]->user_to_in_favorite;
        $anotherUserAvatar = !empty($lettersArray[0]->user_to_avatar) ? $this->context->serverUrl.'/'.$lettersArray[0]->user_to_avatar : '/img/no_avatar_normal.jpg';
    } else {
        $anotherUserID = $lettersArray[0]->from_user_id;
        $anotherUserName = $lettersArray[0]->from_user_name;
        $anotherUserInFavorite = $lettersArray[0]->user_from_in_favorite;
        $anotherUserAvatar = !empty($lettersArray[0]->from_user_avatar) ? $this->context->serverUrl.'/'.$lettersArray[0]->from_user_avatar : '/img/no_avatar_normal.jpg';
    }
}

$prefix = (isset($lettersArray[0]->user_to_user_type) && $lettersArray[0]->user_to_user_type == BaseController::USER_MALE) ? 'men' : 'girls';

if(isset($anotherUserInFavorite) && $anotherUserInFavorite == 1){
    $showStatus = 'liked';    
}else{
    $showStatus = '';    
}
?>

<div class="main sub-main">
    
    <div class="global-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <aside class="side-bar side-bar-left">
                        <?php echo $this->render('//layouts/parts/message-vertical-menu-man.php'); ?>                        
                    </aside>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="message-container">
                        <?php if(!empty($lettersArray)){
                            foreach($lettersArray as $letter){ ?>
                                <div class="history-message-item">
                                    <a href="/message/<?= $letter->id; ?>" class="history-message-item-link"></a>
                                    <div class="history-message-item-header clearfix">
                                        <div class="message-autor">
                                            <ul class="list-inline">
                                                <li>From: </li>
                                                <li>
                                                    <div class="message-autor-avatar" style="background-image: url('<?= !empty($letter->from_user_avatar) ? $this->context->serverUrl.'/'.$letter->from_user_avatar : '/img/no_avatar_normal.jpg'; ?>')"></div>
                                                </li>
                                                <li>
                                                    <span class="autor-name"><?=$letter->from_user_name?></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="income-date">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span><span class="time"><?= date("H:i",strtotime($letter->created_at)); ?></span> <span class="month"><?= date("d M",strtotime($letter->created_at)); ?></span>, <span class="year"><?= date("Y",strtotime($letter->created_at)); ?></span></span>
                                        </div>
                                    </div>

                                    <div class="history-message-item-body">
                                        <h5 class="message-theme"><?= Html::decode($letter->theme); ?></h5>
                                        <p class="message-content"><?= Html::decode($letter->description); ?></p>
                                    </div>
                                </div>
                            <?php }
                            } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

