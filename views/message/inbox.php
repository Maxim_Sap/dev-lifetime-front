<?php $ajax = Yii::$app->request->getIsAjax();
	use app\controllers\BaseController;
	$session = Yii::$app->session;
	$userType = isset($session['user_data']['userType']) && $session['user_data']['userType'] == BaseController::USER_FEMALE ? BaseController::USER_FEMALE : BaseController::USER_MALE;
	$this->title = "Inbox messages";
	if(!$ajax){
	?>
<?php } ?>

<div class="global-content-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-9 col-sm-push-3">
				<div class="messages-container">					
					<input id="limit" type="hidden" value="<?=$limit?>">					
					<h3 class="messages-title">My messages</h3>					
					<?php if(true){ ?>
						<div class="filter-line clearfix">
							<?php echo $this->render('//layouts/parts/message-filter.php',['page'=>$page]); ?>
						</div>

						<div class="messages-tabs">
							<ul class="list-inline tab-list">
								<li class="tab inbox active">Inbox <span class="item-count"><?= $inboxLettersCount ?></span></li>
								<li class="tab outbox">Sent <span class="item-count"><?= $outboxLettersCount ?></span></li>
								<li class="tab trash">Trash <span class="item-count"><?= $deletedLettersCount ?></span></li>
								<li class="tab notifications">notifications <span class="item-count"><?= $systemLettersCount ?></span></li>
							</ul>
							<div class="tab-panel-container">
								<div class="tab-panel inbox active">
									<div class="tab-panel-content">
										<?php if (isset($inboxLettersArray) && !empty($inboxLettersArray)) { ?>
										<div class="control-line clearfix">
											<?php echo $this->render('//layouts/parts/message_selector.php',['page'=>$page]); ?>
											<?php echo $this->render('//layouts/parts/message_pagination.php',['lettersCount'=>$inboxLettersCount,'page'=>$page, 'pageName' => 'inbox']); ?>
										</div>

										<div class="messages-box"> 											
										<?php
											$i = 1;
											$count = count($inboxLettersArray);
											foreach($inboxLettersArray as $letter){
												if ($i == $count) {
													$last = 'last';
												} else {
													$last = '';
												}
												echo $this->render('//layouts/parts/item-message-list.php',['letter'=>$letter, 'featuredIDs'=>$featuredIDs, 'black_list_ids'=>$blackListIDs, 'last' => $last]); 
												$i++;
											}  ?>

										</div>

										<div class="control-line clearfix">
											<?php echo $this->render('//layouts/parts/message_selector.php',['page'=>$page]); ?>
											<?php echo $this->render('//layouts/parts/message_pagination.php',['lettersCount'=>$inboxLettersCount,'page'=>$page, 'pageName' => 'inbox']); ?>
										</div>										
										<?php } else { ?>
											<div>You don't have incomming messages.</div>
										<?php } ?>
									</div>
								</div>
								<div class="tab-panel outbox">
									<div class="tab-panel-content">
										<?php if (isset($outboxLettersArray) && !empty($outboxLettersArray)) { ?>
										<div class="control-line clearfix">
											<?php echo $this->render('//layouts/parts/message_selector.php',['page'=>$page]); ?>
											<?php echo $this->render('//layouts/parts/message_pagination.php',['lettersCount'=>$outboxLettersCount,'page'=>$page, 'pageName' => 'outbox']); ?>
										</div>

										<div class="messages-box">											
										<?php
											$i = 1;
											$count = count($outboxLettersArray);
											foreach($outboxLettersArray as $letter){
												if ($i == $count) {
													$last = 'last';
												} else {
													$last = '';
												}
												echo $this->render('//layouts/parts/item-message-list.php',['letter'=>$letter, 'featuredIDs'=>$featuredIDs, 'black_list_ids'=>$blackListIDs, 'last' => $last]); 
												$i++;
											}  ?>

										</div>

										<div class="control-line clearfix">
											<?php echo $this->render('//layouts/parts/message_selector.php',['page'=>$page]); ?>
											<?php echo $this->render('//layouts/parts/message_pagination.php',['lettersCount'=>$outboxLettersCount,'page'=>$page, 'pageName' => 'outbox']); ?>
										</div>										
										<?php } else { ?>
											<div>You don't sent any message yet.</div>
										<?php } ?>
									</div>
								</div>
								<div class="tab-panel deleted">
									<div class="tab-panel-content">
										<?php if (isset($deletedLettersArray) && !empty($deletedLettersArray)) { ?>
										<div class="control-line clearfix">
											<?php echo $this->render('//layouts/parts/message_selector.php',['page'=>$page, 'pageName' => 'deleted']); ?>
											<?php echo $this->render('//layouts/parts/message_pagination.php',['lettersCount'=>$deletedLettersCount,'page'=>$page, 'pageName' => 'deleted']); ?>
										</div>

										<div class="messages-box">											
										<?php
											$i = 1;
											$count = count($deletedLettersArray);
											foreach($deletedLettersArray as $letter){
												if ($i == $count) {
													$last = 'last';
												} else {
													$last = '';
												}
												echo $this->render('//layouts/parts/item-message-list.php',['letter'=>$letter, 'featuredIDs'=>$featuredIDs, 'black_list_ids'=>$blackListIDs, 'last' => $last]); 
												$i++;
											}  ?>

										</div>

										<div class="control-line clearfix">
											<?php echo $this->render('//layouts/parts/message_selector.php',['page'=>$page, 'pageName' => 'deleted']); ?>
											<?php echo $this->render('//layouts/parts/message_pagination.php',['lettersCount'=>$deletedLettersCount,'page'=>$page, 'pageName' => 'deleted']); ?>
										</div>										
										<?php } else { ?>
											<div>You don't have any deleted message yet.</div>
										<?php } ?>
									</div>
								</div>
								<div class="tab-panel system">
									<div class="tab-panel-content">
										<?php if (isset($systemLettersArray) && !empty($systemLettersArray)) { ?>
										<div class="control-line clearfix">
											<?php echo $this->render('//layouts/parts/message_selector.php',['page'=>$page, 'pageName' => 'system']); ?>
											<?php echo $this->render('//layouts/parts/message_pagination.php',['lettersCount'=>$systemLettersCount,'page'=>$page, 'pageName' => 'system']); ?>
										</div>

										<div class="messages-box">											
										<?php
											$i = 1;
											$count = count($systemLettersArray);
											foreach($systemLettersArray as $letter){
												if ($i == $count) {
													$last = 'last';
												} else {
													$last = '';
												}
												echo $this->render('//layouts/parts/item-system-message-list.php',['message'=>$letter, 'last' => $last]); 
												$i++;
											}  ?>

										</div>

										<div class="control-line clearfix">
											<?php echo $this->render('//layouts/parts/message_selector.php',['page'=>$page, 'pageName' => 'system']); ?>
											<?php echo $this->render('//layouts/parts/message_pagination.php',['lettersCount'=>$systemLettersCount,'page'=>$page, 'pageName' => 'system']); ?>
										</div>										
										<?php } else { ?>
											<div>You don't have any notification messages yet.</div>
										<?php } ?>
									</div>
								</div>																
							</div>
						</div>
						<?php }else{ ?>
							<div class="filter-line clearfix">
							<?php echo $this->render('//layouts/parts/message-filter.php',['page'=>$page]);
							?>
							</div>
						<p>No results</p>
						<?php } ?>
						
				</div>
				<?php if ($userType == BaseController::USER_FEMALE) {?>					
				<div class="col-xs-12 col-sm-3" style="padding-top: 20px;">
					<a href="#mailing-list" class="btn mailing-list" data-toggle="modal" data-target="#mailing-list">My mailing list</a>
				</div>
				<?php } ?> 
			</div>
			<div class="col-xs-12 col-sm-3 col-sm-pull-9 hidden-xs">
				<aside class="side-bar side-bar-left">
					<?php echo $this->render('//layouts/parts/message-vertical-menu-man.php'); ?>
				</aside>
			</div>			
		</div>
	</div>

</div>

<?php echo $this->render('message_mailing.php'); ?>
<?php if(!$ajax){ ?>
<?php } ?>

<?php	if($ajax){ ?>   
		<script type="text/javascript">
/*			$('.select-messages .selectpicker').selectpicker('refresh');
			$('.selectpicker.favourites').selectpicker('refresh');
			$('.selectpicker.answered').selectpicker('refresh');*/
			$('.datepicker').datepicker();
			
			$('.select-all').on('click', function() {
				$(this).stop().toggleClass('checked');
				if (!$(".message .select-tap").not(".checked").length) {
					$('.message').removeClass('checked');
					$('.message .select-tap').removeClass('checked');
				}
				else {
					$('.message').not('.message.checked').addClass('checked');
					$('.message .select-tap').not('.message .select-tap.checked').addClass('checked');
				}
			});

			$('.message-option-list .check-all').on('click', function() {
				$('.message').not('.message.checked').addClass('checked');
				$('.message .select-tap').not('.message .select-tap.checked').addClass('checked');
			});

			$('.message-option-list .uncheck-all').on('click', function() {
				$('.message').removeClass('checked');
				$('.message .select-tap').removeClass('checked');
			});
		$('.message-option-list .check-new').on('click', function() {
			$('.message').removeClass('checked');
			$('.message .select-tap').removeClass('checked');
			$('.message.unread .select-tap').addClass('checked');
		});
		$('.message-option-list .check-answered').on('click', function() {
			$('.message').removeClass('checked');
			$('.message .select-tap').removeClass('checked');
			$('.message.answered .select-tap').addClass('checked');
		});




			$('.message .select-tap').on('click', function() {
				$(this).stop().toggleClass('checked');
				$(this).parents('.message').stop().toggleClass('checked');
			});

			$('.message .select-tap').on('click', function() {
				if (!$('.message .select-tap').hasClass("checked")) {
					$('.select-all').removeClass('checked');
				}
				if (!$(".message .select-tap").not(".checked").length) {
					$('.select-all').addClass('checked');
				}
			});


			


			$('.type-list-box .select-type').on('click', function() {
				$(this).parent().find('.type-list').stop().fadeToggle(300);
			});

			$(document).on('click touchstart', function (event) {
				if (!$(event.target).closest('.select-type, .type-list').length) {
					$('.type-list').fadeOut(300);
				}
			});

			if($('.datepicker').length) {
				$('.datepicker').datepicker();
			}
			

			$('.date-button').on('click', function() {
				$('.datepicker-wrapper').stop().fadeToggle(300);
			});

			$(document).on('click touchstart', function (event) {
				if (!$(event.target).closest('.date-button, .datepicker-wrapper, .ui-datepicker-prev, .ui-datepicker-next').length) {
					$('.datepicker-wrapper').fadeOut(300);
				}
			});

			$('.filter-list-button').on('click', function() {
				$('.filters-list').stop().fadeToggle(300);
			});

			$(document).on('click touchstart', function (event) {
				if (!$(event.target).closest('.filter-list-button, .filters-list').length) {
					$('.filters-list').fadeOut(300);
				}
			});


			$('.theme-message-conent').prepend('<span> - </span>');
		</script>
	<?php } ?>