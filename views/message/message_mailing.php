<?php

use app\controllers\BaseController;
$session = Yii::$app->session;
$userType = isset($session['user_data']['userType']) && $session['user_data']['userType'] == BaseController::USER_FEMALE ? BaseController::USER_FEMALE : BaseController::USER_MALE;

?>
<?php if ($userType == BaseController::USER_FEMALE) {?>        
        <div class="modal fade fullscreen-modal" id="mailing-list" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span></span>
              <span></span>
            </button>
            <div class="modal-content">
              <div class="modal-header">
                <div class="modal-header-logo rounded-block">
                  <img src="/img/logo.png" alt="Logo">
                </div>
              </div>
              <div class="modal-body">
                <div class="mailing-container">
                  <div class="container">
                    <div class="row">
                      <div class="col-xs-12">
                        <h2 class="mailing-title">My mailing list</h2>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-12 col-sm-7">
                        <div class="left-side">
                          <div class="mailing-tabs">
                            <ul class="list-inline tab-list">
                              <li class="tab active">
                               My Templates 
                             </li>
                             <li class="tab">
                              New Template
                            </li>
                          </ul>
                          <div class="tab-panel-container">
                            <div class="tab-panel active">
                              <div class="tab-panel-content">
                                <form class="existing-template-form">
                                  <select name="templates" id="mailing-templates">
                                    <option value="Template 1">Template 1</option>
                                    <option value="Template 2">Template 2</option>
                                    <option value="Template 3">Template 3</option>
                                    <option value="Template 4">Template 4</option>
                                  </select>
                                  <textarea name="letter-text" id="" cols="" rows="" placeholder="Letter text"></textarea>
                                  <div class="checkbox-group">
                                    <input type="checkbox" name="black-exclude" id="black-exclude">
                                    <span class="theme-checkbox"></span>
                                    <label for="black-exclude">except black letter</label>
                                  </div>
                                  <button type="submit" class="btn">start a newsletter</button>
                                </form>
                              </div>
                            </div>
                            <div class="tab-panel">
                              <div class="tab-panel-content">
                                <form class="new-mailing-template-form">
                                  <input type="text" name="template-name" placeholder="Template name">
                                  <textarea name="letter-text" placeholder="Template text"></textarea>
                                  <p>Теги, которые можно использовать:</p>
                                  <p>%username% - имя получателя</p>
                                  <button type="submit" class="btn add-template">Add Template</button>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                      <div class="right-side">
                        <div class="mailing-proggress">
                          <h4 class="mailing-proggress-title">mailing progress</h4>
                          <div class="mailing-proggress-info-item">
                            <span class="title">Begin:</span>
                            <span class="description"><span>12-10-2017</span> <span>11:45</span></span>
                          </div>
                          <div class="mailing-proggress-info-item">
                            <span class="title">Received:</span>
                            <span class="description"><span>45</span> of <span>185</span> people</span>
                          </div>
                          <a href="javascript:void(0);" class="btn play-pause">Pause / Resume</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">

            </div>
          </div>
        </div>
      </div>
<?php  

$this->registerJsFile(
    '@web/js/messageTemplates.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

} ?>