<?php
	$createdAt = (isset($lettersData->created_at) ? $lettersData->created_at : null);
	$createdAt = strtotime($createdAt);
	$formatDateCreate = date("H:i d M, Y", $createdAt);
?>

<div style="width: 80%;margin: 25px auto;">
	<h1 style="text-align: center;">Letter from <?=$lettersData->another_user_name;?></h1>
	<div class="left-block" style="float: left;">
		<img style="width: 200px;" src="<?=$lettersData->another_user_avatar;?>">
	</div>
	<div class="right-block" style="float: left;padding-left: 17px;box-sizing: border-box;">
		<div class="user_name" style="font-size: 24px;line-height: 1.4;">User name: <?=$lettersData->another_user_name;?></div>
		<?php if (!empty($lettersData->another_user_id)) { ?> <div class="user_id" style="font-size: 24px;line-height: 1.4;">User ID: <?=$lettersData->another_user_id;?></div> <?php } ?>
		<div class="date_send" style="font-size: 24px;line-height: 1.4;">Date send: <?=$formatDateCreate;?></div>
	</div>
	<div style="clear: both;"></div>
	<hr>
	<h2 style="text-align: center;">Letter content</h2>
	<div>   
		<div class="caption">Caption: <span style="font-size: 20px;font-weight: bold;"><?php if (!empty($lettersData->theme)) {echo $lettersData->theme;} elseif (!empty($lettersData->title)) {echo $lettersData->title;} else {echo 'Not defined'; } ?></span></div>
		<br/>
		Description:
		<div class="description" style="font-size: 20px;">
			<?=$lettersData->description;?>
		</div>
	</div>
	<div style="padding: 10px 0 0 0;">
		<p style="text-align: center">© <?= date('Y') ?>. All rights reserved <a href="/">UKRAINIANBRIDES.COM</a></p>
	</div>
</div>    
