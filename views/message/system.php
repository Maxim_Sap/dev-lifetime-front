<?php $ajax = Yii::$app->request->getIsAjax();
	if(!$ajax){
	?>
<div class="main sub-main">	
	<div class="global-content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-3">
					<aside class="side-bar side-bar-left">					
						<?php echo $this->render('//layouts/parts/message-vertical-menu.php'); ?>
					</aside>
				</div>
				<div class="col-xs-12 col-sm-9">
					<input id="limit" type="hidden" value="<?=$limit?>">
					<div class="message-container">					
					<?php }
					if(isset($messageArray) && !empty($messageArray)){ ?>
						<div class="control-line clearfix">
							
							<?php echo $this->render('//layouts/parts/message_selector.php'); 
								  echo $this->render('//layouts/parts/message_pagination.php',['lettersCount'=>$messageCount,'page'=>$page]);
							?>

						</div>
						<?php echo $this->render('//layouts/parts/mobile_message_pagination.php',['lettersCount'=>$messageCount,'page'=>$page]); ?>
						<div class="messages-wrapper">							
							<?php
							$i = 1;
							$count = count($messageArray);
							foreach($messageArray as $message){
								if ($i == $count) {
									$last = 'last';
								} else {
									$last = '';
								}
								echo $this->render('//layouts/parts/item-system-message-list.php',['message'=>$message, 'featuredIDs'=>0, 'black_list_ids'=>0, 'last' => $last]); 
								$i++;
							}  ?>
						</div>
						<?php echo $this->render('//layouts/parts/mobile_message_pagination.php',['lettersCount'=>$messageCount,'page'=>$page]); ?>
						<?php }else{ ?>
						<p>No results</p>
						<?php }
						if(!$ajax){ ?> 
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<?php } ?>
