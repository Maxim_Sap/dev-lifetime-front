<?php $ajax = Yii::$app->request->getIsAjax();
	use app\controllers\BaseController;
	$session = Yii::$app->session;
	$userType = isset($session['user_data']['userType']) && $session['user_data']['userType'] == BaseController::USER_FEMALE ? BaseController::USER_FEMALE : BaseController::USER_MALE;
	$this->title = "Inbox messages"; ?>

<?php if (isset($systemLettersArray) && !empty($systemLettersArray)) { ?>
<div class="control-line clearfix">
	<?php echo $this->render('//layouts/parts/message_selector.php',['page'=>$page]); ?>
	<?php echo $this->render('//layouts/parts/message_pagination.php',['lettersCount'=>$systemLettersCount,'page'=>$page, 'pageName' => 'system']); ?>
</div>

<div class="messages-box">											
<?php
	$i = 1;
	$count = count($systemLettersArray);
	foreach($systemLettersArray as $letter){
		if ($i == $count) {
			$last = 'last';
		} else {
			$last = '';
		}
		echo $this->render('//layouts/parts/item-system-message-list.php',['message'=>$letter, 'last' => $last]); 
		$i++;
	}  ?>

</div>

<div class="control-line clearfix">
	<?php echo $this->render('//layouts/parts/message_selector.php',['page'=>$page]); ?>
	<?php echo $this->render('//layouts/parts/message_pagination.php',['lettersCount'=>$systemLettersCount,'page'=>$page, 'pageName' => 'system']); ?>
</div>										
<?php } else { ?>
	<div>No results.</div>
<?php } ?>

		<script type="text/javascript">
			$('.datepicker').datepicker();
			
			$('.tab-panel.active .select-all').on('click', function() {
				$('.tab-panel.active .select-all').stop().toggleClass('checked');
				if (!$(".tab-panel.active .message .select-tap").not(".checked").length) {
					$('.tab-panel.active .message').removeClass('checked');
					$('.tab-panel.active .message .select-tap').removeClass('checked');
				}
				else {
					$('.tab-panel.active .message').not('.message.checked').addClass('checked');
					$('.tab-panel.active .message .select-tap').not('.message .select-tap.checked').addClass('checked');
				}
			});

			$('.tab-panel.active .message-option-list .check-all').on('click', function() {
				$('.tab-panel.active .select-all').addClass('checked');
				$('.tab-panel.active .message').not('.message.checked').addClass('checked');
				$('.tab-panel.active .message .select-tap').not('.message .select-tap.checked').addClass('checked');
			});

			$('.tab-panel.active .message-option-list .uncheck-all').on('click', function() {
				$('.tab-panel.active .select-all').removeClass('checked');
				$('.tab-panel.active .message').removeClass('checked');
				$('.tab-panel.active .message .select-tap').removeClass('checked');
			});
		$('.tab-panel.active .message-option-list .check-new').on('click', function() {
			$('.tab-panel.active .message').removeClass('checked');
			$('.tab-panel.active .message .select-tap').removeClass('checked');
			$('.tab-panel.active .message.unread .select-tap').addClass('checked');
		});
		$('.tab-panel.active .message-option-list .check-answered').on('click', function() {
			$('.tab-panel.active .message').removeClass('checked');
			$('.tab-panel.active .message .select-tap').removeClass('checked');
			$('.tab-panel.active .message.answered .select-tap').addClass('checked');
		});
		$('.tab-panel.active .message .select-tap').on('click', function() {
			$(this).stop().toggleClass('checked');
			$(this).parents('.message').stop().toggleClass('checked');
		});

		$('.tab-panel.active .message .select-tap').on('click', function() {
			if (!$('.tab-panel.active .message .select-tap').hasClass("checked")) {
				$('.select-all').removeClass('checked');
			}
			if (!$(".tab-panel.active .message .select-tap").not(".checked").length) {
				$('.select-all').addClass('checked');
			}
		});
		$('.type-list-box .select-type').on('click', function() {
			$(this).parent().find('.type-list').stop().fadeToggle(300);
		});

		$(document).on('click touchstart', function (event) {
			if (!$(event.target).closest('.select-type, .type-list').length) {
				$('.type-list').fadeOut(300);
			}
		});

		if($('.datepicker').length) {
			$('.datepicker').datepicker();
		}
		

		$('.date-button').on('click', function() {
			$('.datepicker-wrapper').stop().fadeToggle(300);
		});

		$(document).on('click touchstart', function (event) {
			if (!$(event.target).closest('.date-button, .datepicker-wrapper, .ui-datepicker-prev, .ui-datepicker-next').length) {
				$('.datepicker-wrapper').fadeOut(300);
			}
		});

		$('.filter-list-button').on('click', function() {
			$('.filters-list').stop().fadeToggle(300);
		});

		$(document).on('click touchstart', function (event) {
			if (!$(event.target).closest('.filter-list-button, .filters-list').length) {
				$('.filters-list').fadeOut(300);
			}
		});


		$('.theme-message-conent').prepend('<span> - </span>');
	</script>
