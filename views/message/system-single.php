<?php
	$session = Yii::$app->session;
    //var_dump($messageArray); die;
	$createdAt = strtotime($messageArray->created_at);
	$formatDateCreate = date("H:i d M, Y",$createdAt);
//	var_dump($letters_data);
?>
<div class="main sub-main">
    <div class="global-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <aside class="side-bar side-bar-left">
                        
                        <?php echo $this->render('//layouts/parts/message-vertical-menu-man.php'); ?>
                        
                    </aside>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="message-container">

                        <div class="message-detail">
                            
                            <div class="content-area">

                                <div class="content-area-header clearfix">

                                    <div class="message-autor-info" data-user-id="<?=$messageArray->message_receiver;?>">
                                        <div class="autor-avatar" style="background-image: url('<?= $anotherUserAvatar; ?>')"></div>
                                        <div class="autor-name"><?= $messageArray->another_user_name; ?></div>
                                    </div>

                                    <div class="reply-top">
                                        <ul class="reply-top-list list-inline">
                                            <?php if(false && $messageArray->message_creator != $session['user_id']){?>
                                            <li>
                                                <a href="javascript:void(0);" onclick="$('.send_msg button').click();" class="reply-active">
                                                    <i class="fa fa-reply" aria-hidden="true" title="Reply message"></i>
                                                </a>
                                            </li>
                                            <?php } ?>
                                            <li>
                                                <a href="javascript:void(0);" class="reply-more" title="More options">
                                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                </a>
                                                <ul class="control-list list-inline">
                                                    <li>
                                                        <a href="javascript:void(0);" onclick="window['messageLibrary']['sendMessageToEmail'](<?=$messageArray->id;?>, '/message/system/');" class="send-my-mail"><i class="fa fa-reply" aria-hidden="true"></i>Send on my email</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);" onclick="window['messageLibrary']['deleteSystemMessage'](<?=$messageArray->id;?>);" class="delete-message"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
                                                    </li>
                                                    <li>
                                                        <a target="_blank" href="/message/system-print-vesion/<?=$messageArray->id;?>" class="print"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="income-date">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <span><span class="time"><?= date("H:i", $createdAt); ?></span> <span class="month"><?= date("d M", $createdAt); ?></span>, <span class="year"><?= date("Y", $createdAt); ?></span></span>
                                    </div>
                                </div>
                                
                                <div class="content-area-body">
                                    <div class="content-area-body-top">
                                        <div class="message-theme"><?=$messageArray->title;?></div>
                                    </div>
                                    <div class="content-area-body-center">
                                        <div class="message-content">
                                            <?=$messageArray->description;?>
                                        </div>
                                    </div>
                                    <div class="content-area-body-bottom">
                                        <ul class="prev-next-list list-inline">
                                            <?php  if(!empty($next_prev->prev)){?>
                                                <li>
                                                    <a href="/message/system/<?=$next_prev->prev;?>" class="prev-message"><i class="fa fa-angle-left" aria-hidden="true"></i> Previous message</a>
                                                </li>
                                            <?php } else { ?>
                                                <li></li>
                                            <?php } 
                                            if(!empty($next_prev->next)){?>
                                                <li>
                                                    <a href="/message/system/<?=$next_prev->next;?>" class="next-message">Next message <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                                </li>
                                            <?php }?>                                            
                                        </ul>
                                        
                                        <ul class="control-list list-inline">
                                            <li>
                                                <a href="javascript:void(0);" onclick="window['messageLibrary']['sendMessageToEmail'](<?=$messageArray->id;?>, '/message/system/');" class="send-my-mail"><i class="fa fa-reply" aria-hidden="true"></i>Send on my email</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" onclick="window['messageLibrary']['deleteSystemMessage'](<?=$messageArray->id;?>);" class="delete-message"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
                                            </li>
                                            <li>
                                                <a target="_blank" href="/message/system-print-vesion/<?=$messageArray->id;?>" class="print"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                                            </li>                                            
                                        </ul>

                                    </div>
                                </div>
                                <?php if(false && $messageArray->message_creator != $session['user_id']){?>
                                <div class="content-area-footer reply_message">
                                    <input id="letters_caption" type="hidden" value="<?=$messageArray->title;?>">
                                    <input class="previous_letters_id" type="hidden" value="<?=$messageArray->id;?>">
                                    <textarea></textarea>
                                    <div class="reply-box">                                     
                                        <div class="send_msg">
                                        <button data-type="/v1/message/send-message" class="btn">Send</button>
                                        </div>
                                    </div>

                                    <div class="attach_foto_block">   
                                        <h4>Attach your private photo</h4>
                                        <div id="in_uploaded_img" style="display:none;">0</div>
                                        <div id="message-attach-photo"></div>
                                        <div class="row uploader" style="position: relative;">
                                            <a style="display: none" href="javascript:void(0);" id="save_order">save</a>
                                            <div class="col-md-12">
                                                <div class="drag_and_drop_area drag-and-drop-area inner-padding">
                                                    
                                                    <a href="javascript:void(0);" class="main-col-text">Click here or drag&amp;drop photos to upload them to your message</a>
                                                    
                                                    <div class="cab_main_photo" id="div_foto_add">
                                                        <span for="choose" id="link" class="choose"></span>
                                                        <form id="chooseform" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="MAX_FILE_SIZE" value="12345"/>
                                                            <input type="file" name="file" multiple accept="image/*" id="choose" data-album-title="letters" data-place="letters" style="display: none;" />
                                                        </form>
                                                    </div>
                                                    <span>Select a file in this format: gif, jpg, jpeg, png</span>
                                                    <span>Should not exceed 2 Mb!</span>
                                                </div>
                                                <div class="private-note">
                                                    Private gallery photos are not available for public viewing on the website. Thus you can use the private gallery for storing your special photos. You can send a photo 
                                                    from this gallery to ladies you want. They will be seen only by ladies you have chosen. We maintain the confidentiality of your information and not share it!
                                                </div>
                                            </div>  
                                            <span style="height: 183px;    position: absolute;    top: 0px;    width: 100%;    left: 0;" class="drop_here" data-album-title="letters" data-place="letters"></span>
                                            <div class="errors"></div>
                                            <div id="status1"></div>
                                        </div> 
                                    </div>                                  

                                </div>
                                <?php } ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
