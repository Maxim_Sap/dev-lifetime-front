<?php
use app\controllers\BaseController;
//var_dump($otherUserInfo); die;
$labelNew = (time() - strtotime($otherUserInfo->created_at) < 60*60*24*30) ? true : false;
$online = (time() - $otherUserInfo->last_activity <= 30) ? true : false;
if (!empty($otherUserInfo->personalInfo->birthday)) {
    $tz  = new \DateTimeZone('Europe/Brussels');
    $age = \DateTime::createFromFormat('Y-m-d', $otherUserInfo->personalInfo->birthday, $tz)
     ->diff(new \DateTime('now', $tz))
     ->y;    
} else {
    $age = 'Not defined';
}
$otherUserAvatar = !empty($otherUserInfo->avatar->normal) ? $this->context->serverUrl."/".$otherUserInfo->avatar->normal : '/img/no_avatar_normal.jpg';
if(isset($otherUserInfo->in_favorite) && $otherUserInfo->in_favorite == 1) {
	$showStatus = 'liked';	
}else{
	$showStatus = '';	
}

$prefix = (isset($userType) && $userType == BaseController::USER_FEMALE) ? 'men' : 'girls';

?>
<style>
	#letters_caption {
		height: 40px;
    line-height: 1;
    color: #313131;
	}
</style>
<div class="main sub-main">
	
	<div class="global-content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-3">
					<aside class="side-bar side-bar-left">
						
						<?php echo $this->render('//layouts/parts/message-vertical-menu-man.php'); ?>
						
					</aside>
				</div>
				<div class="col-xs-12 col-sm-9">
					<div class="message-container">

						<div class="message-detail">
							
							<div class="content-area">

								<div class="content-area-header clearfix">

									<div class="message-autor-info" data-user-id="<?=$otherUserInfo->personalInfo->id;?>">
										<div class="autor-avatar" style="background-image: url('<?= $otherUserAvatar; ?>')"></div>
										<div class="autor-name"><?= $otherUserInfo->personalInfo->first_name; ?></div>
									</div>

								</div>
								
								<div class="new-message-box">
								 	<div class="new-message-box-header">
										<span class="new-message-title">New message</span>
									</div>
								</div>
								<div class="content-area-footer reply_message">
									<div class="new-message-box">
					                	<div class="new-message-box-body">
					                	<div class="new-message-theme-form"><div class="theme-line"><input id="letters_caption" type="text" value="" placeholder="Letter theme" class="input-theme-text" required/></div></div>
					                	</div>
					               	</div>
					                <input class="theme_id" type="hidden"/>
					                <input class="previous_letters_id" type="hidden"/>
					                <textarea></textarea>
									<div class="reply-box">										
										<div class="send_msg">
										<button data-type="/v1/letter/send-letter" class="btn">Send</button>
										</div>
									</div>

					                <div class="attach_foto_block">   
					                    <h4>Attach your private photo</h4>
										<div id="in_uploaded_img" style="display:none;">0</div>
										<div id="message-attach-photo"></div>
					                    <div class="row uploader" style="position: relative;">
					                    	<a style="display: none" href="javascript:void(0);" id="save_order">save</a>
					                        <div class="col-md-12">
					                            <div class="drag_and_drop_area drag-and-drop-area inner-padding">
					                                
					                                <a href="javascript:void(0);" class="main-col-text">Click here or drag&amp;drop photos to upload them to your message</a>
					                                
					                                <div class="cab_main_photo" id="div_foto_add">
										                <span for="choose" id="link" class="choose"></span>
										                <form id="chooseform" method="post" enctype="multipart/form-data">
											                <input type="hidden" name="MAX_FILE_SIZE" value="12345"/>
											                <input type="file" name="file" multiple accept="image/*" id="choose" data-album-title="letters" data-place="letters" style="display: none;" />
										                </form>
										            </div>
					                                <span>Select a file in this format: gif, jpg, jpeg, png</span>
					                                <span>Should not exceed 2 Mb!</span>
					                            </div>
					                            <div class="private-note">
					                                Private gallery photos are not available for public viewing on the website. Thus you can use the private gallery for storing your special photos. You can send a photo 
					                                from this gallery to ladies you want. They will be seen only by ladies you have chosen. We maintain the confidentiality of your information and not share it!
					                            </div>
					                        </div>  
					                        <span style="height: 183px;    position: absolute;    top: 0px;    width: 100%;    left: 0;" class="drop_here" data-album-title="letters" data-place="letters"></span>
										    <div class="errors"></div>
										    <div id="status1"></div>
					                    </div> 
					                </div>									

								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

</div>
