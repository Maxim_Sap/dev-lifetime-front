<?php $ajax = Yii::$app->request->getIsAjax();
	if(!$ajax){
	?>
<div class="main sub-main">	
	<div class="global-content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-3">
					<aside class="side-bar side-bar-left">					
						<?php echo $this->render('//layouts/parts/message-vertical-menu.php'); ?>
					</aside>
				</div>
				<div class="col-xs-12 col-sm-9">
					<input id="limit" type="hidden" value="<?=$limit?>">
					<div class="message-container">					
					<?php }
					if(isset($lettersArray) && !empty($lettersArray)){ ?>
						<div class="control-line clearfix">
							
							<?php echo $this->render('//layouts/parts/message_selector.php'); 
								  echo $this->render('//layouts/parts/message_pagination.php',['lettersCount'=>$lettersCount,'page'=>$page]);
							?>

						</div>
						<div class="filter-line clearfix">
							<?php echo $this->render('//layouts/parts/message-filter.php',['page'=>$page]); ?>
						</div>
						<?php echo $this->render('//layouts/parts/mobile_message_pagination.php',['lettersCount'=>$lettersCount,'page'=>$page]); ?>
						<div class="messages-wrapper">							
							<?php
							$i = 1;
							$count = count($lettersArray);
							foreach($lettersArray as $letter){
								if ($i == $count) {
									$last = 'last';
								} else {
									$last = '';
								}
								echo $this->render('//layouts/parts/item-message-list.php',['letter'=>$letter, 'featuredIDs'=>$featuredIDs, 'black_list_ids'=>$blackListIDs, 'last' => $last]); 
								$i++;
							}  ?>
						</div>
						<?php echo $this->render('//layouts/parts/mobile_message_pagination.php',['lettersCount'=>$lettersCount,'page'=>$page]); ?>
						<?php }else{ ?>
							<div class="filter-line clearfix">
							<?php echo $this->render('//layouts/parts/message-filter.php',['page'=>$page]);
							?>
							</div>
						<p>No results</p>
						<?php }
						if(!$ajax){ ?> 
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<?php } ?>

<?php	if($ajax){ ?>   
		<script type="text/javascript">
/*			$('.select-messages .selectpicker').selectpicker('refresh');
			$('.selectpicker.favourites').selectpicker('refresh');
			$('.selectpicker.answered').selectpicker('refresh');*/
			$('.datepicker').datepicker();
			$('.select-all').on('click', function() {
				$(this).stop().toggleClass('checked');
				if (!$(".message .select-tap").not(".checked").length) {
					$('.message').removeClass('checked');
					$('.message .select-tap').removeClass('checked');
				}
				else {
					$('.message').not('.message.checked').addClass('checked');
					$('.message .select-tap').not('.message .select-tap.checked').addClass('checked');
				}
			});

			$('.message-option-list .check-all').on('click', function() {
				$('.message').not('.message.checked').addClass('checked');
				$('.message .select-tap').not('.message .select-tap.checked').addClass('checked');
			});

			$('.message-option-list .uncheck-all').on('click', function() {
				$('.message').removeClass('checked');
				$('.message .select-tap').removeClass('checked');
			});
			$('.message-option-list .check-new').on('click', function() {
				$('.message').removeClass('checked');
				$('.message .select-tap').removeClass('checked');
				$('.message.unread .select-tap').addClass('checked');
			});
			$('.message-option-list .check-answered').on('click', function() {
				$('.message').removeClass('checked');
				$('.message .select-tap').removeClass('checked');
				$('.message.answered .select-tap').addClass('checked');
			});




			$('.message .select-tap').on('click', function() {
				$(this).stop().toggleClass('checked');
				$(this).parents('.message').stop().toggleClass('checked');
			});

			$('.message .select-tap').on('click', function() {
				if (!$('.message .select-tap').hasClass("checked")) {
					$('.select-all').removeClass('checked');
				}
				if (!$(".message .select-tap").not(".checked").length) {
					$('.select-all').addClass('checked');
				}
			});


			


			$('.type-list-box .select-type').on('click', function() {
				$(this).parent().find('.type-list').stop().fadeToggle(300);
			});

			$(document).on('click touchstart', function (event) {
				if (!$(event.target).closest('.select-type, .type-list').length) {
					$('.type-list').fadeOut(300);
				}
			});

			if($('.datepicker').length) {
				$('.datepicker').datepicker();
			}
			

			$('.date-button').on('click', function() {
				$('.datepicker-wrapper').stop().fadeToggle(300);
			});

			$(document).on('click touchstart', function (event) {
				if (!$(event.target).closest('.date-button, .datepicker-wrapper, .ui-datepicker-prev, .ui-datepicker-next').length) {
					$('.datepicker-wrapper').fadeOut(300);
				}
			});

			$('.filter-list-button').on('click', function() {
				$('.filters-list').stop().fadeToggle(300);
			});

			$(document).on('click touchstart', function (event) {
				if (!$(event.target).closest('.filter-list-button, .filters-list').length) {
					$('.filters-list').fadeOut(300);
				}
			});


			$('.theme-message-conent').prepend('<span> - </span>');
		</script>
	<?php } ?>