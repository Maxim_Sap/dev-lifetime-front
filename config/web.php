<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id'         => 'basic',
    'basePath'   => dirname(__DIR__),
    'modules' => [
		'manager' => [
			'class' => 'app\modules\manager\Module',
			'layout' => 'manager.php',
			'defaultRoute' => 'default/index',
		],
	],
    'bootstrap'  => ['log'],
    'components' => [
        'session' => [
            'class' => 'yii\web\Session',
            'timeout' => 3*60*60,
            'useCookies' => true,
        ],
        'urlManager'   => require(__DIR__ . '/urlManager.php'), 
        'image'        => array(  //image resizer
                                  'class'  => 'yii\image\ImageDriver',
                                  'driver' => 'Imagick',  //GD or Imagick
        ),
        'request'      => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'TnvaQLL3pCqktHEdIZv8GuhoH0Dpjdub',
            'parsers'             => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'cache'        => [
            'class' => 'yii\caching\FileCache',
        ],
        'user'         => [
            'identityClass'   => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => null
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'mailer' => [
            'class'     => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => 'server10.shneider-host.ru',
                'username'   => 'contact@web-synthesis.ru',
                'password'   => 'qwerty123',
                'port'       => '587',
                'encryption' => 'tls',
            ],
        ],
        'log'    => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db'     => require(__DIR__ . '/db.php'),

        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,   // do not publish the bundle
                    'js' => [
                        '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
                    ]
                ],
            ],
        ],
    ],
    'controllerMap' => [
        'elfinder' => [
            //'class' => 'mihaildev\elfinder\PathController',
            'class' => 'mihaildev\elfinder\Controller',
            'access' => ['@'],
            'plugin' => [
                [
                    'class'=>'\mihaildev\elfinder\plugin\Sluggable',
                    'lowercase' => true,
                    'replacement' => '-'
                ]
            ],
            'roots' => [
                [
                    'baseUrl'=>'@web',
                    'basePath'=>'@webroot',
                    'path' => '/source/media',
                    'name' => 'Media storage',
                    'plugin' => [
                        'Sluggable' => [
                            'lowercase' => false,
                        ]
                    ]
                ],
            ]
        ]
    ],
    'params'     => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][]      = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][]    = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
