<?php



return [

    'enablePrettyUrl' => TRUE,

    'showScriptName'  => FALSE,

    'rules'           => [

        'site/payments/<userID:\d+>'                         => 'site/payments',

        'account/<userID:\d+>'                               => 'account',

        'account/edit/<userID:\d+>'                          => 'account/edit',

        'blog/<postId:\d+>'                                  => 'blog',        

        'about'                                              => 'site/about',

        'contact'                                            => 'site/contact',

        'site-rules'                                         => 'site/site-rules',

        'license-agreement'                                  => 'site/license-agreement',

        'privacy-policy'                                     => 'site/privacy-policy',

        'subscribe'                                          => 'site/subscribe',
        'messages'                                           => 'message/index',

        'message/<messageID:\d+>'                            => 'message/single',

        'newmessage/<otherUserID:\d+>'                       => 'message/newmessage',

        'message/system/<messageID:\d+>'                     => 'message/system-single',

        'message/print/<lettersID:\d+>'              => 'message/print-version',

        'message/history/<letterID:\d+>'                   => 'message/history',

        'message/system-print-vesion/<messageID:\d+>'       => 'message/system-print-vesion',

        'men/<otherUserID:\d+>'                              => 'men/single',

        'men/<otherUserID:\d+>/gallery'                      => 'men/gallery',

        'men/<otherUserID:\d+>/album/<albumID:\d+>'         => 'men/album',

        'men/<otherUserID:\d+>/video-gallery'                => 'men/video-gallery',

        'men/<otherUserID:\d+>/video/<videoID:\d+>'         => 'men/single-video',

        'message/send-to-email/<letters_id:\d+>'             => 'message/send-to-email',

        'account/album/<albumID:\d+>'                        => 'account/album',

        'account/photo/<photoID:\d+>'                        => 'account/photo',

        'account/photo-delete/<photoID:\d+>'                => 'account/photo-delete',
        'girls'                                             => 'girls/index',

        'girls/<otherUserID:\d+>'                            => 'girls/single',

        'girls/<otherUserID:\d+>/gallery'                    => 'girls/gallery',

        'girls/<otherUserID:\d+>/album/<album_id:\d+>'       => 'girls/album',

        'girls/<otherUserID:\d+>/video-gallery'              => 'girls/video-gallery',

        'girls/<otherUserID:\d+>/video/<video_id:\d+>'       => 'girls/single-video',

        'account/chat/<otherUserID:\d+>'                     => 'account/chat',

        'account/my-gifts-detail/<giftID:\d+>'              => 'account/my-gifts-detail',

        'account/<action>'                            => 'account/<action>',

        'account/set-video-cam-status/<video_status>'        => 'account/set-video-cam-status',

        'account/shop/<otherUserID:\d+>'                     => 'account/shop',

        'account/cart/<otherUserID:\d+>'                     => 'account/cart',

        'account/delete-form-blacklist/<otherUserID:\d+>'    => 'account/delete-form-blacklist',

        'account/my-gifts-detail/<item_id:\d+>'              => 'account/my-gifts-detail',

        //module админка

        'manager/album/<albumID:\d+>/<otherUserID:\d+>'      => 'manager/default/album',

        'manager/photo/<photoID:\d+>/<otherUserID:\d+>'      => 'manager/default/photo',

        'manager/delete-price-item/<pricelistID:\d+>'        => 'manager/default/delete-price-item',

        'manager/delete-agency-price-item/<pricelistID:\d+>' => 'manager/default/delete-agency-price-item',

        'manager/photo-delete/<photoID:\d+>'                 => 'manager/default/photo-delete',

        'manager/letters/history/<letterID:\d+>'            => 'manager/letters/history',
        'manager/letter/<letterID:\d+>'                     => 'manager/letters/answer',

        'manager/letters/<action:[\w\-]+>/<otherUserID:\d+>' => 'manager/letters/<action>',

        'manager/finance/add-agency-payments/<orderID:\d+>'           => 'manager/finance/add-agency-payments',

        'manager/finance/transfer-agency-payments/<orderID:\d+>'      => 'manager/finance/transfer-agency-payments',

        'manager/finance/<action:[\w\-]+>/<otherUserID:\d+>' => 'manager/finance/<action>',

        'manager/message/new/<otherUserID:\d+>'              => 'manager/message/new',

        'manager/message/<action:[\w\-]+>/<messageID:\d+>'   => 'manager/message/<action>',

        'manager/edit-gift/<giftID:\d+>'                     => 'manager/default/edit-gift',

        'manager/delete-gift/<giftID:\d+>'                   => 'manager/default/delete-gift',

        'manager/edit-shop-item/<actionID:\d+>'              => 'manager/default/edit-shop-item',

        'manager/view-shop-item/<actionID:\d+>'              => 'manager/default/view-shop-item',

        'manager/penalty-edit/<penaltyID:\d+>'               => 'manager/default/penalty-edit',

        'manager/penalty-delete/<penaltyID:\d+>'             => 'manager/default/penalty-delete',

        'manager/rule-violation/<ruleViolationID:\d+>'       => 'manager/rule-violation/view',

        'manager/rule-violation/create/<agencyID:\d+>'       => 'manager/rule-violation/create',        

        'manager/blog/edit/<PostID:\d+>'                     => 'manager/blog/edit',

        'manager/blog/delete/<PostID:\d+>'                   => 'manager/blog/delete',

        'manager/tasks'                                      => 'manager/tasks',

        'manager/chat-mailing-templates'                      => 'manager/chat-mailing-template/index',
        'manager/message-mailing-templates'                   => 'manager/message-mailing-template/index',
        'manager/chat-mailing-template/<action:[\w\-]+>/<templateID:\d+>' => 'manager/chat-mailing-template/<action>',
        'manager/message-mailing-template/<action:[\w\-]+>/<templateID:\d+>' => 'manager/message-mailing-template/<action>',
        'manager/mailing-statistic'                          => 'manager/mailing-statistic/index',
        'manager/mailing-statistic/<sessionID:\d+>'          => 'manager/mailing-statistic/view',

        'manager/add-task'                                   => 'manager/tasks/add-task',

        'manager/edit-task/<taskID:\d+>'                     => 'manager/tasks/edit-task',

        'manager/agency-tasks'                               => 'manager/tasks/agency-tasks',

        'manager/delete-task/<taskID:\d+>'                   => 'manager/tasks/delete-task',

        'manager/chat/<action:[\w\-]+>'                      => 'manager/chat/<action>',

        'manager/<action:[\w\-]+>'                           => 'manager/default/<action>',

        'manager/<action:[\w\-]+>/<otherUserID:\d+>'         => 'manager/default/<action>',

        'manager/get-user-token'                             => 'manager/default/get-user-token',

        'manager/need-to-approve/<action:[\w\-]+>/<otherUserID:\d+>' => 'manager/need-to-approve/<action>',



    ],

];