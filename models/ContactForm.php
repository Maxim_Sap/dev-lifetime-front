<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ContactForm extends Model
{
	public $name;
    public $email;
    public $subject;
    public $body;    

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            [['name'], 'string', 'max' => 120],
            [['subject'], 'integer', 'min' => 1, 'max' => 5],
            [['body'], 'string', 'max' => 400],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'First Name',
            'email' => 'E-mail',
            'body' => 'Your Message'
        ];
    }


}