<?php

namespace app\commands;

use chat\Server;

class ServerController extends \yii\console\Controller
{
    public function actionRun()
    {
        $server = new Server();
        $server->start();
    }
}