<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class ChatAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl  = '@web';
    public $css      = [        
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Roboto:300,400',
        'css/font-awesome.min.css',
        'css/custom-fonts.css',
        'css/reset.css',
        'css/style.css',
        'css/custom-style.css',
        'css/responsive.css',
        'css/simple-scrollbar.css',
        //'css/mediaelementplayer.css',
        'js/node_modules/emojione/extras/css/emojione.min.css',
        'js/node_modules/emojionearea/dist/emojionearea.min.css'
    ];
    public $js       = [
        '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
        'js/node_modules/emojionearea/dist/emojionearea.min.js',
        //'js/node_modules/emojione/lib/js/emojione.min.js',
        'js/ion.rangeSlider.min.js',
        'js/jquery.fancybox.min.js',
        'js/select2.min.js',
        'js/bootstrap.min.js',
        'js/slick.min.js',
        'js/simple-scrollbar.min.js',
        'js/custom.js',
        'js/swfobject.js',
        'js/web_socket.js',
        'js/chatConfig.js',
        //'js/mediaelement-and-player.min.js',
        
    ];

    public $jsOptions = array(
        
    );

    public $depends  = [        
        
    ];
}
