<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl  = '@web';
    public $css      = [        
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Roboto:300,400',        
        'css/font-awesome.min.css',
        'css/custom-fonts.css',
        'css/ion.rangeSlider.css',
        'css/slick.css',
        'css/jquery.fancybox.css',
        'css/select2.min.css',
        'css/reset.css',
        'css/style.css',
        'css/custom-style.css',
        'css/responsive.css',
        'css/mediaelementplayer.css'
    ];
    public $js       = [
        '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
        'js/node_modules/emojionearea/dist/emojionearea.min.js',
        'js/ion.rangeSlider.min.js',
        'js/jquery.fancybox.min.js',
        'js/select2.min.js',
        'js/bootstrap.min.js',
        'js/slick.min.js',
        'js/custom.js',
        'js/isotope.pkgd.min.js',
        'js/masonry.pkgd.min.js',
        'js/mediaelement-and-player.min.js',
        'js/config.js'
    ];

    public $jsOptions = array(
        
    );

    public $depends  = [        
        
    ];
}
