<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl  = '@web';
    public $css      = [
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Roboto:300,400',
        'css/font-awesome.css',
        'css/custom-fonts.css',
        'css/slick.css',
        'css/reset.css',
        'css/admin_style.css',                
        'css/admin-responsive.css'
    ];
    public $js       = [
        'js/bootstrap.min.js',
        'js/slick.min.js',
        'js/admin/admin-custom.js',
        'js/config.js',
        'js/chatConfig.js',       
    ];

    public $jsOptions = array(
        'position' => View::POS_HEAD
    );

    public $depends  = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'app\assets\LteIE9Asset',
    ];
}
