<?php

namespace app\modules\manager\controllers;

use Yii;
use app\modules\manager\controllers\ManagerController;

class SliderController extends ManagerController
{
    
    public $numberOfItemsOnAdminPage;
    public $numberOfTasksOnAdminPage;

    public function init()
    {
        $this->serverUrl = Yii::$app->params['serverUrl'];
        $this->numberOfItemsOnAdminPage = Yii::$app->params['numberOfItemsOnAdminPage'];
        $this->numberOfTasksOnAdminPage = Yii::$app->params['numberOfTasksOnAdminPage'];
        $this->view->registerJsFile('/js/admin/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);        
    }

    public function beforeAction($action)
    {
        $arrayOfActionsWithDisabledCSRFValidation = ['add-task', 'index'];
        if (in_array($action->id, $arrayOfActionsWithDisabledCSRFValidation)) {
            $this->enableCsrfValidation = FALSE;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $session = Yii::$app->session;       		

        $params = [	        
	        
        ];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
        $result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
        //var_dump($result->userData->avatar->small);die;
        if (!$result) {
            return $this->actionLogout();
        }
        if (!in_array($result->userData->userType, [self::USER_SUPERADMIN])) {
            $session->set('message', 'Error user type. Access denied.');
            return $this->actionLogout();
        }
        $result = json_decode($this->getCurl($session['user_token'], '/v1/admin/sliders'));		
		//var_dump($this->getCurl($session['user_token'], '/v1/admin/sliders')); die;

		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/manager', 301);
        }
        if (isset($result->status) && $result->status == 401) {
            $session->set('message', $giftTypes['message']);
            return $this->redirect('/manager', 301);
        }

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/crop_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('index', [
			'sliders' => $result->sliders,
            'albumInfo' => $result->album
		]);
    }

    public function actionAddTask()
    {
        $session = Yii::$app->session;

        $params = [];

        $postArray = Yii::$app->request->post();

        if (!empty($postArray)) {
            $params['agencyIDs']  = $postArray['agencyID'];
            $params['dateTo']     = (isset($postArray['dateTo']) && strtotime($postArray['dateTo'])) ? $postArray['dateTo'] : '';
            $params['description'] = (isset($postArray['description']) && trim($postArray['description']) != '') ? $postArray['description'] : '';
            $params['comment']     = (isset($postArray['comment']) && trim($postArray['comment']) != '') ? $postArray['comment'] : '';
            $params['taskID']     = isset($postArray['taskID']) ? (int)$postArray['taskID'] : null;
            $params['status']      = isset($postArray['status']) ? (int)$postArray['status'] : null;

            $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/add-task");   
            //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/add-task")); die;         
            if (!$result) {
                return $this->redirect('/manager', 301);
            }
            if(in_array($session['user_type'], [self::USER_AGENCY, self::USER_ADMIN])){
                $session->set('message', 'task updated');
            } else {
                $session->set('message', $result->message);
            } 

            return $this->redirect('/manager/tasks', 301);
        }

        $result = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-agency"));
        //var_dump(self::sendCurl($session['user_token'], $params, "/admin/get-agency")); die;
        if (!self::checkCurlResponse($result)) {
            return $this->redirect('/manager', 301);
        }

        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('add-task', [
            'agencyArray' => $result->agencyArray
        ]);

    }

    public function actionEditTask($taskID)
    {        
        $session = Yii::$app->session;
        $params = [            
            'taskID' => $taskID,
        ];

        $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/get-tasks");        
        if (!$result) {
            return $this->redirect('/manager', 301);
        }
        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        
        return $this->render('edit-task', [
            'task' => $result->task
        ]);

    }

    public function actionDeleteTask($taskID)
    {
        $session = Yii::$app->session;
        if ($session['user_type'] != ManagerController::USER_SUPERADMIN) {
            $session->set('message', 'Access denied');
            return $this->redirect('/manager', 301);
        }

        $params = [
            'taskID' => $taskID,
        ];

        $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/delete-task");
        if (!$result) {
            return $this->redirect('/manager', 301);
        }
        return $this->redirect('/manager/tasks', 301);

    }

}