<?php

namespace app\modules\manager\controllers;

use Yii;
use yii\web\Controller;

class ManagerController extends Controller
{
	public $googleHref;
	public $serverUrl;
	const USER_MALE = 1;
	const USER_FEMALE = 2;
	const USER_AGENCY = 3;
	const USER_INTERPRETER = 4;
	const USER_AFFILIATE = 5;
	const USER_SUPERADMIN = 6;
    const USER_ADMIN = 7;
	const USER_SITEADMIN = 8;

    const STATUS_DELETED = 0;
    const STATUS_NO_ACTIVE = 3;
    const STATUS_NO_EMAIL_CONFIRM = 5;
    const STATUS_ACTIVE = 10;  
    const STATUS_INVISIBLE = 0;
    const STATUS_VISIBLE = 10;
    const STATUS_FAKE_YES = 10; 
    const STATUS_FAKE_NO = 0;

    const TASK_NEW = 1;
    const TASK_IN_PROGRESS = 2;
    const TASK_CANCELED = 3;
    const TASK_APPROVED = 4;

    const APPROVE_STATUS_NOT_APPROVED = 1;
    const APPROVE_STATUS_IN_PROGRESS = 2;
    const APPROVE_STATUS_DECLINED = 3;
    const APPROVE_STATUS_APPROVED = 4;

    const PAUSED = 1;
    const NOT_PAUSED = 0;

    const WAS_FINISHED = 1;
    const WASNT_FINISHED = 0;

    const WASNT_RECEIVED = 0;
    const WAS_RECEIVED = 1;

    const WASNT_ABORTED = 0;
    const WAS_ABORTED = 1;

    const ALLOW = 1;
    const DENY = 0;

	public function init()
    {
    	$googleConfig = Yii::$app->params['google_auth'];
        $params      = array(
            'redirect_uri'  => $googleConfig['manager_redirect_uri'],
            'response_type' => 'code',
            'client_id'     => $googleConfig['client_id'],
            'scope'         => $googleConfig['scope'],
        );

        $this->googleHref = $googleConfig['url'] . '?' . urldecode(http_build_query($params));
        $this->serverUrl = Yii::$app->params['serverUrl'];
    }

    public function actionLogout()
    {    	
		$session = Yii::$app->session;
		unset($session['user_id']);
		unset($session['agency_id']);
		unset($session['user_token']);
		unset($session['user_data']);
		setcookie('user_token', "", time()-1800,"/");
		return $this->redirect('/manager', 301);
    }

    protected function checkIfUserNotLoggedIn()
    {
    	if (!isset(Yii::$app->session['user_token'])) {
    		Yii::$app->session->set('message', 'Error token. Please login');
			return $this->actionLogout();
        }
        return false;
    }

    protected function getResponseFromAPI($token, $params, $url="")
    {    	

    	$response = json_decode(self::sendCurl($token, $params, $url));

    	return self::checkCurlResponse($response);
		
    }

	protected static function sendCurl($token, $post, $path)
	{
		if ($token) {
			$authorization = "Authorization: Bearer " . $token;
		} else {
			$authorization = false;
		}
		
		$serverUrl = Yii::$app->params['serverUrl'];
		if( $curl = curl_init() ) {
			if ($authorization) {
				curl_setopt($curl, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded; charset=utf-8', $authorization));
			} else {
				curl_setopt($curl, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded; charset=utf-8'));
			}
		    
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_CRLF, true);
			curl_setopt($curl, CURLOPT_POST, true);
			//убрать когда бует подписаный сертификат
			curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0); 
			//
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));
			curl_setopt($curl, CURLOPT_URL, $serverUrl . $path);
		    $response = curl_exec($curl);

		    if (empty($response)) {
				return false;//'curl error: ' . curl_error($curl);	
		    }
		    curl_close($curl);
		    return $response;
		}
		return false;
    }
    
	protected static function getCurl($token, $path)
    {
        $authorization = "Authorization: Bearer " . $token;
        $serverUrl = Yii::$app->params['serverUrl'];
        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('application/json; charset=utf-8', $authorization));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_CRLF, true);            
            //убрать когда бует подписаный сертификат
            curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0); 
            //            
            curl_setopt($curl, CURLOPT_URL, $serverUrl . $path);
            $response = curl_exec($curl);
            
            if (empty($response)) {                
                return false; //'Curl Error: ' . curl_error($curl); 
            }
            curl_close($curl);
            return $response;
        }
        return false;
    }

    protected static function checkCurlResponse($response) 
    {	
		if (isset($response->success) && $response->success == true) {
			return $response;
		} else {
			$session = Yii::$app->session;
			$errorMessage = (isset($response->message)) ? $response->message : "Missing params";			
			$session->set('message', $errorMessage);
			return false;
		}
    }

}