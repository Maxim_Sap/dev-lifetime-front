<?php

namespace app\modules\manager\controllers;

use Yii;
use app\modules\manager\controllers\ManagerController;
use yii\web\BadRequestHttpException;


class MessageController extends ManagerController
{

    public $numberOfMessagesOnAdminPage;
    public $serverUrl;

    public function init()
    {

        $this->serverUrl = Yii::$app->params['serverUrl'];
        $this->numberOfMessagesOnAdminPage = Yii::$app->params['numberOfMessagesOnAdminPage'];
        $this->view->registerJsFile('/js/admin/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/message.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    }

    public function beforeAction($action)
    {
        $arrayOfActionsWithDisabledCSRFValidation = ['generate-list', 'new', 'answer', 'mailing'];
        if (in_array($action->id, $arrayOfActionsWithDisabledCSRFValidation)) {
            $this->enableCsrfValidation = FALSE;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $session = Yii::$app->session;

        $params = [

        ];
        $result = $this->getResponseFromAPI($session['user_token'], $params, "/admin/get-agency");        
        if (!$result) {
            return $this->redirect('/manager', 301);
        }

        $this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('index', ['agencyArray' => $result->agencyArray]);
    }


    public function actionInbox()
    {
        $session = Yii::$app->session;        
        if ($logout = $this->checkIfUserNotLoggedIn()) {
            return $logout;
        }
        $params = [
            
        ];

        $page = 1;
        if ($session['user_type'] == self::USER_SUPERADMIN || $session['user_type'] == self::USER_SITEADMIN) { //for admin
            $params['limit'] = $this->numberOfMessagesOnAdminPage;
            $params['offset'] = 0;
            $params['searchParams'] = ['type' => 'inbox'];
            $result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/get-message"));
            //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-message"));die;
        } elseif (in_array($session['user_type'], [self::USER_AGENCY, self::USER_ADMIN])) {
            $params['limit'] = $this->numberOfMessagesOnAdminPage;
            $params['offset'] = 0;
            $params['searchParams'] = ['type' => 'inbox'];
            $result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/get-message"));
            //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-message"));die;
        } else {
            $result = false;
        }

        if (!self::checkCurlResponse($result)) {
            return $this->redirect('/manager', 301);
        }

        if(in_array($session['user_type'], [self::USER_AGENCY, self::USER_ADMIN])) { //for agency
            return $this->render('inbox', [
                'messageArray' => $result->messageArray,
                'countMessage' => $result->count, 
                'page' => $page,
                'numberOfMessagesOnAdminPage' => $this->numberOfMessagesOnAdminPage
            ]);
        }

        return $this->render('inbox', [
            'messageArray' => $result->messageArray,
            'countMessage' => $result->count, 
            'page' => $page,
            'numberOfMessagesOnAdminPage' => $this->numberOfMessagesOnAdminPage
        ]);

    }

    public function actionOutbox()
    {
        $session = Yii::$app->session;
        $page   = 1;
        $params = [            
            'limit'         => $this->numberOfMessagesOnAdminPage,
            'offset'        => 0,
            'searchParams' => ['type' => 'outbox'],
        ];

        $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/get-message");
        if (!$result) {
            return $this->redirect('/manager', 301);
        }

        return $this->render('outbox', [
            'messageArray' => $result->messageArray, 
            'countMessage' => $result->count, 
            'page' => $page, 
            'numberOfMessagesOnAdminPage' => $this->numberOfMessagesOnAdminPage
        ]);

    }

    public function actionAnswer($messageID)
    {
        $session = Yii::$app->session;

        $postArray = Yii::$app->request->post();

        if (!empty($postArray)) {
            $toUserID = (isset($postArray['fromUserID']) && is_numeric($postArray['fromUserID'])) ? (int)$postArray['fromUserID'] : null;

            $params = [
                'fromUserID'        => $session['user_id'], //admin
                'toUserID'          => $toUserID,
                'desc'                => trim($postArray['description']),
                'title'             => trim($postArray['caption']),
                'previousLetterID' => $messageID,
            ];
            
            $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/message/send-message");
            //var_dump(self::sendCurl($session['user_token'], $params, "/v1/message/send-message"));die;
            if (!self::checkCurlResponse($result)) {
                return $this->redirect('/manager/message/', 301);
            }
            
            if (!empty($toUserID) || !empty($postArray['email'])) {
                if (empty($postArray['email'])) {
                    $params = [
                        'otherUserID' => $toUserID,
                    ];
                    $result = $this->getResponseFromAPI($session['user_token'], $params, "/admin/get-user-email");

                    $to = $result->email;
                } else {
                    $to = $postArray['email'];
                }

                if (trim($to) != '') {
                    $from = Yii::$app->params['adminEmail'];

                    $sendEmail = Yii::$app->mailer->compose()
                        ->setTo($to)
                        ->setFrom($from)
                        ->setSubject($postArray['caption'])
                        ->setHtmlBody($postArray['description'])
                        ->send();
                }
            }

            $session->set('message', 'Message send');

            return $this->redirect('/manager/message/', 301);
        }

        $typeFromGetArray = (isset($_GET['type']) && trim($_GET['type']) != '') ? $_GET['type'] : 'inbox';
        $params = [
            'messageID'    => $messageID,
            'read'          => 1,
            'searchParams' => ['type' => $typeFromGetArray],
        ];

        $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/get-message");
        //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-message")); die;
        if (!$result) {
            return $this->redirect('/manager', 301);
        }
        if (empty($result->messageArray)) {
            $session->set('message', 'Wrong message Id');

            return $this->redirect('/manager/message/', 301);
        }

        $this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('answer', [
            'messageArray' => $result->messageArray, 
            'countMessage' => $result->count
        ]);

    }

    public function actionNew($otherUserID = null)
    {
        $session = Yii::$app->session;

        $postArray = Yii::$app->request->post();

        if (!empty($postArray)) {
            $toUserID = (isset($postArray['toUserID']) && is_numeric($postArray['toUserID'])) ? (int)$postArray['toUserID'] : null;
            $params = [
                'fromUserID' => $session['user_id'],//admin
                'toUserID'   => $toUserID,
                'desc'         => trim($postArray['description']),
                'caption'      => trim($postArray['caption']),
            ];
            $result = $this->getResponseFromAPI($params, "/v1/message/send-message");
            
            if (!empty($toUserID)) {
                $params = [                    
                    'otherUserID' => $toUserID,
                ];
                $result = $this->getResponseFromAPI($params, "/v1/admin/get-user-email");

                if (trim($result->email) != '') {
                    $from = Yii::$app->params['adminEmail'];
                    $to = $result->email;
                    $sendEmail = Yii::$app->mailer->compose()
                        ->setTo($to)
                        ->setFrom($from)
                        ->setSubject($postArray['caption'])
                        ->setHtmlBody($postArray['description'])
                        ->send();
                }
            }

            $session->set('message', 'Message send');
        }

        $this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('new', ['otherUserID' => $otherUserID]);

    }

    public function actionGenerateList()
    {
        $postArray = Yii::$app->request->post();
        
        if (empty($postArray) || !isset($postArray['message_mass']) || empty($postArray['message_mass'])) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['success' => TRUE, 'table' => ''];
        }

        $messages   = Yii::$app->controller->renderPartial('table.php', [
            'message_mass' => $postArray['message_mass'], 
            'count_message' => $postArray['count_message'], 
            'page_name' => $postArray['page_name']
        ]);
        $pagination = Yii::$app->controller->renderPartial('/layouts/parts/pagination.php', [
            'letters_count' => $postArray['count_message'], 
            'page' => (int)$postArray['page'], 
            'limit' => $this->numberOfMessagesOnAdminPage, 
            'page_name' => ($postArray['page'] == 'outbox') ? 'outbox' : 'inbox'
        ]);

        $result = [
            'message_list' => $messages,
            'pagination'   => $pagination,
        ];
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }

    public function actionMailing()
    {
        $session = Yii::$app->session;
        if (!isset($session['user_token'])) {
            $session->set('message', 'Error token. Please login');

            return $this->redirect('/manager', 301);
        }        

        $userIDs = Yii::$app->request->post('userIDs');
        $description = Yii::$app->request->post('description');
        $caption = Yii::$app->request->post('caption');
        $userTypeID = Yii::$app->request->post('userTypeID');

        $params = [
            'userIDs' => $userIDs,
            'userTypeID' => $userTypeID,
            'description'     => trim($description),
            'caption'  => trim($caption),
        ];
        
        $result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/mailing"));
        //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/mailing")); die;


        if (!self::checkCurlResponse($result)) {
            return $this->redirect('/manager/message/', 301);
        }

        $session->set('message', $result->message);

        return $this->redirect('/manager/message/', 301);

    }

}
