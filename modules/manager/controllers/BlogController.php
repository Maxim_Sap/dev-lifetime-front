<?php

namespace app\modules\manager\controllers;

use Yii;
use yii\helpers\VarDumper;
use app\modules\manager\controllers\ManagerController;
use yii\web\BadRequestHttpException;
use yii\helpers\Json;

class BlogController extends ManagerController
{
	public $numberOfItemsOnAdminPage;

    public function init()
    {
		parent::init();
        $this->numberOfItemsOnAdminPage = Yii::$app->params['numberOfItemsOnAdminPage'];
        $this->view->registerJsFile('/js/admin/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    }
    
    public function beforeAction($action)
	{            	    
		$session = Yii::$app->session;
		if ($session['user_type'] == self::USER_SITEADMIN && (!isset($session['permissions']) || $session['permissions']['blog_access'] == ManagerController::DENY)) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		} 
	    $arrayOfActionsWithDisabledCSRFValidation = [
	    		'add', 'edit'
		];
	    if (in_array($action->id, $arrayOfActionsWithDisabledCSRFValidation)) {
	        $this->enableCsrfValidation = false;
	    }

	    return parent::beforeAction($action);
	}
    
    public function actionIndex()
    {
        $session = Yii::$app->session;
		if(!isset($session['user_id']) || !isset($session['user_token'])) {
			return $this->redirect('/manager/login', 301);
        }
        
        $params = [
	        'userID' => $session['user_id'],
	        'token' => $session['user_token']
        ];        

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
		
		if (!$result) {
			return $this->actionLogout();
        }

		if (!in_array($result->userData->userType, [self::USER_SITEADMIN, self::USER_SUPERADMIN])) {
			$session->set('message', 'Error user type. Access denied.');
			return $this->actionLogout();
        }

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/admin/get-blog-post");
		//var_dump(self::sendCurl($session['user_token'], $params, "/admin/get-blog-post")); die;
		if (!$result) {
			return $this->actionLogout();
		}

		return $this->render('blog-list',['post_list'=>$result->post_list]);
    }

	public function actionAdd()
	{

		$session = Yii::$app->session;
		if(!isset($session['user_id']) || !isset($session['user_token'])) {
			return $this->redirect('/manager/login', 301);
		}
		
		return $this->render('add');
	}

	public function actionEdit($PostID)
	{
		$session = Yii::$app->session;
		if ($session['user_type'] != self::USER_SUPERADMIN && $session['user_type'] != self::USER_SITEADMIN) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager', 301);
		}

		$params = [
			'skinID' => 1,
			'PostID' => (int)$PostID,
			'token' => $session['user_token']
		];

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/admin/get-blog-post");

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('add', [
			'PostID' => $PostID,
			'post_data'=>$result->post_list
		]);

	}

	public function actionDelete($PostID)
	{
		$session = Yii::$app->session;
		if ($session['user_type'] != self::USER_SUPERADMIN && $session['user_type'] != self::USER_SITEADMIN) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager', 301);
		}

		$params = [
			'skinID' => 1,
			'PostID' => (int)$PostID,
			'token' => $session['user_token']
		];

		//$result = $this->getResponseFromAPI($session['user_token'], $params, "/admin/get-blog-post");
		$result = $this->getResponseFromAPI($session['user_token'], $params, "/admin/blog-post-delete");


		//var_dump($result);die;
		if (!$result) {
			$session->set('message', 'Failed to delete the object for unknown reason.');
		}

		return $this->redirect('/manager/blog/', 301);

	}


}
