<?php

namespace app\modules\manager\controllers;

use Yii;
use app\modules\manager\controllers\ManagerController;
use yii\web\BadRequestHttpException;
use yii\helpers\HtmlPurifier;

class MailingStatisticController extends ManagerController
{
    const CREATED_FOR_CHAT = 0;
    const CREATED_FOR_MESSAGE = 1;

	public $numberOfItemsOnAdminPage;
	
    public function init()
    {
        $this->serverUrl = Yii::$app->params['serverUrl'];
        $this->numberOfItemsOnAdminPage = 10;
        $this->view->registerJsFile('/js/admin/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/message.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    }
    
    public function beforeAction($action)
	{            	    
		$arrayOfActionsWithDisabledCSRFValidation = [];
	    if (in_array($action->id, $arrayOfActionsWithDisabledCSRFValidation)) {
	        $this->enableCsrfValidation = false;
	    }

	    return parent::beforeAction($action);
	}
    
    public function actionIndex($page = 1, $dateFrom = '', $dateTo = '')
    {        
        $session = Yii::$app->session;                   
        if(!isset($session['user_id']) || !isset($session['user_token'])) {
            return $this->redirect('/manager/login', 301);
        }

        $page = (int) $page;
        if ($page < 0) {
            $page = 1;
        }

        if (!empty($dateFrom) && strtotime($dateFrom)) {
        	$dateFrom = date('Y-m-d', strtotime($dateFrom));
        }
        if (!empty($dateTo) && strtotime($dateTo)) {
        	$dateTo = date('Y-m-d', strtotime($dateTo));
        }
        if (!empty($dateFrom) && !empty($dateTo)) {
        	$filterString = "&date_from=$dateFrom&date_to=$dateTo";
        } else {
        	$dateFrom = date('Y') . '-' . (date('m')-1) . '-01';
        	$dateTo = date('Y-m-d');
        	$filterString = '&date_from=' . $dateFrom . '&date_to=' . $dateTo;
        }     

        //var_dump($filterString); die;

        $result = json_decode(self::getCurl($session['user_token'], "/v1/mailing-sessions?page=$page&per_page={$this->numberOfItemsOnAdminPage}" . $filterString));
        //var_dump(self::getCurl($session['user_token'], "/v1/mailing-session/chat-sessions?page=$page&per_page={$this->numberOfItemsOnAdminPage}" . $filterString)); die;       
        if ((isset($result->success) && $result->success == false) || 
            (isset($result->status) && ($result->status == 401 || $result->status == 404 || $result->status == 500))) {            
            $session->set('message', $result->message);
            return $this->redirect('/manager', 301);
        }
        return $this->render('index', [
            'sessions' => $result->sessions,
            'count' => (isset($result->count) && $result->count > $this->numberOfItemsOnAdminPage) ? $result->count : null,
            'page' => $page,
            'limit' => $this->numberOfItemsOnAdminPage,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo
        ]);
    }
        	
	public function actionView($sessionID, $page = 1)
    {		
        $session = Yii::$app->session;                   
        if(!isset($session['user_id']) || !isset($session['user_token'])) {
            return $this->redirect('/manager/login', 301);
        }
        $page = (int) $page;
        if ($page < 0) {
            $page = 1;
        }
        $result = json_decode(self::getCurl($session['user_token'], "/v1/mailing-session/$sessionID"));
        //var_dump(self::getCurl($session['user_token'], "/v1/mailing-session/$sessionID")); die;
        if ((isset($result->success) && $result->success == false) || 
            (isset($result->status) && ($result->status == 401 || $result->status == 404 || $result->status == 500))) {           
            $session->set('message', $result->message);
            return $this->redirect('/manager', 301);
        }
        $result2 = json_decode(self::getCurl($session['user_token'], "/v1/mailing-session/$sessionID/queues?page=$page&per_page={$this->numberOfItemsOnAdminPage}"));
        //var_dump(self::getCurl($session['user_token'], "/v1/mailing-session/$sessionID/queues?page=$page&per_page={$this->numberOfItemsOnAdminPage}")); die;
        if ((isset($result2->success) && $result2->success == false) || 
            (isset($result2->status) && ($result2->status == 401 || $result2->status == 404 || $result2->status == 500))) {
            $session->set('message', $result2->message);
            return $this->redirect('/manager', 301);
        }

        return $this->render('view', [
            'session' => $result->session,
            'mailingQueues' => $result2->queues,
            'count' => (isset($result2->count) && $result2->count > $this->numberOfItemsOnAdminPage) ? $result2->count : null,
            'page' => $page,
            'sessionID' => $sessionID,
            'limit' => $this->numberOfItemsOnAdminPage,
        ]);
    }

       
}
