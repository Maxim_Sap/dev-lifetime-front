<?php

namespace app\modules\manager\controllers;

use Yii;
use yii\web\Controller;

class ChatController extends ManagerController
{

    public $numberOfItemsOnAdminPage;

    public function init()
    {
        parent::init();
        $this->numberOfItemsOnAdminPage = Yii::$app->params['numberOfItemsOnAdminPage'];    
        $this->view->registerJsFile('/js/admin/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);    
        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    }

    public function beforeAction($action)
    {
        //отключение проверки для определенных екшенов
        if (in_array($action->id, ['set-women-online', 'generate-user-list'])) {
            $this->enableCsrfValidation = FALSE;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {

        $session = Yii::$app->session;
        if (!isset($session['user_id']) || !isset($session['user_token'])) {
            return $this->redirect('/manager/login', 301);
        }

        $params = [
            'user_id'=> $session['user_id'],
            'token'=>$session['user_token']
        ];

        $result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
        
        if (!$result) {
            return $this->actionLogout();
        }

        if (!in_array($result->userData->userType, [self::USER_INTERPRETER])) {
            $session->set('message', 'Error user type. Access is denied.');
            return $this->actionLogout();
        }

        $params = [
            'skinID' => 1,
            'userType' => self::USER_FEMALE,
            'userID' => $session['user_id'],
            'agencyID' => (isset($result->userData->agencyID)) ? $result->userData->agencyID : null,
            'type' => 'from_agency',
            'notRemoveStatusParam' => false,
            'params' => 'girls',
            'visibility' => 'all'
        ];

        $result = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-users-for-admin"));
        //var_dump(self::sendCurl($session['user_token'], $params, "/user/get-users-for-admin")); die;
        
        if (!self::checkCurlResponse($result)) {
            return $this->redirect('/', 301);
        }

        $this->view->registerCssFile('/js/node_modules/emojione/extras/css/emojione.min.css', ['depends' => [\app\assets\AdminAsset::className()]]);
        $this->view->registerCssFile('/js/node_modules/emojionearea/dist/emojionearea.min.css', ['depends' => [\app\assets\AdminAsset::className()]]);
        $this->view->registerCssFile('/css/admin-chat.css', ['depends' => [\app\assets\AdminAsset::className()]]);
        $this->view->registerJsFile('/js/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('js/node_modules/emojionearea/dist/emojionearea.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/girlChat.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/message.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('index', ['girls' => $result->girls]);
    }

    public function actionSetWomenOnline()
    {
        $session = Yii::$app->session;
        if (!isset($session['user_id']) || !isset($session['user_token'])) {
            return $this->redirect('/manager/login', 301);
        }

        $womenOnline = isset($session['women_online']) ? $session['women_online'] : [];

        $post = Yii::$app->request->post();
        if (isset($post['other_user_id']) && isset($post['status'])) {
            if ($post['status'] == 0) {
                $key = array_search($post['other_user_id'], $womenOnline);
                if ($key !== null) {
                    unset($womenOnline[$key]);
                    $session->set('women_online', $womenOnline);
                }
            } else {
                if (!in_array((int)$post['other_user_id'], $womenOnline)) {
                    $womenOnline[] = (int)$post['other_user_id'];
                }

            }

            $params = [
                'skinID'  => 1,
                'userIDs' => [(int)$post['other_user_id']],
                'status'   => (int)$post['status'],
                'timeTo'  => 60 * 5,//5 минут
            ];

            $result = json_decode(self::SendCurl($session['user_token'], $params, "/admin/set-online-status"));
            //var_dump(self::SendCurl($session['user_token'], $params, "/admin/set-online-status")); die;

            if (!self::checkCurlResponse($result)) {
                return json_encode([
                    'success' => FALSE, 
                    //'code' => $result->code, 
                    'message' => $result->message
                ]);
            }

            $session->set('women_online', $womenOnline);

        } elseif (!empty($womenOnline)) {
            $params = [
                'skinID'  => 1,
                'userIDs' => $womenOnline,
                'status'   => 1,
                'timeTo'  => 60 * 5,//5 минут
            ];

            $result = json_decode(self::SendCurl($session['user_token'], $params, "/admin/set-online-status"));

            if (!self::checkCurlResponse($result)) {
                return json_encode([
                    'success' => FALSE, 
                    'code' => $result->code, 
                    'message' => $result->message, 
                    'women_online' => $womenOnline
                ]);
            }
        }

        return json_encode([
            'success' => TRUE, 
            'code' => 1,             
            'women_online' => $womenOnline
        ]);

    }

    public function actionGenerateUserList()
    {
        $post = Yii::$app->request->post();
        if (empty($post['girls'])) {
            return json_encode('<p>No user online</p>');
        }

        $user_list = Yii::$app->controller->renderPartial('user-list.php', ['userList' => $post['girls']]);

        return json_encode($user_list);
    }

    

}