<?php

namespace app\modules\manager\controllers;

use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\BadRequestHttpException;


class NeedToApproveController extends ManagerController
{

    public $numberOfItemsOnAdminPage;
    public $numberOfTasksOnAdminPage;
	public $numberOfGiftItemsOnFrontPage;

    public function init()
    {
		parent::init();
        $this->numberOfItemsOnAdminPage = Yii::$app->params['numberOfItemsOnAdminPage'];
		$this->numberOfTasksOnAdminPage = Yii::$app->params['numberOfTasksOnAdminPage'];
		$this->numberOfGiftItemsOnFrontPage = Yii::$app->params['numberOfGiftItemsOnFrontPage'];
        $this->view->registerJsFile('/js/admin/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    }

    public function beforeAction($action)
    {
        //отключение проверки для определенных екшенов
        if (in_array($action->id, [''])) {
            $this->enableCsrfValidation = FALSE;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $session = Yii::$app->session;

        if (!isset($session['user_token'])) {
            $session->set('message', 'Error token. Please login');

            return $this->redirect('/manager', 301);
        }

        if ($session['user_type'] != ManagerController::USER_SUPERADMIN) {
            return $this->redirect('/manager/', 301);
        }

        $params = [

        ];

        $result = json_decode(self::getCurl($session['user_token'], "/v1/admin/need-to-approve-items-by-agency"));
        //var_dump(self::getCurl($session['user_token'], "/v1/admin/need-to-approve-items-by-agency")); die;
        if (!self::checkCurlResponse($result)) {
            return $this->redirect('/manager', 301);
        }

        $result2 = json_decode(self::sendCurl($session['user_token'], [], "/admin/get-agency"));
        //var_dump($result2); die;
        if (!self::checkCurlResponse($result2)) {
            return $this->redirect('/manager', 301);
        }

        $temp = [];
        foreach ($result2->agencyArray as $agency) {
            $temp[$agency->user_id] = $agency->name;
        }

        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('index', [
            'agencyList'    => $result->agencyList,
            'agencyArray' => $temp,
            'usersList'     => $result->users,
            'videoList'     => $result->videoList,
            'photosList'     => $result->photosList,
            'deletedPhotosList' => $result->deletedPhotosList,
            'giftsList'     => $result->giftsList,
            'tasksList'     => $result->tasksList,
        ]);

    }

    public function actionAgency($otherUserID)
    {
        $session = Yii::$app->session;
        if (!isset($session['user_token'])) {
            $session->set('message', 'Error token. Please login');

            return $this->redirect('/manager', 301);
        }

        if ($session['user_type'] != ManagerController::USER_SUPERADMIN) {
            return $this->redirect('/manager/', 301);
        }

        $params = [
            'agencyID' => $otherUserID,
        ];

        $result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/need-to-approve-by-param"));
        //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/need-to-approve-by-param"));die;
        if (!self::checkCurlResponse($result)) {
            return $this->redirect('/manager', 301);
        }

        $result2 = json_decode(self::sendCurl($session['user_token'], [], "/admin/get-agency"));
        //var_dump($result2); die;
        if (!self::checkCurlResponse($result2)) {
            return $this->redirect('/manager', 301);
        }
        $id = 1;
        $name = 'Пользователи без агенства';
        foreach ($result2->agencyArray as $agency) {
            if ($agency->id == $otherUserID) {
                $id = $agency->user_id;
                $name = $agency->name;
            }            
        }

        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('agency', [
            'agencyID' => $id,
            'agencyName' => $name,
            'usersList'     => $result->users,
            'videoList'     => $result->videoList,
            'photosList'     => $result->photosList,
            'deletedPhotosList' => $result->deletedPhotosList,
            'giftsList'     => $result->giftsList,
            'tasksList'     => $result->tasksList,
        ]);
    }

    public function actionUser($otherUserID)
    {
        $session = Yii::$app->session;
        if (!isset($session['user_token'])) {
            $session->set('message', 'Error token. Please login');

            return $this->redirect('/manager', 301);
        }

        if ($session['user_type'] != ManagerController::USER_SUPERADMIN) {
            return $this->redirect('/manager/', 301);
        }

        $params = [
            'otherUserID' => $otherUserID,
        ];

        $result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/need-to-approve-by-param"));
        //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/need-to-approve-by-param"));die;
        if (!self::checkCurlResponse($result)) {
            return $this->redirect('/manager', 301);
        }

        $result2 = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-user-info"));
        //var_dump($result2); die;
        if (!self::checkCurlResponse($result2)) {
            return $this->redirect('/manager', 301);
        }
        $userInfo = [
            'id' => $result2->userData->personalInfo->id,
            'name' => $result2->userData->personalInfo->first_name . ' ' . $result2->userData->personalInfo->last_name
        ];

        $result3 = json_decode(self::sendCurl($session['user_token'], [], "/admin/get-agency"));
        //var_dump($result3); die;
        if (!self::checkCurlResponse($result3)) {
            return $this->redirect('/manager', 301);
        }
        $agencyInfo = [
            'id' => 1,
            'name' => 'Пользователи без агенства'
        ];        
        
        if (!empty($result2->userData->agencyID)) {
            foreach ($result3->agencyArray as $agency) {
                if ($agency->id == $result2->userData->agencyID) {
                    $agencyInfo = [
                        'id' => $agency->user_id,
                        'name' => $agency->name
                    ];                
                }            
            }
        }        

        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('user', [
            'userInfo' => $userInfo, 
            'agencyInfo' => $agencyInfo,
            'usersList' => $result->users,
            'videoList' => $result->videoList, 
            'photoList' => $result->photosList, 
            'deletedPhotoList' => $result->deletedPhotosList
        ]);

    }

    
}
