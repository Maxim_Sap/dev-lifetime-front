<?php

namespace app\modules\manager\controllers;

use Yii;
use app\modules\manager\controllers\ManagerController;
use yii\web\BadRequestHttpException;


class FinanceController extends ManagerController
{

	public $numberOfItemsOnAdminPage;
	
    public function init()
    {

		$this->serverUrl = Yii::$app->params['serverUrl'];
        $this->numberOfItemsOnAdminPage = Yii::$app->params['numberOfItemsOnAdminPage'];        
        $this->view->registerJsFile('/js/admin/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    }
    
    public function beforeAction($action)
	{            
		$session = Yii::$app->session;
		if ($session['user_type'] == self::USER_ADMIN && (!isset($session['permissions']) || $session['permissions']['finance_access'] == ManagerController::DENY)) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}  
		if ($session['user_type'] == self::USER_SITEADMIN && (!isset($session['permissions']) || $session['permissions']['finance_access'] == ManagerController::DENY)) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		} 		
	    $arrayOfActionsWithDisabledCSRFValidation = [
			'generate-financy-list','generate-total-finance-report-list','total','affiliates',
			'agency-payments-schedule','add-agency-payments'
		];
	    if (in_array($action->id, $arrayOfActionsWithDisabledCSRFValidation)) {
	        $this->enableCsrfValidation = false;
	    }

	    return parent::beforeAction($action);
	}
    
    public function actionIndex()
    {		
		return $this->redirect('/manager/finance/letters');
    }
    
    public function actionUserInfo($otherUserID)
    {
		$session = Yii::$app->session;
                
		$dateFrom = Yii::$app->request->post('dateFrom');
        $dateTo = Yii::$app->request->post('dateTo');

        $dateFrom = isset($dateFrom) && $this->validateDate($dateFrom) ? $dateFrom : date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y')));
        $dateTo = isset($dateTo) && $this->validateDate($dateTo) ? $dateTo : date('Y-m-d', time());
        
        $params = [
	        'limit' => $this->numberOfItemsOnAdminPage,
	        'offset' => 0,
	        'otherUserID' => $otherUserID,
	        'searchParams' => [
	        	'dateFrom' => $dateFrom,
	        	'dateTo' => $dateTo,
	        ],
        ];
				
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params, "/v1/admin/get-user-finance-info"); 
		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/get-user-finance-info")); die;
		if (!$result) {
			return $this->redirect('/manager',301);
        }
                
		return $this->render('finance-user', [
			'usersFinanceInfo' => $result->usersFinanceInfo,
			'otherUserID' => $otherUserID,
			'limit' => $this->numberOfItemsOnAdminPage,
			'dateFrom' => $dateFrom,
			'dateTo' => $dateTo
		]);
        
    }
    
    public function actionLetters()
    {
		$session = Yii::$app->session;

        $dateFrom = Yii::$app->request->post('dateFrom');
        $dateTo = Yii::$app->request->post('dateTo');

        $dateFrom = isset($dateFrom) && $this->validateDate($dateFrom) ? $dateFrom : date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y')));
        $dateTo = isset($dateTo) && $this->validateDate($dateTo) ? $dateTo : date('Y-m-d', time());

        $params = [
	        'limit' => $this->numberOfItemsOnAdminPage,
	        'offset' => 0,
	        'searchParams' => [
	        	'dateFrom' => $dateFrom,
	        	'dateTo' => $dateTo,
	        	'type' => 'letters',
	        ],
        ];
		
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params,"/v1/admin/get-user-finance-info"); 
		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/get-user-finance-info")); die;
		if (!$result) {
			return $this->redirect('/manager',301);
        }
		return $this->render('letters', [
			'usersFinanceInfo' => $result->usersFinanceInfo,
			'dateFrom' => $dateFrom, 
			'dateTo' => $dateTo,
			'limit' => $this->numberOfItemsOnAdminPage,
			'page' => 1,
			'count' => $result->usersFinanceInfo->letters->count
		]);
    }
    
    public function actionChats()
    {
		$session = Yii::$app->session;
        
		$dateFrom = Yii::$app->request->post('dateFrom');
        $dateTo = Yii::$app->request->post('dateTo');

        $dateFrom = isset($dateFrom) && $this->validateDate($dateFrom) ? $dateFrom : date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y')));
        $dateTo = isset($dateTo) && $this->validateDate($dateTo) ? $dateTo : date('Y-m-d', time());
        
        $params = [	        
	        'limit' => $this->numberOfItemsOnAdminPage,
	        'offset' => 0,
	        'searchParams' => [
	        	'dateFrom' => $dateFrom,
	        	'dateTo' => $dateTo,
	        	'type' =>'chats',
	        ],
        ];
		
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params,"/v1/admin/get-user-finance-info");		
        //var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/get-user-finance-info")); die;
		if (!$result) {
			return $this->redirect('/manager',301);
        }
		return $this->render('chats', [
			'usersFinanceInfo' => $result->usersFinanceInfo,
			'dateFrom' => $dateFrom,
			'dateTo' => $dateTo,
			'limit' => $this->numberOfItemsOnAdminPage,
			'page' => 1,
			'count' => $result->usersFinanceInfo->chats->count
		]);
        
    }
    
    public function actionVideoChats()
    {
		$session = Yii::$app->session;
        
		$dateFrom = Yii::$app->request->post('dateFrom');
        $dateTo = Yii::$app->request->post('dateTo');

        $dateFrom = isset($dateFrom) && $this->validateDate($dateFrom) ? $dateFrom : date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y')));
        $dateTo = isset($dateTo) && $this->validateDate($dateTo) ? $dateTo : date('Y-m-d', time());
        
        $params = [	        
	        'limit' => $this->numberOfItemsOnAdminPage,
	        'offset' => 0,
	        'searchParams' => [
	        	'dateFrom' => $dateFrom,
	        	'dateTo' => $dateTo,
	        	'type' => 'video-chat',
	        ],
        ];
				
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params,"/v1/admin/get-user-finance-info");	
		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/get-user-finance-info")); die;
		if (!$result) {
			return $this->redirect('/manager',301);
		}

		return $this->render('video-chats', [
			'usersFinanceInfo' => $result->usersFinanceInfo,
			'dateFrom' => $dateFrom, 
			'dateTo' => $dateTo,
			'limit' => $this->numberOfItemsOnAdminPage,
			'page' => 1,
			'count' => $result->usersFinanceInfo->videoChats->count
		]);
    }
    
    public function actionGifts()
    {
		$session = Yii::$app->session;

		$dateFrom = Yii::$app->request->post('dateFrom');
        $dateTo = Yii::$app->request->post('dateTo');

        $dateFrom = isset($dateFrom) && $this->validateDate($dateFrom) ? $dateFrom : date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y')));
        $dateTo = isset($dateTo) && $this->validateDate($dateTo) ? $dateTo : date('Y-m-d', time());
        
        $params = [
	        'limit' => $this->numberOfItemsOnAdminPage,
	        'offset' => 0,
	        'searchParams' => [
	        	'dateFrom' => $dateFrom,
	        	'dateTo' => $dateTo,
	        	'type' => 'gifts',
	        ],
        ];
		
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params, "/v1/admin/get-user-finance-info"); 	
        //var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/get-user-finance-info")); die;
		if (!$result) {
			return $this->redirect('/manager',301);
		}
        
		return $this->render('gifts', [
			'usersFinanceInfo' => $result->usersFinanceInfo,
			'dateFrom' => $dateFrom,
			'dateTo' => $dateTo,
			'limit' => $this->numberOfItemsOnAdminPage,
			'page' => 1,
			'count' => $result->usersFinanceInfo->gifts->count
		]);
    }

	public function actionPremiumPhoto()
	{
		$session = Yii::$app->session;

		$dateFrom = Yii::$app->request->post('dateFrom');
		$dateTo = Yii::$app->request->post('dateTo');

		$dateFrom = isset($dateFrom) && $this->validateDate($dateFrom) ? $dateFrom : date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y')));
		$dateTo = isset($dateTo) && $this->validateDate($dateTo) ? $dateTo : date('Y-m-d', time());

		$params = [
			'limit' => $this->numberOfItemsOnAdminPage,
			'offset' => 0,
			'searchParams' => [
				'dateFrom' => $dateFrom,
				'dateTo' => $dateTo,
				'type' => 'premium_photos',
			],
		];

		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params,"/v1/admin/get-user-finance-info");
		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/get-user-finance-info")); die;
		if (!$result) {
			return $this->redirect('/manager',301);
		}
		return $this->render('premium-photo', [
			'usersFinanceInfo' => $result->usersFinanceInfo,
			'dateFrom' => $dateFrom,
			'dateTo' => $dateTo,
			'limit' => $this->numberOfItemsOnAdminPage,
			'page' => 1,
			'count' => $result->usersFinanceInfo->premiumPhotos->count
		]);
	}

	public function actionPremiumVideo()
	{
		$session = Yii::$app->session;

		$dateFrom = Yii::$app->request->post('dateFrom');
		$dateTo = Yii::$app->request->post('dateTo');

		$dateFrom = isset($dateFrom) && $this->validateDate($dateFrom) ? $dateFrom : date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y')));
		$dateTo = isset($dateTo) && $this->validateDate($dateTo) ? $dateTo : date('Y-m-d', time());

		$params = [
			'limit' => $this->numberOfItemsOnAdminPage,
			'offset' => 0,
			'searchParams' => [
				'dateFrom' => $dateFrom,
				'dateTo' => $dateTo,
				'type' => 'premium_video',
			],
		];

		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params,"/v1/admin/get-user-finance-info");
		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/get-user-finance-info")); die;
		if (!$result) {
			return $this->redirect('/manager',301);
		}
		return $this->render('premium-video', [
			'usersFinanceInfo' => $result->usersFinanceInfo,
			'dateFrom' => $dateFrom,
			'dateTo' => $dateTo,
			'limit' => $this->numberOfItemsOnAdminPage,
			'page' => 1,
			'count' => $result->usersFinanceInfo->PremiumVideo->count
		]);
	}
    
    public function actionGenerateFinancyList()
    {
    	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$postArray = Yii::$app->request->post();
	    if (empty($postArray) || !isset($postArray['items'])) {
	        return ['success' => true, 'letters' => '','chats' => '','video_chats' => '','gifts' => ''];
	    }
        $paginator = '';
	   	if (isset($postArray['page_name']) && trim($postArray['page_name']) != "") {
		   $letters = '';
		   $chats = '';
		   $video_chats = '';
		   $gifts = '';
		   $premiumPhotos = '';
		   $PremiumVideo = '';
		   switch ($postArray['page_name']) {
			   case 'letters':
			   		$letters = Yii::$app->controller->renderPartial('/finance/table-finance-letters.php', [
			   			'users_finance_info' => $postArray['items'],
			   			'type' => 'full_table'
			   		]);
			   		$count = $postArray['items']['letters']['count'];
			   		break;
			   case 'chats':
			   		$chats = Yii::$app->controller->renderPartial('/finance/table-finance-chats.php', [
			   			'users_finance_info' => $postArray['items'],
			   			'type' => 'full_table'
			   		]);
			   		$count = $postArray['items']['chats']['count'];
			   		break;
			   case 'video-chat':
			   		$video_chats = Yii::$app->controller->renderPartial('/finance/table-finance-video-chats.php', [
			   			'users_finance_info' => $postArray['items'],
			   			'type' => 'full_table'
			   		]);
			   		$count = $postArray['items']['videoChats']['count'];
			   		break;
			   case 'gifts':
			   		$gifts = Yii::$app->controller->renderPartial('/finance/table-finance-gift.php', [
			   			'users_finance_info' => $postArray['items'],
			   			'type' => 'full_table'
			   		]);
			   		$count = $postArray['items']['gifts']['count'];
			   		break;
			   case 'premium_photos':
				   $premiumPhotos = Yii::$app->controller->renderPartial('/finance/table-finance-premium-photos.php', [
					   'users_finance_info' => $postArray['items'],
					   'type' => 'full_table'
				   ]);
				   $count = $postArray['items']['premiumPhotos']['count'];
				   break;
			   case 'premium_video':
				   $PremiumVideo = Yii::$app->controller->renderPartial('/finance/table-finance-premium-video.php', [
					   'users_finance_info' => $postArray['items'],
					   'type' => 'full_table'
				   ]);
				   $count = $postArray['items']['PremiumVideo']['count'];
				   break;
		    }
		    if (!empty($count)) {
			   $paginator = Yii::$app->controller->renderPartial('/layouts/parts/pagination.php', [
			   	'letters_count' => $count,
			   	'page' => $postArray['page'],
			   	'limit' => $this->numberOfItemsOnAdminPage,
			   	'page_name' => $postArray['page_name']]);
		    }
	    } else {
		    $letters = Yii::$app->controller->renderPartial('/finance/table-finance-letters.php', [
		   		'users_finance_info' => $postArray['items']
		   	]);
	   	    $chats = Yii::$app->controller->renderPartial('/finance/table-finance-chats.php', [
	   	    	'users_finance_info' => $postArray['items']
	   	    ]);
	   	    $video_chats = Yii::$app->controller->renderPartial('/finance/table-finance-video-chats.php', [
	   	    	'users_finance_info' => $postArray['items']
	   	    ]);
	   	    $gifts = Yii::$app->controller->renderPartial('/finance/table-finance-gift.php', [
	   	    	'users_finance_info' => $postArray['items']
	   	    ]);
			$premiumPhotos = Yii::$app->controller->renderPartial('/finance/table-finance-premium-photos.php', [
				'users_finance_info' => $postArray['items']
			]);
			$PremiumVideo = Yii::$app->controller->renderPartial('/finance/table-finance-premium-video.php', [
				'users_finance_info' => $postArray['items']
			]);
	    }
	           
        $result = [
	        'letters' => $letters,
	        'chats' => $chats,
	        'video_chats' => $video_chats,
	        'gifts' => $gifts,
	        'premiumPhotos' => $premiumPhotos,
	        'PremiumVideo' => $PremiumVideo,
	        'paginator' => $paginator,
        ];
        
        return $result;
    }
    
    public function actionAgency()
    {
		$session = Yii::$app->session;
        
		$dateFrom = Yii::$app->request->post('dateFrom');
        $dateTo = Yii::$app->request->post('dateTo');

        $dateFrom = isset($dateFrom) && $this->validateDate($dateFrom) ? $dateFrom : date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y')));
        $dateTo = isset($dateTo) && $this->validateDate($dateTo) ? $dateTo : date('Y-m-d', time());
        
        $params = [            
            'skinID' => 1,
            'user_type' => self::USER_AGENCY,
            'type' => 'admin',
            'active' => false
        ];
		
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params, "/admin/get-agency");
		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/admin/get-agency")); die;
		if (!$result) {
			return $this->redirect('/manager',301);
		}
		return $this->render('agency', [
			'agencyArray' => $result->agencyArray,
			'dateFrom' => $dateFrom, 
			'dateTo' => $dateTo
		]);
    }
    
    public function actionTotal()
    {
		$session = Yii::$app->session;       
        
		$dateFrom = Yii::$app->request->post('dateFrom');
        $dateTo = Yii::$app->request->post('dateTo');

        $dateFrom = isset($dateFrom) && $this->validateDate($dateFrom) ? $dateFrom : date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y')));
        $dateTo = isset($dateTo) && $this->validateDate($dateTo) ? $dateTo : date('Y-m-d', time());

        $params = [            
            'skinID' => 1,
            'searchParams' => [
	        	'dateFrom' => $dateFrom,
	        	'dateTo' => $dateTo,
			]
        ];
		
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params,"/v1/admin/get-total-finance-report");
		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/get-total-finance-report")); die;		
		if (!$result) {
			return $this->redirect('/manager',301);
		}		
        
		return $this->render('total', [
			'agencyList' => $result->agencyArray->agencyList,
			'dateFrom' => $dateFrom,
			'dateTo' => $dateTo,
			'totalFinanceAllPeriod' => $result->totalFinanceAllPeriod, 
			'totalFinanceFromPeriod' => $result->totalFinanceFromPeriod
		]);
    }
    
    public function actionGenerateTotalFinanceReportList()
    {
    	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$postArray = Yii::$app->request->post();
	    if (empty($postArray) || !isset($postArray['data']) || empty($postArray['data'])) {
	        return ['success' => false, 'message' => 'Missing params'];
	    }
        
        $financeList = Yii::$app->controller->renderPartial('/finance/total.php', ['agency_list' => $postArray['data']]);
        
        return ['success' => true, 'finance_list' => $financeList];
    }
  
  	function validateDate($date, $format = 'Y-m-d')
	{
	    $d = \DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	public function actionAffiliates()
	{
		$session = Yii::$app->session;

		if (!in_array($session['user_type'],[self::USER_SUPERADMIN,self::USER_AFFILIATE])) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager',301);
		}

		$date_from = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
		$date_to   = date('Y-m-d', time());
		$other_user_id = null;

		$post = Yii::$app->request->post();
		if($post){
			$other_user_id = isset($post['other_user_id']) ? (int) $post['other_user_id'] : null;
			$date_from = isset($post['date-from']) && strtotime($post['date-from']) ? $post['date-from'] : date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
			$date_to   = isset($post['date-to']) && strtotime($post['date-to']) ? $post['date-to'] : date('Y-m-d', time());
			//var_dump($date_to);die;
		}

		if($session['user_type'] == self::USER_AFFILIATE){
			$other_user_id = $session['user_id'];
		}

		$params = [
			'user_id'     => $session['user_id'],
			'token'       => $session['user_token'],
			'date_from' => $date_from,
			'date_to' => $date_to,
			'other_user_id' => $other_user_id,
			//'payments_id' => $id,
		];
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params,"/admin/get-affiliates-finance");

		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params,"/admin/get-affiliates-finance")); die;

		$ress_data = [
			'date_from' => $date_from,
			'date_to' => $date_to,
			'user_type' => $session['user_type'],
			'other_user_id' => $other_user_id,
		];
		if ($result) {
			$ress_data['total'] = $result->mass;

		}

		return $this->render('affiliates', $ress_data);

	}
        
	public function actionAgencyPaymentsSchedule()
    {
        $session = Yii::$app->session;

        if (!isset($session['user_token'])) {
            $session->set('message', 'Error token. Please login');

            return $this->redirect('/manager', 301);
        }

        if (!in_array($session['user_type'], [self::USER_AGENCY, self::USER_SUPERADMIN, self::USER_ADMIN])) {
            $session->set('message', 'Access denied');

            return $this->redirect('/manager', 301);
        }

        $ajax = Yii::$app->request->getIsAjax();
		$postArray  = Yii::$app->request->post();
        if ($ajax) {
            $dateFrom = $postArray['dateFrom'];
            $dateTo   = $postArray['dateTo'];
        } else {
            $dateFrom = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y') - 1));
            $dateTo   = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
        }

        if (in_array($session['user_type'], [self::USER_AGENCY, self::USER_ADMIN])) { //для агенства и админа агенства
            $agencyID = $session['agency_id'];
        } else {//для админа
            if ($ajax) {
            	if (!empty($postArray['agency_id'])) {
            		$agencyID = $postArray['agency_id'];
            	} else {
            		return [
            			'success' => false,
            			'code' => 2,
            			'error' => 'Enter agency id'
            		];
            	}                
            } else {
                $params = [
                    'skinID'   => 1,
                    'userType' => 3,
                    'type'      => 'admin',
                    'active'    => FALSE,
                ];

                $result = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-agency"));
                //var_dump(self::sendCurl($session['user_token'], $params, "/admin/get-agency"));die;
                if (!self::checkCurlResponse($result)) {
					return $this->redirect('/manager', 301);
                }
                // if superadmin don't select an agency, then display first agency in the list
                $agencyID = isset($result->agencyArray[0]->user_id) ? $result->agencyArray[0]->user_id : null;        
            }
        }

        $params = [
            'skinID'   => 1,
            'agencyID' => $agencyID,
            'dateFrom' => $dateFrom,
            'dateTo'   => $dateTo,
        ];

        $result2 = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/get-agency-payments-schedule"));
        //var_dump($params);
        //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-agency-payments-schedule")); die;
        if (!$ajax) {
            if (!self::checkCurlResponse($result2)) {
                return $this->redirect('/manager', 301);
            }

            return $this->render('agency-payments-schedule', [
                'agencyList'   => ($session['user_type'] == self::USER_SUPERADMIN) ? $result->agencyArray : $agencyID,
                'dateFrom'     => $dateFrom,
                'dateTo'       => $dateTo,
                'paymentsData' => $result2->paymentsSheduleArray,
            ]);
        } else {
            if (!self::checkCurlResponse($result2)) {
                return json_encode(['success' => FALSE, 'code' => $result2->code, 'message' => $result2->message]);
            }
            $html = Yii::$app->controller->renderPartial('agency-payments-schedule.php', [
                    //'agency_list' => $ress->agency_mass,
                    'dateFrom'     => $dateFrom,
                    'dateTo'       => $dateTo,
                    'paymentsData' => $result2->paymentsSheduleArray,
                ]);

            return json_encode(['success' => TRUE, 'code' => 1, 'html' => $html]);
        }

    }

	public function actionAddAgencyPayments()
    {
        $session = Yii::$app->session;

        if (!isset($session['user_token']) || $session['user_type'] != self::USER_SUPERADMIN) {
            $session->set('message', 'Error token. Please login');

            return $this->redirect('/manager', 301);
        }

        $postArray = Yii::$app->request->post();
        if (!empty($postArray)) {
            $params = [
                'agencyID'  => (int)$postArray['agencyID'],
                'orderID'   => (int)$postArray['orderID'],
                'comments'   => $postArray['comments'],
                'paidValue' => (float)$postArray['paymentValue'],
            ];

            $result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/add-agency-payments"));
			//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/add-agency-payments"));die;

            return json_encode($result);

        }

        $orderID = Yii::$app->request->get('orderID');

        if (!$orderID) {
            $session->set('message', 'Error Order ID');

            return $this->redirect('/manager', 301);
        }
        //var_dump($orderID);die;
        $params = [
            'orderID' => $orderID,
        ];

        $result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/get-agency-payments-schedule"));
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-agency-payments-schedule"));die;

        if (!self::checkCurlResponse($result)) {
            return $this->redirect('/manager', 301);
        }

        //var_dump($ress); die;
        return $this->render('add-agency-payments', [
            'order_info' => $result->paymentsSheduleArray,
        ]);
    }

    public function actionTransferAgencyPayments()
    {
        $session = Yii::$app->session;

        if (!isset($session['user_token']) || $session['user_type'] != self::USER_SUPERADMIN) {
            $session->set('message', 'Error token. Please login');

            return $this->redirect('/manager', 301);
        }

        $orderID = Yii::$app->request->get('orderID');

        if (!$orderID) {
            $session->set('message', 'Error Order ID');

            return $this->redirect('/manager', 301);
        }

        $params = [
            'orderID' => $orderID,
        ];

        $result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/transfer-agency-payments"));
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/transfer-agency-payments"));die;

        if (!self::checkCurlResponse($result)) {
            return $this->redirect('/manager', 301);
        }

        $session->set('message', $result->message);

        return $this->redirect('/manager/finance/agency-payments-schedule', 301);
    }

}
