<?php

namespace app\modules\manager\controllers;

use Yii;
use app\modules\manager\controllers\ManagerController;
use yii\web\BadRequestHttpException;
use yii\helpers\HtmlPurifier;

class LettersController extends ManagerController
{
	public $numberOfMessagesOnAdminPage;
	
    public function init()
    {
        $this->serverUrl = Yii::$app->params['serverUrl'];
        $this->numberOfMessagesOnAdminPage = Yii::$app->params['numberOfMessagesOnAdminPage'];
        $this->view->registerJsFile('/js/admin/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/message.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    }
    
    public function beforeAction($action)
	{            	        
        $session = Yii::$app->session;
        if ($session['user_type'] == self::USER_ADMIN && (!isset($session['permissions']) || $session['permissions']['letter_access'] == ManagerController::DENY)) {
            $session->set('message', 'Access is denied.');
            return $this->redirect('/manager', 301);
        } 
        if ($session['user_type'] == self::USER_SITEADMIN && (!isset($session['permissions']) || $session['permissions']['letter_access'] == ManagerController::DENY)) {
            $session->set('message', 'Access is denied.');
            return $this->redirect('/manager', 301);
        }         
		$arrayOfActionsWithDisabledCSRFValidation = ['generate-list', 'answer'];
	    if (in_array($action->id, $arrayOfActionsWithDisabledCSRFValidation)) {
	        $this->enableCsrfValidation = false;
	    }

	    return parent::beforeAction($action);
	}
    
    public function actionIndex()
    {        
        return $this->redirect('/manager/letters/inbox');
    }
        	
	public function actionInbox($otherUserID = null)
    {
		$session = Yii::$app->session;
		
		$page = 1;
		$params = [
	        'limit'=> $this->numberOfMessagesOnAdminPage,
	        'offset'=> 0,
	        'searchParams'=>['type'=>'inbox', 'answered' => 0],
        ];
		
		if($otherUserID !== null) {
			$params['otherUserID'] = $otherUserID;
		}
		if ($session['user_type'] == self::USER_AGENCY || $session['user_type'] == self::USER_ADMIN) {
            $result2 = $this->getResponseFromAPI($session['user_token'], $params, "/admin/get-agency");
            //var_dump(self::sendCurl($session['user_token'], $params, "/admin/get-agency")); die;        
            if (!$result2) {
                return $this->redirect('/manager', 301);
            }
            $agencyUsers = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/get-users-info-from-agency");
            //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-users-info-from-agency")); die;        
            if (!$agencyUsers) {
                return $this->redirect('/manager', 301);
            }            
        }
        

		$result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/letters"));
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/letters")); die;        
        if ((isset($result->success) && $result->success == false) || 
           (isset($result->status) && ($result->status == 401 || $result->status == 404 || $result->status == 500))) {
            $session->set('message', $result->message);
            return $this->redirect('/manager', 301);
        }

        $result3 = json_decode(self::getCurl($session['user_token'], "/v1/admin/agency-notifications"));
        //var_dump(self::getCurl($session['user_token'], "/v1/admin/agency-notifications")); die;
        if ((isset($result3->success) && $result3->success == false) || 
            (isset($result3->status) && ($result3->status == 401 || $result3->status == 404 || $result3->status == 500))) {
            $session->set('message', $result3->message);
            $this->actionLogout();
        }

        return $this->render('inbox', [
        	'lettersArray' => $result->lettersArray,
        	'lettersCount' => $result->countLetters,
        	'page' => $page,
        	'numberOfMessagesOnAdminPage' => $this->numberOfMessagesOnAdminPage,
        	'otherUserID' => $otherUserID,
            'unrepliedLetters' => $result3->unrepliedLetters,
            'agency_id' => isset($result2->agencyArray[0]->id) ? $result2->agencyArray[0]->id : null,
            'agencyUsers' => (isset($agencyUsers) && isset($agencyUsers->usersArray)) ? $agencyUsers->usersArray : null
        ]);
		
    }
    
    public function actionAnswer($letterID)
    {
        $session = Yii::$app->session;

        $postArray = Yii::$app->request->post();

        if (!empty($postArray)) {            
            $fromUserID = (isset($postArray['fromUserID']) && is_numeric($postArray['fromUserID'])) ? (int)$postArray['fromUserID'] : null;
            $toUserID = (isset($postArray['toUserID']) && is_numeric($postArray['toUserID'])) ? (int)$postArray['toUserID'] : null;
            $themeID = (isset($postArray['themeID']) && is_numeric($postArray['themeID'])) ? (int)$postArray['themeID'] : null;
            $messageID = (isset($postArray['messageID']) && is_numeric($postArray['messageID'])) ? (int)$postArray['messageID'] : null;

            if (!$fromUserID || !$toUserID) {
                $session->set('message', 'Cant send letter. User receiver not defined.');
                return $this->redirect('/manager/message/', 301);
            }

            if (trim($postArray['description']) == "") {
                $session->set('message', 'Enter letter description.');
                return $this->redirect('/manager/letter/' . $letterID, 301);
            }

            $params = [
                'fromUserID'        => $fromUserID,
                'toUserID'          => $toUserID,
                'desc'                => trim($postArray['description']),
                'title'             => trim($postArray['caption']),
                'themeID' => $themeID,
                'previousLetterID' => $messageID,
                'skinID' => 1
            ];                    

            $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/letter/send-letter-from-admin");
            //var_dump(self::sendCurl($session['user_token'], $params, "/v1/letter/send-letter-from-admin"));die;
            if (!self::checkCurlResponse($result)) {
                return $this->redirect('/manager/message/', 301);
            }
            
            $session->set('message', 'Letter was send');

            return $this->redirect('/manager/letters/inbox/', 301);
        }
        
        $params = [                    
            'searchParams' => [
                'letterID' => $letterID, 
                'type' => 'inbox'
            ],
        ];

        $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/letters");
        //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/letters")); die;
        if (!$result) {
            return $this->redirect('/manager', 301);
        }
        if (empty($result->lettersArray)) {
            $session->set('message', 'Wrong message Id');

            return $this->redirect('/manager/message/', 301);
        }
        
        if (isset($result->lettersArray[0]->user_to_id)) {
            $readMessageScript = "window['messageAdminLibrary']['readMessage']($letterID, {$result->lettersArray[0]->user_to_id});";
            $this->view->registerJs($readMessageScript, yii\web\View::POS_READY);
        }
        
        $this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('answer', [
            'letterArray' => $result->lettersArray, 
            'countMessage' => $result->countLetters
        ]);

    }

    public function actionOutbox($otherUserID = null)
    {
		$session = Yii::$app->session;

		$page = 1;
		$params = [
	        'limit' => $this->numberOfMessagesOnAdminPage,
	        'offset' => 0,
	        'searchParams' => ['type' => 'outbox'],
        ];
		
		if ($otherUserID !== null) {
			$params['otherUserID'] = $otherUserID;
		}
		
		$result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/letters"); 
        if (!$result) {
            return $this->redirect('/manager', 301);
        }
        if ($session['user_type'] == self::USER_AGENCY || $session['user_type'] == self::USER_ADMIN) {
            $agencyUsers = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/get-users-info-from-agency");
            //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-users-info-from-agency")); die;        
            if (!$agencyUsers) {
                return $this->redirect('/manager', 301);
            }  
        }

        return $this->render('outbox', [
        	'lettersArray' => $result->lettersArray,
        	'lettersCount' => $result->countLetters,
        	'page' => $page,
        	'numberOfMessagesOnAdminPage' => $this->numberOfMessagesOnAdminPage,
        	'otherUserID' => $otherUserID,
            'agencyUsers' => (isset($agencyUsers) && isset($agencyUsers->usersArray)) ? $agencyUsers->usersArray : null
        ]);		
    }
    
    public function actionGenerateList()
    {
		$postArray = Yii::$app->request->post();
        if (empty($postArray) || !isset($postArray['lettersArray']) || empty($postArray['lettersArray'])) {
            return json_encode(['success' => true, 'table' => '']);
        }

        if (empty($postArray) || empty($postArray['inbox'])) {
            $inbox = false;
        } else {
            $inbox = true;
        }
        
        $messages = Yii::$app->controller->renderPartial('table.php', [
        	'lettersArray' => $postArray['lettersArray'], 
        	'lettersCount' => $postArray['countLetters'],
            'inbox' => $inbox
        ]);
        $pagination = Yii::$app->controller->renderPartial('/layouts/parts/pagination.php', [
        	'letters_count' => $postArray['countLetters'],
        	'page' => $postArray['page'],
        	'limit' => $this->numberOfMessagesOnAdminPage,
        	'page_name' => 'outbox'
        ]);
        
        $result = [
	        'messageList' => $messages,
	        'pagination' => $pagination
        ];
        
        return json_encode($result);
    }

    public function actionGenerateLettersListForChat()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $postArray = Yii::$app->request->post();
        if (empty($postArray) || !isset($postArray['lettersData']['lettersArray']) || 
            empty($postArray['lettersData']['lettersArray'])) {
            return [
                'messageList' => '<p>No letters</p>'
            ];
        }

        $messages = Yii::$app->controller->renderPartial('table-for-multi-chat.php', [
                        'lettersArray' => $postArray['lettersData']['lettersArray'], 
                        'lettersCount' => $postArray['lettersData']['countLetters'], 
                        'type' => $postArray['type']
                    ]);

        $result = [
            'messageList' => $messages,
        ];

        return $result;
    }

	public function actionHistory($letterID)
	{
		$session = Yii::$app->session;

		$params = [
			'letterID' => $letterID,
		];

		$result = json_decode($this->getCurl($session['user_token'], "/v1/letter/get-history/" . $letterID));
		//var_dump($this->getCurl($session['user_token'], "/v1/letter/get-history/" . $letterID)); die;
        if (isset($result->status) && $$result->status == 401) {
            $session->set('message', $giftTypes['message']); 
            return $this->redirect('/', 301);
        }
        $lettersResult = [];
        $i=0;
        foreach ($result->lettersArray as $row) {
            $lettersResult[] = $row;
            $lettersResult[$i]->theme = HtmlPurifier::process($row->theme);
            $lettersResult[$i]->description = HtmlPurifier::process($row->description);
            $i++;
        }

		return $this->render('history', [
			'lettersArray'=>$lettersResult,
			'lettersCount'=>$result->lettersCount,
            'numberOfHistoryMessages' => 5,
            'letterID' => $letterID
		]);

	}	        
       
}
