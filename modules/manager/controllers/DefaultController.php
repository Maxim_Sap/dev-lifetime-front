<?php

namespace app\modules\manager\controllers;

use Yii;
use yii\helpers\VarDumper;
use app\modules\manager\controllers\ManagerController;
use yii\web\BadRequestHttpException;
use yii\helpers\Json;

class DefaultController extends ManagerController
{
	public $numberOfItemsOnAdminPage;
	public $numberOfTasksOnAdminPage;
	public $numberOfGiftItemsOnBackend;
	public $numberOfLogItemsOnBackend;
	public $numberOfBlogItems;

    public function init()
    {
		parent::init();
        $this->numberOfItemsOnAdminPage = Yii::$app->params['numberOfItemsOnAdminPage'];
		$this->numberOfTasksOnAdminPage = Yii::$app->params['numberOfTasksOnAdminPage'];
		$this->numberOfGiftItemsOnBackend = Yii::$app->params['numberOfGiftItemsOnBackend'];
		$this->numberOfBlogItems = Yii::$app->params['numberOfBlogItems'];
		$this->numberOfLogItemsOnBackend = 10;
        $this->view->registerJsFile('/js/admin/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    }
    
    public function beforeAction($action)
	{            	    
	    $arrayOfActionsWithDisabledCSRFValidation = [
	    		'generate-activity-list','generate-financy-list', 'user',
				'my-girls','girls','my-man','pricelist','agency-pricelist', 'login-user',
				'settings','get-album-photo','gift-list','edit-shop-item', 'add-gift',
				'add-penalty','penalty-table','my-admins','log','my-translators','referral-stats','my-agency'];
	    if (in_array($action->id, $arrayOfActionsWithDisabledCSRFValidation)) {
	        $this->enableCsrfValidation = false;
	    }

	    return parent::beforeAction($action);
	}
    
    public function actionIndex()
    {				
        $session = Yii::$app->session;                   
        if(!isset($session['user_id']) || !isset($session['user_token'])) {
			return $this->redirect('/manager/login', 301);
        }
        
        $params = [
	        'userID' => $session['user_id'],
	        'token' => $session['user_token']
        ];        

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-user-info"));die;
		if (!$result) {
			return $this->actionLogout();
        }

		if (!in_array($result->userData->userType, [self::USER_AGENCY, self::USER_INTERPRETER, self::USER_AFFILIATE, self::USER_SUPERADMIN, self::USER_ADMIN, self::USER_SITEADMIN])) {
			$session->set('message', 'Error user type. Access denied.');
			return $this->actionLogout();
        }
        $session->set('avatar', $result->userData->avatar->small);
		$session->set('user_type', $result->userData->userType);
		if (!isset($session['agency_id']) && isset($result->userData->agencyID) && is_numeric($result->userData->agencyID)) {
			$session->set('agency_id', $result->userData->agencyID);
		}

    	$postArray = Yii::$app->request->post();
        if ($postArray) {
            $dateFrom = isset($postArray['dateFrom']) && strtotime($postArray['dateFrom']) ? $postArray['dateFrom'] : date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
            $dateTo   = isset($postArray['dateTo']) && strtotime($postArray['dateTo']) ? $postArray['dateTo'] : $dateTo = date('Y-m-d', time());
            if (strtotime($dateFrom) > strtotime($dateTo)) {
                $session->set('message', 'Error date format');
                return $this->redirect('/manager', 301);
            }
        } else {
            $dateFrom =date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
	    	$dateTo =date('Y-m-d', time());
        }

	    $params = [
            'skinID' => 1,
            'searchParams' => [
        		'dateFrom' => $dateFrom,
        		'dateTo' => $dateTo,
			]
        ];

		//var_dump($params);die;
		if ($session['user_type'] == self::USER_SUPERADMIN || $session['user_type'] == self::USER_SITEADMIN) {
			// get statistics
			if (in_array($session['user_type'], [self::USER_SITEADMIN]) && (!isset($session['permissions']) || $session['permissions']["finance_access"] == ManagerController::DENY)) {
				$agencyList = [];
				$financeAllPeriod = [];
				$financeFromPeriod = [];
			} else {
				$result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/get-total-finance-report"));
				//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-total-finance-report"));die;
				if(!self::checkCurlResponse($result)) {				
					$this->actionLogout();
		        }
		        $agencyList = $result->agencyArray->agencyList;
				$financeAllPeriod = $result->totalFinanceAllPeriod;
				$financeFromPeriod = $result->totalFinanceFromPeriod;
			}

						
			$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

			return $this->render('index', [
				'agencyList' => $agencyList,
				'totalFinanceAllPeriod' => $financeAllPeriod, 
				'totalFinanceFromPeriod' => $financeFromPeriod

			]); 
			
		} elseif (in_array($session['user_type'], [self::USER_AGENCY, self::USER_ADMIN])) {

			$params['agencyID'] = isset($session['agency_id']) ? $session['agency_id'] : $session['user_id'];			
			if (in_array($session['user_type'], [self::USER_ADMIN]) && (!isset($session['permissions']) || $session['permissions']["finance_access"] == ManagerController::DENY)) {
				$agencyFinanceInfo = [];
			} else {
				$result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/get-agency-finance-info"));
				//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-agency-finance-info")); die;
				if (!self::checkCurlResponse($result)) {				
					$this->actionLogout();
		        }
		        $agencyFinanceInfo = $result->agencyArray;
			}
			

	        $result2 = json_decode(self::getCurl($session['user_token'], "/v1/admin/agency-notifications"));
	        //var_dump(self::getCurl($session['user_token'], "/v1/admin/agency-notifications")); die;
	        if (isset($result2->status) && ($result2->status == 401 || $result2->status == 404 || $result2->status == 500)) {
	        	$session->set('message', $result2->message);
	        	$this->actionLogout();
	        }	        
			
			$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
			return $this->render('index', ['agencyList' => $agencyFinanceInfo, 'dateFrom' => $dateFrom, 'date_to' => $dateTo, 'unrepliedLetters' => $result2->unrepliedLetters, 'unfinishedTasks' => $result2->unfinishedTasks, 'unsendGifts' => $result2->unsendGifts, 'notApprovedChatTemplates' => $result2->notApprovedChatTemplates, 'notApprovedMessageTemplates' => $result2->notApprovedMessageTemplates]);
		}
		return $this->render('index');
    }
    
    public function actionLogin()
    {
        $this->layout = 'register';
        return $this->render('login');
    }
    
    public function actionForgot()
    {
        $this->layout = 'register';
        return $this->render('forgot');
    }
    
    /*
    public function actionRegister()
    {       
        $this->layout = 'register';
        return $this->render('register');
    }    
    */
       
    public function actionLoginUser()
    {
    	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$postArray = Yii::$app->request->post();
        if (empty($postArray)) {        	
			return [
				'success' => false, 
				'code' => 1, 
				'message' => 'Missing params'
			];
        }
        if (!isset($postArray['email']) || trim($postArray['email']) == "" || 
        	!isset($postArray['password']) || trim($postArray['password']) == "") {        	
        	return [
        		'success' => false, 
        		'code' => 1, 
        		'message' => 'Missing params'
        	];
        }
        
        $result = json_decode(self::sendCurl(Yii::$app->session['user_token'], $postArray, "/user/login"));
        //var_dump(self::sendCurl(Yii::$app->session['user_token'], $postArray, "/user/login")); die;
		if (isset($result->success) && $result->success == false && $result->code == "Validation errors") {
			return [
				'success' => false, 
				'code' => $result->code,
				'errors' => $result->errors				
			];
		} elseif (isset($result->success) && $result->success == false) {
			return [
				'success' => false, 
				'code' => $result->code, 
				'message' => $result->message
			];
		}
		
		self::UserAuthorization($result->userInfo);
			
		return [
			'success' => true,
			'user_id' => $result->userInfo->userID
		];
    }

    public function actionChangePassword()
    {
       /* $get = Yii::$app->request->get();
        if (!empty($get)) {
            if (!isset($get['new_pass']) || trim($get['new_pass']) == "" || !isset($get['code']) || trim($get['code']) == "" || !isset($get['email']) || trim($get['email']) == "") {
                throw new BadRequestHttpException('Missing params');
            }
		}*/
		$session = Yii::$app->session;

		$result = json_decode(self::sendCurl($session['user_token'], $get, "/reg-email/change-password-check"));
		
		    if (!$result) {
				throw new BadRequestHttpException('Error send request');
		    }
		    if (!isset($result->success) || $result->success == false) {
				
				$errorMessage = isset($result->message) ? $result->message : "Error send request";
				$session->set('message', $errorMessage);
				return $this->redirect('/manager', 301);
		    }
		    
		    self::UserAuthorization($result->user_info);

		  	$session = Yii::$app->session;
			$session->set('message', 'Password change');
			
		return $this->redirect('/manager/', 301);
    }   
   
    public function actionMyAgency()
    {
        $session = Yii::$app->session;
        
        if ($session['user_type'] != self::USER_SUPERADMIN) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager',301);
		}
        
		/*$params = [
            'skinID' => 1,
            'userType' => self::USER_AGENCY,
            'userID' => $session['user_id'],
            'token' => $session['user_token'],
            'type' => 'admin',
            'status' => false,
            'byPeriodOnly' => true,
        ];*/

		$dateFrom = Yii::$app->request->post('dateFrom');
		$dateTo = Yii::$app->request->post('dateTo');

		$dateFrom = isset($dateFrom) && $this->validateDate($dateFrom) ? $dateFrom : date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y')));
		$dateTo = isset($dateTo) && $this->validateDate($dateTo) ? $dateTo : date('Y-m-d', time());

		$sortBy = Yii::$app->request->post('sortBy');
		$sortBy = !empty($sortBy) ? $sortBy : 'profile-count';

		$sortWay = Yii::$app->request->post('sortWay');
		$sortWay = !empty($sortWay) ? $sortWay : 'down';


		$params = [
			'skinID' => 1,
			//'agencyOnly' => true,
			'searchParams' => [
				'dateFrom' => $dateFrom,
				'dateTo' => $dateTo,
				'sortBy' => $sortBy,
				'sortWay' => $sortWay,
			]
		];
		
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
 
		//$result = $this->getResponseFromAPI($session['user_token'], $params, "/admin/get-agency");
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-agencies-with-finance-info")); die;

			// get statistics

			$result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/get-agencies-with-finance-info"));
			//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-agencies-with-finance-info"));die;
			if(!self::checkCurlResponse($result)) {
				//var_dump($result);die;
				return $this->redirect('/manager', 301);
			}

			$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
			$this->view->registerJsFile('/lib/table/table.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
			$this->view->registerCssFile('/lib/table/table.css');

			return $this->render('my-agency2', [
				'agencyArray' => $result->agencyArray->agencyList,
				//'totalFinanceFromPeriod' => $result->totalFinanceFromPeriod,
				'count' => 100,
				'dateFrom' => $dateFrom,
				'dateTo' => $dateTo,
				'sortBy' => $sortBy,
				'sortWay' => $sortWay,
				'page' => 1,
				'page_name' => 'my-agency',
				'limit' => 30
			]);

    }

    public function actionMyGirls()
    {
        $session = Yii::$app->session;        

		$params = [
			'user_id'=> $session['user_id'],
			'token'=>$session['user_token']
		];

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}		

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
		
		if (!$result) {
			return $this->actionLogout();
		}

		if (!in_array($result->userData->userType, [self::USER_AGENCY, self::USER_INTERPRETER, self::USER_SUPERADMIN, self::USER_ADMIN, self::USER_SITEADMIN])) {
			$session->set('message', 'Error user type. Access is denied.');
			return $this->actionLogout();
		}
		if ($result->userData->userType == self::USER_ADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}
		if ($result->userData->userType == self::USER_SITEADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}        
        $ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		if ($ajax) {
			$page = Yii::$app->request->post('page');
		}
		if (empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfItemsOnAdminPage;
        
		$dateFrom = Yii::$app->request->post('dateFrom');
    	$dateTo = Yii::$app->request->post('dateTo');
    	$sortBy = Yii::$app->request->post('sortBy');
    	$sortWay = Yii::$app->request->post('sortWay'); 
    	$userId = Yii::$app->request->post('userId'); 
    	$userName = Yii::$app->request->post('userName'); 
    	$userAgeFrom = Yii::$app->request->post('userAgeFrom'); 
    	$userAgeTo = Yii::$app->request->post('userAgeTo'); 
    	$userCity = Yii::$app->request->post('userCity');
    	$userCountry = Yii::$app->request->post('userCountry');
    	$activityStatus = Yii::$app->request->post('activityStatus');

		$userId = isset($userId) && is_string($userId) ? $userId : '';
		$userName = isset($userName) && is_string($userName) ? $userName : '';
    	$userAgeFrom = isset($userAgeFrom) && is_numeric($userAgeFrom) ? (int)$userAgeFrom : 18; 
    	$userAgeTo = isset($userAgeTo) && is_numeric($userAgeTo) ? (int)$userAgeTo : 65; 
    	$userCity = isset($userCity) && is_string($userCity) ? $userCity : '';
    	$userCountry = isset($userCountry) && is_string($userCountry) ? $userCountry : '';
    	$activityStatus = isset($activityStatus) && is_string($activityStatus) ? $activityStatus : 'all';
   	
    	$params = [
    		'searchParams' => [
    			'dateFrom' => isset($dateFrom) && strtotime($dateFrom) ? $dateFrom : date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y'))),
				'dateTo' => isset($dateTo) && strtotime($dateTo) ? $dateTo . ' 23:59:59' : date('Y-m-d', time()) . ' 23:59:59',
				'sortWay' => (isset($sortWay) && in_array($sortWay, ['up'])) ? $sortWay : 'down',
				'sortBy' => (isset($sortBy) && in_array($sortBy, ['status', 'name', 'age', 'id'])) ? $sortBy : 'status',
				'agency_id' => (isset($result->userData->agencyID)) ? $result->userData->agencyID : null,
    		]
    	];
    	//var_dump($params['searchParams']['sortWay']); die; 
		$result2 = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/get-girls-with-payments");
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-girls-with-payments")); die;
        if (!$result2) {
        	return $this->redirect('/manager', 301);	
        }
        $sortedResult = $this->userFilterAndSort($result2->girlsList, [
        	'userId' => $userId,
        	'userAgeFrom' => $userAgeFrom,
        	'userAgeTo' => $userAgeTo,
        	'userName' => $userName,
        	'userCity' => $userCity,
        	'userCountry' => $userCountry,
        	'activityStatus' => $activityStatus
        ], [
        	'sortBy' => $params['searchParams']['sortBy'],
        	'sortWay' => $params['searchParams']['sortWay']
        ]);
        $temp = $sortedResult['users'];
		//var_dump($temp); die;
		if (count($temp) > $this->numberOfItemsOnAdminPage) {
			$splice = array_chunk($temp, $this->numberOfItemsOnAdminPage);
		} else {
			$splice[0] = $temp;
		}        
        $context['girlsArray'] = $splice[$page-1];
        $context['dateFrom'] = $params['searchParams']['dateFrom'];
        $context['dateTo'] = str_replace(' 23:59:59', '', $params['searchParams']['dateTo']);
        $context['sortBy'] = $params['searchParams']['sortBy'];
        $context['sortWay'] = $params['searchParams']['sortWay'];
        $context['userId'] = $userId;
        $context['userName'] = $userName;
        $context['userAgeFrom'] = $userAgeFrom;
        $context['userAgeTo'] = $userAgeTo;
        $context['userCity'] = $userCity;
        $context['userCountry'] = $userCountry;
        $context['active'] = $sortedResult['active'];
        $context['notActive'] = $sortedResult['notActive'];
        $context['online'] = $sortedResult['online'];	
        $context['activityStatus'] = $activityStatus;
        $context['count'] = count($temp);
        $context['page'] = $page;

        if (!$ajax) {
        }	
        		
        //var_dump($context); die;
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		
		if ($ajax) {
			$items = Yii::$app->controller->renderPartial('my-girls-v2.php', $context);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $items;
		}
		return $this->render('my-girls-v2', $context);         
    }
    
/*    public function actionMyGirls()
    {
        $session = Yii::$app->session;        

		$params = [
			'user_id'=> $session['user_id'],
			'token'=>$session['user_token']
		];

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}		

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
		
		if (!$result) {
			return $this->actionLogout();
		}

		if (!in_array($result->userData->userType, [self::USER_AGENCY, self::USER_INTERPRETER, self::USER_AFFILIATE, self::USER_SUPERADMIN, self::USER_ADMIN])) {
			$session->set('message', 'Error user type. Access is denied.');
			return $this->actionLogout();
		}
        
        $ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		if ($ajax) {
			$page = Yii::$app->request->post('page');
		}
		if (empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfItemsOnAdminPage;
        
		if ($session['user_type'] == self::USER_SUPERADMIN) {
			$type = 'from_admin';
		} else {
			$type = 'from_agency';
		}

		$statusParams = \Yii::$app->request->post('params');

		$params = [
            'limit' => $this->numberOfItemsOnAdminPage,
            'offset'=> $offset,
            'skinID' => 1,
            'userType' => self::USER_FEMALE,
            'userID' => $session['user_id'],
            'agencyID' => (isset($result->userData->agencyID)) ? $result->userData->agencyID : null,
            'type' => $type,
            'notRemoveStatusParam' => false,
			'statusParams' => $statusParams,
			'params' => 'girls',
			'visibility' => 'all'
        ];
		
		$result = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-users-for-admin"));
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-users-for-admin")); die;
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
        }
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		
		if ($ajax) {
			$items = Yii::$app->controller->renderPartial('my-girls.php', [
				'girls' => $result->girls,
				'count' => $result->count,
				'page' => $page,
				'pageName' => 'my-girls',
				'show_token_button' => true,
				'statusArray' => $result->statusArray,
				'curr_search_status' => $statusParams
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $items;
		}
		return $this->render('my-girls', [
			'girls' => $result->girls,
			'count' => $result->count,
			'page' => $page,
			'pageName' => 'my-girls',
			'show_token_button' => true,
			'statusArray' => $result->statusArray,
			'curr_search_status' => $statusParams
		]);
        
    }*/

	public function actionMyAdmins()
	{
		$session = Yii::$app->session;		

		$params = [
			'user_id'=> $session['user_id'],
			'token'=>$session['user_token']
		];

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");

		if(!$result){
			return $this->actionLogout();
		}

		if (!in_array($result->userData->userType, [self::USER_AGENCY])) {
			$session->set('message', 'Error user type. Access is denied.');
			return $this->actionLogout();
		}

		$ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		if ($ajax) {
			$page = Yii::$app->request->post('page');
		}
		if (empty($page) or $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfItemsOnAdminPage;

		$type = 'from_agency';

		$statusParams = \Yii::$app->request->post('params');

		$params = [
			'limit' => $this->numberOfItemsOnAdminPage,
			'offset' => $offset,
			'skinID' => 1,
			'userType' => self::USER_ADMIN,
			'userID' => $result->userData->userID,
			'agencyID' => (isset($result->userData->agencyID)) ? $result->userData->agencyID : null,
			'type' => $type,
			'notRemoveStatusParam' => false,
			'statusParams'=> $statusParams,
			'params'=> 'admin',
			'visibility' => 'all'
		];

		$result = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-users-for-admin"));
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-users-for-admin")); die;
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
		}
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		if ($ajax) {
			$items = Yii::$app->controller->renderPartial('my-admin.php', [
				'admins' => $result->girls,
				'count' => $result->count,
				'page' => $page,
				'pageName' => 'my-admins',
				'show_token_button' => true,
				'statusArray' => $result->statusArray,
				'curr_search_status' => $statusParams
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return $items;
		}

		return $this->render('my-admin', [
			'admins' => $result->girls,
			'count' => $result->count,
			'page' => $page,
			'pageName' => 'my-admins',
			'show_token_button' => true,
			'statusArray' => $result->statusArray,
			'curr_search_status' => $statusParams
		]);

	}

	public function actionMyInterpreters()
	{
		$session = Yii::$app->session;

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$params = [
			'token' => $session['user_token']
		];

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-user-info")); die;
		if (!$result) {
			return $this->actionLogout();
        }

		if (!in_array($result->userData->userType, [self::USER_AGENCY, self::USER_ADMIN, self::USER_SUPERADMIN, self::USER_SITEADMIN])) {
			$session->set('message', 'Error user type. Access is denied.');
			return $this->actionLogout();
		}
		if (($result->userData->userType == self::USER_ADMIN || $result->userData->userType == self::USER_SITEADMIN) && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}

		$ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		if ($ajax) {
			$page = Yii::$app->request->post('page');
		}
		if (empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfItemsOnAdminPage;

		$type = 'from_agency';

		$statusParams = Yii::$app->request->post('params');

		$params = [
			'limit'   => $this->numberOfItemsOnAdminPage,
			'offset'=> $offset,
			'skinID' => 1,
			'userType' => self::USER_INTERPRETER,
			'userID' => $result->userData->userID,
			'agencyID' => isset($result->userData->agencyID) ? $result->userData->agencyID : null,
			'type' => $type,
			'notRemoveStatusParam' => false,
			'statusParams'=> $statusParams,
			'params'=> 'interpreters',
			'visibility' => 'all'
		];

		$result = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-users-for-admin"));
		//var_dump($result); die;
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
		}
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		if ($ajax) {
			$items = Yii::$app->controller->renderPartial('my-interpreters.php', [
				'girls' => $result->girls,
				'count' => $result->count,
				'page' => $page,
				'pageName' => 'my-interpreters',
				'show_token_button' => true,
				'statusArray' => $result->statusArray,
				'curr_search_status' => $statusParams
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return $items;
		}
		
		return $this->render('my-interpreters', [
			'girls' => $result->girls,
			'count' => $result->count,
			'page' => $page,
			'pageName' => 'my-interpreters',
			'show_token_button' => true,
			'statusArray' => $result->statusArray,
			'curr_search_status' => $statusParams
		]);

	}

	public function actionAdmins()
	{
		$session = Yii::$app->session;

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		if($session['user_type'] != self::USER_SUPERADMIN) {
			$session->set('message', 'Error user type. Access is denied.');
			return $this->redirect('/manager/', 301);
		}

		$ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		if ($ajax) {
			$page = Yii::$app->request->post('page');
		}

		if (empty($page) or $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfItemsOnAdminPage;

		$type = 'from_admin';

		$statusParams = \Yii::$app->request->post('params');

		$params = [
			'limit' => $this->numberOfItemsOnAdminPage,
			'offset' => $offset,
			'skinID' => 1,
			'userType' => self::USER_SITEADMIN,
			'params' => 'admin',
			'userID' => $session['user_id'],
			'type' => $type,
			'notRemoveStatusParam' => false,
			'statusParams' => $statusParams,
			'visibility' => 'all'
		];


		$result2 = json_decode(self::sendCurl($session['user_token'], $params,"/user/get-users-for-admin"));
		//var_dump($result2); die;
		if (!self::checkCurlResponse($result2)) {
			return $this->redirect('/manager', 301);
		}
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		if ($ajax) {
			//var_dump($result2); die;
			$items = Yii::$app->controller->renderPartial('admins.php', [
				'girls' => $result2->girls,
				'count' => $result2->count,
				'page' => $page,
				'pageName' => 'admins',
				'show_token_button' => true,
				'statusArray' => $result2->statusArray,
				'curr_search_status' => $statusParams
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return $items;
		}

		return $this->render('admins', [
			'girls' => $result2->girls,
			'count' => $result2->count,
			'page' => $page,
			'pageName' => 'admins',
//			'agencyArray' => $result->agencyArray,
			'show_token_button' => true,
			'statusArray' => $result2->statusArray,
			'curr_search_status' => $statusParams
		]);

	}

	public function actionInterpreters()
	{
		$session = Yii::$app->session;

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		if($session['user_type'] != self::USER_SUPERADMIN && $session['user_type'] != self::USER_SITEADMIN) {
			$session->set('message', 'Error user type. Access is denied.');
			return $this->redirect('/manager/', 301);
		}

		$ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		if ($ajax) {
			$page = Yii::$app->request->post('page');
			$agencyID = Yii::$app->request->post('agencyID');
		} else {
			$params = [
				'skinID' => 1,
				'userType' => self::USER_AGENCY,
				'token' => $session['user_token'],
				'type' => 'admin',
				'notRemoveStatusParam' => false
			];

			$result = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-agency"));
			//var_dump($result); die;
			if (!self::checkCurlResponse($result) || empty($result->agencyArray)) {
				return $this->redirect('/manager', 301);
			}
			$agencyID = $result->agencyArray[0]->id;
		}

		if (!is_numeric($agencyID)) {
			return $this->redirect('/manager', 301);
		}

		if (empty($page) or $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfItemsOnAdminPage;

		$type = 'from_agency';

		$statusParams = \Yii::$app->request->post('params');

		$params = [
			'limit' => $this->numberOfItemsOnAdminPage,
			'offset' => $offset,
			'skinID' => 1,
			'userType' => self::USER_INTERPRETER,
			'params' => 'interpreters',
			'userID' => $session['user_id'],
			'agencyID' => $agencyID,
			'type' => $type,
			'notRemoveStatusParam' => false,
			'statusParams' => $statusParams,
			'visibility' => 'all'
		];


		$result2 = json_decode(self::sendCurl($session['user_token'], $params,"/user/get-users-for-admin"));
		//var_dump($result2); die;
		if (!self::checkCurlResponse($result2)) {
			return $this->redirect('/manager', 301);
		}
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		if ($ajax) {
			//var_dump($result2); die;
			$items = Yii::$app->controller->renderPartial('interpreters.php', [
				'girls' => $result2->girls,
				'count' => $result2->count,
				'page' => $page,
				'pageName' => 'interpreters',
				'show_token_button' => true,
				'statusArray' => $result2->statusArray,
				'curr_search_status' => $statusParams
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return $items;
		}

		return $this->render('interpreters', [
			'girls' => $result2->girls,
			'count' => $result2->count,
			'page' => $page,
			'pageName' => 'interpreters',
			'agencyArray' => $result->agencyArray,
			'show_token_button' => true,
			'statusArray' => $result2->statusArray,
			'curr_search_status' => $statusParams
		]);

	}
    
	public function actionGirlVideo()
	{
		$session = Yii::$app->session;
        
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
        
        if ($session['user_type'] != ManagerController::USER_SUPERADMIN) {
			return $this->redirect('/manager/', 301);
		}

        $ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		if ($ajax) {
			$page = Yii::$app->request->post('page');
			$agencyID = Yii::$app->request->post('agencyID');
		} else {
			$params = [
	            'skinID' => 1,
	            'userType' => self::USER_AGENCY,
	            'user_id' => $session['user_id'],
	            'token' => $session['user_token'],
	            'type' => 'admin',
	            'active' => false
	        ];
			
			$result = json_decode(self::sendCurl($session['user_token'], $params,"/admin/get-agency"));
	        //var_dump($result); die;
	        if (!self::checkCurlResponse($result) || empty($result->agencyArray)) {
				return $this->redirect('/manager', 301);
	        }
	        $agencyID = $result->agencyArray[0]->id;	        
		}
		//var_dump($agencyID);die;
		if (!is_numeric($agencyID)) {
			return $this->redirect('/manager', 301);
		}

		if (empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfItemsOnAdminPage;
        
		$type = 'from_agency';

		$statusParams = \Yii::$app->request->post('params');
		$statusParams['cam_online'] = 1;

		$params = [
            'limit' => $this->numberOfItemsOnAdminPage,
            'offset'=> $offset,
            'skinID' => 1,
            'userType' => self::USER_FEMALE,
            'userID' => $session['user_id'],
            'agencyID' => $agencyID,
            'params' => 'girls',            
            'type' => $type,
            'notRemoveStatusParam' => false,
            'statusParams' => $statusParams,
			'visibility' => 'all'
        ];

		$result2 = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-users-for-admin"));
		//var_dump(self::sendCurl($session['user_token'], $params,"/user/get-users-for-admin")); die;
		if (!self::checkCurlResponse($result2)) {
			return $this->redirect('/manager', 301);
        }
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		
		if ($ajax) {
			$items = Yii::$app->controller->renderPartial('girl-video.php', [
				'girls'=>$result2->girls,
				'count'=>$result2->count,
				'page'=>$page,
				'pageName'=>'girls',
				'show_token_button'=>true,
				'statusArray'=>$result2->statusArray,
				'curr_search_status'=>$statusParams
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $items;
		}
		return $this->render('girl-video', [
			'girls'=>$result2->girls,
			'count'=>$result2->count,
			'page'=>$page,
			'pageName'=>'girls',
			'agencyArray'=>$result->agencyArray,
			'show_token_button'=>true,
			'statusArray'=>$result2->statusArray,
			'curr_search_status'=>$statusParams
		]);        
	}

	public function actionMyGirlVideo()
	{
		$session = Yii::$app->session;
        
		$params = [
			'user_id'=> $session['user_id'],
			'token'=>$session['user_token']
		];

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
		
		if (!$result) {
			return $this->actionLogout();
		}
        
		if (!in_array($result->userData->userType, [self::USER_AGENCY, self::USER_SUPERADMIN, self::USER_ADMIN, self::USER_SITEADMIN])) {
			$session->set('message', 'Error user type. Access is denied.');
			return $this->actionLogout();
		}
		if ($result->userData->userType == self::USER_ADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}
		if ($result->userData->userType == self::USER_SITEADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}
        $ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		if ($ajax) {
			$page = Yii::$app->request->post('page');
		}
		if (empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfItemsOnAdminPage;
        
		if ($session['user_type'] == self::USER_SUPERADMIN) {
			$type = 'from_admin';
		} else {
			$type = 'from_agency';
		}

		$statusParams = \Yii::$app->request->post('params');
		$statusParams['cam_online'] = 1;

		$params = [
            'limit' => $this->numberOfItemsOnAdminPage,
            'offset'=> $offset,
            'skinID' => 1,
            'userType' => self::USER_FEMALE,
            'userID' => $session['user_id'],
            'agencyID' => (isset($result->userData->agencyID)) ? $result->userData->agencyID : null,
            'params' => 'girls',            
            'type' => $type,
            'notRemoveStatusParam' => false,
            'statusParams' => $statusParams,
			'visibility' => 'all'
        ];

		$result2 = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-users-for-admin"));
		//var_dump(self::sendCurl($session['user_token'], $params,"/user/get-users-for-admin")); die;
		if (!self::checkCurlResponse($result2)) {
			return $this->redirect('/manager', 301);
        }
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		
		if ($ajax) {
			$items = Yii::$app->controller->renderPartial('my-girl-video.php', [
				'girls'=>$result2->girls,
				'count'=>$result2->count,
				'page'=>$page,
				'pageName'=>'girls',
				'show_token_button'=>true,
				'statusArray'=>$result2->statusArray,
				'curr_search_status'=>$statusParams
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $items;
		}
		return $this->render('my-girl-video', [
			'girls'=>$result2->girls,
			'count'=>$result2->count,
			'page'=>$page,
			'pageName'=>'girls',
			'show_token_button'=>true,
			'statusArray'=>$result2->statusArray,
			'curr_search_status'=>$statusParams
		]);        
	}

    public function actionGirls()
    {
        $session = Yii::$app->session;
        
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
        
        if ($session['user_type'] != ManagerController::USER_SUPERADMIN) {
			return $this->redirect('/manager/', 301);
		}

        $ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		$agencyID = Yii::$app->request->post('agencyID');
		if ($ajax) {
			$page = Yii::$app->request->post('page');			
		} else {
			$params = [
	            'skinID' => 1,
	            'userType' => self::USER_AGENCY,
	            'user_id' => $session['user_id'],
	            'token' => $session['user_token'],
	            'type' => 'admin',
	            'active' => false
	        ];
			
			$result = json_decode(self::sendCurl($session['user_token'], $params,"/admin/get-agency"));
	        //var_dump($result); die;
	        if (!self::checkCurlResponse($result) || empty($result->agencyArray)) {
				return $this->redirect('/manager', 301);
	        }
	        if (empty($agencyID)) {
	        	$agencyID = $result->agencyArray[0]->id;
	        }	        	        
		}
		//var_dump($agencyID);die;
		if (!is_numeric($agencyID)) {
			return $this->redirect('/manager', 301);
		}

		if (empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfItemsOnAdminPage;
        
		$dateFrom = Yii::$app->request->post('dateFrom');
    	$dateTo = Yii::$app->request->post('dateTo');
    	$sortBy = Yii::$app->request->post('sortBy');
    	$sortWay = Yii::$app->request->post('sortWay'); 
    	$userId = Yii::$app->request->post('userId'); 
    	$userName = Yii::$app->request->post('userName'); 
    	$userAgeFrom = Yii::$app->request->post('userAgeFrom'); 
    	$userAgeTo = Yii::$app->request->post('userAgeTo'); 
    	$userCity = Yii::$app->request->post('userCity');
    	$userCountry = Yii::$app->request->post('userCountry');
    	$activityStatus = Yii::$app->request->post('activityStatus');

		$userId = isset($userId) && is_string($userId) ? $userId : '';
		$userName = isset($userName) && is_string($userName) ? $userName : '';
    	$userAgeFrom = isset($userAgeFrom) && is_numeric($userAgeFrom) ? (int)$userAgeFrom : 18; 
    	$userAgeTo = isset($userAgeTo) && is_numeric($userAgeTo) ? (int)$userAgeTo : 65; 
    	$userCity = isset($userCity) && is_string($userCity) ? $userCity : '';
    	$userCountry = isset($userCountry) && is_string($userCountry) ? $userCountry : '';
    	$activityStatus = isset($activityStatus) && is_string($activityStatus) ? $activityStatus : 'all';
   	
    	$params = [
    		'searchParams' => [
    			'dateFrom' => isset($dateFrom) && strtotime($dateFrom) ? $dateFrom : date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y'))),
				'dateTo' => isset($dateTo) && strtotime($dateTo) ? $dateTo . ' 23:59:59' : date('Y-m-d', time()) . ' 23:59:59',
				'sortWay' => (isset($sortWay) && in_array($sortWay, ['up'])) ? $sortWay : 'down',
				'sortBy' => (isset($sortBy) && in_array($sortBy, ['status', 'name', 'age', 'id'])) ? $sortBy : 'status',
				'agency_id' => $agencyID
    		]
    	];
    	//var_dump($params['searchParams']['sortWay']); die; 
		$result2 = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/get-girls-with-payments");
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-girls-with-payments")); die;
        if (!$result2) {
        	return $this->redirect('/manager', 301);	
        }
        $sortedResult = $this->userFilterAndSort($result2->girlsList, [
        	'userId' => $userId,
        	'userAgeFrom' => $userAgeFrom,
        	'userAgeTo' => $userAgeTo,
        	'userName' => $userName,
        	'userCity' => $userCity,
        	'userCountry' => $userCountry,
        	'activityStatus' => $activityStatus
        ], [
        	'sortBy' => $params['searchParams']['sortBy'],
        	'sortWay' => $params['searchParams']['sortWay']
        ]);
        $temp = $sortedResult['users'];
		//var_dump($temp); die;
		if (count($temp) > $this->numberOfItemsOnAdminPage) {
			$splice = array_chunk($temp, $this->numberOfItemsOnAdminPage);
		} else {
			$splice[0] = $temp;
		}        
        $context['girlsArray'] = $splice[$page-1];
        $context['dateFrom'] = $params['searchParams']['dateFrom'];
        $context['dateTo'] = str_replace(' 23:59:59', '', $params['searchParams']['dateTo']);
        $context['sortBy'] = $params['searchParams']['sortBy'];
        $context['sortWay'] = $params['searchParams']['sortWay'];
        $context['userId'] = $userId;
        $context['userName'] = $userName;
        $context['userAgeFrom'] = $userAgeFrom;
        $context['userAgeTo'] = $userAgeTo;
        $context['userCity'] = $userCity;
        $context['userCountry'] = $userCountry;
        $context['active'] = $sortedResult['active'];
        $context['notActive'] = $sortedResult['notActive'];
        $context['online'] = $sortedResult['online'];	
        $context['activityStatus'] = $activityStatus;
        $context['count'] = count($temp);
        $context['page'] = $page;        

        if (!$ajax) {
        	$context['agencyID']  = $agencyID;
        	$context['agencyArray'] = $result->agencyArray;
        }	
        		
        //var_dump($context); die;
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		
		if ($ajax) {
			$items = Yii::$app->controller->renderPartial('girls-v2.php', $context);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $items;
		}
		return $this->render('girls-v2', $context);        
    }

/*    public function actionGirls()
    {
        $session = Yii::$app->session;
        
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
        
        if ($session['user_type'] != ManagerController::USER_SUPERADMIN) {
			return $this->redirect('/manager/', 301);
		}

        $ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		if ($ajax) {
			$page = Yii::$app->request->post('page');
			$agencyID = Yii::$app->request->post('agencyID');
		} else {
			$params = [
	            'skinID' => 1,
	            'userType' => self::USER_AGENCY,
	            'user_id' => $session['user_id'],
	            'token' => $session['user_token'],
	            'type' => 'admin',
	            'active' => false
	        ];
			
			$result = json_decode(self::sendCurl($session['user_token'], $params,"/admin/get-agency"));
	        //var_dump($result); die;
	        if (!self::checkCurlResponse($result) || empty($result->agencyArray)) {
				return $this->redirect('/manager', 301);
	        }
	        $agencyID = $result->agencyArray[0]->id;	        
		}
		//var_dump($agencyID);die;
		if (!is_numeric($agencyID)) {
			return $this->redirect('/manager', 301);
		}

		if (empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfItemsOnAdminPage;
        
		$type = 'from_agency';

		$statusParams = \Yii::$app->request->post('params');

		$params = [
            'limit' => $this->numberOfItemsOnAdminPage,
            'offset'=> $offset,
            'skinID' => 1,
            'userType' => self::USER_FEMALE,
            'userID' => $session['user_id'],
            'agencyID' => $agencyID,
            'params' => 'girls',            
            'type' => $type,
            'notRemoveStatusParam' => false,
			'statusParams' => $statusParams,
			'visibility' => 'all'
        ];

		$result2 = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-users-for-admin"));
		//var_dump(self::sendCurl($session['user_token'], $params,"/user/get-users-for-admin")); die;
		if (!self::checkCurlResponse($result2)) {
			return $this->redirect('/manager', 301);
        }
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		
		if ($ajax) {
			$items = Yii::$app->controller->renderPartial('girls.php', [
				'girls'=>$result2->girls,
				'count'=>$result2->count,
				'page'=>$page,
				'pageName'=>'girls',
				'show_token_button'=>true,
				'statusArray'=>$result2->statusArray,
				'curr_search_status'=>$statusParams
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $items;
		}
		return $this->render('girls', [
			'girls'=>$result2->girls,
			'count'=>$result2->count,
			'page'=>$page,
			'pageName'=>'girls',
			'agencyArray'=>$result->agencyArray,
			'show_token_button'=>true,
			'statusArray'=>$result2->statusArray,
			'curr_search_status'=>$statusParams
		]);        
    }*/

	public function actionAddUser()
    {
        $session = Yii::$app->session;
		if (!isset($session['user_token'])) {
			$session->set('message', 'Error token. Please login');
			return $this->redirect('/manager', 301);
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		
		$userTypeFromGetArray = Yii::$app->request->get('user_type');
		$userType = !empty($userTypeFromGetArray) && is_numeric($userTypeFromGetArray) ? $userTypeFromGetArray : self::USER_FEMALE; //women as default
		
		if ($session['user_type'] == self::USER_ADMIN) { //agency admin can create only women
			$userType = self::USER_FEMALE;
		} elseif ($session['user_type'] == self::USER_AGENCY) { //for agency
			if (!in_array($userType, [self::USER_FEMALE, self::USER_INTERPRETER, self::USER_ADMIN])) { //women, translater, agency admin
				$session->set('message', 'Error user type');
				return $this->redirect('/manager', 301);
			}
			
		} elseif ($session['user_type'] == self::USER_SUPERADMIN) {
			
		} else {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager', 301);
		}

		// get agency list
		$params = [
			'skin_id' => 1,
			'user_type' => self::USER_AGENCY,
			'user_id' => $session['user_id'],
			'token' => $session['user_token'],			
			'approve_status' => 4
		];

		$result = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-agency"));
		
		if (!self::checkCurlResponse($result) || empty($result->agencyArray)) {
			return $this->redirect('/manager', 301);
		}

		$params = [
			'skin_id' => 1,			
			'user_id' => $session['user_id'],
			'token' => $session['user_token'],						
		];

		$result2 = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-user-types"));
		
		if (!self::checkCurlResponse($result2)) {
			return $this->redirect('/manager', 301);
		}
		
        return $this->render('add-user', [
        	'userType' => $userType,
        	'agencyArray' => $result->agencyArray,
        	'userTypes' => $result2->userTypes
        ]);
        
    }

	public function actionSignalizator()
	{
		$session = Yii::$app->session;

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$params = [
			'token' => $session['user_token']
		];

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
		//var_dump($result); die;
		if (!$result) {
			return $this->actionLogout();
        }

        if (!in_array($result->userData->userType, [self::USER_AGENCY, self::USER_SUPERADMIN, self::USER_ADMIN])) {
        	return $this->redirect('/manager',301);
        }
		if ($result->userData->userType == self::USER_ADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}
		if ($result->userData->userType == self::USER_SITEADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}		
		$params = [
			'skinID' => 1,
			'userType' => self::USER_FEMALE,
			'userID' => $result->userData->userID, 
			//get agency ID
			'agencyID' => isset($result->userData->agencyID) ? $result->userData->agencyID : null,
			'type' => 'from_agency',
			'params' => 'girls',
			'visibility' => 'all'
		];		

		$result = $this->getResponseFromAPI($session['user_token'], $params,"/user/get-users-for-admin");
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-users-for-admin")); die;
		//var_dump($result); die;
		if(!$result) {
			return $this->redirect('/manager',301);
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('signalizator', [
			'girls' => $result->girls,
			'count' => $result->count
		]);
	}

	function actionGetUserToken()
	{

		$session = Yii::$app->session;
		$otherUserID = Yii::$app->request->post('otherUserID');
		$skinID = Yii::$app->request->post('skinID');

		$params = [
	        'token' => $session['user_token'],
	        'otherUserID' => $otherUserID,
	        'skinID' => $skinID
        ];		       
        
		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info"); 
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-user-info")); die;
		if (!$result) {
			return $this->redirect('/manager', 301);
		}
				
		$session->set('user_type', $result->userData->userType);
        
        if (!in_array($result->userData->userType, [self::USER_FEMALE, self::USER_INTERPRETER])) {
			$session->set('message', 'Error user type. Access denied.');
			unset($session['user_id']);
			unset($session['user_token']);
			setcookie('user_token', "", time()-3600,"/");
			return $this->redirect('/manager', 301);
        }
        if (!empty($result->userData->token)) {
        	self::UserAuthorization($result->userData);
        }

        $userBalance = $result->userData->balance;
	    if (empty($userBalance)) {
			$userBalance = "0.00";
	    } else {			
			$userBalance = $result->userData->balance;
	    }
        
        if ($result->userData->avatar->small != null) {
			$userAvatar = $this->serverUrl . "/" . $result->userData->avatar->small;
	    } else {
			$userAvatar = "/img/no_avatar_small.jpg";
	    }
	    
	    if ($result->userData->avatar->normal != null) {
			$result->userData->avatar->normal = $this->serverUrl . "/" . $result->userData->avatar->normal;
	    } else {
			$result->userData->avatar->normal = "/img/no_avatar_normal.jpg";
	    }
        
        $userData = [
	        'userID' => $result->userData->personalInfo->id,
	        'userType' => $result->userData->userType,
	        'name' => $result->userData->personalInfo->first_name,
	        'avatar' => $userAvatar,
			'avatarPhotoID' => $result->userData->avatarPhotoID,
	        'email'=> $result->userData->email,
            'balance' => (float)$userBalance,
	        'camOnline' => $result->userData->cam_online,
        ];

        if ($result->userData->userType == self::USER_INTERPRETER) {
        	$userData['last_name'] =  $result->userData->personalInfo->last_name;
        }
        
        $session->set('user_data', $userData);       		
		
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		if ($result->userData->userType == self::USER_FEMALE) {
			return [
        		'success' => true,
        		'type'    => 'girl'
        	];
		} else {
			return [
        		'success' => true,
        	];			
		}
        
	}

	public function actionSignalizatorMen()
	{
		$session = Yii::$app->session;

		if (!isset($session['user_token']) || $session['user_type'] != ManagerController::USER_SUPERADMIN) {
			return $this->redirect('/manager/login', 301);
		}

		$params = [
			'skinID' => 1,
			'userType' => 1,			
			'type' => 'fake-men',
			'visibility' => 'all'
		];

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-users-for-admin");
		//var_dump($result); die;
		if (!$result) {
			return $this->redirect('/', 301);
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('signalizator-men', [
			'girls' => $result->girls,
			'count' => $result->count
		]);
	}

	public function actionEditUser($otherUserID)
    {
        $session = Yii::$app->session;		
		
		$params = [
	        'userID'=> $session['user_id'],
	        'token' => $session['user_token'],
	        'otherUserID' => $otherUserID,	        
        ];
        
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-user-info")); die;
		if (!$result) {
			return $this->actionLogout();
        }

        $result->userData->avatar->normal = (!empty($result->userData->avatar->normal)) ? $this->serverUrl . "/" . $result->userData->avatar->normal : "/img/no_avatar_normal.jpg";

        if ($session['user_id'] == $result->userData->userID) {
        	$session->set('avatar', $result->userData->avatar->small);
        	if (isset($result->userData->personalInfo->first_name) && isset($result->userData->personalInfo->last_name)) {
        		$session->set('userName', $result->userData->personalInfo->first_name . ' ' . $result->userData->personalInfo->last_name);
        	} elseif (isset($result->userData->personalInfo->first_name)) {
        		$session->set('userName', $result->userData->personalInfo->first_name);
        	} elseif (isset($result->userData->agency_info->name)) {
				$session->set('userName', $result->userData->agency_info->name);
			}elseif(isset($result->userData->affiliates_info->name)) {
				$session->set('userName', $result->userData->affiliates_info->name);
			} else {
        		$session->set('userName', 'Noname');
        	}
        }        
        //var_dump($session['user_type'] == self::USER_ADMIN); die;
		if ($result->userData->userID != $session['user_id'] && $session['user_type'] == self::USER_ADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}
		if ($result->userData->userID != $session['user_id'] && $session['user_type'] == self::USER_SITEADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}
		if (!in_array($result->userData->userType, [self::USER_INTERPRETER, self::USER_ADMIN])) {  // interpreters and admins can't download document foto to profile			
			$this->view->registerJs('window["adminAccountLibrary"]["getAlbumPhoto"]("document");', Yii\web\View::POS_READY);
		}
		$this->view->registerMetaTag(['http-equiv'=>"Cache-Control",'content'=>"no-cache"]);
		$this->view->registerCssFile('/plagins/jquery.imgareaselect-0.9.10/css/imgareaselect-default.css');
		$this->view->registerJsFile('/plagins/jquery.imgareaselect-0.9.10/js/jquery.imgareaselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/crop_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.maskedinput.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJs('$("input[name=\"m_phone\"]").mask("+99(999) 999-9999");', Yii\web\View::POS_READY);

		$content = [
        	'userData' => $result->userData,
        	'countryList' => $result->countryList,
        	'approve_statuses' => $result->approve_statuses,
        	'girlsArray' => $result->girlsArray
        ];

        if (in_array($result->userData->userType, [self::USER_AGENCY, self::USER_ADMIN])) {
        	$content['agencyList'] = (!empty($result->agencyList)) ? $result->agencyList : [];
        }
        
        if (in_array($result->userData->userType, [self::USER_ADMIN, self::USER_SITEADMIN])) {        	
        	$content['permissions'] = $this->getAdminPermissions($result->userData->permissions, $result->userData->userType);        	
        }

        if (in_array($result->userData->userType, [self::USER_AGENCY])) {
        	$content['agencyRuleViolations'] = (!empty($result->userData->agencyRuleViolations)) ? $result->userData->agencyRuleViolations : [];
        }

		if (in_array($result->userData->userType, [self::USER_MALE, self::USER_FEMALE])) {
			$hairColors = JSON::decode($this->getCurl($session['user_token'], '/v1/hair-colors'));
		
			if (isset($hairColors['status']) && $hairColors['status'] == 401) {
				$session->set('message', $hairColors['message']); 
				return $this->redirect('/', 301);
			}

			$eyesColors = JSON::decode($this->getCurl($session['user_token'], '/v1/eyes-colors'));
			if (isset($eyesColors['status']) && $eyesColors['status'] == 401) {
				$session->set('message', $eyesColors['message']); 
				return $this->redirect('/', 301);
			}
			$physiques = JSON::decode($this->getCurl($session['user_token'], '/v1/physiques'));
			if (isset($physiques['status']) && $physiques['status'] == 401) {
				$session->set('message', $physiques['message']); 
				return $this->redirect('/', 301);
			}
			$content['hairColors'] = $hairColors;
			$content['eyesColors'] = $eyesColors;
			$content['physiques'] = $physiques;			

		}
        //var_dump($content['agencyRuleViolations']); die;
        return $this->render('edit-user', $content);
        
    }

	public function actionGetAlbumPhoto()
	{
		$session = Yii::$app->session;
		if (!isset($session['user_token'])) {
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return [
				'success' => false, 
				'message' => 'Missing params'
			];
		}

		$otherUserID = \Yii::$app->request->post('otherUserID');
		$albumName = \Yii::$app->request->post('album');
		if (!isset($otherUserID) && !is_numeric($otherUserID)) {
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return ['success' => false, 'message' => 'Missing params'];
		}
		if (!isset($albumName) && trim($albumName) == '') {
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return ['success' => false, 'message' => 'Missing params'];
		}

		$params = [			
			'token' => $session['user_token'],
			'otherUserID' => $otherUserID,
			'albumName' => $albumName,
		];

		$result = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-album-photo"));
		
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/manager', 301);
		}

		if (!empty($result->photos)) {
			$photos = Yii::$app->controller->renderPartial('/layouts/parts/attach-photos.php', [
				'photos' => $result->photos,
				'place' => $albumName
			]);		
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;	
			return ['success'=>true, 'photos' => $photos];
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return ['success' => true, 'photos' => null];
	}


	public function actionEditPassword($otherUserID)
    {
        $session = Yii::$app->session;
		if (!isset($session['user_token'])) {
			$session->set('message', 'Error token. Please login');
			return $this->redirect('/manager', 301);
		}

		$params = [	        
	        'token' => $session['user_token']
        ];

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
		
		if (!$result) {
			return $this->actionLogout();
        }

		if (!in_array($result->userData->userType, [self::USER_AGENCY, self::USER_INTERPRETER, self::USER_AFFILIATE, self::USER_SUPERADMIN, self::USER_ADMIN, self::USER_SITEADMIN])) {
			$session->set('message', 'Error user type. Access denied.');
			return $this->actionLogout();
        }
		if ($result->userData->userID != $session['user_id'] && $session['user_type'] == self::USER_ADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}       
		if ($result->userData->userID != $session['user_id'] && $session['user_type'] == self::USER_SITEADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		} 
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		
		return $this->render('edit-password', [
			'otherUserID' => $otherUserID
		]); 
	}
	
	public function actionDeleteUser($otherUserID)
    {
        $session = Yii::$app->session;

		$params = [	       
	        'otherUserID' => $otherUserID
        ];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/admin/delete-user");
		//var_dump(self::sendCurl($session['user_token'], $params, "/admin/delete-user")); die;
		
		if (!$result) {
			return $this->redirect('/manager', 301);
        }		

        $session->set('message', 'User ID: ' . $otherUserID . ' delete');

		$redirectUrl = (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/manager/my-girls');

		return $this->redirect($redirectUrl, 301);
        
	}
	
	public function actionGallery($otherUserID)
    {        
        $session = Yii::$app->session;
		
		$params = [
	        'otherUserID' => $otherUserID
        ];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI($session['user_token'], $params,"/user/get-albums-list-info");
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-albums-list-info")); die;

		if (!$result) {
			return $this->redirect('/manager', 301);
        }
		
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/crop_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		

		return $this->render('gallery', [
			'albums' => $result->albums,
			'otherUserID' => $otherUserID
		]);
	}
	
	public function actionAlbum($albumID, $otherUserID)
    {
        $session = Yii::$app->session;
        
        $params = [	        
	        'albumID' => $albumID,
	        'otherUserID' => $otherUserID
        ];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI($session['user_token'], $params, "/admin/get-photos");
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-photos")); die;

		if (!$result) {
			return $this->redirect('/manager', 301);
        }
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/crop_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('album', [
			'photos' => $result->photos,
			'albumInfo' => $result->albumInfo
		]);
    }
    
    public function actionPhoto($photoID, $otherUserID)
    {
        $session = Yii::$app->session;       
		
        $params = [	        	        
	        'photoID' => $photoID,
	        'otherUserID' => $otherUserID
        ];

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$result = $this->getResponseFromAPI($session['user_token'], $params,"/v1/photo/get-one-photo");
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/photo/get-one-photo")); die;

		if (!$result) {
			return $this->redirect('/manager', 301);
        }

		$this->view->registerMetaTag(['http-equiv'=>"Cache-Control",'content'=>"no-cache"]);
		$this->view->registerCssFile('/plagins/jquery.imgareaselect-0.9.10/css/imgareaselect-default.css');
		$this->view->registerJsFile('/plagins/jquery.imgareaselect-0.9.10/js/jquery.imgareaselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/crop_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('photo', [
			'photo' => $result->photo
		]);
    }
    
    public function actionPhotoDelete($photoID)
    {
        $session = Yii::$app->session;        
		
        $params = [	        
	        'photoID' => $photoID,	        
        ];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/photo/delete-photo");
		
		$session->set('message', 'Photo deleted');
		return $this->redirect('/manager/gallery/' . $result->userID, 301);		
    }
	
	public function actionUser($otherUserID)
    {
        $session = Yii::$app->session;      
		
        $params = [	        
	        'token' => $session['user_token'],
	        'otherUserID' => $otherUserID
        ];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-other-user-info");
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-other-user-info")); die;
        if (!$result) {
        	return $this->redirect('/manager', 301);	
        }

        $result->userData->avatar->normal = (!empty($result->userData->avatar->normal)) ? $this->serverUrl . "/" . $result->userData->avatar->normal : "/img/no_avatar_normal.jpg";
        $userAvatar = (!empty($result->userData->avatar->small)) ? $this->serverUrl . "/" . $result->userData->avatar->small : "/img/no_avatar_small.jpg";
        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        //var_dump($result->userData); die;
        if (in_array($result->userData->user_type, [self::USER_ADMIN, self::USER_SITEADMIN])) {        	
        	$result->userData->permissions = $this->getAdminPermissions($result->userData->permissions, $result->userData->user_type);
        }        
        if ($otherUserID != $session['user_id'] && $session['user_type'] == self::USER_ADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		} 
        if ($otherUserID != $session['user_id'] && $session['user_type'] == self::USER_SITEADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}		 
        $context = ['userData' => $result->userData];        
        if ($result->userData->user_type == self::USER_AGENCY) {
        	$dateFrom = Yii::$app->request->post('dateFrom');
        	$dateTo = Yii::$app->request->post('dateTo');
        	$sortBy = Yii::$app->request->post('sortBy');
        	$sortWay = Yii::$app->request->post('sortWay'); 
        	$userId = Yii::$app->request->post('userId'); 
        	$userName = Yii::$app->request->post('userName'); 
        	$userAgeFrom = Yii::$app->request->post('userAgeFrom'); 
        	$userAgeTo = Yii::$app->request->post('userAgeTo'); 
        	$userCity = Yii::$app->request->post('userCity');
        	$userCountry = Yii::$app->request->post('userCountry');
        	$activityStatus = Yii::$app->request->post('activityStatus');

			$userId = isset($userId) && is_string($userId) ? $userId : '';
			$userName = isset($userName) && is_string($userName) ? $userName : '';
        	$userAgeFrom = isset($userAgeFrom) && is_numeric($userAgeFrom) ? (int)$userAgeFrom : 18; 
        	$userAgeTo = isset($userAgeTo) && is_numeric($userAgeTo) ? (int)$userAgeTo : 65; 
        	$userCity = isset($userCity) && is_string($userCity) ? $userCity : '';
        	$activityStatus = isset($activityStatus) && is_string($activityStatus) ? $activityStatus : 'all';
       	
        	$params = [
        		'searchParams' => [
        			'dateFrom' => isset($dateFrom) && strtotime($dateFrom) ? $dateFrom : date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y'))),
					'dateTo' => isset($dateTo) && strtotime($dateTo) ? $dateTo . ' 23:59:59' : date('Y-m-d', time()) . ' 23:59:59',
					'sortWay' => (isset($sortWay) && in_array($sortWay, ['up'])) ? $sortWay : 'down',
					'sortBy' => (isset($sortBy) && in_array($sortBy, ['status', 'name', 'age', 'id'])) ? $sortBy : 'status',
					'agency_id' => $result->userData->agency_info->id
        		]
        	];
        	//var_dump($params['searchParams']['sortWay']); die; 
			$result2 = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/get-girls-with-payments");
			//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-girls-with-payments")); die;
	        if (!$result2) {
	        	return $this->redirect('/manager', 301);	
	        }
	        $sortedResult = $this->userFilterAndSort($result2->girlsList, [
	        	'userId' => $userId,
	        	'userAgeFrom' => $userAgeFrom,
	        	'userAgeTo' => $userAgeTo,
	        	'userName' => $userName,
	        	'userCity' => $userCity,
	        	'userCountry' => $userCountry,
	        	'activityStatus' => $activityStatus
	        ], [
	        	'sortBy' => $params['searchParams']['sortBy'],
	        	'sortWay' => $params['searchParams']['sortWay']
	        ]);
			//var_dump($temp); die;
	        $context['girlsArray'] = $sortedResult['users'];
	        $context['dateFrom'] = $params['searchParams']['dateFrom'];
	        $context['dateTo'] = str_replace(' 23:59:59', '', $params['searchParams']['dateTo']);
	        $context['sortBy'] = $params['searchParams']['sortBy'];
	        $context['sortWay'] = $params['searchParams']['sortWay'];
	        $context['userId'] = $userId;
	        $context['userName'] = $userName;
	        $context['userAgeFrom'] = $userAgeFrom;
	        $context['userAgeTo'] = $userAgeTo;
	        $context['userCity'] = $userCity;
	        $context['userCountry'] = $userCountry;
	        $context['active'] = $sortedResult['active'];
	        $context['notActive'] = $sortedResult['notActive'];
	        $context['online'] = $sortedResult['online'];	
	        $context['activityStatus'] = $activityStatus;        

        }
        //var_dump($context); die;
        return $this->render('view-user', $context);        
    }

    private function userFilterAndSort($inputArray, $filterArray, $sortArray)
    {
		$temp = [];
		$girlIds = [];
		$active = 0;
		$notActive = 0;
		$online = 0;
		foreach ($inputArray as $girl) {
			if (!in_array($girl->id, $girlIds)) {
				$temp[$girl->id] = (array)$girl;										
			} else {
				$temp[$girl->id]['amount'] += $girl->amount;
			}
			$girlIds[] = $girl->id;
		}
		$temp1 = $temp;
		$i=0;
		foreach ($temp as $girl) {
			if (($girl['birthday'] <= date('Y-m-d', strtotime('-' .$filterArray['userAgeTo']. ' year')) || $girl['birthday'] >= date('Y-m-d', strtotime('-' .$filterArray['userAgeFrom']. ' year'))) ) {	
				if ($girl['birthday'] != null) {
					unset($temp1[$girl['id']]);
				}				
			}

			if ($filterArray['userId'] != '' && stripos($girl['id'], $filterArray['userId']) === FALSE) {
				unset($temp1[$girl['id']]);
			}
			if ($filterArray['userName'] != '' && stripos($girl['first_name'] . ' ' . $girl['last_name'], $filterArray['userName']) === FALSE) {
				unset($temp1[$girl['id']]);
			}				
			if ($filterArray['userCity'] != '' && stripos($girl['city_name'], $filterArray['userCity']) === FALSE) {
				unset($temp1[$girl['id']]);
			}
			if ($filterArray['userCountry'] != '' && stripos($girl['country_name'], $filterArray['userCountry']) === FALSE) {
				unset($temp1[$girl['id']]);
			}							
			$i++;
		}		

		//var_dump($temp1); die;
		$temp = $temp1;		
		$temp1 = [];
		$i=0;
		foreach ($temp as $girl) {
			if ($girl['last_activity'] >= time() - 30 && in_array($girl['status'], [self::STATUS_ACTIVE])) {	
				$online++;
			}
			if (in_array($girl['status'], [self::STATUS_ACTIVE])) {
				$active++;
			}
			if (in_array($girl['status'], [self::STATUS_NO_ACTIVE, self::STATUS_DELETED])) {
				$notActive++;
			}			
			if ($filterArray['activityStatus'] == 'online' && in_array($girl['status'], [self::STATUS_ACTIVE]) && $girl['last_activity'] >= time() - 30) {
				$temp1[$i] = $girl;
			} elseif ($filterArray['activityStatus'] == 'active' && in_array($girl['status'], [self::STATUS_ACTIVE])) {
				$temp1[$i] = $girl;
			} elseif ($filterArray['activityStatus'] == 'notActive' && in_array($girl['status'], [self::STATUS_NO_ACTIVE, self::STATUS_DELETED])) {
				$temp1[$i] = $girl;
			} elseif ($filterArray['activityStatus'] == 'all') {
				$temp1[$i] = $girl;
			}
			$i++;
		}
		$temp = $temp1;
		if (!empty($temp)) {
			if ($sortArray['sortBy'] == 'age') {
				$sortBy = 'birthday';
			} elseif ($sortArray['sortBy'] == 'name') {
				$sortBy = 'first_name';
			} else {
				$sortBy = $sortArray['sortBy'];
			}
			if ($sortArray['sortWay'] == 'down') {
	            $sortWay = SORT_DESC;
	        } else {
	            $sortWay = SORT_ASC;
	        }
			if ($sortBy == 'birthday') {
				if ($sortArray['sortWay'] == 'down') {
		            $sortWay = SORT_ASC;
		        } else {
		            $sortWay = SORT_DESC;
		        }
	        }	        

			foreach ($temp as $key => $row) {
			    $amount[$key]  = $row['amount'];
			    ${$sortBy}[$key] = $row[$sortBy];
			}

			array_multisort($$sortBy, $sortWay, $amount, SORT_DESC, $temp);
		}
		return [
			'users' => $temp,
			'online' => $online,
			'active' => $active,
			'notActive' => $notActive
		];
    }
    
    public function actionActivity($otherUserID=null)
    {
        $session = Yii::$app->session;
        
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$params = [	        
	        'token' => $session['user_token']
        ];

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
		
		if (!$result) {
			return $this->actionLogout();
        }

        $dateFrom =date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y')));
        $dateTo = date('Y-m-d', time());
        $typeFromGetArray = Yii::$app->request->get('type');
        
        $userType = null;
        
        if (!empty($typeFromGetArray)) {
			switch($typeFromGetArray) {
				case 'girls':
					$userType = self::USER_FEMALE;
					break;
				case 'men':
					$userType = self::USER_MALE;
					break;
				case 'agency':
					$userType = self::USER_AGENCY;
					break;
			}
		}

		$agencyID = isset($result->userData->agencyID) ? $result->userData->agencyID : $result->userData->userID;
		$agencyArray = [];
		
		if ($result->userData->userType == self::USER_SUPERADMIN) {
			$params = [
		        'skinID' => 1,
		        'userType' => self::USER_AGENCY,
		        'userID' => $result->userData->userID,
		        'token' => $session['user_token'],
		        'type' => 'admin',
		        'notRemoveStatusParam' => false
		    ];
				
			$result = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-agency"));
		    
		    if (!self::checkCurlResponse($result) || empty($result->agencyArray)) {
				return $this->redirect('/manager', 301);
		    }
		    $agencyID = $result->agencyArray[0]->id;
		    $agencyArray = $result->agencyArray;
		}
		
        $params = [	        	        
	        'otherUserID' => $otherUserID,
	        'limit' => $this->numberOfItemsOnAdminPage,
	        'offset' => 0,
	        'search_params'=> [
	        	'dateFrom' => $dateFrom,
	        	'dateTo' => $dateTo,
	        	'userType' => $userType,
	        	'agencyID' => $agencyID,
	        ],
        ];
		
		$result = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-user-activity"));        
		//var_dump(self::sendCurl($session['user_token'], $params, "/admin/get-user-activity")); die;
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/manager', 301);
        }
        
        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        
        if ($otherUserID != null) {
			return $this->render('activity-user', [
				'usersActivityInfo' => $result->usersActivityInfo,
				'count' => $result->count,
				'page' => 1,
				'limit' => $this->numberOfItemsOnAdminPage,
				'otherUserID' => $otherUserID,
				'dateFrom' => $dateFrom,
				'dateTo' => $dateTo,
				'searchUserType' => $userType
			]);
        }
        return $this->render('activity-all-user', [
        	'usersActivityInfo' => $result->usersActivityInfo,
        	'count' => $result->count,
        	'page' => 1,
        	'limit' => $this->numberOfItemsOnAdminPage,
        	'otherUserID' => $otherUserID,
        	'dateFrom' => $dateFrom,
        	'dateTo' => $dateTo,
        	'searchUserType' => $userType,
        	'agencyArray' => $agencyArray
        ]);        
    }
    
    
    public function actionGenerateActivityList()
    {
		$postArray = Yii::$app->request->post();
        if (empty($postArray) || !isset($postArray['items']) || empty($postArray['items'])) {
        	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['success' => true, 'table' => ''];
        }
           
        if (isset($postArray['otherUserID']) && $postArray['otherUserID'] == 'false') {
			$items = Yii::$app->controller->renderPartial('/layouts/parts/table-activity-all-users.php', [
				'usersActivityInfo' => $postArray['items']
			]);
        } else {
			$items = Yii::$app->controller->renderPartial('/layouts/parts/table-activity.php', [
				'usersActivityInfo' => $postArray['items']
			]);
        }
        
        $pagination = Yii::$app->controller->renderPartial('/layouts/parts/pagination.php', [
        	'letters_count' => $postArray['count'], 
        	'page' => $postArray['page'],
        	'limit' => $this->numberOfItemsOnAdminPage,
        	'page_name' => 'activity-page'
        ]);
        
        $result = [
	        'items' => $items,
	        'pagination' => $pagination
        ];
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }
    
    public function actionMyMan()
    {
        $session = Yii::$app->session;
        
        if (!isset($session['user_id']) || !isset($session['user_token'])) {
			return $this->redirect('/manager/login', 301);
        }
        
        if (!in_array($session['user_type'], [ManagerController::USER_SUPERADMIN])) {
			return $this->redirect('/manager/', 301);
		}

        $ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		if ($ajax) {
			$page = Yii::$app->request->post('page');
		}
		if (empty($page) or $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfItemsOnAdminPage;

		$statusParams = \Yii::$app->request->post('params');

		$params = [
            'limit' => $this->numberOfItemsOnAdminPage,
            'offset'=> $offset,
            'skinID' => 1,
            'userType' => self::USER_MALE,
            'userID' => $session['user_id'],
            'type' => 'from_admin',
            'notRemoveStatusParam' => false,
            'statusParams' => $statusParams,
            'visibility' => 'all'
        ];
		
		$result = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-users-for-admin"));
		
		if (gettype($result) === "string") {
			$result = json_decode($result);
		}
		
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/manager/', 301);
        }
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		
		if ($ajax) {
			$items = Yii::$app->controller->renderPartial('my-girls.php', [
				'girls' => $result->girls,
				'count' => $result->count,
				'page' => $page,
				'pageName' => 'my-man',
				'show_token_button' => false,
				'statusArray' => $result->statusArray,
				'curr_search_status' => $statusParams
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $items;
		}		
		return $this->render('my-girls', [
			'girls' => $result->girls,
			'count' => $result->count,
			'page' => $page,
			'pageName' => 'my-man',
			'show_token_button' => false,
			'statusArray' => $result->statusArray,
			'curr_search_status' => $statusParams
		]);        
    }
    
    public function actionPricelist()
    {
        $session = Yii::$app->session;
        
        if (!isset($session['user_id']) || !isset($session['user_token'])) {        	
			return $this->redirect('/manager/login', 301);
        }
        
        $post = Yii::$app->request->post();
        //var_dump($post); die;
        if (!empty($post) && !isset($post['startDate'])) {
        	$session->set('message', 'Can`t update this price');
        	return $this->redirect('/manager/pricelist', 301);
        }

		if (!empty($post) && isset($post['startDate']) && isset($post['stopDate']) && $post['startDate'] <= $post['stopDate']) {
			$session->set('message', 'Wrong date format');
			return $this->redirect('/manager/pricelist', 301);
		}
		if (!empty($post) && isset($post['startDate']) && strtotime($post['startDate'])) {
			$params = $post;			
			$result = json_decode(self::sendCurl($session['user_token'], $params,"/v1/admin/add-pricelist"));
			//var_dump(self::sendCurl($session['user_token'], $params,"/v1/admin/add-pricelist")); die;
		} else {
			$params = [
	            'skinID' => 1,
	        ];
			$result = json_decode(self::sendCurl($session['user_token'], $params,"/v1/admin/get-pricelist"));
			//var_dump(self::sendCurl($session['user_token'], $params,"/v1/admin/get-pricelist")); die;
		}

		if (isset($post['startDate']) && isset($post['stopDate']) && $post['startDate'] >= $post['stopDate']) {
			$session->set('message', 'Start date cant be greater than stop date');
		}


		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/manager', 301);
        }
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		
		return $this->render('pricelist', [
			'actionTypeList' => $result->actionTypeList,
			'pricelist' => $result->pricelist
		]);
        
    }

	public function actionAgencyPricelist()
	{
		$session = Yii::$app->session;

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$post = Yii::$app->request->post();
		if (!empty($post) && !isset($post['startDate'])) {
        	$session->set('message', 'Can`t update this price');
        	return $this->redirect('/manager/agency-pricelist', 301);
        }

		if (!empty($post) && isset($post['startDate']) && isset($post['stopDate']) && $post['startDate'] <= $post['stopDate']) {
			$session->set('message', 'Wrong date format');
			return $this->redirect('/manager/pricelist', 301);
		}

		if (!empty($post) && $session['user_type'] == self::USER_SUPERADMIN) {
			$params = $post;	
			$result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/add-agency-pricelist"));
			//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/add-agency-pricelist"));
		} else {
			$params = [
				'skinID' => 1,				
			];
			$result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/get-pricelist-agency"));
			//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-pricelist-agency")); die;
		}

		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/manager', 301);
		}
		$params = [];
		$result2 = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-agency"));
		//var_dump(self::sendCurl($session['user_token'], $params, "/admin/get-agency")); die;
		if (!self::checkCurlResponse($result2)) {
			return $this->redirect('/manager', 301);
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		if ($session['user_type'] == self::USER_ADMIN && (!isset($session['permissions']) || $session['permissions']['finance_access'] == ManagerController::DENY)) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}  
		if ($session['user_type'] == self::USER_SITEADMIN && (!isset($session['permissions']) || $session['permissions']['finance_access'] == ManagerController::DENY)) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}		
		if (in_array($session['user_type'], [self::USER_AGENCY, self::USER_ADMIN])) {
			return $this->render('agency-pricelist', [				
				'priceLetter' => $result->priceLetter,
				'priceVideoChat' => $result->priceVideoChat,
				'priceChat' => $result->priceChat,
				'priceWatchVideo' => $result->priceWatchVideo,
			]);
		}

		return $this->render('agency-pricelist', [
			'actionTypeList' => $result->actionTypeList,
			'pricelist' => $result->pricelist,
			'agencyInfoArray' => $result2->agencyArray
		]);

	}
    
    public function actionDeletePriceItem($pricelistID)
    {
        $session = Yii::$app->session;
        
		if ($session['user_type'] != self::USER_SUPERADMIN) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager', 301);
		}
        
		$params = [
            'skinID' => 1,
	        'pricelistID' => $pricelistID,
        ];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI($session['user_token'], $params,"/v1/admin/delete-pricelist-item"); 		
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/delete-pricelist-item")); die;
		return $this->redirect('/manager/pricelist', 301);
        
    }

	public function actionDeleteAgencyPriceItem($pricelistID)
	{
		$session = Yii::$app->session;

		if ($session['user_type'] != self::USER_SUPERADMIN) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager', 301);
		}

		$params = [
			'skinID' => 1,			
			'pricelistID' => $pricelistID,
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$result = $this->getResponseFromAPI($session['user_token'], $params,"/v1/admin/delete-agency-pricelist-item");
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/delete-agency-pricelist-item")); die;
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/manager', 301);
		}
		return $this->redirect('/manager/agency-pricelist', 301);

	}        
    
    public static function UserAuthorization($userInfo)
    {    	
		$session = Yii::$app->session;
		$session->set('user_id', $userInfo->userID);
		if (!empty($userInfo->userType)) {
			$session->set('user_type', $userInfo->userType);
		}
		$session->set('user_token', $userInfo->token);
		if (isset($userInfo->agency_id)) {
			$session->set('agency_id', $userInfo->agency_id);
		}
		if (isset($userInfo->userName)) {
			$session->set('userName', $userInfo->userName);
		} else if (isset($userInfo->first_name) && isset($userInfo->last_name)) {
			$session->set('userName', $userInfo->first_name . ' ' . $userInfo->last_name);
		} else{
			$session->set('userName', 'Noname');
		}
		if (isset($userInfo->avatar)) {
			$session->set('avatar', $userInfo->avatar);
		} else {
			$session->set('avatar', null);
		}
		if (isset($userInfo->permissions)) {
			$session->set('permissions', (array)$userInfo->permissions);
		} else {
			$session->set('permissions', null);
		}

		$session->setTimeout(Yii::$app->params['adminSessionTime']);

    }       
    
    public function actionAbout()
    {		     
		$params = [
			'skinID'=> 1,
			'pageName'=>'about'
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params,"/site/get-info-pages");
		if (!$result && Yii::$app->session['user_type'] != self::USER_SUPERADMIN) {
			return $this->redirect('/manager', 301);
        }
        
        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		        
        return $this->render('about', [
        	'pageContent' => (isset($result->pageContent)) ? $result->pageContent : ''
        ]);        
    }

	public function actionHistory()
	{
		$params = [
			'skinID'=> 1,
			'pageName'=>'history'
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params,"/site/get-info-pages");
		if (!$result && Yii::$app->session['user_type'] != self::USER_SUPERADMIN) {
			return $this->redirect('/manager', 301);
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('history', [
			'pageContent' => (isset($result->pageContent)) ? $result->pageContent : ''
		]);
	}

	public function actionInstructions()
	{		
		$params = [
			'skinID' => 1,
			'pageName' => 'instructions'
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params, "/site/get-info-pages");
		
		if (!$result && Yii::$app->session['user_type'] != self::USER_SUPERADMIN) {
			return $this->redirect('/manager', 301);
        }

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('instructions', [
			'pageContent' => (isset($result->pageContent)) ? $result->pageContent : ''
		]);

	}

	public function actionRules()
	{
		$params = [
			'skinID' => 1,
			'pageName' => 'rules'
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params, "/site/get-info-pages");

		if (!$result && Yii::$app->session['user_type'] != self::USER_SUPERADMIN) {
			return $this->redirect('/manager', 301);
        }

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('rules', [
			'pageContent' => (isset($result->pageContent)) ? $result->pageContent : ''
		]);

	}

	public function actionUsersRules()
	{		
		$params = [
			'skinID' => 1,
			'pageName' => 'users-rules'
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params, "/site/get-info-pages");	

		if (!$result && Yii::$app->session['user_type'] != self::USER_SUPERADMIN) {
			return $this->redirect('/manager', 301);
        }

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('users-rules', [
			'pageContent'=> (isset($result->pageContent)) ? $result->pageContent : ''
		]);

	}

	public function actionContract()
	{		
		$params = [
			'skinID' => 1,
			'pageName' => 'contract'
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params,"/site/get-info-pages");		
		if (!$result && Yii::$app->session['user_type'] != self::USER_SUPERADMIN) {
			return $this->redirect('/manager', 301);
        }

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);		
		$this->view->registerJs('window["adminAccountLibrary"]["getAlbumPhoto"]("contract");', Yii\web\View::POS_READY);

		return $this->render('contract', [
			'pageContent' => (isset($result->pageContent)) ? $result->pageContent : ''
		]);

	}

	public function actionLicenseAgreement()
	{		
		$params = [
			'skinID' => 1,
			'pageName' => 'license-agreement'
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params,"/site/get-info-pages");
		
		if (!$result && Yii::$app->session['user_type'] != self::USER_SUPERADMIN) {
			return $this->redirect('/manager', 301);
        }

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('license-agreement', [
			'pageContent' => (isset($result->pageContent)) ? $result->pageContent : ''
		]);

	}

	public function actionPrivacyPolicy()
	{
		$params = [
			'skinID' => 1,
			'pageName' => 'privacy-policy'
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params,"/site/get-info-pages");

		if (!$result && Yii::$app->session['user_type'] != self::USER_SUPERADMIN) {
			return $this->redirect('/manager', 301);
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('privacy-policy', [
			'pageContent' => (isset($result->pageContent)) ? $result->pageContent : ''
		]);

	}
    
    public function actionImageList()
    {
    	$session = Yii::$app->session;
    	$params = [	        
	        'token' => $session['user_token']
        ];

		$result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
		
		if (!$result) {
			return $this->actionLogout();
        }
        $this->layout = 'image-list';
        return $this->render('image-list');        
    }
    
    public function actionSettings()
    {
		$session = Yii::$app->session;
        
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		
		if ($session['user_type'] != self::USER_SUPERADMIN) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager',301);
		}
			
		$postArray = Yii::$app->request->post();
        $filepath = __DIR__ . '/../../../config/settings.json';
        if (!empty($postArray)) {
	        
			$json['settings'] = [
				'serverUrl' => (isset($postArray['server_url'])) ? $postArray['server_url'] : '',
				'numberOfMessagesOnPage' => (isset($postArray['message_count_on_page'])) ? $postArray['message_count_on_page'] : '',
				'numberOfMessagesOnAdminPage' => (isset($postArray['admin_message_count_on_page'])) ? $postArray['admin_message_count_on_page'] : '',
				'numberOfItemsOnAdminPage' => (isset($postArray['admin_item_count_on_page'])) ? $postArray['admin_item_count_on_page'] : '',
				'numberOfTasksOnAdminPage' => (isset($postArray['admin_tasks_count_on_page'])) ? $postArray['admin_tasks_count_on_page'] : '',
				'numberOfItemsOnSearchPage' => (isset($postArray['search_item_count_on_page'])) ? $postArray['search_item_count_on_page'] : '',
				'numberOfItemsOnFrontPage' => (isset($postArray['front_item_count_on_page'])) ? $postArray['front_item_count_on_page'] : '',
				'numberOfBlogItems' => (isset($postArray['blog_items_on_frontend'])) ? $postArray['blog_items_on_frontend'] : '',
				'numberOfGiftItemsOnFrontPage' => (isset($postArray['front_gift_item_count_on_page'])) ? $postArray['front_gift_item_count_on_page'] : '',
				'numberOfGiftItemsOnBackend' => (isset($postArray['backend_gifts_item_count_on_page'])) ? $postArray['backend_gifts_item_count_on_page'] : '',				
			];
			if (file_put_contents($filepath, json_encode($json)) !== false) {
				$session->set('message', 'All items updated');
			} else {
				$session->set('message', 'Some error occurred during settings update');
			}
		} else {
			$json = json_decode(file_get_contents($filepath), true);
		}		
			
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        return $this->render('settings', [
        	'serverUrl' => $json['settings']['serverUrl'],
        	'numberOfMessagesOnPage' => $json['settings']['numberOfMessagesOnPage'],
        	'numberOfMessagesOnAdminPage' => $json['settings']['numberOfMessagesOnAdminPage'],
        	'numberOfItemsOnAdminPage' => $json['settings']['numberOfItemsOnAdminPage'],
        	'numberOfTasksOnAdminPage' => $json['settings']['numberOfTasksOnAdminPage'],
        	'numberOfItemsOnSearchPage' => $json['settings']['numberOfItemsOnSearchPage'],
        	'numberOfItemsOnFrontPage' => $json['settings']['numberOfItemsOnFrontPage'],
        	'numberOfBlogItems' => $json['settings']['numberOfBlogItems'],
        	'numberOfGiftItemsOnFrontPage' => $json['settings']['numberOfGiftItemsOnFrontPage'],
        	'numberOfGiftItemsOnBackend' => $json['settings']['numberOfGiftItemsOnBackend'],        	
        ]);
    }
    
    public function actionGiftList()
    {
		$session = Yii::$app->session;
		$ajax = Yii::$app->request->getIsAjax();
		
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		if ($session['user_type'] == self::USER_ADMIN && (!isset($session['permissions']) || $session['permissions']['gift_access'] == ManagerController::DENY)) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}
		if ($session['user_type'] == self::USER_SITEADMIN && (!isset($session['permissions']) || $session['permissions']['gift_access'] == ManagerController::DENY)) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}		
		$offset = 0;
		$page = 1;
		$searchParams = [
			'sort' => 'asc',
			'giftsCategory' => [],
		];

		if ($ajax) {
			$page = \Yii::$app->request->post('page');
			$parameters = \Yii::$app->request->post('searchParams');
			$searchParams = [
				'sort' => isset($parameters['sort']) && $parameters['sort'] == 'desc' ? 'desc' : 'asc',
				'giftsCategory' => isset($parameters['giftsCategory']) && is_array($parameters['giftsCategory']) ? $parameters['giftsCategory'] : [],
			];
		}

		if (empty($page) || $page < 0) {
			$page = 1;
		}

		$offset = ($page - 1) * $this->numberOfGiftItemsOnBackend;
		
		$params = [
			'skinID' => 1,			
			'token' => $session['user_token'],
			'limit' => $this->numberOfGiftItemsOnBackend,
			'page' => $page-1,
			'searchParams' => $searchParams,
		];

		$result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/gifts/get-gifts"));
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/gifts/get-gifts")); die;	
		
		if(!self::checkCurlResponse($result)) {
			return $this->redirect('/manager', 301);
        }

        $giftTypes = JSON::decode($this->getCurl($session['user_token'], '/v1/gift-types'));		
        //var_dump($this->getCurl($session['user_token'], '/v1/gift-types')); die;
		if ((isset($giftTypes['status']) && 
			($giftTypes['status'] == 401 || 
			 $giftTypes['status'] == 404 || 
			 $giftTypes['status'] == 500))) {
			$session->set('message', $giftTypes['message']); 
			return $this->redirect('/', 301);
		}

		if ($ajax) {
			$giftsArray = Yii::$app->controller->renderPartial('gift-list', [
				'giftsArray' => $result->giftsArray,
				'count' => $result->giftsCount,
				'allGiftsCategories' => $giftTypes,
				'page' => $page,
				'searchParams' => $searchParams
			]);

			if ($giftsArray != '') {
				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return [
					'success' => true, 
					'gifts_html' => $giftsArray
				];
			}
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return [
				'success' => false, 
				'message' => 'Missing params'
			];
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('gift-list', [
        	'giftsArray' => $result->giftsArray,
        	'allGiftsCategories' => $giftTypes,
        	'count' => $result->giftsCount,
        	'page' => $page,
        	'searchParams' => $searchParams
        ]);
    }
    
    public function actionAddGift()
    {
		$session = Yii::$app->session;

		if ($session['user_type'] != self::USER_SUPERADMIN) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager', 301);
		}

		$params = [
			'skinID' => 1,						
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$giftTypes = JSON::decode($this->getCurl($session['user_token'], '/v1/gift-types'));		

		if ((isset($giftTypes['status']) && 
			($giftTypes['status'] == 401 || 
			 $giftTypes['status'] == 404 || 
			 $giftTypes['status'] == 500))) {
			$session->set('message', $giftTypes['message']); 
			return $this->redirect('/', 301);
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('add-gift', [
			'giftTypeArray' => $giftTypes
		]);
		
    }
    
    public function actionEditGift($giftID)
    {
		$session = Yii::$app->session;
		if ($session['user_type'] != self::USER_SUPERADMIN) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager', 301);
		}

		$params = [
			'skinID' => 1,						
			'giftsIDs' => [(int)$giftID],
		];
		
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/gifts/get-gifts");	
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/gifts/get-gifts")); die;
		$giftTypes = JSON::decode($this->getCurl($session['user_token'], '/v1/gift-types'));		

		if ((isset($giftTypes['status']) && 
			($giftTypes['status'] == 401 || 
			 $giftTypes['status'] == 404 || 
			 $giftTypes['status'] == 500))) {
			$session->set('message', $giftTypes['message']); 
			return $this->redirect('/', 301);
		}
		
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('add-gift', [
			'giftsArray' => $result->giftsArray,
			'giftTypeArray' => $giftTypes
		]);
		
    }
    
    public function actionDeleteGift($giftID)
    {
		$session = Yii::$app->session;
        if ($session['user_type'] != self::USER_SUPERADMIN) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager', 301);
        }       
		
		$params = [
            'skinID' => 1,           
	        'giftID' => $giftID,
        ];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/gift/delete-gift");
		if (!$result) {
			$session->set('message', 'Failed to delete the object for unknown reason.');
		}
		
        return $this->redirect('/manager/gift-list', 301);
		
    }
    
    public function actionMyGift()
    {
		$session = Yii::$app->session;   
		$monthFrom = Yii::$app->request->post('monthFrom');
		$yearFrom = Yii::$app->request->post('yearFrom');
		$monthTo = Yii::$app->request->post('monthTo');
		$yearTo = Yii::$app->request->post('yearTo');		
		if (!empty($monthFrom) && !empty($yearFrom) &&
			!empty($monthTo) && !empty($yearTo)) {
			$dateFrom = (is_numeric($monthFrom) && $monthFrom >= 1 && $monthFrom <= 12 && 
						 is_numeric($yearFrom) && $yearFrom >= 2015 && $yearFrom <= 2100) ? 
						 date('Y-m-d', mktime(0, 0, 0, $monthFrom, 1, $yearFrom)) : 
						 date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y') - 1));
			$dateTo = (is_numeric($monthTo) && $monthTo >= 1 && $monthTo <= 12 && 
						 is_numeric($yearTo) && $yearTo >= 2015 && $yearTo <= 2100) ? 
						 date('Y-m-d', mktime(0, 0, 0, $monthTo+1, 1, $yearTo)-24*3600) : 
						 date('Y-m-d H:i:s', time());			
		} else {
			$dateFrom = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y') - 1));
        	$dateTo   = date('Y-m-d H:i:s', time());
		}				

		$params = [
            'skinID' => 1,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo
        ];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		if ($session['user_type'] == self::USER_ADMIN && (!isset($session['permissions']) || $session['permissions']['gift_access'] == ManagerController::DENY)) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}
		if ($session['user_type'] == self::USER_SITEADMIN && (!isset($session['permissions']) || $session['permissions']['gift_access'] == ManagerController::DENY)) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}		
		$result = $this->getResponseFromAPI($session['user_token'], $params,"/v1/gifts/get-shop-items");
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/gifts/get-shop-items")); die;
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/lib/table/table.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerCssFile('/lib/table/table.css');		

        return $this->render('my-gift', [
        	'giftsArray' => $result->items,
        	'orderStatusArray' => $result->orderStatusArray,
        	'dateFrom' => $dateFrom,
        	'dateTo' => $dateTo
        ]);
    }

	public function actionViewShopItem($actionID)
	{
		$session = Yii::$app->session;

		$params = [
			'skinID' => 1,
			'actionID' => $actionID,
			'date_param'=>1,
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI($session['user_token'], $params,"/v1/gifts/get-shop-items");

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('edit-shop-item', [
			'giftsArray' => $result->items,
			'orderStatusArray' => $result->orderStatusArray,
			'discuss' => $result->discuss,
			'actionID' => $actionID,
			'type' => 'view'
		]);
	}

	public function actionEditShopItem($actionID)
	{
		$session = Yii::$app->session;

		$post = Yii::$app->request->post();
		if (!empty($post)) {
			$params = [
				'skinID' => 1,			
				'shopItemID' => (isset($post['actionID']) && is_numeric($post['actionID']) ? $post['actionID'] : null),
				'status' => isset($post['status']) && is_numeric($post['status']) ? $post['status'] : null,
				'comment' => (isset($post['comment']) && trim($post['comment']) != '') ? $post['comment'] : null,
				'discuss'      => (isset($post['discuss']) && trim($post['discuss']) != '') ? $post['discuss'] : null,
			];
			if ($logout = $this->checkIfUserNotLoggedIn()) {
				return $logout;
			}
			$result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/gift/set-shop-gift-status"));
			//var_dump(self::sendCurl($session['user_token'], $params, "/v1/gift/set-shop-gift-status")); die;
			if ($result->success == true) {
				$session->set('message', 'Item was updated');
			} else {
				$session->set('message', $result->message);
			}
			
			return $this->redirect('/manager/edit-shop-item/' . $post['actionID'], 301);
		}

		$params = [
			'skinID' => 1,						
			'actionID' => $actionID,
			'date_param'=>1,
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI($session['user_token'], $params,"/v1/gifts/get-shop-items");
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/gifts/get-shop-items")); die;
		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);		
		$this->view->registerJs('window["adminAccountLibrary"]["getAlbumPhoto"]("girls-with-gifts");', Yii\web\View::POS_READY);

		return $this->render('edit-shop-item', [
			'giftsArray' => $result->items,
			'orderStatusArray' => $result->orderStatusArray,
			'discuss' => $result->discuss,
			'actionID' => $actionID
		]);
	}

	public function actionVideoGallery($otherUserID)
	{		

		$params = [									
			'token' => Yii::$app->session['user_token']
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params, "/user/get-user-info");
		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/user/get-user-info")); die;
		if (!$result) {
			return $this->redirect('/manager', 301);
		}	

		if (!in_array($result->userData->userType, [self::USER_AGENCY, self::USER_AFFILIATE, self::USER_SUPERADMIN, self::USER_ADMIN, self::USER_SITEADMIN])) {
			Yii::$app->session->set('message', 'Error user type. Access denied.');
			return $this->actionLogout();
        }
        if ($result->userData->userType == self::USER_ADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		} 
        if ($result->userData->userType == self::USER_SITEADMIN && $result->userData->permissions->users_access == ManagerController::DENY) {
			$session->set('message', 'Access is denied.');
			return $this->redirect('/manager', 301);
		}		 
        $params = [						
			'otherUserID'=> $otherUserID			
		];
		$result = $this->getResponseFromAPI(Yii::$app->session['user_token'], $params, "/user/get-user-video");
		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/user/get-user-video")); die;
		if (!$result) {
			return $this->redirect('/manager', 301);
		}		

		$js_video = '';
		if (!empty($result->videoList)) {
			foreach ($result->videoList as $video) { 
				$js_video .= 'jwplayer("video_id_' . $video->id.'").setup({'
				   . 'file: "' . $this->serverUrl . "/" . $video->path .'",'
				   . 'image: "' .((!empty($video->medium_thumb)) ? $this->serverUrl . "/" . $video->medium_thumb : '').'",'
				.'}); ';
			}
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jwplayer/jwplayer.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJs($js_video, Yii\web\View::POS_READY);

		return $this->render('video-gallery', [
			'videoList' => $result->videoList, 
			'userInfo' => $result->userInfo, 
			'otherUserID' => $otherUserID
		]);

	}

	public function actionWaitToApprove()
	{
		$session = Yii::$app->session;

		$params = [			
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = json_decode(self::getCurl(Yii::$app->session['user_token'], "/v1/admin/wait-to-approve-items"));
		//var_dump(self::getCurl(Yii::$app->session['user_token'], "/v1/admin/wait-to-approve-items")); die;

		if (isset($result->status) && $result->status == 401) {
			$session->set('message', $result['message']); 
			return $this->redirect('/manager', 301);
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('wait-to-approve', [
			'usersList'=>$result->users,
			'videoList'=>$result->videoArray,
			'photoList'=>$result->photosArray,
			'deletedPhotoList'=>$result->deletedPhotosArray
		]);

	}

	public function actionPenalty()
	{
		$session = Yii::$app->session;
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$ajax = Yii::$app->request->getIsAjax();
        if ($ajax) {
            $postArray  = Yii::$app->request->post();
            $dateFrom = $postArray['dateFrom'];
            $dateTo   = $postArray['dateTo'];
            $agencyID = (isset($postArray['agencyID']) && $postArray['agencyID'] != '') ? $postArray['agencyID'] : null;
        } else {
            $dateFrom   = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
            $dateTo     = date('Y-m-d', time());
            $agencyID   = null;
            $agencyArray = null;
            if ($session['user_type'] == ManagerController::USER_SUPERADMIN) {
                $params = [
                    'skinID'   => 1,
                    'userType' => ManagerController::USER_AGENCY,                    
                    'type'      => 'admin',
                    'active'    => FALSE,
                ];

                $result = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-agency"));                

                if (!self::checkCurlResponse($result) || empty($result->agencyArray)) {
                    return $this->redirect('/manager', 301);
                }

                $agencyArray = $result->agencyArray;
            }
        }

        $params = [
            'dateFrom' => $dateFrom,
            'dateTo'   => $dateTo,
            'agencyID' => $agencyID,
        ];

        $result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/get-penalty"));
        //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-penalty")); die;
        if ($ajax) {
            if (!self::checkCurlResponse($result)) {
                return json_encode(['success' => FALSE, 'code' => $result->code, 'message' => $result->message]);
            }
            $html = Yii::$app->controller->renderPartial('penalty.php', [
            	'penaltyGifts' => $result->penaltyGifts, 
            	'penaltyLetters' => $result->penaltyLetters, 
            	'otherPenalty' => $result->otherPenalty
            ]);

            return json_encode(['success' => TRUE, 'code' => 1, 'html' => $html]);
        } else {
            if (!self::checkCurlResponse($result)) {
                return $this->redirect('/manager', 301);
            }

            $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

            return $this->render('penalty', [
            	'penaltyGifts' => $result->penaltyGifts, 
            	'penaltyLetters' => $result->penaltyLetters, 
            	'otherPenalty' => $result->otherPenalty, 
            	'dateFrom' => $dateFrom, 
            	'dateTo' => $dateTo, 
            	'agencyArray' => $agencyArray
            ]);
        }

	}

	public function actionAddPenalty()
	{
		$session = Yii::$app->session;

		if (!isset($session['user_token']) || $session['user_type'] != self::USER_SUPERADMIN) {
			$session->set('message', 'Error token. Please login');
			return $this->redirect('/manager', 301);
		}

		$params = [
		];

		$post = Yii::$app->request->post();

		if (!empty($post)) {
			$params['otherUserID'] = (int) $post['otherUserID'];
			$params['penaltyValue'] = (float)$post['penaltyValue'];
			$params['description'] = trim($post['description']);
			$params['penaltyID'] = (isset($post['penaltyID']) && is_numeric($post['penaltyID']) ? (int)$post['penaltyID'] : "");
			
			$result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/admin/add-penalty"));
			//var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/add-penalty")); die;

			if (!self::checkCurlResponse($result)) {
				return $this->redirect('/manager', 301);
			}

			$session->set('message', 'Penalty added');
			if ($params['penaltyID'] != '') {
				$session->set('message', 'Penalty updated');
			}
			return $this->redirect('/manager/penalty', 301);
		}

		$result = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-agency"));

		if (!self::checkCurlResponse($result) || empty($result->agencyArray)) {
			return $this->redirect('/manager', 301);
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('add-penalty', ['agencyArray'=>$result->agencyArray]);

	}

	public function actionPenaltyEdit($penaltyID)
	{

		$session = Yii::$app->session;
		if ($session['user_type'] != self::USER_SUPERADMIN) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager', 301);
		}		

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$params = [
            'penaltyID' => $penaltyID,
        ];

		$result = json_decode(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/get-penalty"));
		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/get-penalty")); die;

		if (isset($result->status) && $result->status == 401) {
			$session->set('message', $result['message']); 
			return $this->redirect('/manager', 301);
		}	
		$params = [];
		$result2 = json_decode(self::sendCurl($session['user_token'], $params,"/admin/get-agency"));

		if (!self::checkCurlResponse($result2) || empty($result2->agencyArray)) {
			return $this->redirect('/manager', 301);
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('add-penalty', [
			'penaltyArray'=>$result->otherPenalty,
			'agencyArray'=>$result2->agencyArray
		]);
	}

	public function actionPenaltyDelete($penaltyID)
	{
		$session = Yii::$app->session;

		if ($session['user_type'] != self::USER_SUPERADMIN) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager', 301);
		}

		$params = [			
			'penaltyID' => $penaltyID,
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI($session['user_token'], $params,"/v1/admin/delete-penalty");
		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/delete-penalty")); die;
		if (!$result) {
			return $this->redirect('/manager',301);
        }		

		$session->set('message', 'Penalty deleted');

		return $this->redirect('/manager/penalty', 301);
	}

	public function actionPenaltyTable()
	{
		$session = Yii::$app->session;

		if ($session['user_type'] != self::USER_SUPERADMIN) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager', 301);
		}

		$typeData = "";

		$postArray = Yii::$app->request->post();
		if (!empty($postArray) && $session['user_type'] == self::USER_SUPERADMIN) {
			$typeData = $postArray;
		}

		$params = [
			'skinID' => 1,
			'typeData' => $typeData,
		];
		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}
		$result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/pricelist-penalty");
		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/pricelist-penalty")); die;

		if (!$result) {
			return $this->redirect('/manager',301);
        }

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('penalty-table', [
			'penaltyPricelist' => $result->penaltyPricelist
		]);

	}

    public function actionBalance()
    {
        $session = Yii::$app->session;

        if (!isset($session['user_token'])) {
            $session->set('message', 'missing params');

            return $this->redirect('/manager', 301);
        }
        if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

        $ajax = Yii::$app->request->getIsAjax();
        if ($ajax) {
            $postArray         = Yii::$app->request->post();
            $dateFrom = date('Y-m-d H:i:s', mktime(0, 0, 0, (int)$postArray['month'], 1, $postArray['year']));
            $dateTo   = date('Y-m-d H:i:s', mktime(0, 0, 0, (int)$postArray['month'] + 1, 1, $postArray['year']));
        } else {
            $dateFrom = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
            $dateTo   = date('Y-m-d', time());
        }

        $params = [
            'findUserID' => (isset($session['agency_id']) && is_numeric($session['agency_id']) ? $session['agency_id'] : $session['user_id']),
            'dateFrom'    => $dateFrom,
            'dateTo'      => $dateTo,
        ];

        $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/agency-balance");
        //var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/agency-balance")); die;
		if (!$result) {
			return $this->redirect('/manager', 301);
		}

		unset($params['findUserID']);
		$params['agencyID'] = (isset($session['agency_id']) && is_numeric($session['agency_id']) ? $session['agency_id'] : $session['user_id']);

        $result2 = json_decode(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/get-penalty"));
        //var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/admin/get-penalty")); die;
        
		if (!$result2) {
			return $this->redirect('/manager', 301);
		}

        if ($ajax) {            
            $html = Yii::$app->controller->renderPartial('balance.php', [
                'balance'        => $result->balance,
                'penaltyGifts'   => $result2->penaltyGifts,
                'penaltyLetters' => $result2->penaltyLetters->lettersArray,
                'otherPenalty'   => $result2->otherPenalty,
                'month'           => $postArray['month'],
                'year'            => $postArray['year'],
            ]);

            return json_encode(['success' => TRUE, 'code' => 1, 'html' => $html]);
        }

        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('balance', [
            'balance'         => $result->balance,
            'penaltyGifts'   => $result2->penaltyGifts,
            'penaltyLetters' => $result2->penaltyLetters->lettersArray,
            'otherPenalty'   => $result2->otherPenalty,
            'month'           => date('m'),
            'year'            => date('Y'),
        ]);
    }

	public function actionLog()
	{
		$session = Yii::$app->session;

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$offset = 0;
		$page = 1;
		$searchParams = [
			'dateFrom' => date('Y-m-d', time()),
			'dateTo'   => date('Y-m-d', time()) . ' 23:59:59',
			'userType' => self::USER_AGENCY
		];


		$ajax = Yii::$app->request->getIsAjax();
		$postArray = Yii::$app->request->post();

		$params = [	
			'token'   => $session['user_token']
		];		

		$result2 = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-user-types"));
		
		if (!self::checkCurlResponse($result2)) {
			return $this->redirect('/manager', 301);
		}

		if ($ajax) {
			$page = \Yii::$app->request->post('page');
			$parameters = \Yii::$app->request->post('searchParams');
			$searchParams = [
				'dateFrom' => isset($parameters['dateFrom']) && strtotime($parameters['dateFrom']) ? $parameters['dateFrom']  : date('Y-m-d', time()),
				'dateTo' => isset($parameters['dateTo']) && strtotime($parameters['dateTo']) ? $parameters['dateTo'] . ' 23:59:59' : date('Y-m-d', time()) . ' 23:59:59',
				'userType' => isset($parameters['userType']) && is_numeric($parameters['userType']) ? (int)$parameters['userType'] : self::USER_AGENCY
			];
		}

		if (empty($page) || $page < 0) {
			$page = 1;
		}
		
		$params = [
			'skinID' => 1,			
			'token' => $session['user_token'],
			'limit' => $this->numberOfLogItemsOnBackend,
			'page' => $page-1,
			'searchParams' => $searchParams,
		];		

		$result = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-log"));
		//var_dump(self::sendCurl($session['user_token'], $params, "/admin/get-log")); die;
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/manager', 301);
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		return $this->render('log', [
			'logArray' => $result->logArray,
			'userTypes' => $result2->userTypes,
			'count' => $result->count,
			'numberOfLogItems' => $this->numberOfLogItemsOnBackend
		]);
	}

	public function actionAffiliates()
	{
		$session = Yii::$app->session;

		if ($session['user_type'] != self::USER_SUPERADMIN) {
			$session->set('message', 'Access denied');
			return $this->redirect('/manager', 301);
		}

		$params = [
			'skinID' => 1,
			'userType' => self::USER_AFFILIATE,
			'userID' => $session['user_id'],
			'token' => $session['user_token'],
			'type' => 'admin',
			'status' => false
		];

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$result = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-affiliates"));

		if (!$result) {
			return $this->redirect('/manager',301);
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('affiliates', ['affiliates_mass' => $result->affiliatesArray, 'count' => $result->count, 'page' => 1, 'pageName' => 'affiliates', 'limit' => 30]);

	}

	public function actionReferralStats()
	{
		$session = Yii::$app->session;

		if (!isset($session['user_token']) || !in_array($session['user_type'], [self::USER_SUPERADMIN,self::USER_AFFILIATE])) {
			$session->set('message', 'Error token. Please login');
			return $this->redirect('/manager', 301);
		}

		$date_from = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
		$date_to   = date('Y-m-d', time());
		$other_user_id = null;

		$post = Yii::$app->request->post();
		if($post){
			$other_user_id = isset($post['other_user_id']) ? (int) $post['other_user_id'] : null;
			$date_from = isset($post['date-from']) && strtotime($post['date-from']) ? $post['date-from'] : date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
			$date_to   = isset($post['date-to']) && strtotime($post['date-to']) ? $post['date-to'] : date('Y-m-d', time());
			//var_dump($date_to);die;
		}

		if($session['user_type'] == self::USER_AFFILIATE){
			$other_user_id = $session['user_id'];
		}

		if ($logout = $this->checkIfUserNotLoggedIn()) {
			return $logout;
		}

		$params = [
			'user_id'     => $session['user_id'],
			'limit'              => null,
			'offset'             => 0,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'other_user_id' => $other_user_id,
		];

		$result = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-affiliates-stats"));
		//var_dump($result);die;

		if (!$result) {
			return $this->redirect('/manager',301);
		}

		$this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

		return $this->render('referral-stats', [
			'stats' => $result->stats,
			'limit' => null,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'user_type' => $session['user_type'],
			'other_user_id' => $other_user_id,
		]);

	}

	function validateDate($date, $format = 'Y-m-d')
	{
		$d = \DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}

	public function actionSubscribes()
	{
		$session = Yii::$app->session;
		$post = Yii::$app->request->post();

		if (!isset($session['user_token']) || !in_array($session['user_type'], [self::USER_SUPERADMIN])) {
			$session->set('message', 'Error token. Please login');
			return $this->redirect('/manager', 301);
		}

		$params = [
			'user_id'     => $session['user_id'],
			'limit'              => null,
			'offset'             => 0,
		];

		$result = json_decode(self::sendCurl($session['user_token'], $params, "/site/get-subscribe"));

		return $this->render('subscribes', [
			'subscribes' => $result
		]);
	}

	private function getAdminPermissions($perm, $userType)
	{
		$currentPermissions = [];
		if ($userType == self::USER_SITEADMIN) {
			$permissions = Yii::$app->params['siteadmin_permissions'];
		} else {
			$permissions = Yii::$app->params['admin_permissions'];
		}    			
    	$newPermissions = (array)$perm;        	
    	foreach ($permissions as $access => $permission) {
    		foreach ($newPermissions as $newAccess => $newPermission) {        			
    			if ($access == $newAccess) {
    				$currentPermissions[$newAccess]['description'] = $permission['description'];
    				$currentPermissions[$newAccess]['value'] = $newPermissions[$newAccess];       
    				unset($permissions[$newAccess]);
    			}    			
    		}    		
    	}    	
    	$currentPermissions = array_merge($currentPermissions, $permissions);
    	return $currentPermissions;
	}
}
