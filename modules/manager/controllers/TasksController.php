<?php

namespace app\modules\manager\controllers;

use Yii;
use app\modules\manager\controllers\ManagerController;

class TasksController extends ManagerController
{
    
    public $numberOfItemsOnAdminPage;
    public $numberOfTasksOnAdminPage;

    public function init()
    {
        $this->serverUrl = Yii::$app->params['serverUrl'];
        $this->numberOfItemsOnAdminPage = Yii::$app->params['numberOfItemsOnAdminPage'];
        $this->numberOfTasksOnAdminPage = Yii::$app->params['numberOfTasksOnAdminPage'];
        $this->view->registerJsFile('/js/admin/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    }

    public function beforeAction($action)
    {
        $session = Yii::$app->session;
        if ($session['user_type'] == self::USER_ADMIN && (!isset($session['permissions']) || $session['permissions']['tasks_access'] == ManagerController::DENY)) {
            $session->set('message', 'Access is denied.');
            return $this->redirect('/manager', 301);
        } 
        if ($session['user_type'] == self::USER_SITEADMIN && (!isset($session['permissions']) || $session['permissions']['tasks_access'] == ManagerController::DENY)) {
            $session->set('message', 'Access is denied.');
            return $this->redirect('/manager', 301);
        }        
        $arrayOfActionsWithDisabledCSRFValidation = ['add-task', 'index'];
        if (in_array($action->id, $arrayOfActionsWithDisabledCSRFValidation)) {
            $this->enableCsrfValidation = FALSE;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $session = Yii::$app->session;

        if ($session['user_type'] != ManagerController::USER_SUPERADMIN) {
            return $this->redirect('/manager/agency-tasks', 301);
        }

        $page         = 1;        
        $searchParams = null;

        $ajax = Yii::$app->request->getIsAjax();

        if ($ajax) {
            $page         = Yii::$app->request->post('page');
            $searchParams = Yii::$app->request->post('searchParams');
        }

        if (empty($page) || $page < 0) {
            $page = 1;
        }
        $offset = ($page - 1) * $this->numberOfTasksOnAdminPage;

        $params = [
            'limit'         => $this->numberOfTasksOnAdminPage,
            'offset'        => $offset,
            'searchParams' => $searchParams,
        ];

        $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/get-tasks");
        //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-tasks")); die;
        if (!$result) {
            return $this->redirect('/manager', 301);
        }

        if ($ajax) {            
            $items = Yii::$app->controller->renderPartial('tasks.php', [
                'tasksList' => $result->tasksList,
                'count' => $result->count,
                'page' => $page,
                'limit' => $this->numberOfTasksOnAdminPage
            ]);
            return json_encode(['success' => true, 'tasksList' => $items]);
        }

        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('tasks', [
            'tasksList' => $result->tasksList,
            'count' => $result->count,
            'page' => $page, 
            'limit' => $this->numberOfTasksOnAdminPage
        ]);
    }

    public function actionAddTask()
    {
        $session = Yii::$app->session;

        $params = [];

        $postArray = Yii::$app->request->post();

        if (!empty($postArray)) {
            $params['agencyIDs']  = $postArray['agencyID'];
            $params['dateTo']     = (isset($postArray['dateTo']) && strtotime($postArray['dateTo'])) ? $postArray['dateTo'] : '';
            $params['description'] = (isset($postArray['description']) && trim($postArray['description']) != '') ? $postArray['description'] : '';
            $params['comment']     = (isset($postArray['comment']) && trim($postArray['comment']) != '') ? $postArray['comment'] : '';
            $params['taskID']     = isset($postArray['taskID']) ? (int)$postArray['taskID'] : null;
            $params['status']      = isset($postArray['status']) ? (int)$postArray['status'] : null;

            $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/add-task");   
            //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/add-task")); die;         
            if (!$result) {
                return $this->redirect('/manager', 301);
            }
            if(in_array($session['user_type'], [self::USER_AGENCY, self::USER_ADMIN])){
                $session->set('message', 'task updated');
            } else {
                $session->set('message', $result->message);
            } 

            return $this->redirect('/manager/tasks', 301);
        }

        $result = json_decode(self::sendCurl($session['user_token'], $params, "/admin/get-agency"));
        //var_dump(self::sendCurl($session['user_token'], $params, "/admin/get-agency")); die;
        if (!self::checkCurlResponse($result)) {
            return $this->redirect('/manager', 301);
        }

        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

        return $this->render('add-task', [
            'agencyArray' => $result->agencyArray
        ]);

    }

    public function actionAgencyTasks()
    {        
        $session = Yii::$app->session;
        $params = [

        ];

        $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/get-tasks");
        
        if (!$result) {
            return $this->redirect('/manager', 301);
        }
        return $this->render('agency-tasks', [
            'tasksList' => $result->tasksList,
            'count' => $result->count
        ]);

    }

    public function actionEditTask($taskID)
    {        
        $session = Yii::$app->session;
        $params = [            
            'taskID' => $taskID,
        ];

        $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/get-tasks");
        //var_dump(self::sendCurl($session['user_token'], $params, "/v1/admin/get-tasks")); die;
        if (!$result) {
            return $this->redirect('/manager', 301);
        }
        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        
        return $this->render('edit-task', [
            'task' => $result->task,
            'comments' =>$result->comments,
        ]);

    }

    public function actionDeleteTask($taskID)
    {
        $session = Yii::$app->session;
        if ($session['user_type'] != ManagerController::USER_SUPERADMIN) {
            $session->set('message', 'Access denied');
            return $this->redirect('/manager', 301);
        }

        $params = [
            'taskID' => $taskID,
        ];

        $result = $this->getResponseFromAPI($session['user_token'], $params, "/v1/admin/delete-task");
        if (!$result) {
            return $this->redirect('/manager', 301);
        }
        return $this->redirect('/manager/tasks', 301);

    }

}