<?php

namespace app\modules\manager\controllers;

use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;


class RuleViolationController extends ManagerController
{

    public function init()
    {
		parent::init();
        $this->view->registerJsFile('/js/admin/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/rule-violation.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    }

    public function beforeAction($action)
    {
        //отключение проверки для определенных екшенов
        if (in_array($action->id, ['create'])) {
            $this->enableCsrfValidation = FALSE;
        }

        return parent::beforeAction($action);
    }

    public function actionView($ruleViolationID)
    {
        $session = Yii::$app->session;

        if (!isset($session['user_token'])) {
            $session->set('message', 'Error token. Please login');

            return $this->redirect('/manager', 301);
        }

        if ($session['user_type'] != ManagerController::USER_SUPERADMIN) {
            return $this->redirect('/manager/', 301);
        }

        $result = json_decode(self::getCurl($session['user_token'], "/v1/rule-violation/" . $ruleViolationID));
        //var_dump(self::getCurl($session['user_token'], "/v1/rule-violation/" . $ruleViolationID)); die;

        if (isset($result->status) && $result->status == 401) {
            $session->set('message', $result->message); 
            return $this->redirect('/', 301);
        }
        if (isset($result->agency_id)) {
            $params = [
                'otherUserID' => $result->agency_id,
            ];

            $result2 = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
            //var_dump($result2->userData->agency_info->name);die;
            if (!$result2) {
                return $this->redirect('/manager', 301);
            }
            if (empty($result2->userData->agency_info->name)) {
                $session->set('message', 'Error user id');
                return $this->redirect('/manager', 301);
            }
        } else {
            $session->set('message', 'Error user id');
            return $this->redirect('/manager', 301);
        }
        
        return $this->render('view', [
            'ruleViolation' => $result,
            'agencyID' => $result->agency_id,
            'agencyName'    => $result2->userData->agency_info->name,
        ]);

    }

    public function actionCreate($agencyID)
    {
        $session = Yii::$app->session;
        if (!isset($session['user_token'])) {
            $session->set('message', 'Error token. Please login');

            return $this->redirect('/manager', 301);
        }

        if ($session['user_type'] != ManagerController::USER_SUPERADMIN) {
            return $this->redirect('/manager/', 301);
        }
        if (!empty(Yii::$app->request->post())) {           
            $params = [
                'agency_id' => $agencyID,
                'description' => Yii::$app->request->post('rule-violation'),
                'created_at' => date('Y-m-d H:i:s')
            ];
            $result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/rule-violation"));
            if (isset($result->status) && $result->status == 401) {
                $session->set('message', $result->message); 
                return $this->redirect('/', 301);
            }
            return $this->redirect('/manager/edit-user/' . $agencyID, 301);
        } else {
            $params = [
                'otherUserID' => $agencyID,
            ];

            $result = $this->getResponseFromAPI($session['user_token'], $params, "/user/get-user-info");
            //var_dump($result->userData->agency_info->name);die;
            if (!$result) {
                return $this->redirect('/manager', 301);
            }
            if (empty($result->userData->agency_info->name)) {
                $session->set('message', 'Error user id');
                return $this->redirect('/manager', 301);
            }
        }        

        return $this->render('create', [
            'agencyName'    => $result->userData->agency_info->name,
        ]);
    }
  
}
