<?php

namespace app\modules\manager\controllers;

use Yii;
use app\modules\manager\controllers\ManagerController;
use yii\web\BadRequestHttpException;
use yii\helpers\HtmlPurifier;

class MessageMailingTemplateController extends ManagerController
{
	public $numberOfTemplatesOnAdminPage;
	
    public function init()
    {
        $this->serverUrl = Yii::$app->params['serverUrl'];
        $this->numberOfTemplatesOnAdminPage = 10;
        $this->view->registerJsFile('/js/admin/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/admin/message.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    }
    
    public function beforeAction($action)
	{            	    
		$arrayOfActionsWithDisabledCSRFValidation = ['generate-list', 'create'];
	    if (in_array($action->id, $arrayOfActionsWithDisabledCSRFValidation)) {
	        $this->enableCsrfValidation = false;
	    }

	    return parent::beforeAction($action);
	}
    
    public function actionIndex($page = 1)
    {        
        $session = Yii::$app->session;                   
        if(!isset($session['user_id']) || !isset($session['user_token'])) {
            return $this->redirect('/manager/login', 301);
        }

        $page = (int) $page;
        if ($page < 0) {
            $page = 1;
        }

        $result = json_decode(self::getCurl($session['user_token'], "/v1/mailing-template/message-templates?page=$page&per_page={$this->numberOfTemplatesOnAdminPage}"));
        //var_dump(self::getCurl($session['user_token'], "/v1/mailing-template/message-templates?page=$page&per_page={$this->numberOfTemplatesOnAdminPage}")); die;       
        if ((isset($result->success) && $result->success == false) || 
            (isset($result->status) && ($result->status == 401 || $result->status == 404 || $result->status == 500))) {            
            $session->set('message', $result->message);
            return $this->redirect('/manager', 301);
        }
        return $this->render('index', [
            'templates' => $result->templates,
            'count' => (isset($result->count) && $result->count > $this->numberOfTemplatesOnAdminPage) ? $result->count : null,
            'page' => $page,
            'limit' => $this->numberOfTemplatesOnAdminPage
        ]);
    }
        	
	public function actionCreate()
    {		
        $session = Yii::$app->session;                   
        if(!isset($session['user_id']) || !isset($session['user_token'])) {
            return $this->redirect('/manager/login', 301);
        }
		return $this->render('create');
    }

    public function actionEdit($templateID)
    {   
        $session = Yii::$app->session;                   
        if(!isset($session['user_id']) || !isset($session['user_token'])) {
            return $this->redirect('/manager/login', 301);
        } 

        $result = json_decode(self::getCurl($session['user_token'], "/v1/mailing-template/message-template/$templateID"));
        //var_dump(self::getCurl($session['user_token'], "/v1/mailing-template/message-template/$templateID")); die;
        if ((isset($result->success) && $result->success == false) || 
            (isset($result->status) && ($result->status == 401 || $result->status == 404 || $result->status == 500))) {            
            $session->set('message', $result->message);
            return $this->redirect('/manager', 301);
        }

        return $this->render('edit', [
            'template' => $result->template 
        ]);
    }    
              
       
}
