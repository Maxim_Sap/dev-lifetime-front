<?php
use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;
//var_dump($task);die;
?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<?php if($session['user_type'] == ManagerController::USER_SUPERADMIN || ($session['user_type'] == ManagerController::USER_AGENCY && in_array($task->status_id, [1,2]))){?>
				<h2 class="content-title">Редактирование задачи</h2>
			<?php }else{?>
				<h2 class="content-title">Просмотр задачи</h2>
			<?php }?>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<form action="/manager/add-task" method="post" id="" class="admin-custom-border margin-top">
					<table cellspacing="3" cellpadding="0" border="0" align="center">
						<tbody>
						<?php if($session['user_type'] == ManagerController::USER_SUPERADMIN){?>
							<tr>
								<td align="left" style="width: 200px;">Агенство:</td>
								<td align="left"><?=$task->name?>
									<input type="hidden" name="agencyID[]" value="<?=$task->task_performer;?>" />
									<input type="hidden" name="taskID" value="<?=$task->id;?>" />
								</td>
							</tr>
							<tr>
								<td align="left">Статус:</td>
								<td>
									<select name="status" style="width:200px" required>
										<option value="1" <?php if($task->status_id == 1){echo 'selected';}?>>new</option>
										<option value="2" <?php if($task->status_id == 2){echo 'selected';}?>>waiting for approve</option>
										<option value="3" <?php if($task->status_id == 3){echo 'selected';}?>>expired</option>
										<option value="4" <?php if($task->status_id == 4){echo 'selected';}?>>done</option>
									</select>
								</td>
							</tr>
							<tr>
								<td align="left">Срок выполнения до</td>
								<td><input type="date" name="dateTo"  class="input-data" style="width: 200px" value="<?=($task->approved_at != null) ? date("Y-m-d",strtotime($task->approved_at)) : "";?>" required></td>
							</tr>
							<tr>
								<td align="left">Текст задания</td>
								<td>
									<textarea name="description" style="width: 100%; height: 115px;" required><?=$task->description;?></textarea>
								</td>
							</tr>
							<?php if(!empty($comments)){?>
								<tr>
									<td align="left">Комментарии</td>
									<td>
										<?php foreach($comments as $one){?>
											<p>
												<span style="color: #4549de;font-weight: bold;"><?=(trim($one->name) == 'Users without agency' ? "Admin" : trim($one->name))?></span>
												[<?=$one->date_create?>]:
												<span><?=$one->comments?></span>
											</p>
										<?php }?>
									</td>
								</tr>
							<?php }?>
							<tr>
								<td align="left">Добавить комментарий</td>
								<td>
									<textarea name="comment" style="width: 100%; height: 115px;"></textarea>
								</td>
							</tr>
						<?php } elseif(in_array($session['user_type'],[ManagerController::USER_AGENCY, ManagerController::USER_ADMIN])){ ?>
							<tr>
								<td align="left">Статус:</td>
								<td>
									<?php if(in_array($task->status_id, [1,2])){?>
										<select name="status" required>
											<option value="1" <?php if($task->status_id == 1){echo 'selected';}?>>new</option>
											<option value="2" <?php if($task->status_id == 2){echo 'selected';}?>>send to approve</option>
										</select>
									<?php }else{ echo $task->status_name;}?>
								</td>
							</tr>
							<tr>
								<td align="left">Срок выполнения до</td>
								<td><?=($task->approved_at != null) ? date("Y-m-d",strtotime($task->approved_at)) : "";?></td>
							</tr>
							<tr>
								<td align="left">Текст задания</td>
								<td><?=$task->description;?></td>
							</tr>
							<?php if(!empty($comments)){?>
								<tr>
									<td align="left">Комментарии</td>
									<td>
										<?php foreach($comments as $one){?>
											<p>
												<span style="color: #4549de;font-weight: bold;"><?=(trim($one->name) == 'Users without agency' ? "Admin" : trim($one->name))?></span>
												[<?=$one->date_create?>]:
												<span><?=$one->comments?></span>
											</p>
										<?php }?>
									</td>
								</tr>
							<?php }?>
							<?php if(in_array($task->status_id, [1,2])){?>
								<tr>
									<td align="left">Добавить комментарий</td>
									<td>
										<textarea name="comment" style="width: 100%; height: 115px;"></textarea>
										<input type="hidden" name="taskID" value="<?=$task->id;?>" />
										<input type="hidden" name="agencyID[]" value="<?=$task->task_performer;?>" />
										<input type="hidden" name="dateTo" value="<?=($task->approved_at != null) ? date("Y-m-d",strtotime($task->approved_at)) : "";?>">
									</td>
								</tr>
							<?php }?>
						<?php }?>
						<?php if($session['user_type'] == ManagerController::USER_SUPERADMIN || (in_array($session['user_type'],[ManagerController::USER_AGENCY,ManagerController::USER_ADMIN]) && in_array($task->status_id, [1,2]))){?>
							<tr>
								<td align="center" colspan="4">
									<input type="submit" class="btn admin-custom-btn" value="изменить">
								</td>
							</tr>
						<?php }?>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
