<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Задачи:</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<div id="tasks-list">
					<table class="data-table agency-task-table">
						<thead>
						<tr>
							<th style="text-align:center;width: 100px;">ID</th>
							<th style="text-align:center;">Текст</th>
							<th style="text-align:center;width: 170px;">дата создания</th>
							<th style="text-align:center;width: 110px;">срок до</th>
							<th style="text-align:center;width: 120px;">статус</th>
							<th style="text-align:center;width: 80px;">действие</th>
						</tr>
						</thead>
						<tbody>
						<?php if(!empty($tasksList)) {
							foreach($tasksList as $task){
								$class = 'new';
								if((time() > strtotime($task->approved_at)) && !in_array($task->status_id,[3,4])){
								$class = 'expired';
								}elseif ($task->status_id == 4){
								$class = 'approved';
								}elseif ($task->status_id == 3){
								$class = 'cenceled';
								} ?>
								<tr class="<?=$class;?>">
									<td class="task-id"><?=$task->id?></td>
									<td class="description"><?=mb_substr($task->description,0,100,'UTF-8')?></td>
									<td class="data-create"><?=$task->created_at?></td>
									<td class="data-approve"><?=date('Y-m-d',strtotime($task->approved_at));?></td>
									<td class=""><?=$task->status_name?></td>
									<td class="">
										<?php if(in_array($task->status_id,[1,2])){?>
											<a href="/manager/edit-task/<?=$task->id?>" title="edit">
												<i class="fa fa-pencil" aria-hidden="true"></i>
											</a>
										<?php }else{?>
											<a href="/manager/edit-task/<?=$task->id?>" title="view">
												<i class="fa fa-eye" aria-hidden="true"></i>
											</a>
										<?php }?>
										<?php if($task->comments_id != null){?>
											<span class="new-comments" title="new comments"></span>
										<?php }?>
									</td>
								</tr>
							<?php }
						}else{ ?>
							<tr><td colspan="7">No tasks</td></tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	.new-comments {
		background-color: #00ff11;
		width: 15px;
		height: 15px;
		position: absolute;
		border-radius: 25px;
		right: 12px;
	}
</style>