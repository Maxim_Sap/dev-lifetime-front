<?php
use app\modules\manager\controllers\ManagerController;

$ajax = Yii::$app->request->getIsAjax();
if(!$ajax){?>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-body">
			<div class="content-container">
				<div class="content-area-inner-header">
					<a href="/manager/add-task" class="btn">Добавить новую задачу</a>
					<h2 class="content-title">Current tasks:</h2>
				</div>

				<div class="content-area-inner-body">
					<div class="content-container">
						<form action="#" method="post" id="" class="">
							<table align="center" cellspacing="3" cellpadding="0" border="0">
								<tbody>
									<tr>
									<td align="right">Status:</td>
										<td>
											<select name="status">
												<option value="">All</option>
												<option value="1">New</option>
												<option value="2">In progress</option>
												<option value="3">Expired</option>
												<option value="4">Done</option>
											</select>
										</td>
										<td align="right">Date from:</td>
										<td><input name="dateFrom" type="date" class="input-data"></td>
										<td align="right">Date to:</td>
										<td><input name="dateTo" type="date" class="input-data"></td>
										<td align="center">
											<input name="box" value="" type="hidden">
											<input class="btn admin-custom-btn search_tasks_btn" value="Search" name="submit" type="submit">
										</td>
									</tr>
								</tbody>
							</table>
						</form>
						<div id="tasks-list">
						<?php } ?>
							<table class="data-table task-table">
								<thead>
								<tr>
									<th style="text-align:center;">ID</th>
									<th style="text-align:center;">кому</th>
									<th style="text-align:center;">Текст</th>
									<th style="text-align:center;">дата создания</th>
									<th style="text-align:center;">срок до</th>
									<th style="text-align:center;">статус</th>
									<th style="text-align:center;">действия</th>
								</tr>
								</thead>
								<tbody>
								<?php if(isset($tasksList) && !empty($tasksList)){
									foreach($tasksList as $task){
										$class = 'new';
										if((time() > strtotime($task->approved_at)) && !in_array($task->status_id, [ManagerController::TASK_CANCELED, ManagerController::TASK_APPROVED])){
											$class = 'expired';
										}elseif ($task->status_id == ManagerController::TASK_APPROVED){
											$class = 'approved';
										}elseif ($task->status_id == ManagerController::TASK_CANCELED){
											$class = 'canceled';
										}
										?>
										<tr class="<?=$class;?>">
											<td class="task-id"><?=$task->id?></td>
											<td>
												<span class="message-from">
													<a href="/manager/edit-user/<?=$task->task_performer?>" class="table_link"><?=$task->name;?></a>
														[ID: <?=$task->task_performer;?>]
												</span>
											</td>
											<td class="description"><?=mb_substr($task->description,0,100,'UTF-8')?></td>
											<td class="data-create"><?=$task->created_at?></td>
											<td class="data-approve"><?=date('Y-m-d',strtotime($task->approved_at));?></td>
											<td class=""><?=$task->status_name?></td>
											<td align="center">
												<a href="/manager/edit-task/<?=$task->id?>" title="edit">
													<i class="fa fa-pencil" aria-hidden="true"></i>
												</a>
												<a href="/manager/delete-task/<?=$task->id?>" title="delete task" onclick="if(!confirm('Are you sure do you wont delete task?')){return false;}">
													<i class="fa fa-trash" aria-hidden="true"></i>
												</a>
												<?php if($task->comments_id != null){?>
													<span class="new-comments" title="new comments"></span>
												<?php }?>
											</td>
										</tr>
									<?php }
								}else{ ?>
									<tr><td colspan="7">No result</td></tr>
								<?php } ?>
								</tbody>
							</table>
							<div class="admin-tasks-pagination pagination">
								<?php if(isset($tasksList) && !empty($tasksList)){
									echo $this->render('/layouts/parts/pagination.php',['letters_count'=>$count,'page'=>$page,'page_name'=>'tasks','limit'=>$limit]);
								} ?>
							</div>
						</div>
						<?php if(!$ajax){?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<style>
	.new-comments {
		background-color: #00ff11;
		width: 15px;
		height: 15px;
		position: absolute;
		border-radius: 25px;
		left: 10px;
	}
</style>
<br> <br> <br> <br> <br> <br>
