<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Новая задача</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<form action="/manager/add-task" method="post" id="" class="admin-custom-border margin-top">
				<table cellspacing="3" cellpadding="0" border="0" align="center">
					<tbody>
					<tr>
						<td align="left" style="width: 200px;">Агенства:</td>
						<td align="left">
							<?php if(!empty($agencyArray)){?>
								<div id="agency-box" style="height: 400px;overflow: auto;">
									<input id="all_agency" type="checkbox" />
									<label for="all_agency">все</label><br/>
									<?php foreach ($agencyArray as $agency){?>
										<input name="agencyID[]" id="agency_id_<?=$agency->user_id?>" type="checkbox" value="<?=$agency->user_id?>" />
										<label for="agency_id_<?=$agency->user_id?>"><?=$agency->name?></label><br/>
									<?php } ?>
								</div>
							<?php } ?>
							
						</td>
					</tr>
					<tr>
						<td align="left">Срок выполнения до</td>
						<td><input type="date" name="dateTo" class="input-data" style="width: 200px" required></td>
					</tr>
					<tr>
						<td align="left">Текст задания</td>
						<td>
							<textarea name="description" style="width: 100%; height: 115px;" required></textarea>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="4">
							<input type="submit" class="btn admin-custom-btn" value="добавить">
						</td>
					</tr>
					</tbody>
				</table>
				</form>
			</div>
		</div>
	</div>
</div>			


<div class="admin-content inbox_container">
	<h2></h2>

</div>