<?php 
use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;

?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">						
			<h2 class="content-title">Письма: Раcсылка</h2>
			<a href="/manager/message/" class="btn active admin-tab">Раcсылка</a>
			<a href="/manager/message/inbox" class="btn admin-tab">Входящие</a>
			<a href="/manager/message/outbox" class="btn admin-tab">Исходящие</a>  <br>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<form method="post" action="/manager/message/mailing" class="admin-message admin-letters-search margin-top" style="width: auto">
					<table cellspacing="3" cellpadding="0" border="0" align="center" style="width: 600px; margin-left: 0;">
						<tbody>
							<tr>
								<td style="width: 195px;    vertical-align: top;">
									<select name="userTypeID" id="userTypeID">
										<option value="3">агенства</option>
										<option value="2">девочки</option>
										<option value="1">мальчики</option>
										<option value="4">переводчики</option>
										<option value="7">админы агенств</option>
										<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN ||
												  $session['user_type'] == ManagerController::USER_AGENCY) { ?>
										<option value="8">админы сайта</option>
										<?php } ?>
									</select>
								</td>
								<td>
									<div class="result" style="height: 400px;overflow: auto;">
										<?php if(!empty($agencyArray)){?>
											<input id="all_users_check" type="checkbox" />
											<label for="all_users_check">все</label><br/>
											<?php foreach ($agencyArray as $agency){?>
												<input name="userIDs[]" id="user_id_<?=$agency->user_id?>" type="checkbox" value="<?=$agency->user_id?>" />
												<label for="user_id_<?=$agency->user_id?>"><?=$agency->name?> [ID: <?=$agency->user_id?>]</label><br/>
											<?php } ?>
										<?php } ?>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="line">
						<label class="margin-top">Caption:</label>
						<input type="text" name="caption" value="" class="margin-top" required>
					</div>
					<div class="line">
						<textarea name="description"></textarea>
					</div>
					<div class="line">
					<br>
						<button class="btn margin-top pull-right">Send</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
