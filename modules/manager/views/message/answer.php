<?php //var_dump($message_mass);
if(empty($messageArray)){
	echo '<p>No message</p>';
}else{
$messageArray = $messageArray[0];
$answer_status = ($messageArray->reply_status == 1) ? 'yes' : 'no';
$read = ($messageArray->readed_at !== null) ? $messageArray->readed_at : 'no';
$label = (isset($_GET['type']) && $_GET['type'] == 'outbox') ? false : 'Message from: ';

$avatar = (!empty($messageArray->another_user_avatar)) ? $this->context->serverUrl."/".$messageArray->another_user_avatar : '/img/no_avatar_normal.jpg';

?>
<style>
	.product-description {
		border: 1px solid #ccc;
		padding: 10px;
	}

</style>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Reply to message</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<div class="card">
					<div class="container-fliud">
						<div class="wrapper row">
							<div class="preview col-md-4">								
								<div class="preview-pic tab-content">
								  <img src="<?=$avatar?>" />
								</div>								
							</div>
							<div class="details col-md-8">
								<h3 class="product-title">Caption: <b class="caption"><?=$messageArray->title?></b></h3>	
								<h6 class="product-title">Message ID: <b><?=$messageArray->id?></h6>								
								<h6 class="product-title"><?=($label) ? $label : 'Message to: ';?><b><?=trim($messageArray->first_name." ".$messageArray->last_name)?></h6>								
								<h6 class="product-title">Message read at: <b class="data-read grey_text"><?=$read?></h6>						
								<h6 class="product-title">Created At: <b class="data-create grey_text"><?=$messageArray->created_at?></h6>	
								<h6 class="product-title">Answered: <b class="answered grey_text"><?=$answer_status?></b></h6>			
								<h4>Содержание:</h4>	
								<div class="product-description"><?=$messageArray->description?></div>											
							</div>
						</div>
					</div>
				</div>
				<?php if($label){?>
					<br>
					<h3>Your answer:</h3>
					<div style="min-height: 300px;">
						<form method="post" action="" class="admin-message">
							<?php if(empty($messageArray->message_creator)){?>
								<label>Email:</label>
								<input type="email" name="email" required>
							<?php } ?>
							<label>Caption:</label>
							<input type="text" name="caption" value="" required>
							<textarea name="description"></textarea>
							<input type="hidden" name="messageID" value="<?=$messageArray->id?>">
							<input type="hidden" name="fromUserID" value="<?=$messageArray->message_creator?>">
							<br>
							<button class="pull-right btn margin-top">Send</button>
						</form>
						<div class="clear"></div>
					</div>
				<?php } 
				}
				?>			
			</div>
		</div>
	</div>
</div>
