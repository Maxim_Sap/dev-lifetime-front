<?php 
	use app\modules\manager\controllers\ManagerController;
	$session = Yii::$app->session;
?>
<div id="content-area">
	<div class="content-area-inner">		

		<div class="content-area-inner-header">
			<h2 class="content-title">Письма: Входящие</h2>
			<a href="/manager/message/" class="btn admin-tab">Раcсылка</a>
			<a href="/manager/message/inbox" class="btn active admin-tab">Входящие</a>
			<a href="/manager/message/outbox" class="btn admin-tab">Исходящие</a>  <br>
			<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) { ?>
			<a href="/manager/message/new" class="btn">Написать новое письмо</a>
			<?php } ?>
		</div>

		<?=$this->render('letters-inner.php',['messageArray'=>$messageArray,'page_name'=>'inbox','countMessage'=>$countMessage,'page'=>$page,'limit'=>$numberOfMessagesOnAdminPage])?>
		<input type="hidden" id="numberOfMessagesOnAdminPage" value="<?=$numberOfMessagesOnAdminPage;?>">

	</div>
</div>