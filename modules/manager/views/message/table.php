<?php if (isset($message_mass) && !empty($message_mass)){
	foreach($message_mass as $message){
		$read_status = !empty($message['readed_at']) ? 'yes' : 'no';
		$answer_status = ($message['reply_status'] == 1) ? 'yes' : 'no';?>
		<tr>
			<td class="message-id"><?=$message['id']?></td>
			<td>
				<span class="message-from">
					<?php 
					if($page_name == 'outbox'){
						if(!empty($message['message_receiver'])){?>
							<a href="/manager/user/<?=$message['message_receiver']?>" class="table_link"><?php 
							if (!empty($message['first_name'])) {
								echo trim($message['first_name']." ".$message['last_name']);
							} elseif (!empty($message['name'])) {
								echo trim($message['name']);
							}										
							?></a>
							[ID: <?=$message['message_receiver'];?>]
						<?php }else{
							echo 'not register';
						} 
					}else{
						if(!empty($message['message_creator'])){?>
							<a href="/manager/user/<?=$message['message_creator']?>" class="table_link"><?=trim($message['first_name']." ".$message['last_name']);?></a>
							[ID: <?=$message['message_creator'];?>]
						<?php }else{
							echo 'not register';
						} 
					}
					?>
				</span>
			</td>
			<td class="data-create"><?=$message['created_at']?></td>
			<td class="data-read" data-read="<?=$message['readed_at'];?>"><?=$read_status?></td>
			<td class="answered"><?=$answer_status?></td>
			<td align="center">
				<?php if($page_name == 'outbox'){?>
					<a class="admin-table-btn btn" href="/manager/message/answer/<?=$message['id']?>?type=outbox">посмотреть</a>
				<?php }else{ ?>
					<a class="admin-table-btn btn" href="/manager/message/answer/<?=$message['id']?>?type=inbox">ответить</a>
				<?php } ?>
			</td>
		</tr>
	<?php }
}?>