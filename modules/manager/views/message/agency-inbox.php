<?php //var_dump($messageArray); die; ?>
<div id="content-area">
    <div class="content-area-inner">
        
        <div class="content-area-inner-header">
            <h2 class="content-title">Входящие сообщения:</h2>            
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <div id="tasks-list">
                    <table class="data-table agency-message-table">
                        <thead>
                        <tr>
                            <th style="text-align:center;width: 100px;">ID</th>
                            <th style="text-align:center;">Тема</th>
                            <th style="text-align:center;">Текст</th>
                            <th style="text-align:center;width: 170px;">дата создания</th>
                            <th style="text-align:center;width: 110px;">статус</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($messageArray)) {
                            $read_message_script = '';
                            foreach ($messageArray as $message) { ?>
                                <tr class="<?=($message->readed_at == null) ? 'new' : 'read';?>">
                                    <td class="task-id"><?=$message->id?></td>
                                    <td class="description"><?=$message->title?></td>
                                    <td class="description"><?=$message->description?></td>
                                    <td class="data-create"><?=$message->created_at?></td>
                                    <td class=""><?=($message->readed_at == null) ? 'новое' : 'прочитано';?></td>
                                </tr>
                            <?php
                                if($message->readed_at == null){
                                    $read_message_script .= "window['messageAdminLibrary']['readSystemMessage']($message->id); ";
                                }
                            }
                            $this->registerJs($read_message_script, yii\web\View::POS_READY);
                        } else { ?>
                            <tr>
                                <td colspan="5">No tasks</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
