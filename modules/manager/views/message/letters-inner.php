<div class="content-area-inner-body">
	<div class="content-container">
	<form action="#" method="post" id="admin-message-search" class="admin-letters-search admin-custom-border margin-top">
		<table cellspacing="3" cellpadding="0" border="0" align="center">
			<tbody><tr>
					<td align="right">Письма</td>
					<td>
						<select name="answered">
							<option value="">все</option>
							<option value="0">неотвеченные</option>
							<option value="1">отвеченные</option>
						</select>
					</td>
					<td></td>
					<td>
						<select name="read">
							<option value="">все</option>
							<option value="1">прочитанные</option>
							<option value="0">не прочитанные</option>
						</select>
					</td>
				</tr>
				<tr>
					<td align="right">ID отправителя</td>
					<td>
						<input type="text" name="user_id_from" <?php if($page_name == 'outbox'){echo 'disabled';}?>>
					</td>
					<td align="right">Дата от</td>
					<td><input type="date" name="date_from"></td>
				</tr>
				<tr>
					<td align="right">ID получателя</td>
					<td>
						<input type="text" name="user_id_to" <?php if($page_name == 'inbox'){echo 'disabled';}?>>
					</td>
					<td align="right">Дата до</td>
					<td><input type="date" name="date_to"></td>
				</tr>
				<tr>
					<td align="right" colspan="4">
						<input type="hidden" name="box" value="<?=$page_name?>">
						<input type="submit" class="btn admin-custom-btn search_inbox_btn" value="Поиск" name="submit">
					</td>
				</tr>
			</tbody></table>
	</form>
	<br>
		<table class="data-table">
			<thead>
				<tr>
					<th style="text-align:center;">ID письма</th>
					<th style="text-align:center;"><?php if($page_name == 'outbox'){echo 'кому';}else{echo 'от кого';}?></th>
					<th style="text-align:center;">дата</th>
					<th style="text-align:center;">прочитано</th>
					<th style="text-align:center;">отвечено</th>
					<th style="text-align:center;">действия</th>
				</tr>
			</thead>
			<tbody id="message-list">
				<?php if(isset($messageArray) && !empty($messageArray)){
					foreach($messageArray as $message){ //var_dump($one);die;
						$read_status = !empty($message->readed_at) ? 'yes' : 'no';
						$answer_status = ($message->reply_status == 1) ? 'yes' : 'no';
					?>
					<tr>
						<td class="message-id"><?=$message->id?></td>
						<td>
							<span class="message-from">
								<?php if($page_name == 'outbox'){
									 if(!empty($message->message_receiver)){?>
										<a href="/manager/user/<?=$message->message_receiver?>" class="table_link"><?php 
										if (!empty($message->first_name)) {
											echo trim($message->first_name." ".$message->last_name);
										} elseif (!empty($message->name)) {
											echo trim($message->name);
										}										
										?></a>
										[ID: <?=$message->message_receiver;?>]
									<?php }else{
										echo 'not register';
									}
								}else{
									 if(!empty($message->message_creator)){?>
										<a href="/manager/user/<?=$message->message_creator?>" class="table_link"><?=trim($message->first_name." ".$message->last_name);?></a>
										[ID: <?=$message->message_creator;?>]
									<?php }else{
										echo 'not register';
									}
								} ?>
							</span>
						</td>
						<td class="data-create"><?=$message->created_at?></td>
						<td class="data-read" data-read="<?=$message->readed_at;?>"><?=$read_status?></td>
						<td class="answered"><?=$answer_status?></td>
						<td align="center">
							<?php if($page_name == 'outbox'){?>
								<a class="btn" href="/manager/message/answer/<?=$message->id?>?type=outbox">посмотреть</a>
							<?php }else{ ?>
								<a class="btn" href="/manager/message/answer/<?=$message->id?>?type=inbox">ответить</a>
							<?php } ?>
						</td>
					</tr>
					<?php }
				} ?>
			</tbody>
		</table>
		<div class="admin-message-pagination">
			<?php if(isset($messageArray) && !empty($messageArray)){				
				echo $this->render('/layouts/parts/pagination.php',['letters_count'=>$countMessage,'page'=>$page,'page_name'=>$page_name,'limit'=>$limit]);
				} ?>
		</div>

	</div>
</div>
	
