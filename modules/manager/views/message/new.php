<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Новое административное письмо</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<form method="post" action="" class="admin-message">
					<div class="line">
						<label class="margin-top">User ID:</label>
						<input type="text" name="toUserID" class="margin-top" value="<?=$otherUserID;?>" required>
					</div>
					<div class="line">
						<label class="margin-top">Caption:</label>
						<input type="text" name="caption" value="" class="margin-top" required>
					</div>
					<div class="line">
						<label>Текст:</label>
						<textarea name="description"></textarea>
					</div>
					<div class="line">
						<br>
						<button class="admin-custom-btn margin-top btn pull-right">Send</button>
					</div>
				</form>		
			</div>
		</div>
	</div>
</div>
			
