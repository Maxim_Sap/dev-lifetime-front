<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>" >
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="<?=Yii::$app->charset?>">
    <?=Html::csrfMetaTags()?>
    <title><?=Html::encode($this->title)?></title>
    <?php $this->head() ?>
</head>
<body class="admin_body">
<?php $this->beginBody(); ?>                    
<div class="admin-panel">
    <?php 
    echo $this->render('//layouts/parts/icon-sprites.php');
    echo $content;
    ?>
</div>
<!-- Modal MESSAGE-->
<div id="layout"></div>
<div id="modalMessage">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <span></span>
            </div>
        </div>
    </div>
</div>
<?php
$session = Yii::$app->session;
if(isset($session['message'])){
    echo "<script type='text/javascript'>
        $('#modalMessage,#layout').show();
        $('#modalMessage .modal-body span').text('".$session['message']."');
    </script>";
    unset($session['message']);
}?>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage() ?>
