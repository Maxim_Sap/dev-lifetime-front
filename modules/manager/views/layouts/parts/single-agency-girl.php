<?php 
use app\modules\manager\controllers\ManagerController;
$serverUrl = $this->context->serverUrl;
$avatar = !empty($girl->medium_thumb) ? $serverUrl.'/'.$girl->medium_thumb : '/img/no_avatar_normal.jpg';
if (!empty($girl->birthday)) {
    $birthday = new \DateTime($girl->birthday);
    $tz  = new \DateTimeZone('Europe/Brussels');
    $age = \DateTime::createFromFormat('Y-m-d', $girl->birthday, $tz)
         ->diff(new \DateTime('now', $tz))
         ->y;	
} else {
	$age = "Not defined";
}
$onlineStatus = ($girl->status == ManagerController::STATUS_ACTIVE && $girl->last_activity >= time() - 30) ? "online" : "offline";
//var_dump($girl);die;
?>

<figure class="card-box girl-tile" data-user-id="<?=$girl->id;?>">
	<strong><?=$girl->first_name;?> <?=$girl->last_name;?> <span class="status <?= $onlineStatus ?>"></span></strong> 
	<a href="/manager/user/<?=$girl->id?>" title="просмотр">
		<div class="card-box-image cover" style="background-image: url('<?=$avatar;?>')"></div>
	</a>
	<figcaption class="card-box-description">		
		<span class="card-box-id">Profile: <strong><?=$girl->id;?></strong> <a href="/manager/edit-user/<?=$girl->id?>">Edit</a></span>
		<span class="card-box-id"><?php 
			if ($girl->status == ManagerController::STATUS_ACTIVE) {
				echo "Status: <strong>Active</strong>"; 
			} elseif ($girl->status == ManagerController::STATUS_NO_ACTIVE) {
				echo "Status: <strong>Not Active</strong>";
			}			
		?> <a href="/manager/edit-user/<?=$girl->id?>">Edit</a></span>
		<span class="card-box-id"><?php if (!empty($girl->city_name) && !empty($girl->country_name)) {
			echo "City: <strong>" . $girl->city_name . ", " . $girl->country_name ."</strong>";
			} elseif (!empty($girl->city_name)) {
				echo "City: <strong>" . $girl->city_name . "</strong>";
			}
		?></span>		
		<span class="card-box-id">Age: <strong><?=$age;?></strong></span>
		<span class="card-box-id">Video: <a href="/manager/video-gallery/<?=$girl->id?>">Upload</a></span>
	</figcaption>
</figure>