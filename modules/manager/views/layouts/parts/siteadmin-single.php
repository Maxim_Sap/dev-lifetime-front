<?php 
	use app\modules\manager\controllers\ManagerController;
	$serverUrl = $this->context->serverUrl;	
	
	//если пользоватлеь добавлен мение месяца назад, то ему добавляется значек NEW
	$avatar = !empty($admin->medium_thumb) ? $serverUrl.'/'.$admin->medium_thumb : '/img/no_avatar_normal.jpg';
?>


<figure class="card-box">
	<?php if($admin->approve_status_id == 4){?>
		<div class="card-box-status yes"><span>approved</span></div>
	<?php }elseif($admin->approve_status_id == 1 || $admin->approve_status_id == 2){?>
		<div class="card-box-status no"><span>not approved</span></div>
	<?php }elseif($admin->approve_status_id == 3){?>
		<div class="card-box-status decline"><span>decline</span></div>
	<?php } ?>
	<a href="/manager/user/<?=$admin->id;?>" title="просмотреть">
		<div class="card-box-image cover" style="background-image: url('<?=$avatar;?>')"></div>
	</a>
	<figcaption class="card-box-description">
		<span class="card-box-name">Name: <?=$admin->first_name;?></span>
		<span class="card-box-id">ID: <?=$admin->id;?></span>
		<ul class="card-box-edit list-inline controls">				
				<li><a href="/manager/edit-user/<?=$admin->id;?>" title="edit"><i class="fa fa-pencil" aria-hidden="true"></i>
								</a></li>
				<li><a href="#" data-user-id="<?=$admin->id;?>" class="user-activate" title="<?=($admin->status == ManagerController::STATUS_ACTIVE) ? 'deactivate' : 'activate'?>">
									<i class="fa <?=($admin->status == ManagerController::STATUS_ACTIVE) ? 'fa-toggle-on' : 'fa-toggle-off'?>" aria-hidden="true"></i>
								</a></li>
				<li><a href="/manager/delete-user/<?=$admin->id;?>" title="delete user" onclick="if(!confirm('Are you sure that you want delete user?')){return false;}"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
		</ul>
	</figcaption>
</figure>