<?php 
use app\modules\manager\controllers\ManagerController;
$serverUrl = $this->context->serverUrl;
$avatar = !empty($agency->medium_thumb) ? $serverUrl.'/'.$agency->medium_thumb : '/img/no_avatar_normal.jpg';

?>

<figure class="card-box girl-tile" data-user-id="<?=$agency->user_id;?>">
	<a href="/manager/user/<?=$agency->user_id?>" title="просмотр">
		<div class="card-box-image cover" style="background-image: url('<?=$avatar;?>')"></div>
	</a>
	<figcaption class="card-box-description">
		<span class="card-box-name">Name: <?=$agency->name;?></span>
		<span class="card-box-id">ID: <?=$agency->user_id;?></span>
		<ul class="card-box-edit list-inline controls">
			<li><a href="/manager/edit-user/<?=$agency->user_id?>" title="редактировать"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
			<?php if($agency->status == ManagerController::STATUS_ACTIVE){?>
			<li><a data-user-id="<?=$agency->user_id?>" class="user-activate" href="javascript:void(0);" title="отключить"><i class="fa fa-toggle-on" aria-hidden="true"></i></a></li>
			<?php }else{?>
			<li><a data-user-id="<?=$agency->user_id?>" class="user-activate" href="javascript:void(0);" title="включить"><i class="fa fa-toggle-off" aria-hidden="true"></i></a></li>
			<?php }?>
			<li>
				<a href="/manager/delete-user/<?=$agency->user_id?>" onclick="if(!confirm('Are you sure do you wont delete user?')){return false;}" title="удалить">
					<i class="fa fa-trash" aria-hidden="true"></i>
				</a>
			</li>
		</ul>
	</figcaption>
</figure>