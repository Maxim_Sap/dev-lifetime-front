<?php 
	use app\modules\manager\controllers\ManagerController;
	$serverUrl = $this->context->serverUrl;	
	
	$labelNew = ($girl->new) ? '<li class="is-new"><span>NEW</span></li>': "";
	$avatar = !empty($girl->medium_thumb) ? $serverUrl.'/'.$girl->medium_thumb : '/img/no_avatar_normal.jpg';
?>

<figure class="card-box">
	<?php if($girl->approve_status_id == 4){?>
		<div class="card-box-status yes"><span>approved</span></div>
	<?php }elseif($girl->approve_status_id == 1 || $girl->approve_status_id == 2){?>
		<div class="card-box-status no"><span>not approved</span></div>
	<?php }elseif($girl->approve_status_id == 3){?>
		<div class="card-box-status decline"><span>decline</span></div>
	<?php } ?>
	<a href="/manager/user/<?=$girl->id?>" title="просмотреть">
		<div class="card-box-image cover" style="background-image: url('<?=$avatar;?>')"></div>
	</a>
	<figcaption class="card-box-description">
		<span class="card-box-name">Name: <?=$girl->first_name;?></span>
		<span class="card-box-id">ID: <?=$girl->id;?></span>
		<button class="btn btn-large start-video" data-id="<?=$girl->id?>">Check camera</button>
		<button class="btn btn-large stop-video" data-id="<?=$girl->id?>">Stop video</button>
	</figcaption>
</figure>
