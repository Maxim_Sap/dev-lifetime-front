<div class="sort_by sort">
	<span>Sort by price: </span>
	<ul class="sort-buttons-list list-inline">
		<li><a href="javascript:void(0);" <?=isset($sort) && $sort == 'asc' ? 'class="active"':'';?> data-value="asc" title="Asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a></li>
		<li><a href="javascript:void(0);" <?=isset($sort) && $sort == 'desc' ? 'class="active"':'';?> data-value="desc" title="Desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
	</ul>
</div>