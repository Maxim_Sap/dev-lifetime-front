<?php 

use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;
$serverUrl = $this->context->serverUrl;

if (!empty($session['avatar']->small)) {
	$avatar = $serverUrl.'/'.$session['avatar']->small;
} elseif (!empty($session['avatar'])) {
	$avatar = $serverUrl.'/'.$session['avatar'];
} else {
	$avatar = '/img/no_avatar_small.jpg';
}



?>
<header class="admin-header">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="logo-container">
					<a href="javascript:void(0);" class="logo-icon">
						<img src="/img/logo-admin.png" alt="logo">
					</a>
				</div>
				<div class="content-container clearfix">

					<button class="bar-toggle">
						<span></span>
						<span></span>
						<span></span>
					</button>
					<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) {?>
						<a href="/manager/settings" class="settings">
					<?php } else { ?>
						<a href="/manager/edit-user/<?=$session['user_id']?>" class="settings">
					<?php } ?>
						<img src="/img/settings.svg" alt="settings">
					</a>

					<div class="login-block">
						<div class="thumbnail" style="background-image: url('<?= $avatar ?>')"></div>
						<button class="login-name"><?= $session['userName'] ?></button>
						<ul class="login-options">
						<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) {?>
							<li><a href="/manager/settings">Site Settings</a></li>
						<?php } else {?>
							<li><a href="/manager/edit-user/<?=$session['user_id']?>">Account Settings</a></li>
						<?php } ?>
							<li><a href="/manager/logout">Log Out</a></li>
						</ul>
					</div>
					<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) { ?>
					<ul class="notification-list list-inline">
						<li><a href="/manager/need-to-approve/" class="warnings"><span class="counter orange">0</span><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></a></li>
						<li><a href="javascript:void(0);" class="alerts"><span class="counter green">0</span><i class="fa fa-bell" aria-hidden="true"></i></a></li>
					</ul>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</header>

<button class="scroll-top">
	<i class="fa fa-angle-up" aria-hidden="true"></i>
</button>