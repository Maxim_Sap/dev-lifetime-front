<?php



use app\modules\manager\controllers\ManagerController;

$session = Yii::$app->session;
$serverUrl = $this->context->serverUrl;


//var_dump($session['avatar']); die;

$avatar = (!empty($session['avatar'])) ? $serverUrl.'/'.$session['avatar'] : '/img/no_avatar_normal.jpg';

$pageName = '';

if (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'index') {

    $pageName = 'homePage';

} elseif (Yii::$app->controller->id == 'finance') {

    $pageName = 'finance';

} elseif (Yii::$app->controller->id == 'default' &&

    (Yii::$app->controller->action->id == 'about' || Yii::$app->controller->action->id == 'history' ||

        Yii::$app->controller->action->id == 'instructions' || Yii::$app->controller->action->id == 'users-rules' ||

        Yii::$app->controller->action->id == 'contract' || Yii::$app->controller->action->id == 'license-agreement')) {

    $pageName = 'static';

}





?>



<div class="tool-bar-content">

    <div class="tool-bar-header">

        <div class="greeting-block">

            <div class="greeting-avatar" style="background-image: url('<?= $avatar ?>')"></div>

            <span class="greeting-text">Hello, <span class="admin-name"><?= $session['userName'] ?></span></span>

            <span class="current-date"><span class="status"></span><span class="month"><?= date('M') ?></span> <span class="day"><?= date('d') ?></span>, <span class="year"><?= date('Y') ?></span> <span class="time"><?= date('h:i:s A') ?></span></span>

        </div>

    </div>



    <div class="tool-bar-body">

        <input type="hidden" id="agency_id" value="<?=isset($session['agency_id']) ? $session['agency_id'] : $session['user_id'] ;?>">

        <ul class="tools-nav">

            <li <?php if ($pageName == 'homePage') {echo 'class="active"';} ?>>

                <a href="/manager"><span class="icon-wrapper"><i class="fa fa-desktop" aria-hidden="true"></i></span>Рабочий стол</a>

            </li>

            <li <?php if (Yii::$app->controller->action->id == 'about') {echo 'class="active"';} ?>>

                <a href="/manager/about"><span class="icon-wrapper"><i class="fa fa-file-text" aria-hidden="true"></i></span>О нас</a>

            </li>

            <li <?php if (Yii::$app->controller->action->id == 'instructions') {echo 'class="active"';} ?>>

                <a href="/manager/instructions"><span class="icon-wrapper"><i class="fa fa-file-text" aria-hidden="true"></i></span>Инструкции</a>

            </li>

            <li <?php if (Yii::$app->controller->action->id == 'users-rules') {echo 'class="active"';} ?>>

                <a href="/manager/users-rules"><span class="icon-wrapper"><i class="fa fa-file-text" aria-hidden="true"></i></span>Правила для пользователей</a>

            </li>

            <li <?php if (Yii::$app->controller->action->id == 'referral-stats') {echo 'class="active"';} ?>>

                <a href="/manager/referral-stats"><span class="icon-wrapper"><i class="fa fa-gears" aria-hidden="true"></i></span>Статистика привлечения</a>

            </li>

            <li <?php if (Yii::$app->controller->action->id == 'affiliates') {echo 'class="active"';} ?>>

                <a href="/manager/finance/affiliates"><span class="icon-wrapper"><i class="fa fa-gears" aria-hidden="true"></i></span>Мои финансы</a>

            </li>

            <li <?php if ($_SERVER['REQUEST_URI'] == '/manager/edit-user/'.$session['agency_id']) {echo 'class="active"';} ?>>

                <a href="/manager/edit-user/<?=$session['user_id']?>"><span class="icon-wrapper"><i class="fa fa-gears" aria-hidden="true"></i></span>Профиль аффилейта</a>

            </li>

            <li>

                <a href="/manager/logout"><span class="icon-wrapper"><i class="fa fa-sign-out" aria-hidden="true"></i></span>Выход</a>

            </li>

        </ul>

    </div>



    <div class="tool-bar-footer">

        <button class="collapse-bar"><i class="fa fa-angle-double-left" aria-hidden="true"></i>Collapse Menu</button>

    </div>

</div>

<style>

    #content-area .content-area-inner {

        min-height: 1300px;

    }

</style>

