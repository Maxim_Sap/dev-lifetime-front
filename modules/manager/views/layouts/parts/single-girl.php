<?php 
	use app\modules\manager\controllers\ManagerController;
	$serverUrl = $this->context->serverUrl;	
	
	$labelNew = ($girl->new) ? '<li class="is-new"><span>NEW</span></li>': "";
	$avatar = !empty($girl->medium_thumb) ? $serverUrl.'/'.$girl->medium_thumb : '/img/no_avatar_normal.jpg';
?>

<figure class="card-box">
	<?php if($girl->approve_status_id == 4){?>
		<div class="card-box-status yes"><span>approved</span></div>
	<?php }elseif($girl->approve_status_id == 1 || $girl->approve_status_id == 2){?>
		<div class="card-box-status no"><span>not approved</span></div>
	<?php }elseif($girl->approve_status_id == 3){?>
		<div class="card-box-status decline"><span>decline</span></div>
	<?php } ?>
	<a href="/manager/user/<?=$girl->id?>" title="просмотреть">
		<div class="card-box-image cover" style="background-image: url('<?=$avatar;?>')"></div>
	</a>
	<figcaption class="card-box-description">
		<span class="card-box-name">Name: <?=$girl->first_name;?></span>
		<span class="card-box-id">ID: <?=$girl->id;?></span>
		<ul class="card-box-edit list-inline controls">
			<?php if(isset($curr_user_type) && $curr_user_type == ManagerController::USER_INTERPRETER){?>
				<li><a href="/manager/letters/inbox/<?=$girl->id;?>" title="letters">
					<i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
				<li><a class="get_userToken_to_login" data-other-user-id="<?=$girl->id;?>" href="javascript:void(0);" title="login with use user data"><i class="fa fa-sign-in" aria-hidden="true"></i></a></li>
			<?php }else{?>
				<li><a href="/manager/finance/user-info/<?=$girl->id;?>" title="finance"><i class="fa fa-usd" aria-hidden="true"></i></a></li>
				<li><a href="/manager/activity/<?=$girl->id;?>" title="activity"><i class="fa fa-clock-o" aria-hidden="true"></i></a></li>
				<li><a href="/manager/letters/inbox/<?=$girl->id;?>" title="letters"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
				<li><a href="/manager/gallery/<?=$girl->id;?>" title="manager photo"><i class="fa fa-file-image-o" aria-hidden="true"></i></a></li>
				<li><a href="/manager/video-gallery/<?=$girl->id;?>" title="manager video"><i class="fa fa fa-video-camera" aria-hidden="true"></i></a></li>
				<li><a href="/manager/edit-user/<?=$girl->id;?>" title="edit"><i class="fa fa-pencil" aria-hidden="true"></i>
								</a></li>
				<?php if ($show_token_button) { ?>
					<li><a class="get_userToken_to_login" data-other-user-id="<?=$girl->id;?>" href="javascript:void(0);" title="login with use user data"><i class="fa fa-sign-in" aria-hidden="true"></i></a></li>
				<?php } ?>
				<li><a href="#" data-user-id="<?=$girl->id;?>" class="user-invisible" title="<?=($girl->visible == ManagerController::STATUS_INVISIBLE) ? 'invisible' : 'visible'?>">
									<i class="fa <?=($girl->visible != ManagerController::STATUS_INVISIBLE) ? 'fa-eye' : 'fa-eye-slash'?>" aria-hidden="true"></i>
								</a></li>
				<li><a href="#" data-user-id="<?=$girl->id;?>" class="user-approve-status" title="<?=($girl->approve_status_id == 2) ? 'withdraw' : 'send to approve'?>">
									<i class="fa <?=($girl->approve_status_id != 2) ? 'fa-hand-o-right' : 'fa-hand-o-left'?>" aria-hidden="true"></i>
								</a></li>
				<li><a href="#" data-user-id="<?=$girl->id;?>" class="user-activate" title="<?=($girl->status == ManagerController::STATUS_ACTIVE) ? 'deactivate' : 'activate'?>">
									<i class="fa <?=($girl->status == ManagerController::STATUS_ACTIVE) ? 'fa-toggle-on' : 'fa-toggle-off'?>" aria-hidden="true"></i>
								</a></li>
				<li><a href="/manager/delete-user/<?=$girl->id;?>" title="delete user" onclick="if(!confirm('Are you sure that you want delete user?')){return false;}"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
			<?php }?>
		</ul>
	</figcaption>
</figure>