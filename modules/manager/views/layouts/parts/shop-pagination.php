<?php
if (Yii::$app->params['numberOfGiftItemsOnBackend'] < $count) {
    $giftsOnPage = (Yii::$app->params['numberOfGiftItemsOnBackend'] > $count) ? $count : Yii::$app->params['numberOfGiftItemsOnBackend'];
    $total       = (($count - 1) / $giftsOnPage) + 1;
    $total       = intval($total);
    $pagePrev    = $page - 1;
    $pageNext    = $page + 1;
    $currentPage = '<li class="active"><a href="#" class="active">' . $page . '</a></li>';
    if ($page > 2) {
        $prevPage = "<li class=\"prev\"><a href='javascript:void(0);' class='custom-border' onclick=\"window['adminAccountLibrary']['giftPagination'](" . ($page - 1) . ");return false;\"><i class='fa fa-angle-left' aria-hidden='true'></i></a></li>";
    } else {
        $prevPage = '';
    }
    if ($page <= $total - 2) {
        $nextPage = "<li class=\"next\"><a href='javascript:void(0);' class='custom-border' onclick=\"window['adminAccountLibrary']['giftPagination'](" . ($page + 1) . ");return false;\"><i class='fa fa-angle-right' aria-hidden='true'></i></a></li>";
    } else {
        $nextPage = '';
    }
    if ($page < 4 && $page - 3 > 0 || ($page == $total && $page - 2 > 0)) {
        $page2left = "<li><a href='#' class='custom-border' onclick=\"window['adminAccountLibrary']['giftPagination'](" . ($page - 2) . ");return false;\">" . ($page - 2) . '</a></li>';
    } else {
        $page2left = "";
    }
    if ($page - 2 >= 0) {
        $page1left = "<li><a href='#' class='custom-border' onclick=\"window['adminAccountLibrary']['giftPagination'](" . ($page - 1) . ");return false;\">" . ($page - 1) . '</a></li>';
    } else {
        $page1left = "";
    }
    if (($page < 2 && $page + 2 <= $total) || $page > 3 && $page + 2 <= $total) {
        $page2right = "<li><a class='custom-border' href='#' onclick=\"window['adminAccountLibrary']['giftPagination'](" . ($page + 2) . ");return false;\">" . ($page + 2) . '</a></li>';
    } else {
        $page2right = "";
    }
    if ($page + 1 <= $total) {
        $page1right = "<li><a class='custom-border' href='#' onclick=\"window['adminAccountLibrary']['giftPagination'](" . ($page + 1) . ");return false;\">" . ($page + 1) . '</a></li>';
    } else {
        $page1right = "";
    }
    $navigationLine = $prevPage . $page2left . $page1left . $currentPage . $page1right . $page2right . $nextPage;
    ?>
    <div class="page-nav-wrapper">
        <span>Show: </span>
        <ul class="page-navigation list-inline">
            <?=$navigationLine?>
        </ul>
    </div>
<?php } ?>