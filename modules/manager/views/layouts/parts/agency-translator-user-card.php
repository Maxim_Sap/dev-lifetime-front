<?php 
use app\modules\manager\controllers\ManagerController;

$session = Yii::$app->session;
$girlsIDs = isset($userData->girlsIDs) && is_array($userData->girlsIDs) ? $userData->girlsIDs : [];
?>
<style>
    input.checkbox{
        height: 12px!important;
        margin-left: 0px !important;
        display: inline;
        width: auto !important;
    }
    .girls-list .item label{
        margin-left: 8px;
    }
    .girls-list .item{
        display: inline-block;
        padding-right: 30px;
        margin-bottom: 10px;
    }
</style>
<table class="margin-top margin-bottom">
    <tr>
        <td>User ID:</td>
        <td><span><?=$userData->personalInfo->id;?></span></td>
        <td>Your email:</td>
        <td class="edit"><input type="email" name="email" class="custom-border"
                                value="<?=$userData->email;?>"></td>
    </tr>
    <tr>
        <td>First Name:</td>
        <td class="edit"><input type="text" name="first_name" class="custom-border"
                               value="<?=$userData->personalInfo->first_name;?>" required></td>
        <td>Last Name:</td>
        <td class="edit"><input type="text" name="last_name" class="custom-border"
                                value="<?=(!empty($userData->personalInfo->last_name)) ? $userData->personalInfo->last_name : "";?>"
                                placeholder="---"></td>
    </tr>
    <?php if (in_array($session['user_type'], [ManagerController::USER_AGENCY, ManagerController::USER_SUPERADMIN, ManagerController::USER_ADMIN])) { ?>
        <tr>
            <td>Phone:</td>
            <td class="edit">
                <input type="text" name="phone" class="custom-border"
                       value="<?=(!empty($userData->personalInfo->phone)) ? $userData->personalInfo->phone : "";?>"
                       placeholder="---">
            </td>
            <td>Mob phone:</td>
            <td>
                <input type="text" name="m_phone" class="custom-border"
                       value="<?=(!empty($userData->personalInfo->m_phone)) ? $userData->personalInfo->m_phone : "";?>"
                       placeholder="---">
            </td>
        </tr>
        <tr>
            <td>Assigned Girls:</td>
            <td colspan="3" class="girls-list">
                <?php if(isset($girlsArray) && !empty($girlsArray)) {
                    foreach ($girlsArray as $girl) {?>
                    <div class="item">

                        <input id="girls_id_<?=$girl->id?>" class="checkbox" type="checkbox" value="<?=$girl->id?>" <?php if($girlsIDs && in_array($girl->id, $girlsIDs)) {echo "checked";}?>>
                        <label for="girls_id_<?=$girl->id?>"><?=trim($girl->first_name." ".$girl->last_name)?></label>
                    </div>
                <?php }
                } ?>
            </td>
        </tr>
    <?php } ?>
</table>
<?php if (in_array($session['user_type'],[ManagerController::USER_AGENCY, ManagerController::USER_SUPERADMIN, ManagerController::USER_ADMIN])) { ?>
    <div class="margin-top margin-bottom">
        <label>Active status:</label>
        <select name="user_active_status" required style="width: 200px">
            <option value="<?= ManagerController::STATUS_NO_ACTIVE ?>" <?=($userData->active_status == ManagerController::STATUS_NO_ACTIVE) ? 'selected' : '';?>>not active</option>
            <option value="<?= ManagerController::STATUS_ACTIVE ?>" <?=($userData->active_status == ManagerController::STATUS_ACTIVE) ? 'selected' : '';?>>active</option>
        </select>
    </div>
<?php } ?>
<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) { ?>
    <div class="margin-top margin-bottom">
        <label>User approve status:</label>
        <select name="approve_status" required style="width: 200px">
            <?php if (!empty($approve_statuses)) {
                foreach ($approve_statuses as $approveStatus) {
                    ?>
                    <option
                        value="<?=$approveStatus->id?>" <?=($userData->approve_status == $approveStatus->id) ? 'selected' : '';?>><?=$approveStatus->description?>
                    </option>
                <?php }
            } ?>
        </select>
    </div>
<?php } ?>
<button class="save_button btn">save</button>