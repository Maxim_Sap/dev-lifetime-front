<?php 

use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;
$serverUrl = $this->context->serverUrl;
//var_dump($session['permissions']); die;
$avatar = (!empty($session['avatar'])) ? $serverUrl.'/'.$session['avatar'] : '/img/no_avatar_normal.jpg';
$pageName = '';
if (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'index') {
    $pageName = 'homePage';
} elseif (Yii::$app->controller->id == 'blog') {
    $pageName = 'blog';
} elseif (Yii::$app->controller->id == 'finance') {
    $pageName = 'finance';
} elseif (Yii::$app->controller->id == 'message') {
    $pageName = 'message';
} elseif (Yii::$app->controller->id == 'default' && 
          (Yii::$app->controller->action->id == 'about' || Yii::$app->controller->action->id == 'history' ||
           Yii::$app->controller->action->id == 'instructions' || Yii::$app->controller->action->id == 'users-rules' ||
           Yii::$app->controller->action->id == 'contract' || Yii::$app->controller->action->id == 'license-agreement')) {
    $pageName = 'static';
} elseif (Yii::$app->controller->id == 'tasks') {
    $pageName = 'tasks';
} elseif (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'need-to-approve') {
    $pageName = 'need-to-approve';
} elseif (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'log') {
    $pageName = 'log';
} elseif (Yii::$app->controller->id == 'default' && (
          Yii::$app->controller->action->id == 'gift-list' || Yii::$app->controller->action->id == 'edit-shop-item' || Yii::$app->controller->action->id == 'my-gift' || Yii::$app->controller->action->id == 'add-gift')) {
    $pageName = 'gift';
} elseif ((isset($_GET) && isset($_GET['user_type']) && $_GET['user_type'] == 3) || Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'my-agency') {
    $pageName = 'agency';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'penalty' || Yii::$app->controller->action->id == 'penalty-table')) {
    $pageName = 'penalty';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'activity')) {
    $pageName = 'activity';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'add-user' || Yii::$app->controller->action->id == 'girls' || Yii::$app->controller->action->id == 'my-man' || Yii::$app->controller->action->id == 'signalizator-men' || Yii::$app->controller->action->id == 'interpreters' || Yii::$app->controller->action->id == 'user')) {
    $pageName = 'users';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'add-user' || Yii::$app->controller->action->id == 'girls' || Yii::$app->controller->action->id == 'my-man' || Yii::$app->controller->action->id == 'signalizator-men' || Yii::$app->controller->action->id == 'interpreters' || Yii::$app->controller->action->id == 'user')) {
    $pageName = 'users';
} elseif (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'settings') {
    $pageName = 'settings';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'pricelist' || Yii::$app->controller->action->id == 'agency-pricelist')) {
    $pageName = 'pricelist';
} elseif ((isset($_GET) && isset($_GET['user_type']) && $_GET['user_type'] == 4) || Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'my-interpreters') {
    $pageName = 'interpreters';
} elseif ((isset($_GET) && isset($_GET['user_type']) && $_GET['user_type'] == 2) || Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'my-girls') || (Yii::$app->controller->action->id == 'signalizator') || (Yii::$app->controller->action->id == 'letters') || (Yii::$app->controller->action->id == 'activity') || (Yii::$app->controller->action->id == 'finance') || (Yii::$app->controller->action->id == 'my-gift')) {
    $pageName = 'girls';
}


?>

<div class="tool-bar-content">
    <div class="tool-bar-header">
        <div class="greeting-block">
            <div class="greeting-avatar" style="background-image: url('<?= $avatar ?>')"></div>
            <span class="greeting-text">Hello, <span class="admin-name"><?= $session['userName'] ?></span></span>
            <span class="current-date"><span class="status"></span><span class="month"><?= date('M') ?></span> <span class="day"><?= date('d') ?></span>, <span class="year"><?= date('Y') ?></span> <span class="time"><?= date('h:i:s A') ?></span></span>
        </div>
    </div>

    <div class="tool-bar-body">
        <input type="hidden" id="agency_id" value="<?=isset($session['agency_id']) ? $session['agency_id'] : $session['user_id'] ;?>">
        <ul class="tools-nav">
            <li <?php if ($pageName == 'homePage') {echo 'class="active"';} ?>>
                <a href="/manager"><span class="icon-wrapper"><i class="fa fa-desktop" aria-hidden="true"></i></span>Рабочий стол</a>
            </li>
            <?php if (isset($session['permissions']) && $session['permissions']['users_access'] == ManagerController::ALLOW) { ?>
            <li <?php if ($pageName == 'girls') {echo 'class="active"';} ?>>
                <a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-group" aria-hidden="true"></i></span>Мои девочки</a>
                <ul class="sub-nav">
                    <li><a href="/manager/my-girls">Список анкет</a></li>                    
                    <li><a href="/manager/add-user?user_type=2">Добавить анкету</a></li>                    
                    <li><a href="/manager/activity">Учет активности девочек</a></li>
                    <li><a href="/manager/finance/">Финансовая информация</a></li>
                    <li><a href="/manager/my-gift">Подарки</a></li>
                </ul>
            </li>
            <?php } ?>
            <?php if (isset($session['permissions']) && $session['permissions']['letter_access'] == ManagerController::ALLOW) { ?>
            <li class="letters">
                <a href="/manager/letters/"><span class="icon-wrapper"><i class="fa fa-envelope" aria-hidden="true"></i></span>Письма</a>
            </li>
            <?php } ?>
            <?php if (isset($session['permissions']) && $session['permissions']['gift_access'] == ManagerController::ALLOW) { ?>
            <li <?php if (Yii::$app->controller->action->id == 'gift-list') {echo 'class="active"';} ?>>
                <a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-gift" aria-hidden="true"></i></span>Подарки</a>
                <ul class="sub-nav">
                    <li><a href="/manager/gift-list">Список подарков</a></li>
                    <li class="gift"><a href="/manager/my-gift">Статусы купленных подарков</a></li>
                </ul>
            </li>
            <?php } ?>
            <li>
                <a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-group" aria-hidden="true"></i></span>To Do</a>
                <ul class="sub-nav">
                    <?php if (isset($session['permissions']) && $session['permissions']['tasks_access'] == ManagerController::ALLOW) { ?>
                    <li class="tasks <?php if ($pageName == 'tasks') {echo 'active';} ?>">
                        <a href="/manager/agency-tasks"><span class="icon-wrapper"><i class="fa fa-tasks" aria-hidden="true"></i></span>Задачи</a>
                    </li>
                    <?php } ?>
                    <li class="message <?php if ($pageName == 'message') {echo 'active';} ?>">
                        <a href="/manager/message/"><span class="icon-wrapper"><i class="fa fa-envelope" aria-hidden="true"></i></span>Административные сообщения</a>
                    </li>
                    <li class="chat-mailing <?php if ($pageName == 'chat-mailing') {echo 'active';} ?>">
                        <a href="/manager/chat-mailing-templates"><span class="icon-wrapper"><i class="fa fa-id-card" aria-hidden="true"></i></span>Шаблоны для чата</a>
                    </li>     
                    <li class="message-mailing <?php if ($pageName == 'message-mailing') {echo 'active';} ?>">
                        <a href="/manager/message-mailing-templates"><span class="icon-wrapper"><i class="fa fa-id-card" aria-hidden="true"></i></span>Шаблоны для писем</a>
                    </li>                                    
                </ul>
            </li>
            <li>
                <a href="/manager/mailing-statistic"><span class="icon-wrapper"><i class="fa fa-envelope" aria-hidden="true"></i></span>Статистика рассылок</a>
            </li>
            <?php if (isset($session['permissions']) && $session['permissions']['finance_access'] == ManagerController::ALLOW) { ?>
            <li>
                <a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-usd" aria-hidden="true"></i></span>Финансы</a>
                <ul class="sub-nav">
                    <li><a href="/manager/finance/"><span class="icon-wrapper"><i class="fa fa-usd" aria-hidden="true"></i></span>Финансовая информация</a></li>
                    <li <?php if (Yii::$app->controller->action->id == 'penalty') {echo 'class="active"';} ?>>
                    <a href="/manager/penalty"><span class="icon-wrapper"><i class="fa fa-usd" aria-hidden="true"></i></span>Штрафы</a>
                    </li>                     
                    <li <?php if (Yii::$app->controller->action->id == 'balance') {echo 'class="active"';} ?>>
                        <a href="/manager/balance"><span class="icon-wrapper"><i class="fa fa-bar-chart" aria-hidden="true"></i></span>Баланс</a>
                    </li>
                    <li>
                        <a href="/manager/finance/agency-payments-schedule"><span class="icon-wrapper"><i class="fa fa-gears" aria-hidden="true"></i></span>График выплат</a>
                    </li>
                </ul>
            </li>
            <?php } ?>
            <li <?php if (Yii::$app->controller->action->id == 'wait-to-approve') {echo 'class="active"';} ?>>
                <a href="/manager/wait-to-approve"><span class="icon-wrapper"><i class="fa fa-upload" aria-hidden="true"></i></span>На проверке</a>
            </li>
            <?php if (false) { ?>
            <li <?php if ($pageName == 'log') {echo 'class="active"';} ?>>
                <a href="/manager/log"><span class="icon-wrapper"><i class="fa fa-upload" aria-hidden="true"></i></span>История операций (Log)</a>
            </li>
            <?php } ?>
            <?php if (isset($session['permissions']) && $session['permissions']['users_access'] == ManagerController::ALLOW) { ?>
            <li <?php if ($pageName == 'interpreters') {echo 'class="active"';} ?>>
                <a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-user" aria-hidden="true"></i></span>Переводчики</a>
                <ul class="sub-nav">
                    <li><a href="/manager/my-interpreters">Список переводчиков</a></li>
                    <li><a href="/manager/add-user?user_type=4">Добавить нового переводчика</a></li>
                </ul>
            </li>
            <?php } ?>
            <?php if (isset($session['permissions']) && $session['permissions']['finance_access'] == ManagerController::ALLOW) { ?>
            <li <?php if (Yii::$app->controller->action->id == 'agency-pricelist') {echo 'class="active"';} ?>>
                <a href="/manager/agency-pricelist"><span class="icon-wrapper"><i class="fa fa-bar-chart" aria-hidden="true"></i></span>Прайслист</a>
            </li>
            <?php } ?>
            <li>
                <a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-question" aria-hidden="true"></i></span>Помощь</a>
                <ul class="sub-nav">
                    <li <?php if (Yii::$app->controller->action->id == 'instructions') {echo 'class="active"';} ?>>
                        <a href="/manager/instructions"><span class="icon-wrapper"><i class="fa fa-file-text" aria-hidden="true"></i></span>Инструкции</a>
                    </li>
                    <li <?php if (Yii::$app->controller->action->id == 'rules') {echo 'class="active"';} ?>>
                        <a href="/manager/rules"><span class="icon-wrapper"><i class="fa fa-file-text" aria-hidden="true"></i></span>Правила для агенств</a>
                    </li>
                    <li <?php if (Yii::$app->controller->action->id == 'users-rules') {echo 'class="active"';} ?>>
                        <a href="/manager/users-rules"><span class="icon-wrapper"><i class="fa fa-file-text" aria-hidden="true"></i></span>Правила для пользователей</a>
                    </li>
                </ul>
            </li>
            <?php if (false && isset($session['permissions']) && $session['permissions']['users_access'] == ManagerController::ALLOW) { ?>
            <li>
                <a href="/manager/my-girl-video"><span class="icon-wrapper"><i class="fa fa-video-camera" aria-hidden="true"></i></span>Просмотреть камеры девушек</a> 
            </li>  
            <?php } ?>          
            <li>
                <a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-gears" aria-hidden="true"></i></span>Настройка</a>
                <ul class="sub-nav">
                    <li <?php if (Yii::$app->controller->action->id == 'about') {echo 'class="active"';} ?>>
                        <a href="/manager/about"><span class="icon-wrapper"><i class="fa fa-file-text" aria-hidden="true"></i></span>О нас</a>
                    </li>
                    <li <?php if (Yii::$app->controller->action->id == 'contract') {echo 'class="active"';} ?>>
                        <a href="/manager/contract"><span class="icon-wrapper"><i class="fa fa-file-text" aria-hidden="true"></i></span>Договор</a>
                    </li>
                    <?php if (false && isset($session['permissions']) && $session['permissions']['users_access'] == ManagerController::ALLOW) { ?>
                    <li <?php if ($_SERVER['REQUEST_URI'] == '/manager/edit-user/'.$session['agency_id']) {echo 'class="active"';} ?>>
                        <a href="/manager/edit-user/<?=$session['agency_id']?>"><span class="icon-wrapper"><i class="fa fa-gears" aria-hidden="true"></i></span>Профиль агенства</a>
                    </li>
                    <li><a href="/manager/my-admins"><span class="icon-wrapper"><i class="fa fa-users" aria-hidden="true"></i></span>Список админов</a></li>
                    <li><a href="/manager/add-user?user_type=7"><span class="icon-wrapper"><i class="fa fa-user-plus" aria-hidden="true"></i></span>Добавить администратора</a></li>
                    <?php } ?>
                    <li><a href="/manager/edit-password/<?= $session['user_id']; ?>"><span class="icon-wrapper"><i class="fa fa-key" aria-hidden="true"></i></span>Изменить пароль</a></li>
                </ul>
            </li>  
            <li>
                <a href="/manager/logout"><span class="icon-wrapper"><i class="fa fa-sign-out" aria-hidden="true"></i></span>Выход</a>
            </li>
        </ul>
    </div>

    <div class="tool-bar-footer">
        <button class="collapse-bar"><i class="fa fa-angle-double-left" aria-hidden="true"></i>Collapse Menu</button>
    </div>
</div>
<style>
    #content-area .content-area-inner {
        min-height: 1300px;
    }
</style>