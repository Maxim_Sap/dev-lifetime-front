<?php
    use app\modules\manager\controllers\ManagerController;

    $session = Yii::$app->session;
    $referral_link = isset($userData->affiliates_info->referral_link) ? $userData->affiliates_info->referral_link : '';
    $name = isset($userData->affiliates_info->name) ? $userData->affiliates_info->name : '';
    $m_phone = isset($userData->affiliates_info->m_phone) ? $userData->affiliates_info->m_phone : '';
?>
<table class="margin-top margin-bottom">
    <tr>
        <td>User ID:</td>
        <td><span><?=$userData->userID;?></span></td>
        <td>Email</td>
        <td class="edit"><input type="email" name="email" class="custom-border"
                                value="<?=$userData->email;?>">
        </td>
    </tr>
    <tr>
        <td>Name</td>
        <td class="edit">
            <input type="text" name="first_name" class="custom-border" value="<?= $name?>" required>
        </td>
        <td>Mob phone</td>
        <td class="edit">
            <input type="text" name="m_phone" class="custom-border" value="<?= $m_phone?>">
        </td>
    </tr>
    <tr>
        <td>Referral code</td>
        <td><?=$referral_link?></td>
        <td>Referral link</td>
        <td>
            <a href="javascript:void(0);" id="referral_link" onclick="copyToClipboard(this);" style="cursor: pointer;" title="click to copy"><?="https://".$_SERVER['SERVER_NAME']."?referral_link=".$referral_link;?></a>
        </td>
    </tr>
</table>
<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) { ?>
    <div class="margin-top margin-bottom">
        <label>Active status:</label>
        <select name="user_active_status" required style="width: 180px">
            <option value="<?=ManagerController::STATUS_NO_ACTIVE?>" <?=($userData->active_status == ManagerController::STATUS_NO_ACTIVE) ? 'selected' : '';?>>not active</option>
            <option value="<?php echo ManagerController::STATUS_ACTIVE; ?>" <?=($userData->active_status == ManagerController::STATUS_ACTIVE) ? 'selected' : '';?>>active</option>
        </select>
    </div>
    <div class="margin-top margin-bottom">
        <label>User approve status:</label>
        <select name="approve_status" required style="width: 180px">
            <?php if (!empty($approve_statuses)) {
                foreach ($approve_statuses as $approveStatus) {
                    ?>
                    <option
                        value="<?=$approveStatus->id?>" <?=($userData->approve_status == $approveStatus->id) ? 'selected' : '';?>><?=$approveStatus->description?></option>
                <?php }
            } ?>
        </select>
    </div>
    <button class="save_button btn">save</button>
<?php }else{ ?>
    <button class="save_button btn">save</button>
    <?php
    $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved</span>';
    switch ($userData->approve_status){
        case 1:
            $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved</span>';
            break;
        case 2:
            $approve_status = '<span style="color: lawngreen; margin-left: 10px;font-weight: bold;">Approve in progress</span>';
            break;
        case 3:
            $approve_status = '<span style="color: #ffcc00; margin-left: 10px;font-weight: bold;">Approve delcine</span>';
            break;
        case 4:
            $approve_status = '<span style="color: #ffcc00; margin-left: 10px;font-weight: bold;">Approved</span>';
            break;
        default:
            $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved</span>';
            break;
    }
    echo $approve_status; ?>
<?php } ?>
