<?php	
	
	use app\modules\manager\controllers\ManagerController;
	$serverUrl = $this->context->serverUrl;
	
	$avatar = !empty($girl->medium_thumb) ? $serverUrl.'/'.$girl->medium_thumb : '/img/no_avatar_normal.jpg';
?>
<div class="girl-tile" data-user-id="<?=$girl->id;?>">
	<a href="/manager/edit-user/<?=$girl->id;?>" class="girl-link">
		<div class="girl-photo" style="background-image:url(<?=$avatar;?>);"></div>
		<?php if($girl->approve_status_id == 4){?>
			<div class="approved-status yes"><span>approved</span></div>
		<?php }elseif($girl->approve_status_id == 1 || $girl->approve_status_id == 2){?>
			<div class="approved-status no"><span>not approved</span></div>
		<?php }elseif($girl->approve_status_id == 3){?>
			<div class="approved-status decline"><span>decline</span></div>
		<?php } ?>
	</a>   
	<div class="girl-meta">
		<div class="meta-left">
			<div class="girl-name"><?=$girl->first_name;?></div>
			<div class="girl-id">ID - <?=$girl->id;?></div>
		</div>
		<div class="meta-right">
			<div class="controls">
				<a href="/manager/activity/<?=$girl->id;?>" title="activity">
					<i class="fa fa-clock-o" aria-hidden="true"></i>
				</a>
				<a href="/manager/edit-user/<?=$girl->id;?>" title="edit">
					<i class="fa fa-pencil" aria-hidden="true"></i>
				</a>
				<a href="#" data-user-id="<?=$girl->id;?>" class="user-approve-status" title="<?=($girl->approve_status_id == 2) ? 'withdraw' : 'send to approve'?>">
					<i class="fa <?=($girl->approve_status_id != 2) ? 'fa-hand-o-right' : 'fa-hand-o-left'?>" aria-hidden="true"></i>
				</a>
				<a href="#" data-user-id="<?=$girl->id;?>" class="user-activate" title="<?=($girl->status == ManagerController::STATUS_ACTIVE) ? 'deactivate' : 'activate'?>">
					<i class="fa <?=($girl->status == ManagerController::STATUS_ACTIVE) ? 'fa-toggle-on' : 'fa-toggle-off'?>" aria-hidden="true"></i>
				</a>
				<a href="/manager/delete-user/<?=$girl->id;?>" title="delete user" onclick="if(!confirm('Are you sure that you want to delete this user?')){return false;}">
					<i class="fa fa-trash" aria-hidden="true"></i>
				</a>
			</div>
		</div>
	</div>

</div>