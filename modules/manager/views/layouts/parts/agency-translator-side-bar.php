<?php 

use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;
$serverUrl = $this->context->serverUrl;

//var_dump($session['avatar']); die;
if (!empty($session['avatar']->small)) {
    $avatar = $serverUrl.'/'.$session['avatar']->small;
} elseif (!empty($session['avatar'])) {
    $avatar = $serverUrl.'/'.$session['avatar'];
} else {
    $avatar = '/img/no_avatar_small.jpg';
}
$pageName = '';
if (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'index') {
    $pageName = 'homePage';
} elseif (Yii::$app->controller->id == 'blog') {
    $pageName = 'blog';
} elseif (Yii::$app->controller->id == 'finance') {
    $pageName = 'finance';
} elseif (Yii::$app->controller->id == 'message') {
    $pageName = 'message';
} elseif (Yii::$app->controller->id == 'default' && 
          (Yii::$app->controller->action->id == 'about' || Yii::$app->controller->action->id == 'history' ||
           Yii::$app->controller->action->id == 'instructions' || Yii::$app->controller->action->id == 'users-rules' ||
           Yii::$app->controller->action->id == 'contract' || Yii::$app->controller->action->id == 'license-agreement')) {
    $pageName = 'static';
} elseif (Yii::$app->controller->id == 'tasks') {
    $pageName = 'tasks';
} elseif (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'need-to-approve') {
    $pageName = 'need-to-approve';
} elseif (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'log') {
    $pageName = 'log';
} elseif (Yii::$app->controller->id == 'default' && (
          Yii::$app->controller->action->id == 'gift-list' || Yii::$app->controller->action->id == 'edit-shop-item' || Yii::$app->controller->action->id == 'my-gift' || Yii::$app->controller->action->id == 'add-gift')) {
    $pageName = 'gift';
} elseif ((isset($_GET) && isset($_GET['user_type']) && $_GET['user_type'] == 3) || Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'my-agency') {
    $pageName = 'agency';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'penalty' || Yii::$app->controller->action->id == 'penalty-table')) {
    $pageName = 'penalty';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'activity')) {
    $pageName = 'activity';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'add-user' || Yii::$app->controller->action->id == 'girls' || Yii::$app->controller->action->id == 'my-man' || Yii::$app->controller->action->id == 'signalizator-men' || Yii::$app->controller->action->id == 'interpreters' || Yii::$app->controller->action->id == 'user')) {
    $pageName = 'users';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'add-user' || Yii::$app->controller->action->id == 'girls' || Yii::$app->controller->action->id == 'my-man' || Yii::$app->controller->action->id == 'signalizator-men' || Yii::$app->controller->action->id == 'interpreters' || Yii::$app->controller->action->id == 'user')) {
    $pageName = 'users';
} elseif (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'settings') {
    $pageName = 'settings';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'pricelist' || Yii::$app->controller->action->id == 'agency-pricelist')) {
    $pageName = 'pricelist';
} elseif ((isset($_GET) && isset($_GET['user_type']) && $_GET['user_type'] == 4) || Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'my-interpreters') {
    $pageName = 'interpreters';
} elseif ((isset($_GET) && isset($_GET['user_type']) && $_GET['user_type'] == 2) || Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'my-girls') || (Yii::$app->controller->action->id == 'signalizator') || (Yii::$app->controller->action->id == 'letters') || (Yii::$app->controller->action->id == 'activity') || (Yii::$app->controller->action->id == 'finance') || (Yii::$app->controller->action->id == 'my-gift')) {
    $pageName = 'girls';
}


?>

<div class="tool-bar-content">
    <div class="tool-bar-header">
        <div class="greeting-block">
            <div class="greeting-avatar" style="background-image: url('<?= $avatar ?>')"></div>
            <span class="greeting-text">Hello, <span class="admin-name"><?= $session['userName'] ?></span></span>
            <span class="current-date"><span class="status"></span><span class="month"><?= date('M') ?></span> <span class="day"><?= date('d') ?></span>, <span class="year"><?= date('Y') ?></span> <span class="time"><?= date('h:i:s A') ?></span></span>
        </div>
    </div>

    <div class="tool-bar-body">
    <input type="hidden" id="agency_id" value="<?=isset($session['agency_id']) ? $session['agency_id'] : $session['user_id'] ;?>">
        <ul class="tools-nav">
            <li <?php if ($pageName == 'homePage') {echo 'class="active"';} ?>>
                <a href="/manager"><span class="icon-wrapper"><i class="fa fa-desktop" aria-hidden="true"></i></span>Рабочий стол</a>
            </li>       
            <li <?php if (Yii::$app->controller->action->id == 'about') {echo 'class="active"';} ?>>
                <a href="/manager/about"><span class="icon-wrapper"><i class="fa fa-file-text" aria-hidden="true"></i></span>О нас</a>            
            </li>
            <li <?php if (Yii::$app->controller->action->id == 'instructions') {echo 'class="active"';} ?>>
                <a href="/manager/instructions"><span class="icon-wrapper"><i class="fa fa-file-text" aria-hidden="true"></i></span>Инструкции</a>            
            </li>            
            <li <?php if (Yii::$app->controller->action->id == 'users-rules') {echo 'class="active"';} ?>>
                <a href="/manager/users-rules"><span class="icon-wrapper"><i class="fa fa-file-text" aria-hidden="true"></i></span>Правила для пользователей</a>            
            </li>             
            <li <?php if (Yii::$app->controller->action->id == 'gift-list') {echo 'class="active"';} ?>>
                <a href="/manager/gift-list"><span class="icon-wrapper"><i class="fa fa-gift" aria-hidden="true"></i></span>Перечень подарков</a>
            </li>
            <li <?php if ($pageName == 'girls') {echo 'class="active"';} ?>>
                <a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-group" aria-hidden="true"></i></span>Мои девочки</a>
                <ul class="sub-nav">
                    <li><a href="/manager/my-girls">Список анкет</a></li>
                    <li><a href="/manager/letters/">Письма</a></li>
                    <li><a href="/manager/chat/">Мульти чат</a></li>
                    <li><a href="/manager/activity">Учет активности девочек</a></li>
                </ul>
            </li>
            <li <?php if ($_SERVER['REQUEST_URI'] == '/manager/edit-user/'.$session['user_id']) {echo 'class="active"';} ?>>
                <a href="/manager/edit-user/<?=$session['user_id']?>"><span class="icon-wrapper"><i class="fa fa-gears" aria-hidden="true"></i></span>Профиль переводчика</a> 
            </li>           
            <li>
                <a href="/manager/logout"><span class="icon-wrapper"><i class="fa fa-sign-out" aria-hidden="true"></i></span>Выход</a>  
            </li>           
        </ul>
    </div>

    <div class="tool-bar-footer">
        <button class="collapse-bar"><i class="fa fa-angle-double-left" aria-hidden="true"></i>Collapse Menu</button>
    </div>
</div>
<style>
    #content-area .content-area-inner {
        min-height: 1300px;
    }
</style>
