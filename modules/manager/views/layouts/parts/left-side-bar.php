<?php
use app\modules\manager\controllers\ManagerController;

$session = Yii::$app->session;
$apiServer = $this->context->serverUrl;
?>

<div id="tool-bar">
    <input type="hidden" id="apiServer" value="<?=$apiServer?>">
    <?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) { //для админа
        echo $this->render('admin-left-side-bar.php',['session'=>$session]);
    } elseif ($session['user_type'] == ManagerController::USER_SITEADMIN) {
        echo $this->render('siteadmin-left-side-bar.php',['session'=>$session]);
    } elseif($session['user_type'] == ManagerController::USER_AGENCY) { //для агенства
        echo $this->render('agency-left-side-bar.php',['session'=>$session]);
    } elseif($session['user_type'] == ManagerController::USER_INTERPRETER) { //для переводчика
        echo $this->render('agency-translator-side-bar.php',['session'=>$session]);
    } elseif($session['user_type'] == ManagerController::USER_ADMIN) { //для админов агенства
        echo $this->render('agency-admin-left-side-bar.php',['session'=>$session]);
    }elseif($session['user_type'] == ManagerController::USER_AFFILIATE){ //для аффилейта
        echo $this->render('affiliates-side-bar.php',['session'=>$session]);
    } ?>

</div>