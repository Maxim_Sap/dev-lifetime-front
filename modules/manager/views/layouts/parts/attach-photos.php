<?php
$placeName = isset($place) && $place != '' ? $place : 'edit-user';
?>
<div class="image-cotent">
    <?php if(!empty($photos)){
    	foreach($photos as $photo){ ?>
			<div class="attach-image-items fl" style="position: relative">
				<?php if($photo->approve_status == 0){?>
					<div class="approved-status no"><span>not approved</span></div>
				<?php }else{ ?>
					<div class="approved-status yes"><span>approved</span></div>
				<?php }?>
				<img data-original="<?=$this->context->serverUrl."/".$photo->original_image;?>" src="<?=$this->context->serverUrl."/".$photo->medium_thumb;?>" <?php if($photo->status == 0){echo 'style="opacity:0.3;"';}?>>
		        <div class="remove_foto">
					<?php if($photo->status == 1){?>
					<a class='delete_link' href="javascript:void(0);" onclick="window['adminAccountLibrary']['setPhotoStatus'](<?=$photo->id;?>,0,'<?=$placeName;?>')">
		                <i class="fa fa-trash-o text-red" aria-hidden="true"></i>
		                Remove foto
		            </a>
					<?php }else{ ?>
					<a class='delete_link' href="javascript:void(0);" onclick="window['adminAccountLibrary']['setPhotoStatus'](<?=$photo->id;?>,1,'<?=$placeName;?>')">
						<i class="fa fa-trash-o text-red" aria-hidden="true"></i>
						Recovery foto
					</a>
				<?php } ?>
		        </div>
		    </div>
    	<?php }
		}?>
</div>