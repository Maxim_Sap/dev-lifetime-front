<?php if (isset($usersActivityInfo) && !empty($usersActivityInfo)) {
	foreach($usersActivityInfo as $activity){
		$activity = (object)$activity;
		$duration = strtotime($activity->ended_at) - strtotime($activity->started_at);
		?>
	<tr>
		<td><?php if (!empty($activity->first_name)) { echo trim($activity->first_name ." ". $activity->last_name); } elseif (!empty($activity->name)) {echo $activity->name; };?></td>
		<td><?=$activity->started_at;?></td>
		<td><?=(strtotime($activity->ended_at) < time()-60) ? $activity->ended_at: 'на сайте';?></td>
		<td><?=date("H:i:s", mktime(0, 0, $duration));?></td>
		<td><?=date("H:i:s", mktime(0, 0, $activity->cam_duration));?></td>
	</tr>
	<?php }	
}?>