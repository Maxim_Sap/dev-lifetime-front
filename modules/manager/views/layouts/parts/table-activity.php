<?php if (isset($usersActivityInfo) && !empty($usersActivityInfo)){
	foreach($usersActivityInfo as $activityInfo){
		$activityInfo = (object)$activityInfo;
		$duration = strtotime($activityInfo->ended_at) - strtotime($activityInfo->started_at);		
		?>
	<tr>
		<?php if (!empty($activityInfo->first_name)) { ?>
		<td><?=$activityInfo->first_name ?></td>
		<?php } elseif (!empty($activityInfo->name)) {
			echo '<td>' . $activityInfo->name . "</td>";
		} else {
			//echo '<td></td>';
		}?>
		<td><?=$activityInfo->started_at;?></td>
		<td><?=$activityInfo->ended_at;?></td>
		<td><?=date("H:i:s", mktime(0, 0, $duration));?></td>
		<td><?=date("H:i:s", mktime(0, 0, $activityInfo->cam_duration));?></td>
	</tr>
	<?php }	
}?>