<?php
switch($page_name){
	case 'inbox':
		$function = 'window[\'messageAdminLibrary\'][\'messagePagination\']';
		break;
	case 'outbox':
		$function = 'window[\'messageAdminLibrary\'][\'messagePagination\']';
		break;
	case 'activity-page':
		$function = 'window[\'adminAccountLibrary\'][\'activityPagination\']';
		break;
	case 'premiumPhotos':
		$function = 'window[\'adminAccountLibrary\'][\'financePagination\']';
		break;
	case 'premiumVideo':
		$function = 'window[\'adminAccountLibrary\'][\'financePagination\']';
		break;
	case 'letters':
		$function = 'window[\'adminAccountLibrary\'][\'financePagination\']';
		break;
	case 'chats':
		$function = 'window[\'adminAccountLibrary\'][\'financePagination\']';
		break;
	case 'video-chat':
		$function = 'window[\'adminAccountLibrary\'][\'financePagination\']';
		break;
	case 'gifts':
		$function = 'window[\'adminAccountLibrary\'][\'financePagination\']';
		break;
	case 'tasks':
		$function = 'window[\'adminAccountLibrary\'][\'tasksPagination\']';
		break;
	case 'log':
		$function = 'window[\'adminAccountLibrary\'][\'logPagination\']';
		break;	
	case 'history':
		$function = 'window[\'adminAccountLibrary\'][\'historyPagination\']';
		break;		
}
	$count_on_page = ($limit > $letters_count) ? $letters_count : $limit;
	$total = (($letters_count - 1) / $count_on_page) + 1;
	$total =  intval($total);
	$page_prev = $page - 1;
	$page_next = $page + 1;
?>
<?php
	$CurPage = '<li class="active"><a href="javascript:void(0);">'.$page.'</a></li>';
	if ($page > 7) {$pervpage = "<li class=\"prev\"><a href='#' onclick=\"$function('1');return false;\">1</a></li><li><a href='#' class='dots'>...</a></li>";}else{$pervpage = '';}
	if ($page == 7) {$pervpage = "<li class=\"prev\"><a href='#' onclick=\"$function('1');return false;\">1</a></li>";}
	if ($page <= $total-7) {$nextpage = "<li class=\"next\"><a href='#' class='dots'>...</a></li> <li><a href='#' onclick=\"$function($total);return false;\">".$total.'</a></li>';}else{$nextpage = '';}
	if($page - 5 > 0) {$page5left = "<li><a href='#' onclick=\"$function(".($page-5).");return false;\">". ($page - 5) .'</a></li>';}else{$page5left = "";}
	if($page - 4 > 0) {$page4left = "<li><a href='#' onclick=\"$function(".($page-4).");return false;\">". ($page - 4) .'</a></li>';}else{$page4left = "";}
	if($page - 3 > 0) {$page3left = "<li><a href='#' onclick=\"$function(".($page-3).");return false;\">". ($page - 3) .'</a></li>';}else{$page3left = "";}
	if($page - 2 > 0) {$page2left = "<li><a href='#' onclick=\"$function(".($page-2).");return false;\">". ($page - 2) .'</a></li>';}else{$page2left = "";}
	if($page - 1 > 0) {$page1left = "<li><a href='#' onclick=\"$function(".($page-1).");return false;\">". ($page - 1) .'</a></li>';}else{$page1left = "";}
	if($page + 5 <= $total) {$page5right = "<li><a href='#' onclick=\"$function(".($page + 5).");return false;\">". ($page + 5) .'</a></li>';}else{$page5right = "";}
	if($page + 4 <= $total) {$page4right = "<li><a href='#' onclick=\"$function(".($page + 4).");return false;\">". ($page + 4) .'</a></li>';}else{$page4right = "";}
	if($page + 3 <= $total) {$page3right = "<li><a href='#' onclick=\"$function(".($page + 3).");return false;\">". ($page + 3) .'</a></li>';}else{$page3right = "";}
	if($page + 2 <= $total) {$page2right = "<li><a href='#' onclick=\"$function(".($page + 2).");return false;\">". ($page + 2) .'</a></li>';}else{$page2right = "";}
	if($page + 1 <= $total) {$page1right = "<li><a href='#' onclick=\"$function(".($page + 1).");return false;\">". ($page + 1) .'</a></li>';}else{$page1right = "";}
	$navigLine = $pervpage.$page5left.$page4left.$page3left.$page2left.$page1left.$CurPage.$page1right.$page2right.$page3right.$page4right.$page5right.$nextpage;
?>
<div class="page-nav-wrapper">	
	<ul class="page-navigation list-inline">
		<?=$navigLine?>			
	</ul>
</div>
