<?php 
use app\modules\manager\controllers\ManagerController;
use yii\helpers\ArrayHelper;

$session = Yii::$app->session;

?>
<table class="margin-top margin-bottom">
    <input type="hidden" name="sex" value="<?=(!empty($userData->personalInfo->sex)) ? $userData->personalInfo->sex : ManagerController::USER_FEMALE ?>" />
    <tr>
        <td>User ID:</td>
        <td><span><?=$userData->personalInfo->id;?></span></td>
        <td>Weight</td>
        <td class="edit"><input type="text" name="weight" class="custom-border"
                                value="<?=(!empty($userData->personalInfo->weight)) ? $userData->personalInfo->weight : "";?>"
                                placeholder="---"></td>
    </tr>
    <tr>
        <td>First Name</td>
        <td class="edit"><input type="text" name="first_name" class="custom-border"
                                value="<?=$userData->personalInfo->first_name;?>" required></td>
        <td>Height</td>
        <td class="edit"><input type="text" name="height" class="custom-border"
                                value="<?=(!empty($userData->personalInfo->height)) ? $userData->personalInfo->height : "";?>"
                                placeholder="---"></td>
    </tr>
    <tr>
        <td>Last Name</td>
        <td class="edit"><input type="text" name="last_name" class="custom-border"
                                value="<?=(!empty($userData->personalInfo->last_name)) ? $userData->personalInfo->last_name : "";?>"
                                placeholder="---"></td>
        <td>Hair</td>
        <td>
            <div class="custom-select">
                <select class="selectpicker custom-border" name="hair" data-actions-box="true">
                    <option value="">---</option>
                    <?php $hairArray = ArrayHelper::map($hairColors, 'id', 'name');
                    foreach ($hairArray as $key => $name){ ?>
                        <option
                            value="<?=$key;?>" <?=($userData->personalInfo->hair == $key) ? "selected" : "";?>>
                            <?=$name?>
                        </option>
                    <?php }?>
                </select>
            </div>
        </td>
    </tr>
    <tr>
        <td>Your email</td>
        <td class="edit"><input type="email" name="email" class="custom-border"
                                value="<?=$userData->email;?>"></td>
        <td>Date of birth</td>
        <td class="edit"><input type="date" name="birthday" class="custom-border"
                                value="<?=$userData->personalInfo->birthday;?>"
                                placeholder="dd.mm.YY"></td>
    </tr>
    <tr>
        <td>Country</td>
        <td>
            <div class="custom-select">
                <select class="selectpicker custom-border" name="country" data-live-search="true"
                        data-actions-box="true" data-size="8">
                    <option value="">---</option>
                    <?php if (!empty($countryList)) {
                        foreach ($countryList as $country) {
                            ?>
                            <option
                                value="<?=$country->id;?>" <?=(isset($userData->location->country_id) && $userData->location->country_id == $country->id) ? "selected" : "";?>><?=$country->name;?></option>
                        <?php }
                    } ?>
                </select>
            </div>
        </td>
        <td>Eyes</td>
        <td>
            <div class="custom-select">
                <select class="selectpicker custom-border" name="eyes" data-actions-box="true"
                        data-title="---">
                    <option value="">---</option>
                    <?php $eyesColorArray = ArrayHelper::map($eyesColors, 'id', 'name');                    
                    foreach ($eyesColorArray as $key => $name){ ?>
                        <option
                            value="<?=$key;?>" <?=($userData->personalInfo->eyes == $key) ? "selected" : "";?>>
                            <?=$name?>
                        </option>
                    <?php }?>
                </select>
            </div>
        </td>
    </tr>
    <tr>
        <td>Region</td>
        <td class="edit"><input type="text" name="region" class="custom-border"
                                value="<?=(isset($userData->city)) ? $userData->city : "Not defined";?>"
                                placeholder="<?=(isset($userData->city)) ? "Not defined" : "---";?>">                                
        </td>
        <td>Education</td>
        <td>
            <div class="custom-select">
                <select class="selectpicker custom-border" name="education" data-actions-box="true"
                        data-title="---">
                    <option value="">---</option>
                    <?php $educationArray = ['None','Primary School','Hight School','College','University Degree'];
                    $i = 0;
                    foreach ($educationArray as $edication){ $i++?>
                        <option
                            value="<?=$i;?>" <?=($userData->personalInfo->education == $i) ? "selected" : "";?>>
                            <?=$educationArray[$i-1]?>
                        </option>
                    <?php }?>
                </select>
            </div>
        </td>
    </tr>
    <tr>
        <td>Religion</td>
        <td>
            <div class="custom-select">
                <select class="selectpicker custom-border" name="religion" data-actions-box="true"
                        data-title="---">
                    <option value="">---</option>
                    <?php $religionArray = ['Catholicity','Christianity','Ateism','Islam','Hinduism','Buddhism','Folk','Judaism','Orthodox','Other'];
                    $i = 0;
                    foreach ($religionArray as $religion){ $i++?>
                        <option
                            value="<?=$i;?>" <?=($userData->personalInfo->religion == $i) ? "selected" : "";?>>
                            <?=$religionArray[$i-1]?>
                        </option>
                    <?php }?>
                </select>
            </div>
        </td>
        <td>Ocupation</td>
        <td class="edit"><input type="text" name="occupation" class="custom-border"
                                value="<?=(!empty($userData->personalInfo->occupation)) ? $userData->personalInfo->occupation : "";?>"
                                placeholder="---">
        </td>
    </tr>
    <tr>
        <td>Ethnicity</td>
        <td>
            <div class="custom-select">
                <select class="selectpicker custom-border" name="ethnos" data-actions-box="true"
                        data-title="---">
                    <option value="">---</option>
                    <?php $ethnosArray = ['European','African','American','Asian','East Asian','Spanish','Indian','Latin American','Mediterranean','Caucasian','Middle Eastern','Mixed'];
                    $i = 0;
                    foreach ($ethnosArray as $ethnos){ $i++?>
                        <option
                            value="<?=$i;?>" <?=($userData->personalInfo->ethnos == $i) ? "selected" : "";?>>
                            <?=$ethnosArray[$i-1]?>
                        </option>
                    <?php }?>
                </select>
            </div>
        </td>
        <td>English level</td>
        <td>
            <div class="custom-select">
                <select class="selectpicker custom-border" name="english_level" data-actions-box="true"
                        data-title="---">
                    <option value="">---</option>
                    <?php $englishLevelArray = ['Beginner','Intermediate','Advanced','Fluent'];
                    $i = 0;
                    foreach ($englishLevelArray as $englishLevel){ $i++?>
                        <option
                            value="<?=$i;?>" <?=($userData->personalInfo->english_level == $i) ? "selected" : "";?>>
                            <?=$englishLevelArray[$i-1]?>
                        </option>
                    <?php }?>
                </select>
            </div>
        </td>
    </tr>
    <tr>
        <td>Address</td>
        <td class="edit"><input type="text" name="address" class="custom-border"
                                value="<?=(!empty($userData->personalInfo->address)) ? $userData->personalInfo->address : "";?>"
                                placeholder="---">
        </td>
        <td>Other languages</td>
        <td class="edit"><input type="text" name="other_language" class="custom-border"
                                value="<?=(!empty($userData->personalInfo->other_language)) ? $userData->personalInfo->other_language : "";?>"
                                placeholder="---">
        </td>
    </tr>
    <tr>
        <td>Marital status</td>
        <td>
            <div class="custom-select">
                <select class="selectpicker custom-border" name="marital" data-actions-box="true"
                        data-title="---">
                    <option value="">---</option>
                    <?php $maritalArray = ['Single','Married','Divorced','Widowed','Complicated'];
                    $i = 0;
                    foreach ($maritalArray as $maritalStatus){ $i++?>
                        <option
                            value="<?=$i;?>" <?=($userData->personalInfo->marital == $i) ? "selected" : "";?>>
                            <?=$maritalArray[$i-1]?>
                        </option>
                    <?php }?>
                </select>
            </div>
        </td>
        <td>Smoking</td>
        <td>
            <div class="custom-select">
                <select class="selectpicker custom-border" name="smoking" data-actions-box="true"
                        data-title="---">
                    <option value="">---</option>
                    <?php $smokingArray = ['Yes','No','Casual','Heavy'];
                    $i = 0;
                    foreach ($smokingArray as $smoking){ $i++?>
                        <option
                            value="<?=$i;?>" <?=($userData->personalInfo->smoking == $i) ? "selected" : "";?>>
                            <?=$smokingArray[$i-1]?>
                        </option>
                    <?php }?>
                </select>
            </div>
        </td>
    </tr>
    <tr>
        <td>Number of children</td>
        <td class="edit"><input type="text" name="kids" class="custom-border"
                                value="<?=(!empty($userData->personalInfo->kids)) ? $userData->personalInfo->kids : "";?>"
                                placeholder="---"></td>
        <td>Drinking</td>
        <td>
            <div class="custom-select">
                <select class="selectpicker custom-border" name="alcohol" data-actions-box="true"
                        data-title="---">
                    <option value="">---</option>
                    <?php $alcoholArray = ['Yes','No','Casual','Heavy'];
                    $i = 0;
                    foreach ($alcoholArray as $alcohol){ $i++?>
                        <option
                            value="<?=$i;?>" <?=($userData->personalInfo->alcohol == $i) ? "selected" : "";?>>
                            <?=$alcoholArray[$i-1]?>
                        </option>
                    <?php }?>
                </select>
            </div>
        </td>
    </tr>
    <tr>
        <td>Looking for an age group</td>
        <td>
            <div class="custom-select small fl">
                <select class="selectpicker custom-border sm_selectpicker" name="looking_age_from"
                        data-actions-box="true" data-title="---">
                    <option value="">---</option>
                    <?php for ($i = 18; $i <= 65; $i++) { ?>
                        <option
                            value="<?=$i;?>" <?=($userData->personalInfo->looking_age_from == $i) ? "selected" : "";?>><?=$i;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="delimiter fl">-</div>
            <div class="custom-select small fl">
                <select class="selectpicker custom-border sm_selectpicker" name="looking_age_to"
                        data-actions-box="true" data-title="---">
                    <option value="">---</option>
                    <?php for ($i = 18; $i <= 65; $i++) { ?>
                        <option
                            value="<?=$i;?>" <?=($userData->personalInfo->looking_age_to == $i) ? "selected" : "";?>><?=$i;?></option>
                    <?php } ?>
                </select>
            </div>
        </td>
        <td>
            <div class="sm_left fl">Phone</br></br></br>Mobile</div>
        </td>
        <td class="edit">
            <input type="text" name="phone" class="custom-border"
                                value="<?=(!empty($userData->personalInfo->phone)) ? $userData->personalInfo->phone : "";?>"
                                placeholder="---">
            </br></br>
            <input type="text" name="m_phone" class="custom-border"
                   value="<?=(!empty($userData->personalInfo->m_phone)) ? $userData->personalInfo->m_phone : "";?>"
                   placeholder="---">
        </td>
    </tr>
    <tr>
        <td>Passport S/N</td>
        <td class="edit"><input type="text" name="passport" class="custom-border"
                                value="<?=(!empty($userData->personalInfo->passport)) ? $userData->personalInfo->passport : "";?>"
                                placeholder="SSNNNNNN">
        </td>
        <td>
            <div class="sm_left fl">Physiques</div>
        </td>
        <td><div class="custom-select">
                <select class="selectpicker custom-border" name="physique" data-actions-box="true"
                        data-title="---">
                    <option value="">---</option>
                    <?php $physiquesArray = ArrayHelper::map($physiques, 'id', 'name');
                    foreach ($physiquesArray as $key => $name){ ?>
                        <option
                            value="<?=$key;?>" <?=($userData->personalInfo->physique == $key) ? "selected" : "";?>>
                            <?=$name?>
                        </option>
                    <?php }?>
                </select>
            </div></td>
    </tr>
</table>
<div class="text_areas margin-bottom">
    <div class="sm_left fl">About Me</div>
                    <textarea name="about_me" class="admin-custom-border fl"
                              rows="5"><?=$userData->personalInfo->about_me;?></textarea>
    <div class="clear"></div>
</div>
<div class="text_areas margin-bottom">
    <div class="sm_left fl">My hobbies</div>
                    <textarea name="hobbies" class="admin-custom-border fl"
                              rows="5"><?=$userData->personalInfo->hobbies;?></textarea>
    <div class="clear"></div>
</div>

<div class="text_areas margin-bottom">
    <div class="sm_left fl">Ideal relationships</div>
                    <textarea name="my_ideal" class="admin-custom-border fl"
                              rows="5"><?=$userData->personalInfo->my_ideal;?></textarea>
    <div class="clear"></div>
</div>
<div class="margin-top margin-bottom">
    <label>Active status:</label>
        <select class="girl" name="user_active_status" style="width: 200px" required>
            <option value="<?php if ($userData->active_status == ManagerController::STATUS_NO_ACTIVE || $userData->active_status == ManagerController::STATUS_ACTIVE) {echo ManagerController::STATUS_NO_ACTIVE; } ?>" <?=($userData->active_status == ManagerController::STATUS_NO_ACTIVE) ? 'selected' : '';?>>not active</option>
            <option value="<?php if ($userData->active_status == ManagerController::STATUS_ACTIVE || $userData->active_status == ManagerController::STATUS_NO_ACTIVE) {echo ManagerController::STATUS_ACTIVE; } ?>" <?=($userData->active_status == ManagerController::STATUS_ACTIVE) ? 'selected' : '';?>>active</option>
        </select>
</div>
<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) { ?>
    <div class="margin-top margin-bottom" <?=($userData->userType == ManagerController::USER_AGENCY) ? 'hidden' : "";?>>
        <label>Approve status:</label>
        <select name="approve_status" style="width: 184px" required>
            <?php if (!empty($approve_statuses)) {
                foreach ($approve_statuses as $approveStatus) {
                    ?>
                    <option
                        value="<?=$approveStatus->id?>" <?=($userData->approve_status == $approveStatus->id) ? 'selected' : '';?>><?=$approveStatus->description?></option>
                <?php }
            } ?>
        </select>
    </div>
<?php } ?>
<button class="save_button btn">save</button>