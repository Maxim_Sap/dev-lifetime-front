<?php 

use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;
$serverUrl = $this->context->serverUrl;

//var_dump($session['avatar']); die;
$avatar = (!empty($session['avatar'])) ? $serverUrl.'/'.$session['avatar'] : '/img/no_avatar_normal.jpg';
$pageName = '';
if (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'index') {
	$pageName = 'homePage';
} elseif (Yii::$app->controller->id == 'blog') {
	$pageName = 'blog';
} elseif (Yii::$app->controller->id == 'finance') {
	$pageName = 'finance';
}elseif (Yii::$app->controller->id == 'message') {
	$pageName = 'message';
} elseif (Yii::$app->controller->id == 'default' && 
		  (Yii::$app->controller->action->id == 'about' || Yii::$app->controller->action->id == 'history' ||
		   Yii::$app->controller->action->id == 'instructions' || Yii::$app->controller->action->id == 'users-rules' ||
		   Yii::$app->controller->action->id == 'contract' || Yii::$app->controller->action->id == 'license-agreement')) {
	$pageName = 'static';
} elseif (Yii::$app->controller->id == 'tasks') {
	$pageName = 'tasks';
} elseif (Yii::$app->controller->id == 'need-to-approve') {
	$pageName = 'need-to-approve';
} elseif (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'log') {
	$pageName = 'log';
} elseif (Yii::$app->controller->id == 'default' && (
		  Yii::$app->controller->action->id == 'gift-list' || Yii::$app->controller->action->id == 'edit-shop-item' || Yii::$app->controller->action->id == 'my-gift' || Yii::$app->controller->action->id == 'add-gift')) {
	$pageName = 'gift';
} elseif ((isset($_GET) && isset($_GET['user_type']) && $_GET['user_type'] == 3) || Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'my-agency') {
	$pageName = 'agency';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'penalty' || Yii::$app->controller->action->id == 'penalty-table')) {
	$pageName = 'penalty';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'activity')) {
	$pageName = 'activity';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'add-user' || Yii::$app->controller->action->id == 'girls' || Yii::$app->controller->action->id == 'my-man' || Yii::$app->controller->action->id == 'signalizator-men' || Yii::$app->controller->action->id == 'interpreters' || Yii::$app->controller->action->id == 'user')) {
	$pageName = 'users';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'add-user' || Yii::$app->controller->action->id == 'girls' || Yii::$app->controller->action->id == 'my-man' || Yii::$app->controller->action->id == 'signalizator-men' || Yii::$app->controller->action->id == 'interpreters' || Yii::$app->controller->action->id == 'user')) {
	$pageName = 'users';
} elseif (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'settings') {
	$pageName = 'settings';
} elseif (Yii::$app->controller->id == 'default' && (Yii::$app->controller->action->id == 'pricelist' || Yii::$app->controller->action->id == 'agency-pricelist')) {
	$pageName = 'pricelist';
}elseif (Yii::$app->controller->action->id == 'affiliates' || Yii::$app->controller->action->id == 'referral-stats') {
	$pageName = 'affiliates';
}

?>

<div class="tool-bar-content">
	<div class="tool-bar-header">
		<div class="greeting-block">
			<div class="greeting-avatar" style="background-image: url('<?= $avatar ?>')"></div>
			<span class="greeting-text">Hello, <span class="admin-name"><?= $session['userName'] ?></span></span>
			<span class="current-date"><span class="status"></span><span class="month"><?= date('M') ?></span> <span class="day"><?= date('d') ?></span>, <span class="year"><?= date('Y') ?></span> <span class="time"><?= date('h:i:s A') ?></span></span>
		</div>
	</div>

	<div class="tool-bar-body">
		<ul class="tools-nav">
			<li <?php if ($pageName == 'homePage') {echo 'class="active"';} ?>>
				<a href="/manager"><span class="icon-wrapper"><i class="fa fa-desktop" aria-hidden="true"></i></span>Рабочий стол</a>
			</li>		
			<?php if (isset($session['permissions']) && $session['permissions']['staticpage_access'] == ManagerController::ALLOW) { ?>
			<li <?php if ($pageName == 'static')  {echo 'class="active"';} ?>>
				<a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-file-text" aria-hidden="true"></i></span>Статические страницы</a>
				<ul class="sub-nav">
					<li><a href="/manager/about">О нас</a></li>
					<li><a href="/manager/history">История компании</a></li>
					<li><a href="/manager/instructions">Инструкции</a></li>
					<li><a href="/manager/rules">Правила для агенств</a></li>
					<li><a href="/manager/users-rules">Правила для пользователей</a></li>
					<li><a href="/manager/contract">Договор</a></li>
					<li><a href="/manager/license-agreement">Лицензионное соглашение</a></li>
					<li><a href="/manager/privacy-policy">Privacy policy</a></li>
				</ul>
			</li>
			<?php } ?>
			<?php if (isset($session['permissions']) && $session['permissions']['blog_access'] == ManagerController::ALLOW) { ?>
			<li <?php if ($pageName == 'blog') {echo 'class="active"';} ?>>
				<a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-files-o" aria-hidden="true"></i></span>Блог Love stories</a>
				<ul class="sub-nav">
					<li><a href="/manager/blog/">список статтей</a></li>
					<li><a href="/manager/blog/add">добавить новую</a></li>
				</ul>
			</li>
			<?php } ?>
			<?php if (isset($session['permissions']) && $session['permissions']['tasks_access'] == ManagerController::ALLOW) { ?>
			<li <?php if ($pageName == 'tasks') {echo 'class="active"';} ?>>
				<a href="/manager/tasks"><span class="icon-wrapper"><i class="fa fa-tasks" aria-hidden="true"></i></span>Задачи</a>
			</li>
			<?php } ?>
			<li <?php if ($pageName == 'message') {echo 'class="active"';} ?>>
				<a href="/manager/message/"><span class="icon-wrapper"><i class="fa fa-envelope" aria-hidden="true"></i></span>Административные сообщения</a>
			</li>			
			<li <?php if ($pageName == 'need-to-approve') {echo 'class="active"';} ?>>
				<a href="/manager/need-to-approve/"><span class="icon-wrapper"><i class="fa fa-upload" aria-hidden="true"></i></span>Ожидает подтверждения</a>
			</li>			
			<?php if (isset($session['permissions']) && $session['permissions']['gift_access'] == ManagerController::ALLOW) { ?>
			<li <?php if ($pageName == 'gift') {echo 'class="active"';} ?>>
				<a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-gift" aria-hidden="true"></i></span>Подарки</a>
				<ul class="sub-nav">
					<li><a href="/manager/add-gift">Добавить новый</a></li>
					<li><a href="/manager/gift-list">Список подарков</a></li>
					<li><a href="/manager/my-gift">Статусы купленных подарков</a></li>
				</ul>
			</li>
			<?php } ?>
			<?php if (isset($session['permissions']) && $session['permissions']['users_access'] == ManagerController::ALLOW) { ?>
			<li <?php if ($pageName == 'affiliates') {echo 'class="active"';} ?>>
				<a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-building-o" aria-hidden="true"></i></span>Аффилейты</a>
				<ul class="sub-nav">
					<li><a href="/manager/affiliates">Список Аффилейтов</a></li>
					<li><a href="/manager/add-user?user_type=5">Добавить нового</a></li>
					<li><a href="/manager/referral-stats">Статистика привлечения</a></li>
					<li><a href="/manager/finance/affiliates">Финансы</a></li>
				</ul>
			</li>
			<li <?php if ($pageName == 'agency') {echo 'class="active"';} ?>>
				<a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-building-o" aria-hidden="true"></i></span>Агенства</a>
				<ul class="sub-nav">
					<li><a href="/manager/my-agency">Список Агенств</a></li>
					<li><a href="/manager/add-user?user_type=3">Добавить агенство</a></li>
				</ul>
			</li>
			<li <?php if ($pageName == 'users')  {echo 'class="active"';} ?>>
				<a href="javascript:void(0);"><span class="icon-wrapper"><i class="fa fa-group" aria-hidden="true"></i></span>Пользователи</a>
				<ul class="sub-nav">
					<li><a href="/manager/add-user">Добавить нового</a></li>
					<li><a href="/manager/girls">Девочки</a></li>
					<li><a href="/manager/my-man">Мальчики</a></li>
					<li><a href="/manager/signalizator-men">Фейковые мальчики (сигнализатор чата)</a></li>
					<li><a href="/manager/interpreters">Переводчики</a></li>
				</ul>
			</li>
			<li <?php if ($pageName == 'subscribes')  {echo 'class="active"';} ?>>
				<a href="/manager/subscribes"><span class="icon-wrapper"><i class="fa fa-gears" aria-hidden="true"></i></span>Подписчики</a>
			</li>
			<li>
				<a href="/manager/girl-video"><span class="icon-wrapper"><i class="fa fa-video-camera" aria-hidden="true"></i></span>Просмотреть камеры девушек</a>	
			</li>	
			<?php } ?>		
			<li>
				<a href="/manager/logout"><span class="icon-wrapper"><i class="fa fa-sign-out" aria-hidden="true"></i></span>Выход</a>	
			</li>			
		</ul>
	</div>

	<div class="tool-bar-footer">
		<button class="collapse-bar"><i class="fa fa-angle-double-left" aria-hidden="true"></i>Collapse Menu</button>
	</div>
</div>
