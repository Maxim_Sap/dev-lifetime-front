<?php 
$server_url = $this->context->server_url;
$avater = !empty($one->thumb_normal) ? $server_url.'/'.$one->thumb_normal : '/img/no_avatar_normal.jpg';

?>
<div class="girl-tile" data-user-id="<?=$one->user_id;?>">
    <div class="girl-photo" style="background-image:url(<?=$avater;?>);"></div>
    <div class="girl-meta">
        <div class="meta-left">
            <div class="girl-name">Name: <?=trim($one->first_name. " ".$one->last_name);?></div>
            <div class="girl-id">ID: <?=$one->user_id;?></div>  
        </div>
        <div class="meta-right">
            <div class="controls">
                <a href="/manager/edit-user/<?=$one->user_id?>" title="edit">
					<i class="fa fa-pencil" aria-hidden="true"></i>
				</a>
				<?php if($one->active == 1){?>
					<a href="#" data-user-id="<?=$one->user_id?>" class="user-activate" title="deactivate">
						<i class="fa fa-toggle-on" aria-hidden="true"></i>
					</a>
				<?php }else{?>
					<a href="#" data-user-id="<?=$one->user_id?>" class="user-activate" title="activate">
						<i class="fa fa-toggle-off" aria-hidden="true"></i>
					</a>
				<?php }?>
				<a href="#" title="delete user" onclick="if(!confirm('Are you sure do you wont delete user?')){return false;}">
					<i class="fa fa-trash" aria-hidden="true"></i>
				</a>
            </div>
        </div>
    </div>
</div>