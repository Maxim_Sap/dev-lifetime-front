<?php 
use app\modules\manager\controllers\ManagerController;
use yii\helpers\Url;

$session = Yii::$app->session;

?>
<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN && !empty($agencyRuleViolations)) { ?>
<div class="rule-violations">
    <?php foreach ($agencyRuleViolations as $violation) { ?>
           <a href="<?= Url::to(['/manager/rule-violation/view', 'ruleViolationID' => $violation->id]); ?>"><i class="fa fa-star fa-6"></i></a>
<?php } ?>
</div>

<a class="btn btn-large" href="<?= Url::to(['/manager/rule-violation/create', 'agencyID' => $userData->agency_info->id]); ?>">Add rule violation</a>
<?php } ?>
<table class="margin-top margin-bottom">
<input type="hidden" name="sex" value="<?=(!empty($userData->personalInfo->sex)) ? $userData->personalInfo->sex : ManagerController::USER_FEMALE ?>" />    
    <tr>
        <td>User ID:</td>
        <td><span><?=$userData->userID;?></span></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Agency Name</td>
        <td class="edit">
            <input readonly="readonly" type="text" name="agency_name" class="custom-border"
                   value="<?= (!empty($userData->agency_info->name)) ? $userData->agency_info->name : '' ;?>" >
        </td>
        <td>Email</td>
        <td class="edit"><input readonly="readonly" type="email" name="email" class="custom-border"
                                value="<?=$userData->email;?>">
        </td>
    </tr>
    <tr>
        <?php /*?>
        <td>Pasport serial number</td>
        <td class="edit"><input type="text" name="passport_number" class="custom-border"
                                value="<?=(isset($userData->agency_info->passport_number) ? $userData->agency_info->passport_number : "");?>">
        </td><?php */?>
        <td>Agency director name</td>
        <td class="edit"><input readonly="readonly" type="text" name="contact_person" class="custom-border"
                                value="<?=(isset($userData->agency_info->contact_person) ? $userData->agency_info->contact_person : "");?>">
        </td>

        <td>Agency director address</td>
        <td class="edit"><input readonly="readonly" type="text" name="director_address" class="custom-border"
                                value="<?=(isset($userData->agency_info->director_address) ? $userData->agency_info->director_address : "");?>">
        </td>
    </tr>
    <tr>
        <td>Office address</td>
        <td class="edit"><input readonly="readonly" type="text" name="office_address" class="custom-border"
                                value="<?=(isset($userData->agency_info->office_address) ? $userData->agency_info->office_address : "");?>">
        </td>
        <td>Office phone</td>
        <td class="edit"><input type="text" name="office_phone" class="custom-border"
                                value="<?=(isset($userData->agency_info->office_phone) ? $userData->agency_info->office_phone : "");?>">
        </td>
    </tr>
    <tr>
        <td>Mobile phone</td>
        <td class="edit">
            <input type="text" name="m_phone" class="custom-border"
                   value="<?=(isset($userData->agency_info->m_phone) ? $userData->agency_info->m_phone : "");?>">
        </td>
        <td>Skype</td>
        <td class="edit">
            <input readonly="readonly" type="text" name="skype" class="custom-border"
                   value="<?=(isset($userData->agency_info->skype) ? $userData->agency_info->skype : "");?>">
        </td>
        <?php /*?>
        <td>Card number</td>
        <td class="edit"><input type="text" name="card_number" class="custom-border"
                                value="<?=(isset($userData->agency_info->card_number) ? $userData->agency_info->card_number : "");?>">
        </td>
        <td>Card user name</td>
        <td class="edit"><input type="text" name="card_user_name" class="custom-border"
                                value="<?=(isset($userData->agency_info->card_user_name) ? $userData->agency_info->card_user_name : "");?>">
        </td>
        <?php */?>
    </tr>
    <tr>
        <td>Country</td>
        <td>
            <div class="custom-select">
                <select disabled="disabled" class="selectpicker custom-border" name="country" data-live-search="true"
                        data-actions-box="true" data-size="8">
                    <option value="">---</option>
                    <?php if (!empty($countryList)) {
                        foreach ($countryList as $country) {
                            ?>
                            <option
                                value="<?=$country->id;?>" <?=(isset($userData->location->country_id) && $userData->location->country_id == $country->id) ? "selected" : "";?>><?=$country->name;?></option>
                        <?php }
                    } ?>
                </select>
            </div>
        </td>
        <td>Region</td>
        <td class="edit"><input readonly="readonly" type="text" name="region" class="custom-border"
                                value="<?=(isset($userData->city)) ? $userData->city : "Not defined";?>"
                                placeholder="<?=(isset($userData->city)) ? "" : "---";?>">
        </td>
    </tr>
</table>
<?php if ($session['user_type'] == ManagerController::USER_AGENCY || $session['user_type'] == ManagerController::USER_SUPERADMIN) {?>
<div>
    <textarea readonly="readonly" name="bank_detail" placeholder="Bank detain info"><?=(isset($userData->agency_info->bank_detail) ? $userData->agency_info->bank_detail : "");?></textarea>
</div>
<?php } ?>
<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) { ?>
    <div class="margin-top margin-bottom" style="width: 300px">
        <label>Active status:</label>
        <select disabled="disabled" name="user_active_status" required>
            <option value="<?=ManagerController::STATUS_NO_ACTIVE?>" <?=($userData->active_status == ManagerController::STATUS_NO_ACTIVE) ? 'selected' : '';?>>not active</option>
            <option value="<?php echo ManagerController::STATUS_ACTIVE; ?>" <?=($userData->active_status == ManagerController::STATUS_ACTIVE) ? 'selected' : '';?>>active</option>
        </select>
    </div>
    <div class="margin-top margin-bottom"  style="width: 300px">
        <label>User approve status:</label>
        <select disabled="disabled" name="approve_status" required>
            <?php if (!empty($approve_statuses)) {
                foreach ($approve_statuses as $approveStatus) {
                    ?>
                    <option
                        value="<?=$approveStatus->id?>" <?=($userData->approve_status == $approveStatus->id) ? 'selected' : '';?>><?=$approveStatus->description?></option>
                <?php }
            } ?>
        </select>
    </div>
    <?php if (!empty($userData->agency_info)) { ?>
    <div class="margin-top margin-bottom"  style="width: 300px">
        <label>Agency approve status:</label>
        <select disabled="disabled" name="agency_approve" required>
            <?php if (!empty($approve_statuses)) {
                foreach ($approve_statuses as $approveStatus) {
                    ?>
                    <option
                        value="<?=$approveStatus->id?>" <?=($userData->agency_info->approve_status == $approveStatus->id) ? 'selected' : '';?>><?=$approveStatus->description?></option>
                <?php }
            } ?>            
        </select>
    </div>
    <?php } ?>
    <button class="save_button btn">save</button>
<?php }else{ ?>
    <button class="save_button btn btn-large">save and send to approve</button>
    <?php
    $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved agency info</span>';
    if(isset($userData->agency_info->approve_status)){
        switch ($userData->agency_info->approve_status){
            case 1:
                $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved agency info</span>';
                break;
            case 2:
                $approve_status = '<span style="color: lawngreen; margin-left: 10px;font-weight: bold;">Approve in progress</span>';
                break;
            case 3:
                $approve_status = '<span style="color: #ffcc00; margin-left: 10px;font-weight: bold;">Approve delcine</span>';
                break;
            case 4:
                $approve_status = '<span style="color: #ffcc00; margin-left: 10px;font-weight: bold;">Approved</span>';
                break;
            default:
                $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved agency info</span>';
                break;
        }
    }
    echo $approve_status; ?>
<?php } ?>

<style>
    #content-area .btn-large {
        max-width: 250px;
    }
    .fa-6 {
        font-size: 3em;
        color: red;
    }
    .rule-violations    {
        margin: 20px 0;
    }
    .edit-profile input, .edit-profile select {
        width: 85%;
    }
    .edit-profile textarea {
        width: 95%;
    }
    textarea + .icon-element, textarea + .icon-element + .icon-element {
        position: relative;
        top: -80px;
    }
    .icon-element {
        background: red;
        padding: 10px;
        margin-left: 5px;
        border-radius: 5px;
    }
    .icon-button {
        color: white;
    }
</style>

<?php 
$this->registerJsFile(
    '@web/js/admin/editable.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>