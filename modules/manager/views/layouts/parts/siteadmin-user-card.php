<?php 
use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;

?>
<table class="margin-top margin-bottom">
    <tr>
        <td>User ID:</td>
        <td><span><?=$userData->personalInfo->id;?></span></td>
        <td>Your email</td>
        <td class="edit"><input type="email" name="email" class="custom-border"
                                value="<?=$userData->email;?>"></td>
    </tr>
    <tr>
        <td>First Name</td>
        <td class="edit"><input type="text" name="first_name" class="custom-border"
                               value="<?=$userData->personalInfo->first_name;?>" required></td>
        <td>Last Name</td>
        <td class="edit"><input type="text" name="last_name" class="custom-border"
                                value="<?=(!empty($userData->personalInfo->last_name)) ? $userData->personalInfo->last_name : "";?>"
                                placeholder="---"></td>
    </tr>
    <tr>
        <td>Sex</td>  
        <td>
            <select name="sex" required>
                <option value="<?= ManagerController::USER_MALE ?>" <?=($userData->personalInfo->sex == ManagerController::USER_MALE) ? 'selected' : '';?>>Male</option>
                <option value="<?= ManagerController::USER_FEMALE ?>" <?=($userData->personalInfo->sex == ManagerController::USER_FEMALE) ? 'selected' : '';?>>Female</option>
            </select>
        </td>
        <?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN || $session['user_type'] == ManagerController::USER_AGENCY) {?>
        <td>Admin Permissions</td>     
        <td>
            <?php $i=0; foreach ($permissions as $key => $permission) { ?>
                <input type="hidden" name="<?= $key ?>" value="0" />
                <input id="permission-<?=$i ?>" type="checkbox" name="<?= $key ?>" <?php if ($permission['value']) {echo "checked"; } ?> value="1"/>
                <label for="permission-<?=$i ?>"><?= $permission['description'] ?></label><br>
            <?php $i++; } ?>
        </td>
        <?php } ?>
    </tr>
</table>
<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) { ?>
<div class="margin-top margin-bottom">
    <label>Active status:</label>
    <select name="user_active_status" required>
        <option value="<?= ManagerController::STATUS_NO_ACTIVE ?>" <?=($userData->active_status == ManagerController::STATUS_NO_ACTIVE) ? 'selected' : '';?>>not active</option>
        <option value="<?= ManagerController::STATUS_ACTIVE ?>" <?=($userData->active_status == ManagerController::STATUS_ACTIVE) ? 'selected' : '';?>>active</option>
    </select>
</div>
<?php } else {?>
<input type="hidden" name="user_active_status" value="<?= ManagerController::STATUS_ACTIVE ?>">
<?php } ?>
<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) { ?>
    <div class="margin-top margin-bottom">
        <label>User approve status:</label>
        <select name="approve_status" required>
            <?php if (!empty($approve_statuses)) {
                foreach ($approve_statuses as $approveStatus) {
                    ?>
                    <option
                        value="<?=$approveStatus->id?>" <?=($userData->approve_status == $approveStatus->id) ? 'selected' : '';?>><?=$approveStatus->description?>
                    </option>
                <?php }
            } ?>
        </select>
    </div>
<?php } ?>
<button class="save_button btn">save</button>
