<?php 
	$server_url = $this->context->server_url;
	$date_create = strtotime($womans->date_create);
	//если пользоватлеь добавлен мение месяца назад, то ему добавляется значек NEW
	$avater = !empty($womans->thumb_normal) ? $server_url.'/'.$womans->thumb_normal : '/img/no_avatar_normal.jpg';
?>
<div class="girl-tile" data-user-id="<?=$womans->user_id;?>">
	<a href="/manager/edit-user/<?=$womans->user_id;?>" class="girl-link">
		<div class="girl-photo" style="background-image:url(<?=$avater;?>);"></div>
		<?php if($womans->approve_status_id == 4){?>
			<div class="approved-status yes"><span>approved</span></div>
		<?php }elseif($womans->approve_status_id == 1 || $womans->approve_status_id == 2){?>
			<div class="approved-status no"><span>not approved</span></div>
		<?php }elseif($womans->approve_status_id == 3){?>
			<div class="approved-status decline"><span>decline</span></div>
		<?php } ?>
	</a>   
	<div class="girl-meta">
		<div class="meta-left">
			<div class="girl-name"><?=$womans->first_name;?></div>
			<div class="girl-id">ID - <?=$womans->user_id;?></div>
		</div>
		<div class="meta-right">
			<div class="controls">
				<a href="/manager/activity/<?=$womans->user_id;?>" title="activity">
					<i class="fa fa-clock-o" aria-hidden="true"></i>
				</a>
				<a href="/manager/edit-user/<?=$womans->user_id;?>" title="edit">
					<i class="fa fa-pencil" aria-hidden="true"></i>
				</a>
				<a href="#" data-user-id="<?=$womans->user_id;?>" class="user-approve-status" title="<?=($womans->approve_status_id == 2) ? 'withdraw' : 'send to approve'?>">
					<i class="fa <?=($womans->approve_status_id != 2) ? 'fa-hand-o-right' : 'fa-hand-o-left'?>" aria-hidden="true"></i>
				</a>
				<a href="#" data-user-id="<?=$womans->user_id;?>" class="user-activate" title="<?=($womans->active == 1) ? 'deactivate' : 'activate'?>">
					<i class="fa <?=($womans->active == 1) ? 'fa-toggle-on' : 'fa-toggle-off'?>" aria-hidden="true"></i>
				</a>
				<a href="/manager/delete-user/<?=$womans->user_id;?>"title="delete user" onclick="if(!confirm('Are you sure do you wont delete user?')){return false;}">
					<i class="fa fa-trash" aria-hidden="true"></i>
				</a>
			</div>
		</div>
	</div>

</div>