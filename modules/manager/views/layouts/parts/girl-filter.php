<?php //var_dump($sortBy); die; ?>
<h2>Фильтр:</h2>
<form id="girl-filter" method="post">
	<div style="width: 470px; display: inline-flex;"><table style="width: 400px">
                <tr>
                    <td><label>С:</label></td>
                    <td><input type="date" name="dateFrom" value="<?=$dateFrom;?>"></td>
                </tr>
                <tr>
                    <td><label>по:</label></td>
                    <td><input type="date" name="dateTo" value="<?=$dateTo;?>"></td>
                </tr>
                <tr>
                    <td><label>Сортировать по:</label></td>
                    <td>       
                        <select name="sortBy" id="by">
                            <?php foreach (['status', 'name', 'age', 'id'] as $by) { ?>
                                <option value="<?= $by ?>" <?php if ($sortBy == $by) {echo 'selected="selected"'; } ?>><?= $by ?></option>
                            <?php } ?>                    
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label>порядок:</label></td>
                    <td>                
                        <?php if ($sortBy == 'status') { ?>
                        <select name="sortWay" id="way">
                            <?php foreach (['сначала активные' => 'down', 'сначала неактивные' => 'up'] as $key => $way) { ?>        
                            <option value="<?= $way ?>" <?php if ($sortWay == $way) {echo 'selected="selected"'; } ?>><?= $key ?></option>                    
                            <?php } ?>
                        </select>
                        <?php } else { ?>
                        <select name="sortWay" id="way">         
                            <?php foreach (['возрастание' => 'up', 'убывание' => 'down'] as $key => $way) { ?>        
                            <option value="<?= $way ?>" <?php if ($sortWay == $way) {echo 'selected="selected"'; } ?>><?= $key ?></option>                    
                            <?php } ?>
                        </select>
                       <?php } ?>
                    </td>
                </tr>        
            </table>
    </div>   
    <div style="width: 400px; display: inline-flex;"> 
    <table style="width: 400px">
        <tr>
            <td><label>Искать по id:</label></td>
            <td><input type="text" name="userId" value="<?=$userId;?>"></td>
        </tr>    
        <tr>
            <td><label>Искать по имени:</label></td>
            <td><input type="text" name="userName" value="<?=$userName;?>"></td>
        </tr>
        <tr>
            <td><label>Искать по возрасту:</label></td>
            <td>  <p>От:</p>  
            <select name="userAgeFrom" id="">
                <?php for ($i=18; $i <= 65; $i++) { ?>                    
                    <option <?php if ($i == $userAgeFrom) {echo 'selected="selected"';} ?> value="<?= $i ?>"><?= $i ?></option>
                <?php } ?>
            </select>
            <p>До:</p>
            <select name="userAgeTo" id="">
                <?php for ($i=18; $i <= 65; $i++) { ?>                    
                    <option <?php if ($i == $userAgeTo) {echo 'selected="selected"';} ?> value="<?= $i ?>"><?= $i ?></option>
                <?php } ?>
            </select>
            </td>
        </tr>
        <tr>
            <td><label>Искать по городу:</label></td>
            <td><input type="text" name="userCity" value="<?=$userCity;?>"></td>
        </tr>
        <tr>
            <td><label>Искать по стране:</label></td>
            <td><input type="text" name="userCountry" value="<?=$userCountry;?>"></td>
        </tr>                
    </table>
    </div>
    <div>  
        <div class="box">
            <label for="r1">All (<?= $active + $notActive ?>):</label>
            <input type="radio" name="activityStatus" value="all" id="r1" <?php if ($activityStatus == 'all') {echo "checked='checked'";} ?> />
        </div>
        <div class="box">
            <label for="r2">Active (<?= $active?>):</label>
            <input type="radio" name="activityStatus" value="active" id="r2" <?php if ($activityStatus == 'active') {echo "checked='checked'";} ?> />
        </div>
        <div class="box">
            <label for="r3">Not Active (<?= $notActive?>):</label>
            <input type="radio" name="activityStatus" value="notActive" id="r3" <?php if ($activityStatus == 'notActive') {echo "checked='checked'";} ?> />
        </div>
        <div class="box">
            <label for="r4">Online (<?= $online?>):</label>
            <input type="radio" name="activityStatus" value="online" id="r4" <?php if ($activityStatus == 'online') {echo "checked='checked'";} ?> />
        </div>
    </div>    
    <br>    
	<input type="submit" value="применить" class="btn admin-custom-btn margin-top margin-bottom">
</form>

<?php
$this->registerJs(
    "$('body').on('change', '#by', function() { if ($(this).val() != 'status') { $('#way').html(\"<option value='up'>возрастание</option><option value='down'>убывание</option>\"); } else { $('#way').html(\"<option value='down'>сначала активные</option><option value='up'>сначала неактивные</option>\"); } }); 
    $('body').on('click', 'input[name=\"activityStatus\"]', function(){
        $(\"form #agencyID\").remove();
        $(\"#girl-filter\").append('<input type=\"hidden\" name=\"agencyID\" id=\"agencyID\" value=\"' + $('#agency-select1').val() + '\">');
        $(\"#girl-filter\").submit();
    });

    "
);