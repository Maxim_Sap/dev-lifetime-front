<?php	
	$itemsOnPage = (Yii::$app->params['numberOfItemsOnAdminPage'] > $count) ? $count : Yii::$app->params['numberOfItemsOnAdminPage'];
	$total = (($count - 1) / $itemsOnPage) + 1;
	$total =  intval($total);
	$pagePrev = $page - 1;
	$pageNext = $page + 1;
?>

<div class="page-nav-wrapper">    
    <ul class="page-navigation list-inline">
        <li class="prev"><a href="javascript:void(0);" <?php if($pagePrev <= 0 || $pagePrev >$total){ echo "disabled";}?>" <?php if($pagePrev > 0 && $pagePrev <= $total){echo "onclick=\"window['adminAccountLibrary']['userCustomPagination']($pagePrev,'$pageName','');\"";}?>><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>        
        <li class="next"><a href="javascript:void(0);" <?php if($pageNext <= 0 || $pageNext >$total){ echo "disabled";}?>" <?php if($pageNext > 0 && $pageNext <= $total){echo "onclick=\"window['adminAccountLibrary']['userCustomPagination']($pageNext,'$pageName','');\"";}?>><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
    </ul>
    <span id="page" data-page="<?=$page;?>" data-page-name="<?=$pageName;?>" class="pages_counter margin-right fr">(<?=$count?> items) <?=" Pages ".$page." of ".$total?></span> 
</div>

