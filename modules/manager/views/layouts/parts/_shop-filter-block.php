<ul class="gifts-categories gifts_categories list-inline">
	<li data-value="all" class="<?=(empty($giftsCategories)) ? 'active':'';?>"><span>All gifts</span></li>
	<?php foreach ($allGiftsCategories as $category) { ?>
	<li data-value="<?= $category['id'] ?>" class="<?=(in_array($category['id'], $giftsCategories)) ? 'active':'';?>"><span><?= $category['title'] ?></span></li>
	<?php } ?>
</ul>