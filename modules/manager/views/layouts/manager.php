<?php

	/* @var $this \yii\web\View */
	/* @var $content string */

	use yii\helpers\Html;
	use yii\bootstrap\Nav;
	use yii\bootstrap\NavBar;
	use yii\widgets\Breadcrumbs;
	use app\assets\AdminAsset;
	use app\modules\manager\controllers\ManagerController;
	AdminAsset::register($this);
	$session = Yii::$app->session;
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta charset="<?=Yii::$app->charset?>">
		<?=Html::csrfMetaTags()?>
		<title><?=Html::encode($this->title)?></title>
		<meta name="userID" content="<?=$session['user_id'];?>">
		<meta name="userToken" content="<?=$session['user_token'];?>">
		<?php $this->head() ?>
	</head>
	<body class="admin-body"><!-- test -->
			<?php $this->beginBody(); ?>
			<?php echo $this->render('parts/header.php');	?>
			<div class="system-global-wrapper">
				<?php echo $this->render('parts/left-side-bar.php');	?>
				<?php echo $content; ?>
			</div>
			<!-- Modal MESSAGE-->
			<div id="layout"></div>
			<div id="modalMessage">
				<div class="modal-body">
					<span></span>
				</div>
			</div>
			<div id="modal-detail-message">
				<div class="modal-info">
					<div class="message-d_title margin-bottom">Message ID: <span class="message-id" ></span></div>
					<div>Message from: <span class="message-from" ></span></div>
					<div>Message to: <span class="message-to grey_text" ></span> </div>
					<div>Date read:<span class="data-read grey_text"></span></div>
					<div>Date create: <span class="data-create grey_text"></span></div>
					<div>Answered:<span class="answered grey_text"></span></div>
					<div class="cap_t">Caption: <span class="caption"></span></div>
					<div class="text_msg margin-bottom margin-top admin-custom-border">

						<div class="modal-body"></div>
					</div>
				</div>
			</div>
			<?php
			if(isset($session['message'])){
				echo "<script type='text/javascript'>
					$('#modalMessage,#layout').show();
					$('#modalMessage .modal-body span').text('".$session['message']."');
					</script>";
				unset($session['message']);
			}
			?>
			<?php $this->endBody(); ?>
			<?php if (in_array($session['user_type'], [ManagerController::USER_AGENCY, ManagerController::USER_ADMIN])) { ?>
				<script type='text/javascript'>
					$(document).ready(function(){
						$.ajax({
							url: window["commonAdminLibrary"]["apiServer"]+'/v1/admin/agency-notifications',
							dataType: "json",
							type: "GET",
					        headers: {
					            "Authorization": "Bearer " + window["commonAdminLibrary"]["userToken"]
					        },
							success: function (response) {
								if (response.success == true) {									
									if (response['unrepliedLetters'] > 0) {										
										$('.tools-nav .letters a').append('<span class="icon-wrapper notification">'+ response['unrepliedLetters'] +'</span>');
									}
									if (response['unfinishedTasks'] > 0) {										
										$('.tools-nav .tasks a').append('<span class="icon-wrapper notification">'+ response['unfinishedTasks'] +'</span>');
									}
									if (response['unsendGifts'] > 0) {										
										$('.tools-nav .gift a').append('<span class="icon-wrapper notification">'+ response['unsendGifts'] +'</span>');
									}
									if (response['unrepliedMessages'] > 0) {										
										$('.tools-nav .message a').append('<span class="icon-wrapper notification">'+ response['unrepliedMessages'] +'</span>');
									}									
									if (response['notApprovedChatTemplates'] > 0) {										
										$('.tools-nav .chat-mailing a').append('<span class="icon-wrapper notification">'+ response['notApprovedChatTemplates'] +'</span>');
									}																						
									if (response['notApprovedMessageTemplates'] > 0) {										
										$('.tools-nav .message-mailing a').append('<span class="icon-wrapper notification">'+ response['notApprovedMessageTemplates'] +'</span>');
									}							
								} 
							},
							error: function (xhr, ajaxOptions, thrownError) {
								
							}
						});
					});
				</script>
			<?php } ?>
	</body>
</html>
<?php $this->endPage() ?>