<?php
$serverUrl = $this->context->serverUrl;
if(isset($userList) && !empty($userList)){
    foreach($userList as $user){
        $user = (object)$user;
        $avatar = !empty($user->small_thumb) ? $serverUrl.'/'.$user->small_thumb : '/img/no_avatar_small.jpg';
        ?>
        <div class="user-tile" data-user-id="<?=$user->id?>">
            <div class="avatar" data-src="<?=$avatar;?>" style="background-image:url(<?=$avatar;?>);"></div>
            <div data-status="<?= $user->online; ?>" class="online-status"><?=($user->online) ? '<span class="green">online</span>' : '<span class="red">offline</span>';?></div>
            <div class="name"><?=trim($user->first_name);?></div>
            <div class="user-id">[ID: <?=$user->id;?>]</div>
        </div>
    <?php }
}else{?>
    <p>No users</p>
<?php } ?>