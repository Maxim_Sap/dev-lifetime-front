<?php
$serverUrl = $this->context->serverUrl;

?>
<div id="content-area">
    <audio class="audio_new_message hide" controls>
        <source src="/sounds/new_message.mp3" type="audio/mpeg">
    </audio>
    <audio class="audio_new_mail hide" controls>
        <source src="/sounds/new_letter.mp3" type="audio/mpeg">
    </audio>
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Мульти чат</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<?php if(!empty($girls)){?>
				<div class="chat-name-block">
					<?php foreach ($girls as $girl){?>
						<div class="tab user-name" data-user-id="<?=$girl->id?>">
							<?=trim($girl->first_name." ".$girl->last_name);?>
							<span class="new-message-marker blink"></span>
						</div>
					<?php }?>
				</div>
				<div class="clear"></div>
				<div class="chat-content-block row">
					<?php foreach ($girls as $girl){
						$avatar = !empty($girl->small_thumb) ? $serverUrl.'/'.$girl->small_thumb : '/img/no_avatar_normal.jpg';?>
						<div class="tab user-content col-md-12" data-user-id="<?=$girl->id?>">
							<div class="label">выбранная девочка</div>
							<div class="set-status">
								<table>
									<tr>
										<td rowspan="3"><img class="avatar" src="<?=$avatar?>"></td>
										<td><?=$girl->birthday?></td>
									</tr>
									<tr>
										<td>
											<input type="checkbox" id="status_<?=$girl->id?>" data-status="<?=($girl->last_activity >= time()-30) ? 1: 0;?>" class="online-status" <?php if($girl->last_activity >= time()-30){echo "checked";}?>>
											<label for="status_<?=$girl->id?>">online</label>
										</td>
									</tr>
									<tr>
										<td>
											<button class="btn">установить</button>
										</td>
									</tr>
								</table>
							</div>
						</div>
					<?php }?>
					<div class="men-block-wrapper fl-l">
						<div class="label">Список мужчин <button class="btn" onclick="window['messageAdminLibrary']['getMenList']();">обновить</button></div>
						<div class="men-online fl-l"></div>
					</div>
					<div class="clear"></div>
					<div class="chat-letters-content">
						<div class="chat-list-block">
							<h3>Чат</h3>
							<div class="new-chats">
								<?php foreach ($girls as $girl){?>
									<div class="new-chat-section row" data-user-id="<?=$girl->id;?>"></div>
								<?php } ?>
							</div>
							<input type="hidden" value="" class="chat-other-user-id">
							<input type="hidden" value="" class="current-chat-other-user-id">
							<input type="hidden" value="" class="current-chat-other-user-avatar">
							<input type="hidden" value="" class="chat-other-user-status">
							<input type="hidden" value="" id="last-chat-message-id">
							<button class="btn" id="load-chat">загрузить историю чата</button>
							<div class="chat-wrapper">
		                        <div class="chat-box">
		                            <div ss-container class="chat-box-messages-wrapper">                                
		                            </div>                 
		                            <div class="print-message-container">
		                                <form class="print-message-form" method="post" enctype="multipart/form-data">               
		                                    <div class="chat-message-wrapper">
		                                        <textarea name="chat-message" id="chat-message" rows="10" cols="30" placeholder="Type your message here"></textarea>
		                                    </div>
		                                    <ul class="print-message-form-action-list list-inline">
		                                        <li>
		                                            <div class="file-type-wrapper">
		                                                <button class="btn btn-reset btn-file">Attach image</button>
		                                                <input type="file" name="image" id="attach-image" style="width: 100%">
		                                            </div>
		                                        </li>
		                                        <li>
		                                            <button class="btn" type="submit" class="btn">Send Message</button>
		                                        </li>
		                                    </ul>
		                                </form>
		                            </div>
		                        </div>
							</div>
						</div>
						<div class="message-section">
							<h3 class="pos-rel">Письма
								<?php foreach ($girls as $girl){?>
									<span class="marker blink" data-user-id="<?=$girl->id;?>"></span>
								<?php } ?>
							</h3>
							<button class="btn" id="load-letters">загрузить/обновить историю переписки</button>
							<div class="message-block">
								<div class="letters-labels">
									<div data-source="new-letters">написать новое</div>
									<div data-source="inbox-letters">входящие</div>
									<div data-source="outbox-letters">исходящие</div>
								</div>
								<div class="clear"></div>
								<div class="letters-content">
									<div class="tab new-letters">
										<form action="#" method="post" class="new-letter-form">
											<div><label>Тема:<input type="text" value="" placeholder="Letters theme" class="custom-border letters_caption" required/></label></div>
											<div><label>Кому ID:<input type="text" value="" placeholder="Man ID" class="custom-border other_user_id" required/></label></div>
											<div><p><strong>Имя получателя:</strong></p> <span class="other_user_name"></span></div>
											<p>Текст сообщения:</p>
											<textarea></textarea>
											<input type="hidden" value="" class="previous_letters_id" />
											<input type="hidden" value="" class="theme_id" />
											<button class="submit btn">send</button>
										</form>
									</div>
									<div class="tab inbox-letters"></div>
									<div class="tab outbox-letters"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php }else{?>
				<p>No users</p>
			<?php }?>
			</div>
		</div>
	</div>
</div>

<?php
$js = '$(".chat-name-block>div").eq(0).click();
setInterval(function(){
$.get("/manager/chat/set-women-online");
},60000);
setInterval(function(){
   window["adminAccountLibrary"]["getMultiChatNewMessageCount"]();
},5000);
window["messageAdminLibrary"]["getMenList"]();
setInterval(function(){
   window["messageAdminLibrary"]["getMenList"]();
},60000);
if($(".letters-content textarea").length > 0){
		tinymce.init({
			selector: ".letters-content textarea",
			plugins: "paste textcolor emoticons",
			paste_as_text: true,
			skin: "custom",
			height: "300",
			menubar: false,
			toolbar: "bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | forecolor backcolor | emoticons | undo redo | fullscreen",
			statusbar: true,
			language: "en",
		});
	}
';
$this->registerJs($js, Yii\web\View::POS_READY);
?>