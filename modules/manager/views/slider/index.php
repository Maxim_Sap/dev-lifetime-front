<?php 
$session = Yii::$app->session;

?>
<style>
	.drag-and-drop-area {
	    font-size: 18px;
	    border: 2px dashed rgba(235, 65, 97, 0.7);
	    text-align: center;
	    padding: 20px 0;
	    line-height: 1.4;
	    position: relative;
	    margin-bottom: 40px;
	}
	.card-box {
		max-width: inherit;
		margin: 0px;
		padding: 0px;
		border: none;
		min-height: 300px;
	}
	.card-box img {
		height: 250px;
	}
	.foto_foot {
		width: 250px;
		margin: 0 AUTO;
	}
	.drop_here {
		height: 183px;
	    position: absolute;
	    top: 0px;
	    width: 100%;
	    left: 0;
	}
</style>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2>Слайдер сайта</h2>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<input id="otherUserID" type="hidden" value="<?=$session['user_id'];?>">

        <?php if(!in_array($albumInfo->title, ['default','document', 'slider'])){?>
			<div class="row">
				<div class="col-xs-12 col-md-6">		        
				<form class="album-edit form-horizontal">
		        	<div class="form-group" style="display: none">
	            		<label for="album_title">Album title</label>
	            		<input type="text" id="album_title" class="form-control" value="<?=$albumInfo->title;?>">
	        		</div>
	            	<div class="form-group" style="display: none">
	            		<label for="album-description">Album Description</label>
	            		<textarea name="description" class="form-control" id="album-description" cols="30" rows="10"><?=$albumInfo->description;?></textarea>
	            	</div>
	            	<input type="hidden" id="album_id" value="<?=$albumInfo->id;?>">
	            	<div class="checkbox">
	            		<input type="checkbox" id="album_status" <?php if($albumInfo->public == '1'){echo 'checked';}?>>
						<label for="album_status">Public</label>
	            	</div>
	            	<div class="checkbox">
	            		<input type="checkbox" id="album_active" <?php if($albumInfo->active == '1'){echo 'checked';}?>>
						<label for="album_active">Active</label>
	            	</div>
	            	<input type="submit" class="btn btn-default" value="save">
		        </form>
		        </div>
		     </div>
	        <?php } ?>
	        <div class="drag_and_drop_area drag-and-drop-area pos_rel inner-padding">
		        <a href="javascript:void(0);" class="main-col-text">Click here or drag&amp;drop photos to upload them to your album</a>
		        
		        <div class="cab_main_photo" id="div_foto_add">
					<span for="choose" id="link" class="choose"></span>
					<form id="chooseform" method="post" enctype="multipart/form-data">
						<input type="hidden" name="MAX_FILE_SIZE" value="12345"/>
						<input type="file" name="file" multiple accept="image/*" id="choose" data-album-title="<?=$albumInfo->title;?>" data-place="albums" style="display: none;" />
					</form>
				</div>
		        <div>Select a file in this format: gif, jpg, jpeg, png</div>
		        <div>Should not exceed 2 Mb!</div>
		        <span class="drop_here pos_abs" data-album-title="<?=$albumInfo->title;?>" data-place="albums"></span>
		    </div>                            
	        <div class="row">
	    			<?php if(!empty($sliders)){
						$i=1;
						foreach($sliders as $photo){
							$img = empty($photo->medium_thumb) ? '/img/no_image.jpg' : $this->context->serverUrl."/".$photo->medium_thumb;
							?>
							<div class="col-md-3 col-xs-12 card-box">
								<?php if($photo->approve_status == 0){?>
									<div class="card-box-status no"><span>not approved</span></div>
								<?php }elseif($photo->approve_status == 1){?>
									<div class="card-box-status no"><span>in progress</span></div>
								<?php }elseif($photo->approve_status == 2){?>
									<div class="card-box-status decline"><span>decline</span></div>
								<?php } elseif($photo->approve_status == 4){?>
									<div class="card-box-status yes"><span>approved</span></div>
								<?php }?>

								<a href="/manager/photo/<?=$photo->id;?>/<?=$session['user_id'];?>">
						            <img src="<?=$img;?>">
						        </a>
						        <div class="foto_foot">
						            <a href="/manager/photo/<?=$photo->id;?>/<?=$session['user_id'];?>" class="album_name main-col-text"><?=$photo->title?></a>
						        </div>
						    </div>
							<?php if($i%3 == 0){?>
							<?php }?>
							<?php
							$i++;
						}
		            }else{?>
						<p>No sliders</p>
		            <?php } ?>
				</div>  
			</div>
		</div>
	</div>
</div>
