

<?php

$serverUrl = $this->context->serverUrl;

?>

<div id="content-area">
    <div class="content-area-inner">
        
        <div class="content-area-inner-header">
            <h2 class="content-title">Articles</h2>
            <a href="/manager/blog/add" class="btn">Add New</a>
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <?php if(!empty($post_list)){?>
                    <table class="articles-table">
                        <thead>
                        <th>ID</th>
                        <th>Active</th>
                        <th>Category</th>
                        <th style="width: 330px;">image</th>
                        <th style="width: 175px;">title</th>
                        <th>description</th>
                        <th>action</th>
                        </thead>
                        <?php foreach ($post_list as $one){
                            $thumb = !empty($one->image) ? $serverUrl.'/'.$one->image : '/img/no_avatar_normal.jpg';
                            ?>
                            <tr>
                                <td><?=$one->id?></td>
                                <td><?=($one->status == 1) ? "Active" : "No Active";?></td>
                                <td><?=$one->category?></td>
                                <td class="img" style="background-image:url(<?=$thumb?>);height: 130px;"></td>
                                <td class="title"><?=$one->title;?></td>
                                <td class="desc"><?=mb_substr($one->description,0,400);?></td>
                                <td>
                                    <a href="/manager/blog/edit/<?=$one->id?>"><i class="fa fa-pencil" style="margin-right: 13px;" aria-hidden="true"></i></a>
                                    <a href="/manager/blog/delete/<?=$one->id?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        <?php }?>
                    </table>
                <?php }else{?>
                    <p>No result</p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>