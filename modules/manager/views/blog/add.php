<?php

use mihaildev\ckeditor\CKEditor;

use mihaildev\elfinder\ElFinder;

$serverUrl = $this->context->serverUrl;
?>
<div id="content-area">
    <div class="content-area-inner">
        
        <div class="content-area-inner-header">
            <h2 class="content-title"><?=(isset($PostID) ? 'Редактировать статью:' : 'Добавить статью:');?></h2>   
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <form class="add-post" action="" method="post">
                    <table class="add-post">
                        <tr>
                            <td colspan="3">
                                <div class="box">
                                    <label for="status">Active:</label>
                                    <input id="status" type="checkbox" <?=(isset($post_data->status) && $post_data->status == 1 ? 'checked' : '');?> >
                                </div>
                                <div class="box">
                                    <label for="category_story">Story:</label>
                                    <input id="category_story" type="radio" name="category" value="story" <?=(isset($post_data->category) && $post_data->category == 'story' ? 'checked' : '');?> required>
                                    <label for="category_post">Post:</label>
                                    <input id="category_post" type="radio" name="category" value="post" <?=(isset($post_data->category) && $post_data->category == 'post' ? 'checked' : '');?> required>
                                </div>
                                <div class="box">
                                    <label for="on_main">On main page:</label>
                                    <input id="on_main" type="checkbox" <?=(isset($post_data->on_main) && $post_data->on_main == 1 ? 'checked' : '');?> >
                                </div>
                                <div class="box">
                                    <label for="on_main">Sort order:</label>
                                    <input id="sort_order" type="number" value="<?=(isset($post_data->sort_order) ? $post_data->sort_order : 0);?>" >
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Image:</td>
                            <td>
                                <div class="prev-img" <?php if(isset($post_data->image)){echo 'style="background-image: url('.$serverUrl.'/'.$post_data->image.')"';}?> id="add_post_image">+</div>
                                <input type="hidden" id="post-image" value="<?php if(isset($post_data->image)){echo $post_data->image;}?>">
                                <form id="post-image-form" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="MAX_FILE_SIZE" value="12345"/>
                                    <input type="file" name="file" multiple accept="image/*" id="post_image_change" data-post-id=""
                                           style="display: none;"/>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td>Title:</td>
                            <td><input id="post-title" type="text" value="<?=(isset($post_data->title) ? $post_data->title : '');?>" required></td>
                        </tr>
                        <tr>
                            <td>Description:</td>
                            <td>
                                <?php
                                echo CKEditor::widget([
                                    'name' => 'post-desc',
                                    'value'=>(isset($post_data->description) ? $post_data->description : ''),
                                    'id' => 'post-desc',
                                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                                        /* Some CKEditor Options */
                                        'preset' => 'basic',
                                        'inline' => FALSE,
                                        'height' => 500,
                                    ]),
                                ]);?>
                            </td>
                        </tr>
                    </table>
                </form>
                <input id="post-id" type="hidden" value="<?php if(isset($post_data->id)){echo $post_data->id;}?>">
                <button id="add-post" class="btn margin-top pull-right"><?=(isset($PostID) ? 'обновить' : 'добавить');?></button>
            </div>
        </div>
    </div>
</div>
