<?php
use yii\helpers\Url;

?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">
				Add chat mailing template
			</h2>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<section class="profile-container admin-content">				                                      
				<div class="profile_information_block">
					<form>
						<label for="template-title">Template title</label>
						<input type="text" id="template-title" name="title">
						<label for="template-body">Template body</label>                          
						<div class="textarea">
							<textarea name="body" id="template-body" cols="30" rows="10"></textarea>                       	
                        </div>
						<button class="btn pull-right create-template">Create</button>
					</form>
				</div> 
				</section>
			</div>
		</div>
	</div>
</div>

<style>
	.textarea {
		margin: 20px 0;
	}
</style>

<?php 

$js = "
$('.create-template').on('click', function(event) {
	var userToken = window['commonAdminLibrary']['userToken'];
	var apiServer = window['commonAdminLibrary']['apiServer'];
	event.preventDefault();
	$.ajax({
		url: apiServer+'/v1/mailing-template/chat-template',
		dataType: 'json',
		type: 'POST',	
        headers: {
            'Authorization': 'Bearer ' + userToken
        },
        data: {
        	'title': $('#template-title').val(),
        	'body': $('#template-body').val()       	
        },
		success: function (response) {
			if (response.success == true) {
				$('#modalMessage .modal-body span').text('Template was created');
              	$('#modalMessage, #layout').show();
              	setTimeout(function() {
					location.href='/manager/chat-mailing-templates';
              	}, 1500);
			} else {
				$('#modalMessage .modal-body span').text(response.message);
              	$('#modalMessage, #layout').show();
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			
		}
	});
});	     
";

$this->registerJs($js);