<?php
	use app\modules\manager\controllers\ManagerController;
	$session = Yii::$app->session;

	//var_dump($session['user_id']); die;
	
?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Chat Mailing Templates</h2>
			<a href="/manager/chat-mailing-template/create" class="btn">Add New</a>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<?php if (!empty($templates)) {?>
				<table>
					<tr>
						<th>ID</th>
						<th>Название</th>
						<th>Содержимое</th>
						<th>Статус</th>
						<th>Создан</th>						
						<th>Действия</th>
					</tr>
					<?php foreach ($templates as $template) {?>
					<?php
						if ($template->status == ManagerController::APPROVE_STATUS_APPROVED) {
							$currentStatus = 'Approved';
							$rowClass = 'approved';
						} elseif ($template->status == ManagerController::APPROVE_STATUS_IN_PROGRESS) {
							$currentStatus = 'In progress';
							$rowClass = 'in-progress';
						} elseif ($template->status == ManagerController::APPROVE_STATUS_DECLINED) {
							$currentStatus = 'Declined';
							$rowClass = 'declined';
						} else {
							$currentStatus = 'Not approved';
							$rowClass = 'not-approved';
						}				
						if (!empty($template->first_name) && !empty($template->last_name)) {
							$createdBy = $template->first_name . ' ' . $template->last_name;
						} elseif ($session['user_id'] == $template->template_creator) {
							$createdBy = $session['userName'];
						} else {
							$createdBy = '';
						}
					 ?>
					<tr class="<?= $rowClass ?>">
						<td><?= $template->id ?></td>
						<td><?= $template->title ?></td>
						<td><?= $template->body ?></td>
						<td><?= $currentStatus ?></td>
						<td><?= $createdBy ?></td>						
						<td class="edit">
							<ul class="edit-list list-inline">
								<li><a href="/manager/chat-mailing-template/edit/<?= $template->id ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
								<li><a href="javascript:void(0);" class="delete-template" data-id="<?= $template->id ?>"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
							</ul>
						</td>
					</tr>
					<?php } ?>
				</table>
				<?php if (isset($count)) {?>
					<div class="col-xs-12 col-sm-6 admin-finance-pagination">
						<div class="page-nav-wrapper">
							<?= $this->render('pagination.php', ['pageName' => 'mailing', 'limit' => $limit, 'userCount' => $count, 'currentpage' => $page]) ?>
						</div>	
					</div>				
				<?php } ?>
				<?php } else { ?>
				<div>Your agency don't have mailing templates.</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<style>
	.in-progress {
		background: rgba(252, 252, 0, 0.4);
	}
	.not-approved {
		background: rgba(251, 176, 38, 0.4);
	}
	.declined {
		background: rgba(255, 0, 0, 0.4);
	}
</style>

<?php 

$js = "
$('.delete-template').on('click', function(event) {
	var userToken = window['commonAdminLibrary']['userToken'];
	var apiServer = window['commonAdminLibrary']['apiServer'];
	event.preventDefault();
	$.ajax({
		url: apiServer+'/v1/mailing-template/chat-template/' + $(this).data('id'),
		dataType: 'json',
		type: 'DELETE',	
        headers: {
            'Authorization': 'Bearer ' + userToken
        },
        statusCode: {
		    204: function() {
		    	$('#modalMessage .modal-body span').text('Template was deleted');
              	$('#modalMessage, #layout').show();
              	setTimeout(function() {
					location.href='/manager/chat-mailing-templates';
              	}, 1500);
			}
		},
		success: function (response) {

		},
		error: function (xhr, ajaxOptions, thrownError) {
			
		}
	});
});	     
";

$this->registerJs($js);