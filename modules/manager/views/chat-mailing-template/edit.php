<?php
use yii\helpers\Url;
use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;

$approveStatuses = [
	ManagerController::APPROVE_STATUS_NOT_APPROVED => 'Not Approved',
	ManagerController::APPROVE_STATUS_IN_PROGRESS => 'In progress',
	ManagerController::APPROVE_STATUS_DECLINED => 'Declined',
	ManagerController::APPROVE_STATUS_APPROVED => 'Approved',
];

if (!empty($template->first_name) && !empty($template->last_name)) {
	$createdBy = $template->first_name . ' ' . $template->last_name;
} elseif ($session['user_id'] == $template->template_creator) {
	$createdBy = $session['userName'];
} else {
	$createdBy = '';
}

?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">
				Edit chat mailing template
			</h2>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<section class="profile-container admin-content">				                                      
				<div class="profile_information_block">
					<form>
						<p>ID: <?= $template->id ?></p>
						<p>Created at: <?= $template->created_at ?></p>
						<p>Created by: <?= $createdBy ?></p>
						<label for="template-title">Template title</label>
						<input type="text" id="template-title" name="title" value="<?= $template->title ?>">
						<label for="template-body">Template body</label>                          
						<div class="textarea">
							<textarea name="body" id="template-body" cols="30" rows="10"><?= $template->body ?></textarea>     
                        </div>
                        <select name="status">
                        	<?php foreach ($approveStatuses as $key => $value) { ?>
                        	<option value="<?= $key ?>" <?php if ($key == $template->status) {echo "selected='selected'"; } ?>><?= $value ?></option>
                        	<?php } ?>
                        </select>
						<button class="btn pull-right update-template" data-id="<?= $template->id ?>">Edit</button>
					</form>
				</div> 
				</section>
			</div>
		</div>
	</div>
</div>

<style>
	.textarea {
		margin: 20px 0;
	}
</style>

<?php 

$js = "
$('.update-template').on('click', function(event) {
	var userToken = window['commonAdminLibrary']['userToken'];
	var apiServer = window['commonAdminLibrary']['apiServer'];
	event.preventDefault();
	$.ajax({
		url: apiServer+'/v1/mailing-template/chat-template/' + $(this).data('id'),
		dataType: 'json',
		type: 'PATCH',	
        headers: {
            'Authorization': 'Bearer ' + userToken
        },
        data: {
        	'title': $('#template-title').val(),
        	'body': $('#template-body').val(),
        	'status': $('select[name=\"status\"]').val()
        },
		success: function (response) {
			if (response.success == true) {
				$('#modalMessage .modal-body span').text('Template was updated');
              	$('#modalMessage, #layout').show();
              	setTimeout(function() {
					location.href='/manager/chat-mailing-templates';
              	}, 1500);
			} else {
				$('#modalMessage .modal-body span').text(response.message);
              	$('#modalMessage, #layout').show();
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			
		}
	});
});	     
";

$this->registerJs($js);