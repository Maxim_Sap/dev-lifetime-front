<?php
	if (!function_exists('links')) {
		function links($currentpage, $filterString, $from, $to, $function, $link) 
		{
			$pages = array();
			// loop to show links to range of pages around current page
			for ($x = $from; $x <= $to; $x++) {
			   // if it's a valid page number...
			   if (($x > 0) && ($x <= $to)) {
			      // if we're on current page...
			      if ($x == $currentpage) {
			         // 'highlight' it but don't make a link
			      	if (!empty($function)) {
			        	$pages[] = "<li class=\"active\"><a href=\"javascript:void(0);\" onclick=\"$function($x);return false;\">$x</a></li>";
			      	} else {
			      		if ($x == 1) {
			      			$pages[] = "<li class=\"active\"><a href=\"$link?$filterString\">$x</a></li>";
			      		} else {
			      			$pages[] = "<li class=\"active\"><a href=\"$link?page=$x&$filterString\">$x</a></li>";
			      		}
			      	}
			      // if not current page...
			      } else {
			         // make it a link
			      	if (!empty($function)) {
			        	$pages[] = "<li><a href=\"javascript:void(0);\" onclick=\"$function($x);return false;\">$x</a></li>";
			     	} else {
			     		if ($x == 1) {
			     			$pages[] = "<li><a href=\"$link?$filterString\">$x</a></li>";
			     		} else {
			     			$pages[] = "<li><a href=\"$link?page=$x&$filterString\">$x</a></li>";
			     		}
			     	}
			      } // end else
			   } // end if 
			} // end for
			//var_dump($pages);
			return implode('', $pages);
		}
	}

switch($pageName){
	case 'mailing':
		$function = "";
		$link = "/manager/mailing-statistic";
		$filterString="dateFrom=$dateFrom&dateTo=$dateTo";
		break;
}
	
	$totalpages = (int) (ceil($userCount / $limit));
	//var_dump($totalpages);
?>
<?php

	/******  build the pagination links ******/
	// range of num links to show
	$range = 1;
	$window = $range * 2;
	$dots = "<li><a href=\"javascript:void(0);\">...</a></li>";
	
	$paginationResult = '';
	// if not on page 1, don't show prev links
	if ($currentpage > 1) {
	   // get previous page num
	   $prevpage = $currentpage - 1;
	   // show < link to go back to prev page
	    if (!empty($function)) {
		   $paginationResult .= "<li class=\"prev\"><a href=\"javascript:void(0);\" onclick=\"$function('$prevpage');return false;\">&lt;</a></li>";	   
	    } else {
	    	if ($prevpage == 1) {
	    		$paginationResult .= "<li class=\"prev\"><a href=\"$link?$filterString\">&lt;</a></li>";	 
	    	} else {
	    		$paginationResult .= "<li class=\"prev\"><a href=\"$link?page=$prevpage&$filterString\">&lt;</a></li>";	 
	    	}
	    }

	} // end if 

    // Example: 1 2 3 4
    if ($totalpages < 5 + $range*2) {
    	$from = 1;
    	$to = $totalpages;    	
		$paginationResult .= links($currentpage, $filterString, $from, $to, $function, $link);				
    }
    // Example: 1 [2] 3 4 5 6 ... 23 24
	elseif ($currentpage <= $window + 2)
	{
		$from = 1;
		$to = $window + 2;
		$paginationResult .= links($currentpage, $filterString, $from, $to, $function);
		$paginationResult .= $dots . ' '. links($currentpage, $filterString, $totalpages-1, $totalpages, $function, $link);
	}
	// Example: 1 2 ... 32 33 34 35 [36] 37
	elseif ($currentpage >= $totalpages - $window)
	{
		$from = $totalpages - $window - 1;
		$to = $totalpages;
		$paginationResult .= links($currentpage, $filterString, 1, 2, $function) . ' ' . $dots;
		$paginationResult .= links($currentpage, $filterString, $from, $to, $function);
	} else if (($currentpage > $window) && ($currentpage < $totalpages - $window)) {
		// Example: 1 2 ... 23 24 25 [26] 27 28 29 ... 51 52
		$paginationResult .= links($currentpage, $filterString, 1, 2, $function) . ' ' . $dots;
		$paginationResult .= links($currentpage, $filterString, $currentpage - $range, $currentpage + $range, $function, $link);
		$paginationResult .= $dots . ' '. links($currentpage, $filterString, $totalpages-1, $totalpages, $function, $link);
	}
		
	// if not on last page, show forward link
	if ($currentpage != $totalpages) {
	   // get next page
	   $nextpage = $currentpage + 1;
	    // echo forward link for next page
	   	if (!empty($function)) {
			$paginationResult .= "<li class=\"next\"><a href=\"javascript:void(0);\" onclick=\"$function('$nextpage');return false;\">&gt;</a></li>";	   
	   	} else {
	   		if ($nextpage == 1) {
	   			$paginationResult .= "<li class=\"next\"><a href=\"$link?$filterString\">&gt;</a></li>";	
	   		} else {
	   			$paginationResult .= "<li class=\"next\"><a href=\"$link?page=$nextpage&$filterString\">&gt;</a></li>";	
	   		}
	   	}
	   	   	  
	} // end if
	/****** end build pagination links ******/


?>
<ul class="page-navigation list-inline"><?= $paginationResult; ?></ul>
