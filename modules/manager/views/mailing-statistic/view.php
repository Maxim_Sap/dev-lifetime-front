<?php
	use app\modules\manager\controllers\ManagerController;
?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Статистика рассылки с ID - <?= $session->id ?></h2>
		</div>
		<div class="content-area-inner-body">
			<div class="content-container">
				<p><strong>Создатель</strong> - <?= $session->first_name ?> <?= $session->last_name ?></p>
				<p><strong>Название шаблона</strong></p>
				<div><?= $session->title ?></div> <br>
				<p><strong>Содержимое шаблона</strong></p>
				<div><?= $session->body ?></div> <br>
				<p><strong>Рассылка приостановлена?</strong> - <?= ($session->paused == ManagerController::PAUSED) ? 'Да' : 'Нет'; ?></p>
				<p><strong>Рассылка закончена?</strong> - <?= ($session->was_finished == ManagerController::WAS_FINISHED) ? 'Да' : 'Нет'; ?></p>
				<p><strong>Дата создания</strong> - <?= $session->started_at ?></p>
				<p><strong>Дата завершения</strong> - <?= $session->ended_at ?></p>
				<p><strong>Отправлено сообщений</strong> - <?= $session->sended_messages ?></p>
				<p><strong>Дубли (такой шаблон, уже были отправлен пользователю ранее)</strong> - <?= $session->dublicated_messages ?></p>
				<p><strong>Всего планировалось отправить</strong> - <?= $session->total_messages ?></p>
				<p><strong>При отправке рассылки был исключен черный список</strong> - <?= ($session->exclude_blacklist == 1) ? 'Да' : 'Нет'; ?></p>
				<?php if (!empty($mailingQueues)) {?>
					<table>
						<tr>
							<th>№ п/п</th>
							<th>Получатель</th>						
							<th>Было доставлено?</th>
							<th>Доставка была отменена (девушка отменила отправку шаблона)</th>						
							<th>Дата создания сообщения</th>												
							<th>Дата доставки сообщения</th>												
						</tr>
						<?php $i = 1; foreach ($mailingQueues as $queue) { ?>
							<tr>
								<td><?= ($page-1)*$limit + $i; ?></td>
								<td><?= $queue->first_name ?> <?= $queue->last_name ?></td>
								<td><?= ($queue->was_received == ManagerController::WAS_RECEIVED) ? "Да" : "Нет"; ?></td>
								<td><?= ($queue->was_aborted == ManagerController::WAS_ABORTED) ? "Да" : "Нет"; ?></td>
								<td><?= $queue->created_at ?></td>
								<td><?= $queue->received_at ?></td>						
							</tr>
						<?php $i++; } ?>						
					</table>
					<?php if (isset($count)) {?>
					<div class="col-xs-12 col-sm-6 admin-finance-pagination">
						<div class="page-nav-wrapper">
							<?= $this->render('pagination2.php', ['pageName' => 'queue', 'limit' => $limit, 'userCount' => $count, 'currentpage' => $page, 'sessionID' => $sessionID]) ?>
						</div>	
					</div>				
					<?php } ?>
				<?php } else {?>
					<p>В этой сессии не было отправлено никаких сообщений.</p>
				<?php } ?>
			</div>
		</div>
	</div>
</div>