<?php
	use app\modules\manager\controllers\ManagerController;
	use yii\helpers\Url;
	$userSession = Yii::$app->session;
	$month = [
		'01' => 'Январь',
		'02' => 'Февраль',
		'03' => 'Март',
		'04' => 'Апрель',
		'05' => 'Май',
		'06' => 'Июнь',
		'07' => 'Июль',
		'08' => 'Август',
		'09' => 'Сентябрь',
		'10' => 'Октябрь',
		'11' => 'Ноябрь',
		'12' => 'Декабрь',
	];
	$year = [];
	for ($i=0; $i < 100; $i++) { 
		$year[]=2015+$i;
	}
?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Статистика рассылок</h2>
			<form action="<?= Url::current(); ?>" method="get">
				<div>C: <select name="monthFrom">
					<?php foreach ($month as $key => $value) { ?>
						<option value="<?= $key ?>" <?php if (strtotime($dateFrom) && date('m', strtotime($dateFrom)) == $key) echo "selected='selected'"?>><?= $value ?></option>
					<?php }	?>							
					</select>
					<select name="yearFrom">
						<?php foreach ($year as $value) { ?>
							<option value="<?= $value ?>" <?php if (strtotime($dateFrom) && date('Y', strtotime($dateFrom)) == $value) echo "selected='selected'"?>><?= $value ?></option>
						<?php }	?>	
					</select>
				</div>
				<div>По: <select name="monthTo">
						<?php foreach ($month as $key => $value) { ?>
							<option value="<?= $key ?>" <?php if (strtotime($dateTo) && date('m', strtotime($dateTo)) == $key) echo "selected='selected'"?>><?= $value ?></option>
						<?php }	?>
					</select>
					<select name="yearTo">
						<?php foreach ($year as $value) { ?>
							<option value="<?= $value ?>" <?php if (strtotime($dateTo) && date('Y', strtotime($dateTo)) == $value) echo "selected='selected'"?>><?= $value ?></option>
						<?php }	?>	
					</select>
				</div>
				<input type="hidden" name="page" value="<?= $page ?>">
				<button class="btn filter">Filter</button>
			</form>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<?php if (!empty($sessions)) {?>
				<table>
					<tr>
						<th>ID</th>
						<th>Название шаблона</th>
						<th>Содержимое шаблона</th>
						<th>Приостановлена?</th>						
						<th>Закончена?</th>
						<th>Создана</th>						
						<th>Завершена</th>												
						<th>Подробнее</th>												
					</tr>
					<?php foreach ($sessions as $session) {?>
					<?php		
						if (!empty($session->first_name) && !empty($session->last_name)) {
							$createdBy = $session->first_name . ' ' . $session->last_name;
						} elseif ($userSession['user_id'] == $session->creator) {
							$createdBy = $userSession['userName'];
						} else {
							$createdBy = '';
						}
					 ?>
					<tr>
						<td><?= $session->id ?></td>
						<td><?= $session->title ?></td>
						<td><?= $session->body ?></td>
						<td><?= ($session->paused == ManagerController::PAUSED) ? 'Да' : 'Нет'; ?></td>
						<td><?= ($session->was_finished == ManagerController::WAS_FINISHED) ? 'Да' : 'Нет'; ?></td>
						<td><?= $createdBy ?><br><?= $session->started_at ?></td>						
						<td class="edit">
							<?= $session->ended_at ?>
						</td>
						<td><a href="/manager/mailing-statistic/<?= $session->id ?>"><i class="fa fa-eye"></i></a></td>
					</tr>
					<?php } ?>
				</table>
				<?php if (isset($count)) {?>
					<div class="col-xs-12 col-sm-6 admin-finance-pagination">
						<div class="page-nav-wrapper">
							<?= $this->render('pagination.php', ['pageName' => 'mailing', 'limit' => $limit, 'userCount' => $count, 'currentpage' => $page, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo]) ?>
						</div>	
					</div>				
				<?php } ?>
				<?php } else { ?>
				<div>Your agency don't have mailing sessions.</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<style>
	.in-progress {
		background: rgba(252, 252, 0, 0.4);
	}
	.not-approved {
		background: rgba(251, 176, 38, 0.4);
	}
	.declined {
		background: rgba(255, 0, 0, 0.4);
	}
</style>

<?php

$js = "
	$('.filter').on('click', function(event){
		event.preventDefault();
		var params = {
			//'page': $('input[name=\"page\"]').val(),
			'dateFrom': $('select[name=\"yearFrom\"]').val() + '-' + $('select[name=\"monthFrom\"]').val() + '-' + '01',
			'dateTo': $('select[name=\"yearTo\"]').val() + '-' + $('select[name=\"monthTo\"]').val() + '-' + '01'
		};
		location.href = '//' + location.host + location.pathname + '?' + $.param(params);
	});

";

$this->registerJs(
    $js
);

?>