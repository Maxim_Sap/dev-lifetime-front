<?php
	$session = Yii::$app->session;	
?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Ожидает подтверждения</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<h3>Agency profile</h3>
				<?php if(!empty($agencyList)) { ?>				
				<div class="table_scroll_wrap">
					<table class="finance-leters-table">
						<thead>
							<th>ID</th>
							<th>Название агенства</th>
							<th>последняя активность</th>
							<th>статус</th>
							<th>действие</th>
						</thead>
						<tbody>
							<?php foreach($agencyList as $agency){
								if($agency->approve_status == 2){
									$approve_status = 'in progress';
								}elseif($one->approve_status == 1){
									$approve_status = 'approved';
								}else{
									$approve_status = 'not approved';
								}
								?>
								<tr>
									<td><?=$agency->id?></td>
									<td><?=trim($agency->name)?></td>
									<td style="text-align: center"><?=date('Y-m-d H:i:s',$agency->last_activity)?></td>
									<td style="text-align: center"><?=$approve_status?></td>
									<td style="text-align: center"><a href="/manager/edit-user/<?=$one->user_id?>">проверить</a></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<?php } else {?>
					<p>No results</p>
				<?php } ?>
				<h3>Agency Users</h3>
				<?php if(!empty($usersList) || !empty($videoList) || !empty($photosList) || !empty($deletedPhotosList)) { ?>
				<div class="table_scroll_wrap">
				<table class="finance-leters-table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Название агенства</th>
							<th>действие</th>
						</tr>
					</thead>
					<tbody>
					<?php $userArray = []; ?>
					<?php foreach($usersList as $user){ ?>
							<?php if (!in_array($user->agency_id, $userArray)) {?>
							<tr>
								<td><?=$user->agency_id?></td>
								<td><?=$user->name?></td>
								<td style="text-align: center"><a href="/manager/need-to-approve/agency/<?=$user->agency_id?>">подробнее</a></td>
							</tr>
							<?php $userArray[] = $user->agency_id; } ?>
						<?php } ?>
					<?php foreach($videoList as $video){ ?>
							<?php if (!in_array($video->agency_id, $userArray)) {?>
							<tr>
								<td><?=$video->agency_id?></td>
								<?php if ($video->agency_id == 1) {?>
								<td>Пользователи без агенства</td>
								<?php } else { ?>
								<td><?= $agencyArray[$video->agency_id] ?></td>
								<?php } ?>
								<td style="text-align: center"><a href="/manager/need-to-approve/agency/<?=$video->agency_id?>">подробнее</a></td>
							</tr>
							<?php $userArray[] = $video->agency_id; } ?>
						<?php } ?>	
					<?php foreach($photosList as $photo){ ?>
							<?php if (!in_array($photo->agency_id, $userArray)) {?>
							<tr>
								<td><?=$photo->agency_id?></td>
								<?php if ($photo->agency_id == 1) {?>
								<td>Пользователи без агенства</td>
								<?php } else { ?>
								<td><?= $agencyArray[$photo->agency_id] ?></td>
								<?php } ?>
								<td style="text-align: center"><a href="/manager/need-to-approve/agency/<?=$photo->agency_id?>">подробнее</a></td>
							</tr>
							<?php $userArray[] = $photo->agency_id; } ?>
						<?php } ?>	
					<?php foreach($deletedPhotosList as $deletedPhotos){ ?>
							<?php if (!in_array($deletedPhotos->agency_id, $userArray)) {?>
							<tr>
								<td><?=$deletedPhotos->agency_id?></td>
								<?php if ($deletedPhotos->agency_id == 1) {?>
								<td>Пользователи без агенства</td>
								<?php } else { ?>
								<td><?=$agencyArray[$deletedPhotos->agency_id]?></td>
								<?php } ?>
								<td style="text-align: center"><a href="/manager/need-to-approve/agency/<?=$deletedPhotos->agency_id?>">подробнее</a></td>
							</tr>
							<?php $userArray[] = $deletedPhotos->agency_id; } ?>
						<?php } ?>																		
					</tbody>
				</table>
				</div>
				<?php } else {?>
					<p>No results</p>
				<?php } ?>
				<h3>Agency Gifts</h3>
				<?php if(!empty($giftsList)){ ?>
					<div class="table_scroll_wrap">
						<table class="finance-leters-table">
							<thead>
							<tr>
								<th>ID</th>
								<th>Название агенства</th>
								<th>действие</th>
							</tr>
							</thead>
							<tbody>
							<?php 
								foreach($giftsList as $gift){ ?>
									<tr>
										<td><?=$gift->agency_id?></td>
										<td><?=$gift->name?></td>
										<td style="text-align: center"><a href="/manager/need-to-approve/agency/<?=$gift->agency_id?>">подробнее</a></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				<?php } else {?>
					<p>No results</p>
				<?php } ?>
				<h3>Agency Tasks</h3>
				<?php if(!empty($tasksList)){ ?>
				<div class="table_scroll_wrap">
				<table class="finance-leters-table">
					<thead>
					<tr>
						<th>ID</th>
						<th>Название агенства</th>
						<th>действие</th>
					</tr>
					</thead>
					<tbody>
						<?php
						foreach($tasksList as $task){ ?>
							<tr>
								<td><?=$task->agency_id?></td>
								<td><?=$task->name?></td>
								<td style="text-align: center"><a href="/manager/need-to-approve/agency/<?=$task->agency_id?>">подробнее</a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php }else{?>
					<p>No results</p>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

