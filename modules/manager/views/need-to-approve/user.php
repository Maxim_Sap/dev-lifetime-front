<?php
	$session = Yii::$app->session;
?>
<div id="content-area">
	<div class="content-area-inner">
		<div class="content-area-inner-header">
			<h2 class="content-title">Ожидает подтверждения</h2>			
		</div>
		<div class="content-area-inner-body">
			<div class="content-container">
				<h3>Agency name: <a target="_blank" href="/manager/edit-user/<?=$agencyInfo['id'];?>"><?=trim($agencyInfo['name'])?></a></h3>
				<h3>User name: <a target="_blank" href="/manager/edit-user/<?=$userInfo['id'];?>"><?=trim($userInfo['name'])." ID: ".$userInfo['id']?></a></h3>
				<h3>Не подтвержденный профиль</h3>
				<?php if(!empty($usersList)){ ?>
					<div class="table_scroll_wrap">
						<table class="finance-leters-table">
							<thead>
							<th>ID</th>
							<th>User Profile</th>
							<th>действие</th>
							</thead>
							<tbody>
								<tr>
								<?php foreach ($usersList as $user) { ?>
									<td><?=$user->id?></td>
									<td><?=trim($user->first_name . ' '.$user->last_name);?></td>
									<td style="text-align: center"><a target="_blank" href="/manager/edit-user/<?=$user->id?>">проверить</a></td>
								<?php } ?>
								</tr>
							</tbody>
						</table>
					</div>
				<?php } else {?>
					<p>No users for approve</p>
				<?php } ?>
				<h3>Не подтвержденные видео</h3>
				<?php if(!empty($videoList)) {?>
				<div class="table_scroll_wrap">
					<table class="finance-leters-table">
						<thead>
							<th>Video ID</th>
							<th>дата добавления</th>
							<th>действие</th>
						</thead>
						<tbody>
						<?php foreach($videoList as $video){ ?>
							<tr>
								<td><?=$video->id?></td>
								<td style="text-align: center"><?=$video->created_at?></td>
								<td style="text-align: center"><a target="_blank" href="/manager/video-gallery/<?=$video->user_id?>">проверить</a></td>
							</tr>
						<?php }?>
						</tbody>
					</table>
				</div>
				<?php } else {?>
					<p>No video for approve</p>
				<?php } ?>
				<h3>Не подтвержденные фото</h3>
				<?php if(!empty($photoList)){?>
				<div class="table_scroll_wrap">
					<table class="finance-leters-table">
						<thead>
							<th>Photo ID</th>
							<th>дата добавления</th>
							<th>изображение</th>
							<th>действие</th>
						</thead>
						<tbody>
						<?php foreach($photoList as $photo){ ?>
							<tr>
								<td><?=$photo->id?></td>
								<td style="text-align: center"><?=$photo->created_at?></td>
								<td style="text-align: center"><img src="<?=$this->context->serverUrl.'/'.$photo->small_thumb;?>" /></td>
								<td style="text-align: center"><a target="_blank" href="/manager/photo/<?=$photo->id?>/<?=$userInfo['id']?>">проверить</a></td>
							</tr>
						<?php }?>
						</tbody>
					</table>
				</div>
				<?php } else {?>
					<p>No photo to approve</p>
				<?php } ?>
				<h3>Не подтвержденные удаленные фото</h3>
				<?php if(!empty($deletedPhotoList)){?>				
				<div class="table_scroll_wrap">
					<h3>Users' photos deleted</h3>
					<table class="finance-leters-table">
						<thead>
						<th>Photo ID</th>
						<th>дата добавления</th>
						<th>изображение</th>
						<th>действие</th>
						</thead>
						<tbody>
						<?php foreach($deletedPhotoList as $deletedPhoto){ ?>
							<tr>
								<td><?=$deletedPhoto->id?></td>
								<td style="text-align: center"><?=$deletedPhoto->created_at?></td>
								<td style="text-align: center"><img src="<?=$this->context->serverUrl.'/'.$deletedPhoto->small_thumb;?>" /></td>
								<td style="text-align: center"><a target="_blank" href="/manager/photo/<?=$deletedPhoto->id?>/<?=$userInfo['id']?>">проверить</a></td>
							</tr>
						<?php }?>
						</tbody>
					</table>
				</div>
			<?php }?>
			</div>
		</div>
	</div>
</div>
