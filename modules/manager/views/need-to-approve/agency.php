<?php
	$session = Yii::$app->session;
?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2>Ожидает подтверждения</h2>
			<?php if ($agencyID == 1) {?>
				<h3>Agency name: <?=trim($agencyName)?></h3>
			<?php } else {?>
				<h3>Agency name: <a href="/manager/edit-user/<?=$agencyID;?>"><?=trim($agencyName)?></a></h3>
			<?php } ?>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<h3>Users</h3>
				<?php if(!empty($usersList) || !empty($videoList) || !empty($photosList) || !empty($deletedPhotosList)){ ?>
				<div class="table_scroll_wrap">
				<table class="finance-leters-table">
					<thead>
					<th>ID</th>
					<th>Имя</th>
					<th>действие</th>
					</thead>
					<tbody>
					<?php
						$userArray = [];
						foreach($usersList as $user){ 
							if (!in_array($user->id, $userArray)) {?>
							<tr>
								<td><?=$user->id?></td>
								<td><?=$user->first_name . ' ' . $user->last_name; ?></td>
								<td style="text-align: center"><a href="/manager/need-to-approve/user/<?=$user->id?>">подробнее</a></td>
							</tr>
						<?php } 
							$userArray[] = $user->id; 
						} ?>
						<?php foreach($videoList as $video){ 
								if (!in_array($video->user_id, $userArray)) {?>
								<tr>
									<td><?=$video->user_id?></td>
									<td><?=$video->first_name . ' ' . $video->last_name; ?></td>
									<td style="text-align: center"><a href="/manager/need-to-approve/user/<?=$video->user_id?>">подробнее</a></td>
								</tr>	
							<?php } 
							$userArray[] = $video->user_id; 
						} ?>
						<?php foreach($photosList as $photo){ 
								if (!in_array($photo->user_id, $userArray)) {?>
								<tr>
									<td><?=$photo->user_id?></td>
									<td><?=$photo->first_name . ' ' . $photo->last_name; ?></td>
									<td style="text-align: center"><a href="/manager/need-to-approve/user/<?=$photo->user_id?>">подробнее</a></td>
								</tr>	
							<?php } 
							$userArray[] = $photo->user_id; 
						} ?>
						<?php foreach($deletedPhotosList as $deletedPhoto){ 
								if (!in_array($deletedPhoto->user_id, $userArray)) {?>
								<tr>
									<td><?=$deletedPhoto->user_id?></td>
									<td><?=$deletedPhoto->first_name . ' ' . $deletedPhoto->last_name; ?></td>
									<td style="text-align: center"><a href="/manager/need-to-approve/user/<?=$deletedPhoto->user_id?>">подробнее</a></td>
								</tr>	
							<?php } 
							$userArray[] = $deletedPhoto->user_id; 
						} ?>						
					</tbody>
				</table>
				</div>
				<?php } else {?>
					<p>No results</p>
				<?php } ?>
				<h3>Gifts</h3>
				<?php if(!empty($giftsList)){ ?>
				<div class="table_scroll_wrap">
					<table class="finance-leters-table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Кому</th>
								<th>действие</th>
							</tr>
						</thead>
						<tbody>
						<?php 
							foreach($giftsList as $gift){ ?>
								<tr>
									<td><?=$gift->id?></td>
									<td><?=$gift->first_name . ' ' . $gift->last_name; ?></td>
									<td style="text-align: center"><a href="/manager/edit-shop-item/<?= $gift->action_id ?>">подробнее</a></td>
								</tr>
							<?php } ?>						
						</tbody>
					</table>
				</div>
				<?php } else {?>
					<p>No results</p>
				<?php } ?>
				<h3>Tasks</h3>
				<?php if(!empty($tasksList)){ ?>
				<div class="table_scroll_wrap">
				<table class="finance-leters-table">
					<thead>
					<tr>
						<th>ID</th>
						<th>краткое описание</th>
						<th>действие</th>
					</tr>
					</thead>
					<tbody>
					<?php 
						mb_internal_encoding('utf-8');
						foreach($tasksList as $task){ ?>
							<tr>
								<td><?=$task->id?></td>
								<td><?=mb_substr($task->description,0,80)?></td>
								<td style="text-align: center"><a href="/manager/edit-task/<?=$task->id?>">подробнее</a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php }else{?>
					<p>No results</p>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
