<?php
use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;
$monthArray = array("1"=>"Январь","2"=>"Февраль","3"=>"Март","4"=>"Апрель","5"=>"Май", "6"=>"Июнь", "7"=>"Июль","8"=>"Август","9"=>"Сентябрь","10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");
for ($i=2015; $i < 2100; $i++) { 
	$yearArray[] = $i;
}
$paymentStatus = array(1=>'текущий','в обработке','закрыт');
$statusClass = array(1=>'yellow','blue','green');
$ajax = Yii::$app->request->getIsAjax();
if(!$ajax){
?>
<style>
	.yellow>td {
		background-color: #f2f19f;
	}
	.blue>td {
		background-color: #d0e6f3;
	}
	.green>td {
		background-color: #2bca1e;
	}
	.admin-table-btn{
		text-decoration: underline;
		margin: 0 6px;
	}
</style>
<div id="content-area">
	<div class="content-area-inner">		
		<div class="content-area-inner-header">
			<h2 class="content-title">График выплат:</h2>			
		</div>
		<div class="content-area-inner-body">
			<div class="content-container">
				<form action="" method="post" class="agency-payments-schedule-form margin-bottom" style="width: 500px">
		        <table>
					<?php if(isset($agencyList) && !empty($agencyList)){
						if(is_array($agencyList)){?>
						<tr>
							<td><label>Агенство:</label></td>
							<td colspan="4">
								<select name="agency_id" class="my-users1">
									<?php foreach($agencyList as $agency){?>
										<option value="<?=$agency->user_id?>"><?=trim($agency->name);?></option>
									<?php }?>
								</select>
							</td>
						</tr>
						<?php }else{?>
							<input type="hidden" name="agency_id" value="<?=(int)$agencyList;?>">
						<?php }
					}else{ ?>
						<tr><td colspan="2"><p>no agency</p></td></tr>
					<?php } ?>
					<tr>
		                <td><label>С:</label></td>
						<td>месяц</td>
						<td>
							<select id="month_from">
								<?php foreach ($monthArray as $key=>$month){?>
									<option value="<?=$key?>" <?=($key == date('m',strtotime($dateFrom))) ? 'selected': ''?>><?=$month;?></option>
								<?php } ?>
							</select>
						</td>
						<td>год</td>
						<td>
							<select id="year_from">
								<?php foreach ($yearArray as $year){?>
									<option value="<?=$year?>" <?=($year == date('Y',strtotime($dateFrom))) ? 'selected': ''?>><?=$year;?></option>
								<?php } ?>
							</select>
						</td>
		            </tr>
		            <tr>
		                <td><label>по:</label></td>
						<td>месяц</td>
						<td>
							<select id="month_to">
								<?php foreach ($monthArray as $key=>$month){?>
									<option value="<?=$key?>" <?=($key == date('m',strtotime($dateTo))) ? 'selected': ''?>><?=$month;?></option>
								<?php } ?>
							</select>
						</td>
						<td>год</td>
						<td>
							<select id="year_to">
								<?php foreach ($yearArray as $year){?>
									<option value="<?=$year?>" <?=($year == date('Y',strtotime($dateTo))) ? 'selected': ''?>><?=$year;?></option>
								<?php } ?>
							</select>
						</td>
		            </tr>
		        </table>
				<input type="submit" value="показать" class="btn admin-custom-btn margin-top margin-bottom">
				</form>
				<?php }?>
				<div id="agency-payments-schedule-block">
				<table class="agency-payments-schedule-table">
					<thead>
						<th>период</th>
						<th>статус</th>
						<th>заработок</th>
						<th>штрафы</th>
						<th>перенесено</th>
						<th>к выплате</th>
						<th>комментарий</th>
						<th>дата выплаты</th>
						<th>выплачено</th>
						<th></th>
					</thead>
					<tbody>
						<?php
						if(!empty($paymentsData)){?>
								<?php foreach($paymentsData as $payment){
									$needToPay = $payment->earnings + $payment->transfer - $payment->penalty;
									?>
									<tr class="<?=$statusClass[$payment->status]?>">
										<td><?=date('M Y',strtotime($payment->period_date))?></td>
										<td><?=$paymentStatus[$payment->status]?></td>
										<td><?=$payment->earnings?></td>
										<td><?=$payment->penalty?></td>
										<td><?=$payment->transfer?></td>
										<td><?=($needToPay < 0 ? "<span style='color: red;font-weight: bold'>".$needToPay."</span>" : $needToPay)?></td>
										<td><?=$payment->comments?></td>
										<td><?=(strtotime($payment->payment_day) ? date('Y-m-d',strtotime($payment->payment_day)) : '')?></td>
										<td><?=$payment->paid_value?></td>
										<td>
											<?php
											if($session['user_type'] == ManagerController::USER_SUPERADMIN && $payment->status == 2){ //только для статуса в обработке?>
												<a class="admin-table-btn" href="/manager/finance/transfer-agency-payments/<?=$payment->id?>" title="перенести выплату на следующий месяц">перенести</a>
												<a class="admin-table-btn" href="/manager/finance/add-agency-payments/<?=$payment->id?>" title="перейти к форме оплаты">выплатить</a>
											<?php } ?>
										</td>
									</tr>
							<?php } ?>
						<?php }else{?>
							<tr>
								<td colspan="9">No results</td>
							</tr>
						<?php } ?>

					</tbody>
				</table>
				</div>
				<?php if(!$ajax){?>
				<div class="agency-detail-finance-info"></div>
			</div>
		</div>
	</div>
</div>
<?php } 

$js = 'if($(\'select.my-users1 option:selected\').val() != ""){
		$(\'.agency-finance-form\').submit();
	}';
$this->registerJs($js, Yii\web\View::POS_READY);

?>
