<?php
$month_mass     = array("1" => "Январь", "2" => "Февраль", "3" => "Март", "4" => "Апрель", "5" => "Май", "6" => "Июнь", "7" => "Июль", "8" => "Август", "9" => "Сентябрь", "10" => "Октябрь", "11" => "Ноябрь", "12" => "Декабрь");
$payment_status = array(1 => 'текущий', 'в обработке', 'закрыт');

?>
<div id="content-area">
    <div class="content-area-inner">

        <h2>Добавить выплату:</h2>
        <div class="content-area-inner-body">
            <form action="" method="post" class="add-agency-payments-form margin-bottom" style="width: 500px">
                <table>
                    <tr>
                        <td><label>Агенство:</label></td>
                        <td>
                            <a href="/manager/edit-user/<?=$order_info->agency_id?>" title="user info">
                                <?=trim($order_info->first_name . " " . $order_info->last_name) . " ID:[" . $order_info->agency_id . "]"?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td><label>За:</label></td>
                        <td>
                            <?=$month_mass[date('n', strtotime($order_info->period_date))];?>
                            <?=date('Y', strtotime($order_info->period_date));?>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Заработано в прошлом месяце:</label></td>
                        <td>
                            <input id="agency_bonus" type="text" disabled
                                   value="<?=$order_info->earnings?>">
                        </td>
                    </tr>
                    <tr>
                        <td><label>Перенесена выплата:</label></td>
                        <td>
                            <input id="agency_transfer" type="text" disabled
                                   value="<?=$order_info->transfer?>">
                        </td>
                    </tr>
                    <tr>
                        <td><label>Штраф за прошлый месяц:</label></td>
                        <td>
                            <input id="agency_penalty" type="text" disabled
                                   value="<?=$order_info->penalty?>">
                        </td>
                    </tr>
                    <tr>
                        <td><label>Комментарий</label></td>
                        <td>
                            <textarea id="comments" rows="4" cols="31">выплачено</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Подлежит к оплате:</label></td>
                        <td>
                            <input id="payment_value" type="text"
                                   value="<?=$order_info->earnings + $order_info->transfer - $order_info->penalty?>">
                        </td>
                    </tr>
                </table>
                <input type="hidden" id="agency_id" value="<?=$order_info->agency_id?>">
                <input type="hidden" id="order_id" value="<?=$order_info->id?>">
                <input type="submit" value="Оплатить" class="admin-custom-btn margin-top margin-bottom">
            </form>
            <div class="agency-detail-finance-info"></div>
        </div>
    </div>
</div>