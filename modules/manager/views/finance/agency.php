<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Финансы: Агенств</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
					<form action="" method="post" class="agency-finance-form margin-bottom">
			        <table>
			            <tr>
			                <td><label>Агенство:</label></td>
			                <td>
			                <select name="agency_id" class="my-users">
			                    <?php if(isset($agencyArray) && !empty($agencyArray)){
			                        foreach($agencyArray as $agency){?>
			                            <option value="<?=$agency->user_id?>"><?=trim($agency->name);?></option>
			                    <?php }
			                    }else{ ?>
			                        <option value="">no agency</option>
			                    <?php } ?>
			                </select>
			                </td>
			            </tr>
			            <tr>
			                <td><label>С:</label></td>
			                <td>
			                    <input type="date" name="date-from" value="<?=$dateFrom;?>">
			                </td>
			            </tr>
			            <tr>
			                <td><label>по:</label></td>
			                <td>
			                    <input type="date" name="date-to" value="<?=$dateTo;?>">
			                </td>
			            </tr>
			        </table>
					<input type="submit" value="поиск" class="btn margin-top margin-bottom">
				</form>
				<table class="agency-finance-table">
					<thead>
						<th>наименование</th>
						<th>количество</th>
						<th>сумма</th>
					</thead>
					<tbody>
						<tr>
							<td><a href="#">письма</a></td>
							<td></td>
							<td> cr</td>
						</tr>
						<tr>
							<td><a href="#">чат</a></td>
							<td></td>
							<td> cr</td>
						</tr>
						<tr>
							<td><a href="#">видео чат</a></td>
							<td></td>
							<td> cr</td>
						</tr>
						<tr>
							<td><a href="#">подарки</a></td>
							<td></td>
							<td> cr</td>
						</tr>
						<tr>
							<td><a href="#">приват фото</a></td>
							<td></td>
							<td> cr</td>
						</tr>
						<tr>
							<td><a href="#">приват видео</a></td>
							<td></td>
							<td> cr</td>
						</tr>
						<tr>
							<td colspan="2"><strong>Итого:</strong></td>
							<td><strong> cr</strong></td>
						</tr>
					</tbody>
				</table>
				<div class="agency-detail-finance-info"></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">$(document).ready(function(){
	if($('select.my-users option:selected').val() != ""){
		$('.agency-finance-form').submit();
	}
	
});</script>