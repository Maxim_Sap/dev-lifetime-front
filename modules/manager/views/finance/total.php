<?php $js_label = "";
      $js_data = "";
      $ajax = Yii::$app->request->getIsAjax();
?>
<div id="content-area">
	<div class="content-area-inner">		
		<div class="content-area-inner-header">
			<?php if(isset($type) && $type == 'index'){?>
			<h2>Рабочий стол админа</h2>
			<div class="admin-content">
				<table style="width: 800px; text-align: center">
					<thead>
						<tr>
							<th>Общее количество мужчин</th>
							<th>Из них онлайн</th>
							<th>Общее количество агенств</th>
							<th>Общее количество девочек</th>
							<th>Из них онлайн</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="total-men"></td>
							<td id="men-online"></td>
							<td id="total-agency"></td>
							<td id="total-girls"></td>
							<td id="girls-online"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<?php
			$js_script = 'window["adminAccountLibrary"]["getTotalPeopleCount"]();';
			$this->registerJs($js_script, Yii\web\View::POS_READY);	?>	
			<?php } ?>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<?php
				setlocale(LC_MONETARY, 'en_US');
				//echo ;
				//var_dump($total_finance_all_period->total_transaction);die;
				if (!empty($totalFinanceAllPeriod)) {
					$totalTransaction = $totalFinanceAllPeriod->totalTransaction[0]->total_amount;
					$totalCharges = $totalFinanceAllPeriod->totalCharges[0]->total_amount;
					$totalAgencyPayments = $totalFinanceAllPeriod->totalAgencyPayments[0]->total_amount;
					$totalDiff = $totalFinanceAllPeriod->totalCharges[0]->total_amount - $totalFinanceAllPeriod->totalAgencyPayments[0]->total_amount;
				} else {
					$totalTransaction = 0;
					$totalCharges = 0;
					$totalAgencyPayments = 0;
					$totalDiff = 0;
				}
				
				if (!empty($totalFinanceFromPeriod)) {
					$transaction = $totalFinanceFromPeriod->totalTransaction[0]->total_amount;
					$charges = $totalFinanceFromPeriod->totalCharges[0]->total_amount;
					$agencyPayments = $totalFinanceFromPeriod->totalAgencyPayments[0]->total_amount;
					$diff = $totalFinanceFromPeriod->totalCharges[0]->total_amount - $totalFinanceFromPeriod->totalAgencyPayments[0]->total_amount;
				} else {
					$transaction = 0;
					$charges = 0;
					$agencyPayments = 0;
					$diff = 0;
				}
				//var_dump($total_finance_all_period->total_transaction[0]->total_amount);die;
				//$ajax = Yii::$app->request->getIsAjax();
					//if(!$ajax){	?>
				<script src="/js/admin/Chart.min.js"></script>
				<h2>Финансы:</h2>
				<table style="width: 800px">
					<thead>
						<tr>
							<th>Всего поступило средств:</th>
							<th>Всего потрачено пользователями:</th>
							<th>Всего выплачено агенствам:</th>
							<th>Всего заработано:</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><?=money_format('%i', $totalTransaction)?></td>
							<td><?=money_format('%i', $totalCharges)?></td>
							<td><?=money_format('%i', $totalAgencyPayments)?></td>
							<td><?=money_format('%i', $totalDiff)?></td>
						</tr>
					</tbody>
				</table>
				<br><br><br>
				<?php if(isset($type) && $type == 'index'){?>
						<h2>Финансовая информация по агенствам за текущий месяц:</h2>
						<?php }else{?>
						<h2>Фильтр:</h2>
			            
						<form action="/manager/finance/total" method="post" class="total-finance-form">
			                <table>
			                    <tr>
			                        <td><label>С:</label></td>
			                        <td><input type="date" name="dateFrom" value="<?=$dateFrom;?>"></td>
			                    </tr>
			                    <tr>
			                        <td><label>по:</label></td>
			                        <td><input type="date" name="dateTo" value="<?=$dateTo;?>"></td>
			                    </tr>
			                </table>            				
							<input type="submit" value="применить" class="btn admin-custom-btn margin-top margin-bottom">
						</form>
						<h2>За указанный период:</h2>
						<?php }?>
					<?php //}	?>
				<table style="width: 800px">
					<thead>
					<tr>
						<th>Поступило средств:</th>
						<th>Потрачено пользователями:</th>
						<th>Выплачено агенствам:</th>
						<th>Заработано:</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td><?=money_format('%i', $transaction)?></td>
						<td><?=money_format('%i', $charges)?></td>
						<td><?=money_format('%i', $agencyPayments)?></td>
						<td><?=money_format('%i', $diff)?></td>
					</tr>
					</tbody>
				</table>
				<br><br><br>
				<h2>Заработано агенствами:</h2>
				<div class="ajax-content">
					<div id="canvas-wrapper">
						<canvas id="diagram" width="1024" height="400"></canvas>
					</div>
					<table class="total-finance-table admin-custom-border margin-top">
						<thead>
							<th>наименование агенства</th>
							<th>письма</th>
							<th>чат</th>
							<th>чат (длительность)</th>
							<th>видео чат</th>
							<th>видео чат (длительность)</th>
							<th>просмотр видеороликов</th>
							<th>просмотр фотографий</th>
							<th>подарки</th>
							<th>Итого</th>
						</thead>
						<tbody>
							<?php if(isset($agencyList) && !empty($agencyList)){
									$agencyList = (object) $agencyList;
									$js_label = "";
									$js_data = "";
									$allTotal = 0;
									foreach($agencyList as $agency){										
										$agency = (object) $agency;
										$js_label .= '"'. trim($agency->name). '",';
										$totalAmount = (float)$agency->letters_amount + (float)$agency->chat_amount + (float)$agency->video_chat_amount + (float)$agency->gifts_amount + (float)$agency->watch_video_amount + (float)$agency->watch_photo_amount;
										$allTotal += $totalAmount;
										$js_data .= '"'. $totalAmount . '",';
									?>
									<tr>
										<td><?=trim($agency->name);?></td>
										<td><?=(!empty($agency->letters_amount) ? round($agency->letters_amount,2) : 0);?> cr</td>
										<td><?=(!empty($agency->chat_amount) ? round($agency->chat_amount,2) : 0);?> cr</td>
										<td><?=date("H:i:s", mktime(0, 0, (int)$agency->chat_duration));?></td>
										<td><?=(!empty($agency->video_chat_amount) ? round($agency->video_chat_amount,2) : 0);?> cr</td>
										<td><?=date("H:i:s", mktime(0, 0, (int)$agency->video_chat_duration));?></td>
										<td><?php echo (!empty($agency->watch_video_amount) ? round($agency->watch_video_amount,2) : 0);?> cr</td>
										<td><?php echo (!empty($agency->watch_photo_amount) ? round($agency->watch_photo_amount,2) : 0);?> cr</td>
										<td><?=(!empty($agency->gifts_amount) ? round($agency->gifts_amount,2) : 0);?> cr</td>
										<td><strong><?=$totalAmount;?> cr</strong></td>
									</tr>
									<?php } ?>
									<tr>
										<td colspan="9"><strong>Итого:</strong></td>
										<td><strong><?=round($allTotal,2);?> cr</strong></td>
									</tr>
								<?php }else{ ?>
								<tr>
									<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
								</tr>
								<?php } ?>
						</tbody>
					</table>

					<script type="text/javascript">
						if( window.myLine!==undefined){
							window.myLine.clear(); 
							window.myLine.destroy();
						}

						var canvas = document.getElementById('diagram');
						var context = canvas.getContext('2d');
						var label = [<?=$js_label?>];
						var data = [<?=$js_data?>];
						var barData = {
							labels : label,
							datasets : [
								{
									fillColor : "#664d94",
									strokeColor : "#4d3b70",
									
									data : data
								},
							]
						}
						var myLine = new Chart(context).Bar(barData, {
							responsive : true,
							
						});
					</script>
				</div>
			</div>
		</div>
	</div>
</div>

