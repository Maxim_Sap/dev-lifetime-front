<?php
use app\modules\manager\controllers\ManagerController;
?>
<div id="content-area">
    <div class="content-area-inner">

        <div class="content-area-inner-header">
            <h2 class="content-title">Финансовая информация за выбраный период</h2>
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <form action="" method="post" class="user-affiliates-form margin-top">
                    <table style="width: 350px">
                        <tr>
                            <td><label>С:</label></td>
                            <td><input type="date" name="date-from" value="<?=$date_from;?>"></td>
                        </tr>
                        <tr>
                            <td><label>по:</label></td>
                            <td><input type="date" name="date-to" value="<?=$date_to;?>"></td>
                        </tr>
                        <?php if($user_type == ManagerController::USER_SUPERADMIN){?>
                            <tr>
                                <td><label>Аффилейт:</label></td>
                                <td><select data-active-user-id="<?=$other_user_id?>" name="other_user_id"
                                            class="list-of-affiliates"></select></td>
                            </tr>
                        <?php } ?>
                    </table>
                    <input type="submit" value="поиск" class="admin-custom-btn margin-top">
                </form>
                <?php
                //var_dump($stats);die;
                if (!empty($total)) {
                    $transaction = (!empty($total->totalTransaction[0]->total_amount)) ? $total->totalTransaction[0]->total_amount : 0;
                    $charges     = (!empty($total->totalCharges[0]->total_amount)) ? $total->totalCharges[0]->total_amount : 0;
                    $bonus       = $charges / 100 * 5;
                    ?>
                    <br/><br/><br/>
                    <table>
                        <thead>
                        <tr>
                            <th>Пополнено пользоватеями депозит на сумму</th>
                            <th>Из них потрачено на услуги</th>
                            <th>Вознаграждение</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="text-align: center"><?=$transaction;?> cr</td>
                            <td style="text-align: center"><?=$charges?> cr</td>
                            <td style="text-align: center"><?=$bonus?> cr</td>
                        </tr>
                        </tbody>
                    </table>
                <?php } else {
                    ?>
                    <p>No results</p>
                <?php } ?>

            </div>
        </div>
    </div>
</div>