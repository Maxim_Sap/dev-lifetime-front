<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Финансы: Чат</h2>
			<a href="/manager/finance/letters" class="admin-tab btn">Письма</a>
			<a href="/manager/finance/chats" class="active btn admin-tab">Чат</a>
			<a href="/manager/finance/video-chats" class="btn admin-tab">Видео чат</a>
			<a href="/manager/finance/gifts" class="admin-tab btn">Подарки</a>
			<a href="/manager/finance/premium-photo" class="admin-tab btn">фото</a>
			<a href="/manager/finance/premium-video" class="admin-tab btn">Видео</a>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<form action="" method="post" class="user-finance-form margin-top">
					<table>
						<tr>
							<td><label>С:</label></td>
							<td><input type="date" name="date-from" value="<?=$dateFrom;?>"></td>
						</tr>
						<tr>
							<td><label>по:</label></td>
							<td><input type="date" name="date-to" value="<?=$dateTo;?>"></td>
						</tr>
					</table>
					<input type="hidden" name="page_name" value="chats">
					<input id="limit" type="hidden" value="<?=$limit;?>">
					<input type="submit" value="поиск" class="btn margin-top">
				</form>
				<table class="finance-chat-table margin-top">
					<thead>
						<th>дата</th>
						<th>инициатор чата</th>
						<th>с кем</th>
						<th>длительность</th>
						<th>стоимость</th>
					</thead>
					<tbody>
						<?=$this->render('/finance/table-finance-chats.php',['users_finance_info'=>$usersFinanceInfo,'type'=>'full_table'])?>
					</tbody>
				</table>
				<div class="admin-finance-pagination pagination">
					<?php if(isset($usersFinanceInfo->chats->chatArray) && !empty($usersFinanceInfo->chats->chatArray)){
						echo $this->render('/layouts/parts/pagination.php',['letters_count'=>$count,'page'=>$page,'limit'=>$limit,'page_name'=>'chats']);
					} ?>
				</div>
			</div>
		</div>
	</div>
</div>
