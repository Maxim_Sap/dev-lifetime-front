<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Финансы: Письма</h2>
		    <a href="/manager/finance/letters" class="active btn admin-tab">Письма</a>
		    <a href="/manager/finance/chats" class="admin-tab btn">Чат</a>
		    <a href="/manager/finance/video-chats" class="btn admin-tab">Видео чат</a>
		    <a href="/manager/finance/gifts" class="admin-tab btn">Подарки</a>
			<a href="/manager/finance/premium-photo" class="admin-tab btn">фото</a>
			<a href="/manager/finance/premium-video" class="admin-tab btn">Видео</a>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<form action="" method="post" class="user-finance-form margin-top">
			        <table>
			            <tr>
			                <td><label>С:</label></td>
			                <td><input type="date" name="date-from" value="<?=$dateFrom;?>"></td>
			            </tr>
			            <tr>
			                <td><label>по:</label></td>
			                <td><input type="date" name="date-to" value="<?=$dateTo;?>"></td>
			            </tr>
			        </table>
					<input type="hidden" name="page_name" value="letters">
					<input id="limit" type="hidden" value="<?=$limit;?>">
					<input type="submit" value="поиск" class="btn margin-top">
				</form>
			    <div class="table_scroll_wrap">
				    <table class="finance-letters-table margin-top">
					    <thead>
						    <th>дата</th>
						    <th>от кого</th>
						    <th>кому</th>
						    <th>стоимость</th>
					    </thead>
					    <tbody>                                
						    <?=$this->render('/finance/table-finance-letters.php',['users_finance_info'=>$usersFinanceInfo,'type'=>'full_table'])?>            
					    </tbody>
				    </table>
			    </div>
				<div class="col-xs-12 col-sm-6 admin-finance-pagination">
					<?php if(isset($usersFinanceInfo->letters->lettersArray) && !empty($usersFinanceInfo->letters->lettersArray)){
						echo $this->render('/layouts/parts/pagination.php',['letters_count'=>$count,'page'=>$page,'limit'=>$limit,'page_name'=>'letters']);
					} ?>
				</div>			
			</div>
		</div>
	</div>
</div>