<?php 
	//var_dump($users_finance_info);
	$users_finance_info = (object) $users_finance_info;
	$users_finance_info->chats = (object) $users_finance_info->chats;
	if(!empty($users_finance_info->chats->chatArray)){
		$all_duration = 0;
		$all_amount = 0;
		foreach($users_finance_info->chats->chatArray as $chat){
			$chat = (object)$chat;
			$all_duration += (int)$chat->duration;
			$all_amount += (float)$chat->amount;?>
		<tr>
			<td><?=$chat->created_at;?></td>
			<td>
				<a href="/manager/user/<?=$chat->otherUserID?>" class="table_link"><?=$chat->first_name?></a> [ID: <?=$chat->otherUserID?>]
			</td>
			<?php if(isset($type) && $type == 'full_table'){?>
				<td>
					<a href="/manager/finance/user-info/<?=$chat->action_receiver?>" class="table_link"><?=$chat->to_name?></a> [ID: <?=$chat->action_receiver?>]
				</td>
				<?php } ?>
			<td><?=date("H:i:s", mktime(0, 0, $chat->duration));?></td>
			<td><?= (float)$chat->amount;?> cr</td>
		</tr>
		<?php }?>
	<tr><td colspan='<?=(isset($type) && $type == 'full_table') ? 3 : 2;?>'><b>Total:</b></td><td><b><?=date("H:i:s", mktime(0, 0, $all_duration));?></b></td><td><b><?=$all_amount;?> cr</b></td></tr>
	<?php }else{?>
	<tr><td colspan='<?=(isset($type) && $type == 'full_table') ? 5 : 4;?>'>no chats</td></tr>
	<?php } ?>