<?php
	use app\modules\manager\controllers\ManagerController;
	$session = Yii::$app->session;
//var_dump($usersFinanceInfo);die;
?>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Финансовая информация</h2>
			<a href="/manager/finance/letters" class="btn">Обобщенная финансовая информация</a>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<input id="otherUserID" type="hidden" value="<?=$otherUserID;?>">
					<form action="" method="post" class="user-finance-form margin-top">
			        <table>
			            <tr>
			                <td><label>Пользователь:</label></td>
			                <td>
			                <?php if($session['user_type'] == ManagerController::USER_SUPERADMIN){?>
			                    <label>ID: <?=$otherUserID;?></label>
			                    <input name="userID_to" type="hidden" value="<?=$otherUserID;?>" />
			                <?php }else{?>
			                    <select name="userID_to" class="my-users"></select>
			                <?php }?>
			                </td>
			            </tr>
			            <tr>
			                <td><label>С:</label></td>
			                <td><input type="date" name="date-from" value="<?=$dateFrom;?>"></td>
			            </tr>
			            <tr>
			                <td><label>по:</label></td>
			                <td><input type="date" name="date-to" value="<?=$dateTo;?>"></td>
			            </tr>
			        </table>		
					<input id="limit" type="hidden" value="<?=$limit;?>">
			        <br>
					<input type="submit" value="поиск" class="btn margin-bottom">
				</form>             
				<h3>Letters</h3>
			    <div class="table_scroll_wrap" style="max-height: 500px;overflow: auto;">
				    <table class="finance-leters-table">
					    <thead>
						    <th>дата</th>
						    <th>с кем</th>
						    <th>стоимость</th>
					    </thead>
					    <tbody>
						    <?=$this->render('/finance/table-finance-letters.php',['users_finance_info'=>$usersFinanceInfo])?>
					    </tbody>
				    </table> 
			    </div>
				<h3>Chats</h3>
			    <div class="table_scroll_wrap" style="max-height: 500px;overflow: auto;">
				    <table class="finance-chat-table">
					    <thead>                                  
						    <th>дата</th>
						    <th>c кем</th>
						    <th>длительность</th>
						    <th>стоимость</th>
					    </thead>
					    <tbody>                                            
						    <?=$this->render('/finance/table-finance-chats.php',['users_finance_info'=>$usersFinanceInfo])?>
					    </tbody>
				    </table>
			    </div>            
				<h3>Video Chats</h3>
			    <div class="table_scroll_wrap" style="max-height: 500px;overflow: auto;">
				    <table class="finance-video-chat-table">
					    <thead>                                         
						    <th>дата</th>
						    <th>c кем</th>
						    <th>длительность</th>
						    <th>стоимость</th>
					    </thead>
					    <tbody>                                                   
						    <?=$this->render('/finance/table-finance-video-chats.php',['users_finance_info'=>$usersFinanceInfo])?>
					    </tbody>
				    </table> 
			    </div>
			    <h3>Gifts</h3>
			    <div class="table_scroll_wrap" style="max-height: 500px;overflow: auto;">
				    <table class="finance-gifts-table">
					    <thead>
						    <th>дата</th>
						    <th>от кого</th>
						    <th>стоимость</th>
					    </thead>
					    <tbody>
						    <?=$this->render('/finance/table-finance-gift.php',['users_finance_info'=>$usersFinanceInfo])?>
					    </tbody>
				    </table> 
			    </div>
				<h3>Premium Photos</h3>
				<div class="table_scroll_wrap" style="max-height: 500px;overflow: auto;">
					<table class="finance-premium-photos-table">
						<thead>
						<tr>
							<th>дата</th>
							<th>Кто смотрел</th>
							<th>стоимость</th>
						</tr>
						</thead>
						<tbody>
						<?=$this->render('/finance/table-finance-premium-photos.php',['users_finance_info'=>$usersFinanceInfo])?>
						</tbody>
					</table>
				</div>
				<h3>Premium Video</h3>
				<div class="table_scroll_wrap" style="max-height: 500px;overflow: auto;">
					<table class="finance-premium-video-table">
						<thead>
						<tr>
							<th>дата</th>
							<th>Кто смотрел</th>
							<th>стоимость</th>
						</tr>
						</thead>
						<tbody>
						<?=$this->render('/finance/table-finance-premium-video.php',['users_finance_info'=>$usersFinanceInfo])?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<?php

$this->registerJsFile(
    '@web/js/user.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);