<?php
$session = Yii::$app->session;
$ajax = Yii::$app->request->getIsAjax();
	if(!$ajax){	?>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<?php } ?>
                    <?= $this->render('/layouts/parts/girl-filter.php', ['activityStatus' => $activityStatus, 'online' => $online, 'active' => $active, 'notActive' => $notActive, 'userId' => $userId, 'userName' => $userName, 'userAgeFrom' => $userAgeFrom, 'userAgeTo' => $userAgeTo, 'userCity' => $userCity, 'userCountry' => $userCountry, 'sortBy' => $sortBy, 'sortWay' => $sortWay, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo]) ?>
                <?php if (!empty($girlsArray)) { ?>
                <div class="cards-wrapper">
                    <div class="row">
                    <?php foreach ($girlsArray as $girl) { ?>                    
                        <div class="col-xs-12 col-sm-3 col-md-2">   
                            <?=$this->render('/layouts/parts/single-agency-girl.php',['girl'=>(object)$girl])?>
                        </div>
                    <?php }?>               
                <?php }else{ ?>
                    <p>You have not users</p>
                    <a class="btn" href="/manager/add-user">Add new user</a>
                <?php } ?>
                    </div>
                </div>
                <?php if ($count > Yii::$app->params['numberOfItemsOnAdminPage']) {echo $this->render('/layouts/parts/admin_user_pagination.php',['page'=>$page, 'count'=>$count, 'pageName' => 'my-girls']);} ?>
			<?php if(!$ajax){ ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<style>
    .status {
        display: inline-block!important;
        width: 10px;
        height: 10px;        
        border-radius: 50%;
    }
    .online {
        background: green;
    }
    .offline {
        background: red;
    }
</style>

<?php 
    $js = '

    $( document ).ajaxComplete(function( event, xhr, settings ) {
        if (settings.url == "/manager/my-girls") {
            $(\'#content-area input[type="radio"]\').each(function(){
                var $this = $(this),
                $label = $this.siblings( "label[for=" + $this.attr( "id" ) + "]" );
                $this.add($label).wrapAll(\'<div class="radio-group"/>\');
            });

            $(\'.radio-group\').each(function() {
                $(this).append(\'<span class="custom-radio">\');
            });

            $(\'.check-group, .radio-group\').each(function() {
                if($(this).find(\'input\').prev(\'label\').length) {
                    $(this).addClass(\'label-prev\');
                }
            });

            $(\'.check-group, .radio-group\').each(function() {
                if($(this).find("input").next("label").length) {
                    $(this).addClass("label-next");
                }
            });

        }           
    });';

    $this->registerJs($js);

