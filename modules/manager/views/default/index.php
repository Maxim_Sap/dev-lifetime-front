<?php 
	use app\modules\manager\controllers\ManagerController;
    $session = Yii::$app->session;
?>

<?php if($session['user_type'] == ManagerController::USER_SUPERADMIN){?>
	<?php echo $this->render('/finance/total.php',['agencyList'=>$agencyList,'type'=>'index', 'totalFinanceAllPeriod'=>$totalFinanceAllPeriod, 'totalFinanceFromPeriod'=>$totalFinanceFromPeriod]); ?>
<?php } elseif ($session['user_type'] == ManagerController::USER_SITEADMIN) { ?>
	<?php echo $this->render('/default/index-siteadmin.php', ['type' =>'index']); ?>
<?php }elseif(in_array($session['user_type'],[ManagerController::USER_AGENCY, ManagerController::USER_ADMIN])){?>
	<?php echo $this->render('/default/index-agency.php',['session' => $session, 'agencyList'=>$agencyList, 'unrepliedLetters' => $unrepliedLetters, 'unfinishedTasks' => $unfinishedTasks, 'unsendGifts' => $unsendGifts, 'notApprovedChatTemplates' => $notApprovedChatTemplates, 'notApprovedMessageTemplates' => $notApprovedMessageTemplates, 'type'=>'index']); ?>
<?php }elseif(in_array($session['user_type'],[ManagerController::USER_INTERPRETER])){?>
	<?php echo $this->render('/default/index-translator.php'); ?>
<?php }else{?>
<div id="content-area">
	<div class="content-area-inner">
		<div class="content-area-inner-header">
			<h2 class="content-title">Главная страница</h2>			
		</div>
		<div class="content-area-inner-body">
			<div class="content-container">

			</div>
		</div>	
	</div>
</div>
<?php } ?>
