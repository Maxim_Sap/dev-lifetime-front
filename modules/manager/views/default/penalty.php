<?php
	use app\modules\manager\controllers\ManagerController;
	$session = Yii::$app->session;
	$ajax = Yii::$app->request->getIsAjax();

?>
<?php if(!$ajax){?>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Штрафы</h2>			
			<?php if($session['user_type'] == ManagerController::USER_SUPERADMIN){?>
				<a href="/manager/add-penalty" class="btn margin-top">Добавить штраф</a>
			<?php } ?>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<form action="" method="post" class="user-penalty-form margin-bottom">
					<table>
						<?php if($session['user_type'] == ManagerController::USER_SUPERADMIN){?>
						<tr>
							<td><label>Агенство: </label></td>
							<td>
								<select id="agency-id">
									<option value="">Select agency</option>
									<?php if(!empty($agencyArray)){
										foreach($agencyArray as $agency){?>
											<option value="<?=$agency->user_id?>"><?=trim($agency->name);?></option>
										<?php }
									} ?>
								</select>
							</td>
							<?php } ?>
						</tr>
						<tr>
							<td>С:</td>
							<td><input type="date" id="dateFrom" value="<?=$dateFrom;?>"></td>
						</tr>
						<tr>
							<td>по:</td>
							<td><input type="date" id="dateTo" value="<?=$dateTo;?>"></td>
						</tr>
					</table>
					<input type="submit" class="btn admin-custom-btn margin-top" value="поиск">
				</form>
      			<div class="penalty-content">
				<?php } ?>
					<h3>Gifts</h3>
					<div class="table_scroll_wrap">
						<table class="finance-leters-table">
							<thead>
								<th>ID</th>
								<th>дата штрафа</th>
								<th>от кого</th>
								<th>кому</th>
								<th>агенство</th>
								<th>сумма</th>
							</thead>
							<tbody>
								<?php if(!empty($penaltyGifts)){
									$total = 0;
									foreach($penaltyGifts as $penaltyGift){
										$total += $penaltyGift->penalty_value;?>
										<tr>
											<td>
												<a href="/manager/view-shop-item/<?=$penaltyGift->action_id?>" title="view detail">
													<?=$penaltyGift->action_id?>
												</a>
											</td>
											<td><?=$penaltyGift->created_at?></td>
											<td>
												<a href="/manager/user/<?=$penaltyGift->user_from_id?>" title="view user detail">
													<?=trim($penaltyGift->user_from_name. " ".$penaltyGift->user_from_last_name);?>
												</a>
											</td>
											<td>
												<a href="/manager/user/<?=$penaltyGift->user_to_id?>" title="view user detail">
													<?=trim($penaltyGift->user_to_name . " ".$penaltyGift->user_to_last_name);?>
												</a>
											</td>
											<td>
												<a href="/manager/user/<?=$penaltyGift->agency_id?>" title="view user detail">
													<?=trim($penaltyGift->agency_name);?>
												</a>
											</td>
											<td><?=$penaltyGift->penalty_value?></td>
										</tr>
									<?php }?>
									<tr>
										<td colspan="5"><strong>Total:</strong></td>
										<td><strong><?=$total;?></strong></td>
									</tr>
								<?php }else{?>
									<tr><td colspan="6"><p>No results</p></td></tr>
								<?php }?>
							</tbody>
						</table>
					</div>
					<h3>Letters</h3>
					<div class="table_scroll_wrap">
						<table class="finance-chat-table">
							<thead>
								<th>ID</th>
								<th>дата штрафа</th>
								<th>от кого</th>
								<th>кому</th>
								<th>агенство</th>
								<th>сумма</th>
							</thead>
							<tbody>
							<?php
							if(!empty($penaltyLetters->lettersArray)){
								$total = 0;
								foreach($penaltyLetters->lettersArray as $letter) {
									$total += $letter->penalty_value;?>
									<tr>
										<td><?=$letter->action_id?></td>
										<td><?=$letter->created_at?></td>
										<td>
											<a href="/manager/user/<?=$letter->from_user_id?>" title="detail info">
												<?=trim($letter->from_name.' '.$letter->from_last_name);?>
											</a>
										</td>
										<td>
											<a href="/manager/user/<?=$letter->to_user_id?>" title="detail info">
												<?=trim($letter->to_name.' '.$letter->to_last_name);?>
											</a>
										</td>
										<td>
											<a href="/manager/user/<?=$letter->agency_user_id?>" title="detail info">
												<?=trim($letter->agency_name.' '.$letter->agency_last_name);?>
											</a>
										</td>
										<td><?=$letter->penalty_value?></td>
									</tr>
								<?php }?>
								<tr>
									<td colspan="5"><strong>Total:</strong></td>
									<td><strong><?=$total;?></strong></td>
								</tr>
							<?php }else{?>
								<tr><td colspan="6"><p>No results</p></td></tr>
							<?php }?>
							</tbody>
						</table>
					</div>
					<h3>Other</h3>
					<div class="table_scroll_wrap">
						<table class="finance-video-chat-table">
							<thead>
								<th>ID</th>
								<th>дата штрафа</th>
								<th>агенство</th>
								<th>Подробнее</th>
								<th>сумма</th>
								<?php if($session['user_type'] == ManagerController::USER_SUPERADMIN){?>
									<th>действие</th>
								<?php }?>
							</thead>
							<tbody>
							<?php if(!empty($otherPenalty)){
								$total = 0;
								foreach($otherPenalty as $penaltyItem){
									$total += $penaltyItem->penalty_value;?>
									<tr>
										<td><?=$penaltyItem->id?></td>
										<td><?=$penaltyItem->created_at?></td>
										<td>
											<a href="/manager/user/<?=$penaltyItem->user_to_id?>">
												<?=trim($penaltyItem->agency_name)?>
											</a>
										</td>
										<td><?=$penaltyItem->description?></td>
										<td><?=$penaltyItem->penalty_value?></td>
										<?php if($session['user_type'] == ManagerController::USER_SUPERADMIN){?>
											<td>
												<a href="/manager/penalty-edit/<?=$penaltyItem->id?>">
													<i class="fa fa-pencil" aria-hidden="true"></i>
												</a>
												<a href="/manager/penalty-delete/<?=$penaltyItem->id?>" onclick="if(confirm('Are you sure do you want delete this penalty?')){return true;}else{return false;}">
													<i class="fa fa-trash" aria-hidden="true"></i>
												</a>
											</td>
										<?php } ?>
									</tr>
								<?php }?>
								<tr>
									<td colspan="4"><strong>Total:</strong></td>
									<td colspan="<?=($session['user_type'] == ManagerController::USER_SUPERADMIN) ? 2 : 1 ;?>"><strong><?=$total;?></strong></td>
								</tr>
							<?php }else{?>
								<tr><td colspan="<?=($session['user_type'] == ManagerController::USER_SUPERADMIN) ? 6 : 5 ;?>"><p>No results</p></td></tr>
							<?php }?>
							</tbody>
						</table>
					</div>
				<?php if(!$ajax){?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>