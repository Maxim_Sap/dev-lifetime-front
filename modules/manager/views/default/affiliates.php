<?php
//var_dump($womans);
?>
<style>
    .card-box {
        min-height: 320px;
    }
</style>
<div id="content-area">
    <div class="content-area-inner">

        <div class="content-area-inner-header">
            <h2 class="content-title"><span class="text-pink">My </span> Affiliates</h2>
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <div class="cards-wrapper">
                    <div class="row">
                        <?php if(!empty($affiliates_mass)){
                            foreach($affiliates_mass as $one){?>
                                <div class="col-xs-12 col-sm-3 col-md-2">
                                    <?=$this->render('/layouts/parts/single-agency.php',['agency'=>$one])?>
                                </div>
                            <?php }?>
                        <?php }else{ ?>
                            <p>You don't have affiliates</p>
                            <a href="/manager/add-user?user_type=5" class="search_inbox_btn btn">Добавить</a>
                        <?php } ?>

                        <div class="admin-agency pagination">
                            <?php if(!empty($affiliates_mass) && $count > $limit){
                                echo $this->render('/layouts/parts/admin_pagination.php',['count'=>$count,'pageName'=>$pageName,'page'=>$page]);
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


