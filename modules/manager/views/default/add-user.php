<?php
use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;
?>
<div id="content-area">
    <div class="content-area-inner">
        
        <div class="content-area-inner-header">
            <h2 class="content-title">Register new <?php 
            if (isset($_GET['user_type']) && $_GET['user_type'] == ManagerController::USER_FEMALE) {
                echo "girl";
            } elseif (isset($_GET['user_type']) && $_GET['user_type'] == ManagerController::USER_ADMIN) {
                echo "admin";
            } elseif (isset($_GET['user_type']) && $_GET['user_type'] == ManagerController::USER_INTERPRETER) {
                echo "interpreter";                
            } else {
                echo "user";
            }
            ?></h2>            
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <form class="admin-register-form">
                    <?php if (in_array($session['user_type'], [ManagerController::USER_AGENCY, ManagerController::USER_ADMIN])) { ?>
                        <input type="hidden" name="user_type" value="<?=$userType?>">
                        <input type="hidden" name="parentUserID" value="<?=(isset($session['agency_id']) ? $session['agency_id'] : '')?>">
                    <?php } elseif($session['user_type'] == ManagerController::USER_SUPERADMIN) { ?>
                        <div id="fake_man_cover" <?=$userType != ManagerController::USER_MALE ? 'hidden' :''?> style="margin-bottom: 10px">
                            <input type="checkbox" id="fake_man" class="margin-bottom">
                            <label for="fake_man">Fake man</label>
                        </div>
                        <label for="user_type_select" style="display: block;margin-bottom: 10px">User type:</label>
                        <select name="user_type" class="margin-bottom">
                            <?php foreach ($userTypes as $type) { ?>
                                <option value="<?= $type->id ?>" <?=($userType == $type->id) ? 'selected' : ""?>><?= $type->title ?></option>    
                            <?php } ?>                            
                        </select>
                        <div id="agency-list-select" <?=(in_array($userType,[ManagerController::USER_MALE,ManagerController::USER_AGENCY,ManagerController::USER_AFFILIATE,ManagerController::USER_SUPERADMIN])) ? 'hidden' : '';?>>
                            <label style="display: block;margin-bottom: 10px">Agency:</label>
                            <select name="parentUserID" class="margin-bottom">
                                <?php if (!empty($agencyArray)) {
                                    foreach ($agencyArray as $agency) { ?>
                                        <option value="<?=$agency->user_id?>"><?=$agency->name?></option>
                                    <?php }
                                } ?>
                            </select>
                        </div>
                    <?php } ?>
                    <input type="text" class="margin-bottom" name="first_name" placeholder="name" required="required">
                    <input type="text" class="margin-bottom" name="email" placeholder="e-mail" required="required">
                    <input type="text" class="margin-bottom" name="password" placeholder="password" required="required">
                    <input type="text" class="margin-bottom" name="conf_password" placeholder="confirm password"
                           required="required">

                    <?php if (isset($_GET['user_type']) && $_GET['user_type'] == ManagerController::USER_ADMIN) { ?>
                        <p>Права администратора</p>
                        <?php $i=0; foreach (Yii::$app->params['admin_permissions'] as $key => $permission) { ?>
                            <input type="hidden" name="<?= $key ?>" value="0" />
                            <input id="permission-<?=$i ?>" type="checkbox" name="<?= $key ?>" <?php if ($permission['value']) {echo "checked"; } ?> value="1"/>
                            <label for="permission-<?=$i ?>"><?= $permission['description'] ?></label><br>
                        <?php $i++; } ?>                       
                    <?php } ?>
                    <?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) { ?>
                        <div class="admin-permissions hidden">
                            <p>Права администратора</p>
                            <?php $i=0; foreach (Yii::$app->params['admin_permissions'] as $key => $permission) { ?>
                            <input type="hidden" name="<?= $key ?>" value="0" />
                            <input id="permission-<?=$i ?>" type="checkbox" name="<?= $key ?>" <?php if ($permission['value']) {echo "checked"; } ?> value="1"/>
                            <label for="permission-<?=$i ?>"><?= $permission['description'] ?></label><br>
                            <?php $i++; } ?>   
                        </div>
                        <div class="siteadmin-permissions hidden">
                        <p>Права администратора сайта</p>
                        <?php $i=0; foreach (Yii::$app->params['siteadmin_permissions'] as $key => $permission) { ?>
                            <input type="hidden" name="<?= $key ?>" value="0" />
                            <input id="permission-<?=$i ?>" type="checkbox" name="<?= $key ?>" <?php if ($permission['value']) {echo "checked"; } ?> value="1"/>
                            <label for="permission-<?=$i ?>"><?= $permission['description'] ?></label><br>
                        <?php $i++; } ?>  
                        </div>                 
                    <?php } ?>
                    <input type="submit" value="Register" class="search_inbox_btn btn">
                </form>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>