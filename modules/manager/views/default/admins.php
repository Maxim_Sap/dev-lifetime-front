<?php $ajax = Yii::$app->request->getIsAjax();
$this->title = 'Site admins';
    if(!$ajax){    ?>

<div id="content-area">
    <div class="content-area-inner">
        
        <div class="content-area-inner-header">
            <h2 class="content-title">Site admins</h2>        
        </div>

        <div class="content-area-inner-body">
            <div class="content-container"> 
                <?php }
                echo $this->render('/layouts/parts/user-filter.php',['statusArray'=>$statusArray,'pageName'=>$pageName,'curr_search_status'=>$curr_search_status]);
                if(!empty($girls)){ ?>
                    <div class="cards-wrapper">
                    <div class="row">
                    <?php foreach($girls as $interpreter) {?>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <?=$this->render('/layouts/parts/siteadmin-single.php',['admin'=>$interpreter])?>
                    </div>
                    <?php }?>
                    </div>
                    </div>

                    <div class="col-xs-12 col-sm-6"><?=$this->render('/layouts/parts/admin_pagination.php',['count'=>$count,'pageName'=>$pageName,'page'=>$page]); ?></div>
                <?php }else{?>
                    <p>You have not site admins</p>
                <?php } 
                if(!$ajax){ ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>

