<?php
use app\modules\manager\controllers\ManagerController;

if (!empty($userData->personalInfo->looking_age)) {
    $lookingAgeArray = explode(',', $userData->personalInfo->looking_age);
    if (isset($lookingAgeArray[1])) {
        $lookingAgeFrom = (int)$lookingAgeArray[0];
        $lookingAgeTo   = (int)$lookingAgeArray[1];
    } else {
        $lookingAgeFrom = (int)$lookingAgeArray[0];
        $lookingAgeTo   = "";
    }
} else {
    $lookingAgeFrom = "";
    $lookingAgeTo   = "";
}
$session = Yii::$app->session;

?>
<style>
.drag-and-drop-area {
        font-size: 18px;
        border: 2px dashed rgba(235, 65, 97, 0.7);
        text-align: center;
        padding: 20px 0;
        line-height: 1.4;
        position: relative;
        margin-bottom: 40px;
    }
.private-note {
    font-size: 12px;
    color: #9a9a9a;
}
#content-area .save_button {
    max-width: 200px;
}
table td:nth-child(2n)  {
    width: 30%!important;
}
table td:nth-child(2n+1)  {
    width: 20%!important;
}
.approved-status {
    position: absolute;
    top: 10px;
    left: 10px;
    padding: 10px;    
    color: white;
}
.approved-status.yes{
    background: green;
}
.approved-status.no {
    background: red;
}
#content-area .btn.large {
    max-width: 300px;
    min-width: 300px;
}
.deactivation-reason {
    color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
}
</style>
<div id="content-area">
    <div class="content-area-inner">
        
        <div class="content-area-inner-header">
            <h2 class="content-title">Edit profile</h2> 
            <?php if (!empty($userData->personalInfo->deactivation_reason)) { ?>
                <div class="deactivation-reason">
                    <div>
                        <?= $userData->personalInfo->deactivation_reason ?>                            
                    </div>
                    <p>Deactivation date/time: <?= $userData->personalInfo->deactivation_time ?></p>
                </div>
            <?php } ?>          
        </div>

            <div class="content-area-inner-body">
                <div class="content-container">
                    <div class="edit_profile_block">
                        <?php if (!in_array($userData->userType,[ManagerController::USER_AGENCY,ManagerController::USER_ADMIN,ManagerController::USER_INTERPRETER])) {?>
                            <div class="edit_profile_avatar fl margin-right" style="position: relative;">
                                <?php $img_original = empty($userData->avatar->origin) ? '/img/no_image.jpg' : $this->context->serverUrl."/".$userData->avatar->origin; ?>
                                <img id="myphoto" data-original-src="<?=$img_original;?>" src="<?=$userData->avatar->normal;?>">
                                <?php if($userData->approve_status == 1 || $userData->approve_status == 2){?>
                                    <div class="approved-status no"><span>not approved</span></div>
                                <?php }elseif($userData->approve_status == 4){?>
                                    <div class="approved-status yes"><span>approved</span></div>
                                <?php }elseif($userData->approve_status == 3){?>
                                    <div class="approved-status decline"><span>decline</span></div>
                                <?php }?>
                            </div>
                            <div class="change_avatar_block">
                                To change your avatar
                                <br>
                                <a id="change_avatar" class="btn margin-top margin-bottom" href="javascript:void(0);"><span
                                        class="change_avatar_btn">click here</span></a>
                                <form id="avatarform" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="MAX_FILE_SIZE" value="12345"/>
                                    <input type="file" name="file" multiple accept="image/*" id="avatar_file" data-album-title="letters"
                                           style="display: none;"/>
                                </form>
                                <a id="crop_photo" class="table_link fl btn" href="javascript:void(0);" onclick="window['CropImageLibrary']['cropPhoto']();">Crop photo</a>
                                <a id="apply_crop_photo" class="table_link fl btn" style="display: none" href="javascript:void(0);" onclick="window['CropImageLibrary']['applyCrop']();">Apply crop photo</a>
                                <br>
                                <input id="photo-id" type="hidden" value="<?=$userData->avatarPhotoID;?>">
                                <input id="x1" type="hidden" value="0">
                                <input id="y1" type="hidden" value="0">
                                <input id="x2" type="hidden" value="150">
                                <input id="y2" type="hidden" value="169">
                                <input id="w" type="hidden" value="150">
                                <input id="h" type="hidden" value="169">
                            </div>
                        <?php }?>
        <div class="edit_pass_block">
            If you need edit your password account, please press:<br>
            <a href="/manager/edit-password/<?=$userData->userID;?>"
               class="btn margin-top margin-bottom"><span class="edit_pass_btn">edit password</span></a>
        </div>
        <div class="clear"></div>
    </div>
    <?php if (in_array($userData->userType, [ManagerController::USER_MALE, ManagerController::USER_FEMALE])) { ?>
    <div class="buttons-after-avatar">        
        <a class="btn large" href="/manager/letters/inbox/<?=$userData->personalInfo->id?>" title="">Просмотреть историю писем</a>
    </div>
    <?php } ?>
    <div class="edit_profile_fields">
        <form class="edit-profile">
            <input id="otherUserID" type="hidden" value="<?=$userData->userID;?>">
            <?php if ($userData->userType == ManagerController::USER_AGENCY) {
                echo $this->render('/layouts/parts/agency-user-card.php', ['userData'=>$userData,'countryList'=>$countryList,'approve_statuses'=>$approve_statuses, 'agencyList' => $agencyList, 'agencyRuleViolations' => $agencyRuleViolations]);
                //for agency edit?>
            <?php } elseif($userData->userType == ManagerController::USER_ADMIN) {
                echo $this->render('/layouts/parts/agency-admin-user-card.php',['userData'=>$userData,'countryList'=>$countryList,'approve_statuses'=>$approve_statuses, 'agencyList' => $agencyList, 'permissions' => $permissions]);
                //agency admin?>            
            <?php } elseif($userData->userType == ManagerController::USER_SITEADMIN) {
                echo $this->render('/layouts/parts/siteadmin-user-card.php',['userData'=>$userData,'countryList'=>$countryList,'approve_statuses'=>$approve_statuses, 'agencyList' => $agencyList, 'permissions' => $permissions]);
                //agency admin?>
            <?php } elseif($userData->userType == ManagerController::USER_INTERPRETER) {
                echo $this->render('/layouts/parts/agency-translator-user-card.php',['userData'=>$userData,'countryList'=>$countryList,'approve_statuses'=>$approve_statuses,'girlsArray'=>$girlsArray]);
                //agency translator?>
            <?php } elseif($userData->userType == ManagerController::USER_AFFILIATE) {
                echo $this->render('/layouts/parts/affiliates-user-card.php',['userData'=>$userData,'countryList'=>$countryList,'approve_statuses'=>$approve_statuses,'girlsArray'=>$girlsArray]);
                //agency translator?>
            <?php } else {
                echo $this->render('/layouts/parts/user-card.php',['userData'=>$userData,'countryList'=>$countryList,'approve_statuses'=>$approve_statuses,'lookingAgeFrom'=>$lookingAgeFrom,'lookingAgeTo'=>$lookingAgeTo, 'hairColors' => $hairColors, 'eyesColors' => $eyesColors, 'physiques' => $physiques]);
                //other user?>
            <?php } ?>
        </form>
    </div>
    <?php if (in_array($userData->userType, [ManagerController::USER_MALE, ManagerController::USER_FEMALE, ManagerController::USER_AGENCY])) { //men, women, agency only?>
        <?php if (!in_array($userData->userType, [ManagerController::USER_AGENCY])) { ?>
            <div style="margin: 10px 0;">
                <a href="/manager/gallery/<?= $userData->userID ?>" class="save_button btn">Go to Albums</a>
            </div>
        <?php } ?>
        <div class="attach_foto_block">
            <h4>Attach your private document photo (Passport)</h4>
            <div id="in_uploaded_img" style="display:none;">0</div>
            <div id="document-attach-photo"></div>
            <div class="row uploader" style="position: relative;">
                <a style="display: none" href="javascript:void(0);" id="save_order">save</a>
                <div class="col-md-12">
                    <div class="drag_and_drop_area drag-and-drop-area inner-padding">
                        <a href="javascript:void(0);" class="main-col-text">Click here or drag&amp;drop photos to upload them</a>
                        <div class="cab_main_photo" id="div_foto_add">
                            <span for="choose" id="link" class="choose"></span>
                            <form id="chooseform" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="MAX_FILE_SIZE" value="12345"/>
                                <input type="file" name="file" multiple accept="image/*" id="choose"
                                       data-album-title="document" data-place="albums" style="display: none;"/>
                            </form>
                        </div>
                        <div>Select a file in this format: gif, jpg, jpeg, png</div>
                        <div>Should not exceed 2 Mb!</div>
                    </div>
                    <div class="private-note">
                        Private gallery photos are not available for public viewing on the website. Thus you can use the
                        private gallery for storing your special photos. You can send a photo
                        from this gallery to ladies you want. They will be seen only by ladies you have chosen. We maintain
                        the confidentiality of your information and not share it!
                    </div>
                </div>
                <span style="height: 183px;    position: absolute;    top: 0px;    width: 100%;    left: 0;"
                      class="drop_here" data-album-title="document" data-place="albums"></span>
                <div class="errors"></div>
                <div id="status1"></div>
            </div>
        </div>
    <?php } ?>
            </div>
        </div>
    </div>
</div>
