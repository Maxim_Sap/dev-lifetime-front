<?php
$session = Yii::$app->session;
$ajax = Yii::$app->request->getIsAjax();
$monthArray = array("1"=>"Январь","2"=>"Февраль","3"=>"Март","4"=>"Апрель","5"=>"Май", "6"=>"Июнь", "7"=>"Июль","8"=>"Август","9"=>"Сентябрь","10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");
for ($i=2015; $i < 2100; $i++) { 
	$yearArray[] = $i;
}

$letters = 0;
$lettersCount = 0;
$chat = 0;
$chatCount = 0;
$videoChat = 0;
$videoChatCount = 0;
$videoTrack = 0;
$videoTrackCount = 0;
$gifts = 0;
$giftsCount = 0;
$watchPhoto = 0;
$watchPhotoCount = 0;
$users_contact = 0;
$users_contact_count = 0;
if(!empty($balance)){
	foreach($balance as $one){
		switch ($one->action_name){
			case 'send present':
				$gifts += $one->amount;
				$giftsCount++;
				break;
			case 'send letter':
				$letters += $one->amount;
				$lettersCount++;
				break;
			case 'chat':
				$chat += $one->amount;
				$chatCount++;
				break;
			case 'watch photo':
				$watchPhoto += $one->amount;
				$watchPhotoCount++;
				break;
			case 'videochat':
				$videoChat += $one->amount;
				$videoChatCount++;
				break;
			case 'watch video':
				$videoTrack += $one->amount;
				$videoTrackCount++;
				break;
		}
	}
}
$penaltyGiftsAmount = 0;
$penaltyGiftsCount = 0;
$penaltyLettersAmount = 0;
$penaltyLettersCount = 0;
$otherPenaltyAmount = 0;
$otherPenaltyCount = 0;

if(!empty($penaltyGifts)){
	foreach($penaltyGifts as $penaltyGift){
		$penaltyGiftsAmount += $penaltyGift->penalty_value;
		$penaltyGiftsCount++;
	}
}
if(!empty($penaltyLetters)){
	foreach($penaltyLetters as $penaltyLetter){
		$penaltyLettersAmount += $penaltyLetter->penalty_value;
		$penaltyLettersCount++;
	}
}
if(!empty($otherPenalty)){
	foreach($otherPenalty as $penalty){
		$otherPenaltyAmount += $penalty->penalty_value;
		$otherPenaltyCount++;
	}
}
if(!$ajax){ ?>
<style>
	p{
		font-size: 18px;
	}
</style>

<div id="content-area">
	<div class="content-area-inner">		
		<div class="content-area-inner-header">
			<h2 class="content-title">Баланс</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<h3>Период:</h3>
			<form action="" method="post" class="user-balance-form margin-bottom">
				<table>
					<tr>
						<td>месяц</td>
						<td>
							<select id="month">
								<?php foreach ($monthArray as $key=>$one){?>
									<option value="<?=$key?>" <?=($key == $month) ? 'selected': ''?>><?=$one;?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>год</td>
						<td>
							<select id="year">
								<?php foreach ($yearArray as $one){?>
									<option value="<?=$one?>" <?=($one == $year) ? 'selected': ''?>><?=$one;?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
				</table>
				<input type="submit" class="btn admin-custom-btn margin-top" value="поиск">
			</form>

			<div class="balance-content">
		<?php } ?>
		<p>Поступления за <strong><?=$monthArray[(int)$month]." ".$year?>:</strong></p>
	    <table class="finance-leters-table">
		    <thead>
			    <th>Описание</th>
			    <th>Количество</th>
			    <th>Сумма</th>
		    </thead>
		    <tbody>
			    <tr>
					<td>Письма</td>
					<td><?=$lettersCount;?></td>
					<td><?=$letters;?></td>
				</tr>
				<tr>
					<td>Чат</td>
					<td><?=$chatCount;?></td>
					<td><?=$chat;?></td>
				</tr>
				<tr>
					<td>Видео Чат</td>
					<td><?=$videoChatCount;?></td>
					<td><?=$videoChat;?></td>
				</tr>
				<tr>
					<td>Просмотр фотографий</td>
					<td><?=$watchPhotoCount;?></td>
					<td><?=$watchPhoto;?></td>
				</tr>
				<tr>
					<td>Просмотр видеороликов</td>
					<td><?=$videoTrackCount;?></td>
					<td><?=$videoTrack;?></td>
				</tr>
				<tr>
					<td>Подарки</td>
					<td><?=$giftsCount;?></td>
					<td><?=$gifts;?></td>
				</tr>
				<tr>
					<td>Контактные данные</td>
					<td><?=$users_contact_count;?></td>
					<td><?=$users_contact;?></td>
				</tr>
				<tr>
					<td colspan="2"><strong>Итого:</strong></td>
					<td><strong><?=$total_bonus = $letters+$chat+$videoChat+$videoTrack+$gifts+$users_contact + $watchPhoto?></strong></td>
				</tr>
		    </tbody>
	    </table>
		<p>Штрафы за <strong><?=$monthArray[(int)$month]." ".$year?>:</strong></p>
		<table class="finance-leters-table">
			<thead>
			<th>Описание</th>
			<th>Количество дней</th>
			<th>Сумма</th>
			</thead>
			<tbody>
			<tr>
				<td>Просроченые письма</td>
				<td><?=$penaltyLettersCount;?></td>
				<td><?=$penaltyLettersAmount;?></td>
			</tr>
			<tr>
				<td>Просроченые подарки</td>
				<td><?=$penaltyGiftsCount;?></td>
				<td><?=$penaltyGiftsAmount;?></td>
			</tr>
			<tr>
				<td>Другие штрафы</td>
				<td><?=$otherPenaltyCount;?></td>
				<td><?=$otherPenaltyAmount;?></td>
			</tr>
			<tr>
				<td colspan="2"><strong>Итого:</strong></td>
				<td><strong><?=$total_panalty = $penaltyLettersAmount+$penaltyGiftsAmount+$otherPenaltyAmount?></strong></td>
			</tr>
			</tbody>
		</table>
		<p>Итоговый баланс:</p>
		<table class="finance-leters-table">
			<thead>
			<th colspan="2">Период</th>
			<th>Сумма</th>
			</thead>
			<tbody>
				<tr>
					<td colspan="2"><strong><?=($monthArray[(int)$month]." ".$year);?></strong></td>
					<td style="text-align: center"><strong><?=($total_bonus - $total_panalty)?></strong></td>
				</tr>
			</tbody>
		</table>
		<?php if(!$ajax){ ?>
    </div>
			</div>
		</div>
	</div>
</div>

<?php } ?>