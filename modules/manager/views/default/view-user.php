<?php
use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;

if($userData->user_type == ManagerController::USER_AGENCY){
    echo $this->render('/default/view-agency.php', ['activityStatus' => $activityStatus, 'online' => $online, 'active' => $active, 'notActive' => $notActive, 'userId' => $userId, 'userName' => $userName, 'userAgeTo' => $userAgeTo, 'userAgeFrom' => $userAgeFrom, 'userCity' => $userCity, 'userCountry' => $userCountry, 'userData'=>$userData, 'girlsArray' => $girlsArray, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo, 'sortBy' => $sortBy, 'sortWay' => $sortWay]);
}elseif($userData->user_type == ManagerController::USER_INTERPRETER) {
    echo $this->render('/default/view-interpreters.php', ['userData'=>$userData]);
}elseif($userData->user_type == ManagerController::USER_AFFILIATE) {
    echo $this->render('/default/view-affiliates.php', ['userData'=>$userData]);
} elseif ($userData->user_type == ManagerController::USER_ADMIN) {
    echo $this->render('/default/view-admin.php', ['userData'=>$userData]);
} elseif ($userData->user_type == ManagerController::USER_SITEADMIN) {
    echo $this->render('/default/view-siteadmin.php', ['userData'=>$userData]);
}else{
    if (!empty($userData->personalInfo->birthday)) {
        $birthday = new \DateTime($userData->personalInfo->birthday);
        $tz  = new \DateTimeZone('Europe/Brussels');
        $age = \DateTime::createFromFormat('Y-m-d', $userData->personalInfo->birthday, $tz)
             ->diff(new \DateTime('now', $tz))
             ->y;
        // zodiak
        $signs = [
                    "Capricorn",
                    "Aquarius",
                    "Pisces",
                    "Aries",
                    "Taurus",
                    "Gemini",
                    "Cancer",
                    "Leo",
                    "Virgo",
                    "Libra",
                    "Scorpio",
                    "Sagittarius"
                ];
        $signsstart = array(1=>21, 2=>20, 3=>20, 4=>20, 5=>20, 6=>20, 7=>21, 8=>22, 9=>23, 10=>23, 11=>23, 12=>23);
        //$zodiak = $birthday->format('d') < $signsstart[$birthday->format('m') + 1] ? $signs[$birthday->format('m') - 1] : $signs[$birthday->format('m') % 12];
        $zodiak = (int)$birthday->format('d') < $signsstart[(int)$birthday->format('m')] ? $signs[(int)$birthday->format('m') - 1] : $signs[(int)$birthday->format('m') % 12];
    } else {
        $age = "Not defined";
        $zodiak = "Not defined";
    }
    if (!empty($userData->personalInfo->weight)) {
        $weight = $userData->personalInfo->weight." kg (" . round($userData->personalInfo->weight/0.45359237)." lbs)";
    } else {
        $weight = "Not defined";
    }

    if (!empty($userData->personalInfo->height)) {
        $heightInMeters = sprintf("%.2f", $userData->personalInfo->height/100);
        $heightInFeet = $userData->personalInfo->height*0.03280839895013123;
        $heightFeet = (int)$heightInFeet; // truncate the float to an integer
        $heightInches = round(($heightInFeet-$heightFeet)*12);

        if ($heightInches == 0) {
            $height = "$heightInMeters m ($heightFeet')";
        } else {
            $height = "$heightInMeters m ($heightFeet' $heightInches'')";
        }
    } else {
        $height = "Not defined";
    }

    if (!empty($userData->location)) {
        $city = $userData->city;
        $country = $userData->location;
    } else {
        $user_city = "Not defined";
        $user_country = "Not defined";
    }


    $maritalArray = ['Single','Married','Divorced','Widowed','Complicated'];
    if ($userData->personalInfo->marital != null && isset($maritalArray[$userData->personalInfo->marital-1])) {
        $marital = $maritalArray[$userData->personalInfo->marital-1];
    } else {
        $marital = 'Not defined';
    }

    if ($userData->personalInfo->eyes != null) {
        $eyes = $userData->personalInfo->eyes;
    } else {
        $eyes = 'Not defined';
    }

    if ($userData->personalInfo->hair != null) {
        $hair = $userData->personalInfo->hair;
    } else {
        $hair = 'Not defined';
    }

    if (isset($userData->personalInfo->children)) {
        $children = $userData->personalInfo->children;
        if ($children == 0) {
            $children = "no";
        }
    } else {
        $children = 'Not defined';
    }

    $smokingArray = ['Yes','No','Casual','Heavy'];
    if ($userData->personalInfo->smoking != null && isset($smokingArray[$userData->personalInfo->smoking-1])) {
        $smoking = $smokingArray[$userData->personalInfo->smoking-1];
    } else {
        $smoking = 'Not defined';
    }

    switch ($userData->personalInfo->religion) {
        case 1:
            $religion = 'Catholicity';
            break;
        case 2:
            $religion = 'Christianity';
            break;
        case 3:
            $religion = 'Ateism';
            break;
        case 4:
            $religion = 'Islam';
            break;
        case 5:
            $religion = 'Hinduism';
            break;
        case 6:
            $religion = 'Buddhism';
            break;
        case 7:
            $religion = 'Folk';
            break;
        case 8:
            $religion = 'Judaism';
            break;
        case 9:
            $religion = 'Orthodox';
            break;
        case 10:
            $religion = 'Other';
            break;
        default:
            $religion = '---';
    }

    ?>

    <style>
    .user_avatar span.offline{
        background-color: #e94902;
    }
    #content-area .btn.large {
        min-width: 300px;
        max-width: 300px;
    }
    .buttons-after-avatar {
        margin-top: 20px;
    }
    .deactivation-reason {
        color: #a94442;
        background-color: #f2dede;
        border-color: #ebccd1;
        padding: 15px;
        margin-bottom: 20px;
        border: 1px solid transparent;
        border-radius: 4px;
    }
    </style>

    <div id="content-area">
        <div class="content-area-inner">

            <div class="content-area-inner-header">
                <h2 class="content-title"><?=trim($userData->personalInfo->first_name." ".$userData->personalInfo->last_name);?></h2>
                <?php if (!empty($userData->personalInfo->deactivation_reason)) { ?>
                    <div class="deactivation-reason">
                        <div>
                            <?= $userData->personalInfo->deactivation_reason ?>                            
                        </div>
                        <p>Deactivation date/time: <?= $userData->personalInfo->deactivation_time ?></p>
                    </div>
                <?php } ?>
            </div>

            <div class="content-area-inner-body">
                <div class="content-container">
                    <div class="user_avatar margin-bottom">
                        <div>
                            <a href="/manager/edit-user/<?=$userData->personalInfo->id?>" title="редактировать профиль">
                                <img src="<?=$userData->avatar->normal;?>">
                            </a>
                        </div>
                        <?php if (in_array($userData->user_type, [ManagerController::USER_MALE, ManagerController::USER_FEMALE])) { ?>
                        <div class="buttons-after-avatar">
                            <a class="btn large" href="/manager/edit-user/<?=$userData->personalInfo->id?>" title="редактировать профиль">Редактировать профиль</a> <br>
                            <a class="btn large" href="/manager/letters/inbox/<?=$userData->personalInfo->id?>" title="">Просмотреть историю писем</a>
                        </div>
                        <?php } ?>
                    </div>
                    <br>
                    <div class="profile_information_block">
                    <table border="1">
                        <tr>
                            <td>ID:</td>
                            <td><span class="user_profile_id"> <?=$userData->personalInfo->id;?></span></td>
                            <td>AGE</td>
                            <td><span class="detail_profile_info"><?=$age;?></span></td>
                        </tr>
                        <tr>
                            <td>Date of birth</td>
                            <td><span class="detail_profile_info"><?=empty($userData->personalInfo->birthday) ? "---": $userData->personalInfo->birthday;?></span></td>
                            <td>Country</td>
                            <td><span class="detail_profile_info country"><?=$country;?></span></td>
                        </tr>
                        <tr>
                            <td>City</td>
                            <td><span class="detail_profile_info"><?=$city;?></span></td>
                            <td>Hair</td>
                            <td><span class="detail_profile_info"><?=$hair;?></span></td>
                        </tr>
                        <tr>
                            <td>Eyes</td>
                            <td><span class="detail_profile_info date_of"><?=$eyes;?></span></td>
                            <td>Weight</td>
                            <td><span class="detail_profile_info"><?=$weight;?></span></td>
                        </tr>
                        <tr>
                            <td>Height</td>
                            <td><span class="detail_profile_info date_of"><?=$height;?></span></td>
                            <td>Education</td>
                            <td><span class="detail_profile_info"><?=empty($userData->personalInfo->education) ? "---": $userData->personalInfo->education;?></span></td>
                        </tr>
                        <tr>
                            <td>Occupation</td>
                            <td><span class="detail_profile_info"><?=empty($userData->personalInfo->occupation) ? "---": $user_data->personalInfo->occupation;?></span></td>
                            <td>Zodiac</td>
                            <td><span class="detail_profile_info"><?=$zodiak;?></span></td>
                        </tr>
                        <tr>
                            <td>Religion</td>
                            <td><span class="detail_profile_info"><?=$religion;?></span></td>
                            <td>Marital status</td>
                            <td><span class="detail_profile_info"><?=$marital;?></span></td>
                        </tr>
                        <tr>
                            <td>Number of children</td>
                            <td><span class="detail_profile_info"><?=empty($userData->personalInfo->children) ? "---": $userData->personalInfo->children;?></span></td>
                            <td>Smoking</td>
                            <td><span class="detail_profile_info"><?=$smoking;?></span></td>
                        </tr>
                        <?php if (in_array($session['user_type'], [ManagerController::USER_AGENCY, ManagerController::USER_ADMIN, ManagerController::USER_SUPERADMIN])) { ?>
                        <tr>
                            <td>Phone</td>
                            <td><span class="detail_profile_info">
                                    <?=(!empty($userData->personalInfo->phone) && $userData->personalInfo->phone != '') ? "<a title='позвонить' href='skype:".$userData->personalInfo->phone."'>".$userData->personalInfo->phone."</a>" : '---';?>
                                </span></td>
                            <td>Email</td>
                            <td><span class="detail_profile_info"><a href="mailto:<?=$userData->email?>" title="написать"><?=$userData->email?></span></td>
                        </tr>
                        <tr>
                            <td>Mob phone</td>
                            <td><span class="detail_profile_info">
                                    <?=(!empty($userData->personalInfo->m_phone) && $userData->personalInfo->m_phone != '') ? "<a title='позвонить' href='skype:".$userData->personalInfo->m_phone."'>".$userData->personalInfo->m_phone."</a>" : '---';?>
                                </span></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php } ?>
                    </table>


                    <h3>Short about me</h3>
                    <p>
                    <?=empty($userData->personalInfo->about_me) ? "---": $userData->personalInfo->about_me;?>
                    </p>
                    <hr>
                    <h3>my hobbies</h3>
                    <p>
                    <?=empty($userData->personalInfo->hobbies) ? "---": $userData->personalInfo->hobbies;?>
                    </p>
                    <hr>

                    <h3>my ideal relationships</h3>
                    <p>
                    <?=empty($userData->personalInfo->my_ideal) ? "---": $userData->personalInfo->my_ideal;?>
                    </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>