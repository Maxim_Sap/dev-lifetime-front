<?php
use app\modules\manager\controllers\ManagerController;

$session       = Yii::$app->session;
$name          = trim($userData->first_name);
?>
<style>
    .user_avatar span.offline {
        background-color: #e94902;
    }
</style>

<div id="content-area">
    <div class="content-area-inner">

        <div class="content-area-inner-header">
            <h2 class="content-title"><?=$name?></h2>
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <div class="user_avatar margin-bottom">
                    <a href="/manager/edit-user/<?=$userData->userID?>" title="редактировать профиль">
                        <img src="<?=$userData->avatar->normal;?>">
                    </a>
                    <br>  <br>  
                    <a href="/manager/edit-user/<?=$userData->userID?>" class="btn">Edit</a>
                </div>
                <br>
                <div class="profile_information_block">
                    <table class="margin-top margin-bottom">
                        <tr>
                            <td>User ID:</td>
                            <td><span><?=$userData->userID;?></span></td>
                            <td>Email</td>
                            <td><?=$userData->email;?></td>
                        </tr>
                        <tr>
                            <td>First Name</td>
                            <td><?=$name?></td>
                            <td>Last Name</td>
                            <td><?=trim($userData->last_name);?></td>
                        </tr>
                        <tr>
                            <td>Sex</td>
                            <td><?= ($userData->sex == 2) ? "Female" : "Male" ?></td>
                            <td>Права администратора</td>
                            <td>
                                <?php foreach ($userData->permissions as $access => $permission) { ?>
                                    <p><?= $permission['description'] ?> - <?= ($permission['value'] == ManagerController::ALLOW) ? "Да" : "Нет" ?></p>
                                <?php } ?>
                            </td>
                        </tr>
                    </table>
                    <?php
                    $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved</span>';
                    switch ($userData->approve_status) {
                        case 1:
                            $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved</span>';
                            break;
                        case 2:
                            $approve_status = '<span style="color: lawngreen; margin-left: 10px;font-weight: bold;">Approve in progress</span>';
                            break;
                        case 3:
                            $approve_status = '<span style="color: #ffcc00; margin-left: 10px;font-weight: bold;">Approve delcine</span>';
                            break;
                        case 4:
                            $approve_status = '<span style="color: #ffcc00; margin-left: 10px;font-weight: bold;">Approved</span>';
                            break;
                        default:
                            $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved</span>';
                            break;
                    }
                    echo $approve_status; ?>

                </div>
            </div>
        </div>
    </div>
</div>
