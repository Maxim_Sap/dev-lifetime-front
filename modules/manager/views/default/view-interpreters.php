<?php
use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;
?>
    <style>
        .user_avatar span.offline{
            background-color: #e94902;
        }
    </style>

    <div id="content-area">
        <div class="content-area-inner">

            <div class="content-area-inner-header">
                <h2 class="content-title"><?=trim($userData->personalInfo->first_name." ".$userData->personalInfo->last_name);?></h2>
            </div>

            <div class="content-area-inner-body">
                <div class="content-container">
                    <div class="user_avatar margin-bottom">
                        <a href="/manager/edit-user/<?=$userData->personalInfo->id?>" title="редактировать профиль">
                            <img src="<?=$userData->avatar->normal;?>">
                        </a>
                    </div>
                    <br>
                    <div class="profile_information_block">
                        <table border="1">
                            <tr>
                                <td>ID:</td>
                                <td><span class="user_profile_id"> <?=$userData->personalInfo->id;?></span></td>
                                <td>Email</td>
                                <td><span class="detail_profile_info"><a href="mailto:<?=$userData->email?>" title="написать"><?=$userData->email?></span></td>
                            </tr>
                            <?php if (in_array($session['user_type'], [ManagerController::USER_ADMIN, ManagerController::USER_AGENCY, ManagerController::USER_SUPERADMIN])) { ?>
                                <tr>
                                    <td>Phone</td>
                                    <td>
                                        <span class="detail_profile_info">
                                            <?=(!empty($userData->personalInfo->phone) && $userData->personalInfo->phone != '') ? "<a title='позвонить' href='skype:".$userData->personalInfo->phone."'>".$userData->personalInfo->phone."</a>" : '---';?>
                                        </span>
                                    </td>
                                    <td>Mob phone</td>
                                    <td>
                                        <span class="detail_profile_info">
                                            <?=(!empty($userData->personalInfo->m_phone) && $userData->personalInfo->m_phone != '') ? "<a title='позвонить' href='skype:".$userData->personalInfo->m_phone."'>".$userData->personalInfo->m_phone."</a>" : '---';?>
                                        </span>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>