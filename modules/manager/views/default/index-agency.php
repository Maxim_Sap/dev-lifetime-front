<?php use app\modules\manager\controllers\ManagerController; ?>
<div id="content-area">
	<div class="content-area-inner">
		<?php if ($unrepliedLetters + $unfinishedTasks + $unsendGifts + $notApprovedChatTemplates + $notApprovedMessageTemplates > 0) {?>
		<div class="content-area-inner-header">
			<h2 class="content-title">Уведомления:</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<?php if ($unrepliedLetters) { ?>
				<?php if ($session['user_type'] == ManagerController::USER_AGENCY || (isset($session['permissions']) && $session['permissions']['letter_access'] == ManagerController::ALLOW)) { ?>
				<div class="alert alert-danger">
					У вашего агенства <?= $unrepliedLetters ?> <?php if ($unrepliedLetters > 1) {echo "неотвеченных писем"; } else {echo "неотвеченное письмо"; } ?> от мужчин
				</div>
				<?php } ?>
				<?php } ?>
				<?php if ($unfinishedTasks) { ?>
				<?php if ($session['user_type'] == ManagerController::USER_AGENCY || (isset($session['permissions']) && $session['permissions']['tasks_access'] == ManagerController::ALLOW)) { ?>
				<div class="alert alert-danger">
					У вашего агенства <?= $unfinishedTasks ?> <?php if ($unfinishedTasks > 1) {echo "невыполненных задач"; } else {echo "невыполненная задача"; } ?>
				</div>
				<?php } ?>
				<?php } ?>
				<?php if ($unsendGifts) {?>
				<?php if ($session['user_type'] == ManagerController::USER_AGENCY || (isset($session['permissions']) && $session['permissions']['gift_access'] == ManagerController::ALLOW)) { ?>
				<div class="alert alert-danger">				
					У вашего агенства <?= $unsendGifts ?> <?php if ($unsendGifts > 1 && $unsendGifts < 5) {echo "недоставленных подарка"; } elseif ($unsendGifts >= 5) {echo "недоставленных подарков";} else {echo "недоставленный подарок"; } ?>
				</div>
				<?php } ?>
				<?php } ?>
				<?php if ($notApprovedChatTemplates) { ?>
				<div class="alert alert-danger">
					У вашего агенства <?= $notApprovedChatTemplates ?> <?php if ($notApprovedChatTemplates > 1 && $notApprovedChatTemplates < 5) {echo "неутвержденных шаблона"; } elseif ($notApprovedChatTemplates >= 5) {echo "неутвержденных шаблонов";} else {echo "неутвержденный шаблон"; } ?> для чата
				</div>
				<?php } ?>
				<?php if ($notApprovedMessageTemplates) { ?>
				<div class="alert alert-danger">
					У вашего агенства <?= $notApprovedMessageTemplates ?> <?php if ($notApprovedMessageTemplates > 1 && $notApprovedMessageTemplates < 5) {echo "неутвержденных шаблона"; } elseif ($notApprovedMessageTemplates >= 5) {echo "неутвержденных шаблонов";} else {echo "неутвержденный шаблон"; } ?> для писем
				</div>
				<?php } ?>							
			</div>
		</div>
		<?php } ?>
		<?php if ($session['user_type'] == ManagerController::USER_AGENCY || (isset($session['permissions']) && $session['permissions']['finance_access'] == ManagerController::ALLOW)) { ?>
		<div class="content-area-inner-header">
			<h2 class="content-title">Финансовая информация по агенству за текущий месяц:</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<table class="agency-finance-table">
					<thead>
					<th>наименование</th>
					<th>количество</th>
					<th>сумма</th>
					</thead>
					<tbody>
					<tr>
						<td>письма</td>
						<td><?php if (!empty($agencyList)) { echo $agencyList->letters->count; } ?></td>
						<td><?php if (!empty($agencyList)) { echo round($agencyList->letters->amount, 2); } ?> cr</td>
					</tr>
					<tr>
						<td>чат</td>
						<td><?php if (!empty($agencyList)) { echo date("H:i:s", mktime(0, 0, (int)$agencyList->chats->duration)); } ?></td>
						<td><?php if (!empty($agencyList)) { echo round($agencyList->chats->amount, 2); } ?> cr</td>
					</tr>
					<tr>
						<td>видео чат</td>
						<td><?php if (!empty($agencyList)) { echo date("H:i:s", mktime(0, 0, (int)$agencyList->videoChats->duration)); } ?></td>
						<td><?php if (!empty($agencyList)) { echo round($agencyList->videoChats->amount,2); } ?> cr</td>
					</tr>
					<tr>
					<tr>
						<td>подарки</td>
						<td><?php if (!empty($agencyList)) { echo $agencyList->gifts->count; } ?></td>
						<td><?php if (!empty($agencyList)) { echo round($agencyList->gifts->amount, 2); } ?> cr</td>
					</tr>
					<td colspan="2"><strong>Итого:</strong></td>
					<td><strong><?php if (!empty($agencyList)) { echo $agencyList->letters->amount + $agencyList->chats->amount + $agencyList->videoChats->amount + $agencyList->gifts->amount; } ?> cr</strong></td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
