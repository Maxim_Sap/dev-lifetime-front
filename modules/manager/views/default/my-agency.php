<?php
	//var_dump($womans);
?>
<style>
	.card-box {
		min-height: 320px;
	}
</style>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title"><span class="text-pink">My </span> Agency</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
			<div class="cards-wrapper">
				<div class="row">
				<?php if(!empty($agencyArray)){
				foreach($agencyArray as $agency){ ?>
				<div class="col-xs-12 col-sm-3 col-md-2">	
					<?=$this->render('/layouts/parts/single-agency.php',['agency'=>$agency])?>
				</div>
				<?php }?>				
				<?php }else{ ?>
					<p>You have not agency</p>
				<?php } ?>
				
				<div class="admin-agency pagination">
					<?php if(!empty($agencyAgency) && $count > $limit){
						  echo $this->render('/layouts/parts/admin_pagination.php',['count'=>$count,'pageName'=>$pageName,'page'=>$page]);
					} ?>
				</div> 
				</div>
			</div>
			</div>
		</div>
	</div>
</div>

	
