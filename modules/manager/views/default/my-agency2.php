<?php
use app\modules\manager\controllers\ManagerController;
use yii\helpers\Url;
//var_dump($agencyArray);die;
//var_dump($sortBy);
?>
<style>
    .card-box {
        min-height: 320px;
    }
    table th{
        padding-left: 16px!important;
    }
    table th span.sort{
        border: 4px solid transparent;
        position: absolute;
        right: 5px;
        top:22px;
    }
    table th span.sort.up{
        border-bottom: 11px solid #004580;
    }
    table th span.sort.down{
        border-top: 11px solid #004580;
    }
    .agency-sort-list input,.agency-sort-list select{
        margin: 0;
        padding: 0 0 0 5px;
        height: 24px;
    }
    .agency-sort-list .table-link{
        font-weight: bold;
        cursor: pointer;
    }
</style>
<div id="content-area">
    <div class="content-area-inner">

        <div class="content-area-inner-header">
            <h2 class="content-title"><span class="text-pink">My </span> Agencies</h2>
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <div class="cards-wrapper" style="width: 100%;overflow: auto;">
                        <h2>Фильтр:</h2>
                        <form action="/manager/my-agency" method="post" class="agency-sort-list-form">
                            <table style="width: 400px">
                                <tr>
                                    <td><label>С:</label></td>
                                    <td><input type="date" name="dateFrom" value="<?=$dateFrom;?>"></td>
                                </tr>
                                <tr>
                                    <td><label>по:</label></td>
                                    <td><input type="date" name="dateTo" value="<?=$dateTo;?>"></td>
                                </tr>
                                <input type="hidden" id="sortBy" name="sortBy" value="">
                                <input type="hidden" id="sortWay" name="sortWay" value="">
                            </table>
                            <input type="submit" value="применить" class="btn admin-custom-btn margin-top margin-bottom">
                        </form>
                        <table class="agency-sort-list table-autosort" style="width: auto">
                            <thead>
<!--                            <tr>-->
<!--                                <th>ID</th>-->
<!--                                <th>Статус</th>-->
<!--                                <th data-sort="title">Название<span class="sort --><?//=($sortBy == 'title') ? $sortWay : '';?><!--"></span></th>-->
<!--                                <th data-sort="profile-count">Анкет<span class="sort --><?//=($sortBy == 'profile-count') ? $sortWay : '';?><!--"></span></th>-->
<!--                                <th data-sort="letters">Писем<span class="sort --><?//=($sortBy == 'letters') ? $sortWay : '';?><!--"></span></th>-->
<!--                                <th data-sort="chat">Чат<span class="sort --><?//=($sortBy == 'chat') ? $sortWay : '';?><!--"></span></th>-->
<!--                                <th data-sort="video-chat">Видео чат<span class="sort --><?//=($sortBy == 'video-chat') ? $sortWay : '';?><!--"></span></th>-->
<!--                                <th data-sort="watch-video">Просмотр видеороликов<span class="sort --><?//=($sortBy == 'watch-video') ? $sortWay : '';?><!--"></span></th>-->
<!--                                <th data-sort="watch-photo">Просмотр фотографий<span class="sort --><?//=($sortBy == 'watch-photo') ? $sortWay : '';?><!--"></span></th>-->
<!--                                <th data-sort="gifts">Подарки<span class="sort --><?//=($sortBy == 'gifts') ? $sortWay : '';?><!--"></span></th>-->
<!--                                <th data-sort="money">Заработано денег<span class="sort --><?//=($sortBy == 'money') ? $sortWay : '';?><!--"></span></th>-->
<!--                            </tr>-->
                            <tr>
                                <th class="table-sortable:numeric table-sortable">ID</th>
                                <th class="table-sortable:default table-sortable">Статус</th>
                                <th class="table-sortable:default table-sortable">Название</th>
                                <th class="table-sortable:default table-sortable table-filterable">Страна</th>
                                <th class="table-sortable:default table-sortable">Город</th>
                                <th class="table-sortable:numeric table-sortable">Анкет</th>
                                <th class="table-sortable:numeric table-sortable">Писем></th>
                                <th class="table-sortable:default table-sortable">Чат</th>
                                <th class="table-sortable:default table-sortable">Видео чат</th>
                                <th class="table-sortable:numeric table-sortable">Премиум видео</th>
                                <th class="table-sortable:numeric table-sortable">Премиум фото</th>
                                <th class="table-sortable:numeric table-sortable">Подарки</th>
                                <th class="table-sortable:numeric table-sortable">Заработано денег</th>
                            </tr>
                            <tr>
                                <th class=""><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
                                <th class=""><select onchange="Table.filter(this,this)"><option value="">All</option><option value="Active">Active</option><option value="Not Active">Not Active</option></select></th>
                                <th class=""><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
                                <th class=""><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
                                <th class=""><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
                                <th class=""></th>
                                <th class=""></th>
                                <th class=""></th>
                                <th class=""></th>
                                <th class=""></th>
                                <th class=""></th>
                                <th class=""></th>
                                <th class=""></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($agencyArray)){
                                $allTotal = 0;
                                foreach($agencyArray as $agency){
                                    $totalAmount = (float)$agency->letters_amount + (float)$agency->chat_amount + (float)$agency->video_chat_amount + (float)$agency->gifts_amount + (float)$agency->watch_video_amount + (float)$agency->watch_photo_amount;
                                    $allTotal += $totalAmount;
                                    ?>
                                        <tr>
                                            <td><?=$agency->id?></td>
                                            <td><?=($agency->status == ManagerController::STATUS_ACTIVE) ? 'Active' : 'Not Active'?></td>
                                            <td class="table-link" data-ulr="/manager/user/<?=$agency->agency_user_id?>"><?=$agency->name;?>
                                                <?php if (!empty($agency->ruleViolations)) { ?>
                                                <div class="rule-violations">
                                                    <?php foreach ($agency->ruleViolations as $violation) { ?>
                                                           <a href="<?= Url::to(['/manager/rule-violation/view', 'ruleViolationID' => $violation->id]); ?>"><i class="fa fa-star"></i></a>
                                                <?php } ?>
                                                </div>
                                                <?php } ?>                                                
                                            </td>
                                            <td class="table-filterable"><?=(!empty($agency->country) ? $agency->country : 'Not set');?></td>
                                            <td><?=(!empty($agency->city) ? $agency->city : 'Not set');?></td>
                                            <td><?=(!empty($agency->profile_count) ? $agency->profile_count : 0);?></td>
                                            <td><?=(!empty($agency->letters_count) ? $agency->letters_count : 0);?></td>
                                            <td><?=date("H:i:s", mktime(0, 0, (int)$agency->chat_duration));?></td>
                                            <td><?=date("H:i:s", mktime(0, 0, (int)$agency->video_chat_duration));?></td>
                                            <td><?=(!empty($agency->watch_video_count) ? $agency->watch_video_count : 0);?></td>
                                            <td><?=(!empty($agency->watch_photo_count) ? $agency->watch_photo_count : 0);?></td>
                                            <td><?=(!empty($agency->gifts_count) ? $agency->gifts_count : 0);?></td>
                                            <td><strong><?=$totalAmount;?> cr</strong></td>
                                        </tr>
                                <?php }?>
                            <?php }else{ ?>
                                <tr>
                                    <td colspan="10"><p>You don't have agencies</p></td>
                                </tr>
                            <?php } ?>

                            </tbody>
                        </table>
                        <div class="admin-agency pagination">
                            <?php if(!empty($agencyAgency) && $count > $limit){
                                echo $this->render('/layouts/parts/admin_pagination.php',['count'=>$count,'pageName'=>$pageName,'page'=>$page]);
                            } ?>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$js = '$(".agency-sort-list .table-link").click(function(){
    var url = $(this).attr("data-ulr");
    if(typeof(url) != "undefined" && url != ""){
        location.href = url;
    }
});
 
';
$this->registerJs($js, Yii\web\View::POS_READY);
?>