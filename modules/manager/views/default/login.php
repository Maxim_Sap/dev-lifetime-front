<?php
    $apiServer = $this->context->serverUrl;
?>
<div id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog admin_form rounded_block shadow_block border_block" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <span class="log_in_title">Sign in to get in touch</span>
                <span class="error-message-text"></span>
                <form action="" class="login-form">
                    <input class="admin_input" type="email" placeholder="E-mail" name="login" required="required">
                    <input class="admin_input" type="password" placeholder="Password" name="password" required="required">
                    <input type="submit" value="Log in" class="btn btn-wide log_in_btn"> 
                </form>
                <a href="/manager/forgot">Forgot password?</a>
            </div>
            <input type="hidden" id="api_server" value="<?=$apiServer?>">
        </div>
    </div>
</div>