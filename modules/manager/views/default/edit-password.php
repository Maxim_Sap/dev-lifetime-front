<div id="content-area">
	<div class="content-area-inner">		
		<div class="content-area-inner-header">
			<h2 class="content-title">Edit password</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<form class="edit-password">
			    	<div class="field_column margin-bottom">
			            <div class="sm_left fl">Your password:</div>
			            <input type="password" name="oldPassword" class="form-control" required>
			        </div>
		            <div class="field_column margin-bottom">
			            <div class="sm_left fl">New password:</div>
			            <input type="password" name="newPassword" class="form-control" required>
			        </div>
			        <div class="field_column margin-bottom">
			            <div class="sm_left fl">Confirm password:</div>
			            <input type="password" name="confirmPassword" class="form-control" required> 
			        </div>
			        <div class="text_areas">
			            <div class="sm_left fl"></div>
			            <button  class="save_button search_inbox_btn btn margin-top">Save</button>
			        </div>
			        <input type="hidden" name="otherUserID" value="<?=$otherUserID;?>">
			    </form> 
			</div>
		</div>
	</div>
</div>



<section class="admin-content page_editpass edit-pass">       
    <h2>Edit password</h2>   
    <div class="edit_profile_fields">
	    
</section>
