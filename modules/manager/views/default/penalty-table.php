<?php
	$session = Yii::$app->session;
?>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Таблица штрафов</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
			    <form action="/manager/penalty-table" method="post" class="update-penalty-value">
				<table class="finance-leters-table" style="width: inherit;">
					<thead>
						<th>ID</th>
						<th>Тип</th>
						<th>сумма за 1 день просрочки</th>
					</thead>
					<tbody>
						<?php if(!empty($penaltyPricelist)){
							foreach($penaltyPricelist as $pricelist){?>
								<tr>
									<td>
										<?=$pricelist->id?>
									</td>
									<td>
										<?php
										 switch ($pricelist->type_id){
											 case 1:
												 $type_name = 'letters';
												 break;
											 case 2:
												 $type_name = 'gifts';
												 break;
											 default:
												 $type_name = $pricelist->type_id;
										 }
										echo $type_name; ?>
									</td>
									<td class="edit">
										<input type="text" name="<?=$pricelist->type_id?>[price]" class="custom-border" value="<?=$pricelist->price;?>" required>
									</td>
								</tr>
							<?php }
						}else{?>
							<tr><td colspan="6"><p>No results</p></td></tr>
						<?php }?>
					</tbody>
				</table>
				<?php if(!empty($penaltyPricelist)){?>
					<button class="save_button btn margin-top">update</button>
				<?php } ?>
				</form>
			</div>
		</div>
	</div>
</div>
