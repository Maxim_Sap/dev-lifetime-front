<?php 
    $session = Yii::$app->session;
    //пока заранее задан один скин 
	$skinList = [['code'=>'skin_1','skin_id'=>1]];
	$planList = [['name'=>'plan_1','plan_id'=>1]];
?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Price List</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<div class="">				    
					<?php if($session['user_type'] == 6){
				//		для админа
						if(!empty($pricelist)){?>
								<table class="price margin-bottom">
									<thead>
										<tr>
											<th>Agency</th>
											<th>Skin Id</th>
											<th>Plan Id</th>
											<th>Action</th>
											<th>Date From</th>
											<th>Date To</th>
											<th>Price</th>
											<th></th>
										</tr>
									</thead>
									<tbody id="price-list">
										<?php
										//var_dump($price_list);
											foreach($pricelist as $pricelistItem){ if($pricelistItem->action_type_id == 4){continue;}?>
												<form method="post" action="/manager/agency-pricelist">
													<tr>
														<td class="">
															<input value="<?=(!empty($pricelistItem->name) ? trim($pricelistItem->name) : "");?>" placeholder="all agency" disabled>
															<input type="hidden" name="otherUserID" value="<?=$pricelistItem->user_id;?>">
														</td>
														<td>
															<select name="skinID" <?php if($pricelistItem->important == 1){echo "disabled";} ?>>
																<?php  if(!empty($skinList)){
																	foreach($skinList as $items){
																		$items = (object) $items; ?>
																		<option value="<?=$items->skin_id?>" <?php if($items->skin_id == $pricelistItem->skin_id){echo " selected";}?>><?=$items->code?></option>
																	<?php }
																}?>
															</select>
														</td>
														<td>
															<select name="planID" <?php if($pricelistItem->important == 1){echo "disabled";} ?>>
																<?php  if(!empty($planList)){
																	foreach($planList as $items){
																		$items = (object) $items; ?>
																		<option value="<?=$items->plan_id?>" <?php if($items->plan_id == $pricelistItem->plan_id){echo " selected";}?>><?=$items->name?></option>
																	<?php }
																}?>
															</select>
														</td>
														<td>
															<select name="actionTypeID" <?php if($pricelistItem->important == 1){echo "disabled";} ?>>
																<?php if(!empty($actionTypeList)){
																	foreach($actionTypeList as $items){ if(in_array($items->id,[4])){continue;}?>
																		<option value="<?=$items->id?>" <?php if($items->id == $pricelistItem->action_type_id){echo " selected";}?>><?=$items->name?></option>
																	<?php }
																}?>
															</select>
														</td>
														<td><input type="date" name="startDate" value="<?=(!empty($pricelistItem->start_date)) ? date("Y-m-d",strtotime($pricelistItem->start_date)) : "";?>" <?php if($pricelistItem->important == 1){echo "disabled";} ?>></td>
														<?php if($pricelistItem->important == 1){?>
															<input type="hidden" name="startDate" value="<?=(!empty($pricelistItem->start_date)) ? date("Y-m-d",strtotime($pricelistItem->start_date)) : date("Y-m-d",time());?>">
														<?php } ?>
														<td><input type="date" name="stopDate"  value="<?=(!empty($pricelistItem->stop_date)) ? date("Y-m-d",strtotime($pricelistItem->stop_date)) : "";?>" <?php if($pricelistItem->important == 1){echo "disabled";} ?>></td>
														<td><input type="text" name="price" value="<?=$pricelistItem->price?>"></td>
														<td>
															<input type="hidden" name="pricelistID" value="<?=$pricelistItem->id?>">
															<input type="submit" value="update" class="btn ">
															<?php if($pricelistItem->important == 0){ ?>
															<br><br>
																<a href="/manager/delete-agency-price-item/<?=$pricelistItem->id?>" class="delete_link btn" title="delete" onclick="if(!confirm('Are you sure?')) return false;">delete</a>
															<?php } ?>
														</td>
													</tr>
												</form>
											<?php } ?>
									</tbody>
								</table>
							</form>
						<?php }else{?>
							<p>No pricelist items</p>
						<?php } ?>
						<h3>Add new price line</h3>
						<form method="post" action="/manager/agency-pricelist">
							<table class="price">
								<thead>
									<tr>
										<th>Agency</th>
										<th>Skin Id</th>
										<th>Plan Id</th>
										<th>Action</th>
										<th>Date From</th>
										<th>Date To</th>
										<th>Price</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<select name="otherUserID">
												<?php  if(!empty($agencyInfoArray)){
													foreach($agencyInfoArray as $items){
														$items = (object) $items; ?>
														<option value="<?=$items->user_id?>"><?=trim($items->name)?></option>
													<?php }
												}?>
											</select>
										</td>
										<td>
											<select name="skinID">
												<?php  if(!empty($skinList)){
													foreach($skinList as $items){
														$items = (object) $items; ?>
														<option value="<?=$items->skin_id?>" <?php if($items->skin_id == $pricelistItem->skin_id){echo " selected";}?>><?=$items->code?></option>
													<?php }
												}?>
											</select>
										</td>
										<td>
											<select name="planID">
												<?php  if(!empty($planList)){
													foreach($planList as $items){
														$items = (object) $items; ?>
														<option value="<?=$items->plan_id?>" <?php if($items->plan_id == $pricelistItem->plan_id){echo " selected";}?>><?=$items->name?></option>
													<?php }
												}?>
											</select>
										</td>
										<td><select name="actionTypeID">
												<?php if(!empty($actionTypeList)){
													foreach($actionTypeList as $actionType){if(in_array($actionType->id,[4])){continue;}?>
														<option value="<?=$actionType->id?>"><?=$actionType->name?></option>
													<?php }
												}?>
											</select>
										</td>
										<td><input type="date" name="startDate" value="<?=date("Y-m-d",time());?>"></td>
										<td><input type="date" name="stopDate" value=""></td>
										<td><input type="text" name="price" style="width: 100px" required></td>
										<td><input type="submit" value="add" class="btn"></td>
									</tr>
								</tbody>
							</table>
						</form>
					<?php }else{?>
							<table class="price margin-bottom">
								<thead>
									<tr>
										<th>Action</th>
										<th>Price</th>
									</tr>
								</thead>
								<tbody id="price-list">
									<tr><td>Letters</td><td><?=$priceLetter?></td></tr>
									<tr><td>Video chat (1 minute)</td><td><?=$priceVideoChat?></td></tr>
									<tr><td>Chat (1 minute)</td><td><?=$priceChat?></td></tr>
									<tr><td>Watch girls video</td><td><?=$priceWatchVideo?></td></tr>
								</tbody>
							</table>
					<?php } ?>
				</div>				
			</div>
		</div>
	</div>
</div>

