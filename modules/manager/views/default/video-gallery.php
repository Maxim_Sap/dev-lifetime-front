<?php
	use app\modules\manager\controllers\ManagerController;
	use yii\helpers\Html;
	$apiServer = $this->context->serverUrl;
	$session = Yii::$app->session;
?>
<style>
.drag-and-drop-area {
	    font-size: 18px;
	    border: 2px dashed rgba(235, 65, 97, 0.7);
	    text-align: center;
	    padding: 20px 0;
	    line-height: 1.4;
	    position: relative;
	    margin-bottom: 40px;
	}
.private-note {
    font-size: 12px;
    color: #9a9a9a;
}
.approved-status {
    position: absolute;
    top: 10px;
    left: 10px;
    padding: 10px;    
    color: white;
}
.approved-status.yes{
    background: green;
}
.approved-status.no {
    background: red;
}
</style>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title"><?php if (!empty($userInfo)) {?>
			<a href="/manager/user/<?=$otherUserID?>"><?=trim($userInfo->first_name.' '.$userInfo->last_name)." [ID: $otherUserID]";?></a> >
		<?php } ?>
			Video gallery</h2>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<input type="hidden" id="otherUserID" value="<?=$otherUserID;?>">
				<div class="video-content row">
					<?php if (!empty($videoList)) {
						foreach ($videoList as $video) { ?>
							<div class="item_video col-md-12">						
								<div class="col-md-6">
									<div style="float: left;" id="video_id_<?=$video->id;?>">Loading the player...</div>
									<div class="video-data fl-l" style="padding: 0px 20px">						
										<img src="<?= (!empty($video->medium_thumb)) ? $apiServer . '/' . $video->medium_thumb : '' ?>" alt="">					
										<p>To change video poster</p>
										<a id="change_poster-<?=$video->id?>" class="admin-custom-btn margin-top margin-bottom" href="javascript:void(0);"><span class="btn">click here</span></a>
							            <form id="posterform-<?=$video->id?>" method="post" enctype="multipart/form-data">
							                <input type="hidden" name="MAX_FILE_SIZE" value="12345"/>
							                <input type="file" name="file" multiple accept="image/*" id="poster_file-<?=$video->id?>" data-album-title="poster" data-video-id="<?=$video->id?>"
							                       style="display: none;"/>
							            </form>
									</div>
								</div>
								<?php if (in_array($session['user_type'], [ManagerController::USER_AGENCY, ManagerController::USER_ADMIN])) {
									if ($video->status == 4) {?>
										<div class="approved-status yes">
											<span>video approved</span>
										</div>
									<?php } elseif($video->status == 1 || $video->status == 2) { ?>
										<div class="approved-status no">
											<span>video not approved</span>
										</div>
									<?php } elseif($video->status == 3) { ?>
										<div class="approved-status decline">
											<span>video decline</span>
										</div>
									<?php }
								}?>											
								<div class="video-data fl-l col-md-6">
									<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) {?>
										<a class="remove_btn btn" href="javascript:void(0);">Remove
											<i class="fa fa-trash" aria-hidden="true"></i>
										</a>
										<a href="<?=$apiServer . "/" . $video->path;?>" class="download_btn btn" download title="download video">Download<i class="fa fa-download" aria-hidden="true"></i>
										</a>
									<?php } ?>
									<textarea placeholder="Description"><?= Html::decode($video->description) ?></textarea>
									<?php if (in_array($session['user_type'], [ManagerController::USER_AGENCY, ManagerController::USER_ADMIN, ManagerController::USER_SUPERADMIN])) { ?>
									<div class="approve-block">
									<select class="public" style="height: 30px;width: 100%">
										<option <?php if($video->public == 0){echo "selected";}?> value="0">Not active</option>
										<option <?php if($video->public == 1){echo "selected";}?> value="1">Active</option>
									</select>
									</div>
									<div class="approve-block">
										<select class="premium" style="height: 30px;width: 100%">
											<option <?php if($video->premium == 0){echo "selected";}?> value="0">free</option>
											<option <?php if($video->premium == 1){echo "selected";}?> value="1">premium</option>
										</select>
									</div>	
									<?php } ?>							
									<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) {?>
										<div class="approve-block">
											<select class="approve" style="height: 30px;width: 100%">
												<option <?php if($video->status == 1){echo "selected";}?> value="1">not approved</option>
												<option <?php if($video->status == 2){echo "selected";}?> value="2">in process</option>										
												<option <?php if($video->status == 4){echo "selected";}?> value="4">approved</option>
												<option <?php if($video->status == 3){echo "selected";}?> value="3">decline</option>
											</select>
										</div>
									<?php } ?>
									<button class="btn" data-video-id="<?=$video->id?>">save</button>
								</div>
							</div>
							<div class="clear"></div>
						<?php }
					}else{?>
						<p>No result found</p>
					<?php } ?>
				</div>
				<div class="row">
					<div class="row">
						<div class="attach_foto_block">
							<div id="in_uploaded_img" style="display:none;">0</div>
							<div id="document-attach-photo"></div>
							<div class="row uploader" style="position: relative;">
								<a style="display: none" href="javascript:void(0);" id="save_order">save</a>
								<div class="col-md-12">
									<div class="drag_and_drop_area drag-and-drop-area inner-padding">

										<a href="javascript:void(0);" class="main-col-text">Click here or drag&amp;drop video to
											upload them to your gallery</a>

										<div class="cab_main_photo" id="div_foto_add">
											<span for="choose" id="link" class="choose"></span>
											<form id="choosevideoform" method="post" enctype="multipart/form-data">
												<input type="hidden" name="MAX_FILE_SIZE" value="12345"/>
												<input type="file" name="file" multiple accept="video/*" id="choosevideo"
													   style="display: none;"/>
											</form>
										</div>
										<div>Select a file in this format: avi, mp4, flv</div>
										<div>Should not exceed 20 Mb!</div>
									</div>
									<div class="private-note">
										Private gallery video are not available for public viewing on the website. Thus you
										can use the private gallery for storing your special video. We maintain the confidentiality of your information and not share it!
									</div>
								</div>
		                            <span style="height: 110px;    position: absolute;    top: 0px;    width: 100%;    left: 0;"
										  class="video_drop_here"></span>
								<div class="errors"></div>
								<div id="status1"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
