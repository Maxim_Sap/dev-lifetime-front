<?php
    use app\modules\manager\controllers\ManagerController;
    $ajax = Yii::$app->request->getIsAjax();
    $session = Yii::$app->session;  
    $this->title = 'Log page';
?>

<div id="content-area">
    <div class="content-area-inner">
        <input type="hidden" id="log-items" value="<?= $numberOfLogItems ?>">
        <div class="content-area-inner-header">
            <h2 class="content-title">История операций</h2>            
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <form action="" method="post" id="log-search-form" class="margin-bottom">
                    <table>
                        <tr>
                            <td><label>ID пользователя:</label></td>
                            <td><input type="text" id="other-user-id"></td>
                        </tr>
                        <tr>
                            <td><label>Тип пользователей: </label></td>
                            <td>
                                <select id="user-type">
                                    <?php if($session['user_type'] == ManagerController::USER_SUPERADMIN) {?>
                                        <?php foreach ($userTypes as $userType) {?>
                                            <option value="<?= $userType->id?>" <?php if ($userType->id == ManagerController::USER_AGENCY) echo "selected" ?>><?=$userType->title?></option>
                                        <?php } ?>                                                    
                                    <?php } elseif (in_array($session['user_type'], [ManagerController::USER_AGENCY, ManagerController::USER_ADMIN])) {?>
                                            <?php foreach ($userTypes as $userType) {?>
                                                <?php if (in_array($userType->id, [ManagerController::USER_FEMALE, ManagerController::USER_AGENCY, ManagerController::USER_INTERPRETER, ManagerController::USER_ADMIN])) {?>
                                                <option value="<?= $userType->id?>" <?php if ($userType->id == ManagerController::USER_AGENCY) echo "selected" ?>><?=$userType->title?></option>
                                                <?php } ?>
                                            <?php } ?> 
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>c:</td>
                            <td><input type="date" id="date-from" value="<?=date('Y-m-d',time());?>"></td>
                        </tr>
                        <tr>
                            <td>по:</td>
                            <td><input type="date" id="date-to" value="<?=date('Y-m-d',time());?>"></td>
                        </tr>
                    </table>
                    <input type="submit" class="btn margin-top" value="поиск">
                </form>
                <div>
                    <table class="log-table">
                        <thead>
                            <th>User ID</th>
                            <th>User Type</th>
                            <th>Действие</th>
                            <th>дата</th>
                        </thead>
                        <tbody>
                            <?php if (isset($logArray) && !empty($logArray)) {
                                foreach($logArray as $log){ $log = (object)$log;  ?>
                                    <tr>
                                        <td><a href="/manager/user/<?=$log->user_id;?>"><?=$log->user_id;?></a></td>
                                        <td><?=$log->title ?></td>
                                        <td><?=$log->description?></td>
                                        <td><?=$log->created_at;?></td>
                                    </tr>
                                <?php }
                            } else { ?>
                            <tr><td align="center" colspan="4">No data</td></tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php if(isset($logArray) && !empty($logArray) && $count > $numberOfLogItems){                
                        echo $this->render('/layouts/parts/pagination.php',['letters_count'=>$count,'page'=>1,'page_name'=>'log','limit'=>$numberOfLogItems]);
                    } ?>
                </div>
            </div>
        </div>
    </div>
</div>
