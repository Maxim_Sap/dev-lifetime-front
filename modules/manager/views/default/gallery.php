<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2>
			<?php if (!empty($albums)) {?>
				<a href="/manager/user/<?=$otherUserID?>"><?=trim($albums[0]->first_name.' '.$albums[0]->last_name)." [ID: $otherUserID]";?></a> >
			<?php } ?>
				photo albums
			</h2>		
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<input type="hidden" id="otherUserID" value="<?=$otherUserID;?>">
		        <div >
		            <!--<div class="new_album">
		                <span>Create new album</span>
		                <form class="add-new-album">
		                    <input type="text" name="title" placeholder="Album title" class="advanced_input margin-top margin-bottom fl margin-right" required>
		                    <input type="submit" value="Create" class="search_inbox_btn admin-custom-btn margin-top margin-bottom">
		                </form>
		            </div>  -->          
		            <?php if(!empty($albums)){
						foreach($albums as $album) {
							$img = empty($album->medium_thumb) ? '/img/no_image.jpg' : $this->context->serverUrl."/".$album->medium_thumb;
							?>
							<div class="album_wrap fl admin-custom-border col-md-3 col-xs-12">
		                        <div class="album_cover">
								<a href="/manager/album/<?=$album->id;?>/<?=$otherUserID?>" class="foto_bg">
									<img src="<?=$img;?>" alt="">
						        </a>
		                        </div>
						        <div class="foto_foot">
						            <a href="/manager/album/<?=$album->id;?>/<?=$otherUserID?>" class="album_name table_link"><?=($album->title == 'default' ? 'avatar-album (invisible)' : $album->title);?></a>
						            <div><?php
						            $album->count_photos = !empty($album->count_photos) ? $album->count_photos : 0;
						            echo $album->count_photos;?>
						             photos</div>
						        </div>
						    </div>     
						<?php }
		            }else{?>
						<p>No albums</p>
		            <?php } ?>  
		        </div>  
			</div>
		</div>
	</div>
</div>
