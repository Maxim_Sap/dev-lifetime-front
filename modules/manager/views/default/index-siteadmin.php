<?php $js_label = "";
      $js_data = "";
      $ajax = Yii::$app->request->getIsAjax();
?>
<div id="content-area">
	<div class="content-area-inner">		
		<div class="content-area-inner-header">
			<?php if(isset($type) && $type == 'index'){?>
			<h2>Рабочий стол админа</h2>
			<div class="admin-content">
				<table style="width: 800px; text-align: center">
					<thead>
						<tr>
							<th>Общее количество мужчин</th>
							<th>Из них онлайн</th>
							<th>Общее количество агенств</th>
							<th>Общее количество девочек</th>
							<th>Из них онлайн</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="total-men"></td>
							<td id="men-online"></td>
							<td id="total-agency"></td>
							<td id="total-girls"></td>
							<td id="girls-online"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<?php
			$js_script = 'window["adminAccountLibrary"]["getTotalPeopleCount"]();';
			$this->registerJs($js_script, Yii\web\View::POS_READY);	?>	
			<?php } ?>			
		</div>

	</div>
</div>

