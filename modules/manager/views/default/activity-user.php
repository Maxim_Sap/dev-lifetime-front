<?php
    use app\modules\manager\controllers\ManagerController;
	$session = Yii::$app->session;
?>
<div id="content-area">
    <div class="content-area-inner">
        
        <div class="content-area-inner-header">
            <h2 class="content-title">Страница активности</h2>            
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <input id="other_userID" type="hidden" value="<?=$otherUserID;?>">
                <input id="limit" type="hidden" value="<?=$limit;?>">
                <form action="" method="post" class="user-activity-form">
                    <table>
                        <tr>
                            <td><label>Пользователь:</label></td>
                            <td>
                            <?php if($session['user_type'] == ManagerController::USER_SUPERADMIN){?>
                                    <label>ID: <?=$otherUserID;?></label>
                                    <input name="user_id_to" type="hidden" value="<?=$otherUserID;?>" />
                            <?php }else{?>
                                <select name="user_type" id="type">
                                    <option value="" selected>Все</option>
                                    <option value="men">Мальчики</option>
                                    <option value="girls">Девочки</option>
                                    <?php if ($session['user_type'] == ManagerController::USER_ADMIN || $session['user_type'] == ManagerController::USER_AGENCY) { ?>
                                        <option value="translators">Переводчики</option>
                                        <option value="agency">Агенство</option>
                                        <option value="admin">Админы</option>
                                    <?php } ?>
                                </select>
                                <select name="user_id_to" class="my-users"></select>
                            <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><label>С:</label></td>
                            <td><input type="date" name="date-from" value="<?=$dateFrom;?>"></td>
                        </tr>
                        <tr>
                            <td><label>по:</label></td>
                            <td><input type="date" name="date-to" value="<?=$dateTo;?>"></td>
                        </tr>
                    </table>
                    <input type="submit" value="поиск" class="btn margin-bottom margin-top">
                </form>
                <table class="activity-table">
                    <thead>
                    <?php if (!empty($usersActivityInfo[0]->first_name)) { ?>
                        <th>имя</th>
                    <?php } ?>
                        <th>дата входа</th>
                        <th>дата выхода</th>
                        <th>время активности</th>
                        <th>время активности камеры</th>
                    </thead>
                    <tbody>
                        <?php echo $this->render('/layouts/parts/table-activity.php',['usersActivityInfo'=>$usersActivityInfo]);?>
                    </tbody>
                </table>
                <div class="admin-activity-pagination col-xs-12 col-md-6">
                    <?php if(isset($usersActivityInfo) && !empty($usersActivityInfo)){
                        echo $this->render('/layouts/parts/pagination.php',['letters_count'=>$count,'page'=>$page,'limit'=>$limit,'page_name'=>'activity-page']);
                        } ?>
                </div>                
            </div>
        </div>
    </div>
</div>
