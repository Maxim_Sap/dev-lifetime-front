<?php $ajax = Yii::$app->request->getIsAjax();
    if(!$ajax){    ?>

<div id="content-area">
    <div class="content-area-inner">
        
        <div class="content-area-inner-header">
            <h2 class="content-title">Users</h2>    
            <div>
                <label>Агенство:</label>
                <select id="agency-select1" data-user-type="girls" style="width: 350px">
                    <?php if(!empty($agencyArray)){
                        foreach($agencyArray as $agency){?>
                            <option value="<?=$agency->id?>"><?= $agency->name; ?></option>
                    <?php }
                    } ?>
                </select>
            </div>      
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <?php }
                echo $this->render('/layouts/parts/user-filter.php',['statusArray'=>$statusArray,'pageName'=>$pageName,'curr_search_status'=>$curr_search_status]);
                if(!empty($girls)){ ?>
                    <div class="cards-wrapper">
                    <div class="row">
                    <?php foreach($girls as $girl) {?>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <?=$this->render('/layouts/parts/single-girl.php',['girl'=>$girl,'show_token_button'=>$show_token_button])?>
                    </div>
                    <?php }?>
                    </div>
                    </div>

                    <div class="col-xs-12 col-sm-6"><?=$this->render('/layouts/parts/admin_pagination.php',['count'=>$count,'pageName'=>$pageName,'page'=>$page]); ?></div>
                <?php }else{?>
                    <p>Agency have not user</p>
                <?php } 
                if(!$ajax){ ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>

