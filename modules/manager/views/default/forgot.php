<?php
    $apiServer = $this->context->serverUrl;
?>
<!-- Modal FORGOT PASWORD-->
<div id="modalForgotPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body admin_form rounded_block shadow_block border_block">
                <span class="modal-message-text forgot_title">Enter your e-mail below<br>and we will send you reset instructions!</span>
                <span class="error-message-text"></span>
                <form action="">
                    <input class="admin_input" type="email" placeholder="E-mail" name="email" required="required">
                    <input type="submit" value="Send" class="btn btn-wide log_in_btn"> 
                </form>
                <span class="forgot_foot">Remembered? <a href="/manager/login" class="text-green">Sign in</a></span>
                <input type="hidden" id="api_server" value="<?=$apiServer?>">
            </div>
        </div>
    </div>
</div>