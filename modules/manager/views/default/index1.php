<?php 
	use app\modules\manager\controllers\ManagerController;
    $session = Yii::$app->session;
?>
<div class=" main_page">
<?php if($session['user_type'] == ManagerController::USER_SUPERADMIN){?>
	<?php //echo $this->render('/finance/total.php',['agencyList'=>$agencyList,'type'=>'index']); ?>
<?php }elseif(in_array($session['user_type'],[ManagerController::USER_AGENCY, ManagerController::USER_ADMIN])){?>
	<?php echo $this->render('/default/index-agency.php',['agencyList'=>$agencyList,'type'=>'index']); ?>
<?php }elseif(in_array($session['user_type'],[ManagerController::USER_INTERPRETER])){?>
	<?php echo $this->render('/default/index-translator.php'); ?>
<?php }else{?>
	<p>главная страница</p>
<?php } ?>
</div>