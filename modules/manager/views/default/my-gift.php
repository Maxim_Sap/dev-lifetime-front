<?php
	use app\modules\manager\controllers\ManagerController;
	$session = Yii::$app->session;
	$monthArray = array("1"=>"Январь","2"=>"Февраль","3"=>"Март","4"=>"Апрель","5"=>"Май", "6"=>"Июнь", "7"=>"Июль","8"=>"Август","9"=>"Сентябрь","10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");
	for ($i=2015; $i < 2100; $i++) { 
		$yearArray[] = $i;
	}
?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">My gifts</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				    <p> Цветовые обозначения статусов
						<p><span class="new">подарок находится в статусе in process/cancelled</span></p>
						<p><span class="expired">подарок находится в статусе expired</span></p>
						<p><span class="approved">подарок находится в статусе done</span></p>
					</p>
					<h4>Инструкция по работе с подарками</h4>
					<p>1. Получив подарок агенство должно ...</p>
					<p>2. После вручения подарка необходимо сделать фото девочки с подарком</p>
					<p>3. После перейти в редактирование статуса подарка и загрузить фото в форму</p> 
					<p>4. Сделать описание к фото</p>
					<h3>Фильтр подарков</h3>
					<form action="" method="post" class="margin-bottom" style="width: 500px">
					<input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
           				value="<?=Yii::$app->request->csrfToken?>"/>
			        <table>
						<tr>
			                <td><label>С:</label></td>
							<td>месяц</td>
							<td>
								<select id="month_from" name="monthFrom">
									<?php foreach ($monthArray as $key=>$month){?>
										<option value="<?=$key?>" <?=($key == date('m',strtotime($dateFrom))) ? 'selected': ''?>><?=$month;?></option>
									<?php } ?>
								</select>
							</td>
							<td>год</td>
							<td>
								<select id="year_from" name="yearFrom">
									<?php foreach ($yearArray as $year){?>
										<option value="<?=$year?>" <?=($year == date('Y',strtotime($dateFrom))) ? 'selected': ''?>><?=$year;?></option>
									<?php } ?>
								</select>
							</td>
			            </tr>
			            <tr>
			                <td><label>по:</label></td>
							<td>месяц</td>
							<td>
								<select id="month_to" name="monthTo">
									<?php foreach ($monthArray as $key=>$month){?>
										<option value="<?=$key?>" <?=($key == date('m',strtotime($dateTo))) ? 'selected': ''?>><?=$month;?></option>
									<?php } ?>
								</select>
							</td>
							<td>год</td>
							<td>
								<select id="year_to" name="yearTo">
									<?php foreach ($yearArray as $year){?>
										<option value="<?=$year?>" <?=($year == date('Y',strtotime($dateTo))) ? 'selected': ''?>><?=$year;?></option>
									<?php } ?>
								</select>
							</td>
			            </tr>
			        </table>
					<input type="submit" value="показать" class="btn margin-top margin-bottom">
					</form>
				    <table class="table-autosort">
				    	<thead>
				    		<tr>
				    			<th class="table-sortable:numeric table-sortable">ID</th>
				    			<th class="table-sortable:default table-sortable">Наименование</th>
				    			<th class="table-sortable:numeric table-sortable">К-во</th>
				    			<th class="table-sortable:default table-sortable">Агенство</th>
				    			<th class="table-sortable:default table-sortable">От кого</th>
				    			<th class="table-sortable:default table-sortable">Кому</th>
				    			<th class="table-sortable:default table-sortable">Дата покупки</th>
				    			<th class="table-sortable:default table-sortable">Дата обновления статуса</th>
				    			<th class="table-sortable:default table-sortable">Статус</th>
				    			<th class="table-sortable:numeric table-sortable">Дней прошло</th>
				    			<th class="table-sortable:numeric table-sortable">Штраф</th>
				    			<th>Действия</th>
				    		</tr>
				    		<tr>
				    			<th style="width: 70px;"><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
				    			<th class=""><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
				    			<th style="width: 50px;"><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
				    			<th class=""><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
				    			<th class=""><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
				    			<th class=""><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
				    			<th class=""></th>
				    			<th class=""></th>
				    			<th class="">
				    				<select onchange="Table.filter(this,this)">
				    					<option value="">All</option>
				    					<option value="new">new/in progress</option>
				    					<option value="expired">expired</option>
				    					<option value="cancelled">cancelled</option>
				    					<option value="approved">done</option>
				    				</select>
				    			</th>
				    			<th class=""></th>
				    			<th class=""></th>
				    			<th class=""></th>
				    		</tr>
				    	</thead>
				    	<tbody>
				    		<?php if(!empty($giftsArray)){
								foreach($giftsArray as $gift){
									$class = 'new';
									if((time() - strtotime($gift->created_at) > 120*60*60) && !in_array($gift->order_status_id,[3,5])){
										$class = 'expired';
									}elseif ($gift->order_status_id == 5){
										$class = 'approved';
									}elseif ($gift->order_status_id == 3){
										$class = 'canceled';
									}
									$penalty_date = (floor((time() - strtotime($gift->created_at))/86400) < 15) ? (floor((time() - strtotime($gift->created_at))/86400)) : 15;
									
									?>
									<tr class="<?=$class;?>">
										<td style="text-align: center"><?=$gift->action_id;?></td>
										<td><?=$gift->gift_name?></td>
										<td><?=$gift->quantity?></td>
										<td><?=trim($gift->agency_name);?></td>
										<td><?=trim($gift->user_from_name . ' '. $gift->user_from_last_name);?></td>
										<td><?=trim($gift->user_to_name . ' '. $gift->user_to_last_name);?></td>
										<td><?=$gift->created_at?></td>
										<td><?=$gift->updated_at?></td>
										<td><?php if(!empty($orderStatusArray)){
												foreach($orderStatusArray as $orderStatus){
													if($orderStatus->id == $gift->order_status_id){ echo $orderStatus->description;}
												}
											}?></td>
										<td><?=$penalty_date;?></td>
										<td><?=$gift->penalty_value?></td>
										<td align="center">
											<?php if($session['user_type'] == ManagerController::USER_AGENCY || 
												    ($session['user_type'] == ManagerController::USER_ADMIN && 
												     isset($session['permissions']) && $session['permissions']['gift_access'] == ManagerController::ALLOW)) {
												if(in_array($gift->order_status_id,[1,2,4]) && $penalty_date != 15){?>
													<a class="btn" href="/manager/edit-shop-item/<?=$gift->action_id;?>">изменить</a>
												<?php }else{?>
													<a class="btn" href="/manager/view-shop-item/<?=$gift->action_id;?>">просмотреть</a>
												<?php }?>
											<?php }elseif($session['user_type'] == ManagerController::USER_SUPERADMIN){?>
												<?php if(in_array($gift->order_status_id,[1,2,4])){?>
													<a class="btn" href="/manager/edit-shop-item/<?=$gift->action_id;?>">изменить</a>
												<?php }else{?>
													<a class="btn" href="/manager/view-shop-item/<?=$gift->action_id;?>">просмотреть</a>
												<?php }?>
											<?php }?>
											<?php if($gift->comments_id != null){?>
												<span class="new-comments" title="new comments"></span>
											<?php }?>
										</td>
									</tr>
								<?php }
				    		} ?>
				    	</tbody>
				    </table>
			</div>
		</div>
	</div>
</div>
<style>
	.new-comments {
		background-color: #00ff11;
		width: 15px;
		height: 15px;
		position: absolute;
		border-radius: 25px;
		right: 15px;
		top: 22px;
	}
</style>