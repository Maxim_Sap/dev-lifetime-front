<style>
	.card-box{
		min-height: 345px;
	}
</style>
<?php
	use app\modules\manager\controllers\ManagerController;
	$session = Yii::$app->session;
	$giftsCategories = isset($searchParams['giftsCategory']) && is_array($searchParams['giftsCategory']) ? $searchParams['giftsCategory'] : [];
	$sort             = isset($searchParams['sort']) && $searchParams['sort'] == 'desc' ? 'desc' : 'asc';
	$ajax = Yii::$app->request->getIsAjax();
if (!$ajax) { ?>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Gift List</h2>
			<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) {?>
				<a href="/manager/add-gift" class="btn">добавить новый</a>
			<?php } ?>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<div id="gifts_content">
				<?php } ?>
					<div class="row">
						<div class="col-xs-12">
							<?php echo $this->render('/layouts/parts/_shop-filter-block.php', ['allGiftsCategories' => $allGiftsCategories, 'giftsCategories' => $giftsCategories]); ?>
						</div>
					</div>
					<?php if(!empty($giftsArray)) { ?>
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<?php echo $this->render('/layouts/parts/_shop-sort-block.php', ['sort' => $sort]); ?>
							</div>
							<div class="col-xs-12 col-sm-6">							
									<?php echo $this->render('/layouts/parts/shop-pagination.php', ['count' => $count, 'page' => $page]); ?>								
							</div>
						</div>
						<div class="row">
							<div class="cards-wrapper clearfix">
								<?php
								foreach($giftsArray as $gift) {
									$img = !empty($gift->image) ? $this->context->serverUrl.'/uploads/gifts/thumb/'.$gift->image : '/img/no_avatar_normal.jpg';
								?>
								<div class="col-xs-12 col-sm-3 col-md-2">
									<figure class="card-box">
										<div class="card-box-image" style="background-image: url('<?=$img;?>')"></div>
										<figcaption class="card-box-description">
											<?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN) {?>
											<ul class="card-box-edit list-inline">
												<li><a href="/manager/edit-gift/<?=$gift->id?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
												<li><a href="/manager/delete-gift/<?=$gift->id?>" onclick="if(!confirm('Are you sure do you wont delete gift?')){return false;}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											</ul>
											<?php } ?>
											<span class="card-box-name"><?=mb_substr($gift->name,0,15);?></span>
											<span class="card-box-price">Price: <span><?=$gift->price;?></span> cr</span>
											<span class="card-box-price">Agency Price: <span><?=$gift->agency_price;?></span> cr</span>
										</figcaption>
									</figure>
								</div>
								<?php } ?>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<?php echo $this->render('/layouts/parts/_shop-sort-block.php', ['sort' => $sort]); ?>
							</div>

							<div class="col-xs-12 col-sm-6">
									<?php echo $this->render('/layouts/parts/shop-pagination.php', ['count' => $count, 'page' => $page]); ?>								
							</div>
						</div>
					<?php } 
					if(!$ajax){ ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>