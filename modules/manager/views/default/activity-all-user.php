<?php
    use app\modules\manager\controllers\ManagerController;
	$session = Yii::$app->session;
?>
<div id="content-area">
    <div class="content-area-inner">
        
        <div class="content-area-inner-header">
            <h2 class="content-title">Страница активности</h2>            
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <input id="search_user_type" type="hidden" value="<?=$searchUserType;?>">
                <input id="limit" type="hidden" value="<?=$limit;?>">
                <form action="" method="post" class="user-activity-form margin-bottom">
                <table>
                    <?php if($session['user_type'] == ManagerController::USER_SUPERADMIN && isset($_GET['type']) && $_GET['type'] == 'girls'){?>
                    <tr>
                        <td><label>Агенство: </label></td>
                        <td>
                            <select id="agency-select">
                                <?php if(!empty($agencyArray)){
                                    foreach($agencyArray as $agency) {?>
                                        <option value="<?=$agency->id?>"><?=trim($agency->name);?></option>
                                <?php }
                                } ?>
                            </select>
                        </td>
                    <?php } ?>
                    </tr>
                    <tr>
                        <td>Имя:</td>
                        <td><input type="text" name="name" value=""></td>
                    </tr>
                    <tr>
                        <td>С:</td>
                        <td><input type="date" name="date-from" value="<?=$dateFrom;?>"></td>
                    </tr>
                    <tr>
                        <td>по:</td>
                        <td><input type="date" name="date-to" value="<?=$dateTo;?>"></td>
                    </tr>
                </table>
                <input type="submit" class="btn margin-top" value="поиск">
            </form>
            <div>
            <table class="activity-table">
                <thead>
                    <th>имя</th>
                    <th>дата входа</th>
                    <th>дата выхода</th>
                    <th>время активности</th>
                    <th>время активности камеры</th>
                </thead>
                <tbody>
                    <?php echo $this->render('/layouts/parts/table-activity-all-users.php',['usersActivityInfo'=>$usersActivityInfo]);?>
                </tbody>
            </table>
            <div class="admin-activity-pagination col-xs-12 col-md-6">
                <?php if (isset($usersActivityInfo) && !empty($usersActivityInfo)) {
                    echo $this->render('/layouts/parts/pagination.php', [
                            'letters_count'=>$count,
                            'page'=>$page,
                            'limit'=>$limit,
                            'page_name'=>'activity-page'
                        ]);
                    } ?>
            </div>
            </div>
            </div>
        </div>
    </div>
</div>
