<?php
$session = Yii::$app->session;
?>
<style>
.drag-and-drop-area {
  font-size: 18px;
  border: 2px dashed rgba(235, 65, 97, 0.7);
  text-align: center;
  padding: 20px 0;
  line-height: 1.4;
  position: relative;
  margin-bottom: 40px;
}
.drag-and-drop-area span {
  display: block;
}
.drag-and-drop-area .title {
  color: #eb4161;
  margin-bottom: 10px;
}
.drag-and-drop-area .upload-form {
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
}
.drag-and-drop-area .upload-form input[type="file"] {
  margin: 0;
  width: auto;
  height: auto;
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  cursor: default;
  padding: 0;
}
.private-note {
  font-size: 12px;
  color: #9a9a9a;
}   
</style>
<div id="content-area">
    <div class="content-area-inner">
        
        <div class="content-area-inner-header">
            <h2 class="content-title"><a href="/manager/my-gift">My gifts</a> -> item <?=$actionID?></h2>
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <div>
                    <form action="" method="post">
                        <table>
                            <thead>
                            <tr>
                                <th>Наименование</th>
                                <th>Значение</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($giftsArray)) {
                                $gift = $giftsArray[0];
                                //foreach ($gift_mass as $one) { ?>
                                    <tr>
                                        <td>Item ID:</td>
                                        <td>
                                            <?=$gift->action_id?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Имя агенства</td>
                                        <td>
                                            <?=trim($gift->agency_name);?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Название подарка</td>
                                        <td>
                                            <?=$gift->gift_name?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Картинка подарка</td>
                                        <td>
                                            <div
                                                style="width: 100px; height: 100px; background-position: center; background-size: cover; background-image: url(<?=(!empty($gift->gift_img)) ? $this->context->serverUrl . '/uploads/gifts/thumb/' . $gift->gift_img : '/img/no_avatar_normal.jpg';?>);">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Количество</td>
                                        <td>
                                            <?=$gift->quantity?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>От кого</td>
                                        <td>
                                            <?=trim($gift->user_from_name . ' ' . $gift->user_from_last_name);?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Кому</td>
                                        <td>
                                            <?=trim($gift->user_to_name . ' ' . $gift->user_to_last_name);?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Дата заказа</td>
                                        <td>
                                            <?=$gift->created_at?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Статус</td>
                                        <td>
                                            <?php if (isset($type) && $type == 'view') {
                                                foreach ($orderStatusArray as $orderStatus) {
                                                    if ($orderStatus->id == $gift->order_status_id) {
                                                        echo $orderStatus->description;
                                                    }
                                                }
                                            } else { ?>
                                                <select name="status">
                                                    <?php
                                                    if ($session['user_type'] == 3) {
                                                        ?>
                                                        <option value="1" <?php if ($gift->order_status_id == 1) {
                                                            echo 'selected';
                                                        } ?>>new
                                                        </option>
                                                        <option value="2" <?php if ($gift->order_status_id == 2) {
                                                            echo 'selected';
                                                        } ?>>send to cancel
                                                        </option>
                                                        <option value="4" <?php if ($gift->order_status_id == 4) {
                                                            echo 'selected';
                                                        } ?>>send to approve
                                                        </option>
                                                    <?php } elseif ($session['user_type'] == 6) {
                                                        if (!empty($orderStatusArray)) {
                                                            foreach ($orderStatusArray as $orderStatus) {
                                                                ?>
                                                                <option
                                                                    value="<?=$orderStatus->id?>" <?php if ($orderStatus->id == $gift->order_status_id) {
                                                                    echo 'selected';
                                                                } ?>><?=$orderStatus->description?></option>
                                                            <?php }
                                                        }
                                                    } ?>
                                                </select>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Дата последнего изменения статуса</td>
                                        <td>
                                            <?=$gift->updated_at?>
                                        </td>
                                    </tr>
                                    <?php if(!empty($discuss)){?>
                                        <tr>
                                            <td align="left">Обсуждение</td>
                                            <td>
                                                <?php foreach($discuss as $item){?>
                                                    <p>
                                                        <span style="color: #4549de;font-weight: bold;"><?=(trim($item->name) == 'Users without agency' ? "Admin" : trim($item->name))?></span>
                                                        [<?=$item->date_create?>]:
                                                        <span><?=$item->comments?></span>
                                                    </p>
                                                <?php }?>
                                            </td>
                                        </tr>
                                    <?php }?>
                                    <?php if (!isset($type) || $type != 'view') {?>
                                        <tr>
                                            <td align="left">Добавить комментарий в обсуждение</td>
                                            <td>
                                                <textarea name="discuss" style="width: 100%; height: 115px;"></textarea>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td colspan="2" style="text-align: left">Комментарий к полученому подарку, фото (будет отображаться у отправителя подарка)</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <?php if (isset($type) && $type == 'view') {
                                                echo $gift->comment;
                                            } else { ?>
                                                <textarea id="gift_comment" name="comment"><?=$gift->comment;?></textarea>
                                                <script>
                                                    $(document).ready(function () {
                                                        if ($('#gift_comment').length > 0) {
                                                            tinymce.init({
                                                                selector: "#gift_comment",
                                                                plugins: "paste textcolor emoticons link code",
                                                                paste_as_text: true,
                                                                skin: "custom",
                                                                height: "400",
                                                                menubar: false,
                                                                toolbar: "bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | forecolor backcolor | emoticons | undo redo | fullscreen | link code",
                                                                statusbar: true,
                                                                language: "en",
                                                            });
                                                        }
                                                    });
                                                </script>
                                                <div class="attach_foto_block">
                                                    <h4>Attach your photo girls with gifts (click on photo and this photo will be added in letter)</h4>
                                                    <div id="in_uploaded_img" style="display:none;">0</div>
                                                    <div id="document-attach-photo"></div>
                                                    <div class="row uploader" style="position: relative;">
                                                        <a style="display: none" href="javascript:void(0);" id="save_order">save</a>
                                                        <div class="col-md-12">
                                                            <div class="drag_and_drop_area drag-and-drop-area inner-padding">
                                                                
                                                                <a href="javascript:void(0);" class="main-col-text">Click here or
                                                                    drag&amp;drop photos to upload them</a>
                                                                <div class="cab_main_photo" id="div_foto_add">
                                                                    <span for="choose" id="link" class="choose"></span>
                                                                    <form id="chooseform" method="post"
                                                                          enctype="multipart/form-data">
                                                                        <input type="hidden" name="MAX_FILE_SIZE" value="12345"/>
                                                                        <input type="file" name="file" multiple accept="image/*"
                                                                               id="choose"
                                                                               data-album-title="girls-with-gifts" data-place="albums"
                                                                               style="display: none;"/>
                                                                    </form>
                                                                </div>
                                                                <div>Select a file in this format: gif, jpg, jpeg, png</div>
                                                                <div>Should not exceed 2 Mb!</div>
                                                            </div>
                                                            <div class="private-note">
                                                                Private gallery photos are not available for public viewing on the website. Thus you can use the private gallery for storing your special photos. We maintain the confidentiality of your information and not share it!
                                                            </div>
                                                        </div>
                                                        <span style="height: 183px;    position: absolute;    top: 0px;    width: 100%;    left: 0;"
                                                          class="drop_here" data-album-title="girls-with-gifts" data-place="albums"></span>
                                                        <div class="errors"></div>
                                                        <div id="status1"></div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php if (!isset($type)) { ?>
                                        <tr>
                                            <td colspan="2">
                                                <input type="hidden" name="actionID" value="<?=$actionID?>">
                                                <input id="otherUserID" type="hidden" value="<?=$gift->agency_id?>">
                                                <input type="submit" class="btn pull-right" value="Сохранить">
                                            </td>
                                        </tr>
                                    <?php }
                                //}
                            } else { ?>
                                <tr>
                                    <td colspan="2">No found gifts</td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>