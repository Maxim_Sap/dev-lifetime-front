<?php
use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;

//var_dump($userData);die;
use yii\helpers\Url;
?>
<div id="content-area">
    <div class="content-area-inner">

        <div class="content-area-inner-header">
            <h2 class="content-title">View profile</h2>
        </div>
        <div class="content-area-inner-body">
            <div class="content-container">
                <?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN && !empty($userData->agencyRuleViolations)) { ?>
                <div class="rule-violations">
                    <?php foreach ($userData->agencyRuleViolations as $violation) { ?>
                           <a href="<?= Url::to(['/manager/rule-violation/view', 'ruleViolationID' => $violation->id]); ?>"><i class="fa fa-star fa-6"></i></a>
                <?php } ?>
                </div>
                <?php } ?>
                <div class="user_avatar" style="margin-bottom: 25px">
                <?php if (false): ?>
                    <a href="/manager/edit-user/<?=$userData->userID?>" title="редактировать профиль">
                        <img src="<?=$userData->avatar->normal;?>">
                    </a>
                    <br><br>
                <?php endif; ?>
                    <a class="btn" href="/manager/edit-user/<?=$userData->userID?>" title="редактировать профиль">
                        Edit profile
                    </a>
                </div>
                <table class="margin-top margin-bottom">
                    <tr>
                        <td>User ID:</td>
                        <td><span><?=$userData->userID;?></span></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Agency Name</td>
                        <td><?=(!empty($userData->agency_info->name)) ? $userData->agency_info->name : '';?></td>
                        <td>Your email</td>
                        <td><a href="mailto:<?=$userData->email?>"><?=$userData->email;?></a></td>                        
                    </tr>
                    <tr>
                        <td>Agency director name</td>
                        <td><?=(isset($userData->agency_info->contact_person) ? $userData->agency_info->contact_person : "");?></td>
                        <td>Agency director address</td>
                        <td><?=(isset($userData->agency_info->director_address) ? $userData->agency_info->director_address : "");?></td>
                    </tr>
                    <tr>
                        <?php if (false) { ?>
                        <td>Pasport serial number</td>
                        <td><?=(isset($userData->agency_info->passport_number) ? $userData->agency_info->passport_number : "");?></td>
                        <?php } ?>
                        <td>Office address</td>
                        <td><?=(isset($userData->agency_info->office_address) ? $userData->agency_info->office_address : "");?></td>
                        <td>Office phone</td>
                        <td><?=(isset($userData->agency_info->office_phone) ? "<a href='skype:".$userData->agency_info->office_phone."'>".$userData->agency_info->office_phone."</a>" : "");?></td>
                    </tr>
                    <tr>
                        <td>Mobile phone</td>
                        <td><?=(isset($userData->agency_info->m_phone) ? "<a href='skype:".$userData->agency_info->m_phone."'>".$userData->agency_info->m_phone."</a>" : "");?></td>
                        <td>Skype</td>
                        <td>
                            <?=(isset($userData->agency_info->skype) ? $userData->agency_info->skype : "");?>
                        </td>
                    </tr>
<!--                     <tr>
    <td>Card number</td>
    <td><?=(isset($userData->agency_info->card_number) ? $userData->agency_info->card_number : "");?></td>
    <td>Card user name</td>
    <td><?=(isset($userData->agency_info->card_user_name) ? $userData->agency_info->card_user_name : "");?></td>
</tr> -->
                    <tr>
                        <td>Country</td>
                        <td><?=(isset($userData->country)) ? $userData->country : "Not defined";?></td>
                        <td>Region</td>
                        <td><?=(isset($userData->city)) ? $userData->city : "Not defined";?></td>                        
                    </tr>
                </table>
                <?php if ($session['user_type'] == ManagerController::USER_SUPERADMIN || $session['user_type'] == ManagerController::USER_AGENCY) {?>
                <div>
                    <p>Bank detail info</p>
                    <?= (isset($userData->bank_detail)) ? $userData->city : "<p>Not defined</p>"; ?>
                </div>
                <?php } ?>
                <div>
                <?php
                $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved agency info</span>';
                if (isset($userData->agency_info->approve_status)) {
                    switch ($userData->agency_info->approve_status) {
                        case 1:
                            $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved agency info</span>';
                            break;
                        case 2:
                            $approve_status = '<span style="color: lawngreen; margin-left: 10px;font-weight: bold;">Approve in progress</span>';
                            break;
                        case 3:
                            $approve_status = '<span style="color: #ffcc00; margin-left: 10px;font-weight: bold;">Approve delcine</span>';
                            break;
                        case 4:
                            $approve_status = '<span style="color: #ffcc00; margin-left: 10px;font-weight: bold;">Approved</span>';
                            break;
                        default:
                            $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved agency info</span>';
                            break;
                    }
                }
                echo "Agency status: ".$approve_status; ?>
                </div>
            </div>
            <?= $this->render('/layouts/parts/girl-filter.php', ['activityStatus' => $activityStatus, 'online' => $online, 'active' => $active, 'notActive' => $notActive, 'userId' => $userId, 'userName' => $userName, 'userAgeFrom' => $userAgeFrom, 'userAgeTo' => $userAgeTo, 'userCity' => $userCity, 'userCountry' => $userCountry, 'sortBy' => $sortBy, 'sortWay' => $sortWay, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo]) ?>
            <?php if (!empty($girlsArray)) { ?>
            <div class="cards-wrapper">
                <div class="row">
                <?php foreach ($girlsArray as $girl) { ?>                    
                    <div class="col-xs-12 col-sm-3 col-md-2">   
                        <?=$this->render('/layouts/parts/single-agency-girl.php',['girl'=>(object)$girl])?>
                    </div>
                <?php }?>               
            <?php }else{ ?>
                <p>You have not users</p>
            <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    #content-area .btn-large {
        max-width: 250px;
    }
    .fa-6 {
        font-size: 3em;
        color: red;
    }
    .rule-violations    {
        margin: 20px 0;
    }
    #content-area .content-area-inner {
        min-height: 1955px;
    }
    .status {
        display: inline-block!important;
        width: 10px;
        height: 10px;        
        border-radius: 50%;
    }
    .online {
        background: green;
    }
    .offline {
        background: red;
    }    
</style>