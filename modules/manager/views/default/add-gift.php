<?php

$name = isset($giftsArray[0]->name) ? $giftsArray[0]->name : '';
$description = isset($giftsArray[0]->description) ? $giftsArray[0]->description : '';
$price = isset($giftsArray[0]->price) ? $giftsArray[0]->price : '';
$agency_price = isset($giftsArray[0]->agency_price) ? $giftsArray[0]->agency_price : '';
$image = (isset($giftsArray[0]->image) && $giftsArray[0]->image != '') ? $this->context->serverUrl.'/uploads/gifts/large/'.$giftsArray[0]->image : '/img/no_avatar_normal.jpg';
$giftID = isset($giftsArray[0]->id) ? $giftsArray[0]->id : '';
$giftType = isset($giftsArray[0]->type_id) ? $giftsArray[0]->type_id : false;
?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title"><?=($giftID != '' ? 'Edit': 'Add');?> Gift</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<form method="post" class="add-gift" action="">
				<select name="type_id" class="margin-bottom" title="gift category" required>
					<option value="">--- please select gift type ---</option>
					<?php if(isset($giftTypeArray) && !empty($giftTypeArray)){
						foreach($giftTypeArray as $type){ ?>
							<option value="<?=$type['id']?>" <?=(!empty($giftType) && $giftType == $type['id'])? 'selected': '';?>><?=$type['title']?></option>
						<?php }
					}?>
				</select>
				<input type="text" name="name" class="margin-bottom" placeholder="Name" value="<?=$name;?>" title="title" required>
				<textarea cols="22" rows="4" name="description" class="margin-bottom" placeholder="Description" title="Description" required><?=$description;?></textarea>
				<div>
					<img id="gift-img" src="<?=$image?>" onclick="$('#gift-img-form input[type=\'file\']').click();">
					<input type="hidden" name="image" value="<?= (isset($giftsArray[0]->image)) ? $giftsArray[0]->image : '' ?>">
				</div>
				<a id="change_gift_img" class="btn pull-right" href="javascript:void(0);" onclick="$('#gift-img-form input[type=\'file\']').click();"><span>change image</span></a>
				<br><br><br><br>
				<input type="" name="price" class="margin-bottom" placeholder="Price for man" value="<?=$price?>" title="User Price" required>
				<input type="" name="agency_price" class="margin-bottom" placeholder="Agency Price" value="<?=$agency_price?>" title="Agency Price" required>
				<br/>
				<input type="hidden" name="giftID" value="<?=$giftID?>" required>
				<input type="submit" class="btn pull-right" value="<?=($giftID != '' ? 'Update': 'Save');?>">
			</form>
			<form id="gift-img-form" method="post" enctype="multipart/form-data">
				<input type="hidden" name="MAX_FILE_SIZE" value="12345"/>
				<input id="choose" type="file" name="file" multiple accept="image/*" data-album-title="temp" data-place="admin-add-gift" style="display: none;" />
			</form>
			</div>
		</div>
	</div>
</div>


<section class="admin-content">
	<div class="">
		<h2></h2>
	</div>
	<div class="item-content">

	</div>
</section>