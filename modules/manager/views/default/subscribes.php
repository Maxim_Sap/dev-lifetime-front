<div id="content-area">
    <div class="content-area-inner">

        <div class="content-area-inner-header">
            <h2 class="content-title">Подписчики</h2>
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">

                <div class="table_scroll_wrap">
                    <table class="finance-leters-table">
                        <thead>
                        <tr>
                            <th>№ п/п</th>
                            <th>дата подписки</th>
                            <th>почта</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($subscribes)) { $i = 1;
                            foreach ($subscribes as $subscriber) {
                                ?>
                                <tr>
                                    <td>
                                        <?=$i?>
                                    </td>
                                    <td><?=$subscriber->created_at?></td>
                                    <td>
                                        <?=$subscriber->email;?>
                                    </td>
                                </tr>
                            <?php $i++; } ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="3"><p>No results</p></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
