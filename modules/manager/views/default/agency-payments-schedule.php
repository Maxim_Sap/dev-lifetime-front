<?php
$session = Yii::$app->session;
$monthArray = array("1"=>"Январь","2"=>"Февраль","3"=>"Март","4"=>"Апрель","5"=>"Май", "6"=>"Июнь", "7"=>"Июль","8"=>"Август","9"=>"Сентябрь","10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");
for ($i=2015; $i < 2100; $i++) { 
	$yearArray[] = $i;
}
$paymentStatus = array(1=>'текущий','в обработке','закрыт');
$statusClass = array(1=>'yellow','blue','green');
$ajax = Yii::$app->request->getIsAjax();
if(!$ajax){
?>
<div class="inbox_container admin-content finance_agency_page">
	<h2>График выплат:</h2>
	<form action="" method="post" class="agency-payments-schedule-form margin-bottom" style="width: 500px">
        <table>
			<?php if(isset($agencyList) && !empty($agencyList)){
				if(is_array($agencyList)){?>
				<tr>
					<td><label>Агенство:</label></td>
					<td colspan="4">
						<select name="agency_id" class="my-users1">
							<?php foreach($agencyList as $agency){?>
								<option value="<?=$agency->user_id?>"><?=trim($agency->name);?></option>
							<?php }?>
						</select>
					</td>
				</tr>
				<?php }else{?>
					<input type="hidden" name="agency_id" value="<?=(int)$agencyList;?>">
				<?php }
			}else{ ?>
				<tr><td colspan="2"><p>no agency</p></td></tr>
			<?php } ?>
			<tr>
                <td><label>С:</label></td>
				<td>месяц</td>
				<td>
					<select id="month_from">
						<?php foreach ($monthArray as $key=>$one){?>
							<option value="<?=$key?>" <?=($key == date('m',strtotime($dateFrom))) ? 'selected': ''?>><?=$one;?></option>
						<?php } ?>
					</select>
				</td>
				<td>год</td>
				<td>
					<select id="year_from">
						<?php foreach ($yearArray as $year){?>
							<option value="<?=$year?>" <?=($year == date('Y',strtotime($dateFrom))) ? 'selected': ''?>><?=$one;?></option>
						<?php } ?>
					</select>
				</td>
            </tr>
            <tr>
                <td><label>по:</label></td>
				<td>месяц</td>
				<td>
					<select id="month_to">
						<?php foreach ($monthArray as $key=>$one){?>
							<option value="<?=$key?>" <?=($key == date('m',strtotime($dateTo))) ? 'selected': ''?>><?=$one;?></option>
						<?php } ?>
					</select>
				</td>
				<td>год</td>
				<td>
					<select id="year_to">
						<?php foreach ($yearArray as $year){?>
							<option value="<?=$year?>" <?=($year == date('Y',strtotime($dateTo))) ? 'selected': ''?>><?=$one;?></option>
						<?php } ?>
					</select>
				</td>
            </tr>
        </table>
		<input type="submit" value="показать" class="btn admin-custom-btn margin-top margin-bottom">
	</form>
	<?php }?>
	<div id="agency-payments-schedule-block">
		<table class="agency-payments-schedule-table">
			<thead>
				<th>период</th>
				<th>статус</th>
				<th>заработок</th>
				<th>штрафы</th>
				<th>перенесено</th>
				<th>к выплате</th>
				<th>комментарий</th>
				<th>дата выплаты</th>
				<th>выплачено</th>
				<th></th>
			</thead>
			<tbody>
				<?php
				if(!empty($payments_data)){?>
						<?php foreach($payments_data as $one){
							$need_to_pay = $one->earnings + $one->transfer - $one->penalty;
							?>
							<tr class="<?=$status_class[$one->status]?>">
								<td><?=date('M Y',strtotime($one->period_date))?></td>
								<td><?=$payment_status[$one->status]?></td>
								<td><?=$one->earnings?></td>
								<td><?=$one->penalty?></td>
								<td><?=$one->transfer?></td>
								<td><?=($need_to_pay < 0 ? "<span style='color: red;font-weight: bold'>".$need_to_pay."</span>" : $need_to_pay)?></td>
								<td><?=$one->comments?></td>
								<td><?=(strtotime($one->date_of_pay) ? date('Y-m-d',strtotime($one->date_of_pay)) : '')?></td>
								<td><?=$one->paid_value?></td>
								<td>
									<?php
									if($session['user_type'] == 6 && $one->status == 2){ //только для статуса в обработке?>
										<a class="admin-table-btn" href="/manager/finance/transfer-agency-payments/<?=$one->id?>" title="перенести выплату на следующий месяц">перенести</a>
										<a class="admin-table-btn" href="/manager/finance/add-agency-payments/<?=$one->id?>" title="перейти к форме оплаты">выплатить</a>
									<?php } ?>
								</td>
							</tr>
					<?php } ?>
				<?php }else{?>
					<tr>
						<td colspan="9">No results</td>
					</tr>
				<?php } ?>

			</tbody>
		</table>
	</div>
	<?php if(!$ajax){?>
	<div class="agency-detail-finance-info"></div>
</div>
<script type="text/javascript">$(document).ready(function(){
	if($('select.my-users1 option:selected').val() != ""){
		$('.agency-finance-form').submit();
	}
});</script>
<?php } ?>