<?php 
use app\modules\manager\controllers\ManagerController;

?>
<style>
    .check-group {
        display: inline-block;
    }
    .avatar {
        width: 63px;
    }
    table tr.online{
        background-color: #d5e6d5;
    }
    table tr.offline{
        background-color: #f3dada
    }
    .signalizator table tr th,.signalizator table tr td{
        position: relative;
    }
    .signalizator table tr th label,.signalizator table tr td label{
        width: 50px;
        height: 50px;
        position: absolute;
        top: -24px;
        left: -9px;
        z-index: 1;
    }
</style>
<div id="content-area">
    <div class="content-area-inner">
        
        <div class="content-area-inner-header">
            <h2 class="content-title">Chat signalizator</h2>            
        </div>

        <div class="content-area-inner-body">
            <div class="content-container signalizator">
                <?php
                if (!empty($girls)) { ?>                
                    <table>
                        <thead>
                            <th align="center">
                                <label for="check_all"></label>
                                <input id="check_all" type="checkbox"/>
                            </th>
                            <th>ID</th>
                            <th>Active</th>
                            <th>Invisible</th>
                            <th>Approved</th>
                            <th>Name</th>
                            <th>Avatar</th>
                            <th>Status</th>
                        </thead>
                        <?php foreach ($girls as $girl) { ?>
                            <tr class="<?=($girl->last_activity >= time() - 30 ? 'online':'offline');?>">
                                <?php
                                $serverUrl = $this->context->serverUrl;
                                $avatar     = !empty($girl->small_thumb) ? $serverUrl . '/' . $girl->small_thumb : '/img/no_avatar_normal.jpg';
                                switch ($girl->approve_status_id) {
                                    case 1:
                                        $approve_status = 'not approved';
                                        break;
                                    case 2:
                                        $approve_status = 'in progress';
                                        break;
                                    case 3:
                                        $approve_status = 'decline';
                                        break;
                                    case 4:
                                        $approve_status = 'approved';
                                        break;
                                }

                                ?>
                                <td align="center">
                                    <label for="check_<?=$girl->id;?>"></label>
                                    <input type="checkbox" id="check_<?=$girl->id;?>" data-user-id="<?=$girl->id;?>"/>
                                </td>
                                <td><a href="/manager/edit-user/<?=$girl->id;?>"
                                       class="girl-link">[<?=$girl->id;?>]</a></td>
                                <td><?=($girl->status == ManagerController::STATUS_ACTIVE) ? 'active':'not active';?></td>
                                <td><?=($girl->visible == ManagerController::STATUS_VISIBLE) ? 'visible': 'invisible';?></td>
                                <td><?=$approve_status;?></td>
                                <td><?=trim($girl->first_name.' '.$girl->last_name);?></td>
                                <td>                                    
                                    <img class="avatar" src="<?=$avatar;?>" alt="">
                                </td>
                                <td><?=($girl->last_activity >= time() - 30 ? 'online':'offline');?></td>
                            </tr>
                        <?php } ?>

                    </table>
                    <div class="margin-top">
                        <input id="set_online" data-value="1" type="submit" value="set online" class="btn set_status">
                        <input id="set_offline" data-value="0" type="submit" value="set offline" class="btn set_status">
                    </div>
                <?php } else { ?>
                    <p>You have not user</p>
                    <a href="/manager/add-user">Add new user</a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

