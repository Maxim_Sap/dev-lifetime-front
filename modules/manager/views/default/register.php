<?php
    $apiServer = $this->context->serverUrl;
?>
<style>
    .admin_body .registration_block {
        height: 470px !important;
    }
    .admin_body .admin_form{
        width: 320px;
    }
</style>
<div id="modalSignIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body admin_form rounded_block shadow_block border_block registration_block ">
                <span  class="log_in_title">Sign in to get in touch</span>
                <form class="register-form">
                    <input class="admin_input" type="text" name="first_name"  placeholder="name" required="required">
                    <input class="admin_input" type="text" name="email" placeholder="e-mail" required="required">
                    <input class="admin_input" type="password" name="password" placeholder="password" required="required">
                    <input class="admin_input" type="password" name="conf_password" placeholder="confirm password" required="required">
                    <input type="submit" value="Sign up" class="btn btn-wide log_in_btn"> 
                    <div class="singin-agreement custom-checkbox">
                        <input type="checkbox" id="singin-agreement" checked="checked" >
                        <label for="singin-agreement" class="pos-rel">I agree with terms of conditions</label>
                    </div>
                </form>
                <span>Already have an account?</span>
                <button class="btn btn-wide log_in_btn margin-top" onclick="location.href = '/manager/login';">Sign in</button>                
            </div>
            <input type="hidden" id="api_server" value="<?=$apiServer?>">
        </div>
    </div>
</div>
