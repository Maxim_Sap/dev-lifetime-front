<?php
    use app\modules\manager\controllers\ManagerController;
    use yii\helpers\Html;
	$session = Yii::$app->session;
    $this->title = 'Contract page';
?>
<style>
.approved-status {
    position: absolute;
    top: 10px;
    left: 10px;
    padding: 10px;    
    color: white;
}
.approved-status.yes{
    background: green;
}
.approved-status.no {
    background: red;
}
    .drag-and-drop-area {
        font-size: 18px;
        border: 2px dashed rgba(235, 65, 97, 0.7);
        text-align: center;
        padding: 20px 0;
        line-height: 1.4;
        position: relative;
        margin-bottom: 40px;
    }
    .card-box {
        max-width: inherit;
        margin: 0px;
        padding: 0px;
        border: none;
        min-height: 300px;
    }
    .card-box img {
        height: 250px;
    }
    .foto_foot {
        width: 250px;
        margin: 0 AUTO;
    }
    .attach_foto_block {
        margin-top: 20px;
    }
</style>
<div id="content-area">
    <div class="content-area-inner">
        
        <div class="content-area-inner-header">
            <h2 class="content-title">Contract page</h2>            
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
            <?php if($session['user_type'] == ManagerController::USER_SUPERADMIN) {?>
            <textarea id="text-memo"><?php if (isset($pageContent->description)) {echo Html::decode($pageContent->description); } ?></textarea>
            <input id="page-name" type="hidden" value="contract" /> <br>
            <button id="save-page" class="btn pull-right">Save</button>
            <script>
                $(document).ready(function(){
                    if($('#text-memo').length > 0){
                        tinymce.init({
                            selector: "#text-memo",
                            plugins: "paste textcolor emoticons link code",
                            file_picker_callback: function(callback, value, meta) {
                                imageFilePicker(callback, value, meta);
                            },

                            paste_as_text: true,
                            skin: "custom",
                            height: "700",
                            menubar: false,
                            toolbar: "bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | forecolor backcolor | emoticons | undo redo | fullscreen | link code",
                            statusbar: true,
                            language: "en",
                        });
                    }
                });

                var imageFilePicker = function (callback, value, meta) {
                    tinymce.activeEditor.windowManager.open({
                            title: 'File and Image Picker',
                            url: '/manager/image-list',
                            width: 400,
                            height: 300,
                            buttons: [{
                                text: 'Insert',
                                onclick: function () {
                                    //do some work to select an item and insert it into TinyMCE
                                    tinymce.activeEditor.windowManager.close();
                                }
                            },
                                {
                                    text: 'Close',
                                    onclick: 'close'
                                }],
                        },
                        {
                            oninsert: function (url) {
                                callback(url);
                            }
                        });
                };
            </script>
            <?php }else{ ?>
                <div class="page-content"><?php if (isset($pageContent->description)) {echo Html::decode($pageContent->description); } ?></div>

                <br/>
                You must
                <a href="/contract.docx" download="">download contract here</a> and
                <input type="hidden" id="otherUserID" value="<?=(isset($session['agency_id']) && is_numeric($session['agency_id']) ? $session['agency_id'] : $session['user_id'])?>">

                <div class="attach_foto_block">
                    <h4>Attach your contract scan copy</h4>
                    <div id="in_uploaded_img" style="display:none;">0</div>
                    <div id="document-attach-photo"></div>
                    <div class="row uploader" style="position: relative;">
                        <a style="display: none" href="javascript:void(0);" id="save_order">save</a>
                        <div class="col-md-12">
                            <div class="drag_and_drop_area drag-and-drop-area inner-padding">

                                <a href="javascript:void(0);" class="main-col-text">Click here or drag&amp;drop photos to upload them</a>

                                <div class="cab_main_photo" id="div_foto_add">
                                    <span for="choose" id="link" class="choose"></span>
                                    <form id="chooseform" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="MAX_FILE_SIZE" value="12345"/>
                                        <input type="file" name="file" multiple accept="image/*" id="choose"
                                               data-album-title="contract" data-place="albums" style="display: none;"/>
                                    </form>
                                </div>
                                <div>Select a file in this format: gif, jpg, jpeg, png</div>
                                <div>Should not exceed 2 Mb!</div>
                            </div>
                            <div class="foot_reply-block_text text-dark-grey">
                                Private photos are not available for public viewing on the website. Thus you can use the
                                private gallery for storing your special photos. We maintain
                                the confidentiality of your information and not share it!
                            </div>
                        </div>
                <span style="height: 183px;    position: absolute;    top: 0px;    width: 100%;    left: 0;"
                      class="drop_here" data-album-title="contract" data-place="albums"></span>
                        <div class="errors"></div>
                        <div id="status1"></div>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
</div>
