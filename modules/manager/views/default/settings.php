<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Site settings</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
					<form class="settings" action="/manager/settings" method="post">
					    <table>
					    	<thead>
					    		<tr>
					    			<th>name</th>
					    			<th>value</th>
					    		</tr>
					    	</thead>
					    	<tbody>
					    		<tr>
					    			<td>Server url</td>
					    			<td><input type="text" name="server_url" value="<?= $serverUrl ?>" required></td>
					    		</tr>
					    		<tr>
					    			<td>Number of messages on page</td>
					    			<td><input type="number" name="message_count_on_page" value="<?= $numberOfMessagesOnPage ?>" required></td>
					    		</tr>
					    		<tr>
					    			<td>Number of messages on admin page</td>
					    			<td><input type="number" name="admin_message_count_on_page" value="<?= $numberOfMessagesOnAdminPage ?>" required></td>
					    		</tr>
								<tr>
									<td>Number of items on admin page</td>
									<td><input type="number" name="admin_item_count_on_page" value="<?= $numberOfItemsOnAdminPage ?>" required></td>
								</tr>
					    		<tr>
					    			<td>Number of tasks on admin page</td>
					    			<td><input type="number" name="admin_tasks_count_on_page" value="<?= $numberOfTasksOnAdminPage ?>" required></td>
					    		</tr>
					    		<tr>
					    			<td>Number of items on search page</td>
					    			<td><input type="number" name="search_item_count_on_page" value="<?= $numberOfItemsOnSearchPage ?>" required></td>
					    		</tr>	    		
					    		<tr>
					    			<td>Number of items on front page</td>
					    			<td><input type="number" name="front_item_count_on_page" value="<?= $numberOfItemsOnFrontPage ?>" required></td>
					    		</tr>
								<tr>
									<td>Number of gifts items on front page</td>
									<td><input type="number" name="front_gift_item_count_on_page" value="<?= $numberOfGiftItemsOnFrontPage ?>" required></td>
								</tr>
								<tr>
									<td>Number of gifts items on backend</td>
									<td><input type="number" name="backend_gifts_item_count_on_page" value="<?= $numberOfGiftItemsOnBackend ?>" required></td>
								</tr>
								<tr>
									<td>Number of blog items on frontend</td>
									<td><input type="number" name="blog_items_on_frontend" value="<?= $numberOfBlogItems ?>" required></td>
								</tr>
					    	</tbody>
					    </table>
				        <button class="btn pull-right">Save</button>
				    </form>
			</div>
		</div>
	</div>
</div>


<div class="container admin-content inbox_container">
    <h2><span class="text-pink"></span></h2>

</div>