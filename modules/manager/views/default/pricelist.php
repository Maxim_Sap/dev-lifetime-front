<?php
$session = Yii::$app->session;
//пока заранее задан один скин
$skinList = [['code'=>'skin_1','id'=>1]];
$planList = [['name'=>'plan_1','id'=>1]];
?>
<div id="content-area">
	<div class="content-area-inner">

		<div class="content-area-inner-header">
			<h2 class="content-title">Price List</h2>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<?php if(!empty($pricelist)){?>
					<table class="price margin-bottom">
						<thead>
						<tr>
							<th>User Id</th>
							<th>Skin Id</th>
							<th>Plan Id</th>
							<th>Action</th>
							<th>Date From</th>
							<th>Date To</th>
							<th>Price</th>
							<th></th>
						</tr>
						</thead>
						<tbody id="price-list">
						<?php
						foreach($pricelist as $pricelistItem){ if($pricelistItem->action_type_id == 4){continue;}?>
								<tr>
									<form method="post" action="/manager/pricelist">
									<td class=""><input type="number" name="otherUserID" value="<?=$pricelistItem->user_id?>" placeholder="all user" disabled></td>
									<td>
										<select name="skinID" <?php if($pricelistItem->important == 1){echo "disabled";} ?>>
											<?php  if(!empty($skinList)){
												foreach($skinList as $skin){
													$skin = (object) $skin; ?>
													<option value="<?=$skin->id?>" <?php if($skin->id == $pricelistItem->skin_id){echo " selected";}?>><?=$skin->code?></option>
												<?php }
											}?>
										</select>
									</td>
									<td>
										<select name="planID" <?php if($pricelistItem->important == 1){echo "disabled";} ?>>
											<?php  if(!empty($planList)){
												foreach($planList as $plan){
													$plan = (object) $plan; ?>
													<option value="<?=$plan->id?>" <?php if($plan->id == $pricelistItem->plan_id){echo " selected";}?>><?=$plan->name?></option>
												<?php }
											}?>
										</select>
									</td>
									<td>
										<select name="actionTypeID" <?php if($pricelistItem->important == 1){echo "disabled";} ?>>
											<?php if(!empty($actionTypeList)){
												foreach($actionTypeList as $actionType){?>
													<option value="<?=$actionType->id?>" <?php if($actionType->id == $pricelistItem->action_type_id){echo " selected";}?>><?=$actionType->name?></option>
												<?php }
											}?>
										</select>
									</td>
									<td><input type="date" name="startDate" value="<?=(!empty($pricelistItem->start_date)) ? date("Y-m-d",strtotime($pricelistItem->start_date)) : "";?>" <?php if($pricelistItem->important == 1){echo "disabled";} ?>></td>
										<?php if($pricelistItem->important == 1){?>
											<input type="hidden" name="startDate" value="<?=(!empty($pricelistItem->start_date)) ? date("Y-m-d",strtotime($pricelistItem->start_date)) : date("Y-m-d",time());?>">
										<?php } ?>

									<td><input type="date" name="stopDate"  value="<?=(!empty($pricelistItem->stop_date)) ? date("Y-m-d",strtotime($pricelistItem->stop_date)) : "";?>" <?php if($pricelistItem->important == 1){echo "disabled";} ?>></td>
									<td><input type="text" name="price" value="<?=$pricelistItem->price?>"></td>
									<td>
										<input type="hidden" name="pricelistID" value="<?=$pricelistItem->id?>">
										<input type="submit" value="update" class="btn ">
										<?php if($pricelistItem->important == 0){ ?>
											<br><br><br>
											<a class="btn" href="/manager/delete-price-item/<?=$pricelistItem->id?>" class="delete_link" title="delete" onclick="if(!confirm('Are you sure?')) return false;">delete</a>
										<?php } ?>
									</td>
									</form>
								</tr>							
						<?php } ?>
						</tbody>
					</table>
				<?php }else{?>
					<p>No pricelist items</p>
				<?php } ?>
				<h3>Add new price line</h3>
				<form method="post" action="/manager/pricelist">
					<table class="price">
						<thead>
						<tr>
							<th>User Id</th>
							<th>Skin Id</th>
							<th>Plan Id</th>
							<th>Action</th>
							<th>Date From</th>
							<th>Date To</th>
							<th>Price</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td><input type="number" min="1" name="otherUserID" placeholder="all user"></td>
							<td>
								<select name="skinID">
									<?php  if(!empty($skinList)){
										foreach($skinList as $skin){
											$skin = (object) $skin; ?>
											<option value="<?=$skin->id?>" <?php if($skin->id == $pricelistItem->skin_id){echo " selected";}?>><?=$skin->code?></option>
										<?php }
									}?>
								</select>
							</td>
							<td>
								<select name="planID">
									<?php  if(!empty($planList)){
										foreach($planList as $plan){
											$plan = (object) $plan; ?>
											<option value="<?=$plan->id?>" <?php if($plan->id == $pricelistItem->plan_id){echo " selected";}?>><?=$plan->name?></option>
										<?php }
									}?>
								</select>
							</td>
							<td><select name="actionTypeID">
									<?php if(!empty($actionTypeList)){
										foreach($actionTypeList as $actionType){if(in_array($actionType->id,[4])){continue;}?>
											<option value="<?=$actionType->id?>"><?=$actionType->name?></option>
										<?php }
									}?>
								</select>
							</td>
							<td><input type="date" name="startDate" value="<?=date("Y-m-d",time());?>"></td>
							<td><input type="date" name="stopDate" value=""></td>
							<td><input type="number" name="price" required></td>
							<td><input type="submit" value="add" class="btn"></td>
						</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>