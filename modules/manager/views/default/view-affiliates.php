<?php
use app\modules\manager\controllers\ManagerController;

$session       = Yii::$app->session;
$referral_link = isset($userData->affiliates_info->referral_link) ? $userData->affiliates_info->referral_link : '';
$name          = isset($userData->affiliates_info->name) ? $userData->affiliates_info->name : '';
$m_phone       = isset($userData->affiliates_info->m_phone) ? $userData->affiliates_info->m_phone : '';
?>
<style>
    .user_avatar span.offline {
        background-color: #e94902;
    }
</style>

<div id="content-area">
    <div class="content-area-inner">

        <div class="content-area-inner-header">
            <h2 class="content-title"><?=$name?></h2>
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <div class="user_avatar margin-bottom">
                    <a href="/manager/edit-user/<?=$userData->userID?>" title="редактировать профиль">
                        <img src="<?=$userData->avatar->normal;?>">
                    </a>
                </div>
                <br>
                <div class="profile_information_block">
                    <table class="margin-top margin-bottom">
                        <tr>
                            <td>User ID:</td>
                            <td><span><?=$userData->userID;?></span></td>
                            <td>Email</td>
                            <td><?=$userData->email;?></td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td><?=$name?></td>
                            <td>Mob phone</td>
                            <td><?=$m_phone?></td>
                        </tr>
                        <tr>
                            <td>Referral code</td>
                            <td><?=$referral_link?></td>
                            <td>Referral link</td>
                            <td>
                                <a href="javascript:void(0);" id="referral_link" onclick="copyToClipboard(this);"
                                   style="cursor: pointer;"
                                   title="click to copy"><?="https://" . $_SERVER['SERVER_NAME'] . "?referral_link=" . $referral_link;?></a>
                            </td>
                        </tr>
                    </table>
                    <?php
                    $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved</span>';
                    switch ($userData->approve_status) {
                        case 1:
                            $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved</span>';
                            break;
                        case 2:
                            $approve_status = '<span style="color: lawngreen; margin-left: 10px;font-weight: bold;">Approve in progress</span>';
                            break;
                        case 3:
                            $approve_status = '<span style="color: #ffcc00; margin-left: 10px;font-weight: bold;">Approve delcine</span>';
                            break;
                        case 4:
                            $approve_status = '<span style="color: #ffcc00; margin-left: 10px;font-weight: bold;">Approved</span>';
                            break;
                        default:
                            $approve_status = '<span style="color: #ff0000; margin-left: 10px;font-weight: bold;">Not approved</span>';
                            break;
                    }
                    echo $approve_status; ?>

                </div>
            </div>
        </div>
    </div>
</div>
