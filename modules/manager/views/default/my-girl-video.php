<?php $ajax = Yii::$app->request->getIsAjax();
    if(!$ajax){    ?>

<div id="content-area">
    <div class="content-area-inner">
        
        <div class="content-area-inner-header">
            <h2 class="content-title">Users</h2>    
            <div>
                <button class="update-status btn btn-large">Обновить</button>
            </div>      
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <?php }                
                if(!empty($girls)){ ?>
                    <div class="cards-wrapper">
                    <div class="row">
                    <?php foreach($girls as $girl) {?>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <?=$this->render('/layouts/parts/camera-girl.php',['girl'=>$girl,'show_token_button'=>$show_token_button])?>
                    </div>
                    <?php }?>
                    </div>
                    </div>

                    <div class="col-xs-12 col-sm-6"><?=$this->render('/layouts/parts/admin_pagination.php',['count'=>$count,'pageName'=>$pageName,'page'=>$page]); ?></div>
                <?php }else{?>
                    <p>Agency have not user with active camera</p>
                <?php } 
                if(!$ajax){ ?>
            </div>
        </div>
        <div style="clear:both;"></div>
        <div class="content-area-inner-body">
            <input type="hidden" id="otherUserID">
            <div class="centered web-cam">
                
            </div>            
        </div>
    </div>
</div>
<?php } ?>
<style>
    #content-area .btn-large {
        margin-top: 20px;
        max-width: 250px;
    }
    [id^=video] {
        display: inline-block;
        width: 320px;
        height: 240px;
    }
    .centered {
        text-align: center;
    }
</style>

<?php

$this->registerJs(
    "$('.update-status').on('click', function() { adminAccountLibrary['userPagination'](1,'my-girl-video',''); }); setInterval(function() {
  adminAccountLibrary['userPagination'](1,'my-girl-video',''); }, 20000);"    
);

$this->registerJsFile(
    '@web/js/adapter.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
    '@web/js/admin/camera.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);