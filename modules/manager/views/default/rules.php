<?php
    use app\modules\manager\controllers\ManagerController;
    use yii\helpers\Html;
	$session = Yii::$app->session;
?>
<div id="content-area">
    <div class="content-area-inner">        
        <div class="content-area-inner-header">
            <h2 class="content-title">Rules page</h2>
        </div>
        <div class="content-area-inner-body">
            <div class="content-container">
                <?php if($session['user_type'] == ManagerController::USER_SUPERADMIN){?>
            <textarea id="text-memo"><?php if (!empty($pageContent->description)) {echo Html::decode($pageContent->description); } ?></textarea>
            <input id="page-name" type="hidden" value="rules" /> <br>
            <button id="save-page" class="btn pull-right">Save</button>
            <script>
                $(document).ready(function(){
                    if($('#text-memo').length > 0){
                        tinymce.init({
                            selector: "#text-memo",
                            plugins: "paste textcolor emoticons link code",
                            file_picker_callback: function(callback, value, meta) {
                                imageFilePicker(callback, value, meta);
                            },

                            paste_as_text: true,
                            skin: "custom",
                            height: "700",
                            menubar: false,
                            toolbar: "bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | forecolor backcolor | emoticons | undo redo | fullscreen | link code",
                            statusbar: true,
                            language: "en",
                        });
                    }
                });

                var imageFilePicker = function (callback, value, meta) {
                    tinymce.activeEditor.windowManager.open({
                            title: 'File and Image Picker',
                            url: '/manager/image-list',
                            width: 400,
                            height: 300,
                            buttons: [{
                                text: 'Insert',
                                onclick: function () {
                                    //do some work to select an item and insert it into TinyMCE
                                    tinymce.activeEditor.windowManager.close();
                                }
                            },
                                {
                                    text: 'Close',
                                    onclick: 'close'
                                }],
                        },
                        {
                            oninsert: function (url) {
                                callback(url);
                            }
                        });
                };
            </script>
            <?php }else{ ?>
                <div class="page-content"><?php if (!empty($pageContent->description)) {echo Html::decode($pageContent->description); } ?></div>
            <?php } ?>
            </div>
        </div>
    </div>  
</div>

