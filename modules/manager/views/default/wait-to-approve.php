<?php
	$session = Yii::$app->session;
	//var_dump($deletedPhotoList); die;
?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Ожидает подтверждения</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<h3>Users profile</h3>
				<div class="table_scroll_wrap">
					<table class="finance-leters-table">
						<thead>
						<th>ID</th>
						<th>имя фамилия</th>
						<th>последняя активность</th>
						<th>статус</th>
						<th>действие</th>
						</thead>
						<tbody>
						<?php if(!empty($usersList)){
							foreach($usersList as $user){ ?>
								<tr>
									<td><?=$user->id?></td>
									<td><?=trim($user->first_name . ' '.$user->last_name)?></td>
									<td style="text-align: center"><?=date('Y-m-d H:i:s',$user->last_activity)?></td>
									<td style="text-align: center"><?=$user->description?></td>
									<td style="text-align: center"><a href="/manager/edit-user/<?=$user->id?>">посмотреть</a></td>
								</tr>
							<?php }
						}else{?>
							<tr><td colspan="5">No results</td></tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
				<h3>User video</h3>
			    <div class="table_scroll_wrap">
				    <table class="finance-chat-table">
					    <thead>
							<th>Video ID</th>
							<th>ID пользователя</th>
							<th>имя фамилия</th>
							<th>дата добавления</th>
							<th>действие</th>
					    </thead>
					    <tbody>
						<?php if(!empty($videoList)){
							foreach($videoList as $video){ ?>
								<tr>
									<td><?=$video->id?></td>
									<td><?=$video->user_id?></td>
									<td><?=trim($video->first_name . ' '.$video->last_name)?></td>
									<td style="text-align: center"><?=$video->created_at?></td>
									<td style="text-align: center"><a href="/manager/video-gallery/<?=$video->user_id?>">посмотреть</a></td>
								</tr>
							<?php }
						}else{?>
							<tr><td colspan="5">No results</td></tr>
						<?php } ?>
					    </tbody>
				    </table>
			    </div>            
				<h3>User photos</h3>
			    <div class="table_scroll_wrap">
				    <table class="finance-video-chat-table">
					    <thead>
							<th>Photo ID</th>
							<th>ID пользователя</th>
							<th>имя фамилия</th>
							<th>дата добавления</th>
							<th>изображение</th>
							<th>действие</th>
					    </thead>
					    <tbody>
						<?php if(!empty($photoList)){
							foreach($photoList as $photo){ ?>
								<tr>
									<td><?=$photo->id?></td>
									<td><?=$photo->user_id?></td>
									<td><?=trim($photo->first_name . ' '.$photo->last_name)?></td>
									<td style="text-align: center"><?=$photo->created_at?></td>
									<td style="text-align: center"><img src="<?=$this->context->serverUrl.'/'.$photo->small_thumb;?>" /></td>
									<td style="text-align: center"><a href="/manager/photo/<?=$photo->id?>/<?= $photo->user_id ?>">посмотреть</a></td>
								</tr>
							<?php }
						}else{?>
							<tr><td colspan="6">No results</td></tr>
						<?php } ?>
					    </tbody>
				    </table> 
			    </div>
			    <h3>User deleted photos</h3>
			    <div class="table_scroll_wrap">
				    <table class="finance-gifts-table">
					    <thead>
							<th>Photo ID</th>
							<th>ID пользователя</th>
							<th>имя фамилия</th>
							<th>дата добавления</th>
							<th>изображение</th>
							<th>действие</th>
					    </thead>
					    <tbody>
						<?php if(!empty($deletedPhotoList)){
							foreach($deletedPhotoList as $deletedPhoto){ ?>
								<tr>
									<td><?=$deletedPhoto->id?></td>
									<td><?=$deletedPhoto->user_id?></td>
									<td><?=trim($deletedPhoto->first_name . ' '.$deletedPhoto->last_name)?></td>
									<td style="text-align: center"><?=$deletedPhoto->created_at?></td>
									<td style="text-align: center"><img src="<?=$this->context->serverUrl.'/'.$deletedPhoto->small_thumb;?>" /></td>
									<td style="text-align: center"><a href="/manager/photo/<?=$deletedPhoto->id?>/<?= $deletedPhoto->user_id ?>">посмотреть</a></td>
								</tr>
							<?php }
						}else{?>
							<tr><td colspan="6">No results</td></tr>
						<?php } ?>
					    </tbody>
				    </table> 
			    </div>               			
			</div>
		</div>
	</div>
</div>