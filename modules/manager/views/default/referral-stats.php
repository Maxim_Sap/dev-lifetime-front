<?php
$session = Yii::$app->session;
?>
<div id="content-area">
    <div class="content-area-inner">

        <div class="content-area-inner-header">
            <h2 class="content-title">Информация по привлечению пользователей</h2>
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <form action="" method="post" class="user-affiliates-form margin-top" style="width: 400px">
                    <table>
                        <tr>
                            <td><label>С:</label></td>
                            <td><input type="date" name="date-from" value="<?=$date_from;?>"></td>
                        </tr>
                        <tr>
                            <td><label>по:</label></td>
                            <td><input type="date" name="date-to" value="<?=$date_to;?>"></td>
                        </tr>
                        <?php if($user_type == 6){?>
                            <tr>
                                <td><label>Аффилейт:</label></td>
                                <td><select data-active-user-id="<?=$other_user_id?>" name="other_user_id" class="list-of-affiliates"></select></td>
                            </tr>
                        <?php }?>
                    </table>
                    <input type="submit" value="поиск" class="btn margin-bottom margin-top">
                </form>
                <?php
                //var_dump($stats);die;
                if(!empty($stats)){
                    $ress = [];
                    foreach ($stats as $one){
                        $day = date("d-m-Y",strtotime($one->date_create));
                        $user = $one->referral_link;
                        $user_name = $one->name;
                        $ress[$day][$user]['referral_link'] = $one->referral_link;
                        $ress[$day][$user]['user_id'] = $one->affiliates_id;
                        $ress[$day][$user]['full_name'] = $user_name;
                        $ress[$day][$user]['visit'] = (isset($ress[$day][$user]['visit'])) ? $ress[$day][$user]['visit'] : 0; //если нету то 0
                        $ress[$day][$user]['signup'] = (isset($ress[$day][$user]['signup'])) ? $ress[$day][$user]['signup'] : 0; //если нету то 0
                        if($one->type == 1){
                            $ress[$day][$user]['visit'] += 1;
                        }else{
                            $ress[$day][$user]['signup'] += 1;
                        }


                        /*
                        echo "<pre>".$one->date_create." ";
                        echo "Link: ".$one->referral_link." ";
                        echo "Visit: ".$one->visit_count." ";
                        echo "Sign-up: ".(!empty($one->signup_count) ? $one->signup_count : 0)." ";
                        echo "</pre>";*/
                    }
                    //var_dump($ress);die;
                    ?>

                    <table>
                        <thead>
                        <tr>
                            <th>Дата</th>
                            <th>Рефферальная ссылка</th>
                            <th>Имя аффилейта</th>
                            <th>Переходов</th>
                            <th>Из них регистраций</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($ress as $date => $user){
                            foreach($user as $reff => $data){
                                $curr_user = $data['referral_link'];
                                $referr = $data['referral_link'];
                                $user_id = $data['user_id'];
                                $full_name = $data['full_name'];
                                $visit = $data['visit'];
                                $signup = $data['signup'];
                                ?>
                                <tr>
                                    <td><?=$date?></td>
                                    <td align="center">
                                        <a href="/manager/edit-user/<?=$user_id?>" target="_blank" title="view profile"><?=$referr?></a>
                                    </td>
                                    <td><?=$full_name?></td>
                                    <td><?=$visit?></td>
                                    <td><?=$signup?></td>
                                </tr>
                            <?php }
                        }?>
                        </tbody>
                    </table>

                <?php }else{?>
                    <p>No results</p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
