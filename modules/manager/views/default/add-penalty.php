<?php
$session = Yii::$app->session;
?>
<div id="content-area">
    <div class="content-area-inner">        
        <div class="content-area-inner-header">
            <h2 class="content-title"><?=(isset($penaltyArray[0])) ? 'Edit penalty' : 'Add penalty' ;?></h2>            
        </div>

        <div class="content-area-inner-body">
            <div class="content-container">
                <form action="/manager/add-penalty" class="admin-add-penalty" style="width: 285px" method="post">
                    <select name="otherUserID" class="margin-bottom" style="width:100%">
                        <?php if (!empty($agencyArray)) {
                            foreach ($agencyArray as $agency) { ?>
                                <option value="<?=$agency->user_id?>" <?=(isset($penaltyArray[0]) && $penaltyArray[0]->user_to_id == $agency->user_id) ? 'selected': '';?>><?=$agency->name?></option>
                            <?php }
                        } ?>
                    </select>
                    <input type="text" class="margin-bottom" style="width:100%" name="penaltyValue" placeholder="penalty value" value="<?=(isset($penaltyArray[0]) ? $penaltyArray[0]->penalty_value: '');?>" required="required">
                    <textarea name="description" style="width:100%; height: 150px; margin-bottom: 20px" placeholder="description"><?=(isset($penaltyArray[0]) ? $penaltyArray[0]->description : '');?></textarea>
                    <?php if(isset($penaltyArray[0])){?>
                        <input type="hidden" value="<?=$penaltyArray[0]->id?>" name="penaltyID">
                        <input type="submit" value="Update penalty" class="search_inbox_btn btn pull-right" style="width:100% ">
                    <?php }else{?>
                        <input type="submit" value="Add penalty" class="search_inbox_btn btn pull-right" style="width:100% ">
                    <?php } ?>

                </form>
            </div>
        </div>
    </div>
</div>
