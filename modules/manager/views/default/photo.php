<?php
$session = Yii::$app->session;

?>
<style>
	.approved-status {
		position: absolute;
		top: 10px;
		left: 10px;	
		padding: 10px;	
	}
	.approved-status.yes {
		background: green;
	}
	.approved-status.no {
		background: red;
	}
	#content-area #crop_photo {
		margin-top:20px;
	}
</style>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Photo</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
					<?php if(!empty($photo)){ ?>
					<div class="breadcrumbs">
						<?php
						if(isset($photo->agency_user_id) && is_numeric($photo->agency_user_id)) {?>
							<a href="/manager/edit-user/<?=$photo->agency_user_id;?>"><?=$photo->name?> > </a>
						<?php } ?>
						<a href="/manager/user/<?=$photo->user_id;?>"><?=trim($photo->first_name.' '.$photo->last_name);?> > </a>
						<a href="/manager/album/<?=$photo->album_id;?>/<?=$photo->user_id;?>"><?=$photo->album_title;?> > </a>
						<?php if(isset($photo->title) && trim($photo->title) != ''){?>
							<span><?=$photo->title;?></span>
						<?php } ?>
						<br/><br/>
					</div>
		            <?php
					$img_original = empty($photo->original_image) ? '/img/no_image.jpg' : $this->context->serverUrl."/".$photo->original_image;
					$img_medium = empty($photo->medium_thumb) ? '/img/no_image.jpg' : $this->context->serverUrl."/".$photo->medium_thumb?>
						<div class="album_wrap view_foto" style="height: auto;overflow: inherit;width: 350px">
						    <div class="pos_rel">
						        <img id="myphoto" src="<?=$img_medium;?>" data-original-src="<?=$img_original;?>" data-normal-src="<?=$img_medium;?>" style="width: 100% !important;">
		                        <a onclick="if(confirm('Are you sure you want delete this photo?')){return true}else{return false;};" class="remove_btn pos_abs" href="/manager/photo-delete/<?=$photo->id;?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
								<?php if($session['user_type'] != 6){
								if ($photo->approve_status == 4) {?>
									<div class="approved-status yes">
										<span>approved</span>
									</div>
								<?php }elseif($photo->approve_status == 1 || $photo->approve_status == 2) { ?>
									<div class="approved-status no">
										<span>not approved</span>
									</div>
								<?php }elseif($photo->approve_status == 3){ ?>
									<div class="approved-status decline">
										<span>decline</span>
									</div>
								<?php }
								}?>
						    </div>
						    <div style="margin-top: 10px">
						        <form action="" class="photo-title-form margin-top">
						        	<input type="hidden" id="otherUserID" value="<?=$photo->user_id;?>" />
		                            <a style="display:none" id="set-album-cover" data-other-user-id="<?=$photo->user_id;?>" data-id="<?=$photo->id;?>" data-album-title="<?=$photo->album_id;?>" class="table_link fr" href="#">Set album cover</a>
									<a id="crop_photo" class="table_link fl btn" href="javascript:void(0);" onclick="window['CropImageLibrary']['cropPhoto']();">Crop photo</a>
									<a id="apply_crop_photo" class="table_link fl btn" style="display: none" href="javascript:void(0);" onclick="window['CropImageLibrary']['applyCrop']();">Apply crop photo</a>
									<br>							
									<input id="photo-id" type="hidden" value="<?=$photo->id;?>">
									<label style="" for="photo-title">Photo title</label> <br>
									<input style="width: 500px" id="photo-title" type="text" class="margin-bottom" value="<?=$photo->title;?>">
									<?php if($session['user_type'] == 6){?>
										<select id="approved" name="approve_status" style="width: 200px;display: block">
											<option <?php if($photo->approve_status == 1){echo "selected";}?> value="1">not approved</option>
											<option <?php if($photo->approve_status == 2){echo "selected";}?> value="2">in progress</option>
											<option <?php if($photo->approve_status == 4){echo "selected";}?> value="4">approved</option>
											<option <?php if($photo->approve_status == 3){echo "selected";}?> value="3">decline</option>
										</select>
										<br/>
										<input type="checkbox" id="user_deleted" name="deleted_status" <?php if($photo->status == 0){echo "checked";}?>>
										<label for="user_deleted">deleted in user album</label><br>
									<?php } ?>
										<input type="checkbox" id="premium" name="premium" <?php if($photo->premium == 1){echo "checked";}?>>
										<label for="premium">Premium only</label><br>
									<input class="custom_btn btn fr margin-bottom clear" style="width: 100%; margin-top: 10px" type="submit" value="Update">
						        </form>
						        </br>
						    </div>
						</div>
					<input id="x1" type="hidden" value="0">
					<input id="y1" type="hidden" value="0">
					<input id="x2" type="hidden" value="150">
					<input id="y2" type="hidden" value="169">
					<input id="w" type="hidden" value="150">
					<input id="h" type="hidden" value="169">
		        <?php }else{ ?>
					<p>No photo</p>
		        <?php } ?>  
			</div>
		</div>
	</div>
</div>