<?php $ajax = Yii::$app->request->getIsAjax();
	
	if(!$ajax){	?>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<?php }
				if(!empty($admins)){?>   
				<h2 class="content-title">My admin</h2>
					<?=$this->render('/layouts/parts/user-filter.php',['statusArray'=>$statusArray,'pageName'=>$pageName,'curr_search_status'=>$curr_search_status])?>
					<div class="cards-wrapper">
						<div class="row">
					<?php foreach($admins as $admin){?>
						<div class="col-xs-12 col-sm-3 col-md-3">
						<?=$this->render('/layouts/parts/admin-single.php',['admin'=>$admin])?>
						</div>
					<?php }?>
						</div>
					</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<?=$this->render('/layouts/parts/admin_pagination.php',['count'=>$count,'pageName'=>$pageName,'page'=>$page]); ?>
							</div>
						</div>										
					<?php } else {?>
					<p>You have not admins</p>
					<a class="btn" href="/manager/add-user?user_type=7">Add new admin</a>
				<?php } ?>
			<?php if(!$ajax){ ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>