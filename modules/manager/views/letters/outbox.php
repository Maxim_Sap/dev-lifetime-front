<?php
	$addUrl = ($otherUserID !== null) ? "/".$otherUserID : '';
?>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Письма: Исходящие</h2>
			<a href="/manager/letters/inbox<?=$addUrl?>" class="btn active admin-tab">Входящие</a>
			<a href="/manager/letters/outbox<?=$addUrl?>" class="btn admin-tab">Исходящие</a>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<?=$this->render('letters-inner.php',['lettersArray'=>$lettersArray,'page_name'=>'outbox','lettersCount'=>$lettersCount,'page'=>$page,'limit'=>$numberOfMessagesOnAdminPage, 'agencyUsers' => $agencyUsers])?>
					<input type="hidden" id="numberOfMessagesOnAdminPage" value="<?=$numberOfMessagesOnAdminPage;?>">
					<input type="hidden" id="otherUserID" value="<?=($otherUserID !== null)?$otherUserID:"";?>">
			</div>
		</div>
	</div>
</div>