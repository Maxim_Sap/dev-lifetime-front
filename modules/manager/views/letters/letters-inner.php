<?php
use app\modules\manager\controllers\ManagerController;
$session = Yii::$app->session;
?>
<style>

</style>
<form action="#" method="post" id="admin-letters-search" class="admin-custom-border admin-letters-search margin-top">
	<table cellspacing="3" cellpadding="0" border="0" align="center">
		<tbody><tr>
				<td align="right">Письма</td>
				<td>
					<select name="answered">
						<option value="">все</option>
						<option value="0" selected="selected">неотвеченные</option>
						<option value="1">отвеченные</option>
					</select>
				</td>
				<td></td>
				<td>
					<select name="read">
						<option value="">все</option>
						<option value="1">прочитанные</option>
						<option value="0">не прочитанные</option>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right">ID отправителя</td>
				<td>
					<?php if($page_name == 'outbox' && $session['user_type'] != ManagerController::USER_SUPERADMIN && 
							 !empty($agencyUsers)){?>
						<select name="user_id_from" class="my-users">
							<option value="">любой</option>					
							<?php foreach ($agencyUsers as $user) { ?>
								<option value="<?= $user->id?>"><?= $user->id . ' ' . $user->first_name . ' ' . $user->last_name?></option>
							<?php } ?>
						</select>
					<?php }else{?>
						<input type="text" name="user_id_from">
					<?php } ?>
				</td>
				<td align="right">Дата от</td>
				<td><input type="date" name="date_from"></td>
			</tr>
			<tr>
				<td align="right">ID получателя</td>
				<td>
					<?php if($page_name == 'inbox'  && $session['user_type'] != ManagerController::USER_SUPERADMIN && 
							!empty($agencyUsers)){?>
						<select name="user_id_to" class="my-users">
							<option value="">любой</option>					
							<?php foreach ($agencyUsers as $user) { ?>
								<option value="<?= $user->id?>"><?= $user->id . ' ' . $user->first_name . ' ' . $user->last_name?></option>
							<?php } ?>
						</select>
					<?php }else{?>
						<input type="text" name="user_id_to">
					<?php } ?>
				</td>
				<td align="right">Дата до</td>
				<td><input type="date" name="date_to"></td>
			</tr>
			<tr>
				<td align="right" colspan="4">
					<input type="hidden" name="box" value="<?=$page_name?>">
					<input type="submit" class="btn search_inbox_btn admin-custom-btn" value="Поиск" name="submit">
				</td>
			</tr>
		</tbody></table>
</form>
<br>
	<table class="data-table">
		<thead>
			<tr>
				<th style="text-align:center;">ID письма</th>
				<th style="text-align:center;">от кого</th>
				<th style="text-align:center;">кому</th>
				<th style="text-align:center;">дата</th>
				<th style="text-align:center;">прочитано</th>
				<th style="text-align:center;">отвечено</th>
				<th style="text-align:center;">действия</th>
			</tr>
		</thead>
		<tbody id="letters-list">
			<?php if(isset($lettersArray) && !empty($lettersArray)){
				foreach($lettersArray as $letter){
					$readStatus = !empty($letter->readed_at) ? 'yes' : 'no';
					$answerStatus = ($letter->reply_status == 1) ? 'yes' : 'no';
				?>
				<tr>
					<td class="message-id"><?=$letter->id?></td>
					<td>
						<span class="message-from">
							<a href="/manager/user/<?=$letter->user_from_id?>" class="table_link"><?=$letter->user_from_name?></a>
							[ID: <?=$letter->user_from_id?>]
						</span>
					</td>
					<td>
						<span class="message-to">
							<a href="/manager/user/<?=$letter->user_to_id?>" class="table_link"><?=$letter->user_to_name?></a>
							[ID: <?=$letter->user_to_id?>]
						</span>
					</td>
					<td class="data-create"><?=$letter->created_at?></td>
					<td class="data-read" data-read="<?=$letter->readed_at;?>"><?=$readStatus?></td>
					<td class="answered"><?=$answerStatus?></td>
					<td>
						<input class="caption" type="hidden" value="<?=$letter->theme;?>" />
						<div class="desc" hidden><?=$letter->description;?></div>
						<a class="view-letter btn" href="#">читать</a>
						<a class="btn" href="/manager/letters/history/<?=$letter->id?>">переписка</a>
						<?php if (Yii::$app->controller->action->id == 'inbox') {?>
						<br> <br>
						<a class="btn" href="/manager/letter/<?=$letter->id?>">ответить</a>
						<a class="btn" href="#" onclick="window['messageAdminLibrary']['deleteSingleMessage'](<?= $letter->id ?>, <?=$letter->user_to_id?>)">удалить</a>
						<?php } ?>
					</td>
				</tr>
				<?php }
			} ?>
		</tbody>
	</table>
	<div class="col-xs-12 col-sm-6 admin-message-pagination">
		<?php if(isset($lettersArray) && !empty($lettersArray)){
			echo $this->render('/layouts/parts/pagination.php',['letters_count'=>$lettersCount,'page'=>$page,'page_name'=>$page_name,'limit'=>$limit]);
			} ?>
	</div>
<?php 	
$this->registerJs(
    "$('#admin-letters-search .search_inbox_btn').click();"  
);