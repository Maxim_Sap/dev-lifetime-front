<?php //var_dump($message_mass);
if(empty($letterArray)){ ?>
	<div id="content-area">
		<div class="content-area-inner">
			<div class="content-area-inner-header">
				<h2 class="content-title">No message</h2>			
			</div>			
			<div class="content-area-inner-body">
				<div class="content-container">
				</div>
			</div>
		</div>
	</div>
<?php }else{
$letterArray = $letterArray[0];
$answer_status = ($letterArray->reply_status == 1) ? 'yes' : 'no';
$read = ($letterArray->readed_at !== null) ? $letterArray->readed_at : 'no';
$label = 'Message from: ';

?>
<style>
	.product-description {
		border: 1px solid #ccc;
		padding: 10px;
	}

</style>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Reply to message</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<div class="card">
					<div class="container-fliud">
						<div class="wrapper row">
							<div class="details col-md-12">
								<h3 class="product-title">Caption: <b class="caption"><?=$letterArray->theme?></b></h3>	
								<h6 class="product-title">Message ID: <b><?=$letterArray->id?></h6>								
								<h6 class="product-title"><?=($label) ? $label : 'Message to: ';?><b><?=trim($letterArray->user_from_name)?></h6>								
								<h6 class="product-title">Message read at: <b class="data-read grey_text"><?=$read?></h6>						
								<h6 class="product-title">Created At: <b class="data-create grey_text"><?=$letterArray->created_at?></h6>	
								<h6 class="product-title">Answered: <b class="answered grey_text"><?=$answer_status?></b></h6>			
								<h4>Содержание:</h4>	
								<div class="product-description"><?=$letterArray->description?></div>											
							</div>
						</div>
					</div>
				</div>
				<?php if($label){?>
					<br>
					<h3>Your answer:</h3>
					<div style="min-height: 300px;">
						<form method="post" action="" class="admin-message">
							<label>Caption:</label>
							<input type="text" name="caption" value="<?=$letterArray->theme?>" required>
							<textarea name="description"></textarea>
							<input type="hidden" name="messageID" value="<?=$letterArray->id?>">
							<input type="hidden" name="fromUserID" value="<?=$letterArray->user_to_id?>">
							<input type="hidden" name="toUserID" value="<?=$letterArray->user_from_id?>">
							<input type="hidden" name="themeID" value="<?=$letterArray->theme_id?>">
							<br>
							<button class="pull-right btn margin-top">Send</button>
						</form>
						<div class="clear"></div>
					</div>
				<?php } 
				}
				?>			
			</div>
		</div>
	</div>
</div>
