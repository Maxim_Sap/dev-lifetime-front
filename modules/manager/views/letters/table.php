<?php if (isset($lettersArray) && !empty($lettersArray)){
	foreach($lettersArray as $letter){		
		$readStatus = !empty($letter['readed_at']) ? 'yes' : 'no';
		$answerStatus = ($letter['reply_status'] == 1) ? 'yes' : 'no';?>
		<tr>
			<td class="message-id"><?=$letter['id']?></td>
			<td>
				<span class="message-from">
					<a class="table_link" href="/manager/user/<?=$letter['user_from_id']?>"><?=$letter['user_from_name']?></a>
					[ID: <?=$letter['user_from_id']?>]
				</span>
			</td>
			<td>
				<span class="message-to">
					<a class="table_link" href="/manager/user/<?=$letter['user_to_id']?>"><?=$letter['user_to_name']?></a>
					[ID: <?=$letter['user_to_id']?>]
				</span>
			</td>
			<td class="data-create"><?=$letter['created_at']?></td>
			<td class="data-read" data-read="<?=$letter['readed_at'];?>"><?=$readStatus?></td>
			<td class="answered"><?=$answerStatus?></td>
			<td>
				<input class="caption" type="hidden" value="<?=$letter['theme'];?>">
				<div class="desc" hidden><?=$letter['description'];?></div>
				<a class="view-letter btn" href="#">читать</a>
				<a class="btn" href="/manager/letters/history/<?=$letter['id']?>">переписка</a> 
				<?php if ($inbox) {?>
				<br> <br>
				<a class="btn" href="/manager/letter/<?=$letter['id']?>">ответить</a>
				<a class="btn" href="#" onclick="window['messageAdminLibrary']['deleteSingleMessage'](<?= $letter['id'] ?>, <?=$letter['user_to_id']?>)">удалить</a>
				<?php } ?>
			</td>
		</tr>
	<?php }
}?>