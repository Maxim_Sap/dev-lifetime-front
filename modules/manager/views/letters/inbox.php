<?php
	use app\modules\manager\controllers\ManagerController;
	$session = Yii::$app->session;
	$addUrl = ($otherUserID !== null) ? "/".$otherUserID : '';
?>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Письма: Входящие</h2>
			<a href="/manager/letters/inbox<?=$addUrl?>" class="btn active admin-tab">Входящие</a>
			<a href="/manager/letters/outbox<?=$addUrl?>" class="btn admin-tab">Исходящие</a>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<?php if ($unrepliedLetters > 0 && in_array($session['user_type'], [ManagerController::USER_ADMIN, ManagerController::USER_AGENCY])) {?>
				<div class="alert alert-danger">
					<p>Your agency has received letters without replies.</p>
					<p>You can delete not replied letters, received in last 30 days.</p>
					<p>You can delete any number of replied letters.</p>
					<p>You can delete any number of letters received over last 30 days.</p>
					<p>Any not replied, not deleted letter will be fined after 7 days after receiving.</p>
					<p>Replied letters don't get fines; deleted letters don't get fined.</p>
				</div>
				<?php } ?>
				<?=$this->render('letters-inner.php',['lettersArray'=>$lettersArray,'page_name'=>'inbox','lettersCount'=>$lettersCount,'page'=>$page,'limit'=>$numberOfMessagesOnAdminPage, 'agencyUsers' => $agencyUsers])?>
				<input type="hidden" id="numberOfMessagesOnAdminPage" value="<?=$numberOfMessagesOnAdminPage;?>">
				<input type="hidden" id="otherUserID" value="<?=($otherUserID !== null)?$otherUserID:"";?>">
			</div>
		</div>
	</div>
</div>
