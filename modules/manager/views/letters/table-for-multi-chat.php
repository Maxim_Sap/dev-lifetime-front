<?php if (isset($lettersArray) && !empty($lettersArray)){?>
	<table>
		<thead>
			<tr>
				<th><?=($type=='inbox' ? 'от кого': 'кому');?></th>
				<th>дата, время</th>
				<th>прочитано</th>
				<th>отвечено</th>
				<th>действие</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($lettersArray as $letter) {
				$readStatus = !empty($letter['readed_at']) ? 'да' : 'нет';
				$answerStatus = ($letter['reply_status'] == 1) ? 'да' : 'нет';?>
				<tr>
					<td>
						<span class="<?=($type=='inbox' ? 'message-from': 'message-to');?>">
							<a class="table_link" target="_blank" href="/manager/user/<?=$letter['another_user_id']?>"><?=$letter['another_user_name']?></a>
							[ID: <?=$letter['another_user_id']?>]
						</span>
					</td>
					<td class="data-create"><?=$letter['created_at']?></td>
					<td class="data-read" data-read="<?=$letter['readed_at'];?>"><?=$readStatus?></td>
					<td class="answered"><?=$answerStatus?></td>
					<td>
						<div class="message-id" hidden><?=$letter['id'];?></div>
						<input type="hidden" class="<?=($type!='inbox' ? 'message-from': 'message-to');?>" value='' />
						<input type="hidden" class="caption" value='<?=$letter['theme'];?>' />
						<input type="hidden" class="theme_id" value='<?=$letter['theme_id'];?>' />
						<input type="hidden" class="other_user_id" value='<?=$letter['another_user_id']?>' />
						<div class="desc" hidden><?=$letter['description'];?></div>
						<a class="view-letter admin-table-btn <?=(($type=='inbox' && empty($letter['readed_at'])) ? 'read': '');?>" href="#">читать</a>
						<?php if($type == 'inbox'){?>
							<a class="admin-table-btn reply" href="#">ответить</a>
						<?php } ?>

					</td>
				</tr>
			<?php }?>
		</tbody>
	</table>
<?php }else{?>
	<p>No letters</p>
<?php }?>