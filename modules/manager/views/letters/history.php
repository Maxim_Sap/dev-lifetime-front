<?php
$user1_name = $user2_name = '';
if(!empty($lettersArray)){
	 $user1_name = $lettersArray[0]->from_user_name;
	 $user1_avatar = !empty($lettersArray[0]->from_user_avatar) ? $this->context->serverUrl.'/'.$lettersArray[0]->from_user_avatar : '/img/no_avatar_normal.jpg';
	 $user1_id = $lettersArray[0]->from_user_id;
	 $user2_name = $lettersArray[0]->user_to_name;
	 $user2_avatar = !empty($lettersArray[0]->user_to_avatar) ? $this->context->serverUrl.'/'.$lettersArray[0]->user_to_avatar : '/img/no_avatar_normal.jpg';
	 $user2_id = $lettersArray[0]->to_user_id;
 }

?>
<style>
	#tool-bar {
		min-height: 1100px!important;
	}
	.name {
		text-align: center;
	}
	.text{
	    width:75%;display:flex;flex-direction:column;
	}
	.text > p small{
	    width:100%;text-align:right;color:silver;margin-bottom:-7px;margin-top:auto;
	}
	.text-l{
	    float:left;padding-right:10px;
	}        
	.text-r{
	    float:right;padding-left:10px;
	}
	.avatar{
	    display:flex;
	    justify-content:center;
	    align-items:center;
	    width:25%;
	    float:left;
	    padding-right:10px;
	}
	.macro{
	    margin-top:5px;width:85%;border-radius:5px;padding:5px;display:flex;
	}
	.msj-rta{
	    float:right;background:whitesmoke;
	}
	.msj{
	    background:white;
	}
	#content-area .content-area-inner {
		margin: 0 30px;
		float: left;
	}
	ul.messages {
	    width:100%;
	    list-style-type: none;
	    padding:18px;
	    background: #e0e0de;
	    float: left;
	}
	ul.messages li {
		float: left;
	}
	.msj:before{
	    width: 0;
	    height: 0;
	    content:"";
	    top:-5px;
	    left:-14px;
	    position:relative;
	    border-style: solid;
	    border-width: 0 13px 13px 0;
	    border-color: transparent #ffffff transparent transparent;            
	}
	.msj-rta:after{
	    width: 0;
	    height: 0;
	    content:"";
	    top:-5px;
	    left:14px;
	    position:relative;
	    border-style: solid;
	    border-width: 13px 13px 0 0;
	    border-color: whitesmoke transparent transparent transparent;           
	}  
</style>
<div id="content-area">
	<div class="content-area-inner">		
		<input type="hidden" id="history-items" value="<?= $numberOfHistoryMessages ?>">
		<input type="hidden" id="letterID" value="<?= $letterID ?>">
		<div class="content-area-inner-header">
			<h2 class="content-title">Письма: История переписки <a href="/manager/user/<?=$user1_id?>" title="view user"><?=$user1_name?></a> и <a href="/manager/user/<?=$user2_id?>" title="view user"><?=$user2_name?></a> (<?=$lettersCount?> писем)</h2>			
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<?php if(!empty($lettersArray)){?>
				<div class="col-xs-6 col-md-6">
					<div class="pull-left">
						<a href="/manager/user/<?=$user1_id?>" title="view user">							
							<img data-user="<?=$user1_id?>" class="user1 img-responsive" src="<?=$user1_avatar?>">
							<p class="name"><?=$user1_name?></p>
						</a>
					</div>
				</div>
				<div class="col-xs-6 col-md-6">
					<div class="pull-right">
						<a href="/manager/user/<?=$user2_id?>" title="view user">
							<img data-user="<?=$user2_id?>" class="user2 img-responsive" src="<?=$user2_avatar?>">
							<p class="name"><?=$user2_name?></p>
						</a>
					</div>
				</div>
				<div class="col-xs-12">
					<ul class="messages">
					<?php foreach($lettersArray as $letter){?>					
						<?php if ($user1_id == $letter->from_user_id) { ?>
							<li style="width:100%">
								<div class="msj macro">
									<div class="avatar">
										<img class="img-circle" style="width:100%;" src="<?= $user1_avatar ?>">
									</div>
									<div class="text text-l">
										<h3>Тема письма: <?=$letter->theme?></h3>
										<p>Содержание письма:</p>
										<div><?=$letter->description?></div>
										<p><small>Дата создания: <?=$letter->created_at?></small></p>
										<p><small>Дата прочтения: <?=$letter->readed_at?></small></p>
									</div>
								</div>
							</li>
						<?php }	else if ($user2_id == $letter->from_user_id) {?>
							<li style="width:100%">
								<div class="msj-rta macro">									
									<div class="text text-r">
										<h3>Тема письма: <?=$letter->theme?></h3>
										<p>Содержание письма:</p>
										<div><?=$letter->description?></div>
										<p><small>Дата создания: <?=$letter->created_at?></small></p>
										<p><small>Дата прочтения: <?=$letter->readed_at?></small></p>
									</div>
									<div class="avatar">
										<img class="img-circle" style="width:100%;" src="<?= $user2_avatar ?>">
									</div>
								</div>
							</li>
						<?php } ?>					
					<?php } ?>
					</ul>
				</div>
			<?php }?>
			<?php if(isset($lettersArray) && !empty($lettersArray) && $lettersCount > $numberOfHistoryMessages){                
                        echo $this->render('/layouts/parts/pagination.php',['letters_count'=>$lettersCount,'page'=>1,'page_name'=>'history','limit'=>$numberOfHistoryMessages]);
                    } ?>
			</div>
		</div>
	</div>
</div>
