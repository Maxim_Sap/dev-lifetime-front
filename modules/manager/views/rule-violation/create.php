<?php
use yii\helpers\Url;

use mihaildev\ckeditor\CKEditor;

use mihaildev\elfinder\ElFinder;

?>

<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">
				Add rule violation to agency - <?= $agencyName ?>
			</h2>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<section class="profile-container admin-content"> 
				                                      
				<div class="profile_information_block">
					<form action="<?= Url::current(); ?>" method="post">  
						<label for="violation-description">Rule violation description</label>                          
						<div class="textarea">
							<?php
		                        echo CKEditor::widget([
		                            'name' => 'rule-violation',
		                            'value'=>'',
		                            'id' => 'violation-description',
		                            'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
		                                /* Some CKEditor Options */
		                                'preset' => 'basic',
		                                'inline' => FALSE,
		                                'height' => 500,
		                            ]),
	                        ]);?>                        	
                        </div>
						<button class="btn pull-right">Create</button>
					</form>
				</div> 
				</section>
			</div>
		</div>
	</div>
</div>

<style>
	.textarea {
		margin: 20px 0;
	}
</style>