<?php

use mihaildev\ckeditor\CKEditor;

use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div id="content-area">
	<div class="content-area-inner">
		
		<div class="content-area-inner-header">
			<h2 class="content-title">Rule violation for agency - <?= $agencyName ?></h2>
		</div>

		<div class="content-area-inner-body">
			<div class="content-container">
				<section class="profile-container admin-content"> 
			    <input type="hidden" id="ruleID" value="<?= $ruleViolation->id ?>"                             
				<div class="profile_information_block">                                                             
					<?php
                        echo CKEditor::widget([
                            'name' => 'rule-violation',
                            'value'=> (!empty($ruleViolation->description)) ? Html::decode($ruleViolation->description) : '',
                            'id' => 'violation-description',
                            'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                                /* Some CKEditor Options */
                                'preset' => 'basic',
                                'inline' => FALSE,
                                'height' => 500,
                            ]),
                    ]);?> 
                    <br>      
                    <div class="pull-right">
                    <a class="btn update-rule" href="#">Update</a>&nbsp;
                    <a class="btn delete-rule" href="#">Delete</a>
                    <button class="btn" onclick="window.history.back();">Назад</button>
					</div>
					
				</div> 
				</section>
			</div>
		</div>
	</div>
</div>