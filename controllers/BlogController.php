<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\controllers\BaseController;
use yii\filters\VerbFilter;


class BlogController extends BaseController
{   
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
	{   
        $arrayOfActionsWithDisabledCSRFValidation = [
            'index'];
        if (in_array($action->id, $arrayOfActionsWithDisabledCSRFValidation)) {
            $this->enableCsrfValidation = false;
        }
	    return parent::beforeAction($action);
	}
	
    public function init()
    {
		parent::init();
        $this->view->registerJsFile('/js/common.js', ['depends' => [\app\assets\AppAsset::className()]]);

		$footer_data = json_decode(self::sendCurl('', [], "/site/get-footer-data"));        
		$this->view->params['footer_post'] = $footer_data->posts;
		$this->view->params['footer_history'] = $footer_data->history_page;
    }

    public function actions()
    {
		return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex($postId = null)
    {        
		if($postId){
			$posts = json_decode(self::sendCurl('', ['postId'=>$postId], "/site/get-posts"));
			return $this->render('single', [
				'post' => $posts,
			]);
		}else{
            $ajax    = Yii::$app->request->getIsAjax();
            $page = Yii::$app->request->post('page');

            if (!isset($page) || !is_numeric($page)) {
                $page = 1;
            } else {
                $page = (int)$page;
            }
            $search = Yii::$app->request->post('search');

            $blog_category = ['story','post'];
            $params = [];
            if(isset($_GET['category']) && in_array($_GET['category'],$blog_category)){
                $params['params'] = ['category' => $_GET['category']];
            }
            $params['page'] = $page;
            $params['limit'] = Yii::$app->params['numberOfBlogItems'];
            $params['search'] = $search;
            
            $posts = json_decode(self::sendCurl('', $params, "/site/get-posts"));
            //var_dump(self::sendCurl('', $params, "/site/get-posts")); die;
            //return json_encode($posts);
            if (isset($posts->success) && $posts->success != 'true') {
                $session = Yii::$app->session;
                $session->set('message', $posts->message);
                return $this->redirect('/', 301);
            }   
            if ($ajax) {
                $result = Yii::$app->controller->renderPartial('index', [
                    'postList' => $posts->postList,
                    'count' => $posts->count,
                    'page' => $page
                ]);
                return json_encode([
                    'success' => TRUE, 
                    'code' => 1, 
                    'html' => $result
                ]);
            } else {
                return $this->render('index', [
                    'postList' => $posts->postList,
                    'count' => $posts->count,
                    'page' => $page
                ]);
            }            
		}

    }


}
