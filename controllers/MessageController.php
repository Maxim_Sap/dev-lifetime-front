<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\controllers\BaseController;
use yii\filters\VerbFilter;
use app\models\UploadForm;
use yii\web\BadRequestHttpException;

class MessageController extends BaseController
{
	
	public $numberOfMessagesOnPage;
		
    public function init()
    {
        parent::init();
        $this->numberOfMessagesOnPage = Yii::$app->params['numberOfMessagesOnPage'];
		$footer_data = json_decode(self::sendCurl('', [], "/site/get-footer-data"));
		$this->view->params['footer_post'] = $footer_data->posts;
		$this->view->params['footer_history'] = $footer_data->history_page;
        $this->view->registerJsFile('/js/bootstrap-datepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerCssFile('/css/datepicker.css');
        $this->view->registerCssFile('/css/jquery-ui.min.css');
        $this->view->registerJsFile('/js/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/message.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/jquery-ui.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    }
    
    public function beforeAction($action)
	{            
		$this->enableCsrfValidation = false;
	    return parent::beforeAction($action);
	}
    
    public function actionIndex()
    {
    	$session = Yii::$app->session;
		$ajax = Yii::$app->request->getIsAjax();
		
    	if (!$this->checkIfUserLoggedIn()) {            
            $this->redirect('/', 301);
        } 
		
		$offset = 0;
		$page = 1;		
		$searchParams = null;
		
		if (isset($session['letters_search_params'])) {
			unset($session['letters_search_params']);
		};
		
		if ($ajax) {
			$page = Yii::$app->request->post('page');			
			$searchParams = Yii::$app->request->post('searchParams');
			$session->set('letters_search_params', $searchParams);
		}
		
		if (empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfMessagesOnPage;
		$params = [
	        'skinID' => 1,	       
	        'limit' => $this->numberOfMessagesOnPage,
	        'offset' => $offset,
	        'type' => 'inbox',	        
	        'searchParams' => $searchParams,
        ];
		
		$result = json_decode(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); 
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/letter/get-letters")); die;
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
        }

		$params = [
	        'skinID' => 1,	        
	        'limit' => $this->numberOfMessagesOnPage,
	        'offset' => $offset,
	        'type' => 'outbox',	        
	        'searchParams' => $searchParams,
        ];

        $result2 = json_decode(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters"));
		//var_dump(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); die;
		
		if (!self::checkCurlResponse($result2)) {
			return $this->redirect('/', 301);
        }

        $params = [
	        'limit' => $this->numberOfMessagesOnPage,
	        'offset' => $offset,
	        'type' => 'system'
        ];
		
		$result3 = json_decode(self::sendCurl($session['user_token'], $params, "/v1/message/get-messages")); 
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/message/get-messages")); die;	
		if (!self::checkCurlResponse($result3)) {
			return $this->redirect('/', 301);
        }
				
		$params = [
	        'skinID' => 1,	        
	        'limit' => $this->numberOfMessagesOnPage,
	        'offset' => $offset,
	        'type' => 'deleted',
	        'searchParams' => $searchParams,
        ];
		
		$result4 = json_decode(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); 
		//var_dump(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); die;
		if (!self::checkCurlResponse($result4)) {
			return $this->redirect('/', 301);
        }
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, yii\web\View::POS_READY);
		$this->verticalMenuName = "inbox";
		$this->horizontalMenuName = "message";
		
		if ($ajax) {
			$messages = Yii::$app->controller->renderPartial('//message/inbox.php', [
				'inboxLettersArray' => $result->lettersArray, 
				'inboxLettersCount' => $result->countLetters,
				'outboxLettersArray'  => $result2->lettersArray, 
				'outboxLettersCount' => $result2->countLetters,
				'deletedLettersArray' => $result4->lettersArray,
				'deletedLettersCount' => $result4->countLetters, 
				'systemLettersArray' => $result3->messageArray,
				'systemLettersCount' => $result3->count,
				'featuredIDs' => $result->featuredIDs, 
				'blackListIDs' => $result->blackListIDs,
				'page' => $page
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $messages;
		}
        
		return $this->render('inbox', [
			'inboxLettersArray' => $result->lettersArray, 
			'inboxLettersCount' => $result->countLetters,
			'outboxLettersArray'  => $result2->lettersArray, 
			'outboxLettersCount' => $result2->countLetters,
			'deletedLettersArray' => $result4->lettersArray,
			'deletedLettersCount' => $result4->countLetters,			
			'systemLettersArray' => $result3->messageArray,
			'systemLettersCount' => $result3->count,
			'featuredIDs' => $result->featuredIDs, 
			'blackListIDs' => $result->blackListIDs, 
			'page' => $page,
			'limit' => $this->numberOfMessagesOnPage
		]);
    }
    
    public function actionNew()
    {
    	return $this->redirect('/messages');
    	$session = Yii::$app->session;
		$ajax = Yii::$app->request->getIsAjax();
				
    	if (!$this->checkIfUserLoggedIn()) {            
            $this->redirect('/', 301);
        }
		
		$offset = 0;
		$page = 1;		
		$searchParams = null;
		
		if (isset($session['letters_search_params'])) {
			unset($session['letters_search_params']);
		};
		
		if ($ajax) {
			$page = Yii::$app->request->post('page');
			$searchParams = Yii::$app->request->post('search_params');
			$session->set('letters_search_params', $searchParams);
		}
		
		if (empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfMessagesOnPage;
		$params = [
	        'skinID' => 1,	        	        
	        'limit' => $this->numberOfMessagesOnPage,
	        'offset' => $offset,
	        'type' => 'new',	        
	        'searchParams' => $searchParams,
        ];
		
		$result = json_decode(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); 
		//var_dump(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); die;
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
        }
        
        $this->verticalMenuName = "new";
		$this->horizontalMenuName = "message";
        
        if ($ajax) {        	
			$messages = Yii::$app->controller->renderPartial('//message/new.php', [
				'lettersArray' => $result->lettersArray, 
				'lettersCount' => $result->countLetters, 
				'featuredIDs' => $result->featuredIDs, 
				'blackListIDs' => $result->blackListIDs,
				'page' => $page
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $messages;
		}
		
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, Yii\web\View::POS_READY);
                
        return $this->render('new', [
			'lettersArray' => $result->lettersArray, 
			'lettersCount' => $result->countLetters, 
			'featuredIDs' => $result->featuredIDs, 
			'blackListIDs' => $result->blackListIDs,
        	'page' => $page,
        	'limit' => $this->numberOfMessagesOnPage
        ]);
		
    }
    
    public function actionSingle($messageID)
    {
		$session = Yii::$app->session;
		$ajax = Yii::$app->request->getIsAjax();				
		
		$params = [
	        'type' => 'single',
	        'letterID' => $messageID,
        ];
		
		$result = $this->getResponseFromAPI($params, "/v1/letter/get-letters");
		//var_dump(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); die; 
			
		if (!isset($result->lettersArray[0]) || empty($result->lettersArray[0])) {			
			$session->set('message', 'Letter not exists or was deleted');
			return $this->redirect('/', 301);
		}
		
		if ($result->lettersArray[0]->another_user_avatar != null) {
			$result->lettersArray[0]->another_user_avatar = $this->serverUrl . "/" . $result->lettersArray[0]->another_user_avatar;
	    } else {
			$result->lettersArray[0]->another_user_avatar = "/img/no_avatar_normal.jpg";
	    }
		
		$anotherUserID = $result->lettersArray[0]->another_user_id;
		
		$lettersType = isset($result->letterType->type) ? $result->letterType->type : null;
		
		$readMessageScript = "window['messageLibrary']['getLettersAlbum'](); " . self::JS_GET_MESSAGE_COUNT;
		
		if ($lettersType != 'outbox') {
			$readMessageScript .= "window['messageLibrary']['readMessage']($messageID);";
		}
		
		$this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJs($readMessageScript, yii\web\View::POS_READY);
						
		if ($ajax) {
			$messages = Yii::$app->controller->renderPartial('//message/message-template.php', [
				'lettersData'=>$result->lettersArray[0],
				'next_prev'=>$result->next_prev_letters_ids
			]);
        	
        	$from = Yii::$app->params['adminEmail']; 
        	if (YII_DEBUG) {
        		$to = 'darkstill@ukr.net';
        	} else {
        		$to = $session['user_data']['email'];
        	}
        	
        	if ($session['user_data']['email'] == "") {
        		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        		return ['message'=>'Please add your email in account cabinet'];
			}
        	
			$emailSendSuccessfully = Yii::$app->mailer->compose()
		        ->setFrom($from)
		        ->setTo($to)
		        ->setSubject('message from '.$_SERVER['SERVER_NAME'])
		        ->setHtmlBody($messages)
		        ->send();
        	if ($emailSendSuccessfully) {
        		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return ['message'=>'Message send to email'];
        	} else {
        		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return ['message'=>'Error send message to email'];
        	}        	
		} else {
			return $this->render('single', [
				'lettersData' => $result->lettersArray[0],
				'featuredIDs' => $result->featuredIDs, 
				'blackListIDs' => $result->blackListIDs,
				'next_prev' => $result->next_prev_letters_ids,
				'letterType' => $lettersType
			]);
		}
		
    }
    
    public function actionSystemSingle($messageID)
    {
		
		$session = Yii::$app->session;
		$ajax = Yii::$app->request->getIsAjax();			
		
		$params = [
	        'messageID' => $messageID,
        ];
		
		$result = $this->getResponseFromAPI($params, "/v1/message/get-messages"); 		
        //var_dump(self::sendCurl($session['user_token'], $params, "/v1/message/get-messages")); die;
		if (!isset($result->messageArray[0]) || empty($result->messageArray[0])) {			
			$session->set('message', 'message empty');
			return $this->redirect('/', 301);
		}
		$lettersType = 'outbox';
		if ($result->messageArray[0]->another_user_avatar != null) {
			$anotherUserAvatar = $this->serverUrl . "/" . $result->messageArray[0]->another_user_avatar;
	    } else {
			$anotherUserAvatar = "/img/no_avatar_normal.jpg";
	    }
		
		$this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$readMessageScript = "window['messageLibrary']['readSystemMessage']($messageID);
								window['messageLibrary']['getLettersAlbum'](); " . self::JS_GET_MESSAGE_COUNT;
		$this->view->registerJs($readMessageScript, yii\web\View::POS_READY);
		
		if ($ajax) {
			$messages = Yii::$app->controller->renderPartial('//message/message-template.php', [
				'lettersData'=>$result->messageArray[0],
				'next_prev'=>$result->next_prev_mess_ids
			]);
        	
        	$from = Yii::$app->params['adminEmail'];
        	if (YII_DEBUG) {
        		$to = 'darkstill@ukr.net';
        	} else {
        		$to = $session['user_data']['email'];
        	}
        	
        	if ($session['user_data']['email'] == "") {
        		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        		return ['message' => 'Please add your email in account cabinet'];
			}
        	
			$emailSendSuccessfully = Yii::$app->mailer->compose()
		        ->setFrom($from)
		        ->setTo($to)
		        ->setSubject('message from '.$_SERVER['SERVER_NAME'])
		        ->setHtmlBody($messages)
		        ->send();
        	if ($emailSendSuccessfully) {
        		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return ['message' => 'Message send to email'];
        	} else {
        		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return ['message' => 'Error send message to email'];
        	}        	
		} else {
			$this->verticalMenuName = "system";
			$this->horizontalMenuName = "message";
			
			return $this->render('system-single', [
				'messageArray' => $result->messageArray[0],
				'anotherUserAvatar' => $anotherUserAvatar,
				'next_prev' => $result->next_prev_mess_ids,
				'letterType' => $letterType
			]);
		}
    }
    
    public function actionInbox()
    {
    	$session = Yii::$app->session;
		$ajax = Yii::$app->request->getIsAjax();

		if (!$ajax) {
			return $this->redirect('/messages');
		}
		
    	if (!$this->checkIfUserLoggedIn()) {            
            $this->redirect('/', 301);
        } 
		
		$offset = 0;
		$page = 1;		
		$searchParams = null;
		
		if (isset($session['letters_search_params'])) {
			unset($session['letters_search_params']);
		};
		
		if ($ajax) {
			$page = Yii::$app->request->post('page');			
			$searchParams = Yii::$app->request->post('searchParams');
			$session->set('letters_search_params', $searchParams);
		}
		
		if (empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfMessagesOnPage;
		$params = [
	        'skinID' => 1,	       
	        'limit' => $this->numberOfMessagesOnPage,
	        'offset' => $offset,
	        'type' => 'inbox',	        
	        'searchParams' => $searchParams,
        ];
		
		$result = json_decode(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); 
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/letter/get-letters")); die;
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
        }
				
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, yii\web\View::POS_READY);
		$this->verticalMenuName = "inbox";
		$this->horizontalMenuName = "message";
		
		if ($ajax) {
			$messages = Yii::$app->controller->renderPartial('//message/inbox-ajax.php', [
				'inboxLettersArray' => $result->lettersArray, 
				'inboxLettersCount' => $result->countLetters, 
				'featuredIDs' => $result->featuredIDs, 
				'blackListIDs' => $result->blackListIDs,
				'page' => $page,
				'limit' => $this->numberOfMessagesOnPage
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $messages;
		}
        
    }
    
    public function actionOutbox()
    {
    	$session = Yii::$app->session;
		$ajax = Yii::$app->request->getIsAjax();

		if (!$ajax) {
			return $this->redirect('/messages');
		}

    	if (!$this->checkIfUserLoggedIn()) {            
            $this->redirect('/', 301);
        } 
		
		$offset = 0;
		$page = 1;		
		$searchParams = null;
		if (isset($session['letters_search_params'])) {
			unset($session['letters_search_params']);
		};
		if ($ajax) {
			$page = Yii::$app->request->post('page');			
			$searchParams = Yii::$app->request->post('searchParams');
			$session->set('letters_search_params', $searchParams);
		}
		
		if (empty($page) or $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfMessagesOnPage;
		$params = [
	        'skinID' => 1,	        
	        'limit' => $this->numberOfMessagesOnPage,
	        'offset' => $offset,
	        'type' => 'outbox',	        
	        'searchParams' => $searchParams,
        ];
		
		$result = json_decode(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters"));
		//var_dump(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); die;
		
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
        }
				
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, yii\web\View::POS_READY);
		$this->verticalMenuName = "outbox";
		$this->horizontalMenuName = "message";
		
		if ($ajax) {
			$messages = Yii::$app->controller->renderPartial('//message/outbox-ajax.php', [
				'outboxLettersArray'  => $result->lettersArray, 
				'outboxLettersCount' => $result->countLetters, 
				'featuredIDs' => $result->featuredIDs, 
				'blackListIDs' => $result->blackListIDs,
				'page' => $page,
				'limit' => $this->numberOfMessagesOnPage
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $messages;
		}
		return $this->render('outbox', [
			'lettersArray' => $result->lettersArray,
			'lettersCount' => $result->countLetters, 
			'featuredIDs' => $result->featuredIDs, 
			'blackListIDs' => $result->blackListIDs,
			'page' => $page,
			'limit' => $this->numberOfMessagesOnPage
		]);
		
    }
    
    public function actionSystem()
    {
    	$session = Yii::$app->session;
		$ajax = Yii::$app->request->getIsAjax();
		
		if (!$ajax) {
			return $this->redirect('/messages');
		}

    	if (!$this->checkIfUserLoggedIn()) {            
            $this->redirect('/', 301);
        } 
		
		$offset = 0;
		$page = 1;
		
		if ($ajax) {
			$page = Yii::$app->request->post('page');
		}
		
		if (empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfMessagesOnPage;
		$params = [
	        'limit' => $this->numberOfMessagesOnPage,
	        'offset' => $offset,
	        'type' => 'system'
        ];
		
		$result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/message/get-messages")); 
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/message/get-messages")); die;	
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
        }
		
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, yii\web\View::POS_READY);		
		$this->verticalMenuName = "system";
		$this->horizontalMenuName = "message";
		
		if ($ajax) {
			$messages = Yii::$app->controller->renderPartial('//message/system-ajax.php', [
				'systemLettersArray' => $result->messageArray, 
				'systemLettersCount' => $result->count,
				'page' => $page
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $messages;
		}
		
		return $this->render('system', [
			'messageArray' => $result->messageArray, 
			'messageCount' => $result->count, 
			'page' => $page,
			'limit' => $this->numberOfMessagesOnPage
		]);
    }
    
    public function actionFeatured()
    {
    	return $this->redirect('/messages');
    	$session = Yii::$app->session;
		$ajax = Yii::$app->request->getIsAjax();		
		
    	if (!$this->checkIfUserLoggedIn()) {            
            $this->redirect('/', 301);
        } 
		
		$offset = 0;
		$page = 1;
		$searchParams = null;
		
		if (isset($session['letters_search_params'])) {
			unset($session['letters_search_params']);
		};
		
		if ($ajax) {
			$page = Yii::$app->request->post('page');
			$searchParams = Yii::$app->request->post('searchParams');
			$session->set('letters_search_params', $searchParams);
		}
		
		if(empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfMessagesOnPage;
		$params = [
	        'skinID' => 1,
	        'limit' => $this->numberOfMessagesOnPage,
	        'offset' => $offset,
	        'type' => 'featured',
	        'searchParams' => $searchParams,
        ];
		
		$result = json_decode(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); 
		//var_dump(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); die;
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
        }
        
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, yii\web\View::POS_READY);
		
		$this->verticalMenuName = "featured";
		$this->horizontalMenuName = "message";

		if ($ajax) {
			$messages = Yii::$app->controller->renderPartial('//message/featured.php', [
				'lettersArray' => $result->lettersArray, 
				'lettersCount' => $result->countLetters, 
				'featuredIDs' => $result->featuredIDs, 
				'blackListIDs' => $result->blackListIDs,
				'page' => $page
			]);
        	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $messages;
		}
		return $this->render('featured', [
			'lettersArray' => $result->lettersArray,
			'lettersCount' => $result->countLetters, 
			'featuredIDs' => $result->featuredIDs, 
			'blackListIDs' => $result->blackListIDs,
			'page' => $page,
			'limit' => $this->numberOfMessagesOnPage
		]);
    }
    
    public function actionDeleted()
    {
    	$session = Yii::$app->session;
		$ajax = Yii::$app->request->getIsAjax();
		
		if (!$ajax) {
			return $this->redirect('/messages');
		}

    	if (!$this->checkIfUserLoggedIn()) {            
            $this->redirect('/', 301);
        } 
		
		$offset = 0;
		$page = 1;
		$searchParams = null;
		
		if (isset($session['letters_search_params'])) {
			unset($session['letters_search_params']);
		};
		
		if ($ajax) {
			$page = Yii::$app->request->post('page');
			$searchParams = Yii::$app->request->post('searchParams');
			$session->set('letters_search_params', $searchParams);
		}
		
		if (empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfMessagesOnPage;
		$params = [
	        'skinID' => 1,	        
	        'limit' => $this->numberOfMessagesOnPage,
	        'offset' => $offset,
	        'type' => 'deleted',
	        'searchParams' => $searchParams,
        ];
		
		$result = json_decode(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); 
		//var_dump(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); die;
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
        }
				
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, yii\web\View::POS_READY);
		$this->verticalMenuName = "deleted";
		$this->horizontalMenuName = "message";
		
		if ($ajax) {
			$messages = Yii::$app->controller->renderPartial('//message/deleted-ajax.php', [
				'deletedLettersArray' => $result->lettersArray, 
				'deletedLettersCount' => $result->countLetters, 
				'featuredIDs' => $result->featuredIDs, 
				'blackListIDs' => $result->blackListIDs,
				'page' => $page
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return $messages;
		}
		return $this->render('deleted', [
			'lettersArray' => $result->lettersArray,
			'lettersCount' => $result->countLetters, 
			'featuredIDs' => $result->featuredIDs, 
			'blackListIDs' => $result->blackListIDs,
			'page' => $page,
			'limit' => $this->numberOfMessagesOnPage
		]);
    }
    
    public function actionPrintVersion($lettersID)
    {
		$session = Yii::$app->session;		
		
		$params = [
	        'type' => 'single',
	        'letterID' => $lettersID,
        ];
		
		$result = $this->getResponseFromAPI($params, "/v1/letter/get-letters"); 
		//var_dump(self::sendCurl($session['user_token'], $params,"/v1/letter/get-letters")); die;	
		if (!isset($result->lettersArray[0]) || empty($result->lettersArray[0])) {			
			$session->set('message', 'letters error');
			return $this->redirect('/', 301);
		}
		
		if ($result->lettersArray[0]->another_user_avatar != null) {
			$result->lettersArray[0]->another_user_avatar = $this->serverUrl . "/" . $result->lettersArray[0]->another_user_avatar;
	    } else {
			$result->lettersArray[0]->another_user_avatar = "/img/no_avatar_normal.jpg";
	    }
		
		$messageScript = "<script>window.print();</script>";
		$messageHtml = Yii::$app->controller->renderPartial('message-template', ['lettersData' => $result->lettersArray[0]]);
		
		return $messageHtml . $messageScript;

    }
    
    public function actionSystemPrintVesion($messageID) 
    {
		$session = Yii::$app->session;
		
		$params = [
	        'messageID' => $messageID,
        ];
		
		$result = $this->getResponseFromAPI($params, "/v1/message/get-messages");
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/message/get-messages")); die;
		if (!isset($result->messageArray[0]) || empty($result->messageArray[0])) {			
			$session->set('message', 'letters error');
			return $this->redirect('/', 301);
		}
		
		if ($result->messageArray[0]->another_user_avatar != null) {
			$result->messageArray[0]->another_user_avatar = $this->serverUrl . "/" . $result->messageArray[0]->another_user_avatar;
	    } else {
			$result->messageArray[0]->another_user_avatar = "/img/no_avatar_normal.jpg";
	    }
		
		$messageScript = "<script>window.print();</script>";
		$messageHtml = Yii::$app->controller->renderPartial('message-template', [
			'lettersData' => $result->messageArray[0]
		]);
		
		return $messageHtml . $messageScript;
    }
    
    public function actionSendToEmail()
    {	
    	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return ['success' => true];
    }
    
    public function actionGetLettersAlbum()
    {
		$session = Yii::$app->session;		
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$params = [
	        'albumID' => 'letters',
	        //'albums' => 'letters'
        ];
		
		$result = $this->getResponseFromAPI($params, "/v1/photo/get-photos");

		if (!empty($result->photos)) {
			$photos = Yii::$app->controller->renderPartial('//layouts/parts/message-attach-photos.php', ['photos' => $result->photos]);			
			return [
				'success' => true,
				'photos' => $photos
			];
		}
		
		return $result;
    }
    
    // new message
    public function actionNewmessage($otherUserID)
    {
		$session = Yii::$app->session;

		if ($session['user_id'] == $otherUserID) {
			$session->set('message', 'You can`t write message yourself');
			return $this->redirect('/', 301);
		}

		$params = [	        
	        'otherUserID' => $otherUserID,
        ];
		
		$result = json_decode(self::sendCurl($session['user_token'], $params,"/user/get-other-user-info"));
		
		if (isset($result->success) && $result->success == false) {
			$session->set('message', $result->message);
			return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
		}

		if(!isset($result->userData) || empty($result->userData)){
			return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
        }
        
		$this->view->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		
		$readMessageScript = "window['messageLibrary']['getLettersAlbum']();" . self::JS_GET_MESSAGE_COUNT;
		$this->view->registerJs($readMessageScript, yii\web\View::POS_READY);
		
		$this->verticalMenuName = "";
		$this->horizontalMenuName = "message";
			
		return $this->render('newmessage', [
			'otherUserInfo' => $result->userData
		]);
		
	}       

	public function actionHistory($letterID)
	{
		$session = Yii::$app->session;		
		
		$params = [
			'letterID' => $letterID,
		];		

		$result = json_decode($this->getCurl($session['user_token'], "/v1/letter/get-history/" . $letterID));
		//var_dump($this->getCurl($session['user_token'], "/v1/letter/get-history/" . $letterID)); die;
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
        }
        $this->verticalMenuName = "history";

		return $this->render('history', [
			'lettersArray' => $result->lettersArray,
			'lettersCount' => $result->lettersCount
		]);
	}   
}