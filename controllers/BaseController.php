<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Cookie;

class BaseController extends Controller
{
	public $googleHref;
    public $serverUrl;
    public $horizontalMenuName;
    public $verticalMenuName;
    const USER_MALE = 1;
    const USER_FEMALE = 2;
    const USER_AGENCY = 3;
	const USER_INTERPRETER = 4;
	const USER_AFFILIATE = 5;
	const USER_SUPERADMIN = 6;
	const USER_ADMIN = 7;
    const STATUS_NOT_APPROVED = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_DECLINED = 3;
    const STATUS_APPROVED = 4;

    const JS_GET_MESSAGE_COUNT = "setInterval(function(){window['accountLibrary']['getNewMessageCount']();},3000);";

	public function init()
    {
        $session = Yii::$app->session;
		$googleConfig = Yii::$app->params['google_auth'];
        $params      = array(
            'redirect_uri'  => $googleConfig['redirect_uri'],
            'response_type' => 'code',
            'client_id'     => $googleConfig['client_id'],
            'scope'         => $googleConfig['scope'],
        );
        $this->googleHref = $googleConfig['url'] . '?' . urldecode(http_build_query($params));
        $this->serverUrl = Yii::$app->params['serverUrl'];

        if (isset(Yii::$app->request->cookies['user_token'])) {
            $session->set('user_token', Yii::$app->request->cookies['user_token']->value);
            $params = [];            
            $result = $this->getResponseFromAPI($params, "/user/get-user-info"); 
            //var_dump(self::sendCurl($session['user_token'], ['token' => $session['user_token']], "/user/get-user-info")); die;
            if (!$result) {
                unset($session['user_id']);
                unset($session['user_type']);
                unset($session['user_token']);
                unset($session['user_data']);
                $cookies = Yii::$app->response->cookies;
                $cookies->remove('user_token');
                return $this->redirect('/', 301);
            }
            if (isset($result->userData->token) && ($result->userData->token != $session['user_token'])) {
                $cookie = new Cookie([
                    'name' => 'user_token',
                    'value' => $result->userData->token,
                    'expire' => time()+7*24*60*60,
                ]);
                \Yii::$app->getResponse()->getCookies()->add($cookie);
            }        
            $userBalance = $result->userData->balance;
            if (empty($userBalance)) {
                $userBalance = "0.00";
            } else {            
                $userBalance = (float)$result->userData->balance;
            }
            
            if ($result->userData->avatar->small != null) {
                $userAvatar = $this->serverUrl . "/" . $result->userData->avatar->small;
            } else {
                $userAvatar = "/img/no_avatar_small.jpg";
            }
            
            if ($result->userData->avatar->normal != null) {
                $result->userData->avatar->normal = $this->serverUrl . "/" . $result->userData->avatar->normal;
            } else {
                $result->userData->avatar->normal = "/img/no_avatar_normal.jpg";
            }
            $userData = [
                'userID' => $result->userData->personalInfo->id,
                'userType' => $result->userData->userType,
                'name' => $result->userData->personalInfo->first_name,
                'avatar' => $userAvatar,
                'avatarPhotoID' => $result->userData->avatarPhotoID,
                'email'=> $result->userData->email,
                'balance' => (float)$userBalance,
                'camOnline' => $result->userData->cam_online,
            ];
            $session->set('user_data', $userData);

            $session->set('user_type', $result->userData->userType);
            if (!empty($result->userData->token)) {
                self::UserAuthorization($result->userData);
            }
            $this->view->registerJsFile('/js/account.js', ['depends' => [\app\assets\CommonAsset::className()]]);
            $js_script = /*"accountLibrary['getUsersOnAccountPage']();" .*/ self::JS_GET_MESSAGE_COUNT;        
            $this->view->registerJs($js_script, Yii\web\View::POS_READY);

        } else {
            
        }
                       
        if (!empty($session['user_type']) && in_array($session['user_type'], [self::USER_AGENCY, self::USER_INTERPRETER,self::USER_AFFILIATE,self::USER_SUPERADMIN,self::USER_ADMIN])) {
            //$this->redirect('/manager', 301);
        }elseif(!empty($session['user_type']) && !in_array($session['user_type'], [self::USER_MALE, self::USER_FEMALE])) {  
        	unset($session['user_id']);
            unset($session['user_type']);
            unset($session['user_token']);
            unset($session['user_data']);
            $cookies = Yii::$app->response->cookies;
            $cookies->remove('user_token');
            setcookie('user_token', "", time()-3600, "/");
            $this->redirect('/', 301);
        }
    }

    protected static function sendCurl($token, $post, $path)
    {
        $authorization = "Authorization: Bearer " . $token;        
		$serverUrl = Yii::$app->params['serverUrl'];
		if( $curl = curl_init() ) {
		    curl_setopt($curl, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded; charset=utf-8', $authorization));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_CRLF, true);
			curl_setopt($curl, CURLOPT_POST, true);
			//убрать когда бует подписаный сертификат
			curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0); 
			//
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));
			curl_setopt($curl, CURLOPT_URL, $serverUrl . $path);
		    $response = curl_exec($curl);
            
		    if (empty($response)) {                
				return false; //'Curl Error: ' . curl_error($curl);	
		    }
		    curl_close($curl);
		    return $response;
		}
		return false;
    }

    protected static function getCurl($token, $path)
    {
        $authorization = "Authorization: Bearer " . $token;
        $serverUrl = Yii::$app->params['serverUrl'];
        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('application/json; charset=utf-8', $authorization));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_CRLF, true);            
            //убрать когда бует подписаный сертификат
            curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0); 
            //            
            curl_setopt($curl, CURLOPT_URL, $serverUrl . $path);
            $response = curl_exec($curl);
            
            if (empty($response)) {                
                return false; //'Curl Error: ' . curl_error($curl); 
            }
            curl_close($curl);
            return $response;
        }
        return false;
    }

    protected static function checkCurlResponse($response) 
    {		
        
		if (isset($response->success) && $response->success == true) {
			return true;
		} else {
			$session = Yii::$app->session;                    
			$errorMessage = isset($response->message) ? $response->message : "Error send request";  
			$session->set('message', $errorMessage);
			return false;
		}
    }

    protected function TranslateText($text,$lang = "en-ru"){
        $key = Yii::$app->params['yandex_api_key'];
        //$url = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=$key&text=$text&lang=$lang";
        $url = "https://translate.yandex.net/api/v1.5/tr.json/translate?".http_build_query ([
            "key"=>$key,
            "text"=>$text,
            "lang"=>$lang,
        ]);
        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded; charset=utf-8'));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_CRLF, true);
            //убрать когда бует подписаный сертификат
            curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_URL, $url);
            $response = curl_exec($curl);

            if (empty($response)) {
                return ['success'=>false,'message'=>curl_error($curl)];
            }
            curl_close($curl);
            return $response;
        }
        return ['success'=>false,'message'=>'Curl not init'];
    }

    protected function loadJsFiles() 
    {
    	$this->view->registerJsFile('/js/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, yii\web\View::POS_READY);
    }

    protected function checkIfUserLoggedIn()
    {    	        
		if (!isset(Yii::$app->session['user_token'])) {
			Yii::$app->session->set('message', 'Error token. Please login');
			return false;
		}
        return true;
    }

    protected function getResponseFromAPI($params, $url="")
    {
    	if (!$this->checkIfUserLoggedIn()) {            
            return false;
        }      

    	$response = json_decode(self::sendCurl(Yii::$app->session['user_token'], $params, $url));
        
    	if (!self::checkCurlResponse($response)) {                   
			return false;
        } else {
        	return $response;
        }
    }

    protected static function UserAuthorization($userInfo)
    {
        $session = Yii::$app->session;
        $session->set('user_id', $userInfo->userID);
        if (!empty($userInfo->userType)) {
            $session->set('user_type', $userInfo->userType);
        }       
        $session->set('user_token', $userInfo->token);      
        unset($session['chat_session']);
    }

}