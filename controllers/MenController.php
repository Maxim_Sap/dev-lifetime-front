<?php

namespace app\controllers;

use Yii;
use app\controllers\BaseController;

class MenController extends BaseController
{

	public $numberOfItemsOnFrontPage;
	public $numberOfItemsOnSearchPage;
	
    public function init()
    {
    	parent::init();
		$this->numberOfItemsOnFrontPage = Yii::$app->params['numberOfItemsOnFrontPage'];
		$this->numberOfItemsOnSearchPage = Yii::$app->params['numberOfItemsOnSearchPage'];
		$footer_data = json_decode(self::sendCurl('', [], "/site/get-footer-data"));
		$this->view->params['footer_post'] = $footer_data->posts;
		$this->view->params['footer_history'] = $footer_data->history_page;
		$this->view->registerJsFile('/js/config.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		if (isset($session['user_token'])) {
			$this->view->registerJsFile('/js/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		}

		if (!isset(Yii::$app->session['user_token']) || Yii::$app->session['user_data']['userType'] == self::USER_MALE) {
			return $this->redirect('/', 301);
		}
    }
    
    public function beforeAction($action)
	{            
	    // disable CRSF validation for specific actions
		$arrayOfActionsWithDisabledCSRFValidation = ['generate-girls-list', 'generate-chat-girls-list', 'index', 'new', 'online'];
		if (in_array($action->id, $arrayOfActionsWithDisabledCSRFValidation)) {
			$this->enableCsrfValidation = false;
		}

		return parent::beforeAction($action);
	}
    
    public function actionIndex()
    {
		$ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		if ($ajax) {
			$page = Yii::$app->request->post('page');
		}
		if (empty($page) || $page < 0) {
			$page = 1;
		}

		$offset = ($page - 1) * $this->numberOfItemsOnFrontPage;

		$params = [
			'skinID' => 1,
			'limit' => $this->numberOfItemsOnFrontPage,
			'offset' => $offset,
			'userType' => 1,
			'order' => 'user.last_activity DESC'
		];

		$session = Yii::$app->session;
		if (isset($session['user_token']) && isset($session['user_id'])) {
			$params['userID'] = $session['user_id'];
			$this->loadJsFiles();
		}
		
		$result = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-users"));
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-users")); die;

		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
		}

		if ($ajax) {
			$items = Yii::$app->controller->renderPartial('index.php', [
				'girls' => $result->girls,
				'count' => $result->count,
				'page' => $page,
				'pageName' => 'index'
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return $items;
		}

		return $this->render('index', [
			'girls' => $result->girls,
			'count' => $result->count,
			'page' => $page,
			'pageName' => 'index'
		]);
    }

	public function actionSingle($otherUserID)
	{
		$session = Yii::$app->session;

		$params = [
			'otherUserID'=> $otherUserID,
		];

		$result = $this->getResponseFromAPI($params, "/user/get-other-user-info");
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-other-user-info")); die;
		if (!$result) {
			return $this->redirect('/', 301);
		}

		if ($result->userData->avatar->small != null) {
			$result->userData->avatar->small = $this->serverUrl . "/" . $result->userData->avatar->small;
		} else {
			$result->userData->avatar->small = "/img/no_avatar_small.jpg";
		}

		if ($result->userData->avatar->normal != null) {
			$result->userData->avatar->normal = $this->serverUrl . "/" . $result->userData->avatar->normal;
		} else {
			$result->userData->avatar->normal = "/img/no_avatar_normal.jpg";
		}

		$params['albumID'] = 'Public Album';
		$result2 = $this->getResponseFromAPI($params, "/v1/photo/get-photos");
		$photos = (!empty($result2->photos)) ? $result2->photos : null;

		$result2 = $this->getResponseFromAPI($params, "/v1/video/get-videos");
		$videos = (!empty($result2->videos)) ? $result2->videos : null;
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/photo/get-photos")); die;


		$this->horizontalMenuName = "index";

		if (isset($session['user_token']) && isset($session['user_id'])) {
			$this->loadJsFiles();
		}

		return $this->render('single', [
			'userData' => $result->userData,
			'photos' => $photos,
			'videos' => $videos
		]);
	}
        	  
    public function actionOnline()
    {
		$ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		if ($ajax) {
			$page = Yii::$app->request->post('page');
		}
		if (empty($page) || $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfItemsOnFrontPage;

		$params = [
			'skinID' => 1,
			'limit' => $this->numberOfItemsOnFrontPage,
			'offset' => $offset,
			'userType' => 1,
			'type' => 'last_activity',
		];

		$session = Yii::$app->session;
		if(isset($session['user_token']) && isset($session['user_id'])) {
			$params['userID'] = $session['user_id'];
			$this->loadJsFiles();
		}

		$result = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-users"));
		//var_dump(json_decode(self::sendCurl($session['user_token'], $params, "/user/get-users"))); die;
		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
		}

		if ($ajax) {
			$items = Yii::$app->controller->renderPartial('online.php', [
				'girls' => $result->girls,
				'count' => $result->count,
				'page' => $page,
				'pageName'=>'online'
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return $items;
		}

		return $this->render('online', [
			'girls' => $result->girls,
			'count' => $result->count,
			'page' => $page,
			'pageName' => 'online'
		]);
    }
    
    public function actionNew()
    {
		$ajax = Yii::$app->request->getIsAjax();
		$offset = 0;
		$page = 1;
		if ($ajax) {
			$page = Yii::$app->request->post('page');
		}
		if (empty($page) or $page < 0) {
			$page = 1;
		}
		$offset = ($page - 1) * $this->numberOfItemsOnFrontPage;

		$params = [
			'skinID' => 1,
			'limit' => $this->numberOfItemsOnFrontPage,
			'offset' => $offset,
			'userType' => 1,
			'type' => 'new',
			'order' => 'user.created_at DESC',
		];

		$session = Yii::$app->session;
		if(isset($session['user_token']) && isset($session['user_id'])) {
			$params['userID'] = $session['user_id'];
			$this->loadJsFiles();
		}

		$result = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-users"));
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-users")); die;

		if (!self::checkCurlResponse($result)) {
			return $this->redirect('/', 301);
		}

		if ($ajax) {
			$items = Yii::$app->controller->renderPartial('new.php', [
				'girls' => $result->girls,
				'count' => $result->count,
				'page' => $page,
				'pageName' => 'new'
			]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return $items;
		}

		return $this->render('new',[
			'girls' => $result->girls,
			'count' => $result->count,
			'page' => $page,
			'pageName' => 'new'
		]);
    }

	public function actionGenerateGirlsList()
	{
		$girlsArray = Yii::$app->request->post('girls_mass');
		$count = Yii::$app->request->post('count');
		$page = Yii::$app->request->post('page');
		$girlList = "";
		$session = Yii::$app->session;
		$userType = isset($session['user_data']['userType']) && $session['user_data']['userType'] == self::USER_FEMALE ? self::USER_FEMALE : self::USER_MALE;
		$pagination = "";
		if(!empty($girlsArray)) {
			foreach($girlsArray as $girl) {
				$girlList .= Yii::$app->controller->renderPartial('//layouts/parts/girl-on-account-page.php', [
					'girl' => $girl,
					'userType' => $userType
				]);
			}
			if ($count > $this->numberOfItemsOnSearchPage) {
				$pagination = Yii::$app->controller->renderPartial('/layouts/parts/user_pagination.php', [
					'count' => $count,
					'pageName' => 'generate-girls-list',
					'pageController' => 'search',
					'page' => $page
				]);
			}
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return [
			'girl_list' => $girlList,
			'pagination' => $pagination
		];
	}

	public function actionGenerateChatGirlsList()
	{
		$girlsArray = Yii::$app->request->post('girls_mass');
		$label = Yii::$app->request->post('label');
		$girlList = "";
		$session = Yii::$app->session;
		$userType = isset($session['user_data']['user_types']) && $session['user_data']['user_types'] == self::USER_FEMALE ? self::USER_FEMALE : self::USER_MALE;
		$param = (isset($label) && !empty($label) && $label == 'active_chat_mess') ? 'chat_active' : '';
		if(!empty($girlsArray)) {
			foreach($girlsArray as $girl) {
				$girlList .= Yii::$app->controller->renderPartial('//layouts/parts/chat-item-user-inlist2.php',[
					'girl' => $girl,
					'user_type' => $userType,
					'param' => $param
				]);
			}
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $girlList;
	}

    public function actionGallery($otherUserID)
    {
		$session = Yii::$app->session;        
		
        $params = [
	        'otherUserID' => $otherUserID,
        ];
        
		$params['albumID'] = 'Public Album';
	    $result2 = $this->getResponseFromAPI($params, "/v1/photo/get-photos");
        $photos = (!empty($result2->photos)) ? $result2->photos : null;

        $photoPrice = json_decode($this->getCurl($session['user_token'], "/v1/pricelist/1/1/watch-photo"));
	    //var_dump($this->getCurl($session['user_token'], "/v1/pricelist/1/1/watch-photo")); die;	    
	    if ((isset($photoPrice->status) && $photoPrice->status == 401) || $photoPrice->success == false) {
			$session->set('message', $photoPrice->message); 
			return $this->redirect('/', 301);
		}

        $this->verticalMenuName = "gallery";
		$this->horizontalMenuName = "index";

        if (isset($session['user_token']) && isset($session['user_id'])) {
			$this->loadJsFiles();
		}
		
		return $this->render('gallery', [
			'otherUserID' => $otherUserID,
			'photos' => $photos,
			'photoPrice' => $photoPrice->price
		]);
    }

    public function actionVideoGallery($otherUserID)
	{
		$session = Yii::$app->session;	

		$params = [
			'otherUserID' => $otherUserID,
		];

		$result = $this->getResponseFromAPI($params, "/v1/video/get-videos");
		//var_dump($this->sendCurl($session['user_token'],$params, "/v1/video/get-videos")); die;
		//var_dump($result);die;
	    $videos = (!empty($result->videos)) ? $result->videos : null;

	    $videoPrice = json_decode($this->getCurl($session['user_token'], "/v1/pricelist/1/1/watch-video"));
	    if ((isset($videoPrice->status) && $videoPrice->status == 401) || $videoPrice->success == false) {
			$session->set('message', $videoPrice->message); 
			return $this->redirect('/', 301);
		}

        $this->verticalMenuName = "gallery";
		$this->horizontalMenuName = "index";

		if(isset($session['user_token']) && isset($session['user_id'])) {
			$this->loadJsFiles();
		}

		return $this->render('video-gallery', [
			'videos' => $videos,
			'videoPrice' => $videoPrice->price
		]);
	}


}   