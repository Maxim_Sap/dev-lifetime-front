<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\controllers\BaseController;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;

class PaymentsController extends BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only'  => ['logout'],
				'rules' => [
					[
						'actions' => ['logout'],
						'allow'   => true,
						'roles'   => ['@'],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public $numberOfMessagesOnPage;
		
    public function init()
    {
        parent::init();
        $this->numberOfMessagesOnPage = Yii::$app->params['numberOfMessagesOnPage'];
		$footer_data = json_decode(self::sendCurl('', [], "/site/get-footer-data"));
		$this->view->params['footer_post'] = $footer_data->posts;
		$this->view->params['footer_history'] = $footer_data->history_page;
		$this->view->registerJsFile('/js/config.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        //$this->view->registerJsFile('/js/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    }
    
    public function beforeAction($action)
	{            
		$this->enableCsrfValidation = false;
	    return parent::beforeAction($action);
	}

	public function actionIndex()
	{

		$session = Yii::$app->session;

		$post = Yii::$app->request->post();

		$payment_key = '';
		if($post){
			//for Mastercard service only
			//$f = fopen("payment_log.txt", "w");
			if(is_array($post)){
				$customer_name = $post['cust_name'];
				$user_token = $post['user_token'];
				$random_key = $post['random_key'];
				$user_id = $post['user_id'];
				$total_amount = $post['total_amount'];
				$order_num = $post['order_num'];
				$ServicePassword = $post['SilentPostPassword'];
				if($ServicePassword == Yii::$app->params['merchant_post_pass']){
					//добавляем платеж и пополняем пользователю счет
					$params = [
						'FromUserId' => $user_id,
						'totalAmount' => $total_amount,
						'orderNum' => $order_num,
						'random_key' => $random_key,
						'customer_name' => $customer_name,
					];

					$result = json_decode(self::sendCurl($user_token, $params, "/user/add-user-funds"));
					//$result = $this->getResponseFromAPI($params,"/user/add-user-funds");
					//$text = "cust_name: $customer_name\norder_num: $order_num\nuser_id: $user_id\ntotal_amount: $total_amount\nServicePassword: $ServicePassword\nRandom_key: $random_key\nToken: $user_token";
					//fwrite($f, $text);
					//var_dump(self::sendCurl($session['user_token'], $params, "/user/add-user-funds")); die;
					if (!$result->success) {
						return false;
					}

					$html_response_message = "<!--success--><a href='https://lifetime.dating'>lifetime.dating</a><h2>Congratilation your account was found on $total_amount USD</h2>";
					return $html_response_message;
				}

			}else{
				//fwrite($f, "post empty\n");
			}
			//fclose($f);

		}else{
			$result = $this->getResponseFromAPI(['token' => $session['user_token']],"/user/get-payment-key");
			if (!$result) {
				$session->set('message', 'Error Payment Key');
			}else{
				//$session->set('message', 'Payment Key: '.$result->key);
				$payment_key = $result->key;
			}
			//$this->auth_key = Yii::$app->security->generateRandomString(40);

		}

		/*if (!$this->checkIfUserLoggedIn()) {
			$session = Yii::$app->session;
			$session->set('message', 'Error token. Please login');
			$this->redirect('/', 301);
		}*/

		return $this->render('index', [
			'merchant_id'=>Yii::$app->params['merchant_id'],
			'merchant_url_idx'=>Yii::$app->params['merchant_url_idx'],
			'payment_key'=>$payment_key,
		]);
	}
    

}