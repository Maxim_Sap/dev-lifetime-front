<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Json;

class SearchController extends BaseController
{
	public $googleHref;
	public $serverUrl;
	public $numberOfItemsOnFrontPage;
	const JS_GET_MESSAGE_COUNT = "setInterval(function(){window['accountLibrary']['getNewMessageCount']();},3000);";
	
    public function init()
    {
		$googleConfig = Yii::$app->params['google_auth'];
        $params      = array(
            'redirect_uri'  => $googleConfig['redirect_uri'],
            'response_type' => 'code',
            'client_id'     => $googleConfig['client_id'],
            'scope'         => $googleConfig['scope'],
        );

		$footer_data = json_decode(self::sendCurl('', [], "/site/get-footer-data"));
		$this->view->params['footer_post'] = $footer_data->posts;
		$this->view->params['footer_history'] = $footer_data->history_page;
        $this->googleHref = $googleConfig['url'] . '?' . urldecode(http_build_query($params));
        $this->serverUrl = Yii::$app->params['serverUrl'];
        $this->numberOfItemsOnFrontPage = 9;
    }
    
    public function actionIndex()
    {
		$this->view->registerJsFile('/js/config.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		
		$session = Yii::$app->session;
		if (isset($session['user_token']) && isset($session['user_id'])) {			
			$this->view->registerJsFile('/js/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
			$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, yii\web\View::POS_READY);	
		}

		$hairColors = JSON::decode($this->getCurl($session['user_token'], '/v1/hair-colors'));
		
		if (isset($hairColors['status']) && $hairColors['status'] == 401) {
			$session->set('message', $hairColors['message']); 
			return $this->redirect('/', 301);
		}

		$eyesColors = JSON::decode($this->getCurl($session['user_token'], '/v1/eyes-colors'));
		if (isset($eyesColors['status']) && $eyesColors['status'] == 401) {
			$session->set('message', $eyesColors['message']); 
			return $this->redirect('/', 301);
		}

		$physiques = JSON::decode($this->getCurl($session['user_token'], '/v1/physiques'));
		//var_dump($this->getCurl($session['user_token'], '/v1/physiques')); die;
		if (isset($physiques['status']) && $physiques['status'] == 401) {
			$session->set('message', $physiques['message']); 
			return $this->redirect('/', 301);
		}
		
		return $this->render('index', [
			'limit' => $this->numberOfItemsOnFrontPage, 
			'page' => 1,
        	'hairColors' => $hairColors,
        	'eyesColors' => $eyesColors,
        	'physiques' => $physiques
		]);
    }
    
}   