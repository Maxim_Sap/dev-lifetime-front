<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\controllers\BaseController;
use yii\filters\VerbFilter;
use yii\helpers\HtmlPurifier;
use app\models\ContactForm;


class SiteController extends BaseController
{   
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
	{            
	    // disable CRSF validation for specific actions
	    $arrayOfActionsWithDisabledCSRFValidation = ['feedback','subscribe'];
	    if (in_array($action->id, $arrayOfActionsWithDisabledCSRFValidation)) {
	        $this->enableCsrfValidation = false;
	    }

	    return parent::beforeAction($action);
	}
	
    public function init()
    {
		parent::init();
        $this->view->registerJsFile('/js/common.js', ['depends' => [\app\assets\AppAsset::className()]]);

		$footer_data = json_decode(self::sendCurl('', [], "/site/get-footer-data"));
		//var_dump($footer_data);die;
		$this->view->params['footer_post'] = $footer_data->posts;
		$this->view->params['footer_history'] = $footer_data->history_page;
    }

    public function actions()
    {
		return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $session = Yii::$app->session;
		if (isset($session['user_type']) && $session['user_type'] == self::USER_FEMALE) {
			$userType = self::USER_FEMALE;
			$findUserType = self::USER_MALE;
		} elseif (isset($session['user_type']) && $session['user_type'] == self::USER_MALE) {
			$userType = self::USER_MALE;
			$findUserType = self::USER_FEMALE;
		} elseif (isset($session['user_type'])) {
			unset($session['user_id']);
			unset($session['user_type']);
			unset($session['user_token']);
			setcookie('user_token', "", time()-3600, "/");
			return $this->redirect('/', 301);
		} else {
			$findUserType = self::USER_FEMALE;
		}

		$userID = (isset($session['user_id']) && is_numeric($session['user_id'])) ? (int)$session['user_id'] : null;

		$params = [
			'skinID' => 1,
			'userType' => $findUserType,
			'userID' => $userID,
			'limit' => 12,
		];			

        $result = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-girls-on-main-page"));        
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-girls-on-main-page")); die;
		if (!self::checkCurlResponse($result)) {
			$result = (object) array('girls' => null);
        }

		if (isset($session['user_token']) && isset($session['user_id'])) {
			$this->loadJsFiles();
		}

		$referral_link = Yii::$app->request->get('referral_link');
		if($referral_link){
			$session['referral_link'] = $referral_link;
			$referr_script = "commonLibrary['addReferralVisitor']('".$referral_link."','".$_SERVER['REMOTE_ADDR']."')";
			$this->view->registerJs($referr_script, Yii\web\View::POS_READY);
		}
		
		$blog_data = json_decode(self::sendCurl($session['user_token'], $params, "/site/get-post-and-about-on-main-page"));

		//var_dump($blog_data);die;

        return $this->render('index', [
        	'girls' => $result->girls,
        	'posts' => $blog_data->posts,
        	'about_page' => $blog_data->about_page,
        ]);
    }

    public function actionContact()
    {
        $this->verticalMenuName = "contact";
        $post = Yii::$app->request->post();
        $session = Yii::$app->session;
        if (isset($session['user_token']) && isset($session['user_id'])) {			
			$this->loadJsFiles();	
		}
		
        $context = [
    		'name' => (isset($post['name']) && trim($post['name']) != "") ? $post['name'] : '',
    		'email' => (isset($post['email']) && trim($post['email']) != "") ? $post['email'] : '',
    		'options' => (isset($post['options']) && is_numeric($post['options']) && $post['options'] > 0 && $post['options'] < 6) ? $post['options'] : '',
    		'message' => (isset($post['message']) && trim($post['message']) != "") ? $post['message'] : ''
    	];

        return $this->render('contact', $context);
    }

    public function actionAbout()
    {
    	$session = Yii::$app->session;
		$params = [
			'skinID' => 1,
			'pageName' => 'about'
		];
		$result = json_decode(self::sendCurl($session['user_token'], $params, "/site/get-info-pages"));
		//var_dump(self::sendCurl($session['user_token'], $params, "/site/get-info-pages")); die;

		if (!self::checkCurlResponse($result)) {
			$session->set('message', $result->message);
			return $this->redirect('/',301);
        }

        $this->verticalMenuName = "about";
        
        if (isset($session['user_token']) && isset($session['user_id'])) {			
			$this->loadJsFiles();	
		}

        return $this->render('about', [
        	'pageContent' => $result->pageContent
    	]);
    }

	public function actionSiteRules()
	{
		$session = Yii::$app->session;
		$params = [
			'skinID'=> 1,
			'pageName'=>'users-rules'
		];
		$result = json_decode(self::sendCurl($session['user_token'], $params, "/site/get-info-pages"));

		if (!self::checkCurlResponse($result)) {
			$session->set('message', $result->message);
			return $this->redirect('/',301);
		}

		$this->verticalMenuName = "users-rules";
		
		if (isset($session['user_token']) && isset($session['user_id'])) {
			$this->loadJsFiles();
		}

		return $this->render('site-rules', [
			'pageContent' => $result->pageContent
		]);
	}

	public function actionLicenseAgreement()
	{
		$session = Yii::$app->session;
		$params = [
			'skinID'=> 1,
			'pageName'=>'license-agreement'
		];
		$result = json_decode(self::sendCurl($session['user_token'], $params, "/site/get-info-pages"));

		if(!self::checkCurlResponse($result)) {
			$session->set('message', $result->message);
			return $this->redirect('/',301);
		}

		$this->verticalMenuName = "users-rules";
		
		if(isset($session['user_token']) && isset($session['user_id'])) {
			$this->loadJsFiles();
		}

		return $this->render('license-agreement', [
			'pageContent' => $result->pageContent
		]);
	}

	public function actionPrivacyPolicy()
	{
		$session = Yii::$app->session;
		$params = [
			'skinID'=> 1,
			'pageName'=>'privacy-policy'
		];
		$result = json_decode(self::sendCurl($session['user_token'], $params, "/site/get-info-pages"));

		if(!self::checkCurlResponse($result)) {
			$session->set('message', $result->message);
			return $this->redirect('/',301);
		}

		$this->verticalMenuName = "users-rules";

		if(isset($session['user_token']) && isset($session['user_id'])) {
			$this->loadJsFiles();
		}

		return $this->render('privacy-policy', [
			'pageContent' => $result->pageContent
		]);
	}

    public function actionFeedback()
    {        
    	$session = Yii::$app->session;
        $post = Yii::$app->request->post();
        $message = "";

        $context = [
    		'name' => (isset($post['name']) && trim($post['name']) != "") ? $post['name'] : '',
    		'email' => (isset($post['email']) && trim($post['email']) != "") ? $post['email'] : '',
    		'options' => (isset($post['options']) && is_numeric($post['options']) && $post['options'] > 0 && $post['options'] < 6) ? $post['options'] : '',
    		'message' => (isset($post['message']) && trim($post['message']) != "") ? $post['message'] : ''
    	];

        if (isset($post['name']) && trim($post['name']) != "") {
			$message .= 'From: ' . $post['name'];			
        } else {
        	$session->set('message', 'Please enter first name');
        	return $this->render('contact', $context);
        }
        if (isset($post['email']) && trim($post['email']) != "") {
			$message .= "\r\n<br/>E-mail: " . $post['email'];
        } else {
        	$session->set('message', 'Please enter your e-mail');
        	return $this->render('contact', $context);
        }
        if (isset($post['options']) && trim($post['options']) != "" && is_numeric($post['options']) && $post['options'] > 0 && $post['options'] < 6) {
        	$myOptions = [
        		1 => 'Browser can\'t connect to chat server',
        		2 => 'Problems with chat/videochat',
        		3 => 'Can\'t login to site',
        		4 => 'Bug report',
        		5 => 'Other problem'
        	];
			$message .= "\r\n<br/>Options: " . $myOptions[(int)$post['options']];
        } else {
        	$session->set('message', 'Please choose option');
        	return $this->render('contact', $context);
        }
        if (isset($post['message']) && trim($post['message']) != "") {
			$message .= "\r\n<br/>Message: " . $post['message'];
        } else {
        	$session->set('message', 'Please enter a message');
        	return $this->render('contact', $context);
        }
        
        $message = HtmlPurifier::process($message);
        $fromUserID = isset($post['fromUserID']) ? (int)$post['fromUserID'] : null;
        // write to database
       
        $params = [
	        'fromUserID' => $fromUserID,
	        'desc' => $message,
	        'caption' => 'Message from contact as page',
        ];
        $result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/message/send-message-to-admin")); 

        if (!$result) {
        	$session->set('message', 'Error during message send');
        	return $this->redirect('/contact');
        }

        $from = Yii::$app->params['adminEmail'];
        if (YII_DEBUG) {
        	$to = 'darkstill@ukr.net';
        } else {
        	$to = 'mmxam@bk.ru';
        }
        
        $emailSendSuccessfully = Yii::$app->mailer->compose()
            ->setTo($to)
            ->setFrom($from)
            ->setSubject('Contact Us')
            ->setHtmlBody($message)
            ->send();
        
        
        if ($emailSendSuccessfully) {
			$session->set('message', 'Message to support send');
        } else {
			$session->set('message', 'Error send message');
        }
		
        return $this->redirect('/contact');
    }

    public function actionContactUs()
    {
    	$session = Yii::$app->session;
    	$model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $message = "";
            $message .= 'From: ' . $model->name;
            $message .= "\r\n<br/>E-mail: " . $model->email;
            $myOptions = [
        		1 => 'Browser can\'t connect to chat server',
        		2 => 'Problems with chat/videochat',
        		3 => 'Can\'t login to site',
        		4 => 'Bug report',
        		5 => 'Other problem'
        	];
			$message .= "\r\n<br/>Options: " . $myOptions[$model->subject];
			$message .= "\r\n<br/>Message: " . $model->body;
			$message = HtmlPurifier::process($message);
			$fromUserID = Yii::$app->request->post('fromUserID');
        	$fromUserID = (isset($fromUserID) && is_numeric($fromUserID)) ? (int)$fromUserID : null;
        	$params = [
		        'fromUserID' => $fromUserID,
		        'desc' => $message,
		        'caption' => 'Message from contact as page',
	        ];
	        $result = json_decode(self::sendCurl($session['user_token'], $params, "/v1/message/send-message-to-admin")); 

	        if (!$result) {
	        	$session->set('message', 'Error during message send');
	        	return $this->redirect(Yii::$app->request->referrer);
	        }

	        $from = Yii::$app->params['adminEmail'];
	        if (YII_DEBUG) {
	        	$to = 'darkstill@ukr.net';
	        } else {
	        	$to = 'mmxam@bk.ru';
	        }
	        
	        $emailSendSuccessfully = Yii::$app->mailer->compose()
	            ->setTo($to)
	            ->setFrom($from)
	            ->setSubject('Contact Us')
	            ->setHtmlBody($message)
	            ->send();	        
	        
	        if ($emailSendSuccessfully) {
				$session->set('message', 'Message to support send');
	        } else {
				$session->set('message', 'Error send message');
	        }
            return $this->redirect(Yii::$app->request->referrer);
        } else {
        	$session->set('message', join(', ', $model->getFirstErrors()));
        	return $this->redirect(Yii::$app->request->referrer);
        }
    }

	public function actionSubscribe()
	{
		$session = Yii::$app->session;
		$post = Yii::$app->request->post();

		$email = (isset($post['email']) && trim($post['email']) != "") ? trim($post['email']) : false;

		if (!$email) {
			$session->set('message', 'Error email');
			return $this->redirect('/');
		}

		$params = [
			'skinID' => 1,
			'email' => $email
		];

		$result = json_decode(self::sendCurl('', $params, "/site/add-to-subscribe"));

		if($result->success){
			$message = "Congratulation you email: ".$email . " was added to our subscription";
			$from = Yii::$app->params['adminEmail'];
			$emailSendSuccessfully = Yii::$app->mailer->compose()
				->setTo($email)
				->setFrom($from)
				->setSubject('You was added to subscribe')
				->setHtmlBody($message)
				->send();
		}


		$session->set('message', $result->message);
		
		return $this->redirect($_SERVER['HTTP_REFERER']);
	}
}
