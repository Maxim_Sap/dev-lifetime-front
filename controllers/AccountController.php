<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\controllers\BaseController;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\helpers\Json;
use yii\web\Cookie;

class AccountController extends BaseController
{

	private $numberOfGiftItemsOnFrontPage;   
	const JS_DATEPICKER = "$('.datepicker').datepicker();";
	const GIRLS_IN_SLIDER = 20;
	
	public function behaviors()
    {
        return [
        	'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['account/*'],
                'rules' => [
                    [
                        'actions' => ['logout, edit'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                    [
                        'actions' => ['register, login'],
                        'allow'   => true,
                        'roles'   => ['?'],
                    ],                    
                ],
            ],            
        ];
    }

    public function init()
    {
		parent::init();
		$footer_data = json_decode(self::sendCurl('', [], "/site/get-footer-data"));
		$this->view->params['footer_post'] = $footer_data->posts;
		$this->view->params['footer_history'] = $footer_data->history_page;
		$this->numberOfGiftItemsOnFrontPage = Yii::$app->params['numberOfGiftItemsOnFrontPage'];
        $this->view->registerJsFile('/js/common.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/js/account.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        
    }
    
    public function beforeAction($action)
	{            

	    // disable CRSF validation for specific actions
	    $arrayOfActionsWithDisabledCSRFValidation = [
	    	'login', 'logout', 'update-session-avatar','add-funds-by-stripe',
	    	'get-chat-with-user','stop-chat','set-user-type-for-register',
	    	'add-video-chat-session','stop-video-session','add-gift-to-cart',
	    	'set-session-new-message','shop', 'reset-password','translate'];
	    if (in_array($action->id, $arrayOfActionsWithDisabledCSRFValidation)) {
	        $this->enableCsrfValidation = false;
	    }

	    return parent::beforeAction($action);
	}
	
    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex($userID)
    {

		$soc_token = Yii::$app->request->get('token');
        
        $session = Yii::$app->session;
        if ($soc_token != null) {
			$session['user_token'] = $soc_token;
			$session['user_id'] = $userID;
			$cookie = new Cookie([
				'name' => 'user_token',
				'value' => $soc_token,
				'expire' => time()+Yii::$app->params["remember_token_time"],
			]);
			\Yii::$app->getResponse()->getCookies()->add($cookie);
        }
		
		$session->set('user_type_for_register', self::USER_MALE); // by default men registered using social network
		
		$params = [
	        'userID' => $userID,
	        'token' => $session['user_token']
        ];		       
        
		$result = $this->getResponseFromAPI($params, "/user/get-user-info");
		//var_dump(self::sendCurl($session['user_token'], ['token' => $session['user_token']], "/user/get-user-info")); die;
		if (!$result) {
			return $this->redirect('/', 301);
		}

		$session->set('user_type', $result->userData->userType);
        
        if (!in_array($result->userData->userType, [self::USER_MALE, self::USER_FEMALE])) {
			$session->set('message', 'Error user type. Access denied.');
			unset($session['user_id']);
			unset($session['user_token']);
			$cookies = Yii::$app->response->cookies;
            $cookies->remove('user_token');
			setcookie('user_token', "", time()-3600,"/");
			return $this->redirect('/', 301);
        }
        
        if (!empty($result->userData->token)) {
        	self::UserAuthorization($result->userData);
        }

        if ($soc_token != null) {
        	return $this->redirect('/account/' . $userID, 301);
        }

		$offset = 0;
		$params = [
	        'skinID' => 1,
	        'limit' => self::GIRLS_IN_SLIDER,
            'offset' => $offset,
	        'type' => 'last_activity',
        ];

		$onlineGirls = json_decode(self::sendCurl($session['user_token'], $params, "/user/get-users"));		
		if (!self::checkCurlResponse($onlineGirls)) {
			return $this->redirect('/', 301);
        }
		
		$hairColors = JSON::decode($this->getCurl($session['user_token'], '/v1/hair-colors'));
		
		if (isset($hairColors['status']) && $hairColors['status'] == 401) {
			$session->set('message', $hairColors['message']); 
			return $this->redirect('/', 301);
		}

		$eyesColors = JSON::decode($this->getCurl($session['user_token'], '/v1/eyes-colors'));
		if (isset($eyesColors['status']) && $eyesColors['status'] == 401) {
			$session->set('message', $eyesColors['message']); 
			return $this->redirect('/', 301);
		}			

		$params = [
			'type' => 'inbox'
		];

		$messages = json_decode(self::sendCurl($session['user_token'], $params, "/v1/letter/count"));
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/letter/count")); die;
		if (!self::checkCurlResponse($messages)) {
			return $this->redirect('/', 301);
        }        

        $favorites = $this->getLadiesByParam('favorite', 50);
		
		$admirers = $this->getLadiesByParam('admires', 50);

		$waitingAdmirers = $this->getLadiesByParam('admires', 5);

		$matches = $this->getLadiesByParam('matches', 50);		

		$notifications = json_decode($this->getCurl($session['user_token'], "/v1/messages/count"));
		//var_dump($this->getCurl($session['user_token'], "/v1/messages/count")); die;
		if (!self::checkCurlResponse($notifications)) {
			return $this->redirect('/', 301);
        }

		$photoLikes = json_decode($this->getCurl($session['user_token'], "/v1/photo/like-all"));
		//var_dump($this->getCurl($session['user_token'], "/v1/photo/like-all")); die;
		if (!self::checkCurlResponse($photoLikes)) {
			return $this->redirect('/', 301);
        }      

        $girlsLikedPhoto = json_decode($this->getCurl($session['user_token'], "/v1/user/girls-that-liked-photo"));
        //var_dump($this->getCurl($session['user_token'], "/v1/user/girls-that-liked-photo")); die;        
		if (!self::checkCurlResponse($girlsLikedPhoto)) {
			return $this->redirect('/', 301);
        }         
        
        $userBalance = $result->userData->balance;
	    if (empty($userBalance)) {
			$userBalance = "0.00";
	    } else {			
			$userBalance = $result->userData->balance;
	    }
        
        if ($result->userData->avatar->small != null) {
			$userAvatar = $this->serverUrl . "/" . $result->userData->avatar->small;
	    } else {
			if ($result->userData->userType == self::USER_MALE) {
	    		$userAvatar = $this->serverUrl . "/" . "img/no_avatar_small_man.jpg";
	    	} else {
	    		$userAvatar = $this->serverUrl . "/" . "img/no_avatar_small_girl.jpg";
	    	}
	    }
	    
	    if ($result->userData->avatar->normal != null) {
			$result->userData->avatar->normal = $this->serverUrl . "/" . $result->userData->avatar->normal;
	    } else {
			if ($result->userData->userType == self::USER_MALE) {
	    		$result->userData->avatar->normal = $this->serverUrl . "/img/no_avatar_normal_man.jpg";
	    	} else {
	    		$result->userData->avatar->normal = $this->serverUrl . "/img/no_avatar_normal_girl.jpg";
	    	}
	    }
        
        $userData = [
	        'userID' => $result->userData->personalInfo->id,
	        'userType' => $result->userData->userType,
	        'name' => $result->userData->personalInfo->first_name,
	        'avatar' => $userAvatar,
			'avatarPhotoID' => $result->userData->avatarPhotoID,
	        'email'=> $result->userData->email,
            'balance' => (float)$userBalance,
	        'camOnline' => $result->userData->cam_online,
        ];
        
        $profileArray = [
        	$result->userData->personalInfo->first_name,
        	$result->userData->personalInfo->last_name,
        	$result->userData->personalInfo->phone,
        	$result->userData->personalInfo->birthday,        	
        	$result->userData->personalInfo->marital,
        	$result->userData->personalInfo->kids,
        	$result->userData->personalInfo->height,
        	$result->userData->personalInfo->weight,
        	$result->userData->personalInfo->eyes,
        	$result->userData->personalInfo->hair,
        	$result->userData->personalInfo->smoking,
        	$result->userData->personalInfo->alcohol,
        ];
        $i = 0;
        foreach ($profileArray as $item) {
			if ($item != null) {
				$i++;
			}
        }
        $multiplier = $i/count($profileArray)*100;
        $result->userData->coef_compate = round($multiplier);
        
        $js_script = /*"accountLibrary['getUsersOnAccountPage']();" .*/ self::JS_GET_MESSAGE_COUNT;        
		$this->view->registerJs($js_script, Yii\web\View::POS_READY);
        
        $session->set('user_data', $userData);
        
		$this->horizontalMenuName = "index";
		$renderFile = ($result->userData->userType == self::USER_MALE) ? 'index-man' : 'index';

        //return $this->redirect('/', 301);
        return $this->render($renderFile, [
        	'userData' => $result->userData,
        	'availableGirls' => $onlineGirls->girls,
        	'hairColors' => $hairColors,
        	'eyesColors' => $eyesColors,
        	'countryList' => $result->countryList,
        	'messagesCount' => $messages->count,
        	'favoritesCount' => $favorites->count,
        	'admirersCount' => $admirers->count,
        	'matchesCount' => $matches->count,
        	'notificationsCount' => $notifications->count,
        	'photoLikes' => $photoLikes->likes,
        	'waitingAdmirers' => $waitingAdmirers->girls,
        	'girlsLikedPhoto' => $girlsLikedPhoto->girls
        ]);
    }
    
    public function actionGallery()
    {
        $session = Yii::$app->session;    
		
        $params = [
	        'token' => $session['user_token'],
        ];
        
		$result = $this->getResponseFromAPI($params,"/user/get-albums-list-info");        
        //var_dump(self::sendCurl($session['user_token'], ['token' => $session['user_token']], "/user/get-albums-list-info")); die;
		if (!$result) {
			return $this->redirect('/', 301);
		}
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, Yii\web\View::POS_READY);
        $this->verticalMenuName = "gallery";
		$this->horizontalMenuName = "index";
		return $this->render('gallery', [
			'albums'=>$result->albums
		]);
    }

    public function actionFavorite()
    {
		$session = Yii::$app->session;
		$girls = $this->getLadiesByParam('favorite', 20);		
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, Yii\web\View::POS_READY);
		$this->horizontalMenuName = "ladies";
		$this->verticalMenuName = "favorite";
		return $this->render('ladies-favorite', [
            'girls' => $girls->girls,
            'count' => $girls->count,
            'user_type' => $session['user_data']['userType']
        ]);
    }

    public function actionWinks()
    {
		$session = Yii::$app->session;
		$girls = $this->getLadiesByParam('winks', 20);		
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, Yii\web\View::POS_READY);
		$this->horizontalMenuName = "ladies";
		$this->verticalMenuName = "winks";
		return $this->render('ladies-winks', [
			'girls' => $girls->girls,
			'count' => $girls->count,
			'user_type' => $session['user_data']['userType']
		]);
    }

	public function actionUpdateBalance()
    {
		$session = Yii::$app->session;
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		
		$result = json_decode(self::getCurl($session['user_token'], "/v1/user/balance"));
		//var_dump(self::getCurl($session['user_token'], "/v1/user/balance")); die;	
        
        if (isset($result->status) && $result->status == 401) {        	
        	return [
        		'success' => false,
				'message' => $result->message
        	];
        }
        if(!$result->success){
			return [
				'success' => false,
				'message' => $result->message
			];
        }
        
        $userBalance = number_format($result->balance, 2);
        
		if($userBalance == 0){
			$userBalance = "0.00";
	    }
	    
	    $session_data = $session['user_data'];
	    $session_data['balance'] = $userBalance;
	    $session['user_data'] = $session_data;
	    
	    return [
	    	'success'=>true,
	    	'balance'=>$userBalance
	    ];
	    
    }

    public function getLadiesByParam($type='favorite', $limit=20)
    {
		$session = Yii::$app->session;	
		
		$params = [
            'limit' => $limit,
            'skinID' => 1,
            'userType' => ($session['user_data']['userType'] == self::USER_MALE) ? self::USER_FEMALE : self::USER_MALE,
            'userID' => $session['user_data']['userID'],
            'type' => $type
        ];
		
		$result = $this->getResponseFromAPI($params, "/user/get-users");
		if (!$result) {
			//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-users")); die;
			return $this->redirect('/', 301);
		}
		
		//var_dump($result); die;
        return $result;
    }

    public function actionPhotoDelete($photoID)
    {
        $session = Yii::$app->session;            
		
        $params = [	        
	        'token' => $session['user_token'],
	        'photoID' => $photoID,
	        'status' => 0
        ];
        
		$result = $this->getResponseFromAPI($params,"/v1/photo/set-photo-status");
    	
		$session->set('message', 'Photo deleted');
		return $this->redirect('/account/gallery', 301);		
    }

	public function actionVideo()
	{
		$session = Yii::$app->session;		

		$params = [			
			'token'   => $session['user_token']
		];


		$result = $this->getResponseFromAPI($params, "/user/get-user-video");
		//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-user-video")); die;
		if (!$result) {
			return $this->redirect('/', 301);
		}
		
/*		$js_video = '';
		if (!empty($result->videoList)) {
			foreach ($result->videoList as $video) {
				$js_video .= 'jwplayer("video_id_'.$video->id.'").setup({'
					.'file: "'.$this->serverUrl . "/" . $video->path.'",'
					.'image: "'.((!empty($video->medium_thumb)) ? $this->serverUrl . "/" . $video->medium_thumb : '').'",'
					.'}); ';
			}
		}*/
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		//$this->view->registerJsFile('/js/jwplayer/jwplayer.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		//$this->view->registerJs($js_video, Yii\web\View::POS_READY);

		$this->verticalMenuName = "video";
		$this->horizontalMenuName = "index";

		return $this->render('video', ['videoList' => $result->videoList]);
	}

	public function actionGuests()
    {
		$session = Yii::$app->session;
		$girls = $this->getLadiesByParam('guest', 20);		
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, Yii\web\View::POS_READY);
		$this->horizontalMenuName = "ladies";
		$this->verticalMenuName = "guests";
		return $this->render('ladies-guests',[
			'girls' => $girls->girls,
			'count' => $girls->count,
			'user_type' => $session['user_data']['userType']
		]);
    }

	public function actionBlacklist()
    {
		$session = Yii::$app->session;
		$girls = $this->getLadiesByParam('blacklist',20);		
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, Yii\web\View::POS_READY);
		$this->horizontalMenuName = "ladies";
		$this->verticalMenuName = "blacklist";
		return $this->render('ladies-blacklist',[
			'girls' => $girls->girls,
			'count' => $girls->count,
			'user_type' => $session['user_data']['userType']
		]);
    }

    public function actionEdit($userID)
    {
		$session = Yii::$app->session;

		if ($session['user_type'] == self::USER_FEMALE) {
			$session->set('message', 'Access denied');
			return $this->redirect('/', 301);
		}
		
		$params = [	        
	        'token' => $session['user_token']
        ];
		
		$result = $this->getResponseFromAPI($params, "/user/get-user-info");         
        //var_dump(self::sendCurl($session['user_token'], ['token' => $session['user_token']], "/user/get-user-info")); die;
		if (!$result) {
			return $this->redirect('/', 301);
		}

		$hairColors = JSON::decode($this->getCurl($session['user_token'], '/v1/hair-colors'));
		
		if (isset($hairColors['status']) && $hairColors['status'] == 401) {
			$session->set('message', $hairColors['message']); 
			return $this->redirect('/', 301);
		}

		$eyesColors = JSON::decode($this->getCurl($session['user_token'], '/v1/eyes-colors'));
		if (isset($eyesColors['status']) && $eyesColors['status'] == 401) {
			$session->set('message', $eyesColors['message']); 
			return $this->redirect('/', 301);
		}						

		$physiques = JSON::decode($this->getCurl($session['user_token'], '/v1/physiques'));
		if (isset($physiques['status']) && $physiques['status'] == 401) {
			$session->set('message', $physiques['message']); 
			return $this->redirect('/', 301);
		}

        $result->userData->avatar->normal = (!empty($result->userData->avatar->normal)) ? $this->serverUrl . "/" . $result->userData->avatar->normal : "/img/no_avatar_normal.jpg";
        $userAvatar = (!empty($result->userData->avatar->small)) ? $this->serverUrl."/".$result->userData->avatar->small : "/img/no_avatar_small.jpg";
        
        $userData = [
	        'userID' => $result->userData->personalInfo->id,
	        'userType' => $result->userData->userType,
	        'name' => $result->userData->personalInfo->first_name,
	        'avatar' => $userAvatar,
			'avatarPhotoID' => $result->userData->avatarPhotoID,
	        'email'=> $result->userData->email,
            'balance' => $result->userData->balance,
	        'camOnline' => $result->userData->cam_online,
        ];
        
        $session->set('user_data', $userData);        
        
        $js_script = self::JS_GET_MESSAGE_COUNT;
        //$js_script .= 'accountLibrary["getDocumentAlbum"]();';
		$this->view->registerJs($js_script, Yii\web\View::POS_READY);
		//$this->view->registerJs(self::JS_DATEPICKER, Yii\web\View::POS_READY);
		$this->view->registerJsFile('/js/bootstrap-datepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerCssFile('/css/datepicker.css');
		
        $this->verticalMenuName = "edit";
		$this->horizontalMenuName = "index";
        
        return $this->render('edit', [
        	'userData' => $result->userData,
        	'hairColors' => $hairColors,
        	'eyesColors' => $eyesColors,
        	'physiques' => $physiques,
        	'countryList' => $result->countryList
        ]);
    }

    public function actionChat($otherUserID = null)
    {
		$this->layout = 'chat';
		
		$session = Yii::$app->session;

		if ($session['user_id'] == $otherUserID) {
			$session->set('message', "You can\'t chatting with yourself");
			return $this->redirect('/', 301);
		}

		$findUserType = (isset($session['user_data']['userType']) && $session['user_data']['userType'] == self::USER_FEMALE) ? self::USER_MALE : self::USER_FEMALE;

		if ($otherUserID === null) {
			$fakeUser = true;
		} else {
			$fakeUser = false;
		}		

		/*$params = [
	        'skinID' => 1,	        
	        'userType' => $findUserType,
	        'otherUserID' => $otherUserID,
	        'limit' => 4,	        
        ];

		$result = $this->getResponseFromAPI($params, "/v1/user/get-girls-to-chat-page");*/		
        //var_dump(self::sendCurl($session['user_token'], $params, "/v1/user/get-girls-to-chat-page")); die;

		$contactList = json_decode($this->getCurl($session['user_token'], '/v1/user/contacts'));
		//var_dump($this->getCurl($session['user_token'], '/v1/user/contacts?sort=+last_name&per_page=1')); die;
		if ((isset($contactList->success) && $contactList->success == FALSE) || (isset($contactList->status) && ($contactList->status == 401 || $contactList->status == 404 || $contactList->status == 500))) {
			$session->set('message', 'Error during get user contacts');
			return $this->redirect('/', 301);
		}

		$params = [
	        'otherUserID'=> $otherUserID,	        
        ];
        if (!$fakeUser) {
        	$result2 = $this->getResponseFromAPI($params, "/user/get-other-user-info");
        	//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-other-user-info")); die;
        } else {
        	$result2 = true;
        }        
        
		if (/*!$result || */!$result2) {
			return $this->redirect('/', 301);
        }
        
		if (isset($session['user_data']['userType']) && $session['user_data']['userType'] == self::USER_MALE) {
			// add payment for male
			//$this->view->registerJsFile('/js/RTCMultiConnection.min.js', ['depends' => [\app\assets\ChatAsset::className()]]);
			$this->view->registerJsFile('/js/adapter.js', ['depends' => [\app\assets\ChatAsset::className()]]);
			$this->view->registerJsFile('/js/manChat.js', ['depends' => [\app\assets\ChatAsset::className()]]);
		} else {
			//$this->view->registerJsFile('/js/RTCMultiConnection.min.js', ['depends' => [\app\assets\ChatAsset::className()]]);
			$this->view->registerJsFile('/js/adapter.js', ['depends' => [\app\assets\ChatAsset::className()]]);
			$this->view->registerJsFile('/js/girlChat.js', ['depends' => [\app\assets\ChatAsset::className()]]);
			$this->view->registerJsFile('/js/mailingTemplates.js', ['depends' => [\app\assets\ChatAsset::className()]]);
		}
		
		return $this->render('chat', [
			'user' => [
				'first_name' => (!$fakeUser) ? $result2->userData->personalInfo->first_name : '',
				'last_name' => (!$fakeUser) ? $result2->userData->personalInfo->last_name : '',
				'birthday' => (!$fakeUser) ? $result2->userData->personalInfo->birthday : null,
				'id' => (!$fakeUser) ? $result2->userData->personalInfo->id : null,
				'country' => (!$fakeUser) ? $result2->userData->location : null,
				'city' => (!$fakeUser) ? $result2->userData->city : null,
				'marital' => (!$fakeUser) ? $result2->userData->personalInfo->marital : null,
				'online' => (!$fakeUser) ? (($result2->userData->last_activity > time() - 30) ? 1 : 0) : null,
				'height' => (!$fakeUser) ? $result2->userData->personalInfo->height : null,
				'weight' => (!$fakeUser) ? $result2->userData->personalInfo->weight : null,
				'cam_online' => (!$fakeUser) ? $result2->userData->cam_online : null,
				'small_avatar' => (!$fakeUser) ? $result2->userData->avatar->small : null,
				'medium_avatar' => (!$fakeUser) ? $result2->userData->avatar->normal : null,
				'inFavourite' => (!$fakeUser) ? $result2->userData->in_favorite : null
			],
/*			'userLastChat' => $result->userLastChat,
			'userLastChatCount' => $result->userLastChatCount,
			'userActiveChat' => $result->userActiveChat,			
			'userActiveChatCount' => $result->userActiveChatCount,			
			'userFavorite' => $result->userFavorite,*/
			'contactList' => $contactList->contact_list,
			'otherUserID' => $otherUserID,
			'fakeUser' => $fakeUser
		]);
    }
    /* 
    *	Тестируем новый вариант видеочата. Пока не копивароть 
    *	этот action в продакшен  
    */
    public function actionChat1($otherUserID = null)
    {
		$this->layout = 'chat';
		
		$session = Yii::$app->session;

		if ($session['user_id'] == $otherUserID) {
			$session->set('message', "You can\'t chatting with yourself");
			return $this->redirect('/', 301);
		}

		$findUserType = (isset($session['user_data']['userType']) && $session['user_data']['userType'] == self::USER_FEMALE) ? self::USER_MALE : self::USER_FEMALE;

		if ($otherUserID === null) {
			$fakeUser = true;
		} else {
			$fakeUser = false;
		}		

		$params = [
	        'skinID' => 1,	        
	        'userType' => $findUserType,
	        'otherUserID' => $otherUserID,
	        'limit' => 4,	        
        ];

		$result = $this->getResponseFromAPI($params, "/v1/user/get-girls-to-chat-page");		
        //var_dump(self::sendCurl($session['user_token'], $params, "/v1/user/get-girls-to-chat-page")); die;
		$params = [
	        'otherUserID'=> $otherUserID,	        
        ];
        if (!$fakeUser) {
        	$result2 = $this->getResponseFromAPI($params, "/user/get-other-user-info");
        	//var_dump(self::sendCurl($session['user_token'], $params, "/user/get-other-user-info")); die;
        } else {
        	$result2 = true;
        }        
        
		if (!$result || !$result2) {
			return $this->redirect('/', 301);
        }
        
		if (isset($session['user_data']['userType']) && $session['user_data']['userType'] == self::USER_MALE) {
			// add payment for male			
			$this->view->registerJsFile('/js/adapter.js', ['depends' => [\app\assets\ChatAsset::className()]]);
			$this->view->registerJsFile('/js/manChat.js', ['depends' => [\app\assets\ChatAsset::className()]]);
		} else {			
			$this->view->registerJsFile('/js/adapter.js', ['depends' => [\app\assets\ChatAsset::className()]]);
			$this->view->registerJsFile('/js/girlChat.js', ['depends' => [\app\assets\ChatAsset::className()]]);
			$this->view->registerJsFile('/js/mailingTemplates.js', ['depends' => [\app\assets\ChatAsset::className()]]);
		}
		
		return $this->render('chat1', [
			'user' => [
				'first_name' => (!$fakeUser) ? $result2->userData->personalInfo->first_name : '',
				'last_name' => (!$fakeUser) ? $result2->userData->personalInfo->last_name : '',
				'birthday' => (!$fakeUser) ? $result2->userData->personalInfo->birthday : null,
				'id' => (!$fakeUser) ? $result2->userData->personalInfo->id : null,
				'country' => (!$fakeUser) ? $result2->userData->location : null,
				'city' => (!$fakeUser) ? $result2->userData->city : null,
				'marital' => (!$fakeUser) ? $result2->userData->personalInfo->marital : null,
				'cam_online' => (!$fakeUser) ? $result2->userData->cam_online : null,
				'small_avatar' => (!$fakeUser) ? $result2->userData->avatar->small : null,
				'medium_avatar' => (!$fakeUser) ? $result2->userData->avatar->normal : null,
				'inFavourite' => (!$fakeUser) ? $result2->userData->in_favorite : null
			],
			'userLastChat' => $result->userLastChat,
			'userLastChatCount' => $result->userLastChatCount,
			'userActiveChat' => $result->userActiveChat,			
			'userActiveChatCount' => $result->userActiveChatCount,			
			'userFavorite' => $result->userFavorite,
			'otherUserID' => $otherUserID,
			'fakeUser' => $fakeUser
		]);
    }

    public function actionAlbum($albumID)
    {
        $session = Yii::$app->session;            
		
        $params = [	        
	        'albumID'=> $albumID
        ];
        
		$result = $this->getResponseFromAPI($params,"/v1/photo/get-photos");
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/photo/get-photos")); die;   	
        if (!$result) {
			return $this->redirect('/', 301);
        }

		$result2 = $this->getResponseFromAPI($params,"/v1/album/get-albums-list-info");
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/album/get-albums-list-info")); die;  
		//var_dump($result2) 	;die;
        if (!$result2) {
			return $this->redirect('/', 301);
        } 
        foreach ($result2->albums as $album) {
        	if ($albumID == $album->id) {
        		$albumInfo = $album;
        	}        	
        }
		$params = [	        
	        'token' => $session['user_token']
        ];

		$result3 = $this->getResponseFromAPI($params, "/user/get-user-info"); 
		//var_dump(self::sendCurl($session['user_token'], ['token' => $session['user_token']], "/user/get-user-info")); die;
		if (!$result3) {
			return $this->redirect('/', 301);
		}
        $userAvatar = (!empty($result3->userData->avatar->small)) ? $this->serverUrl."/".$result3->userData->avatar->small : "/img/no_avatar_small.jpg";
        $userData = [
	        'userID' => $result3->userData->personalInfo->id,
	        'userType' => $result3->userData->userType,
	        'name' => $result3->userData->personalInfo->first_name,
	        'avatar' => $userAvatar,
			'avatarPhotoID' => $result3->userData->avatarPhotoID,
	        'email'=> $result3->userData->email,
            'balance' => $result3->userData->balance,
	        'camOnline' => $result3->userData->cam_online,
        ];
        
        $session->set('user_data', $userData);

		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, Yii\web\View::POS_READY);
		$this->view->registerJsFile('/js/upload_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
		$this->view->registerJsFile('/js/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->verticalMenuName = "gallery";
		$this->horizontalMenuName = "index";
		return $this->render('album', [
			'photos' => $result->photos,
			'albumInfo' => $albumInfo
		]);
    }

    public function actionPhoto($photoID)
    {
        $session = Yii::$app->session;    
		
        $params = [	        	        
	        'photoID' => $photoID
        ];
        
		$result = $this->getResponseFromAPI($params,"/v1/photo/get-one-photo");
		//var_dump(self::sendCurl($session['user_token'], $params, "/v1/photo/get-one-photo")); die;    	    
		if (!$result) {
			return $this->redirect('/', 301);
		}
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, Yii\web\View::POS_READY);
        $this->verticalMenuName = "gallery";
		$this->horizontalMenuName = "index";
		
		return $this->render('photo',[
			'photo'=>$result->photo
		]);
    }

	public function actionGetDocumentAlbum()
	{
		$session = Yii::$app->session;
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$params = [
			'user_id' => $session['user_id'],
			'token' => $session['user_token'],
			'albums' => 'document'
		];

		$result = $this->getResponseFromAPI($params, "/user/get-photos");

		if (!empty($result->photos)) {
			$photos = Yii::$app->controller->renderPartial('//layouts/parts/document-attach-photos.php', [
				'photos' => $result->photos
			]);			
			return [
				'success' => true, 
				'photos' => $photos
			];
		}

		return $result;
	}
    
    public function actionUpdateSessionAvatar()
    {   
		$avatar = Yii::$app->request->post('avatar');
		$avatarPhotoID = Yii::$app->request->post('avatar_photo_id');
		$session = Yii::$app->session;
		$userData = $session['user_data'];
		$userData['avatar'] = $avatar;
		$userData['avatar_photo_id'] = $avatarPhotoID;
		$session->set('user_data', $userData);
    }
    
    public function actionEditPassword()
    {   
		$session = Yii::$app->session;
        $this->checkIfUserLoggedIn();
				
		$this->view->registerJs(self::JS_GET_MESSAGE_COUNT, Yii\web\View::POS_READY);
		$this->verticalMenuName = "edit";
		$this->horizontalMenuName = "index";
        
        return $this->render('edit-password');
    }
    	
    public function actionRegister($token)
    {       	
		$session = Yii::$app->session;
		$result = json_decode(self::sendCurl($session['user_token'], ['token' => $token,'ip'=>$_SERVER['REMOTE_ADDR']], "/reg-email/check"));
		if (!$result) {
			throw new BadRequestHttpException('Error occurred while processing your request');
		}
		
	    if (!isset($result->success) || $result->success == false) {		    	
			$errorMessage = isset($result->message) ? $result->message : "Error occurred while processing your request";
			$session->set('message', $errorMessage);
			return $this->redirect('/', 301);
	    }
	    self::UserAuthorization($result->userInfo);

	  	return $this->redirect('/account/' . $result->userInfo->userID, 301);
    }
    
    public function actionChangePassword()
    {
        $get = Yii::$app->request->get();
        $session = Yii::$app->session;
        $this->checkIfUserLoggedIn();
        if (!empty($get)) {
            if (!isset($get['new_pass']) || trim($get['new_pass']) == "" || 
            	!isset($get['code']) || trim($get['code']) == "" || 
            	!isset($get['email']) || trim($get['email']) == "") {
                throw new BadRequestHttpException('Missing params');
            }
		}
		
		$result = json_decode(self::sendCurl($session['user_token'], $get,"/reg-email/change-password-check"));
		
	    if (!$result) {
			throw new BadRequestHttpException('Error send request');
	    }
	    if (!isset($result->success) || $result->success == false) {
			$errorMessage = isset($result->message) ? $result->message : "Error send request";
			$session->set('message', $errorMessage);
			return $this->redirect('/', 301);
	    }
	    
	    self::UserAuthorization($result->userInfo);

	  	$session = Yii::$app->session;
		$session->set('message', 'Password change');
			
		return $this->redirect('/account/' . $result->user_info->user_id, 301);
    }
        
    
    public function actionResetPassword($token)
    {    	    	
    	$session = Yii::$app->session;
    	$postArray = Yii::$app->request->post();
    	$result = json_decode(self::sendCurl($session['user_token'], [
	    			'token' => $token,
	        	], 
	        	"/v1/reg-email/reset-password-check"));

    	if (isset($result->success) && $result->success == false && $result->code != "Validation errors") {
			$errorMessage = isset($result->message) ? $result->message : "Error occurred while processing your request";
			$session->set('message', $errorMessage);
			return $this->redirect('/', 301);
    	}

    	if (!empty($postArray)) {    			    	
    		$result = json_decode(self::sendCurl($session['user_token'], [
	    			'token' => $token,
		        	'ResetPasswordForm' => [
		        		'password' => $postArray['password'],
		        		'confirmPassword' => $postArray['confirmPassword']
		        	]
	        	], 
	        	"/v1/reg-email/reset-password-check"));
    		
	    	if (isset($result->success) && $result->success == false && $result->code == "Validation errors") {
				$text = '';
				foreach ((Array)$result->errors as $error) {
	    			$text .=  $error[0] . " ";
	    		}	    		
				$session = Yii::$app->session;
				$errorMessage = isset($result->errors) ? $text : "Error occurred while processing your request";
				$session->set('message', $errorMessage);
				return $this->render('resetPassword');	    		
	    	} elseif (isset($result->success) && $result->success == false) {
				$session = Yii::$app->session;
				$errorMessage = isset($result->message) ? $result->message : "Error occurred while processing your request";
				$session->set('message', $errorMessage);
				return $this->render('resetPassword');
	    	} elseif (isset($result->success) && $result->success == true) {
				$session = Yii::$app->session;
				$message = "New password saved. Use sign up form to login";
				$session->set('message', $message);
				return $this->redirect('/', 301);
	    	}
    	} else {
    		return $this->render('resetPassword');	
    	}
    }
    
    public function actionRegGoogle() 
    {
		if (!isset($_GET['code'])) {
            throw new BadRequestHttpException('Missing params Google Auth');
        }
        $googleConfig = Yii::$app->params['google_auth'];

        $params = array(
            'client_id'             => $googleConfig['client_id'],
            'client_secret'         => $googleConfig['client_secret'],
            'redirect_uri'          => $googleConfig['redirect_uri'],
            'url_token'  	        => $googleConfig['url_token'],
            'grant_type'            => 'authorization_code',
            'code'                  => $_GET['code'],
            'get_user_info_url'     => $googleConfig['get_user_info_url'],
        );
        $session = Yii::$app->session;
        $userType = self::USER_MALE;
        
        //user_type = 1 // i.e. male        	
        $result = json_decode(self::sendCurl($session['user_token'], [
	        	'user_type' => $userType,
				'userIp'=>$_SERVER['REMOTE_ADDR'],
				'referralLink'=>isset($session['referral_link']) ? $session['referral_link'] : null,
	        	'google_conf' => $params
        	], 
        	"/reg-social/google"));        
        
        if (!isset($result->success) || $result->success == false) {			
			$errorMessage = isset($result->message) ? $result->message : "Error send request";
			$session->set('message', $errorMessage);
			unset($session['user_id']);
			unset($session['userType']);
			unset($session['user_type']);
			unset($session['user_token']);
			unset($session['user_data']);
			unset($session['chat_session']);
			$cookies = Yii::$app->response->cookies;
	        $cookies->remove('user_token');
			setcookie('user_token', "", time()-3600, "/");
			return $this->redirect('/', 301);
		}
//var_dump(!isset($result->success) || $result->success == false); die;
		//var_dump($result);die;
		self::UserAuthorization($result->userInfo);
	  	
		$cookie = new Cookie([
			'name' => 'user_token',
			'value' => $result->userInfo->token,
			'expire' => time()+Yii::$app->params["remember_token_time"],
		]);
		\Yii::$app->getResponse()->getCookies()->add($cookie);

		return $this->redirect('/account/' . $result->userInfo->userID, 301);        
    }
    
    public function actionLogin()
    {
    	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$postArray = Yii::$app->request->post();
        if (empty($postArray)) {        	
			return [
				'success' => false, 
				'code' => 1, 
				'message' => 'Missing params'
			];
        }
        if (!isset($postArray['email']) || trim($postArray['email']) == "" || 
        	!isset($postArray['password']) || trim($postArray['password']) == "" || 
        	!isset($postArray['remember'])) {        	
        	return [
        		'success' => false, 
        		'code' => 1, 
        		'message' => 'Missing params'
        	];
        }
        
        $result = json_decode(self::sendCurl(Yii::$app->session['user_token'], $postArray, "/user/login"));
        //var_dump(self::sendCurl(Yii::$app->session['user_token'], $postArray, "/user/login")); die;
		if (isset($result->success) && $result->success == false && $result->code == "Validation errors") {
			return [
				'success' => false, 
				'code' => $result->code,
				'errors' => $result->errors				
			];
		} elseif (isset($result->success) && $result->success == false) {
			return [
				'success' => false, 
				'code' => $result->code, 
				'message' => $result->message
			];
		}
		
		self::UserAuthorization($result->userInfo);
		
		if ($postArray['remember'] == 1) {			
			Yii::$app->session->timeout = 7*24*60*60;
			$cookie = new Cookie([
				'name' => 'user_token',
				'value' => $result->userInfo->token,
				'expire' => time()+Yii::$app->params["remember_token_time"],
			]);
			\Yii::$app->getResponse()->getCookies()->add($cookie);
			//setcookie('user_token', , time()+Yii::$app->params["remember_token_time"], "/"); //1 week
		} else {
			$cookie = new Cookie([
				'name' => 'user_token',
				'value' => $result->userInfo->token,
				'expire' => time()+3*60*60,
			]);
			\Yii::$app->getResponse()->getCookies()->add($cookie);
		}
		
		return [
			'success' => true,
			'user_id' => $result->userInfo->userID
		];
    }
    
    public function actionLogout()
    {
    	$token = Yii::$app->request->post('token');
		$session = Yii::$app->session;		
		
		$result = json_decode(self::sendCurl(Yii::$app->session['user_token'], ["token" => $token], "/user/logout"));

		if (!self::checkCurlResponse($result)) {
			return JSON::encode([
				'success' => false
			]);
		}
		unset($session['user_id']);
		unset($session['userType']);
		unset($session['user_type']);
		unset($session['user_token']);
		unset($session['user_data']);
		unset($session['chat_session']);
		$cookies = Yii::$app->response->cookies;
        $cookies->remove('user_token');
		setcookie('user_token', "", time()-3600, "/");
		return JSON::encode(['success' => true]);
    }
    
    public function actionShop($otherUserID)
    {
        $session = Yii::$app->session;
        $ajax    = Yii::$app->request->getIsAjax();

        if (!isset($session['user_token'])) {
            $session->set('message', 'Error token. Please login');

            return $this->redirect('/', 301);
        }

        if($session['user_type'] != self::USER_MALE){
			$session->set('message', 'Access denied');
            
            return $this->redirect('/', 301);
        } 
        
        if ($session['user_id'] == $otherUserID) {
            $session->set('message', 'You can`t send gift yourself');

            return $this->redirect('/', 301);
        }


        $offset        = 0;
        $page          = 1;
		$searchParams = [
			'sort' => 'asc',
			'giftsCategory' => [],
        ];

        if ($ajax) {
            $page          = \Yii::$app->request->post('page');
			$parameters = \Yii::$app->request->post('searchParams');
			$searchParams = [
				'sort' => isset($parameters['sort']) && $parameters['sort'] == 'desc' ? 'desc' : 'asc',
				'giftsCategory' => isset($parameters['giftCategoryArray']) && is_array($parameters['giftCategoryArray']) && !empty($parameters['giftCategoryArray']) ? $parameters['giftCategoryArray'] : [],
			];
        }

        if (empty($page) or $page < 0) $page = 1;
        $offset = $page * $this->numberOfGiftItemsOnFrontPage - $this->numberOfGiftItemsOnFrontPage;

		$params = [
			'skinID' => 1,
			'token' => $session['user_token'],
			'limit' => $this->numberOfGiftItemsOnFrontPage,
			'page' => $page-1,
			'searchParams' => $searchParams,
			'otherUserID'=> $otherUserID,
		];

		$result = json_decode(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/gifts/get-gifts"));

		if (isset($result->success) && $result->success == false) {
			return [
				'success' => false,
				'code' => $result->code,
				'errors' => $result->errors
			];
		}

		if (!$ajax) {
			$result2 = $this->getResponseFromAPI($params, "/user/get-other-user-info");
			if (isset($result2->success) && $result2->success == FALSE) {
				return [
					'success' => FALSE,
					'code'    => $result2->code,
					'errors'  => $result2->errors
				];
			}

			if ($result2->userData->avatar->normal != null) {
				$user_avatar = $this->serverUrl . "/" . $result2->userData->avatar->normal;
			} else {
				$user_avatar = "/img/no_avatar_normal.jpg";
			}
		}

        if ($ajax) {
            $gifts_mass = Yii::$app->controller->renderPartial('//account/shop', [
				'giftsArray' => $result->giftsArray,
				//'user_info' => $result2->user_info,
				'other_user_id' => $otherUserID,
				'total_count' => $result->giftsCount,
				'page' => $page,
				'search_params' => $searchParams
			]);
            if ($gifts_mass != '') {
                return json_encode(['success' => TRUE, 'code' => 1, 'message' => 'ok', 'giftsHtml' => $gifts_mass]);
            }

            return json_encode(['success' => FALSE, 'code' => 1, 'message' => 'Missing params']);
        }

        //$js_script = $this->js_getNewMessageCount;
        //$this->view->registerJs($js_script, Yii\web\View::POS_READY);
        return $this->render('shop', [
			'giftsArray' => $result->giftsArray,
			'user_info' => $result2->userData,
			'avatar'=>$user_avatar,
			'other_user_id' => $otherUserID,
			'total_count' => $result->giftsCount,
			'page' => $page,
			'search_params' => $searchParams
		]);
        //return $this->render('shop', ['other_user_id' => $otherUserID]);
    }

	public function actionAddGiftToCart()
	{
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$postArray = Yii::$app->request->post();
		$session = Yii::$app->session;
		$ajax    = Yii::$app->request->getIsAjax();

		if (!$ajax || !isset($session['user_token'])) {

			return [
				'success' => false,
				'code' => 1,
				'message' => 'Missing params'
			];
		}

		if($session['user_type'] != self::USER_MALE){
			return [
				'success' => false,
				'code' => 1,
				'message' => 'Access denied'
			];
		}

		$gift_id = $postArray['gift_id'];
		$price   = $postArray['price'];
		if (!isset($gift_id) || !is_numeric($gift_id) || !isset($price) || !is_numeric($price)) {
			return [
				'success' => false,
				'code' => 1,
				'message' => 'Gift_ID or Gift price error'
			];
		}

		$count      = 0;
		$last_price = 0;

		$params = [
			'skinID' => 1,
			'giftsIDs' => [(int)$gift_id],			
		];

		$result = json_decode(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/gifts/get-gifts"));
		//var_dump(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/gifts/get-gifts")); die;
		if ((isset($result->success) && $result->success == FALSE) || (isset($result->status) && ($result->status == 401 || $result->status == 404 || $result->status == 500))) {
			$session->set('message', 'Error during add gift to cart');
			return $this->redirect('/account/shop/' . $otherUserID, 301);
		}

		if (isset($session['basket'])) {
			$basket = $session['basket'];
			if (isset($basket[$gift_id])) {
				$count      = $basket[$gift_id]['count'];
				$last_price = $basket[$gift_id]['price'];
			}
		}
		foreach ($result->giftsArray as $gift) {
			if ($gift->id == $gift_id) {
				$price = $gift->price;
			} else {
				return [
					'success' => false,
					'code' => 1,
					'message' => 'Gift_ID or Gift price error'
				];
			}
		}
		
		//добавляем количество +1
		$basket[$gift_id]['count'] = $count + 1;
		$basket[$gift_id]['price'] = $last_price + $price;

		$session->set('basket', $basket);

		return [
			'success' => 'true',
			'basket' => $basket
		];

	}

	public function actionCart($otherUserID)
	{
		$session = Yii::$app->session;

		if (!isset($session['user_token'])) {
			$session->set('message', 'Missing params');
			return $this->redirect('/account/shop/' . $otherUserID, 301);
		}

		if($session['user_type'] != self::USER_MALE){
			$session->set('message', 'Access denied');
			return $this->redirect('/account/shop/' . $otherUserID, 301);
		}

		if (!isset($session['basket']) || empty($session['basket'])) {
			$session->set('message', 'Cart is empty');

			return $this->redirect('/account/shop/' . $otherUserID, 301);
		}

		foreach ($session['basket'] as $key => $one) {
			$gift_ids[] = $key;
		}

		$params = [
			'skinID' => 1,
			'token' => $session['user_token'],
			'giftsIDs' => $gift_ids,
			'otherUserID'=> $otherUserID,
		];

		$result = json_decode(self::sendCurl(Yii::$app->session['user_token'], $params, "/v1/gifts/get-gifts"));
		$result2 = $this->getResponseFromAPI($params, "/user/get-other-user-info");

		if (isset($result->success) && $result->success == FALSE || isset($result2->success) && $result2->success == FALSE) {
			$session->set('message', 'Cart has errors');
			return $this->redirect('/account/shop/' . $otherUserID, 301);
		}

		if ($result2->userData->avatar->normal != null) {
			$user_avatar = $this->serverUrl . "/" . $result2->userData->avatar->normal;
		} else {
			$user_avatar = "/img/no_avatar_normal.jpg";
		}

		//$js_script = $this->js_getNewMessageCount;
		//$this->view->registerJs($js_script, Yii\web\View::POS_READY);

		//return $this->render('cart', ['gifft_mass' => $ress->gift_mass, 'user_info' => $ress->user_info, 'other_user_id' => $other_user_id, 'basket' => $session['basket']]);
		return $this->render('cart', [
			'giftsArray' => $result->giftsArray,
			'user_info' => $result2->userData,
			'avatar'=>$user_avatar,
			'other_user_id' => $otherUserID,
			'basket' => $session['basket']
		]);

	}

	public function actionClearCart()
	{
		$session = Yii::$app->session;

		if (!isset($session['user_token'])) {
			$session->set('message', 'Missing params');
			return $this->redirect('/', 301);
		}

		if($session['user_type'] != self::USER_MALE){
			$session->set('message', 'Access denied');
			return $this->redirect('/', 301);
		}

		unset($session['basket']);
		$otherUserID = \Yii::$app->request->get('last_girl_id');
		if(is_numeric($otherUserID)){
			$session->set('message', 'Your gifts will soon be sent to the selected girl');
			return $this->redirect('/account/shop/' . $otherUserID, 301);
		}else{
			return $this->redirect('/', 301);
		}

	}

	public function actionMyGifts()
	{
		$session = Yii::$app->session;

		$params = [
			'skinID'=> 1,
		];

		$result = json_decode(self::SendCurl($session['user_token'], $params,"/v1/gifts/get-shop-items"));
		//var_dump(self::SendCurl($session['user_token'], $params,"/v1/gifts/get-shop-items")); die;
		if(!isset($result->success)){
			$session->set('message', $result->message);
			return $this->redirect('/', 301);
		}

		$this->verticalMenuName = "my-gifts";
		$this->horizontalMenuName = "index";

		return $this->render('my-gifts', ['giftsArray'=>$result->items]);
	}

	public function actionMyGiftsDetail($giftID)
	{
		$session = Yii::$app->session;

		$params = [
			'skinID'=> 1,
			'actionID'=>(int)$giftID,
		];

		$result = json_decode(self::SendCurl($session['user_token'], $params,"/v1/gifts/get-shop-items"));
		
		if(!isset($result->success)){
			$session->set('message', $result->message);
			return $this->redirect('/', 301);
		}

		$this->verticalMenuName = "my-gifts";
		$this->horizontalMenuName = "index";

		return $this->render('my-gifts-detail', [
			'giftsArray'=>$result->items,
			'giftID'=>$giftID
		]);


	}

	public function actionTranslate(){
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$text = Yii::$app->request->post('text');
		$lang = !empty(Yii::$app->request->post('lang')) ? Yii::$app->request->post('lang') : 'ru-en';
		$text = json_decode(self::TranslateText($text,$lang));
		if($text->code == 200){
			return $text->text[0];
		}else{
			return $text->message;
		}
	}
}