(function($) {
    'use strict';
    var userToken = window["commonAdminLibrary"]["userToken"];
    var apiServer = window["commonAdminLibrary"]["apiServer"];

    var webSocketServer = chatConfig['webSocketServer'];
    var chatPort = chatConfig['chatPort'];
    var videoChatPort = chatConfig['videoChatPort'];


    if (typeof console == "undefined") { this.console = { log: function (msg) {  } };}    
    if (typeof WEB_SOCKET_SWF_LOCATION !="undefined") {
        // if your brower not support websocket， we will use this flash automatic websocket modeling
        WEB_SOCKET_SWF_LOCATION = "/swf/WebSocketMain.swf";
        // enable flash websocket debug
        var WEB_SOCKET_DEBUG = false;
    }
    var chatWebSocket;
    var editor, loading = false;
    var chatID, videoChatID, client_list={}, chatWarnings = {};
    var audioEnabled = true;    

    var stream;
    var chatConnectionTries = {};

    function storageAvailable(type) {
        try {
            var storage = window[type],
                x = '__storage_test__';
            storage.setItem(x, x);
            storage.removeItem(x);
            return true;
        }
        catch(e) {
            return e instanceof DOMException && (
                // everything except Firefox
                e.code === 22 ||
                // Firefox
                e.code === 1014 ||
                // test name field too, because code might not be present
                // everything except Firefox
                e.name === 'QuotaExceededError' ||
                // Firefox
                e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
                // acknowledge QuotaExceededError only if there's something already stored
                storage.length !== 0;
        }
    }
    if (storageAvailable('localStorage')) {
        if(!localStorage.getItem('audioEnabled')) {
          saveSettings();
        } else {
          setup();
        }
    }

    function saveSettings() {        
        if ($(".sound-trigger-wrapper.enabled").length > 0) {
            localStorage.setItem('audioEnabled', 'true');
        } else {
            localStorage.setItem('audioEnabled', 'false');            
        }
        setup();
    }
    function setup() {        
        if (localStorage.getItem('audioEnabled') == 'true') {
            var audioEnabled = true;
            setTimeout(function() {
                $(".sound-trigger-wrapper").addClass('enabled sound-enabled').removeClass('sound-disabled');
            }, 500);
        } else {
            var audioEnabled = false;
            setTimeout(function() {
                $(".sound-trigger-wrapper").removeClass('enabled sound-enabled').addClass('sound-disabled');
            }, 500);
        }    
    }

    $(".sound-trigger-wrapper").on('click', function(){ 
        $(".sound-trigger-wrapper").toggleClass('enabled');       
        saveSettings();
    });

    function createChat(myUserID, otherUserID)
    {
        var ws;                    
        ws = new WebSocket("wss://"+webSocketServer+":" + chatPort);
        ws['myUserID'] = myUserID;
        ws['otherUserID'] = otherUserID;               
        ws.onopen = onopen;
        // when new message available print different information base on message type
        ws.onmessage = onmessage; 
        ws.onclose = function() {  
            if (chatID == null) {                
                chatConnectionTries[otherUserID]++;                
                if (chatConnectionTries[otherUserID] > 10) {
                    printError(myUserID, "Can't connect to chat server. Try to reload page or send email to site administration.");
                    console.log("Can't connect to chat server. Try to reload page or send email to site administration.");
                    client_list[ws['myUserID']][ws['otherUserID']] = null;
                    toLastChatMessage();
                } else {
                    console.log("Connection is closing, trying to reconnect in 3 seconds");                    
                    setTimeout(function() {
                        client_list[ws['myUserID']][ws['otherUserID']] = createChat(ws['myUserID'], ws['otherUserID']);                        
                    }, 3000);
                }                
            } else {
                client_list[ws['myUserID']][ws['otherUserID']] = null;
            }            
        };
        ws.onerror = function() {
            console.log("Some error occured");
        };
        return ws;
    }  

    function loadingSpinner()
    {
        $(".chat-box-messages-wrapper").html('<div class="sk-circle">' +
                                '<div class="sk-circle1 sk-child"></div>' +
                                '<div class="sk-circle2 sk-child"></div>' +
                                '<div class="sk-circle3 sk-child"></div>' +
                                '<div class="sk-circle4 sk-child"></div>' +
                                '<div class="sk-circle5 sk-child"></div>' + 
                                '<div class="sk-circle6 sk-child"></div>' +
                                '<div class="sk-circle7 sk-child"></div>' +
                                '<div class="sk-circle8 sk-child"></div>' +
                                '<div class="sk-circle9 sk-child"></div>' +
                                '<div class="sk-circle10 sk-child"></div>' + 
                                '<div class="sk-circle11 sk-child"></div>' +
                                '<div class="sk-circle12 sk-child"></div>' +
                            '</div>');   
    }   

    function onopen()
    {
        // login   
        var login_data = {
            "type":"auth",
            "data": {
                "user": {
                    "id": this.otherUserID
                },
                "asUser": this.myUserID,
                "token": userToken
            }            
        };        
        console.log("websocket successful handshake, sending login data:" + JSON.stringify(login_data));
        this.send(JSON.stringify(login_data));  
        var thiz = this;
        var lastScrollTop = 0;
        $(".chat-box-messages-wrapper").on('scroll', function(event) {
            var otherUserID = $('.chat-list-block input.current-chat-other-user-id').val();
            //console.log(thiz);
            if (loading) {
                //loadingSpinner();
            } else {
                if (client_list[thiz.myUserID] != null && client_list[thiz.myUserID][otherUserID] != null) {
                    var st = $(this).scrollTop();        
                    if(st <= lastScrollTop && st == 0) {                   
                        var message = {
                            "type":"loadhistory",
                            "data": {
                                "user": {
                                    "id": otherUserID
                                },
                                'asUser': thiz.myUserID,
                                "token": userToken,
                                "messages": $(this).find(".chat-message-container").length + 10 - 1
                            }            
                        };                                                    
                        client_list[thiz.myUserID][otherUserID].send(JSON.stringify(message));                    
                    }
                }
            }            
            lastScrollTop = st;        
        });                      
    }         

    // when server sends a message
    function onmessage(e)
    {        
        var data = eval("("+e.data+")");
        console.log(data);
        switch (data['type']) {
            // ping from client
            case 'ping':
                this.send('{"type":"pong", "data":{"token":"' + userToken +'"}}');
                break;
            case 'error':
                printError(data['data']['user']['id'], data['data']['message']);
                chatID = data['data']['chatID'];
                toLastChatMessage();
                break;                
            // login to update user list
            case 'auth':                
                var history = data['data']['history'];
                if (typeof history != "undefined" && history != null && history.length > 0) {
                    $(".chat-box-messages-wrapper").html("");
                    for (var counter in history) {                                             
                        printMessageToChat(history[counter]['id'], history[counter]['username'], history[counter]['avatar'], history[counter]['message'], history[counter]['created_at'], history[counter]['readed_at']);
                    }
                } else {                    
                    if (!data['data']['join']) {
                        $(".chat-box-messages-wrapper").html("");
                        $(".chat-box-messages-wrapper").append('<div>You haven\'t any chat history with this user</div>');    
                    }                    
                }                
                if (data['data']['user']['id'] == this.otherUserID) {
                    printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['user']['username']+' registered in chat', data['timestamp']);
                } else {
                    printMessageToChat(data['data']['user']['id'], 'You', data['data']['user']['avatar'], 'You registered in chat', data['timestamp']);
                }
                if (typeof history != "undefined" && history != null && data['data']['unreadedMessages'] > 0) {
                    $(".user[data-user-id='"  + this.otherUserID + "']").prepend('<div class="person-online-messages"><span>' + data['data']['unreadedMessages'] + '</span></div>');
                } else if (typeof history != "undefined" && history != null && data['data']['unreadedMessages'] == 0) {
                    $(".user[data-user-id='"  + this.otherUserID + "']").remove(".person-online-messages");
                }
/*                $.each($(".chat-message-container.unread-message"), function(i, obj){
                    setTimeout(function() {
                        $(obj).removeClass("unread-message");
                    }, 1000+i*800);
                });   
                setTimeout(function() {
                    $(".person-online-image.user"  + $("#otherUserID").val()).html("");
                }, 1000+data['data']['unreadedMessages']*800); */
                toLastChatMessage();           
                break;
            case 'history':
                loadingSpinner();            
                var history = data['data']['history'];                
                if (typeof history != "undefined" && history != null && history.length > 0) {
                    $(".chat-box-messages-wrapper").html("");
                    for (var counter in history) {                                            
                        printMessageToChat(history[counter]['id'], history[counter]['username'], history[counter]['avatar'], history[counter]['message'], history[counter]['created_at'], history[counter]['readed_at']);
                    }
                } else {                                        
                    $(".chat-box-messages-wrapper").html("");
                    $(".chat-box-messages-wrapper").append('<div>You haven\'t any chat history with this user</div>');                                 
                }
                if (typeof history != "undefined" && history != null && data['data']['unreadedMessages'] > 0) {
                    $(".user[data-user-id='"  + this.otherUserID + "']").prepend('<div class="person-online-messages"><span>' + data['data']['unreadedMessages'] + '</span></div>');
                } else if (typeof history != "undefined" && history != null && data['data']['unreadedMessages'] == 0) {
                    $(".user[data-user-id='"  + this.otherUserID + "']").remove(".person-online-messages");
                }
                if (chatWarnings[this['otherUserID']] != null && chatWarnings[this['otherUserID']].length > 0) {
                    for (var counter in chatWarnings[this['otherUserID']]) {
                        console.log(chatWarnings[this['otherUserID']]);
                        printWarning(chatWarnings[counter][0], chatWarnings[counter][1]);
                        chatID = true;
                        toLastChatMessage();
                    }
                }
/*                var mes =  $(".messages");              
                mes.scrollTop(500);*/   
             
                break;                
            // sending information to client
            case 'message':                
                if (data['data']['user']['id'] == this.otherUserID) {
                    printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['message'], data['data']['created_at'], data['data']['readed_at']);
                } else {
                    printMessageToChat(data['data']['user']['id'], 'You',  data['data']['user']['avatar'], data['data']['message'], data['data']['created_at'], data['data']['readed_at']);
                    $(".user[data-user-id='"  + $('.chat-list-block input.chat-other-user-id').val() + "'] .person-online-messages").remove();
                }
                if (data['data']['unreadedMessages'] > 0) {
                    $(".user[data-user-id='"  + this.otherUserID + "']").append('<div class="person-online-messages"><span>' + data['data']['unreadedMessages'] + '</span></div>');
                    $('.audio_new_message')[0].play();
                } else {
                    $(".user[data-user-id='"  + this.otherUserID + "']").remove(".person-online-messages");
                }
                
                toLastChatMessage();
                break;
            case 'image':                
                if ($('.chat-list-block input.chat-other-user-id').val() == this.otherUserID) {                
                    if (data['data']['user']['id'] == this['otherUserID']) {
                        printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['message'], data['data']['created_at'], data['data']['readed_at']);
                    } else {
                        printMessageToChat(data['data']['user']['id'], 'You', data['data']['user']['avatar'], data['data']['message'], data['data']['created_at'], data['data']['readed_at']);
                    }
                }
                if (data['data']['unreadedMessages'] > 0) {
                    $(".user[data-user-id='"  + this.otherUserID + "']").append('<div class="person-online-messages"><span>' + data['data']['unreadedMessages'] + '</span></div>');
                    $('.audio_new_message')[0].play();
                } else {
                    $(".user[data-user-id='"  + this.otherUserID + "']").remove(".person-online-messages");
                }
                toLastChatMessage();
                chatID = true;
                break;                           
            // some user logout, update user list
/*            case "startVideoCall":
                video_list[data['data']['room']] = videoConnect(data['data']['user']['id'], data['data']['room']);
                break;*/
            case 'logout':
                if ($('.chat-list-block input.current-chat-other-user-id').val() == this.otherUserID) {
                    printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['user']['username']+' logout', data['timestamp']);               
                }
                toLastChatMessage();
                chatID = true;
                break;
            case 'warning':
                if ($('.chat-list-block input.current-chat-other-user-id').val() == this.otherUserID) {
                    printWarning(data['data']['user']['id'], this.otherUserID, data['data']['message']);
                    toLastChatMessage();
                    chatID = true;      
                } else {
                    if (chatWarnings[id] === undefined || chatWarnings[id] == null) {
                        chatWarnings[id] = [];
                    } else {
                        chatWarnings[id].push({'0': data['data']['user']['id'], '1': data['data']['message']});
                    }
                }                            
                break;   
            case 'close':
                printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['user']['username']+' logout', data['timestamp']);                 
                printWarning(data['data']['user']['id'], this.otherUserID, 'Chat was closed. Please refresh page to start chating again.');
                toLastChatMessage();
                chatID = data['chatID'];
                this.close();
        }
    }

    function toLastChatMessage()
    {
        chat_messages_scroll();
        var toBottom = 0;
        $(".chat-message-container").each(function(index, value){              
            toBottom += $(value).height() + 30;
        });
        $('.chat-box-messages-wrapper').animate({
            scrollTop: 2*toBottom
        });
    }

    function chat_messages_scroll() {
        var chat_messages_slim = $('.chat-box-messages-wrapper');
        var chat_content_height = chat_messages_slim.prop('scrollHeight');
        chat_messages_slim.slimscroll({
            size: '10px',
            width: '100%',
            start: 'bottom',
            height: 'calc(100% - 6px)',
            alwaysVisible: true,
            scrollTo: chat_content_height + 'px'
        });
    }

    // send a message
    function onSubmit() {
        var myUserID = $('.tab.user-name.active').data('user-id');
        var otherUserID = $('.chat-list-block input.chat-other-user-id').val();
        var input = editor[0].emojioneArea.getText();
        if (input.trim() == '') {
            return;
        } 
        var message = {
            "type":"message",
            "data": {
                "user": {
                    "id": client_list[myUserID][otherUserID]['otherUserID']
                },
                "asUser": myUserID,
                "message": input,    
                "token": userToken
            }            
        };
        client_list[myUserID][otherUserID].send(JSON.stringify(message));
        setTimeout(function() {
            editor[0].emojioneArea.editor.html('');
        }, 100);        
        $(".chat-message-container.interlocutor-message .message").removeClass("unread-message");
    }

    function printMessageToChat(id, username, avatar, content, time, readed_at) {
        var userAvatar = (avatar != null) ? apiServer + '/' + avatar : '/img/no_avatar_small.jpg';
    	if (username != 'You') {
    		$(".chat-box-messages-wrapper").append('<div class="chat-message-container interlocutor-message">'+
                                                        '<div class="speaker">'+
                                                            '<div class="speaker-avatar" style="background-image: url(' + userAvatar + ')"></div>' +
                                                            '<strong>' + username + '</strong>' +
                                                        '</div>' +
                                                        '<p class="message">'+ messageFilter(content) +'</p><span class="message-time">'+ transformTimestamp(time) +'</span></div>');
            if (readed_at == 0) {
                $(".chat-message-container:last .message").addClass("unread-message");
            }
    	} else {
    		$(".chat-box-messages-wrapper").append('<div class="chat-message-container your-message">'+
                                                        '<div class="speaker">'+
                                                            '<div class="speaker-avatar" style="background-image: url(' + userAvatar + ')"></div>' +
                                                            '<strong>' + username + '</strong>' +
                                                        '</div>' +
                                                        '<p class="message">'+ messageFilter(content) +'</p><span class="message-time">'+ transformTimestamp(time) +'</span></div>');
    	}       
    }

    function transformTimestamp(timestamp)
    {
        try {
            var currentDateTime = new Date(timestamp);
            if (currentDateTime != 'Invalid Date') {
                var currentDate = (new Date(currentDateTime-(new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, 10).replace(/[^0-9]/g, "-");
                var currentTime = (new Date(currentDateTime-(new Date()).getTimezoneOffset() * 60000)).toISOString().slice(11, 19).replace(/[^0-9]/g, ":");
                return currentDate + ' ' + currentTime;
            } else {
                return timestamp;
            }            
        } catch (error) {
            return timestamp;
        }        
    }

    function printError(id, message) {
        $(".chat-box-messages-wrapper").append('<div class="chat-message-container interlocutor-message alert-danger"><div class="speaker"><i class="fa fa-exclamation-circle icon"></i><strong>Error</strong></div><p>'+ message +'</p></div>');
    }

    function printWarning(id, otherUserID, message) {
        if ($('.chat-list-block input.current-chat-other-user-id').val() == otherUserID) {
            $(".chat-box-messages-wrapper").append('<div class="chat-message-container interlocutor-message alert-warning"><div class="speaker"><i class="fa fa-exclamation-triangle icon"></i><strong>Warning</strong></div><p>'+ message +'</p></div>');
        }    
    }

    window.onbeforeunload = function(e) {
        chatID = true;
        videoChatID = true;      
    };
    $(document).ready(function() {         
        $('.print-message-form').on('submit', function(event){
            event.preventDefault();
            onSubmit();
            return false;
        });
        editor = $("#chat-message").emojioneArea({          
            pickerPosition: "top",
            filtersPosition: "top",          
            tonesStyle: "bullet",
            saveEmojisAs: "shortname",
            hidePickerOnBlur: false,
            events: {
                click: function (editor, event) {
                   $("#chat-message").data("emojioneArea").editor.focus();
                },
                // max number of allowed symbols
                keypress: function (editor, event) { 
                    var length = this.getText().length;
                    if (length == 300 && event.keyCode != 8 && event.keyCode != 46) { 
                        event.preventDefault(); 
                    }
                    event = event || window.event;
                    var keyCode = event.keyCode || event.which;            
                    if (keyCode==13) {
                        setTimeout(function() {                    
                            editor.closest('form').submit();
                        }, 50); 
                    }
                } 
            }
        });
    });
    function messageFilter(text)
    {
        if (typeof emojione == 'undefined') {
          document.write('<script type="text/javascript" src="/js/node_modules/emojione/lib/js/emojione.min.js"><\/script>');        
        }
        emojione.ascii = true;
        return emojione.shortnameToImage(unescape(text));      
    }
    $("#attach-image").change(function (e) {
        if (this.disabled) return alert('File upload not supported!');
        var F = this.files;
        if (F) {
            FilePreUpload(F);
        }
    });
    function FilePreUpload(files) {
        var fd = new FormData();    
        var otherUserID = $('.chat-list-block input.chat-other-user-id').val();  
        fd.append('imageFile', files[0]);        
        fd.append('otherUserID', otherUserID);        
        fd.append('asUser', $('.tab.user-name.active').data('user-id'));        
        uploadImage(fd);
    }
    function uploadImage(formData)
    {
        var uploadURL = apiServer + "/v1/photo/upload-image-from-chat"; //Upload URL
        var extraData = {}; //Extra Data.
        var jqXHR=$.ajax({
            xhr: function() {
                var xhrobj = $.ajaxSettings.xhr();
                if (xhrobj.upload) {
                    xhrobj.upload.addEventListener('progress', function(event) {
                        var percent = 0;
                        var position = event.loaded || event.position;
                        var total = event.total;
                        if (event.lengthComputable) {
                            percent = Math.ceil(position / total * 100);
                        }
                    }, false);
                }
                return xhrobj;
            },
            url: uploadURL,
            type: "POST",
            contentType: false,
            processData: false,
            data: formData,
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + userToken
            },            
            success: function(data) {
                var myUserID = $('.tab.user-name.active').data('user-id');
                var otherUserID = $('.chat-list-block input.chat-other-user-id').val();
                console.log(data);
                if (data['success']){
                    var message = {
                        "type":"image",
                        "data": {
                            "user": {
                                "id": client_list[myUserID][otherUserID]['otherUserID']
                            },
                            "asUser": myUserID,
                            "image": data['medium_thumb'],    
                            "token": userToken
                        }            
                    };
                    console.log(JSON.stringify(message));
                    client_list[myUserID][otherUserID].send(JSON.stringify(message));
                } else {
                    window["commonAdminLibrary"]['show_modal_message'](data['success'], data['message'], data['code']);
                }
            }
        });
    }

    $('body').on('click', '.new-chats .user', function(){        
        $('.chat-list-block input.chat-other-user-id').val($(this).attr('data-user-id'));
        $('.chat-list-block input.chat-other-user-status').val($(this).find('.online-status').data('status'));
        $('#load-chat').click();
    });

    $('#load-chat').on('click', function(event){        
        event.preventDefault();
        loadingSpinner();
        var currentGirl = $('.tab.user-name.active').data('user-id');
        var otherUserID = $('.chat-list-block input.chat-other-user-id').val();
        var otherUserStatus = $('.chat-list-block input.chat-other-user-status').val();
        var otherUserAvatar = $('.men-online .user-tile.active .avatar').attr('data-src');
        if (otherUserID == '') {
            window["commonAdminLibrary"]['show_modal_message'](false, 'please select user with who you want to chat', 2);
            $(".chat-box-messages-wrapper").html('');
            return false;
        }
/*        if (otherUserStatus == 0) {
            window["commonAdminLibrary"]['show_modal_message'](false, 'please select online user', 2);
            $(".chat-box-messages-wrapper").html('');
            return false;
        }*/
        $('.chat-list-block input.current-chat-other-user-id').val(otherUserID);

        if (typeof ($('.men-online .user-tile.active .avatar').attr('data-src')) != 'undefined') {
            $('.chat-list-block input.current-chat-other-user-avatar').val(otherUserAvatar);
        }

        if ($('.new-chat-section[data-user-id="'+ currentGirl +'"] .user[data-user-id="'+ otherUserID +'"]').length == 0) {
            $('.new-chat-section[data-user-id="'+ currentGirl +'"]')
            .append(
                $('<div>').addClass('user col-md-2').attr('data-user-id', otherUserID)
                        .append($('<div>').addClass('avatar').css('backgroundImage', 'url(' + otherUserAvatar + ')'))
                        .append($('<div>').addClass('online-status').data('status', otherUserStatus).append($('<span>').addClass((!!+otherUserStatus) ? 'green' : 'red').html((!!+otherUserStatus) ? 'online' : 'offline')))
                        .append($('<div>').addClass('name').html($('.user-tile[data-user-id="'+ otherUserID +'"] .name').html()))
                        .append($('<div>').addClass('user-id').html("[ID: "+ otherUserID +"]"))
            );
        }
        
        $('.chat-letters-content .chat-wrapper').show();
        if (client_list[currentGirl] === undefined || client_list[currentGirl] == null) {
            client_list[currentGirl] = {};
        }
        if (client_list[currentGirl][otherUserID] === undefined || client_list[currentGirl][otherUserID] == null) {             
            client_list[currentGirl][otherUserID] = createChat(currentGirl, otherUserID);
            chatConnectionTries[otherUserID] = 0;                              
        } else {                       
            var message = {
                "type":"loadhistory",
                "data": {
                    "user": {
                        "id": otherUserID
                    },                  
                    'asUser': currentGirl,
                    "token": userToken
                }            
            };                                    
            client_list[currentGirl][otherUserID].send(JSON.stringify(message));
        }                 
        
    });
    
    //setTimeout(function(){ toLastChatMessage(); }, 1000);
})(jQuery);