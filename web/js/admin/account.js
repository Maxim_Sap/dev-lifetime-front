window["adminAccountLibrary"] = (function($) {
'use strict';

var userID = window["commonAdminLibrary"]["userID"];
var userToken = window["commonAdminLibrary"]["userToken"];
var apiServer = window["commonAdminLibrary"]["apiServer"];
var contentType = window["commonAdminLibrary"]["contentType"];
var USER_MALE = window["commonAdminLibrary"]["USER_MALE"];
var USER_FEMALE = window["commonAdminLibrary"]["USER_FEMALE"];
var USER_AGENCY = 3;
var USER_INTERPRETER = 4;
var USER_ADMIN = 7;
var previousAdminLetterCount = 0;

if (window['commonAdminLibrary']['storageAvailable']('localStorage')) {
	localStorage.setItem('previousAdminLetterCount', 0);
} 

$('form.edit-profile').submit(function() {

	var otherUserID = $('#otherUserID').val();
	var firstName = $('form.edit-profile input[name="first_name"]').val();
	var lastName = $('form.edit-profile input[name="last_name"]').val();
	var email = $('form.edit-profile input[name="email"]').val();
	var sex = $('form.edit-profile [name="sex"]').val();
	var region = $('form.edit-profile input[name="region"]').val();
	var birthday = $('form.edit-profile input[name="birthday"]').val();
	var passportNumber = $('form.edit-profile input[name="passport_number"]').val();
	var weight = $('form.edit-profile input[name="weight"]').val();
	var height = $('form.edit-profile input[name="height"]').val();
	var occupation = $('form.edit-profile input[name="occupation"]').val();
	var kids = $('form.edit-profile input[name="kids"]').val();
	var phone = $('form.edit-profile input[name="phone"]').val();
	var m_phone = $('form.edit-profile input[name="m_phone"]').val();
	var passport = $('form.edit-profile input[name="passport"]').val();
	var hair = $('form.edit-profile select[name="hair"]').val();
	var eyes = $('form.edit-profile select[name="eyes"]').val();
	var physique = $('form.edit-profile select[name="physique"]').val();
	var ethnos = $('form.edit-profile select[name="ethnos"]').val();
	var country = $('form.edit-profile select[name="country"] :selected').val();
	var education = $('form.edit-profile select[name="education"] :selected').val();
	var religion = $('form.edit-profile select[name="religion"] :selected').val();
	var marital = $('form.edit-profile select[name="marital"] :selected').val();
	var smoking = $('form.edit-profile select[name="smoking"] :selected').val();
	var alcohol = $('form.edit-profile select[name="alcohol"] :selected').val();
	var lookingAgeFrom = $('form.edit-profile select[name="looking_age_from"] :selected').val();
	var lookingAgeTo = $('form.edit-profile select[name="looking_age_to"] :selected').val();
	var aboutMe = $('form.edit-profile textarea[name="about_me"]').val();
	var hobbies = $('form.edit-profile textarea[name="hobbies"]').val();
	var myIdeal = $('form.edit-profile textarea[name="my_ideal"]').val();	
	var englishLevel = $('form.edit-profile select[name="english_level"]').val();
	var address = $('form.edit-profile input[name="address"]').val();
	var otherLanguage = $('form.edit-profile input[name="other_language"]').val();
	//for agency
	var agencyName = $('form.edit-profile input[name="agency_name"]').val();
	var contactPerson = $('form.edit-profile input[name="contact_person"]').val();
	var directorAddress = $('form.edit-profile input[name="director_address"]').val();
	var officeAddress = $('form.edit-profile input[name="office_address"]').val();
	var officePhone = $('form.edit-profile input[name="office_phone"]').val();
	var skype = $('form.edit-profile input[name="skype"]').val();
	var bank_detail = $('form.edit-profile textarea[name="bank_detail"]').val();
	//var cardNumber = $('form.edit-profile input[name="card_number"]').val();
	//var cardUserName = $('form.edit-profile input[name="card_user_name"]').val();
	//var cardDateEnd = $('form.edit-profile input[name="card_date_end"]').val();
	var agencyApproveStatus = $('form.edit-profile select[name="agency_approve"]').val();

	var userActiveStatus = $('form.edit-profile [name="user_active_status"]').val();
	var approveStatus = $('form.edit-profile select[name="approve_status"]').val();
	//for interpreter
	var girlsIDs = [];
	if($('.girls-list').length > 0){
		$('.girls-list input:checked').each(function(){
			girlsIDs.push($(this).val());
		});
	}
	// for admin
	var permissions = {};
    var checkBoxes = $('.edit-profile input[id^="permission"]');
    for (var i = 0; i <= checkBoxes.length-1; i++) {
    	permissions[$(checkBoxes[i]).attr("name")] = ($(checkBoxes[i]).prop("checked")) ? 1 : 0;    	
    }

	var data = {
			'skin_id': 1,			
			'token': (userToken != null) ? userToken : null,
			'otherUserID': (otherUserID != null) ? otherUserID : null,
			'first_name': (firstName != null) ? firstName : null,
			'last_name': (lastName != null) ? lastName : null,
			'phone': (phone != null) ? phone : null,
			'm_phone': (m_phone != null) ? m_phone : null,
			'sex': (sex != null) ? sex : null,
			'passport_number': (passportNumber != null) ? passportNumber : null,
			'birthday': (birthday != null) ? birthday : null,
			'kids': (kids != null) ? kids : null,
			'height': (height != null) ? height : null,
			'weight': (weight != null) ? weight : null,
			'occupation': (occupation != null) ? occupation : null,
			'looking_age_from': (lookingAgeFrom != null) ? lookingAgeFrom : null,
			'looking_age_to': (lookingAgeTo != null) ? lookingAgeTo : null,
			'about_me': (aboutMe != null) ? aboutMe : null,
			'hobbies': (hobbies != null) ? hobbies : null,
			'my_ideal': (myIdeal != null) ? myIdeal : null,
			'ethnos': (ethnos != null) ? ethnos : null,
			'religion': (religion != null) ? religion : null,
			'marital': (marital != null) ? marital : null,
			'eyes': (eyes != null) ? eyes : null, 
			'physique': (physique != null) ? physique : null, 
			'hair': (hair != null) ? hair : null,
			'smoking': (smoking != null) ? smoking : null,
			'alcohol': (alcohol != null) ? alcohol : null,
			'education': (education != null) ? education : null,
			'email': (email != null) ? email : null,
			'city': (region != null) ? region : null,
			'country': (country != null) ? country : null,
			'agency_name' : (agencyName != null) ? agencyName : null,
			'contact_person': (contactPerson != null) ? contactPerson : null,
			'office_address': (officeAddress != null) ? officeAddress : null,
			'office_phone': (officePhone != null) ? officePhone : null,
			'director_address': (directorAddress != null) ? directorAddress : null,
			'skype': (skype != null) ? skype : null,
			'bank_detail': (bank_detail != null) ? bank_detail : null,
			//'card_number': (cardNumber != null) ? cardNumber : null,
			//'card_user_name': (cardUserName != null) ? cardUserName : null,
			//'card_date_end': (cardDateEnd != null) ? cardDateEnd : null,
			'agency_approve_status': (agencyApproveStatus != null) ? agencyApproveStatus : null,
			'approve_status': (approveStatus != null) ? approveStatus : null,
			'user_active_status': (userActiveStatus != null) ? userActiveStatus : null,			
			'english_level': (englishLevel != null) ? englishLevel : null,
			'address': (address != null) ? address : null,
			'other_language': (otherLanguage != null) ? otherLanguage : null,
			'passport': (passport != null) ? passport : null,
			'girls_ids': (girlsIDs != null) ? girlsIDs : null,
			'permissions': JSON.stringify(permissions)
		};
	
	$.ajax({
		url: apiServer+'/user/edit-info',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: data,
		success: function (response) {
			
			if (response["code"] == 1) {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			} else if (response["code"] == "Validation errors") {
				var message = "";
            	for (var errorType in response.errors) {
                    if (response.errors.hasOwnProperty(errorType)) {
                        message += response.errors[errorType];		                            
                    }
                }
            	$('#modalMessage, #layout').show();
            	$('#modalMessage .modal-body span').text(message);
			} else if (response["success"] == true) {
				location.reload(true);
			}		
		},
	});           
	return false;
});

//edit pass
$('form.edit-password').submit(function() {
	var oldPassword = $('form.edit-password input[name="oldPassword"]').val();
	var newPassword = $('form.edit-password input[name="newPassword"]').val();
	var confirmPassword = $('form.edit-password input[name="confirmPassword"]').val();
	if (newPassword != confirmPassword) {
		$('#modalMessage,#layout').show();
		$('#modalMessage .modal-body span').text('Password and confirm password are different');
		return false;
	}
	var otherUserID = $('form.edit-password input[name="otherUserID"]').val();
	$.ajax({
		url: apiServer+'/user/change-password',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {
			'skin_id': 1,						
			'oldPassword': oldPassword,
			'newPassword': newPassword,
			'confirmPassword': confirmPassword,
			'otherUserID': otherUserID
		},
		success: function (response) {
			if (response['success']) {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response['message']);
				$('form.edit-password input[type=text], form.edit-password input[type=password]').val('');
			} else {
				if (response['code'] == 1) {
					$('#modalMessage, #layout').show();
	                $('#modalMessage .modal-body span').text(response.message);
				} else if (response['code'] == "Validation errors") {
					var message = "";
	            	for (var errorType in response.errors) {
                        if (response.errors.hasOwnProperty(errorType)) {
                            message += response.errors[errorType];		                            
                        }
                    }
	            	$('#modalMessage, #layout').show();
	            	$('#modalMessage .modal-body span').text(message);
				}
			}
			
		},
	});           
	return false;
});

$('#change_avatar').click(function() {
	$('#avatar_file').click();
});
$("#avatar_file").change(function (e) {
    if (this.disabled) return alert('File upload not supported!');
    var F = this.files;
    if (F) {
        FilePreUpload(F);
    }
});

$("[id^='change_poster']").click(function() {
	$(this).parent().find("[id^='poster_file']").click();
});
$("[id^='poster_file']").change(function (e) {
	var videoID = $(this).data('video-id');
    if (this.disabled) return alert('File upload not supported!');
    var F = this.files;
    if (F) {
        PosterPreUpload(F, videoID);
    }
});

function PosterPreUpload(files, videoID) {
    var fd = new FormData();    
    var otherUserID = $('#otherUserID').val();    
    fd.append('imageFile', files[0]);
    fd.append('userID', userID);
    fd.append('otherUserID', otherUserID);
    fd.append('token', userToken);
    fd.append('videoID', videoID);
    changePoster(fd);
}
function changePoster(formData, videoID) {
	var uploadURL = apiServer + "/user/upload-and-set-poster"; //Upload URL
    var extraData = {}; //Extra Data.
    var jqXHR=$.ajax({
        xhr: function() {
            var xhrobj = $.ajaxSettings.xhr();
            if (xhrobj.upload) {
                xhrobj.upload.addEventListener('progress', function(event) {
                    var percent = 0;
                    var position = event.loaded || event.position;
                    var total = event.total;
                    if (event.lengthComputable) {
                        percent = Math.ceil(position / total * 100);
                    }
                }, false);
            }
            return xhrobj;
        },
        url: uploadURL,
        type: "POST",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        contentType: false,
        processData: false,
        data: formData,
        dataType: "json",
        success: function(data) {
            if (data['success']){
                $('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(data['message']);
                location.reload();
            } else {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(data['message']);
            }
        }
    });
}

	$("#add_post_image").click(function() {
		$('#post_image_change').click();
	});
	$("#post_image_change").change(function (e) {
		var postID = $(this).attr('data-post-id');
		var F = this.files;
		if (F) {
			BlogImagePreUpload(F, postID);
		}
	});

	function BlogImagePreUpload(files, postID) {
		var fd = new FormData();
		fd.append('imageFile', files[0]);
		fd.append('userID', userID);
		fd.append('token', userToken);
		fd.append('postID', postID);
		changeBlogImage(fd,postID);
	}

	function changeBlogImage(formData, postID) {
		var uploadURL = apiServer + "/admin/upload-post-image"; //Upload URL
		var extraData = {}; //Extra Data.
		var jqXHR=$.ajax({
			xhr: function() {
				var xhrobj = $.ajaxSettings.xhr();
				if (xhrobj.upload) {
					xhrobj.upload.addEventListener('progress', function(event) {
						var percent = 0;
						var position = event.loaded || event.position;
						var total = event.total;
						if (event.lengthComputable) {
							percent = Math.ceil(position / total * 100);
						}
					}, false);
				}
				return xhrobj;
			},
			url: uploadURL,
			type: "POST",
			headers: {
				"Authorization": "Bearer " + userToken
			},
			contentType: false,
			processData: false,
			data: formData,
			dataType: "json",
			success: function(data) {
				if (!data['success']){
					window["commonAdminLibrary"]['show_modal_message'](data['success'], data['message'], data['code']);
				}else{
					$('.post-content .prev-img').css('background-image','url('+apiServer+"/"+data['thumb_normal']);
					$('#post-image').val(data['thumb_normal']);
				}
			}
		});
	}

function FilePreUpload(files) {
    var fd = new FormData();    
    var otherUserID = $('#otherUserID').val();	
    fd.append('imageFile', files[0]);
    fd.append('userID', userID);
    fd.append('otherUserID', otherUserID);
    fd.append('token', userToken);
    changeAvatar(fd);
}
function changeAvatar(formData) {
	var uploadURL = apiServer + "/user/upload-and-set-avatar"; //Upload URL
    var extraData = {}; //Extra Data.
    var jqXHR=$.ajax({
        xhr: function() {
            var xhrobj = $.ajaxSettings.xhr();
            if (xhrobj.upload) {
                xhrobj.upload.addEventListener('progress', function(event) {
                    var percent = 0;
                    var position = event.loaded || event.position;
                    var total = event.total;
                    if (event.lengthComputable) {
                        percent = Math.ceil(position / total * 100);
                    }
                }, false);
            }
            return xhrobj;
        },
        url: uploadURL,
        type: "POST",
        contentType: false,
        processData: false,
		headers: {
			"Authorization": "Bearer " + userToken
		},
        data: formData,
        dataType: "json",
        success: function(data) {
            if (data['success']){
                $('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(data['message']);
                $('.edit_profile_avatar img').attr('src', apiServer + '/' + data['avatar']['medium_thumb']);
                $('.edit_profile_avatar img').attr('data-original-src', apiServer + '/' + data['avatar']['original_image']);
                $('.login-block .thumbnail, .greeting-avatar').attr('style', 'background-image: url(' + apiServer + '/' + data.avatar.small_thumb+');');
                location.reload(true);
            } else {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(data['message']);
            }
        }
    });
}

$('.admin-register-form select[name="user_type"]').change(function () {
	$('#fake_man').attr('checked', false);
	var agencyOwnerArray = [USER_FEMALE, USER_INTERPRETER, USER_ADMIN];
	if (agencyOwnerArray.indexOf(parseInt($(this).val())) >= 0) {
		$('#agency-list-select').show();
	} else {
		$('#agency-list-select').hide();
	}
	if ($(this).val() == USER_MALE) {
		$('#fake_man_cover').show();
	} else {
		$('#fake_man_cover').hide();
	}
	if ($(this).val() == USER_ADMIN) {
		$('.admin-register-form .admin-permissions').removeClass('hidden');
		var permissions = $('.admin-register-form .siteadmin-permissions input[id^="permission"]');
		for (var i = 0; i <= permissions.length-1; i++) {
			permissions[i].id = "p-" + permissions[i].id;
		}
	} else {
		$('.admin-register-form .admin-permissions').addClass('hidden');
		var permissions = $('.admin-register-form .siteadmin-permissions input[id^="p-permission"]');
		for (var i = 0; i <= permissions.length-1; i++) {
			permissions[i].id = permissions[i].id.replace("p-", "");
		}
	}
	if ($(this).val() == 8) {
		$('.admin-register-form .siteadmin-permissions').removeClass('hidden');
		var permissions = $('.admin-register-form .admin-permissions input[id^="permission"]');
		for (var i = 0; i <= permissions.length-1; i++) {
			permissions[i].id = "p-" + permissions[i].id;
		}
	} else {
		$('.admin-register-form .siteadmin-permissions').addClass('hidden');
		var permissions = $('.admin-register-form .admin-permissions input[id^="p-permission"]');
		for (var i = 0; i <= permissions.length-1; i++) {
			permissions[i].id = permissions[i].id.replace("p-", "");
		}		
	}
});

$('#fake_man').on('click', function() {
	if ($(this).prop('checked')) {
		$('#agency-list-select').show();
	} else {
		$('#agency-list-select').hide();
	}
});

$('.admin-register-form').submit(function () {
    var firstName = $('.admin-register-form input[name="first_name"]').val();
    var userType = $('.admin-register-form [name="user_type"]').val();
    var email = $('.admin-register-form input[name="email"]').val();
    var password = $('.admin-register-form input[name="password"]').val();
    var confirmPassword = $('.admin-register-form input[name="conf_password"]').val();
    var parentUserID = $('.admin-register-form [name="parentUserID"]').val();
    var fakeMan = $('#fake_man').prop('checked');
	fakeMan = (fakeMan) ? 1 : 0;
	if (userType == USER_MALE && !fakeMan) {
		parentUserID = 1;
	}
    if (password != confirmPassword) {
        $('#modalMessage, #layout').show();
        $('#modalMessage .modal-body span').text("Passwords do not match");
        return false;
    }
    
    var permissions = {};
    var checkBoxes = $('.admin-register-form input[id^="permission"]');
    for (var i = 0; i <= checkBoxes.length-1; i++) {
    	permissions[$(checkBoxes[i]).attr("name")] = ($(checkBoxes[i]).prop("checked")) ? 1 : 0;    	
    }

    //check email
	$.ajax({
        url: apiServer+'/reg-email/check-email',
        dataType: "json",
        contentType: contentType,
        type: "POST",
        data: {'email':email},
        success: function (response) {        	
            if (response.success == true) {
				// user register
				var body = {
		            'firstName': firstName,
		            'email': email,
		            'userType': userType, //girl
		            'parentUserID': parentUserID,
		            'fakeMan': fakeMan,
		            'token': userToken,
		            'password': password,
		            'permissions': JSON.stringify(permissions),
		            'skinID': 1
		        };
				$.ajax({
			        url: apiServer+'/admin/register-new-user',
			        dataType: "json",
			        contentType:contentType,
			        type: "POST",
	                headers: {
			            "Authorization": "Bearer " + userToken
			        },
			        data: body,
			        success: function (response) {			        	
			            if (response['success'] == true) {			            	
			                location['href'] = '/manager/edit-user/' + response['userID'];
			            } else if (response['code'] == 1) {
			                $('#modalMessage, #layout').show();
			                $('#modalMessage .modal-body span').text(response.message);
			            } else if (response['code'] == "Validation errors") {
			            	var message = "";
			            	for (var errorType in response.errors) {
		                        if (response.errors.hasOwnProperty(errorType)) {
		                            message += response.errors[errorType];		                            
		                        }
		                    }
			            	$('#modalMessage, #layout').show();
			            	$('#modalMessage .modal-body span').text(message);
			            }
			        },
			    });
            } else {
				$('#modalMessage,#layout').show();
			    $('#modalMessage .modal-body span').text(response.message);
            }
		}
	});
    return false;
});

$('.add-new-album').submit(function() {
	var title = $('.add-new-album [name="title"]').val();
	var otherUserID = $('#otherUserID').val();
	$.ajax({
		url: apiServer+'/user/create-album',
		type: "POST",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		dataType: "json",
		data: {
			'title': title,
			'otherUserID': otherUserID
		},
		success: function (response) {
			if (response['success']) {
				location.reload();
			} else {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response['message']);
			}
		},
	});
	return false;
});

$('.album-edit').submit(function() {
	var title = $('#album_title').val();
	var albumID = $('#album_id').val();
	var description = $('#album-description').val();
	var publicStatus = $('#album_status').prop('checked');
	var activeStatus = $('#album_active').prop('checked');
	var otherUserID = $('#otherUserID').val();
	
	$.ajax({
		url: apiServer+'/v1/album/edit-album',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {						
			'otherUserID': otherUserID,
			'albumID': albumID,
			'title': title,
			'description': description,
			'public': (publicStatus) ? 1 : 0,
			'active': (activeStatus) ? 1 : 0
		},
		success: function (response) {
			$('#modalMessage,#layout').show();
			$('#modalMessage .modal-body span').text(response.message);
		},
	});
	return false;
});

//setLastActivity every 15 second
var timerID = setInterval(function() { 
					setLastActivity(); 					
            	}, 15000);

function setLastActivity() {
  
    $.ajax({
        url: apiServer+'/v1/user/set-last-activity',
        dataType: "json",
        type: "POST",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            
        },
        success: function (response) {
            if (response.success == true) {
                //
            } else {
                if(response.success == false && response.code == 2){
                    clearInterval(timerId);
                    $('#modalMessage').modal('show');
                    $('#modalMessage .modal-body span').text(response.message);
                }
            }
        },        
        error: function (xhr, ajaxOptions, thrownError) {        
        }
    });           
}

$('.photo-title-form').submit(function() {
	var otherUserID = $('#otherUserID').val();
	var photoID = $('#photo-id').val();
	var title = $('#photo-title').val();
	var premium = $('#premium').prop('checked');
	premium = premium ? 1 : 0;
	var approveStatus = $('#approved').val();
	var userDeleteStatus = $('#user_deleted').prop('checked');	
	userDeleteStatus = userDeleteStatus ? 0 : 1;
	$.ajax({
		url: apiServer+'/v1/photo/update-photo-info',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {
			'otherUserID': otherUserID,
			'photoID': photoID,
			'title': title,
			'premium': premium,
			'approveStatus': approveStatus,
			'userDeleteStatus': userDeleteStatus
		},
		success: function (response) {
			$('#modalMessage,#layout').show();
			$('#modalMessage .modal-body span').text(response['message']);
		},
	});           
	return false;
});
$('#set-album-cover').click(function(){
	var photoID = $(this).attr('data-id');
	var albumID = $(this).attr('data-album-title');
	var otherUserID = $(this).data('other-user-id');
	$.ajax({
		url: apiServer+'/v1/album/set-album-cover',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {
			'otherUserID': otherUserID,
			'photoID': photoID,
			'albumID': albumID
		},
		success: function (response) {
			$('#modalMessage,#layout').show();
			$('#modalMessage .modal-body span').text(response['message']);
		},
	});           
	return false;
});

var mainNav = $('.admin-left-menu > ul > li > a');
$('.admin-left-menu > ul > li > ul').hide();

mainNav.click(function(){
    if ($(this).siblings('ul').length > 0) {
		$(this).siblings('ul').slideToggle(1000, 'swing');
    	return false;
    }
});

$(document).ready(function() {
	/*if ($('.admin-register-form select[name="user_type"]').length > 0) {
		if ($('.admin-register-form select[name="user_type"]').val() == USER_AGENCY) {
			//$('.admin-register-form select[name="parentUserID"]').hide();
			$('#agency-list-select>label').hide();
		}
	}*/
	$('select#type').on('change', function() {
		var agencyID = $('#agency_id').val();
		var userType = $('#type').val();

		// get list of users
		$.ajax({
			url: apiServer+'/v1/admin/get-users-info-from-agency',
			type: "POST",
	        headers: {
	            "Authorization": "Bearer " + userToken
	        },			
			dataType: "json",
			data: {
				'otherUserID': agencyID,
				'type': (userType != "") ? userType : null
			},
			success: function (response) {
				if(response.success && response.usersArray != null ){
					var otherUserID = $('#otherUserID').val();
					var selectHtml = '<option value="' + userID + '">---любой---</option>';
					for (var i=0; i < response.usersArray.length; i++) {
						var select = '';
						if (response.usersArray[i]['id'] == otherUserID) {
							select = 'selected';
						}
						var name = response.usersArray[i]['first_name'];
						if (response.usersArray[i]['last_name'] != null) {
							name += ' ' + response.usersArray[i]['last_name'];
						}
						selectHtml +='<option value="'+response.usersArray[i]['id']+'" '+select+'>'+name+'</option>';
					}
					$('select.my-users').html(selectHtml);
				}
			},
		});
	});


	if ($('select.list-of-affiliates').length > 0) {
		var place = 'select.list-of-affiliates';
		get_affiliates_list(place);
	}
    setTimeout(function () {
        getNeedToApproveCount();
    }, 1500);
});

$('select.my-users').change(function() {
	$('#other_userID').val($(this).val());
});

	function get_affiliates_list(place) {
		var selected_user_id = $(place).attr('data-active-user-id');
		$.ajax({
			url: apiServer+'/admin/get-affiliates',
			type: "POST",
			headers: {
				"Authorization": "Bearer " + userToken
			},
			dataType: "json",
			data: {
				'user_type': 5
			},

			success: function (ans) {
				if (ans.success && ans.count > 0) {
					var html = '<option value="">---все---</option>';
					var i =0;
					for (i in ans.affiliatesArray) {
						var selected_attr = "";
						if(selected_user_id == ans.affiliatesArray[i]["user_id"]){
							var selected_attr = "selected";
						}
						html += '<option value="' + ans.affiliatesArray[i]["user_id"] + '" '+selected_attr+'>' + ans.affiliatesArray[i]["name"] + '</option>';
					}
					$(place).html(html);
				}
			},
		});
	}
// user activity
$('.user-activity-form').submit(function() {
	getActivityList(1);
	return false;
});
function getActivityList(page) {
	var searchUserType = $('#search_user_type').val();
	if (searchUserType == null || typeof searchUserType == "undefined") {
		searchUserType = null;
	}
	var otherUserID = $('#other_userID').val();
	if(otherUserID == '') {
		$('#modalMessage, #layout').show();
		$('#modalMessage .modal-body span').text('Please select user');
		return false;
	}
	var agencyID = $('#agency-select').val();
	if (agencyID == null || typeof agencyID == "undefined") {
		agencyID = $('#agency_id').val();
	}
	var dateFrom = $('.user-activity-form input[name="date-from"]').val();
	var dateTo = $('.user-activity-form input[name="date-to"]').val();
	var name = $('.user-activity-form input[name="name"]').val();
	var limit = $('#limit').val();
	if (page == null || page < 0) {
		page = 1;
	}
	var offset = (page - 1) * limit;
	var searchParams = {
		'agencyID': agencyID,
		'dateFrom': dateFrom,
		'dateTo': dateTo,
		'userType': searchUserType,
		'name': name
	};
	$.ajax({
		url: apiServer+'/admin/get-user-activity',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },		
		data: {
			'limit': limit,
			'otherUserID': otherUserID,
			'offset': offset,
			'search_params': searchParams
		},
		success: function (response) {
			if (!response.success) {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			} else {
				if (response.usersActivityInfo != null && response.usersActivityInfo.length > 0) {
					if (typeof(otherUserID) == 'undefined') {
						otherUserID = false;
					}
					generateActivityList(response.usersActivityInfo, response.count, page, otherUserID);
				} else {
					$('table.activity-table tbody').html('');
					$('.admin-activity-pagination').html('');
				}
			}
		},
	});           
}

function getNeedToApproveCount() {
    $.ajax({
        url: apiServer + '/v1/admin/count-need-to-approve',
        type: "GET",
        dataType: "json",
        data: {},
        headers: {
        	"Authorization": "Bearer " + userToken
        },
        success: function (response) {
            if (response['success']) {                
                $('span.counter.orange').text(response['count']);                
            }
        },
    });
}

function generateActivityList(items, count, page, otherUserID) {
	$.ajax({
		url: '/manager/generate-activity-list',
		type: "POST",
		dataType: "json",
		data: {
			'items': items,
			'count': count,
			'page': page,
			'otherUserID': otherUserID
		},
		success: function (response) {
			$('table.activity-table tbody').html(response.items);
			$('.admin-activity-pagination').html(response.pagination);
		}
	});
}

function activityPagination(page){
	getActivityList(page);
	return false;
}

$(document).on('click','.controls .user-activate', function() {	
	var otherUserID = $(this).data('user-id');
	if ($(this).find('i').hasClass('fa-toggle-on')) {
		$('#modalMessage, #layout').show();
		$('#modalMessage').css({"width": "400px", "height": "290px"});
		$('#modalMessage .modal-body').html('');
		$('#modalMessage .modal-body').append('<span>');
		$('#modalMessage .modal-body span').text('Please enter deactivation reason');
		$('#modalMessage .modal-body').append('<textarea class="deactivation-text" placeholder="Enter deactivation reason"></textarea>');
		$('#modalMessage .modal-body').append('<button id="deactivation-reason" class="btn">Deactivate</button>');
		$('#modalMessage .modal-body button').data('user-id', otherUserID);
	} else {
		$('#modalMessage, #layout').show();
		$('#modalMessage').css({"width": "299px", "height": "150px"});
		$('#modalMessage .modal-body').html('');
		$('#modalMessage .modal-body').append('<span>');
		$('#modalMessage .modal-body span').text('Are you really want to activate this account?');		
		$('#modalMessage .modal-body').append('<br><button id="activation-reason-yes" class="btn">Yes</button> &nbsp; ');
		$('#modalMessage .modal-body').append('<button id="activation-reason-no" class="btn">No</button>');
		$('#modalMessage .modal-body button').data('user-id', otherUserID);		
	}	
	return false;
});

$(document).on('click','#deactivation-reason', function() {
	var otherUserID = $(this).data('user-id');
	var deactivationText = $(this).parent().find(".deactivation-text").val();
	$('#modalMessage .modal-body .error-message-text').remove();
	if (deactivationText == '') {		
		$('#modalMessage .modal-body').prepend('<div class="error-message-text" style="color: red;">Enter deactivation text</div>');
	}
	$.ajax({
		url: apiServer+'/admin/set-user-activity',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {
			'userID': userID,
			'token': userToken,
			'otherUserID': otherUserID,
			'deactivationText': deactivationText
		},
		success: function (response) {
			if (!response.success) {
				$('#modalMessage, #layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			} else {
				if (response.success && response.status == 1) {
					$('.controls .user-activate[data-user-id="'+ otherUserID +'"] i').removeClass('fa-toggle-off').addClass('fa-toggle-on');
					$('.controls .user-activate[data-user-id="'+ otherUserID +'"]').attr('title','deactivate');
				} else {
					$('.controls .user-activate[data-user-id="'+ otherUserID +'"] i').removeClass('fa-toggle-on').addClass('fa-toggle-off');
					$('.controls .user-activate[data-user-id="'+ otherUserID +'"]').attr('title','activate');
				}
				if ($('.edit-profile').length > 0) {
					location.reload();
				}
			}	
		}
	});
	$('#modalMessage, #layout').hide();
	$('#modalMessage').css({"width": "299px", "height": "150px"});
	$('#modalMessage .modal-body').html('');
	$('#modalMessage .modal-body').append('<span>');
	return false;
});
$(document).on('click','#activation-reason-yes', function() {
	var otherUserID = $(this).data('user-id');
	$.ajax({
		url: apiServer+'/admin/set-user-activity',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {
			'userID': userID,
			'token': userToken,
			'otherUserID': otherUserID
		},
		success: function (response) {
			if (!response.success) {
				$('#modalMessage, #layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			} else {
				if (response.success && response.status == 1) {
					$('.controls .user-activate[data-user-id="'+ otherUserID +'"] i').removeClass('fa-toggle-off').addClass('fa-toggle-on');
					$('.controls .user-activate[data-user-id="'+ otherUserID +'"]').attr('title','deactivate');
				} else {
					$('.controls .user-activate[data-user-id="'+ otherUserID +'"] i').removeClass('fa-toggle-on').addClass('fa-toggle-off');
					$('.controls .user-activate[data-user-id="'+ otherUserID +'"]').attr('title','activate');
				}
				if ($('.edit-profile').length > 0) {
					location.reload();
				}
			}	
		}
	});
	$('#modalMessage, #layout').hide();
	$('#modalMessage').css({"width": "299px", "height": "150px"});
	$('#modalMessage .modal-body').html('');
	$('#modalMessage .modal-body').append('<span>');
	return false;	
});
$(document).on('click','#activation-reason-no', function() {
	$('#modalMessage, #layout').hide();
	return false;
});

$('.edit-profile .girl[name="user_active_status"]').on('change', function() {
	var otherUserID = $("#otherUserID").val();
	if ($(this).val() == 3) {
		$('#modalMessage, #layout').show();
		$('#modalMessage').css({"width": "400px", "height": "290px"});
		$('#modalMessage .modal-body').html('');
		$('#modalMessage .modal-body').append('<span>');
		$('#modalMessage .modal-body span').text('Please enter deactivation reason');
		$('#modalMessage .modal-body').append('<textarea class="deactivation-text" placeholder="Enter deactivation reason"></textarea>');
		$('#modalMessage .modal-body').append('<button id="deactivation-reason" class="btn">Deactivate</button>');
		$('#modalMessage .modal-body button').data('user-id', otherUserID);
	} else {
		$('#modalMessage, #layout').show();
		$('#modalMessage').css({"width": "299px", "height": "150px"});
		$('#modalMessage .modal-body').html('');
		$('#modalMessage .modal-body').append('<span>');
		$('#modalMessage .modal-body span').text('Are you really want to activate this account?');		
		$('#modalMessage .modal-body').append('<br><button id="activation-reason-yes" class="btn">Yes</button> &nbsp; ');
		$('#modalMessage .modal-body').append('<button id="activation-reason-no" class="btn">No</button>');
		$('#modalMessage .modal-body button').data('user-id', otherUserID);		
	}	
});

//invisible status
$(document).on('click','.controls .user-invisible',function() {
	var otherUserID = $(this).data('user-id');
	$.ajax({
		url: apiServer+'/admin/set-user-invisible',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {
			'userID': userID,
			'token': userToken,
			'otherUserID': otherUserID
		},
		success: function (response) {
			if (response['success'] && response['status'] == 0) {
				$('.controls .user-invisible[data-user-id="'+ otherUserID +'"] i').removeClass('fa-eye-slash').addClass('fa-eye');
				$('.controls .user-invisible[data-user-id="'+ otherUserID +'"]').attr('title','deactivate');
			} else {
				$('.controls .user-invisible[data-user-id="'+ otherUserID +'"] i').removeClass('fa-eye').addClass('fa-eye-slash');
				$('.controls .user-invisible[data-user-id="'+ otherUserID +'"]').attr('title','activate');
			}
		}
	});
	return false;
});

$(document).on('click','.controls .user-approve-status', function() {
	var otherUserID = $(this).attr('data-user-id');
	$.ajax({
		url: apiServer+'/v1/admin/set-user-approve-status',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {
			'otherUserID': otherUserID
		},
		success: function (response) {
			if (response.success) {
				location.reload(true);
			}
		}
	});
	return false;
});

//user finance
$('.user-finance-form').submit(function() {
	getFinancyList(1);
	return false;
});
function getFinancyList(page) {
	var otherUserID = $('[name="userID_to"]').val();
	if (otherUserID == '') {
		$('#modalMessage, #layout').show();
		$('#modalMessage .modal-body span').text('Please select user');
		return false;
	}
	var dateFrom = $('.user-finance-form input[name="date-from"]').val();
	var dateTo = $('.user-finance-form input[name="date-to"]').val();
	var pageName = $('.user-finance-form input[name="page_name"]').val();
	var limit = $('#limit').val();
	if (page == null || page < 0) {
		page = 1;
	}
	var offset = (page - 1) * limit;
	var searchParams = {
		'dateFrom': dateFrom,
		'dateTo': dateTo,
		'type': pageName
	};
	$.ajax({
		url: apiServer+'/v1/admin/get-user-finance-info',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {
			'otherUserID': otherUserID,
			'searchParams': searchParams,
			'limit': limit,
			'offset': offset
		},
		success: function (response) {
			if (!response.success) {
				$('#modalMessage, #layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			} else {
				//generateFinancyList(response.usersFinanceInfo, pageName, page);
				generateLetterList(response.usersFinanceInfo.letters.lettersArray, 1, response.usersFinanceInfo.letters.count);
				generateChatList(response.usersFinanceInfo.chats.chatArray, 1, response.usersFinanceInfo.chats.count);
				generateVideoList(response.usersFinanceInfo.videoChats.chatArray, 1, response.usersFinanceInfo.videoChats.count);
				generateGiftList(response.usersFinanceInfo.gifts.giftsArray, 1, response.usersFinanceInfo.gifts.count);
				generatePremiumPhotoList(response.usersFinanceInfo.premiumPhotos.premiumPhotosArray, 1, response.usersFinanceInfo.premiumPhotos.count);
				generatePremiumVideoList(response.usersFinanceInfo.PremiumVideo.premiumVideoArray, 1, response.usersFinanceInfo.PremiumVideo.count);
			}
		},
	});           
}

function financePagination(page) {
	getFinancyList(page);
	return false;
}

function generateLetterList(items, page, count)
{
	if (items.length > 0) {
		var html = '';
		var total = 0;
		for (var i = 0; i <= items.length - 1; i++) {					
			var amount = (items[i]['amount'] == null) ? 0 : parseFloat(items[i]['amount']).toFixed(2);
			html += '<tr><td>'+ items[i]['created_at'] +'</td><td><a href="/manager/user/' + items[i]['other_user_id'] +'" class="table_link">'+items[i]['first_name']+'</a> [ID: '+ items[i]['other_user_id'] +']</td><td>' + amount + ' cr</td></tr>';			
			total += (items[i]['amount'] == null) ? 0 : parseFloat(items[i]['amount']);
		}
		html += '<tr><td colspan="2"><b>Total:</b></td><td><b>' + total.toFixed(2) +' cr</b></td></tr>';
		$('table.finance-leters-table tbody').html(html);
	} else {
		$('table.finance-leters-table tbody').html('<tr><td colspan="3">no letters</td></tr>');
	}
}

function generateChatList(items, page, count)
{
	if (items.length > 0) {
		var html = '';
		var total = 0;
		var totalDuration = 0;
		for (var i = 0; i <= items.length - 1; i++) {					
			var amount = (items[i]['amount'] == null) ? 0 : parseFloat(items[i]['amount']).toFixed(2);
			var duration = new Date(1970, 1, 1, 0, 0, items[i]['duration']).toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
			html += '<tr><td>'+ items[i]['created_at'] +'</td><td><a href="/manager/user/' + items[i]['otherUserID'] +'" class="table_link">'+items[i]['first_name']+'</a> [ID: '+ items[i]['otherUserID'] +']</td><td>'+ duration +'</td><td>' + amount + ' cr</td></tr>';			
			total += (items[i]['amount'] == null) ? 0 : parseFloat(items[i]['amount']);
			totalDuration +=(items[i]['duration'] == null) ? 0 : parseInt(items[i]['duration']);
		}
		html += '<tr><td colspan="2"><b>Total:</b></td><td><b>'+ new Date(1970, 1, 1, 0, 0, totalDuration).toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1") +'</b></td><td><b>' + total.toFixed(2) +' cr</b></td></tr>';
		$('table.finance-chat-table tbody').html(html);
	} else {
		$('table.finance-chat-table tbody').html('<tr><td colspan="4">no chats</td></tr>');
	}
}

function generateVideoList(items, page, count)
{
	if (items.length > 0) {
		var html = '';
		var total = 0;
		var totalDuration = 0;
		for (var i = 0; i <= items.length - 1; i++) {
			if (parseInt(items[i]['duration']) > 0) {
				var amount = (items[i]['amount'] == null) ? 0 : parseFloat(items[i]['amount']).toFixed(2);
				var duration = new Date(1970, 1, 1, 0, 0, items[i]['duration']).toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
				html += '<tr><td>'+ items[i]['created_at'] +'</td><td><a href="/manager/user/' + items[i]['other_user_id'] +'" class="table_link">'+items[i]['first_name']+'</a> [ID: '+ items[i]['other_user_id'] +']</td><td>'+ duration +'</td><td>' + amount + ' cr</td></tr>';			
				total += (items[i]['amount'] == null) ? 0 : parseFloat(items[i]['amount']);
				totalDuration +=(items[i]['duration'] == null) ? 0 : parseInt(items[i]['duration']);
			}			
		}
		html += '<tr><td colspan="2"><b>Total:</b></td><td><b>'+ new Date(1970, 1, 1, 0, 0, totalDuration).toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1") +'</b></td><td><b>' + total.toFixed(2) +' cr</b></td></tr>';
		$('table.finance-video-chat-table tbody').html(html);
	} else {
		$('table.finance-video-chat-table tbody').html('<tr><td colspan="4">no chats</td></tr>');
	}
}

function generateGiftList(items, page, count)
{
	if (items.length > 0) {
		var html = '';
		var total = 0;		
		for (var i = 0; i <= items.length - 1; i++) {
			var amount = (items[i]['amount'] == null) ? 0 : parseFloat(items[i]['amount']).toFixed(2);
			html += '<tr><td>'+ items[i]['created_at'] +'</td><td><a href="/manager/user/' + items[i]['other_user_id'] +'" class="table_link">'+items[i]['first_name']+'</a> [ID: '+ items[i]['other_user_id'] +']</td><td>' + amount + ' cr</td></tr>';			
			total += (items[i]['amount'] == null) ? 0 : parseFloat(items[i]['amount']);
		}
		html += '<tr><td colspan="2"><b>Total:</b></td><td><b>' + total.toFixed(2) +' cr</b></td></tr>';
		$('table.finance-gifts-table tbody').html(html);
	} else {
		$('table.finance-gifts-table tbody').html('<tr><td colspan="3">no gifts</td></tr>');
	}
}

function generatePremiumPhotoList(items, page, count)
{
	if (items.length > 0) {
		var html = '';
		var total = 0;		
		for (var i = 0; i <= items.length - 1; i++) {
			var amount = (items[i]['amount'] == null) ? 0 : parseFloat(items[i]['amount']).toFixed(2);
			html += '<tr><td>'+ items[i]['created_at'] +'</td><td><a href="/manager/user/' + items[i]['otherUserID'] +'" class="table_link">'+items[i]['first_name']+'</a> [ID: '+ items[i]['otherUserID'] +']</td><td>' + amount + ' cr</td></tr>';			
			total += (items[i]['amount'] == null) ? 0 : parseFloat(items[i]['amount']);
		}
		html += '<tr><td colspan="2"><b>Total:</b></td><td><b>' + total.toFixed(2) +' cr</b></td></tr>';
		$('table.finance-premium-photos-table tbody').html(html);
	} else {
		$('table.finance-premium-photos-table tbody').html('<tr><td colspan="3">no Premium Photos</td></tr>');
	}
}

function generatePremiumVideoList(items, page, count)
{
	if (items.length > 0) {
		var html = '';
		var total = 0;		
		for (var i = 0; i <= items.length - 1; i++) {
			var amount = (items[i]['amount'] == null) ? 0 : parseFloat(items[i]['amount']).toFixed(2);
			html += '<tr><td>'+ items[i]['created_at'] +'</td><td><a href="/manager/user/' + items[i]['otherUserID'] +'" class="table_link">'+items[i]['first_name']+'</a> [ID: '+ items[i]['otherUserID'] +']</td><td>' + amount + ' cr</td></tr>';			
			total += (items[i]['amount'] == null) ? 0 : parseFloat(items[i]['amount']);
		}
		html += '<tr><td colspan="2"><b>Total:</b></td><td><b>' + total.toFixed(2) +' cr</b></td></tr>';
		$('table.finance-premium-video-table tbody').html(html);
	} else {
		$('table.finance-premium-video-table tbody').html('<tr><td colspan="3">no Premium Video</td></tr>');
	}
}

function generateFinancyList(items, pageName, page) {
	$.ajax({
		url: '/manager/finance/generate-financy-list',
		type: "POST",
		dataType: "json",
		data: {
			'items': items,
			'page_name': pageName,
			'page': page
		},
		success: function (response) {
			$('table.finance-leters-table tbody').html(response.letters);
			$('table.finance-chat-table tbody').html(response.chats);
			$('table.finance-video-chat-table tbody').html(response.video_chats);
			$('table.finance-gifts-table tbody').html(response.gifts);
			$('table.finance-premium-photos-table tbody').html(response.premiumPhotos);
			$('table.finance-premium-video-table tbody').html(response.PremiumVideo);
			if (response.paginator != "") {
				$('.admin-finance-pagination').html(response.paginator);
			}
		}
	});
}

var userPagination = function (page, pageName, params) {
	var agencyID = $('#agency-select1').val();
	if (params == '') {
		params = $('.content-area-inner-body .content-container .user-filter span.active').attr('data-search-param');
	}
	$.ajax({
        url: '/manager/' + pageName,
        dataType: "json",
        type: "POST",
        data: {
        	'page': page,
        	'agencyID': agencyID,
        	'params': params},
        success: function (response) {
            $('.content-area-inner-body .content-container').html(response);
			$('.content-area-inner-body .content-container .user-filter span').removeClass('active');
			if (params != '') {
				$('.content-area-inner-body .content-container .user-filter span[data-search-param='+params+']').addClass('active');
			}
        },
    });           
}

var userCustomPagination = function (page, pageName) {
	var agencyID = $('#agency-select1').val();
	var dateFrom = $('input[name="dateFrom"]').val();
	var dateTo = $('input[name="dateTo"]').val();
	var sortBy = $('select[name="sortBy"]').val();
	var sortWay = $('select[name="sortWay"]').val();
	var userId = $('input[name="userId"]').val();
	var userName = $('input[name="userName"]').val();
	var userAgeFrom = $('select[name="userAgeFrom"]').val();
	var userAgeTo = $('select[name="userAgeTo"]').val();
	var userCity = $('input[name="userCity"]').val();
	var userCountry = $('input[name="userCountry"]').val();
	var activityStatus = $('input[name="activityStatus"]:checked').val();

	$.ajax({
        url: '/manager/' + pageName,
        dataType: "json",
        type: "POST",
        data: {
        	'page': page,
        	'agencyID': agencyID,
        	'dateFrom': dateFrom,
        	'dateTo': dateTo,
        	'sortBy': sortBy,
        	'sortWay': sortWay,
        	'userId': userId,
        	'userName': userName,
        	'userAgeFrom': userAgeFrom,
        	'userAgeTo': userAgeTo,
        	'userCity': userCity,
        	'userCountry': userCountry,
        	'activityStatus': activityStatus
        },
        success: function (response) {
            $('.content-area-inner-body .content-container').html(response);			
        },
    }); 	
}

var giftPagination = function (page) {
	var searchParams = getGiftSearchParams();
	$.ajax({
		url: '/manager/gift-list',
		dataType: "json",
		type: "POST",
		data: {
			'page': page,
			'searchParams': searchParams
		},
		success: function (response) {
			if (response['success']) {
				$('#gifts_content').html(response['gifts_html']);
			}
		},
	});
	return false;
}

function getGiftSearchParams() {
	var sort = $('#gifts_content .sort_by a.active').attr('data-value');
	var giftCategoryArray = [];
	$('#gifts_content .gifts_categories li').each(function () {
		if ($(this).hasClass('active') && $(this).attr('data-value') != 'all') {
			giftCategoryArray.push($(this).attr('data-value'));
		}
	});
	return {
		'sort': sort,
		'giftsCategory': giftCategoryArray
	}
}

$(document).on('click', '#gifts_content .sort_by a', function () {
	$('#gifts_content .sort_by a').removeClass('active');
	$(this).addClass('active');
	giftPagination(1);
});

$(document).on('click', '#gifts_content .gifts_categories li', function () {
	$('#gifts_content .gifts_categories li[data-value="all"]').removeClass('active');
	console.log($(this));
	if (!$(this).hasClass('active')) {
		console.log($(this).data('value'));
		if ($(this).attr('data-value') == 'all') {			
			$('#gifts_content .gifts_categories li').removeClass('active');
		}
		$(this).addClass('active');
	} else {
		$(this).removeClass('active');
	}
	giftPagination(1);
});
$('#gifts_content .gifts_categories li[data-value="all"]').click();

$('#agency-select').change(function(){
	$('.user-activity-form').submit();
});

$('#agency-select1').change(function(){
	var user_type = $('#agency-select1').attr('data-user-type');
	userPagination(1,user_type,'');
});

// agency finance
$('.agency-finance-form').submit(function(){
	var agencyID = $('select[name="agency_id"]').val();
	if (agencyID == '') {
		$('#modalMessage, #layout').show();
		$('#modalMessage .modal-body span').text('Please select agency');
		return false;
	}
	var dateFrom = $('.agency-finance-form input[name="date-from"]').val();
	var dateTo = $('.agency-finance-form input[name="date-to"]').val();

	var searchParams = {
		'dateFrom': dateFrom,
		'dateTo': dateTo
	};
	$.ajax({
		url: apiServer+'/v1/admin/get-agency-finance-info',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {
			'agencyID': agencyID,
			'searchParams': searchParams
		},
		success: function (response) {
			if (!response.success) {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			} else {
				var total = parseFloat(response.agencyArray.letters.amount) +
					parseFloat(response.agencyArray.chats.amount) +
					parseFloat(response.agencyArray.videoChats.amount) +
					parseFloat(response.agencyArray.gifts.amount) +
					parseFloat(response.agencyArray.premium_photos.amount) +
					parseFloat(response.agencyArray.premium_video.amount);
				var agencyFinanceHtml = '<tr><td><a href="javascript:void(0)" onclick="window[\'adminAccountLibrary\'][\'getFinanceDetailInfo\'](\'letters\');">письма</a></td>'+ 
											'<td>'+response.agencyArray.letters.count+'</td>'+
											'<td>'+Math.round(parseFloat(response.agencyArray.letters.amount) * 100) / 100+' cr</td></tr>' +
										'<tr><td><a href="javascript:void(0)" onclick="window[\'adminAccountLibrary\'][\'getFinanceDetailInfo\'](\'chats\');">чат</a></td>'+
											'<td>'+generateTimeFormat(response.agencyArray.chats.duration)+'</td>'+
											'<td>'+Math.round(parseFloat(response.agencyArray.chats.amount) * 100) / 100+' cr</td></tr>' +
										'<tr><td><a href="javascript:void(0)" onclick="window[\'adminAccountLibrary\'][\'getFinanceDetailInfo\'](\'video-chat\');">видео чат</a></td>' +
											'<td>'+generateTimeFormat(response.agencyArray.videoChats.duration)+'</td>' + 
											'<td>'+Math.round(parseFloat(response.agencyArray.videoChats.amount) * 100) / 100+' cr</td></tr>' +
										'<tr><td><a href="javascript:void(0)" onclick="window[\'adminAccountLibrary\'][\'getFinanceDetailInfo\'](\'gifts\');">подарки</a></td>' +
											'<td>'+response.agencyArray.gifts.count+'</td><td>'+Math.round(parseFloat(response.agencyArray.gifts.amount) * 100) / 100+' cr</td></tr>' +
										'<tr><td><a href="javascript:void(0)" onclick="window[\'adminAccountLibrary\'][\'getFinanceDetailInfo\'](\'premium_photos\');">премиум фото</a></td>' +
											'<td>'+response.agencyArray.premium_photos.count+'</td><td>'+Math.round(parseFloat(response.agencyArray.premium_photos.amount) * 100) / 100+' cr</td></tr>' +
										'<tr><td><a href="javascript:void(0)" onclick="window[\'adminAccountLibrary\'][\'getFinanceDetailInfo\'](\'premium_video\');">премиум видео</a></td>' +
											'<td>'+response.agencyArray.premium_video.count+'</td><td>'+Math.round(parseFloat(response.agencyArray.premium_video.amount) * 100) / 100+' cr</td></tr>' +
										'<tr><td colspan="2"><strong>Итого:</strong></td><td><strong>'+Math.round(parseFloat(total) * 100) / 100+' cr</strong></td></tr>';
				$('.agency-finance-table tbody').html(agencyFinanceHtml);
			}
		},
	});           
	return false;
});

	$('.add-agency-payments-form').submit(function () {
		var order_id = $('#order_id').val();
		var agency_id = $('#agency_id').val();
		var comments = $('#comments').val();
		var payment_value = $('#payment_value').val();

		$.ajax({
			url: '/manager/finance/add-agency-payments',
			dataType: "json",
			type: "POST",
			data: {'agencyID': agency_id, 'comments': comments, 'orderID': order_id, 'paymentValue': payment_value},
			success: function (ans) {
				window["commonAdminLibrary"]['show_modal_message'](ans.success, ans.message, ans.code);
				if (ans.success) {
					setTimeout(function () {
						location.href = '/manager/finance/agency-payments-schedule';
					}, 2000);
				}
			},
		});

		return false;
	});

function getFinanceDetailInfo(label) {
	var agencyID = $('select[name="agency_id"]').val();
	var dateFrom = $('.agency-finance-form input[name="date-from"]').val();
	var dateTo = $('.agency-finance-form input[name="date-to"]').val();
	var searchParams = {
		'dateFrom': dateFrom,
		'dateTo': dateTo,
		'type': label
	}
	if (agencyID == '') {
		$('#modalMessage,#layout').show();
		$('#modalMessage .modal-body span').text('Please select agency');
		return false;
	}
	if (label == '') {
		$('#modalMessage,#layout').show();
		$('#modalMessage .modal-body span').text('Please try again');
		return false;
	}
	$.ajax({
		url: apiServer+'/v1/admin/get-user-finance-info',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {
			'agencyID': agencyID,
			'searchParams': searchParams
		},
		success: function (response) {
			if (!response.success) {
				$('#modalMessage, #layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			} else {
				generateDetailFinanceInfo(response.usersFinanceInfo,label);
			}
		},
	});
}

function generateDetailFinanceInfo(items, label) {
	var params = {
		'items': items,
		'page_name': label,
		'page': 1
	};
	$.ajax({
		url: '/manager/finance/generate-financy-list',
		type: "POST",
		dataType: "json",
		data: params,
		success: function (response) {
			var str = '';
			if (label == 'letters') {
				str += '<h3>Детальная информация по письмам</h3>' +
							'<table class="finance-leters-table margin-top">' +
							'<thead><th>дата</th><th>от кого</th><th>кому</th><th>стоимость</th></thead>' +
							'<tbody>'+response.letters+'</tbody></table>';
			} else if (label == 'chats') {
				str += '<h3>Детальная информация по чату</h3>' +
							'<table class="finance-chat-table margin-top">' +
								'<thead><th>дата</th><th>инициатор чата</th><th>с кем</th><th>длительность</th><th>стоимость</th></thead>' + 
								'<tbody>'+response.chats+'</tbody></table>';
			} else if (label == 'video-chat') {
				str += '<h3>Детальная информация по видео чату</h3>' +
							'<table class="finance-video-chat-table margin-top">' + 
								'<thead><th>дата</th><th>инициатор чата</th><th>с кем</th><th>длительность</th><th>стоимость</th></thead>' + 
								'<tbody>'+response.video_chats+'</tbody></table>';
			} else if (label == 'gifts') {
				str += '<h3>Детальная информация по подаркам</h3>' +
							'<table class="finance-leters-table margin-top">' + 
							'<thead><th>дата</th><th>от кого</th><th>кому</th><th>стоимость</th></thead>' + 
							'<tbody>'+response.gifts+'</tbody></table>';
			} else if (label == 'premium_photos') {
				str += '<h3>Детальная информация по Premium Фото</h3>' +
					'<table class="finance-leters-table margin-top">' +
					'<thead><th>дата</th><th>от кого</th><th>кому</th><th>стоимость</th></thead>' +
					'<tbody>'+response.premiumPhotos+'</tbody></table>';
			} else if (label == 'premium_video') {
				str += '<h3>Детальная информация по Premium Видео</h3>' +
					'<table class="finance-leters-table margin-top">' +
					'<thead><th>дата</th><th>от кого</th><th>кому</th><th>стоимость</th></thead>' +
					'<tbody>'+response.PremiumVideo+'</tbody></table>';
			}

			$('.agency-detail-finance-info').html(str);
		},
	});
}

function generateTotalFinanceReportList(data) {
	$.ajax({
		url: '/manager/finance/generate-total-finance-report-list',
		type: "POST",
		dataType: "json",
		data: {'data': data},
		success: function (response) {
			if (!response.success) {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			} else {
				$('.ajax-content').html(response.finance_list);
			}
		},
	});           
}

function generateTimeFormat(seconds) {
	var m = Math.floor(seconds / 60);
	var s = seconds % 60;
	var h = Math.floor(m / 60);
	m = m % 60;
	return timeFormat(h) + ":" + timeFormat(m) + ":" + timeFormat(s);
}
function timeFormat(number) {
    var string = number.toString();
    if (string.length < 2) {
		return "0" + string;
    }
    return string;
}

$('#save-page').click(function() {
	tinymce.triggerSave();
	var description = $('#text-memo').val();
	var pageName = $('#page-name').val();
	$.ajax({
		url: apiServer+'/admin/set-custom-page-text',
		type: "POST",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		dataType: "json",
		data: {
			'userID': userID,
			'token': userToken,
			'description': description,
			'pageName': pageName
		},
		success: function (response) {
			if (!response.success) {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			} else {
				location.reload();
			}
		},
	});
});

$('form.add-gift').submit(function() {
	var name = $('form.add-gift [name="name"]').val();
	var giftTypeID = $('form.add-gift [name="type_id"]').val();
	var description = $('form.add-gift [name="description"]').val();
	var price = $('form.add-gift [name="price"]').val();
	var agency_price = $('form.add-gift [name="agency_price"]').val();
	var image = $('form.add-gift [name="image"]').val();
	var giftID = $('form.add-gift [name="giftID"]').val();
	$.ajax({
		url: apiServer+'/v1/gift/add-gift',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {						
			'skinID': 1,
			'name': name,
			'description': description,
			'price': price,
			'agency_price': agency_price,
			'image': image,
			'giftID': giftID,
			'type_id': giftTypeID
		},
		success: function (response) {
			if (!response['success']) {
				$('#modalMessage, #layout').show();
				$('#modalMessage .modal-body span').text(response['message']);
			} else {
				location.href = '/manager/gift-list';
			}
		},
	});
	return false;
});

$('.agency-finance-form select[name="agency_id"]').change(function(){
	$('.agency-detail-finance-info').html('');
	$('.agency-finance-form').submit();
});

$(document).on('click','.get_userToken_to_login',function() {
	var otherUserID = $(this).attr('data-other-user-id');
	$.ajax({
		url: '/manager/get-user-token',
		type: "POST",
		dataType: "json",
		data: {
			'skinID': 1,
			'otherUserID': otherUserID
		},
		success: function (response) {
			if (!response.success) {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			} else {
				if (response.type == 'girl') {
					location.href = '/account/'+otherUserID;
				}
				else {
					location.href = '/manager/edit-user/'+otherUserID;
				}
			}
		},
	});
});

var getAlbumPhoto = function (album) {
	var otherUserID = $('#otherUserID').val();
	$.ajax({
		url: '/manager/get-album-photo',
		type: "post",
		dataType: "json",
		data: {
			'otherUserID': otherUserID,
			'album': album
		},
		success: function (response) {
			if (response['success'] && response['photos'] != null) {
				$('#document-attach-photo').html(response['photos']);
			}
		},
	});
}

var setPhotoStatus = function (photoID, status, place) {
	var otherUserID = $('#otherUserID').val();
	$.ajax({
		url: apiServer+'/user/set-photo-status',
		dataType: "json",
		type: "POST",
		data: {
			'otherUserID': otherUserID,
			'token': userToken,			
			'photoID': photoID,
			'status': status
		},
		success: function (response) {
			if (response.success) {
				getAlbumPhoto(place);
			}
		},
	});
}

$('.video-content .item_video button').click(function() {
	var videoID = $(this).attr('data-video-id');
	var videoDescription = $(this).siblings('textarea').val();
	var publicVideo = $(this).siblings('.approve-block').find('.public').val(); 
	var premium = $(this).siblings('.approve-block').find('.premium').val(); 
	var status = $(this).siblings('.approve-block').find('.approve').val(); 
	var otherUserID = $('#otherUserID').val();
	if (typeof (videoID) != 'undefined' && videoID !='') {
		$.ajax({
			url: apiServer+'/user/update-video-description',
			dataType: "json",
			headers: {
            	"Authorization": "Bearer " + userToken
			},
			type: "POST",
			data: {
				'userID': userID,
				'skin_id': 1,				
				'otherUserID': otherUserID,
				'videoID': videoID,
				'videoDescription': videoDescription,
				'status': status,
				'public': publicVideo,
				'premium': premium
			},
			success: function (response) {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response.message);
				setTimeout(window['commonAdminLibrary']['hideModal'], 2000);
			},
		});
	}		
});

$('.video-data .remove_btn').click(function() {
	if (!confirm('Are you sure you want delete this video?')) {
		return false;
	}
	var videoID = $(this).siblings('button').attr('data-video-id');
	var otherUserID = $('#otherUserID').val();
	if (typeof (videoID) != 'undefined' && videoID !='')
		$.ajax({
			url: apiServer+'/admin/delete-video',
			dataType: "json",
			type: "POST",
	        headers: {
            	"Authorization": "Bearer " + userToken
        	},
			data: {				
				'skinID': 1,				
				'otherUserID': otherUserID,
				'videoID': videoID
			},
			success: function (response) {
				if (response['success']) {
					$('#modalMessage, #layout').show();
					$('#modalMessage .modal-body span').text(response['message']);
					setTimeout(function(){
						window['commonAdminLibrary']['hideModal']();
						location.reload(true);
					}, 2000);
				} else {
					$('#modalMessage,#layout').show();
					$('#modalMessage .modal-body span').text(response['message']);
				}
			},
		});
});

$('#agency-box #all_agency').change(function() {
	if ($(this).prop('checked')) {
		$('#agency-box input[type="checkbox"]').prop('checked', true);
	} else {
		$('#agency-box input[type="checkbox"]').prop('checked', false);
	}
});

function tasksPagination(page) {
	getTasks(page);
	return false;
}

function getTotalPeopleCount() {
    $.ajax({
        url: apiServer + '/v1/admin/get-total-people-count',
        dataType: "json",
        type: "POST",
        data: {'skinID': 1},
        headers: {
        	"Authorization": "Bearer " + userToken
    	},
        success: function (response) {
            if (response.success) {
                $('#total-men').text(response.men.total);
                $('#men-online').text(response.men.online);
                $('#total-girls').text(response.women.total);
                $('#girls-online').text(response.women.online);
                $('#total-agency').text(response.agency.total);
            } else {
                window["commonAdminLibrary"]['show_modal_message'](response.success, response.message, response.code);
            }
        },
    });
}

function getTasks(page) {
	if (page == null || page < 0) {
		page = 1;
	}
	var searchParams = getTasksParams();
	$.ajax({
		url: '/manager/tasks',
		type: "POST",
		dataType: "json",
		data: {
			'searchParams': searchParams,
			'page': page
		},
		success: function (response) {
			if (response.success) {
				if (response.tasksList != null && response.tasksList.length > 0) {
					$('#tasks-list').html(response.tasksList);					
				} else {
					$('#tasks-list').html('');					
				}
			} else {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			}
		},
	});
}

function getTasksParams() {
	var status = $('select[name="status"]').val();
	var dateFrom = $('input[name="dateFrom"]').val();
	var dateTo = $('input[name="dateTo"]').val();
	var searchParams = {
		'status': status,
		'dateFrom': dateFrom,
		'dateTo': dateTo
	};
	return searchParams;
}

$('.search_tasks_btn').click(function() {
	getTasks(1);
	return false;
});

$('#check_all').change(function() {
	if ($(this).prop('checked')) {
		$('.content-container tbody input[type="checkbox"]').prop('checked', true);
	} else {
		$('.content-container tbody input[type="checkbox"]').prop('checked', false);
	}
});

$('.set_status').click(function() {
	var status = $(this).attr('data-value');
	var userIDs = [];
	$('.content-container tbody input[type="checkbox"]').each(function() {
		var currentID = $(this).attr('data-user-id');
		if ($(this).prop('checked')) {
			userIDs.push(currentID);
		}
	});

	if (userIDs.length == 0) {
		$('#modalMessage,#layout').show();
		$('#modalMessage .modal-body span').text('please check users');
		return false;
	}
	$.ajax({
		url: apiServer+'/admin/set-online-status',
		dataType: "json",
		type: "POST",
		data: {
			'userID': userID,
			'skin_id': 1,
			'token': userToken,
			'userIDs': userIDs,
			'status': status
		},
		headers: {
			"Authorization": "Bearer " + userToken
		},
		success: function (response) {
			if (response.success) {
				location.reload();
			} else {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			}
		},
	});

});

	$('.agency-payments-schedule-form').submit(function () {
		var agency_id = $('[name="agency_id"]').val();
		$('#add-agency-payments').attr('href', '/manager/finance/add-agency-payments/' + agency_id);
		var month_from = (($('#month_from').val()).length == 1 ? '0' + $('#month_from').val() : $('#month_from').val());
		var year_from = $('#year_from').val();
		var month_to = (($('#month_to').val()).length == 1 ? '0' + $('#month_to').val() : $('#month_to').val());
		var year_to = $('#year_to').val();
		var date_from = year_from + '-' + month_from + '-01';
		var date_to = year_to + '-' + month_to + '-01';
		if ((new Date(date_from)) > (new Date(date_to))) {
			$('#modalMessage,#layout').show();
			$('#modalMessage .modal-body span').text('Wrong date format');
			return false;
		}
		$.ajax({
			url: '/manager/finance/agency-payments-schedule',
			dataType: "json",
			type: "POST",
			data: {'dateFrom': date_from, 'dateTo': date_to, 'agency_id': agency_id},
			success: function (ans) {
				if (ans.success) {
					$('#agency-payments-schedule-block').html(ans.html);
				} else {
					$('#modalMessage,#layout').show();
					$('#modalMessage .modal-body span').text(ans.message);
				}
			},
		});

		return false;
	});


$(document).on('change','#all_users_check',function() {
	if ($(this).prop('checked')) {
		$('input[id^="user_id_"]').prop('checked', true);
	} else {
		$('input[id^="user_id_"]').prop('checked', false);
	}
});

function getDocumentAlbum() {
	$.ajax({
		url: '/account/get-document-album',
		dataType: "json",
		success: function (response) {
			if (response.success && response.photos != null) {
				$('#document-attach-photo').html(response.photos);
			}
		},
	});
}

//add photo in gift comment text
$(document).on('click','#document-attach-photo img', function() {
	if ($('#gift_comment').length > 0) {
		var href = $(this).attr('src');
		var originalHref = $(this).attr('data-original'),
		innerHtml = '<p style="text-align:center;" data-mce-style="text-align: center;">'
			+'<a href="'+originalHref+'" target="_blank" rel="group" data-mce-href="'+originalHref+'">'
			+'<img src="'+href+'" border="0" width="250" data-thumb="'+href+'" data-mce-src="'+href+'"></a></p>';
		var currentContent = tinyMCE.activeEditor.getContent();
		tinyMCE.activeEditor.setContent(currentContent+innerHtml);
	}
});

var logPagination = function(page) {
	var otherUserID = $('#other-user-id').val();
	var userType = $('#user-type').val();
	var dateFrom = $('#date-from').val();
	var dateTo = $('#date-to').val();
	var limit = $('#log-items').val()
	$.ajax({
		url: apiServer+'/admin/get-log',
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		type: "POST",
		data: {			
			'skinID': 1,
			'token': userToken,
			'otherUserID': otherUserID,
			'page': page,
			'limit': limit,
			'searchParams': {
				'userType': userType,
				'dateFrom': dateFrom,
				'dateTo':dateTo
			}
		},
		success: function (response) {
			if (response['success']) {
				if (response['logArray'].length == 0) {
					$('table.log-table tbody').html('<tr><td colspan="4">No data</td></tr>');
				} else {						
					$('table.log-table tbody').html('');
					var totalPages = Math.ceil(response['count'] / limit);
					var $userDiv;
					for (var i = 0; i <= response['logArray'].length - 1; i++) {
						$('table.log-table tbody').append(
							$('<tr>').append(
								'<td><a href="/manager/user/' + response['logArray'][i]['user_id'] + '">'+ response['logArray'][i]['user_id'] +'</a></td>' +
								'<td>' + response['logArray'][i]['title'] + '</td>' +
								'<td>' + response['logArray'][i]['description'] + '</td>' +
								'<td>' + response['logArray'][i]['created_at'] + '</td>'
							)
						);			
					}	
					if (totalPages > 1) {
						$('.page-nav-wrapper').remove();
						$('#log-search-form + div').append($('<div>').addClass('page-nav-wrapper'));
						$('#log-search-form + div .page-nav-wrapper').html(getPagination(page, totalPages, window['adminAccountLibrary']['logPagination']));
					}												
				}
			} else {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			}
		},
	});
};

var historyPagination = function(page) {
	var	letterID = $('#letterID').val();
	var limit = $('#history-items').val();
	$.ajax({
		url: apiServer+'/v1/letter/get-history/' + letterID,
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		type: "POST",
		data: {			
			'skinID': 1,
			'page': page,
			'limit': limit,			
		},
		success: function (response) {
			if (response['success']) {
				if (response['lettersCount'].length == 0) {
					$('.content-container .col-xs-12').html('<p>No data</p>');
				} else {						
					$('.content-container .col-xs-12').html('<ul class="messages"></ul>');
					var totalPages = Math.ceil(response['lettersCount'] / limit);
					var user1ID = $('img.user1').data('user');
					var user2ID = $('img.user2').data('user');
					for (var i = 0; i <= response['lettersArray'].length - 1; i++) {
						var block = '';
						if (user1ID == response['lettersArray'][i]['from_user_id']) {
							block = '<div class="msj macro">' +
									'<div class="avatar">'+
										'<img class="img-circle" style="width:100%;" src="' + ((response['lettersArray'][i]['from_user_avatar'] != null) ? apiServer + '/' + response['lettersArray'][i]['from_user_avatar'] : '/img/no_avatar_normal.jpg') +'">' +
									'</div>' +
									'<div class="text text-l">' +
										'<h3>Тема письма: '+ response['lettersArray'][i]['theme'] +'</h3>' +
										'<p>Содержание письма:</p>' +
										'<div>'+ response['lettersArray'][i]['description'] +'</div>' +
										'<p><small>Дата создания: '+ response['lettersArray'][i]['created_at'] +'</small></p>' +
										'<p><small>Дата прочтения: '+ ((response['lettersArray'][i]['readed_at'] != null) ? response['lettersArray'][i]['readed_at'] : '') +'</small></p>' +
									'</div>' + 
								'</div>';
						} else if (user2ID == response['lettersArray'][i]['from_user_id']) {
							block = '<div class="msj-rta macro">' +
									'<div class="text text-r">' +
										'<h3>Тема письма: '+ response['lettersArray'][i]['theme'] +'</h3>' +
										'<p>Содержание письма:</p>' +
										'<div>'+ response['lettersArray'][i]['description'] +'</div>' +
										'<p><small>Дата создания: '+ response['lettersArray'][i]['created_at'] +'</small></p>' +
										'<p><small>Дата прочтения: '+ ((response['lettersArray'][i]['readed_at'] != null) ? response['lettersArray'][i]['readed_at'] : '') +'</small></p>' +
									'</div>' +
									'<div class="avatar">' +
										'<img class="img-circle" style="width:100%;" src="' + ((response['lettersArray'][i]['from_user_avatar'] != null) ? apiServer + '/' + response['lettersArray'][i]['from_user_avatar'] : '/img/no_avatar_normal.jpg') + '">' +
									'</div>' +
								'</div>';
						}
						$('.content-container .col-xs-12 .messages').append(
							$('<li>').css('width', '100%').append(block)
						);			
					}	
					if (totalPages > 1) {
						$('.page-nav-wrapper').remove();
						$('.content-container .col-xs-12').append($('<div>').addClass('page-nav-wrapper'));
						$('.content-container .col-xs-12 .page-nav-wrapper').html(getPagination(page, totalPages, window['adminAccountLibrary']['historyPagination']));
					}												
				}
			} else {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			}
		},
	});
};

$('#log-search-form').submit(function() {	
	logPagination(1);
	return false;
});

function getPagination(currentPage, totalPages, myfunction, range = 1, dots = "", paginationClass="") {
	if (dots == "") {
		dots = "<li><a href=\"javascript:void(0);\" class='dots'>...</a></li>";
	}
	var slider = range * 2;
	var $result = $('<ul>');
	var from, to;
	if (paginationClass=="") {
		$result.addClass("page-navigation list-inline");
	} else {
		$result.addClass(paginationClass);
	}
	if (currentPage > 1) {
		var prevpage = currentPage - 1;
		$result.append(buildLink(prevpage, "<", myfunction, 'prev'));
	}
	if (totalPages < 5 + range*2) {
    	from = 1;
    	to = totalPages;    	
		$result.append(buildLinkList(currentPage, from, to, myfunction));				
    }
    // Example: 1 [2] 3 4 5 6 ... 23 24
	else if (currentPage <= slider + 2) {
		from = 1;
		to = slider + 3;
		$result.append(buildLinkList(currentPage, from, to, myfunction));
		$result.append(dots, buildLinkList(currentPage, totalPages-1, totalPages, myfunction));
	}
	// Example: 1 2 ... 32 33 34 35 [36] 37
	else if (currentPage >= totalPages - slider-1) {
		from = totalPages - slider - 2;
		to = totalPages;
		$result.append(buildLinkList(currentPage, 1, 2, myfunction), dots);
		$result.append(buildLinkList(currentPage, from, to, myfunction));
	} else if ((currentPage > slider) && (currentPage < totalPages - slider)) {
		// Example: 1 2 ... 23 24 25 [26] 27 28 29 ... 51 52
		$result.append(buildLinkList(currentPage, 1, 2, myfunction), dots);
		$result.append(buildLinkList(currentPage, currentPage - range, currentPage + range, myfunction));
		$result.append(dots, buildLinkList(currentPage, totalPages-1, totalPages, myfunction));
	} 
	if (currentPage != totalPages) {
		var nextpage = currentPage + 1;
		$result.append(buildLink(nextpage, ">", myfunction, 'next'));
	}
	return $result;
}

function buildLinkList(currentPage, from, to, myfunction) {
	var pages = [];
	var page;
	for (page = from; page <= to; page++) {
		if (page == currentPage) {         
        	pages.push(buildLink(page, page, myfunction, 'active'));
      	// if not current page...
      	} else {
         // make it a link
        	pages.push(buildLink(page, page, myfunction, ''));
      	} // end else
	}
	return pages;
}

function buildLink(page, text, myfunction, linkClass = "") {
	var $link = $('<li>');
	if (linkClass != "") {
		$link.addClass(linkClass);
	}
	$link.append('<a>');
	$link.find('a').attr('href', "javascript:void(0);").on('click', function() {myfunction(page)}).text(text);
	return $link;
}

$('.user-penalty-form').submit(function () {
    var agencyID = $('#agency-id').val();
    var dateFrom = $('#dateFrom').val();
    var dateTo = $('#dateTo').val();
    if ((new Date(dateFrom)) > (new Date(dateTo))) {
        window["commonAdminLibrary"]['show_modal_message'](true, 'Wrong date format', 2);
        return false;
    }
    $.ajax({
        url: '/manager/penalty',
        dataType: "json",
        type: "POST",
        data: {'agencyID': agencyID, 'dateFrom': dateFrom, 'dateTo': dateTo},
        success: function (response) {
            if (response.success) {
                $('.penalty-content').html(response.html);
            } else {
                window["commonAdminLibrary"]['show_modal_message'](response.success, response.message, response.code);
            }
        },
    });
    return false;
});

$('.user-balance-form').submit(function () {
    var month = $('#month').val();
    var year = $('#year').val();
    $.ajax({
        url: '/manager/balance',
        dataType: "json",
        type: "POST",
        data: {'month': month, 'year': year},
        success: function (response) {
            if (response['success']) {
                $('.balance-content').html(response['html']);
            } else {
                window["commonAdminLibrary"]['show_modal_message'](response['success'], response['message'], response['code']);
            }
        },
    });
    return false;
});


	$('.post-content form.add-post').submit(function(){
		var post_title = $('#post-title').val();
		var post_desc = $('#post-desc').val();
		var post_image = $('#post-image').val();
		var post_category = $('input[type="radio"][name="category"]:checked').val();
		var post_id = $('#post-id').val();
		var status = $('#status').prop('checked') ? 1 : 0;
		var on_main = $('#on_main').prop('checked') ? 1 : 0;
		var sort_order = $('#sort_order').val();
		if(post_title == '' || post_desc == '' || post_image == '' || post_category == '' || sort_order == ''){
			return false;
		}

		$.ajax({
			url: apiServer+'/admin/add-post',
			dataType: "json",
			headers: {
				"Authorization": "Bearer " + userToken
			},
			type: "POST",
			data: {
				'skinID': 1,
				'token': userToken,
				'post_title': post_title,
				'post_desc': post_desc,
				'post_image': post_image,
				'status': status,
				'on_main': on_main,
				'post_category': post_category,
				'sort_order': sort_order,
				'post_id': post_id,
			},
			success: function (response) {
				if (response['success']) {
					location.href = '/manager/blog/'
				} else {
					window["commonAdminLibrary"]['show_modal_message'](false, response.message, response.code);
					return false;
				}
			},
		});
		return false;
		
		
	});

	$('#add-post1').click(function(){
		

	});

function getMultiChatNewMessageCount() {
    var myUsersIDs = [];
    $('.chat-name-block .tab.user-name').each(function () {
        myUsersIDs.push($(this).attr('data-user-id'));
    });
    if (myUsersIDs.length == 0) {
        return false;
    }
    $('.chat-name-block .tab.user-name').removeClass('new-message');
    $('.chat-list-block .message-section h3 .marker').removeClass('active');
    $.ajax({
        url: apiServer + '/v1/admin/new-messages',
        dataType: "json",
        type: "POST",
        data: {'skinID': 1, 'myUsersIDs': myUsersIDs},
        headers: {
            "Authorization": "Bearer " + userToken
        },
        success: function (response) {
            if (response['success']) {
                if (response['totalNewMessageCount'] != null) {
                	var lettersAdminCount = 0;
                    for (var girlID in response['totalNewMessageCount']) {
                        var newMessageFlag = false;
                        if (response['totalNewMessageCount'][girlID]['chat']['length'] > 0) {
                            newMessageFlag = true;                                                       
                            for (var chatItem in response['totalNewMessageCount'][girlID]['chat']) {
                                var currentName = response['totalNewMessageCount'][girlID]['chat'][chatItem]['first_name'];
                                var currentUserID = response['totalNewMessageCount'][girlID]['chat'][chatItem]['action_creator'];
                                var currentLastActivity = response['totalNewMessageCount'][girlID]['chat'][chatItem]['last_activity'];
                                $('.new-chats .new-chat-section[data-user-id="' + girlID + '"] .user[data-user-id="'+ currentUserID +'"]').remove();
                                var currentTime = Math.round(parseInt(Date.now()) / 1000);
                                var currentStatus = ((currentTime - currentLastActivity) > 45 ) ? 0 : 1;
                                var currentAvatar = (response['totalNewMessageCount'][girlID]['chat'][chatItem]['small_thumb']) ? apiServer + '/' + response['totalNewMessageCount'][girlID]['chat'][chatItem]['small_thumb'] : '/img/no_avatar_small.jpg';
                                $('.new-chat-section[data-user-id="'+ girlID +'"]')
						            .append(
						                $('<div>').addClass('user col-md-2').attr('data-user-id', currentUserID)
						                		.append('<div class="person-online-messages"><span>' + response['totalNewMessageCount'][girlID]['chat'][chatItem]['unreaded_messages'] + '</span></div>')
						                        .append($('<div>').addClass('avatar').css('backgroundImage', 'url(' + currentAvatar + ')'))
						                        .append($('<div>').addClass('online-status').data('status', currentStatus).append($('<span>').addClass(currentStatus ? 'green' : 'red').html(currentStatus ? 'online' : 'offline')))
						                        .append($('<div>').addClass('name').html(currentName))
						                        .append($('<div>').addClass('user-id').html("[ID: "+ currentUserID +"]"))
						            );
                            }                            
                        }                        
                        if (response['totalNewMessageCount'][girlID]['letters']['count'] != '0') {
                        	lettersAdminCount += parseInt(response['totalNewMessageCount'][girlID]['letters']['count']);        					
                            $('.chat-letters-content .message-section>h3 .marker[data-user-id="' + girlID + '"]').addClass('active');
                            newMessageFlag = true;
                        }
                        if (newMessageFlag) {
                            $('.chat-name-block .tab.user-name[data-user-id="' + girlID + '"]').addClass('new-message');
                        }
                    }
					if (window['commonAdminLibrary']['storageAvailable']('localStorage')) {
						if (!localStorage.getItem('previousAdminLetterCount')) {
							localStorage.setItem('previousAdminLetterCount', 0);
						} else {
							previousAdminLetterCount = parseInt(localStorage.getItem('previousAdminLetterCount'));
							localStorage.setItem('previousAdminLetterCount', lettersAdminCount);
						}
					} else {
						previousAdminLetterCount = 0;
					}                    					
    				if (window["commonAdminLibrary"]["storageAvailable"]('localStorage') && (lettersAdminCount > previousAdminLetterCount)) {				
						$('.audio_new_mail')[0].play();
					}
                }
            } else {
            	window["commonAdminLibrary"]['show_modal_message'](response['success'], response['message'], response['code']);
            }
        },
    });
}

return { 
	'getAlbumPhoto': getAlbumPhoto,
	'setPhotoStatus': setPhotoStatus,
	'userPagination': userPagination,
	'userCustomPagination': userCustomPagination,
	'giftPagination': giftPagination,
	'financePagination': financePagination,
	'activityPagination': activityPagination,
	'logPagination': logPagination,
	'historyPagination': historyPagination,
	'tasksPagination': tasksPagination,
	'getFinanceDetailInfo': getFinanceDetailInfo,
	"getTotalPeopleCount": getTotalPeopleCount,
	"getMultiChatNewMessageCount": getMultiChatNewMessageCount
};

})(jQuery);


$('.agency-sort-list th').click(function(){
	return false;
	var sort_type = $(this).attr('data-sort');
	var sort_elem = $(this).find('span.sort');
	var sort_way = 'down';
	if(sort_elem.hasClass('down')){
		clearSortFlag();
		sort_elem.removeClass('down').addClass('up');
		sort_way = 'up';
	}else if(sort_elem.hasClass('up')){
		clearSortFlag();
		sort_elem.removeClass('up').addClass('down');
		sort_way = 'down';
	}else{
		clearSortFlag();
		sort_elem.addClass('down');
	}
	$('#sortBy').val(sort_type);
	$('#sortWay').val(sort_way);
	$('.agency-sort-list-form').submit();
});

function clearSortFlag(){
	$('.agency-sort-list th span').removeClass('down up');
}

function copyToClipboard(elem) {
	// create hidden text element, if it doesn't already exist
	var targetId = "_hiddenCopyText_";
	var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
	var origSelectionStart, origSelectionEnd;
	if (isInput) {
		// can just use the original source element for the selection and copy
		target = elem;
		origSelectionStart = elem.selectionStart;
		origSelectionEnd = elem.selectionEnd;
	} else {
		// must use a temporary form element for the selection and copy
		target = document.getElementById(targetId);
		if (!target) {
			var target = document.createElement("textarea");
			target.style.position = "absolute";
			target.style.left = "-9999px";
			target.style.top = "0";
			target.id = targetId;
			document.body.appendChild(target);
		}
		target.textContent = elem.textContent;
	}
	// select the content
	var currentFocus = document.activeElement;
	target.focus();
	target.setSelectionRange(0, target.value.length);

	// copy the selection
	var succeed;
	try {
		succeed = document.execCommand("copy");
	} catch(e) {
		succeed = false;
	}
	// restore original focus
	if (currentFocus && typeof currentFocus.focus === "function") {
		currentFocus.focus();
	}

	if (isInput) {
		// restore prior selection
		elem.setSelectionRange(origSelectionStart, origSelectionEnd);
	} else {
		// clear temporary content
		target.textContent = "";
	}
	return succeed;
}