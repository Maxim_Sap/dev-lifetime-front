(function( $ ) {

	var wh = $(window).height();

	/*document ready*/
	$(document).ready(function(){

		if($(window).width() > 767) {
			$('.login-name').on('click', function() {
				$('.login-options').stop().fadeToggle(300);
			});
		}

		$(document).on('click touchstart', function (event) {
			if (!$(event.target).closest('.login-name, .login-options').length) {
				$('.login-options').fadeOut(300);
			}
		});


		$('.bar-toggle').on('click', function() {
			$('.logo-container, .content-container, #tool-bar').stop().toggleClass('collapsed');
			$('#content-area').stop().toggleClass('expanded');
		});

		$('.collapse-bar').on('click', function() {
			$('.logo-container, .content-container, #tool-bar').addClass('collapsed');
			$('#content-area').addClass('expanded');

		});


		/* ---------- tool bar ---------- */

		$('#tool-bar').css('min-height',$(window).height() - 64);

		$('.tools-nav li a').on('click', function() {
			$('.tools-nav .sub-nav').slideUp(400);
			$('.tools-nav .drop-menu').not($(this)).removeClass('droped');
			$('.tools-nav > li').not($(this).parent('li')).removeClass('active');
			$(this).parent('li').addClass('active');
		});


		/* ---------- scroll top ---------- */

		$(window).on('scroll', function() {
			var st = $(this).scrollTop();
			st > wh ? $('.scroll-top').addClass('visible') : $('.scroll-top').removeClass('visible');
		});


		$('.scroll-top').on('click', function() {
			$('html, body').animate({
				scrollTop: 0
			}, 700);
		});



		/* ---------- tools nav ---------- */

		$('.tools-nav .sub-nav').parent('li').children('a').addClass('drop-menu');

		$('.drop-menu').on('click', function() {
			$('.tools-nav .drop-menu').not($(this)).removeClass('droped');
			$(this).stop().toggleClass('droped');
			$('.tools-nav .sub-nav').not($(this).parent('li').find('.sub-nav')).slideUp(400);
			$(this).parent('li').find('.sub-nav').stop().slideToggle(400);
		});

		$('.tools-nav .sub-nav li a').on('click', function() {
			$('.tools-nav .sub-nav li').not($(this).parent('li')).removeClass('active');
			$(this).parent('li').addClass('active');
		});

		$('.tools-nav > li > a').not('.drop-menu').on('click', function() {
			$('.tools-nav .sub-nav li').removeClass('active');
		});


		$('#content-area input[type="checkbox"]').each(function(){
			var $this = $(this),
			$label = $this.siblings( "label[for=" + $this.attr( "id" ) + "]" );
			$this.add($label).wrapAll('<div class="check-group"/>');
		});

		$('#content-area input[type="radio"]').each(function(){
			var $this = $(this),
			$label = $this.siblings( "label[for=" + $this.attr( "id" ) + "]" );
			$this.add($label).wrapAll('<div class="radio-group"/>');
		});

		$('.check-group').each(function() {
			$(this).append('<span class="custom-check">');
		});

		$('.radio-group').each(function() {
			$(this).append('<span class="custom-radio">');
		});


		$('.check-group, .radio-group').each(function() {
			if($(this).find('input').prev('label').length) {
				$(this).addClass('label-prev');
			}
		});

		$('.check-group, .radio-group').each(function() {
			if($(this).find('input').next('label').length) {
				$(this).addClass('label-next');
			}
		});



		$('.gifts-categories li').on('click', function() {
			$('.gifts-categories li').removeClass('active');
			$(this).addClass('active');
		});

		if($(window).width() < 1367) {
			$('#content-area table').each(function() {
				$(this).parent('.content-container').css('overflow-x', 'auto');
			});
		}
		

	});











	/*window load*/
	$(window).on('load', function() {

	});

	/*window resize*/
	$(window).resize(function() {

		var wh = $(window).height();

		/* ---------- tool bar ---------- */

		$('#tool-bar').css('min-height',$(window).height() - 64);

		/* ---------- scroll top ---------- */

		$(window).on('scroll', function() {
			var st = $(this).scrollTop();
			st > wh ? $('.scroll-top').addClass('visible') : $('.scroll-top').removeClass('visible');
		});
		
	});

})(jQuery);