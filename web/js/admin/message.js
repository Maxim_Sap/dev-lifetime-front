var messageAdminLibrary = (function($) {

window["commonLibrary"] = window["commonLibrary"] || window["commonAdminLibrary"];

var userID = window["commonLibrary"]["userID"];
var userToken = window["commonLibrary"]["userToken"];
var apiServer = window["commonLibrary"]["apiServer"];
var USER_FEMALE = 2;
var USER_MALE = 1;

$(document).on('click','#letters-list .view-letter, .letters-content .view-letter',function() {
	var element = $(this).parents('tr');
	var messageID = element.find('.message-id').text();
	var messageFrom = element.find('.message-from').text();
	var messageTo = element.find('.message-to').text();
	var createdAt = element.find('.data-create').text();
	var readedAt = element.find('.data-read').attr('data-read');
	var answered = element.find('.answered').text();
	var caption = element.find('.caption').val();
	var desc = element.find('.desc').html();
	$('#modal-detail-message .modal-info .message-id').text(messageID);
	$('#modal-detail-message .modal-info .message-from').text(messageFrom);
	$('#modal-detail-message .modal-info .message-to').text(messageTo);
	$('#modal-detail-message .modal-info .data-create').text(createdAt);
	$('#modal-detail-message .modal-info .data-read').text(readedAt);
	$('#modal-detail-message .modal-info .answered').text(answered);
	$('#modal-detail-message .modal-info .caption').text(caption);
	$('#modal-detail-message .modal-body').html(desc);
	$('#modal-detail-message,#layout').show();
	return false;
});

$(document).on('click', '.letters-content .view-letter.read', function () {
    var lettersID = $(this).parents('tr').find('.message-id').text();
    var otherUserID = $('.chat-name-block .tab.user-name.active').attr('data-user-id');
    readMessage(lettersID, otherUserID);
});

$(document).on('click', '.letters-content a.reply', function () {
    $('.message-block .letters-labels>div').eq(0).click();
    var caption = $(this).parents('tr').find('.caption').val();
    var themeID = $(this).parents('tr').find('.theme_id').val();
    var messageID = $(this).parents('tr').find('.message-id').text();
    var otherUserName = $(this).parents('tr').find('.message-from').html();
    var otherUserID = $(this).parents('tr').find('.other_user_id').val();
    $('.new-letter-form .letters_caption').attr('disabled', 'disabled').val(caption);
    $('.new-letter-form .other_user_id').attr('disabled', 'disabled').val(otherUserID);
    $('.new-letter-form .other_user_name').html(otherUserName);
    $('.new-letter-form .theme_id').val(themeID);
    $('.new-letter-form .previous_letters_id').val(messageID);

    return false;
});

$('.message-block .letters-labels>div:eq(0)').click(function () {
    $('.new-letter-form input').val('');
    $('.new-letter-form .letters_caption').removeAttr('disabled');
    $('.new-letter-form .other_user_id').removeAttr('disabled');
    $('.new-letter-form .other_user_name').html('');
    $('.new-letter-form textarea').val('');
    if (typeof(tinyMCE) != 'undefined' && tinyMCE.activeEditor != null) {
        tinyMCE.activeEditor.setContent('');
    }
});

$('#admin-letters-search').submit(function() {
	getLetters(1);
	return false;
});

function getParams() {
	var answered = $('select[name="answered"]').val();
	var readed = $('select[name="read"]').val();
	var userIDFrom = $('[name="user_id_from"]').val();
	var userIDTo = $('[name="user_id_to"]').val();
	var dateFrom = $('input[name="date_from"]').val();
	var dateTo = $('input[name="date_to"]').val();
	var pageName = $('input[name="box"]').val();
	var searchParams = {
		'answered': answered,
		'read': readed,
		'userID_from': userIDFrom,
		'userID_to': userIDTo,
		'dateFrom': dateFrom,
		'dateTo': dateTo,
		'type': pageName
	}
	return searchParams;
}

function getLetters(page) {
	var limit = $('#numberOfMessagesOnAdminPage').val();
	var otherUserID = $('#otherUserID').val();
	if (page == null || page < 0) {
		page = 1;
	}
	var offset = (page - 1) * limit;
	var searchParams = getParams();
	$.ajax({
		url: apiServer+'/v1/admin/letters',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {
			'limit': limit,
			'otherUserID': otherUserID,
			'offset': offset,
			'searchParams': searchParams
		},
		success: function (response) {
			if (response.success) {
				if(response.lettersArray != null && response.lettersArray.length > 0) {
					generateLettersList(response.lettersArray, response.countLetters, page)
				} else {
					$('#letters-list').html('');
					$('.admin-message-pagination').html('');
				}
			} else {
				$('#modalMessage,#layout').show();
    			$('#modalMessage .modal-body span').text(response.message);
			}
		},
	});           
}

function messagePagination(page) {
	if ($('#admin-message-search').length > 0) {
		getMessage(page);
	} else {
		getLetters(page);
	}	
	return false;
}

function generateLettersList(lettersArray, numberOfLetters, page) {
	$.ajax({
		url: '/manager/letters/generate-list',
		type: "POST",
		dataType: "json",
		data: {
			'lettersArray': lettersArray,
			'countLetters': numberOfLetters,
			'page': page,
			'inbox': ($("input[name=box]").val() == "inbox") ? 1 : null
		},
		success: function (response) {
			$('#letters-list').html(response.messageList);
			$('.admin-message-pagination').html(response.pagination);
		}
	});
}

$('#admin-message-search').submit(function() {
	getMessage(1);
	return false;
});

function getMessage(page) {
	var limit = $('#numberOfMessagesOnAdminPage').val();
	if (page == null || page < 0) {
		page = 1;
	}
	var offset = (page - 1) * limit;
	var searchParams = getParams();
	$.ajax({
		url: apiServer+'/v1/admin/get-message',
		type: "POST",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {
			'limit': limit,
			'offset': offset,
			'searchParams': searchParams
		},
		success: function (response) {
			if (response.success) {
				if (response.messageArray != null && response.messageArray.length > 0) {
					generateMessageList(response.messageArray, response.count, page);
				} else {
					$('#message-list').html('');
					$('.admin-message-pagination').html('');
				}
			} else {
				$('#modalMessage,#layout').show();
    			$('#modalMessage .modal-body span').text(response.message);
			}
		},
	});           
}

function generateMessageList(messageArray, numberOfMessages, page) {
	var pageName = $('input[name="box"]').val();
	$.ajax({
		url: '/manager/message/generate-list',
		type: "POST",
		dataType: "json",
		data: {
			'message_mass': messageArray,
			'count_message': numberOfMessages,
			'page': page,
			'page_name': pageName
		},
		success: function (response) {
			$('#message-list').html(response.message_list);
			$('.admin-message-pagination').html(response.pagination);
		}
	});
}

$(document).ready(function() {    
	if ($('.admin-message textarea').length > 0) {
		tinymce.init({
			selector: ".admin-message textarea",
			plugins: "paste textcolor emoticons",
			paste_as_text: true,
			skin: "custom",
			height: "300",
			menubar: false,
			toolbar: "bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | forecolor backcolor | emoticons | undo redo | fullscreen",
			statusbar: true,
			language: "en",
		});
	}
});

$('.admin-message').submit(function() {
	tinymce.triggerSave();
	if ($('.admin-message textarea').val() == '') {
		alert('Description can\'t be empty');
		return false;
	}
});

$('#userTypeID').change(function() {
	var userTypeID = $(this).val();
	$.ajax({
		url: apiServer+'/v1/admin/get-users-info-by-users-type',
		dataType: "json",
		type: "POST",
		data: {			
			'skinID': 1,			
			'userType': userTypeID
		},
        headers: {
            "Authorization": "Bearer " + userToken
        },
		success: function (response) {
			if (response.success) {
				if (response.userList.length == 0) {
					$('.admin-letters-search .result').html('');
					return false;
				}
				if (userTypeID == USER_FEMALE || userTypeID == 4 || userTypeID == 7) {
					var htmlBlock = '<select class="agency_list">';
					for (var i=0; i < response.userList.length; i++) {
						htmlBlock +='<option value="'+response.userList[i]['id']+'">' + 
										response.userList[i]['name'] + 
									'</option>';
					}
					htmlBlock += '</select><div class="girl-list"></div>';
					$('.admin-letters-search .result').html(htmlBlock);
					$('.admin-letters-search .agency_list').change();

				} else if (userTypeID == USER_MALE || userTypeID == 8) {
					var htmlBlock = '<input id="all_users_check" type="checkbox" />' + 
										'<label for="all_users_check">все</label><br/>';
					for (var i=0; i < response.userList.length; i++) {
						htmlBlock +='<input name="userIDs[]" id="user_id_'+response.userList[i]['id'] +'" type="checkbox" value="'+response.userList[i]['id']+'" />'
							+'<label for="user_id_'+response.userList[i]['id']+'">'+response.userList[i]['first_name']+' '+((response.userList[i]['last_name'] != null) ? response.userList[i]['last_name'] : '')+' [ID: '+response.userList[i]['id']+']</label><br/>';
					}
					$('.admin-letters-search .result').html(htmlBlock);
				} else {
					var htmlBlock = '<input id="all_users_check" type="checkbox" />' + 
										'<label for="all_users_check">все</label><br/>';
					for (var i=0; i < response.userList.length; i++) {
						htmlBlock +='<input name="userIDs[]" id="user_id_'+response.userList[i]['id'] +'" type="checkbox" value="'+response.userList[i]['id']+'" />'
							+'<label for="user_id_'+response.userList[i]['id']+'">'+response.userList[i]['name']+' [ID: '+response.userList[i]['id']+']</label><br/>';
					}
					$('.admin-letters-search .result').html(htmlBlock);
				}
				wrapCheckbox();
			} else {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			}
		},
	});
});

function wrapCheckbox() {
	$('#content-area input[type="checkbox"]').each(function(){
		var $this = $(this),
		$label = $this.siblings( "label[for=" + $this.attr( "id" ) + "]" );
		$this.add($label).wrapAll('<div class="check-group"/>');
	});
	$('.check-group').each(function() {
		$(this).append('<span class="custom-check">');
	});
	$('.check-group').each(function() {
		if($(this).find('input').prev('label').length) {
			$(this).addClass('label-prev');
		}
	});
	$('.check-group').each(function() {
		if($(this).find('input').next('label').length) {
			$(this).addClass('label-next');
		}
	});
}

$(document).on('change','.admin-letters-search .agency_list',function() {
	var agencyID = $(this).val();
	var userTypeID = $('#userTypeID').val();
	var userType = 'girls';
	if (userTypeID == 7) {
		var userType = 'admin';
	} else if (userTypeID == 4) {
		var userType = 'translators';
	}
	$.ajax({
		url: apiServer+'/v1/admin/get-users-info-from-agency',
		dataType: "json",
		type: "POST",
		data: {			
			'skinID': 1,			
			'otherUserID': agencyID,
			'type': userType
		},
		headers: {
            "Authorization": "Bearer " + userToken
        },
		success: function (response) {
			if (response.success) {
				if (response.usersArray == null) {
					$('.admin-letters-search .result .girl-list').html('');
					return false;
				}
				var htmlBlock = '<br/><input id="all_users_check" type="checkbox" /><label for="all_users_check">все</label><br/>';
				for (var i=0; i<response.usersArray.length; i++) {
					htmlBlock +='<input name="userIDs[]" id="user_id_'+response.usersArray[i]['id']+'" type="checkbox" value="'+response.usersArray[i]['id']+'" />'
						+'<label for="user_id_'+response.usersArray[i]['id']+'">'+((response.usersArray[i]['first_name'] != null) ? response.usersArray[i]['first_name'] : 'Noname')+' '+((response.usersArray[i]['last_name'] != null) ? response.usersArray[i]['last_name'] : '') +' [ID: '+response.usersArray[i]['id']+']</label><br/>';
				}
				$('.admin-letters-search .result .girl-list').html(htmlBlock);
				wrapCheckbox();
			} else {
				$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(response.message);
			}
		},
	});
});

function readSystemMessage(messageID) {
	var userID = $('#user_id').val();
	$.ajax({
		url: apiServer+'/v1/message/read-message',
		dataType: "json",
		type: "POST",
        headers: {
            "Authorization": "Bearer " + userToken
        },
		data: {
			'token': userToken,
			'messageID': messageID
		},
		success: function (response) {
			if(response.success == true){
				//console.log(response);
			} else {
				$('#modalMessage').modal('show');
				$('#modalMessage .modal-body span').text(response.message);
			}
		},
	});
}

function readMessage(lettersID, otherUserID) {
    $.ajax({
        url: apiServer + '/v1/admin/read-letter',
        dataType: "json",
        type: "POST",
        data: {'otherUserID': otherUserID, 'letterID': lettersID},
        headers: {
            "Authorization": "Bearer " + userToken
        },
        success: function (ans) {
            if (ans.success == true) {
                //console.log(ans);
            } else {
                window["commonAdminLibrary"]['show_modal_message'](ans.success, ans.message, ans.code);
            }
        },
    });
}

var chatTimer;
$('.chat-name-block .tab.user-name').click(function () {
    if (!$(this).hasClass('active')) {
        $('.chat-name-block .tab.user-name').removeClass('active');
        $(this).addClass('active');
        $('.chat-content-block .tab.user-content').hide();
        $('.message-block').hide();
        $('.chat-letters-content .chat-wrapper').hide();
        $('.message-block .letters-labels>div').eq(0).click();
        $('.chat-content-block .tab.user-content[data-user-id="' + $(this).attr('data-user-id') + '"]').show();
        $('.new-chats .new-chat-section').hide();
        $('.new-chats .new-chat-section[data-user-id="' + $(this).attr('data-user-id') + '"]').show();
        $('.chat-letters-content .message-section .marker').hide();
        $('.chat-letters-content .message-section .marker[data-user-id="' + $(this).attr('data-user-id') + '"]').show();
        clearInterval(chatTimer);
    }
});
$('.chat-content-block .set-status button').click(function () {
    var other_user_id = $(this).parents('.tab.user-content').attr('data-user-id');
    var status = $('#status_' + other_user_id).prop('checked');
    status = (status) ? 1 : 0;
    $.ajax({
        url: '/manager/chat/set-women-online',
        dataType: "json",
        type: "POST",
        data: {'other_user_id': other_user_id, 'status': status},
        success: function (response) {
            if (response['success']) {
                 window["commonAdminLibrary"]['show_modal_message'](true, 'User status updated', 2);
                $('#status_' + other_user_id).attr('data-status', status);
            } else {
                window["commonAdminLibrary"]['show_modal_message'](response['success'], response['message'], response['code']);
            }
        },
    });
});
$('#load-letters').click(function () {
    $('.message-block').show();
    var girlUserID = $('.chat-name-block .user-name.active').attr('data-user-id');
    getMultichatLetters(1, girlUserID, 'inbox');
    setTimeout(function () {
        getMultichatLetters(1, girlUserID, 'outbox');
    }, 1000);
});

function getMultichatLetters(skinID, userID, type) {
    $.ajax({
        url: apiServer + '/v1/admin/user-letters',
        dataType: "json",
        type: "POST",
        data: {'skinID': skinID, 'otherUserID': userID, 'limit': 50, 'offset': 0, 'type': type},
        headers: {
            "Authorization": "Bearer " + userToken
        },
        success: function (response) {
            if (response['success']) {
                generateMultiChatLettersList(response, type);
                return 1;
            } else {
                window["commonAdminLibrary"]['show_modal_message'](response['success'], response['message'], response['code']);
                return false;
            }
        },
    });
}

function generateMultiChatLettersList(lettersData, type) {
    $.ajax({
        url: '/manager/letters/generate-letters-list-for-chat',
        dataType: "json",
        type: "POST",
        data: {'lettersData': lettersData, 'type': type},
        success: function (response) {
            if (type == 'inbox') {
                $('.letters-content .tab.inbox-letters').html(response.messageList);
            } else if (type == 'outbox') {
                $('.letters-content .tab.outbox-letters').html(response.messageList);
            }
            return 1;
        },
    });
}
$('.message-block .letters-labels>div').click(function () {
    if (!$(this).hasClass('active')) {
        $('.message-block .letters-labels>div').removeClass('active');
        $(this).addClass('active');
        $('.letters-content .tab').hide();
        $('.letters-content .tab.' + $(this).attr('data-source')).show();
    }
});

$('.new-letter-form').submit(function (event) {
	event.preventDefault();
    var fromUserID = $('.chat-name-block .user-name.active').attr('data-user-id');
    var toUserID = $('.new-letters .other_user_id').val();
    var caption = $('.new-letters .letters_caption').val();
    var desc = tinyMCE.activeEditor.getContent();
    var themeID = $('.new-letters .theme_id').val();
    var previousLetterID = $('.new-letters .previous_letters_id').val();
    if (toUserID == '' || caption == '' || desc == '') {
        window["commonAdminLibrary"]['show_modal_message'](false, 'Please fill all fields', 2);
    }

    $.ajax({
        url: apiServer + '/v1/admin/send-letter',
        dataType: "json",
        type: "POST",
        data: {
            'skinID': 1,
            'fromUserID': fromUserID,            
            'toUserID': toUserID,
            'title': caption,
            'desc': desc,
            'themeID': themeID,
            'previousLetterID': previousLetterID
        },
        headers: {
            "Authorization": "Bearer " + userToken
        },
        success: function (response) {
            if (response['success']) {
                window["commonAdminLibrary"]['show_modal_message'](true, 'Letter send', 2);
                $('.message-block .letters-labels>div:eq(0)').click();
            } else {
                window["commonAdminLibrary"]['show_modal_message'](false, response['message'], 2);
                return false;
            }
        },
    });

    return false;
});

function getMenList()
{
	$.ajax({
        url: apiServer + '/user/get-users',
        dataType: "json",
        type: "POST",
        data: {
            'skinID': 1,
            'userType': 1,
            'limit': 50,
            'offset': 0,
            'orderBy': 'last_activity',
            'direction': 'DESC'
        },
        success: function (response) {
            if (response.success) {
                generateUserList(response);
            }
        },
    });
}

function generateUserList(data) {
    $.ajax({
        url: '/manager/chat/generate-user-list',
        dataType: "json",
        type: "POST",
        data: data,
        success: function (html) {
            $('.chat-content-block .men-online').html(html);
        },
    });
}

$(document).on('click', '.men-online .user-tile', function () {
    if (!$(this).hasClass('active')) {
        $('.men-online .user-tile').removeClass('active');
        $(this).addClass('active');
        $('.new-letters input.other_user_id').val($(this).attr('data-user-id'));
        $('.chat-list-block input.chat-other-user-id').val($(this).attr('data-user-id'));
        $('.chat-list-block input.chat-other-user-status').val($(this).find('.online-status').attr('data-status'));
        $('.new-letters .other_user_name').text($(this).find('.name').text());
    }
});

$(document).on('click', '.new-chats .new-chat-item-block', function () {
    if (!$(this).hasClass('active')) {
        $('.new-chats .new-chat-item-block').removeClass('active');
        $(this).addClass('active');
        $('.chat-list-block input.chat-other-user-id').val($(this).attr('data-user-id'));
        $('.chat-list-block input.chat-other-user-status').val($(this).attr('data-user-status'));
        $('.chat-list-block input.current-chat-other-user-avatar').val($(this).attr('data-user-avatar'));
        $('.men-online .user-tile').removeClass('active');
    }
});

var chat_mess_type = 'all';
/*$('#load-chat').click(function () {
    var other_user_id = $('.chat-list-block input.chat-other-user-id').val();
    var other_user_status = $('.chat-list-block input.chat-other-user-status').val();
    if (other_user_id == '') {
        window["commonAdminLibrary"]['show_modal_message'](false, 'please select user with who you want to chat', 2);
        return false;
    }
    if (other_user_status == 0) {
        window["commonAdminLibrary"]['show_modal_message'](false, 'please select online user', 2);
        return false;
    }
    $('.chat-list-block input.current-chat-other-user-id').val(other_user_id);

    if (typeof ($('.men-online .user-tile.active .avatar').attr('data-src')) != 'undefined') {
        $('.chat-list-block input.current-chat-other-user-avatar').val($('.men-online .user-tile.active .avatar').attr('data-src'));
    }

    $('.chat-letters-content .chat-wrapper').show();

    chat_mess_type = 'all';
    clearInterval(chat_timer);
    get_chat_content();
    chat_timer = setInterval(function () {
        get_chat_content();
    }, 3000);
});*/

function get_chat_content() {
    var girl_user_id = $('.chat-name-block .user-name.active').attr('data-user-id');
    var other_user_status = $('#status_' + girl_user_id).attr('data-status');
    if (other_user_status == 0) {
        window["commonAdminLibrary"]['show_modal_message'](false, 'your selected girl not online', 2);
        $('.chat-letters-content .chat-wrapper').hide();
        return false;
    }

    var current_chat_other_user_id = $('.chat-list-block input.current-chat-other-user-id').val();
    if (girl_user_id == '' || current_chat_other_user_id == '') {
        return false;
    }

    var last_chat_mess_id = $("#last-chat-message-id").val();
    chat_session = null;
    chat_messages_scroll();
    $.ajax({
        url: api_server + '/user/get-chat',
        dataType: "json",
        type: "POST",
        data: {
            'user_id': girl_user_id,
            'skin_id': 1,
            'to_user_id': current_chat_other_user_id,
            'chat_session': chat_session,
            'chat_mess_type': chat_mess_type,
            'last_chat_mess_id': last_chat_mess_id,
            'token': user_token
        },
        success: function (ans) {
            if (!ans.success) {
                window["commonAdminLibrary"]['show_modal_message'](false, ans.message, ans.code);
                return false;
            } else {
                get_chat_start_flag = true;
                if (ans.chat_mess_type == 'all') {
                    chat_mess_type = 'new';
                    if (ans.chat_message_mass.length > 0) {
                        var format_chat_mass = generate_chat_mess_list(girl_user_id, ans.chat_message_mass, 'all');
                        $('.chat-letters-content .chat_messages').html(format_chat_mass);
                        chat_messages_scroll();
                    } else {
                        $('.chat-letters-content .chat_messages').html('<p>No message</p>');
                    }
                } else if (ans.chat_mess_type == 'new') {
                    if (ans.chat_message_mass.length > 0) {
                        generate_chat_mess_list(girl_user_id, ans.chat_message_mass, 'new');
                        chat_messages_scroll();
                    }
                }
                chat_send_flag = false;
            }
        },
    });
}


function chat_messages_scroll() {
    var chat_messages_slim = $('.chat-box-messages-wrapper');
    var chat_content_height = chat_messages_slim.prop('scrollHeight');
    chat_messages_slim.slimscroll({
        size: '10px',
        width: '100%',
        start: 'bottom',
        height: 'calc(100% - 6px)',
        alwaysVisible: true,
        scrollTo: chat_content_height + 'px'
    });

}

function generate_chat_mess_list(girl_user_id, chat_mass, chat_type) {
    var message_list = '',
        last_user_id = 0;
    for (var i = 0; i < chat_mass.length; i++) {
        var date_format = new Date(chat_mass[i].dt);
        var date_format_value = date_format.getDate() + '/' + parseInt(date_format.getMonth() + 1) + '/' + date_format.getFullYear() + ' at ' + (date_format.getHours() < 10 ? '0' + date_format.getHours() : date_format.getHours()) + ':' + (date_format.getMinutes() < 10 ? '0' + date_format.getMinutes() : date_format.getMinutes());
        if (i == chat_mass.length - 1) {
            $('#last-chat-message-id').val(chat_mass[i].action_id);
        }
        if (chat_mass[i].user_from == girl_user_id) {
            var user_avatar = $('.chat-content-block .tab.user-content[data-user-id="' + girl_user_id + '"] .avatar').attr('src');
            var chat_user_float = 'fl-r';
            var chat_user_class = 'my_mes';
        } else {
            var user_avatar = $('.chat-list-block input.current-chat-other-user-avatar').val();
            var chat_user_float = 'fl-l';
            var chat_user_class = 'dial_mes';
        }
        //если поменялся id пользователя отвечающего в чате то закрываем блок с сообщениями
        if (chat_type == 'all') {
            var all_chat_mass = generate_all_chat_list(last_user_id, chat_mass[i], user_avatar, chat_user_float, chat_user_class, date_format_value);
            message_list += all_chat_mass.html;
            last_user_id = all_chat_mass.last_user_id;
        } else if (chat_type == 'new') {
            generate_new_chat_list(chat_mass[i], user_avatar, chat_user_float, chat_user_class, date_format_value);
        }
    }
    if (chat_type == 'all') {
        message_list += '</div></div>';
    }

    return message_list;
}

function generate_all_chat_list(last_user_id, chat_mass_item, user_avatar, chat_user_float, chat_user_class, date_format_value) {
    var message_list = '';
    if (last_user_id != chat_mass_item.user_from) {
        if (last_user_id != 0) {
            message_list += '</div></div>';
        }

        message_list += '<div class="' + chat_user_class + '" other-user-id="' + chat_mass_item.user_from + '"><div class="photo_wrap ' + chat_user_float + '"><div style="background-image:url(' + user_avatar + ')">'
            + '</div></div><div class="mess_text_wrap"><div class="mess_text">'
            + chat_mass_item.text + '<div class="date text-dark-grey">' + date_format_value + '</div></div>';
        //иначе пишем внутри одного блока
    } else {
        message_list += '<br><div class="mess_text">'
            + chat_mass_item.text + '<div class="date text-dark-grey">' + date_format_value + '</div></div>';
    }
    last_user_id = chat_mass_item.user_from;
    return {'html': message_list, 'last_user_id': last_user_id};
}

function generate_new_chat_list(chat_mass_item, user_avatar, chat_user_float, chat_user_class, date_format_value) {
    var last_user_id = $('.chat_messages>div').last().attr('other-user-id');
    var message_text = '';
    if (last_user_id == chat_mass_item.user_from) {
        message_text = '<br><div class="mess_text">' + chat_mass_item.text + '<div class="date text-dark-grey">' + date_format_value + '</div></div>';
        $('.chat_messages>div').last().find('div.mess_text_wrap').append(message_text);
    } else {
        message_text = '<div class="' + chat_user_class + '" other-user-id="' + chat_mass_item.user_from + '"><div class="photo_wrap ' + chat_user_float + '"><div style="background-image:url(' + user_avatar + ')">'
            + '</div></div><div class="mess_text_wrap"><div class="mess_text">'
            + chat_mass_item.text + '<div class="date text-dark-grey">' + date_format_value + '</div></div></div></div>';
        $('.chat_messages').append(message_text);
    }
}

function deleteSingleMessage(letterID, otherUserID) {
	if (confirm("Are you sure you want delete this letter?")) {
		$.ajax({
	        url: apiServer+'/v1/letter/delete-letter',
	        dataType: "json",
	        type: "POST",
            headers: {
            	"Authorization": "Bearer " + userToken
        	},	        
	        data: {	        	
	        	'letterID': letterID,
	        	'otherUserID': otherUserID
	        },
	        success: function (response) {
	            if (response.success == true) {
					location.reload();
	            } else {
					$('#modalMessage').modal('show');
					$('#modalMessage .modal-body span').text(response.message);
	            }
	        },
	    });           
	}
}

return {
	"messagePagination": messagePagination,
	"readSystemMessage": readSystemMessage,
	"getMenList": getMenList,
	"readMessage": readMessage,
	'deleteSingleMessage': deleteSingleMessage
}

})(jQuery);