window["commonAdminLibrary"] = (function($) {
'use strict';

var userID = (window["commonLibrary"] != null && window["commonLibrary"]["userID"] != null) ? window["commonLibrary"]["userID"] : $('meta[name=userID]').attr("content");
var userToken = (window["commonLibrary"] != null && window["commonLibrary"]["userToken"] != null) ? window["commonLibrary"]["userToken"] : $('meta[name=userToken]').attr("content");
var apiServer = (window["commonLibrary"] != null && window["commonLibrary"]["apiServer"]!= null) ? window["commonLibrary"]["apiServer"] : config['apiServer'];
var USER_MALE = 1;
var USER_FEMALE = 2;
//Facebook login


$(document).on('click','#layout', function() {
	$('#modalMessage,#modal-detail-message, #layout').hide();
});
                                  
var contentType ="application/x-www-form-urlencoded; charset=utf-8";
if (window.XDomainRequest) //for IE8, IE9
{
    contentType = "text/plain";
}

function storageAvailable(type) {
    try {
        var storage = window[type],
            x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch(e) {
        return e instanceof DOMException && (
            // everything except Firefox
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // test name field too, because code might not be present
            // everything except Firefox
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            storage.length !== 0;
    }
}

$('.register-form').submit(function () {
    var firstName = $('#modalSignIn input[name="first_name"]').val();
    var email = $('#modalSignIn input[name="email"]').val();
    var password = $('#modalSignIn input[name="password"]').val();
    var confirmPassword = $('#modalSignIn input[name="conf_password"]').val();
    if (password != confirmPassword) {
        $('#modalMessage,#layout').show();
        $('#modalMessage .modal-body span').text("Passwords do not match");
        return false;
    }
    
    var signInAgreement = $('#singin-agreement').prop('checked');
    if (!signInAgreement) {
        $('#modalMessage,#layout').show();
        $('#modalMessage .modal-body span').text("I agree with terms of conditions not chacked");
        return false;
    }    
    // send data to server
   
	var url = apiServer+'/reg-email/admin-register';
	var body = {
            'firstName': firstName,
            'email': email,
            'userTypeID': 3, // agency
            'password': password,
            'skinID': 1,
        };

    $.ajax({
        url: url,
        dataType: "json",
        contentType: contentType,
        type: "POST",
        data: body,
        success: function (response) {            
            if (response.success == true) {
                $('#modalSignIn input:not([type=submit])').val('');
                $('#modalMessage,#layout').show();
                $('#modalMessage .modal-body span').text(response.message);
            } else {
                if(response.code == 1){
                    $('#modalMessage,#layout').show();
                    $('#modalMessage .modal-body span').text(response.message);
                }
                if(response.code == 2){
                    location.href = '/manager/forgot';
                } else if (response.code == "Validation errors") {                    
                    var modelText = "";
                    for (var errorType in response.errors) {
                        if (response.errors.hasOwnProperty(errorType)) {
                            modelText += response.errors[errorType] + "<br />";
                            $('#modalMessage .modal-body span').html(modelText);
                            $('#modalMessage,#layout').show();
                        }
                    }
                }
            }
        },
    });           
    return false;
});


$('#modalForgotPassword form').submit(function (event) {
    event.preventDefault();
    var email = $('#modalForgotPassword input[name="email"]').val();
    // send data to server
   
	var url = apiServer+'/v1/reg-email/change-password-request';
	var body = {
            'email': email,            
            'type': 'manager',
        };

    $.ajax({
        url: url,
        dataType: "json",
        contentType: contentType,
        type: "POST",
        data: body,
        success: function (response) {
            $('#modalSignIn input:not([type=submit])').val('');
            $('#modalMessage,#layout').show();
            if (response.code == "Validation errors") {                    
                var modelText = "";
                for (var errorType in response.errors) {
                    if (response.errors.hasOwnProperty(errorType)) {
                        modelText += response.errors[errorType] + "<br />";
                        $('#modalMessage .modal-body span').html(modelText);
                        $('#modalMessage,#layout').show();
                    }
                }
            } else {
                $('#modalMessage .modal-body span').text(response.message);
            }            
        },
    });           
    return false;
});

$('.login-form').submit(function () {
    var login = $('.login-form input[name="login"]').val();
    var password = $('.login-form input[name="password"]').val();
    $.ajax({
        url: '/manager/login-user',
        dataType: "json",
        contentType: contentType,
        type: "POST",
        data: {
            'email': login,
            'password': password,            
        },
        success: function (response) {
            if (response.success == true) {
                location.href = "/manager/";
            } else {
                if(response.code == 1){
                    $('#modalMessage,#layout').show();
        			$('#modalMessage .modal-body span').text(response.message);
                } else if (response.code == "Validation errors") {                    
                    var modelText = "";
                    for (var errorType in response.errors) {
                        if (response.errors.hasOwnProperty(errorType)) {
                            modelText += response.errors[errorType] + "<br />";
                            $('#modalMessage .modal-body span').html(modelText);
                            $('#modalMessage,#layout').show();
                        }
                    }
                }
            }
        },
    });           
    return false;
});

$('#modalForgotPassword input[type="password"],#modalForgotPassword input[type="email"]').keypress(function () {
	$('#modalForgotPassword .error-message-text').text("");
});

$('#modalLogin input[type="password"],#modalLogin input[type="email"]').keypress(function () {
	$('#modalLogin .error-message-text').text("");
});

$('#logout_btn').click(function() {
	Fb_Logout();
	location.href = "/manager/logout";
});


function show_modal_message(success, message, code) {
    if ((!success && code != 1) || success) {
        $('#modalMessage,#layout').show();
        $('#modalMessage .modal-body span').text(message);
        setTimeout(hideModal, 3000);
    }
}

var hideModal = function () {
    $('#modalMessage, #modal-detail-message, #layout').hide();
}

return {    
    "apiServer": apiServer,    
    "contentType": contentType,
    "userID": userID,
    "userToken": userToken,      
    "USER_FEMALE": USER_FEMALE,
    "USER_MALE": USER_MALE, 
    'hideModal': hideModal,
    'show_modal_message': show_modal_message,
    "storageAvailable": storageAvailable,
}

})(jQuery);