(function ($) {
    'use strict';
    var userId = $('#userID').val();
    var userToken = window["commonAdminLibrary"]["userToken"];
    var apiServer = window["commonAdminLibrary"]["apiServer"];

    var webSocketServer = chatConfig['webSocketServer'];
    var chatPort = chatConfig['chatPort'];
    var videoChatPort = chatConfig['videoChatPort'];

    if (typeof console == "undefined") {
        this.console = {
            log: function (msg) {
            }
        };
    }
    if (typeof WEB_SOCKET_SWF_LOCATION != "undefined") {
        // if your brower not support websocket， we will use this flash automatic websocket modeling
        WEB_SOCKET_SWF_LOCATION = "/swf/WebSocketMain.swf";
        // enable flash websocket debug
        var WEB_SOCKET_DEBUG = true;
    }
    var send = false;
    var first = true;
    var second = false;    
    var stop, show = true, chatID, videoChatID, client_list = {}, video_list = {}, chatWarnings = {};
    var videoBuffer = [], bufferLength = 1;
    var chatConnectionTries = {}, videoConnectionTries = 0;
    var myCamera = document.getElementById('myVid');
    var myPC;
    var streamConstraints;
    var myMediaStream;
    var audioEnabled = true;
    var remoteCamera = document.getElementById('PeerVid');

    function byteToHex(byte) {
        return ('0' + byte.toString(16)).slice(-2);
    }

    // str randomString(int len);
    //   len - must be an even number (default: 40)
    function randomString(len) {
        var arr = new Uint8Array((len || 40) / 2);
        window.crypto.getRandomValues(arr);
        return [].map.call(arr, byteToHex).join("");
    }

    function getRoom() {
        return randomString(30);
    }
    
    function createChat(otherUserID) {
        var ws;
        ws = new WebSocket("wss://" + webSocketServer + ":" + chatPort);
        ws['otherUserID'] = otherUserID;
        //console.log(chatWebSocket);
        // when socket connection is open, enter user name
        ws.onopen = onopen;
        // when new message available print different information base on message type
        ws.onmessage = onmessage;
        ws.onclose = function () {
            if (chatID == null) {
                chatConnectionTries[otherUserID]++;
                if (chatConnectionTries[otherUserID] > 2) {
                    printError("Can't connect to chat server. Try to reload page or send email to site administration.");
                    console.log("Can't connect to chat server. Try to reload page or send email to site administration.");
                    client_list[ws['otherUserID']] = null;
                    toLastChatMessage();
                } else {
                    console.log("Connection is closing, trying to reconnect in 3 seconds");
                    setTimeout(function () {
                        client_list[ws['otherUserID']] = createChat(ws['otherUserID']);
                    }, 3000);
                }
            } else {
                if (video_list[otherUserID] != null && (video_list[otherUserID].readyState !== video_list[otherUserID].CLOSED && video_list[otherUserID].readyState !== video_list[otherUserID].CLOSING)) {
                    $("#stop-video").click();
                }
                client_list[ws['otherUserID']] = null;
                $("video-" + otherUserID).remove();
                $("#start-video, #stop-video").addClass('hidden');
            }
        };
        ws.onerror = function () {
            console.log("Some error occured");
            $("video-" + otherUserID).remove();
        };
        return ws;
    }

    function onopen() {
        // login
        chatWarnings[this.otherUserID] = [];
        var login_data = '{"type":"auth","data":{"user":{"id":' + this['otherUserID'] + '}, "token":"' + userToken + '"}}';
        console.log("websocket successful handshake, sending login data:" + login_data);
        this.send(login_data);

    }

    // when server sends a message
    function onmessage(e) {
        var data = JSON.parse(e.data);
        //console.log(data);       
        switch (data['type']) {
            // ping from client
            case 'ping':
                this.send('{"type":"pong", "data":{"token":"' + userToken + '"}}');
                break;
            // login to update user list
            case 'error':
                printError(data['data']['message']);
                chatID = data['data']['chatID'];
                break;
            case 'auth':
                break;
            case 'history':
                break;
            // sending information to client
            case 'message':
                break;
            case 'image':
                break;
            case 'videostart':
                video_list[data['data']['user']['id']] = data['data']['user'];
                break;
            case 'videostop':
                videoChatID = true;
                $('#video-' + data['data']['user']['id']).remove();
                stopMediaStream();
                if (video_list[data['data']['user']['id']] != null && typeof video_list[data['data']['user']['id']] != "undefined") {                    
                    video_list[data['data']['user']['id']] = null;
                }
                delete video_list[data['data']['user']['id']];
                break;
            // some user logout, update user list
            case 'logout':
                break;
            case 'warning':
                printWarning(data['data']['message']);                
                break;
            case 'close':                
                printWarning('VideoChat was closed. Please refresh page to start chating again.');                
                chatID = data['chatID'];
                this.close();
        }
    }

    function answerCall() {
        //check whether user can use webrtc and use that to determine the response to send
        if (checkUserMediaSupport) {
            //set media constraints based on the button clicked. Audio only should be initiated by default
            streamConstraints = {video: {facingMode: 'user'}, audio: false};

            //uncomment the lines below if you comment out the get request above
            startCall(true);
        }

        else {
            var otherUserID = $("#otherUserID").val();
            //inform caller and current user (i.e. receiver) that he cannot use webrtc, then dismiss modal after a while
            video_list[otherUserID].send(JSON.stringify({
                token: userToken,
                userReceiverId: video_list[otherUserID]['otherUserID'],
                data: {
                    action: 'callRejected',
                    msg: "Remote's device does not have the necessary requirements to make call",
                    room: video_list[otherUserID]['room']
                }
            }));

            console.log("Your browser/device does not meet the minimum requirements needed to make a call");

        }

    }

    function reconnectToRemoteUser(isCaller) {
        if (isCaller) {
            myPC.createOffer({iceRestart: true}).then(description, function (e) {
                console.log("Error creating offer", e.message);
            });

            //then notify callee to start call on his end
            video_list[otherUserID].send(JSON.stringify({
                token: userToken,
                userReceiverId: video_list[otherUserID]['otherUserID'],
                data: {
                    action: 'startCall',
                    room: video_list[otherUserID]['room']
                }
            }));
        }

        else {
            //myPC.createAnswer(description);
            myPC.createAnswer().then(description).catch(function (e) {
                console.log("Error creating answer", e);
            });

        }
    }

    function startCall(isCaller) {
        var otherUserID = $('#otherUserID').val();
        if (checkUserMediaSupport) {
            myPC = new RTCPeerConnection(servers);//RTCPeerconnection obj

            //When my ice candidates become available
            myPC.onicecandidate = function (e) {
                if (e.candidate) {
                    //send my candidate to peer
                    video_list[otherUserID].send(JSON.stringify({
                        token: userToken,
                        userReceiverId: video_list[otherUserID]['otherUserID'],
                        data: {
                            action: 'candidate',
                            candidate: e.candidate,
                            room: video_list[otherUserID]['room']
                        }
                    }));
                }
            };
            //When remote stream becomes available
            myPC.ontrack = function (e) {
                console.log(e.streams);
                document.getElementById('video-' + $("#otherUserID").val()).src = window.URL.createObjectURL(e.streams[0]);
            };

            //when remote connection state and ice agent is closed
            myPC.oniceconnectionstatechange = function () {
                if (myPC != null) {
                    switch (myPC.iceConnectionState) {
                        case 'disconnected':
                        case 'failed':
                            printWarning("Ice connection state is failed/disconnected. Call connection problem");
                            break;

                        case 'closed':
                            printWarning("Ice connection state is failed/disconnected. Call connection problem");
                            break;
                    }
                }
            };

            //WHEN REMOTE CLOSES CONNECTION
            myPC.onsignalingstatechange = function () {
                if (myPC != null) {
                    switch (myPC.signalingState) {
                        case 'closed':
                            printWarning("Signalling state is 'closed'. Signal lost");
                            break;
                    }
                }
            };
            //set local media
            $('#myVid').addClass('hidden');
            //setLocalMedia(streamConstraints, isCaller);
        } else {
            printWarning("Your browser does not support video call");
        }
    }

    function checkUserMediaSupport() {
        return !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
    }

    function connectToRemoteUser(isCaller) {
        if (isCaller) {
            myPC.createOffer().then(description, function (e) {
                console.log("Error creating offer", e.message);
            });

            //then notify callee to start call on his end
            video_list[otherUserID].send(JSON.stringify({
                token: userToken,
                userReceiverId: video_list[otherUserID]['otherUserID'],
                data: {
                    action: 'startCall',
                    room: video_list[otherUserID]['room']
                }
            }));
        }

        else {
            //myPC.createAnswer(description);
            myPC.createAnswer().then(description).catch(function (e) {
                console.log("Error creating answer", e);
            });

        }
    }

    //get and set local media
    function setLocalMedia(streamConstraints, isCaller) {
        var otherUserID = $('#otherUserID').val();
        navigator.mediaDevices.getUserMedia(
            streamConstraints
        ).then(function (myStream) {
            //myCamera.src = window.URL.createObjectURL(myStream);

            myPC.addStream(myStream);//add my stream to RTCPeerConnection

            //set var myMediaStream as the stream gotten. Will be used to remove stream later on
            myMediaStream = myStream;

            if (isCaller) {
                myPC.createOffer().then(description, function (e) {
                    console.log("Error creating offer", e.message);
                });

                //then notify callee to start call on his end
                video_list[otherUserID].send(JSON.stringify({
                    token: userToken,
                    userReceiverId: video_list[otherUserID]['otherUserID'],
                    data: {
                        action: 'startCall',
                        room: video_list[otherUserID]['room']
                    }
                }));
            }

            else {
                //myPC.createAnswer(description);

                myPC.createAnswer().then(description).catch(function (e) {
                    console.log("Error creating answer", e);
                });

            }

        }).catch(function (e) {

            switch (e.name) {
                case 'SecurityError':
                    console.log(e.message);
                    break;

                case 'NotAllowedError':
                    console.log(e.message);
                    break;

                case 'NotFoundError':
                    console.log(e.message);
                    break;

                case 'NotReadableError':
                case 'AbortError':
                    console.log(e.message);
                    break;
            }
        });
    }

    function description(desc) {
        var otherUserID = $('#otherUserID').val();
        myPC.setLocalDescription(desc);

        //send sdp
        video_list[otherUserID].send(JSON.stringify({
            token: userToken,
            userReceiverId: video_list[otherUserID]['otherUserID'],
            data: {
                action: 'sdp',
                sdp: desc,
                room: video_list[otherUserID]['room']
            }
        }));
    }

    function endCall(msg, setTimeOut) {
        var otherUserID = $('#otherUserID').val();
        if (video_list[otherUserID] != null && (video_list[otherUserID].readyState !== video_list[otherUserID].CONNECTING && video_list[otherUserID].readyState !== video_list[otherUserID].CLOSED && video_list[otherUserID].readyState !== video_list[otherUserID].CLOSING)) {
            video_list[otherUserID].send(JSON.stringify({
                token: userToken,
                userReceiverId: video_list[otherUserID]['otherUserID'],
                data: {
                    action: 'endCall',
                    msg: msg,
                    room: video_list[otherUserID]['room']
                }
            }));
            $('#video-' + $("#otherUserID").val()).remove();
        }
    }

    function handleCallTermination() {
        myPC = myPC ? myPC.close() : null;//close connection as well

        //tell user that remote terminated call
        console.log("Call terminated by remote");
        $("#otherUserID").val("");

        //remove streams and free media devices
        stopMediaStream();
        myMediaStream = null;
        //remove video playback src

        $('#video-' + $("#otherUserID").val()).remove();

        //enable 'call' button and disable 'terminate call' btn        
        $("#stop-video").addClass('hidden');

    }

    function stopMediaStream() {
        if (myMediaStream) {
            myMediaStream.getAudioTracks().forEach(function (track) {
                track.stop();
            });

            myMediaStream.getVideoTracks().forEach(function (track) {
                track.stop();
            });
        }
    }

    function videoConnect(otherUserID, room) {
        var videoWebSocket = new WebSocket("wss://" + webSocketServer + ":" + videoChatPort);
        videoWebSocket['otherUserID'] = otherUserID;
        videoWebSocket['room'] = room;
        videoWebSocket.onopen = function () {
            //subscribe to room
            videoWebSocket.send(JSON.stringify({
                token: userToken,
                userReceiverId: videoWebSocket['otherUserID'],
                data: {
                    action: 'subscribe',
                    room: room
                }
            }));
            console.log("Connected to the video server!");            
            if (client_list[otherUserID] != null && (client_list[otherUserID].readyState !== client_list[otherUserID].CLOSED && client_list[otherUserID].readyState !== client_list[otherUserID].CLOSING)) {
                client_list[otherUserID].send(JSON.stringify({
                    "type": "startVideoCall",
                    "data": {
                        "user": {
                            "id": client_list[otherUserID]['otherUserID']
                        },
                        "token": userToken,
                        "room": video_list[otherUserID]['room']
                    }
                }));
            }
        };
        videoWebSocket.onclose = function () {
            if (videoChatID == null) {
                videoConnectionTries++;
                if (videoConnectionTries > 2) {
                    console.log("Can't connect to chat server. Try to reload page or send email to site administration.");
                    printError("Can't connect to chat server. Try to reload page or send email to site administration.");
                    video_list[videoWebSocket['otherUserID']] = null;
                } else {
                    if (videoWebSocket['otherUserID'] != null && typeof videoWebSocket['otherUserID'] != "undefined") {
                        console.log("Connection is closing, trying to reconnect in 3 seconds");
                        setTimeout(function () {
                            endCall("Call ended by remote", false);
                            handleCallTermination();
                            var otherUserID = $('#otherUserID').val();
                            var room = getRoom();
                            streamConstraints = {video: {facingMode: 'user'}, audio: false};
                            if (client_list[otherUserID] != null && (client_list[otherUserID].readyState !== client_list[otherUserID].CLOSED && client_list[otherUserID].readyState !== client_list[otherUserID].CLOSING)) {
                                client_list[otherUserID].send(JSON.stringify({
                                    "type": "startVideoCall",
                                    "data": {
                                        "user": {
                                            "id": client_list[otherUserID]['otherUserID']
                                        },
                                        "token": userToken,
                                        "room": room
                                    }
                                }));
                            }
                            video_list[otherUserID] = videoConnect(videoWebSocket['otherUserID'], room);
                            $(".web-cam").remove('#video-' + $('#otherUserID').val());
                            $(".web-cam").append($("<video class='img-responsive' id='video-" + $('#otherUserID').val() + "' autoplay></video>"));                            
                        }, 3000);
                    }
                }
            }
        };
        videoWebSocket.onerror = function () {
            console.log("Unable to connect to the video server!");
            $("#otherUserID").val("");
        };
        videoWebSocket.onmessage = function (e) {
            var data = JSON.parse(e.data);
            console.log(data);
            if (data.room === room) {
                //above check is not necessary since all messages coming to this user are for the user's current room
                //but just to be on the safe side
                switch (data.action) {
                    case 'callRejected':
                        console.log(data.msg);
                        // video chat rejected by man                            

                        break;

                    case 'error':
                        printError(data['data']['message']);
                        videoChatID = true;                        
                        break;
                    case 'endCall':
                        //i.e. when the caller ends the call from his end (after dialing and before recipient respond)
                        //End call                        
                        console.log(data.msg);
                        video_list[videoWebSocket['otherUserID']] = null;
                        videoWebSocket.close();
                        handleCallTermination();
                        break;

                    case 'startCall':
                        document.getElementById('video-' + $("#otherUserID").val()).src = '';
                        startCall(false);//to start call when callee gives the go ahead (i.e. answers call)

                        break;

                    case 'candidate':
                        //message is iceCandidate
                        myPC ? myPC.addIceCandidate(new RTCIceCandidate(data.candidate)) : "";

                        break;
                    case 'restartCall':
                        console.log('restart');
                        break;
                    case 'sdp':
                        //message is signal description
                        myPC ? myPC.setRemoteDescription(new RTCSessionDescription(data.sdp)) : "";
                        connectToRemoteUser(false);

                        break;
                    case 'terminateCall'://when remote terminates call (while call is ongoing)
                        handleCallTermination();
                        break;
                    case 'newSub':
                        //once the other user joined and current user has been notified, current user should also send a signal
                        //that he is online
                        videoWebSocket.send(JSON.stringify({
                            token: userToken,
                            userReceiverId: videoWebSocket['otherUserID'],
                            data: {
                                action: 'imOnline',
                                room: videoWebSocket['room']
                            }
                        }));
                        break;
                    case 'imOnline':
                        videoWebSocket.send(JSON.stringify({
                            token: userToken,
                            userReceiverId: videoWebSocket['otherUserID'],
                            data: {
                                action: 'imOnline',
                                room: videoWebSocket['room']
                            }
                        }));
                        break;
                    case 'imOffline':
                        break;
                }
            }

            else if (data.action === "subRejected") {
                //subscription on this device rejected cos user has subscribed on another device/browser
                console.log("Maximum of two users allowed in room. Communication disallowed");
            }
        };
        return videoWebSocket;
    }

    $("body").on("click", ".start-video", function () {
        var otherUserID = $(this).data('id');
        var currentUser = $("#otherUserID").val();
        var room = getRoom();
        if (otherUserID != null && otherUserID != '') {
        	if (client_list[otherUserID] == null || typeof client_list[otherUserID] === "undefined") {
	            client_list[otherUserID] = createChat(otherUserID);
            	chatConnectionTries[otherUserID] = 0;
        	}            
            streamConstraints = {video: {facingMode: 'user'}, audio: false};
            video_list[otherUserID] = videoConnect(otherUserID, room);
            first = true;
            $(".web-cam").remove('#video-' + otherUserID);
            $(".web-cam").append($("<video class='img-responsive' id='video-" + otherUserID + "' autoplay></video>"));
            $("#otherUserID").val(otherUserID);
/*            if (video_list[currentUser] != null && typeof video_list[currentUser] != "undefined") {
                video_list[currentUser].close();
                video_list[currentUser] = null;
            }*/
        } 
    });
    $("body").on("click", ".stop-video", function () {
        var otherUserID = $(this).data('id');
        videoChatID = true;
        videoBuffer = [];
        $("[id^=video]").remove();
        endCall("Call ended by remote", false);
        handleCallTermination();
        $("#otherUserID").val("");
        if (video_list[otherUserID] != null && typeof video_list[otherUserID] != "undefined") {
           video_list[otherUserID].close();
        }
        
    });


    function printError(message) {
        $('#modalMessage, #layout').show();
        $('#modalMessage .modal-body').html('');
        $('#modalMessage .modal-body').append('<span>');
        $('#modalMessage .modal-body span').text(message);
        $("#otherUserID").val("");
    }

    function printWarning(message) {
        $('#modalMessage, #layout').show();
        $('#modalMessage .modal-body').html('');
        $('#modalMessage .modal-body').append('<span>');        
        $('#modalMessage .modal-body span').text(message);
        $("#otherUserID").val("");
    }

    function buildVideoList(currentUser) {
        var camList = $('.web-cam');
        camList.empty();
        for (var p in video_list) {
            if (p != currentUser) {
                camList.append('<p>' + video_list[p]['username'] + ' Web Cam</p><div><img class="img-responsive" id="img' + p + '" /></div>');
            }
        }
    }    

    window.onbeforeunload = function (e) {
        closeAll();
    };

    function closeAll()
    {
        $('#otherUserID').val("");
        chatID = true;
        videoChatID = true;
        var i;
        for (i in client_list) {
            if (client_list[i] != null) {
                client_list[i].close();
            }
        }
        for (i in video_list) {
            if (video_list[i] != null) {
                endCall("Call ended by remote", false);
                handleCallTermination();
                video_list[i].close();
            }
        }
    }

    $(document).ready(function () {
        $("#agency-select1").on('change', function() {

        });
    });



})(jQuery);