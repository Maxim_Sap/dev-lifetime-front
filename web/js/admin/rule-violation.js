(function($) {
'use strict';
var userToken = window["commonAdminLibrary"]["userToken"];
var apiServer = window["commonAdminLibrary"]["apiServer"];

$('.delete-rule').on('click', function(event){
	event.preventDefault();
	var prompt = confirm('Are you sure that you want to delete this rule violation?');
	if (prompt) {
		var ruleID = $('#ruleID').val();
		$.ajax({
			url: apiServer+'/v1/rule-violation/' + ruleID,
			type: "DELETE",
			dataType: "json",
	        headers: {
	            "Authorization": "Bearer " + userToken
	        },
	        statusCode: {
	        	204: function() {
	        		$('#modalMessage,#layout').show();
					$('#modalMessage .modal-body span').text('Правило успешно удалено');
					setTimeout(function() {window.history.back()}, 1500);
	        	},
	        	401: function(xhr) {
	        		$('#modalMessage,#layout').show();
					$('#modalMessage .modal-body span').text(xhr.responseText);
	        	},
	        	404: function(xhr) {
  	        		$('#modalMessage,#layout').show();
					$('#modalMessage .modal-body span').text(xhr.responseText);
			    },
	        	500: function(xhr) {
	        		$('#modalMessage,#layout').show();
					$('#modalMessage .modal-body span').text(xhr.responseText);
	        	}
	        },
			success: function (response) {		
			},
		});		
	} else {
		return false;
	}
});
$('.update-rule').on('click', function(event){
	event.preventDefault();
	var ruleID = $('#ruleID').val();
	$.ajax({
		url: apiServer+'/v1/rule-violation/' + ruleID,
		type: "PUT",
		dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
        	'description': $('#violation-description').val()
        },
        statusCode: {
        	200: function() {
        		$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text('Правило успешно обновлено');
				setTimeout(function() {window.location.reload(true)}, 1500);
        	},
        	401: function(xhr) {
        		$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(xhr.responseText);
        	},
        	404: function(xhr) {
        		$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(xhr.responseText);
		    },
        	500: function(xhr) {
        		$('#modalMessage,#layout').show();
				$('#modalMessage .modal-body span').text(xhr.responseText);
        	}
        },
		success: function (response) {		
		},
	});		
});


})(jQuery);