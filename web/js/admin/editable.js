(function($) {
'use strict';
var userToken = window["commonAdminLibrary"]["userToken"];
var apiServer = window["commonAdminLibrary"]["apiServer"];
$('input[type="text"], input[type="email"], select, textarea').prop('disabled', true).prop('readonly', true);

$('input[type="text"], input[type="email"], select, textarea').after('<span class="icon-element icon-pencil"><i class="icon-button fa fa-pencil"></i></span><span class="hide icon-element icon-times"><i class="icon-button fa fa-times"></i></span>');

$('body').on('click', '.icon-pencil', function() {	
	$('input[type="text"], input[type="email"], select, textarea').prop('disabled', true).prop('readonly', true);
	$('.icon-pencil').removeClass('hide');
	$('.icon-times').addClass('hide');
	$(this).parent().find('input[type="text"], input[type="email"], select, textarea').prop('disabled', false).prop('readonly', false).css('backgroundColor', 'white').focus();
	$(this).parent().find('.icon-times').removeClass('hide');
	$(this).addClass('hide');
});

$('body').on('click', '.icon-times', function() {	
	$('input[type="text"], input[type="email"], select, textarea').prop('disabled', true).prop('readonly', true).css('backgroundColor', '#f8f8f8');
	$('.icon-pencil').removeClass('hide');
	$('.icon-times').addClass('hide');	
});


})(jQuery);