var mailingTemplatesLibrary = (function($) {
'use strict';

var userID = window["commonLibrary"]["userID"];
var userToken = window["commonLibrary"]["userToken"];
var apiServer = window["commonLibrary"]["apiServer"];
var userType = window["commonLibrary"]["userType"];
var REPETITION_TIME_TEMPLATES = 40000;
var REPETITION_TIME_INFORMATION = 40000;


$(document).ready(function() {
   
	mailingTemplates(true);
	mailingInformation(true);	
    	
});

function mailingTemplates(repeat)
{
	$.ajax({
		url: apiServer+'/v1/mailing-template/chat-templates',
		dataType: "json",
		type: "GET",	
        headers: {
            "Authorization": "Bearer " + userToken
        },	
		success: function (response) {
			if (response.success == true) {
				if (response.templates.length == 0) {
					$('.mailing-tabs .tab-panel:first .tab-panel-content').html("<div><p>You don't have previously created templates.</p><p>You can create a new one using 'New Template' tab.</p></div>");
				} else {									
					var html = '<form class="existing-template-form">' +
                            '<select name="templates" id="mailing-templates">'+
                            '<option value="">Select a template</option>';
                    for (var i = 0; i <= response.templates.length - 1; i++) {
                    	var status = '';
						if (response.templates[i].status == 3) {
							status = ' - templete declined';
						} else if (response.templates[i].status == 2) {
							status = ' - verification in progress';
						} else if (response.templates[i].status == 1) {
							status = ' - template not verified';
						}
                    	html += '<option value="' + response.templates[i].id + '"' + 
                    			((response.templates[i].status != 4) ? 'disabled="disabled"' : '') + '>'+
                    			response.templates[i].title + status + "</option>";
                    }
                    html += '</select>' +
                            '<textarea disabled="disabled" name="letter-text" placeholder="Template text"></textarea>' +
                            	'<div class="checkbox-group">' +
                                    '<input type="checkbox" name="blacklist" id="black-exclude">' +
                                    '<span class="theme-checkbox"></span>' +
                                    '<label for="black-exclude">except blacklist</label>' +
                                  '</div>' +
                            '<button type="submit" class="btn start-mailing">start a newsletter</button>' +
                        '</form>';
					$('.mailing-tabs .tab-panel:first .tab-panel-content').html(html);
				}
			} 
		},
		error: function (xhr, ajaxOptions, thrownError) {
			
		}
	});
	if (repeat) {
		setTimeout(function() {
			mailingTemplates(true);
		}, REPETITION_TIME_TEMPLATES);
	}

}

function mailingInformation(repeat)
{
	$.ajax({
		url: apiServer+'/v1/mailing-session/chat-session',
		dataType: "json",
		type: "GET",	
        headers: {
            "Authorization": "Bearer " + userToken
        },	
        success: function (response) {
        	if (response.success) {
        		var html = "";
        		html = '<h4 class="mailing-proggress-title">mailing progress</h4>';
        		html += '<div class="mailing-proggress-info-item">' +
        					'<p>Mailing session '+ ((response.paused) ? 'was paused' : 'is running') +'</p>' +
                            '<span class="title">Begin:</span>' +
                            '<span class="description">' + transformTimestamp(response.started_at) + '</span>' +
                        '</div>';
                html += '<div class="mailing-proggress-info-item">' +
                            '<span class="title">Received:</span>' +
                            '<span class="description"><span>' + response.sended_messages + '</span> of <span> ' + response.total_messages + ' </span> people</span>' +
                          '</div>';
                if (response.sended_messages == 0 && response.total_messages == 0) {
                	html += '<p>This template was already send to those users.</p>';
                }                          
                html += '<a href="javascript:void(0);" data-id="' + response.id +'" class="btn play-pause">Pause / Resume</a> <br>' +
                		'<a href="javascript:void(0);" data-id="' + response.id +'" class="btn stop">Stop</a>';
        		$(".mailing-proggress").html(html);
        	} else {
        		$(".mailing-proggress").html("<h4>" + response.message + "</h4>");
        	}        	
        },
        error: function (xhr, ajaxOptions, thrownError) {
			
		}
	});
	if (repeat) {
		setTimeout(function() {
			mailingInformation(true);
		}, REPETITION_TIME_INFORMATION);
	}	
}

function transformTimestamp(timestamp) {
    try {
        var currentDateTime = new Date(timestamp);
        if (currentDateTime != 'Invalid Date') {
            var currentDate = (new Date(currentDateTime - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, 10).replace(/[^0-9]/g, "-");
            var currentTime = (new Date(currentDateTime - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(11, 16).replace(/[^0-9]/g, ":");
            return currentDate + ' ' + currentTime;
        } else {
            return timestamp;
        }
    } catch (error) {
        return timestamp;
    }
}

$('body').on('change', '#mailing-templates', function() {
	var templateId = $(this).val();
	if (templateId == '' || templateId == null) {
		alert('Please select a template');
	} else {
		$.ajax({
			url: apiServer+'/v1/mailing-template/chat-template/' + templateId,
			dataType: "json",
			type: "GET",	
	        headers: {
	            "Authorization": "Bearer " + userToken
	        },	
			success: function (response) {
				if (response.success == true) {
					if (response.template.body != '') {
						$('.mailing-tabs .tab-panel:first .tab-panel-content textarea')[0].innerHTML = response.template.body;
					}
				} else {					
                  	$('#modalMessage .modal-body span').text(response.message);
                  	$('#modalMessage').modal('show');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				
			}
		});  
	}
});

$('body').on('click', '.add-template', function(event) {	
	event.preventDefault();
	$.ajax({
		url: apiServer+'/v1/mailing-template/chat-template',
		dataType: "json",
		type: "POST",
		data: {
			'title': $('input[name="template-name"]').val(),
			'body': $('.new-mailing-template-form textarea[name="letter-text"]').val()
		},
        headers: {
            "Authorization": "Bearer " + userToken
        },	
		success: function (response) {
			if (response.success == true) {
				if (response.id != '' && response.id != null) {
					$('.new-mailing-template-form')[0].reset();
              		$('#modalMessage .modal-body span').text("Template was created and sended for approve.");
              		$('#modalMessage').modal('show');
				}
			} else {				
              	$('#modalMessage .modal-body span').text(response.message);
              	$('#modalMessage').modal('show');
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			
		}
	});
});

$('body').on('click', '.start-mailing', function(event) {
	event.preventDefault();
	var templateID = $('#mailing-templates').val();
	if (templateID == '') {
		alert('Select a template');
		return;
	}
	$.ajax({
		url: apiServer+'/v1/mailing-session/start-chat-mailing',
		dataType: "json",
		type: "POST",
		data: {
			'templateID': templateID,
			'blacklist': $('#black-exclude').prop('checked') ? 1 : 0
		},
        headers: {
            "Authorization": "Bearer " + userToken
        },	
		success: function (response) {
			if (response.success == true) {		
				console.log(response);		
				if (response.id != '' && response.id != null) {
              		$('#modalMessage .modal-body span').text("Mailing was successfully started.");
              		$('#modalMessage').modal('show');
              		mailingInformation(false);
				}
			} else {				
              	$('#modalMessage .modal-body span').text(response.message);
              	$('#modalMessage').modal('show');
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			
		}
	});	
});

$('body').on('click', '.play-pause', function(event) {
	event.preventDefault();
	var sessionID = $(this).data('id');
	var confirmMessage = "Are you sure want to pause/resume mailing session?";
	if (confirm(confirmMessage)) {
		$.ajax({
			url: apiServer+'/v1/mailing-session/change-chat-session-status',
			dataType: "json",
			type: "POST",
			data: {
				'session_id': sessionID			
			},
	        headers: {
	            "Authorization": "Bearer " + userToken
	        },	
			success: function (response) {
				if (response.success == true) {								
	          		$('#modalMessage .modal-body span').text(response.message);
	          		$('#modalMessage').modal('show');
	          		mailingInformation(false);				
				} else {				
	              	$('#modalMessage .modal-body span').text(response.message);
	              	$('#modalMessage').modal('show');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				
			}
		});
	}
});

$('body').on('click', '.stop', function(event) {
	event.preventDefault();
	var sessionID = $(this).data('id');
	var confirmMessage = "Are you sure want to stop mailing session?";
	if (confirm(confirmMessage)) {
		$.ajax({
			url: apiServer+'/v1/mailing-session/close-chat-session',
			dataType: "json",
			type: "POST",
			data: {
				'session_id': sessionID			
			},
	        headers: {
	            "Authorization": "Bearer " + userToken
	        },	
			success: function (response) {
				if (response.success == true) {								
	          		$('#modalMessage .modal-body span').text(response.message);
	          		$('#modalMessage').modal('show');
	          		mailingInformation(false);				
				} else {				
	              	$('#modalMessage .modal-body span').text(response.message);
	              	$('#modalMessage').modal('show');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				
			}
		});
	}
});


})(jQuery);