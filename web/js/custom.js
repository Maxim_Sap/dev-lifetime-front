function tabs(parent, tab, tabpanel) {

	/*

	$(parent).find($(tabpanel)).not(":first").css({

		'visibility' : 'hidden',

		'opacity' : '0',

	});

	$(parent).find($(tab)).click(function() {

		if($(this).hasClass('active')) {

			return false;

		}

		$(parent).find($(tab)).removeClass("active").eq($(this).index()).addClass("active");

		$(parent).find($(tabpanel)).css({'visibility': 'hidden', 'opacity': '0'}).removeClass("active").eq($(this).index()).css({'visibility': 'visible', 'opacity': '1'}).addClass("active");

		$(parent).find($(tabpanel)).removeClass("active").eq($(this).index()).addClass("active");

		$('.tab-panel-container').height($('.tab-panel.active').height());

	}).eq(0).addClass("active");*/

	$(parent).find($(tabpanel)).not(":first").css({

		'visibility' : 'hidden',

		'opacity' : '0',

	});



	if($('#gifts_content').length > 0){

		$(parent).find($(tab)).click(function() {

			if($(this).hasClass('active')) {

				return false;

			}

			$(parent).find($(tab)).removeClass("active").eq($(this).index()).addClass("active");

			//$(parent).find($(tabpanel)).css({'visibility': 'hidden', 'opacity': '0'}).removeClass("active").eq($(this).index()).css({'visibility': 'visible', 'opacity': '1'}).addClass("active");

			$(parent).find($(tabpanel)).removeClass("active").eq($(this).index()).addClass("active");

			$('.tab-panel-container').height($('.tab-panel.active').height());

			window["accountLibrary"]["giftPagination"](1);

		});

	}else{

		$(parent).find($(tab)).click(function() {

			if($(this).hasClass('active')) {

				return false;

			}

			$(parent).find($(tab)).removeClass("active").eq($(this).index()).addClass("active");

			$(parent).find($(tabpanel)).css({'visibility': 'hidden', 'opacity': '0'}).removeClass("active").eq($(this).index()).css({'visibility': 'visible', 'opacity': '1'}).addClass("active");

			$('.tab-panel-container').height($('.tab-panel.active').height());

		}).eq(0).addClass("active");

	}

}



function equalHeight(element) {

	var maxHeightTabBlock = 0;

	$(element).outerHeight('');

	$(element).each(function() {

		if ($(this).outerHeight() > maxHeightTabBlock) {

			maxHeightTabBlock = $(this).outerHeight();

		}

		return maxHeightTabBlock;

	});

	$(element).outerHeight(maxHeightTabBlock);

}



function gift_optimization(){

	tabs('.gift-tabs', '.tab', '.tab-panel');



	if($(window).width() > 767) {

		equalHeight('.gift-item .gift-item-title');

	}





	if($(window).width() > 1024) {

		$('.gift-item').hover(function() {

			$(this).stop().toggleClass('active');

		});

	}



	if($(window).width() < 1025) {

		$('.gift-item').on('click', function() {

			$(this).stop().toggleClass('active');

		});



		$(document).on('click touchstart', function (event) {

			if (!$(event.target).closest('.gift-item').length) {

				$('.gift-item').removeClass('active');

			}

		});

	}

	$('.tab-panel-container').height($('.tab-panel.active').height());

}



(function( $ ) {





	/*document ready*/

	$(document).ready(function(){


		/* ---------- message-feed ---------- */

		var $this;
		$('body').on('click', '#messages-feed .message-item .close', function() {
			$this = $(this);
			$this.parent('.message-item').fadeOut(400);
			setTimeout(function() {
				$this.parent('.message-item').remove();
			}, 400);
		});

		for(var i = 0, j = 0; i < $('#messages-feed .message-item').length; i++, j+= 800) {

			$('#messages-feed .message-item').each(function() {
				var $thisItem = $(this);
				if($thisItem.index() === i) {
					setTimeout(function() {
						$thisItem.fadeIn(400);
						if($thisItem.index() > 2) {
							$thisItem.prev().fadeOut(400);
							$thisItem.prev().remove();
						}
					}, j);
				}
			});
		}


		setTimeout(function() {

			for(var i = 0, j = 0; i < $('#messages-feed .message-item').length; i++, j+= 800) {

				$('#messages-feed .message-item').each(function() {
					var $thisItem = $(this);
					if($thisItem.index() === i) {
						setTimeout(function() {
							$thisItem.fadeOut(400);
							$thisItem.remove();
						}, j);
					}
				});
			}

		}, 8000);





		/* ---------- for IE ---------- */

		if(Function('/*@cc_on return document.documentMode===10@*/')()){ $("body").addClass("ie10"); }





		/* ---------- global content wrapper ---------- */

		if ($('.main-header').outerHeight() != null && $('.main-footer').outerHeight() != null) {

			$('.global-content-wrapper').css('min-height', $(window).height() - ($('.main-header').outerHeight() + $('.main-footer').outerHeight()));

		}

		





		/* ---------- forms---------- */



		$('input[type="checkbox"]').parent('label').append('<span class="theme-checkbox">');

		$('input[type="radio"]').parent('label').append('<span class="theme-radio">');



		/* ---------- wrap form elements with icons ---------- */



		$('form input, form select, form textarea').each(function() {

			var data = $(this).attr('data-type');

			if($(this).attr('data-type')) {

				$(this).wrap('<div>');

				$(this).parent().addClass('form-item-wrapper').addClass(data);

			}

			

		});





		/* ---------- ranges ---------- */



		$('.range-age').ionRangeSlider({

			type: 'double',

			min: 18,

			max: 60,

			from: 18,

			to: 60,

			hide_min_max: true,

    	grid: false

		});



		$('.range-height').ionRangeSlider({

			type: 'double',

			min: 140,

			max: 240,

			from: 140,

			to: 240,

			hide_min_max: true,

    	grid: false

		});





		/* ---------- person ---------- */



/*		$('.person-profile-gallery-intro').each(function() {

			$(this).find('.like').on('click', function() {

				$(this).stop().toggleClass('liked');

			});

		});



		$('.person').each(function() {

			$(this).find('.like').on('click', function() {

				$(this).stop().toggleClass('liked');

			});

		});*/

		/* ---------- Header ---------- */

		/* ---------- main menu ---------- */

		/*$('.main-menu li .sub-menu li ul.sub-menu').attr('class', 'sub-sub-menu');

		$('.mobile-menu li .sub-menu li ul.sub-menu').attr('class', 'sub-sub-menu');*/



		/*$('.main-menu li').hover(function() {

			$(this).find('.sub-menu').stop().fadeIn(300);

		}, function() {

			$(this).find('.sub-menu').stop().fadeOut(300);

		});



		$('.main-menu .sub-menu li').hover(function() {

			$(this).find('.sub-sub-menu').stop().fadeIn(300);

		}, function() {

			$(this).find('.sub-sub-menu').stop().fadeOut(300);

		});*/

		/* ---------- sticky ---------- */

		var lastScrollTop = $(window).scrollTop();

		/*if(lastScrollTop > $('.main-header').outerHeight()) {

			$('.main-header').css('top', '0px').addClass('sticky');

			$('.hamb-wrapper').css('top', '0px');

		}



		$(window).on('scroll', function(){

			var st = $(this).scrollTop();

			if (st > lastScrollTop && st > $('.main-header').outerHeight()){ 

				$('.main-header').css('top', -1*($('.main-header').outerHeight()));

				$('.hamb-wrapper').css('top', -1*($('.main-header').outerHeight()));

				setTimeout(function() {

					$('.main-header').addClass('sticky');

				}, 300);

			}



			else if (st == 0) {

				$('.main-header').removeClass('sticky');

			}



			else {

				$('.main-header').css('top', '0px');

				$('.hamb-wrapper').css('top', '0px');

			}



			lastScrollTop = st;

		});*/

		/* ---------- hamb ---------- */



		$('.mobile-menu-button').on('click', function() {

			$(this).stop().toggleClass('menu-opened');

			$('.hamb-wrapper').stop().toggleClass('top-fixed');

			$('.mobile-nav, .system-nav').stop().toggleClass('nav-show');

			// $('.mobile-menu .sub-menu').slideUp(400);

			// $('.mobile-menu .sub-sub-menu').slideUp(400);

			// $('.mobile-menu .has-arrow').removeClass('drop');

			$('.overlay').stop().toggleClass('over');



			if(lastScrollTop > $('.main-header').outerHeight()) {

				$('.main-header').css('top', '0px').addClass('sticky');

				$('.hamb-wrapper').css('top', '0px');

			}

		});





		/* ---------- overlay ---------- */



		$('.overlay').on('click', function() {

			$(this).removeClass('over');

			$('.mobile-menu-button').removeClass('menu-opened');

			$('.hamb-wrapper').removeClass('top-fixed');

			$('.mobile-nav, .system-nav').removeClass('nav-show');

			$('.mobile-menu .sub-menu').slideUp(400);

			$('.mobile-menu .sub-sub-menu').slideUp(400);

			$('.mobile-menu .has-arrow').removeClass('drop');

		});







		/* ---------- operators list ---------- */



		if($(window).width() > 1024) {

			$('.operator').hover(function() {

				$('.operator-list').stop().fadeToggle(400);

			});

		}



		if($(window).width() < 1025) {

			$('.operator').on('click', function(e) {

				e.preventDefault();

				$('.operator-list').stop().slideToggle(400);

				return false;

			});

		}

		





		/* ---------- mobile menu ---------- */



		$('.mobile-menu li a').wrapInner('<span>');

		$('.mobile-menu .sub-menu, .mobile-menu .sub-sub-menu').parent('li').children('a').addClass('has-arrow');



		$('.mobile-menu').css('min-height', $(window).height() - ($('.menu-logo').outerHeight() + $('.mobile-nav > .socials').outerHeight()));



		$('.mobile-menu > li > a').on('click', function() {

			$('.mobile-menu .sub-menu').slideUp(400);

			$('.mobile-menu .sub-sub-menu').slideUp(400);

			$(this).parent().find('.sub-menu').stop().slideToggle(400);

		});



		$('.mobile-menu .has-arrow').on('click', function() {

			$(this).parent().parent().find('.has-arrow').not($(this)).removeClass('drop');

			$(this).stop().toggleClass('drop');

		});



		$('.mobile-menu .sub-menu > li > a').on('click', function() {

			$('.mobile-menu .sub-sub-menu').slideUp(400);

			$(this).parent().find('.sub-sub-menu').stop().slideToggle(400);

		});





		/* ---------- intro slider ---------- */



		// On after slider first initialization.

		$('.basic-slider').on('init', function(event, slick, currentSlide, nextSlide){

			$('.slider-preloader').addClass('slider-loaded');

			$('.slider-wrapper').css('height', 'auto');

		});



		// inititalize slider



		$('.intro-slider').slick({

			slidesToShow: 1,

			slidesToScroll: 1,

			speed: 1000,

			swipe: true,

			arrows: true,

			dots: true,

			infinite: true,

			fade: true,

			autoplay: true,

			autoplaySpeed: 5000,

			prevArrow: '<button class="prev-slide"><span class="control-icon prev-icon"></span></button>',

			nextArrow: '<button class="next-slide"><span class="control-icon next-icon"></span></button>'

		});



		$('.basic-slider .slick-dots li button').text('');





		/* ---------- intro content tabs ---------- */



		tabs('.home-intro-content-tabs', '.tab', '.tab-panel');





		/* ---------- gallery slider ---------- */



		// inititalize slider



		$('.gallery-slider').slick({

			slidesToShow: 6,

			slidesToScroll: 1,

			speed: 300,

			swipe: true,

			arrows: true,

			dots: false,

			infinite: true,

			fade: false,

			/*autoplay: true,

			autoplaySpeed: 7000,*/

			prevArrow: '<button class="prev-slide"><span class="control-icon prev-icon"></span></button>',

			nextArrow: '<button class="next-slide"><span class="control-icon next-icon"></span></button>',

			responsive: [

			{

				breakpoint: 1025,

				settings: {

					slidesToShow: 5

				}

			},

			{

				breakpoint: 991,

				settings: {

					slidesToShow: 4

				}

			},

			{

				breakpoint: 768,

				settings: {

					slidesToShow: 1,

					centerMode: true,

        	centerPadding: '30px'

				}

			}

			]

		});





		/* ---------- gallery tabs ---------- */



		tabs('.gallery-tabs', '.tab', '.tab-panel');



		$('.gallery-tabs .tab-panel-container').css('min-height', $('.gallery-tabs .tab-panel').height());





		if($(window).width() < 1025) {



			$('.person').on('click',function() {

				$('.person').not($(this)).removeClass('person-active');

				$(this).toggleClass('person-active');

			});



			$(document).on('click touchstart', function (event) {

				if (!$(event.target).closest('.person').length) {

					$('.person').removeClass('person-active');

				}

			});



		}





		if($(window).width() < 768) {

			$('.main').css('margin-top', $('.main-header').outerHeight());

		}





		$('.button-list li .btn').on('click', function() {

			$(this).parents('.button-list').find('.btn').removeClass('active').addClass('btn-disabled');

			$(this).removeClass('btn-disabled').addClass('active').addClass('btn');

		});





		$('.color-palette input[type="radio"], .color-palette input[type="checkbox"]').wrap('<label class="color-item">');

		$('.color-palette .color-item input[type="radio"], .color-palette input[type="checkbox"]').each(function() {

			var checkboxColor = $(this).attr('data-color');

			$(this).parent('.color-item').css({

				'backgroundColor': checkboxColor

			});

		});



		$('.color-palette input[type="checkbox"]').on('click', function() {				

			$(this).parent().toggleClass('color-active');

		});



		



		/* ---------- profile tabs ---------- */



		tabs('.person-profile-gallery-tabs', '.tab', '.tab-panel');



		/* ---------- profile photo gallery ---------- */



    $('.tab-panel-container').height($('.tab-panel.active').height());







    /* ---------- search ---------- */



    /*$('#country').on('change', function() {

    	var thisText = $(this).find("option:selected").text();

    	$('.country-list').append('<span class="country-selected">');

    	$('.country-selected').each(function() {

    		if($(this).is(':empty')) {

    			$(this).text(thisvalue).append('<i class="fa fa-times" aria-hidden="true"></i>');

    		}

    	});

    	$('.country-selected .fa').on('click',function() {

    		$(this).parent().fadeOut(200).remove();

    	});

    });*/



		$('#country').select2({

			placeholder: "Select"

		});



		var selectPlaceholder = $('#country').attr('data-placeholder');

		$('#country').siblings('.select2-container').append('<span class="select2-placeholder">');

		$('.select2-placeholder').text(selectPlaceholder);



		$('#country').on('change', function() {

			$('.country-select').height($('.select2-selection').height() + $('.select2-selection__rendered').height());

		});







		/* ---------- reset results ---------- */



		var ageSlider = $('.range-age').data("ionRangeSlider");

		var heightSlider = $('.range-height').data("ionRangeSlider");



		$('.reset-all').on('click', function() {

			$('.color-palette .color-item').removeClass('color-active');

			$('.country-select').css('height', 'auto');

			ageSlider.reset();

			heightSlider.reset();

			$('#switch_right, #switch_2, #switch_4').prop('checked', true);			

		});



		$("select").closest("form").on("reset",function(ev){

			var targetJQForm = $(ev.target);

			setTimeout((function(){

				this.find("select").trigger("change");

			}).bind(targetJQForm),0);

		});





    /* ---------- photo ---------- */



    $('[data-fancybox]').fancybox({

    	speed: 30,

    	touch: false

    });





    /* ---------- video ---------- */



    $(".person-profile-gallery-item-video").fancybox({

    	speed: 30,

    	afterShow: function() {

    		this.content.find('video').trigger('play');

    		this.content.find('video').on('ended', function() {

    			$.fancybox.next();

    		});

    	}

    });





    /* ---------- chat ---------- */



	$('.hide-show-info').on('click', function() {

    	$(this).stop().toggleClass('show-toggle');

    	$('.interlocutor-topline').stop().slideToggle(800);

    	$('.interlocutor-content-wrapper').stop().slideToggle(800);

    });



	$('.sound-trigger-wrapper').on('click', function() {

    	$(this).stop().toggleClass('sound-enabled').stop().toggleClass('sound-disabled');

    });

		if($(window).width() < 1025) {



			$('.person-online').on('click', function() {

				$('.person-online').removeClass('selected');

				$(this).addClass('selected');

			});



			$(document).on('click touchstart', function (event) {

				if (!$(event.target).closest('.person-online').length) {

					$('.person-online').removeClass('selected');

				}

			});

		}





		if ($(window).width() < 768) {

			$('.action-button').on('click', function () {

				$(this).stop().toggleClass('active');

				$('.chat-top-line-action-list').stop().slideToggle(300);

			});



			$(document).on('click touchstart', function (event) {

				if (!$(event.target).closest('.action-button, .chat-top-line-action-list').length) {

					$('.chat-top-line-action-list').slideUp(300);

					$('.action-button').removeClass('active');

				}

			});

		}





		$('.left-toggle-button').on('click', function() {

			$('.right-toggle-button').removeClass('active');

			$('.sidebar-right-wrapper').removeClass('side-collapse');

			$(this).stop().toggleClass('active');

			$(this).parent('.sidebar-left-wrapper').stop().toggleClass('side-collapse');

		});



		$('.right-toggle-button').on('click', function() {

			$('.left-toggle-button').removeClass('active');

			$('.sidebar-left-wrapper').removeClass('side-collapse');

			$(this).stop().toggleClass('active');

			$(this).parent('.sidebar-right-wrapper').stop().toggleClass('side-collapse');

		});



		// $('.chat-box-messages-wrapper').scrollTop($('.chat-box-messages-wrapper')[0].scrollHeight);

		if($(".chat-message-container").length) {			

			var lastMessage = $(".chat-message-container").last().offset().top;			

			$('.ss-content').animate({

				scrollTop: lastMessage

			});

		}

    		/* ---------- gifts tabs ---------- */



		gift_optimization();



		/* ---------- my profile ---------- */



		$('.has-action-submenu').on('click', function() {

			$(this).stop().toggleClass('sub-show');

			$('.main-action-submenu').slideUp(300);

			$(this).children('.main-action-submenu').stop().slideToggle(300);

		});



		$(document).on('click touchstart', function (event) {

			if (!$(event.target).closest('.has-action-submenu, .main-action-submenu').length) {

				$('.main-action-submenu').slideUp(300);

				$('.has-action-submenu').removeClass('sub-show');

			}

		});





		/* ---------- edit profile ---------- */



		$('.field-left, .field-right').css('min-height', $('input').height());



		$('.field-left').wrapAll('<div class="form-left-side">');

		$('.field-right').wrapAll('<div class="form-right-side">');





		/* ---------- my gallery ---------- */



		equalHeight('.created-album-description .created-album-title-link');





		/* ---------- my videos ---------- */



		if($('video, audio').length && $('.chat-box').length == 0) {

			$('video, audio').mediaelementplayer({});

		}



		if($(window).width() > 1024) {

			$('.added-video .description-container textarea').outerHeight($('.added-video .description-container').outerHeight() -55 + 'px');

		}



		if($(window).width() > 767 && $(window).width() < 1025) {

			$('.added-video .description-container textarea').outerHeight($('.added-video .description-container').outerHeight() -51 + 'px');

		}



				/* ---------- blog ---------- */

		$('.search-categories-form input').on('input', function () {

			if ($(this).val() !== '') {

				$(this).parent('.search-categories-form').addClass('show-search');

			}else {

				$(this).parent('.search-categories-form').removeClass('show-search');

			}

		});

		/* ---------- messages ---------- */

		$('.tab-panel.active .select-all').on('click', function() {
				$('.tab-panel.active .select-all').stop().toggleClass('checked');
				if (!$(".tab-panel.active .message .select-tap").not(".checked").length) {
					$('.tab-panel.active .message').removeClass('checked');
					$('.tab-panel.active .message .select-tap').removeClass('checked');
				}
				else {
					$('.tab-panel.active .message').not('.message.checked').addClass('checked');
					$('.tab-panel.active .message .select-tap').not('.message .select-tap.checked').addClass('checked');
				}
			});

			$('.tab-panel.active .message-option-list .check-all').on('click', function() {
				$('.tab-panel.active .select-all').addClass('checked');
				$('.tab-panel.active .message').not('.message.checked').addClass('checked');
				$('.tab-panel.active .message .select-tap').not('.message .select-tap.checked').addClass('checked');
			});

			$('.tab-panel.active .message-option-list .uncheck-all').on('click', function() {
				$('.tab-panel.active .select-all').removeClass('checked');
				$('.tab-panel.active .message').removeClass('checked');
				$('.tab-panel.active .message .select-tap').removeClass('checked');
			});
		$('.tab-panel.active .message-option-list .check-new').on('click', function() {
			$('.tab-panel.active .message').removeClass('checked');
			$('.tab-panel.active .message .select-tap').removeClass('checked');
			$('.tab-panel.active .message.unread .select-tap').addClass('checked');
		});
		$('.tab-panel.active .message-option-list .check-answered').on('click', function() {
			$('.tab-panel.active .message').removeClass('checked');
			$('.tab-panel.active .message .select-tap').removeClass('checked');
			$('.tab-panel.active .message.answered .select-tap').addClass('checked');
		});
		$('.tab-panel.active .message .select-tap').on('click', function() {
			$(this).stop().toggleClass('checked');
			$(this).parents('.message').stop().toggleClass('checked');
		});

		$('.tab-panel.active .message .select-tap').on('click', function() {
			if (!$('.tab-panel.active .message .select-tap').hasClass("checked")) {
				$('.select-all').removeClass('checked');
			}
			if (!$(".tab-panel.active .message .select-tap").not(".checked").length) {
				$('.select-all').addClass('checked');
			}
		});



		$('.type-list-box .select-type').on('click', function() {

			$(this).parent().find('.type-list').stop().fadeToggle(300);

		});



		$(document).on('click touchstart', function (event) {

			if (!$(event.target).closest('.select-type, .type-list').length) {

				$('.type-list').fadeOut(300);

			}

		});



		if($('.datepicker').length) {

			$('.datepicker').datepicker();

		}

		



		$('.date-button').on('click', function() {

			$('.datepicker-wrapper').stop().fadeToggle(300);

		});



		$(document).on('click touchstart', function (event) {

			if (!$(event.target).closest('.date-button, .datepicker-wrapper, .ui-datepicker-prev, .ui-datepicker-next').length) {

				$('.datepicker-wrapper').fadeOut(300);

			}

		});



		$('.filter-list-button').on('click', function() {

			$('.filters-list').stop().fadeToggle(300);

		});



		$(document).on('click touchstart', function (event) {

			if (!$(event.target).closest('.filter-list-button, .filters-list').length) {

				$('.filters-list').fadeOut(300);

			}

		});





		$('.theme-message-conent').prepend('<span> - </span>');







		/* ---------- message detail ---------- */



		$('.reply-more').on('click', function() {

			$(this).parent().find('.control-list').stop().fadeToggle(300);

		});



		$(document).on('click touchstart', function (event) {

			if (!$(event.target).closest('.reply-more, .control-list').length) {

				$('.reply-top .control-list').fadeOut(300);

			}

		});



		if($('.reply-box').length) {

			var replyBoxOffset = $('.reply-box').offset().top;

		}



		$('.reply-active').on('click', function() {

			$('html, body').animate({

				scrollTop: replyBoxOffset - $(window).height()/2

			});



			$('.reply-box textarea').focus();

		});





		$('.page-navigation .select-page').on('click', function() {

			$(this).children('.page-select-form').slideDown(300);

		});



		$(document).on('click touchstart', function (event) {

			if (!$(event.target).closest('.page-navigation .select-page').length) {

				$('.page-navigation .select-page .page-select-form').slideUp(300);

			}

		});

		/* ---------- NEW CHANGES ---------- */

		$('.sub-main.chat').parent().find('.system-header').addClass('chat-header');




		$('.chat-online .chat-button').on('click', function() {
			$('.chat-persons-box').fadeIn(400);
		});

		$('.chat-persons-box .close-box').on('click', function() {
			$('.chat-persons-box').fadeOut(400);
		});


		$('.side-menu-block .side-menu').show();
		$('.side-menu-block .side-menu-button').on('click', function() {
			$(this).parent().find('.side-menu').stop().slideToggle();
		});



		$('.persons-carousel').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			speed: 1000,
			swipe: true,
			arrows: true,
			dots: false,
			infinite: true,
			fade: false,
			/*autoplay: true,
			autoplaySpeed: 3000,*/
			prevArrow: '<button class="prev-slide"><span class="control-icon prev-icon"></span></button>',
			nextArrow: '<button class="next-slide"><span class="control-icon next-icon"></span></button>',
			responsive: [
			{
				breakpoint: 989,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1
				}
			}
			]
		});





		tabs('.messages-tabs', '.tab', '.tab-panel');


		$('.play-video-chat').on('click', function() {
			$(this).fadeOut(300);
			$(this).parent().find('.preview').fadeOut(300);
		});

		/* ---------- mailing list tabs ---------- */



		tabs('.mailing-tabs', '.tab', '.tab-panel');



	});



	/*window load*/

	$(window).on('load', function() {





	});

	/*window resize*/

	$(window).resize(function() {



		/* ---------- global content wrapper ---------- */

		if ($('.main-header').outerHeight() != null && $('.main-footer').outerHeight() != null) {

			$('.global-content-wrapper').css('min-height', $(window).height() - ($('.main-header').outerHeight() + $('.main-footer').outerHeight()));

		}

		/* ---------- mobile menu ---------- */



		$('.mobile-menu').css('min-height', $(window).height() - ($('.menu-logo').outerHeight() + $('.mobile-nav > .socials').outerHeight()));

		

		$('.tab-panel-container').height($('.tab-panel.active').height());



		if($(window).width() > 767) {

			equalHeight('.gift-item .gift-item-title');

		}



		/* ---------- my gallery ---------- */



		equalHeight('.created-album-description .created-album-title-link');



		/* ---------- my videos ---------- */



		if($(window).width() > 1024) {

			$('.added-video .description-container textarea').outerHeight($('.added-video .description-container').height() -55 + 'px');

		}



		if($(window).width() > 767 && $(window).width() < 1025) {

			$('.added-video .description-container textarea').outerHeight($('.added-video .description-container').height() -51 + 'px');

		}







	});



})(jQuery);