(function($) {
    'use strict';
    try {           
        var userId = $('#userID').val();
        var userToken = window["commonLibrary"]["userToken"];
        var apiServer = window["commonLibrary"]["apiServer"];

        var webSocketServer = chatConfig['webSocketServer'];
        var chatPort = chatConfig['chatPort'];
        var videoChatPort = chatConfig['videoChatPort'];

        if (typeof console == "undefined") { this.console = { log: function (msg) {  } };}    
        if (typeof WEB_SOCKET_SWF_LOCATION !="undefined") {
            // if your brower not support websocket， we will use this flash automatic websocket modeling
            WEB_SOCKET_SWF_LOCATION = "/swf/WebSocketMain.swf";
            // enable flash websocket debug
            var WEB_SOCKET_DEBUG = false;
        }
        var send = false;
        var first = true;
        var chatWebSocket;
        var editor, loading = false;
        var chatID, videoChatID, client_list={}, video_list = {}, videoShowTo = [], chatWarnings = {};
        var startRecording = document.getElementById('start-video');
        var stopRecording = document.getElementById('stop-video');    
        var myCamera = $('.video-container')[0];
        var remoteCamera = document.getElementById('PeerVid');    
        var base64data, timeInterval = 500;    
        var videoConnectionTries = 0;
        var audioEnabled = true;
        var recorder;

        var stream;
        var chatConnectionTries = {};        

        var myPC;    
        var streamConstraints;
        var myMediaStream;

        var room;

        if (window['commonLibrary']['storageAvailable']('localStorage')) {
            if(!localStorage.getItem('audioEnabled')) {
              saveSettings();
            } else {
              setup();
            }
        }

        function saveSettings() {        
            if ($(".sound-trigger-wrapper.enabled").length > 0) {
                localStorage.setItem('audioEnabled', 'true');
            } else {
                localStorage.setItem('audioEnabled', 'false');            
            }
            setup();
        }
        function setup() {        
            if (localStorage.getItem('audioEnabled') == 'true') {
                audioEnabled = window['commonLibrary']['setup']();
                setTimeout(function() {
                    $(".sound-trigger-wrapper").addClass('enabled sound-enabled').removeClass('sound-disabled');
                }, 500);
            } else {
                audioEnabled = window['commonLibrary']['setup']();
                setTimeout(function() {
                    $(".sound-trigger-wrapper").removeClass('enabled sound-enabled').addClass('sound-disabled');
                }, 500);
            }    
        }

        $(".sound-trigger-wrapper").on('click', function(){ 
            $(".sound-trigger-wrapper").toggleClass('enabled');       
            saveSettings();
        });

        function stopRecordingMedia(stream)
        {
            var MediaStream = window.MediaStream;

            if (typeof MediaStream === 'undefined' && typeof webkitMediaStream !== 'undefined') {
                MediaStream = webkitMediaStream;
            }        
            /*global MediaStream:true */
            if (typeof MediaStream !== 'undefined' && !('stop' in MediaStream.prototype)) {
                MediaStream.prototype.stop = function() {
                    stream.getAudioTracks().forEach(function(track) {
                        track.stop();
                    });

                    stream.getVideoTracks().forEach(function(track) {
                        track.stop();
                    });
                };
            }
            stream.stop();
        }

        stopRecording.onclick = function() {
            $("#stop-video, .stop-video, #myVid, #PeerVid").addClass('hidden');
            $(".video-window .preview").css({'display': 'inherit'});
            $("#start-video, .play-video-chat, .start").removeClass('hidden');
            try {                
                for (var i in client_list) {
                    client_list[i].send('{"type":"videostop","data":{"token":"' + userToken +'"}}');
                }
                for (var j in video_list) {
                    endCall(j, "Call ended by remote");                    
                    video_list[j].close();
                }
                handleCallTermination();
            } catch (error) {
                handleCallTermination();
            }
            
        }; 

        startRecording.onclick = function() {
            streamConstraints = {video:{facingMode:'user'}, audio:false};
            setLocalMedia(streamConstraints);
            send = true;
            $("#stop-video, .stop-video, #myVid").removeClass('hidden');
            $("#start-video, .start").addClass('hidden');        
            client_list[$('#otherUserID').val()].send(JSON.stringify({
                type: 'videostart',
                data: {
                    token: userToken
                }
            }));
        };


        function createChat(otherUserID)
        {
            try {
                var ws;                    
                ws = new WebSocket("wss://"+webSocketServer+":" + chatPort);
                ws['otherUserID'] = otherUserID;               
                // when socket connection is open, enter user name
                ws.onopen = onopen;
                // when new message available print different information base on message type
                ws.onmessage = onmessage; 
                ws.onclose = function() {  
                    if (chatID == null) {                
                        chatConnectionTries[otherUserID]++;                
                        if (chatConnectionTries[otherUserID] > 10) {
                            printError(userID, "Can't connect to chat server. Try to reload page or send email to site administration.");
                            console.log("Can't connect to chat server. Try to reload page or send email to site administration.");
                            client_list[ws['otherUserID']] = null;
                            toLastChatMessage();
                        } else {
                            console.log("Connection is closing, trying to reconnect in 3 seconds");                    
                            setTimeout(function() {
                                client_list[ws['otherUserID']] = createChat(ws['otherUserID']);                        
                            }, 3000);
                        }                
                    } else {
                        client_list[ws['otherUserID']] = null;
                    }
                    myCamera.src="";
                    $(".video-window .preview, .play-video-chat").css({'display': 'inherit'});
                    $("#start-video, .start").removeClass('hidden');
                    $("#stop-video, .stop-video").addClass('hidden');
                };
                ws.onerror = function() {
                    console.error("Some error occured");
                    myCamera.src="";
                };
                return ws;
            } catch (error) {
                console.error(error.message);
            }
            
        }


        function startCall(room, isCaller){ 
            if (video_list[room] != null && video_list[room].readyState !== video_list[room].CLOSED && video_list[room].readyState !== video_list[room].CLOSING) {           
                if (checkUserMediaSupport){
                    myPC = new RTCPeerConnection(servers);//RTCPeerconnection obj
                    
                    //When my ice candidates become available
                    myPC.onicecandidate = function(e){
                        if(e.candidate){
                            //send my candidate to peer
                            video_list[room].send(JSON.stringify({
                                token: userToken,
                                userReceiverId: video_list[room]['otherUserID'],
                                data: {
                                    action: 'candidate',
                                    candidate: e.candidate,
                                    room: room
                                }
                            }));
                        }
                    };
                
                    //When remote stream becomes available
                    myPC.ontrack = function(e){
                        console.log('remote');                                
                        console.log(e.streams[0]);                        
                        var otherUserID = $('#otherUserID').val();
                        $(".web-cam").append($("<video class='img-responsive' id='video-" + otherUserID + "' autoplay></video>"));
                        document.getElementById('video-' + otherUserID).src = window.URL.createObjectURL(e.streams[0]);                                
                    };            
                    
                    //when remote connection state and ice agent is closed
                    myPC.oniceconnectionstatechange = function(){
                        if (myPC != null) {
                            switch(myPC.iceConnectionState){
                                case 'disconnected':
                                    console.log("Ice connection state is disconnected");
                                    console.log("Call connection problem");
                                    againSetUpStream(room, true);
                                    break;
                                case 'failed':
                                    console.log("Ice connection state is failed");
                                    console.log("Call connection problem");
                                    againSetUpStream(room, true);
                                    break;
                                    
                                case 'closed':
                                    console.log("Ice connection state is 'closed'");
                                    console.log("Call connection closed");
                                    break;
                            }
                        }                    
                    };
                                
                    //WHEN REMOTE CLOSES CONNECTION
                    myPC.onsignalingstatechange = function(){
                        if (myPC != null) {
                            switch(myPC.signalingState){
                                case 'closed':
                                    console.log("Signalling state is 'closed'");
                                    console.log("Signal lost");
                                    break;
                            }
                        }
                    };            
                    //set local media
                    setUpStream(room, isCaller);
                } else{
                    console.log("Your browser does not support video call");
                }
            } else {
                video_list[room] = null;
                handleCallTermination();
            }
        }

        function checkUserMediaSupport(){
            return !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
        }    

        function setUpStream(room, isCaller)
        {
            if (video_list[room] != null && video_list[room].readyState !== video_list[room].CLOSED && video_list[room].readyState !== video_list[room].CLOSING) {    
                if (myMediaStream) {
                    myPC.addStream(myMediaStream);
                    if(isCaller){
                        myPC.createOffer().then(    
                            function(desc){
                                myPC.setLocalDescription(desc);
                                //send sdp
                                video_list[room].send(JSON.stringify({
                                    token: userToken,
                                    userReceiverId: video_list[room]['otherUserID'],
                                    data: {
                                        action: 'sdp',
                                        sdp: desc,
                                        room: room
                                    }
                                }));
                            }, 
                            function(e){
                                console.log("Error creating offer", e.message);                                        
                            });
                        
                        //then notify callee to start call on his end
                        video_list[room].send(JSON.stringify({
                            token: userToken,
                            userReceiverId: video_list[room]['otherUserID'],
                            data: {
                                action: 'startCall',
                                room: room,
                                camera: (video_list[room]['remoteCameraEnabled'] ? "on" : "off")
                            }
                        }));
                    }
                    
                    else{
                        //myPC.createAnswer(description);
                        myPC.createAnswer().then(function(desc){
                                myPC.setLocalDescription(desc);
                                //send sdp
                                video_list[room].send(JSON.stringify({
                                    token: userToken,
                                    userReceiverId: video_list[room]['otherUserID'],
                                    data: {
                                        action: 'sdp',
                                        sdp: desc,
                                        room: room
                                    }
                                }));
                            }).catch(function(e){
                            console.log("Error creating answer", e);                                        
                        });

                    }
                } else {
                    console.log('error. Local media stream not started');
                }
            } else {
                video_list[room] = null;
                handleCallTermination();
            }
            
        }

        function againSetUpStream(room, isCaller)
        {
            if (video_list[room] != null && video_list[room].readyState !== video_list[room].CLOSED && video_list[room].readyState !== video_list[room].CLOSING) {    
                if (myMediaStream) {                
                    if(isCaller){
                        myPC.createOffer({iceRestart: true}).then(    
                            function(desc){
                                myPC.setLocalDescription(desc);
                                //send sdp
                                video_list[room].send(JSON.stringify({
                                    token: userToken,
                                    userReceiverId: video_list[room]['otherUserID'],
                                    data: {
                                        action: 'sdp',
                                        sdp: desc,
                                        room: room
                                    }
                                }));
                            }, 
                            function(e){
                                console.log("Error creating offer", e.message);                                        
                            });
                        
                        //then notify callee to start call on his end
                        video_list[room].send(JSON.stringify({
                            token: userToken,
                            userReceiverId: video_list[room]['otherUserID'],
                            data: {
                                action: 'restartCall',
                                room: room,
                                camera: (video_list[room]['remoteCameraEnabled'] ? "on" : "off")
                            }
                        }));
                    }
                    
                    else{
                        //myPC.createAnswer(description);
                        myPC.createAnswer().then(function(desc){
                                myPC.setLocalDescription(desc);
                                //send sdp
                                video_list[room].send(JSON.stringify({
                                    token: userToken,
                                    userReceiverId: video_list[room]['otherUserID'],
                                    data: {
                                        action: 'sdp',
                                        sdp: desc,
                                        room: room
                                    }
                                }));
                            }).catch(function(e){
                            console.log("Error creating answer", e);                                        
                        });

                    }
                } else {
                    console.log('error. Local media stream not started');
                }
            } else {
                video_list[room] = null;
                handleCallTermination();
            }
            
        }

        //get and set local media
        function setLocalMedia(streamConstraints){
            navigator.mediaDevices.getUserMedia(
                streamConstraints
            ).then(function(myStream){            
                myCamera.src = window.URL.createObjectURL(myStream);                    
                
                //set var myMediaStream as the stream gotten. Will be used to remove stream later on
                myMediaStream = myStream;                        
                
            }).catch(function(e){
                
                switch(e.name){
                    case 'SecurityError':
                        console.log(e.message);
                        
                        //showSnackBar("Media sources usage is not supported on this browser/device", 10000);
                        break;

                    case 'NotAllowedError':
                        console.log(e.message);
                        
                        //showSnackBar("We do not have access to your audio/video sources", 10000);
                        break;
                        
                    case 'NotFoundError':
                        console.log(e.message);
                        
                        //showSnackBar("The requested audio/video source cannot be found", 10000);
                        break;
                    
                    case 'NotReadableError':
                    case 'AbortError':
                        console.log(e.message);
                        //showSnackBar("Unable to use your media sources", 10000);
                        break;
                }
            });
        }    

        function description(desc){
            if (video_list[room].readyState !== video_list[room].CLOSED && video_list[room].readyState !== video_list[room].CLOSING) {        
                myPC.setLocalDescription(desc);

                //send sdp
                video_list[room].send(JSON.stringify({
                    token: userToken,
                    userReceiverId: video_list[room]['otherUserID'],
                    data: {
                        action: 'sdp',
                        sdp: desc,
                        room: room
                    }
                }));
            } else {
                video_list[room] = null;
                handleCallTermination();
            }
        }

        function endCall(room, msg){
            if (video_list[room] != null && video_list[room].readyState !== video_list[room].CLOSED && video_list[room].readyState !== video_list[room].CLOSING) {        
                video_list[room].send(JSON.stringify({
                    token: userToken,
                    userReceiverId: video_list[room]['otherUserID'],
                    data: {   
                        action: 'endCall',
                        msg: msg,
                        room: room
                    }
                }));
                
                if (remoteCamera != null) {
                    remoteCamera.src=""; 
                }
                
                //stopMediaStream();
                myPC = myPC ? myPC.close() : null;
                video_list[room].close();
            }
        }

        function handleCallTermination(){
            myPC = myPC ? myPC.close() : null;//close connection as well
                            
            //tell user that remote terminated call
            console.log("Call terminated by remote");

            //remove streams and free media devices
            stopMediaStream();
            
            //remove video playback src
            myCamera.src="";
            $("#start-video, .start").removeClass('hidden');
            myPC = null;

        }

        function stopMediaStream(){
            
            if(myMediaStream){
        //        myMediaStream.getTracks()[0].stop();
        //        
        //        myMediaStream.getTracks()[1] ? myMediaStream.getTracks()[1].stop() : "";
                
                myMediaStream.getAudioTracks().forEach(function(track) {
                    track.stop();
                });

                myMediaStream.getVideoTracks().forEach(function(track) {
                    track.stop();
                });
            }
        }

        function byteToHex(byte) {
            return ('0' + byte.toString(16)).slice(-2);
        }

        // str randomString(int len);
        //   len - must be an even number (default: 40)
        function randomString(len) {
            var arr = new Uint8Array((len || 40) / 2);
            window.crypto.getRandomValues(arr);
            return [].map.call(arr, byteToHex).join("");
        }

        function getRoom(){
            return randomString(30);
        }

        function videoConnect(otherUserID, room, remoteCameraEnabled)
        {
            var videoWebSocket = new WebSocket("wss://"+webSocketServer+":" + videoChatPort);
            videoWebSocket['otherUserID'] = otherUserID;
            videoWebSocket['room'] = room;
            videoWebSocket['remoteCameraEnabled'] = remoteCameraEnabled;
            videoWebSocket.onopen = function(){
                //subscribe to room            
                videoWebSocket.send(JSON.stringify({
                    token: userToken,
                    userReceiverId: videoWebSocket['otherUserID'],
                    data: {
                        action: 'subscribe',
                        room: room
                    }
                }));
                
                console.log("Connected to the video server!");
            };
            videoWebSocket.onerror = function(){
                console.log("Unable to connect to the video server!");
            };
            videoWebSocket.onmessage = function(e){
                    var data = JSON.parse(e.data);
                    console.log(data);
                    if(data.room === room){
                        //above check is not necessary since all messages coming to this user are for the user's current room
                        //but just to be on the safe side
                        switch(data.action){                           
                            case 'callRejected':
                                console.log(data.msg);
                                // video chat rejected by man                                                            
                                break;
                            case 'error':
                                printError(data['data']['user']['id'], data['data']['message']);
                                videoChatID = data['data']['videochatID'];
                                toLastChatMessage();
                            break;
                            case 'endCall':
                                //i.e. when the caller ends the call from his end (after dialing and before recipient respond)
                                //End call
                                endCall(data.room, "");
                                for (var i in client_list) {
                                    webcamStatus(client_list[i]);
                                }                                
                                console.log(data.msg);                                
                                break;                                
                            case 'startCall':
                                startCall(false);//to start call when callee gives the go ahead (i.e. answers call)                                
                                break;
                            case 'candidate':
                                //message is iceCandidate
                                myPC ? myPC.addIceCandidate(new RTCIceCandidate(data.candidate)) : "";                                
                                break;
                            case 'sdp':
                                //message is signal description
                                myPC ? myPC.setRemoteDescription(new RTCSessionDescription(data.sdp)) : "";
                                
                                break;                        
                            case 'terminateCall'://when remote terminates call (while call is ongoing)
                                handleCallTermination();
                                break;                            
                            case 'newSub':
                                //setRemoteStatus('online');
                                //once the other user joined and current user has been notified, current user should also send a signal
                                //that he is online
                                video_list[data.room].send(JSON.stringify({
                                    token: userToken,
                                    userReceiverId: video_list[data.room]['otherUserID'],
                                    data: {
                                        action: 'imOnline',
                                        room: room
                                    }
                                }));                       
                                break;                            
                            case 'imOnline':
                                answerCall(data.room);
                                //setRemoteStatus('online');
                                break;                            
                            case 'imOffline':
                                //setRemoteStatus('offline');                                            
                                break;
                        }  
                    }
                    
                    else if(data.action === "subRejected"){
                        //subscription on this device rejected cos user has subscribed on another device/browser
                        console.log("Maximum of two users allowed in room. Communication disallowed");
                    }
                }; 
            return videoWebSocket;       
        }

        function loadingSpinner()
        {
            $(".chat-box-messages-wrapper .ss-content").html('<div class="sk-circle">' +
                                    '<div class="sk-circle1 sk-child"></div>' +
                                    '<div class="sk-circle2 sk-child"></div>' +
                                    '<div class="sk-circle3 sk-child"></div>' +
                                    '<div class="sk-circle4 sk-child"></div>' +
                                    '<div class="sk-circle5 sk-child"></div>' + 
                                    '<div class="sk-circle6 sk-child"></div>' +
                                    '<div class="sk-circle7 sk-child"></div>' +
                                    '<div class="sk-circle8 sk-child"></div>' +
                                    '<div class="sk-circle9 sk-child"></div>' +
                                    '<div class="sk-circle10 sk-child"></div>' + 
                                    '<div class="sk-circle11 sk-child"></div>' +
                                    '<div class="sk-circle12 sk-child"></div>' +
                                '</div>');   
        }

        function answerCall(room){
            if (video_list[room].readyState !== video_list[room].CLOSED && video_list[room].readyState !== video_list[room].CLOSING) {        
                //check whether user can use webrtc and use that to determine the response to send
                if (checkUserMediaSupport){
                    streamConstraints = {video:{facingMode:'user'}, audio:false};

                    startCall(room, true);
                } else{
                    //inform caller and current user (i.e. receiver) that he cannot use webrtc, then dismiss modal after a while
                    video_list[room].send(JSON.stringify({
                        token: userToken,
                        userReceiverId: video_list[room]['otherUserID'],
                        data: {
                            action: 'callRejected',
                            msg: "Remote's device does not have the necessary requirements to make call",
                            room: room
                        }
                    }));
                    console.log("Your browser/device does not meet the minimum requirements needed to make a call");
                }
            } else {
                video_list[room] = null;
                handleCallTermination();
            }        
        }    

        function onopen()
        {
            // login   
            try {
                var login_data = '{"type":"auth","data":{"user":{"id":' + $(otherUserID).val() +'}, "token":"' + userToken +'"}}';
                console.log("websocket successful handshake, sending login data:"+login_data);
                this.send(login_data);  
                
                var lastScrollTop = 0;
                $(".chat-box-messages-wrapper .ss-content").on('scroll', function(event) {
                    var otherUserID = $('#otherUserID').val();
                    if (loading) {
                        //loadingSpinner();
                    } else {
                        if (client_list[otherUserID] != null) {
                            var st = $(this).scrollTop();        
                            if(st <= lastScrollTop && st == 0) {                    
                                var message = {
                                    "type":"loadhistory",
                                    "data": {
                                        "user": {
                                            "id": client_list[otherUserID]
                                        },                  
                                        "token": userToken,
                                        "messages": $(this).find(".chat-message-container").length + 10 - 1
                                    }            
                                };                                                    
                                client_list[otherUserID].send(JSON.stringify(message));                    
                            }
                        }
                    }            
                    lastScrollTop = st;        
                });              
            } catch (error) {
                console.error(error.message);
            }
                    
        }         

        function webcamStatus(ws)
        {
            if (myMediaStream != null && typeof myMediaStream !== "undefined" && myMediaStream.active) {
                var video = 1;
            } else {
                var video = 0;
            }
            if (ws != null) {
                ws.send(
                    JSON.stringify({
                        "type":"pong",
                        "data": {                
                            "token": userToken,
                            "video": video
                        }
                    })
                );
            }                             
        }

        // when server sends a message
        function onmessage(e)
        {        
            try {
                var data = JSON.parse(e.data);
                console.log(data);
                switch (data['type']) {
                    // ping from client
                    case 'ping':
                        webcamStatus(this);
                        if (window['mytimer'] == null) {
                            window['mytimer'] = setInterval(function() {
                                window['accountLibrary']['chatPagination'](1);
                                window['accountLibrary']['contactPagination'](1);
                            }, 15000);
                        } 
                        break;
                    case 'error':
                        printError(data['data']['user']['id'], data['data']['message']);
                        chatID = data['data']['chatID'];
                        toLastChatMessage();
                        break;                
                    // login to update user list
                    case 'auth':                
                        var history = data['data']['history'];
                        if (typeof history != "undefined" && history != null && history.length > 0) {
                            $(".chat-box-messages-wrapper .ss-content").html("");
                            for (var counter in history) {                                               
                                printMessageToChat(history[counter]['id'], history[counter]['username'], history[counter]['avatar'], history[counter]['message'], history[counter]['created_at'], history[counter]['readed_at']);
                            }
                        } else {                    
                            if (!data['data']['join']) {
                                $(".chat-box-messages-wrapper .ss-content").html("");
                                $(".chat-box-messages-wrapper .ss-content").append('<div>You haven\'t any chat history with this user</div>');    
                            }                    
                        }                
                        if (data['data']['user']['id'] == $("#otherUserID").val()) {
                            printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['user']['username']+' registered in chat', data['timestamp']);
                        } else {
                            printMessageToChat(data['data']['user']['id'], 'You', data['data']['user']['avatar'], 'You registered in chat', data['timestamp']);
                        }
                        if (typeof history != "undefined" && history != null && data['data']['unreadedMessages'] > 0) {
                            $(".person-online-image.user"  + $("#otherUserID").val()).html("").append('<div class="person-online-messages"><span>' + data['data']['unreadedMessages'] + '</span></div>');
                        } else if (typeof history != "undefined" && history != null && data['data']['unreadedMessages'] == 0) {
                            $(".person-online-image.user"  + $("#otherUserID").val()).html("");
                        }
                        if (data['data']['cam_online'] == 1  && this['otherUserID'] == data['data']['user']['id']) {
                            $('.person-online[data-id=' + $("#otherUserID").val() + '] .person-online-webcam-status').addClass("status-online");
                        } else {
                            $('.person-online[data-id=' + $("#otherUserID").val() + '] .person-online-webcam-status').removeClass("status-online");
                        }
        /*                $.each($(".chat-message-container.unread-message"), function(i, obj){
                            setTimeout(function() {
                                $(obj).removeClass("unread-message");
                            }, 1000+i*800);
                        });   
                        setTimeout(function() {
                            $(".person-online-image.user"  + $("#otherUserID").val()).html("");
                        }, 1000+data['data']['unreadedMessages']*800); */
                        toLastChatMessage();           
                        break;
                    case 'history':            
                        var history = data['data']['history'];                
                        if (typeof history != "undefined" && history != null && history.length > 0) {
                            $(".chat-box-messages-wrapper .ss-content").html("");
                            for (var counter in history) {                                            
                                printMessageToChat(history[counter]['id'], history[counter]['username'], history[counter]['avatar'], history[counter]['message'], history[counter]['created_at'], history[counter]['readed_at']);
                            }
                        } else {                                        
                            $(".chat-box-messages-wrapper .ss-content").html("");
                            $(".chat-box-messages-wrapper .ss-content").append('<div>You haven\'t any chat history with this user</div>');                                 
                        }
                        if (chatWarnings[this['otherUserID']] != null && chatWarnings[this['otherUserID']].length > 0) {
                            for (var counter in chatWarnings[this['otherUserID']]) {
                                console.log(chatWarnings[this['otherUserID']]);
                                printWarning(chatWarnings[counter][0], chatWarnings[counter][1]);
                                chatID = true;
                                toLastChatMessage();
                            }
                        }
                        $(".chat-box-messages-wrapper .ss-content").scrollTop(3);
        /*                var mes =  $(".messages");              
                        mes.scrollTop(500);*/   
                     
                        break;                
                    // sending information to client
                    case 'message':                
                        if (data['data']['user']['id'] == $("#otherUserID").val()) {
                            printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['message'], data['data']['created_at'], data['data']['readed_at']);
                        } else {
                            printMessageToChat(data['data']['user']['id'], 'You',  data['data']['user']['avatar'], data['data']['message'], data['data']['created_at'], data['data']['readed_at']);
                        }
                        if (data['data']['unreadedMessages'] > 0) {
                            $(".person-online-image.user"  + $("#otherUserID").val()).html("").append('<div class="person-online-messages"><span>' + data['data']['unreadedMessages'] + '</span></div>');
                            window['accountLibrary']['getNewMessageCount']();
                        } else {
                            $(".person-online-image.user"  + $("#otherUserID").val()).html("");
                        }
                        if (data['data']['user']['cam_online'] == 1  && this['otherUserID'] == data['data']['user']['id']) {
                            $('.person-online[data-id=' + $("#otherUserID").val() + '] .person-online-webcam-status').addClass("status-online");
                        } else {
                            $('.person-online[data-id=' + $("#otherUserID").val() + '] .person-online-webcam-status').removeClass("status-online");
                        }
                        toLastChatMessage();
                        break;
                    case 'image':                
                        if (this['otherUserID'] == $('#otherUserID').val()) {                
                            if (data['data']['user']['id'] == this['otherUserID']) {
                                printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['message'], data['data']['created_at'], data['data']['readed_at']);
                            } else {
                                printMessageToChat(data['data']['user']['id'], 'You', data['data']['user']['avatar'], data['data']['message'], data['data']['created_at'], data['data']['readed_at']);
                            }
                        }
                        if (data['data']['unreadedMessages'] > 0) {
                            $(".person-online-image.user"  + this['otherUserID']).html("").append('<div class="person-online-messages"><span>' + data['data']['unreadedMessages'] + '</span></div>');
                            window['accountLibrary']['getNewMessageCount']();
                        } else {
                            $(".person-online-image.user"  + this['otherUserID']).html("");
                        }
                        toLastChatMessage();
                        break;                           
                    // some user logout, update user list
                    case "startVideoCall":
                        if (data['data']['camera'] != null && typeof data['data']['camera'] != "undefined" && data['data']['camera'] == "on") {
                            var remoteCameraEnabled = true;
                        } else {
                            var remoteCameraEnabled = false;
                        }                         
                        if (data['data']['restart'] != null && typeof data['data']['restart'] != "undefined" && data['data']['restart'] == "yes") {
                            video_list[data['data']['room']]['remoteCameraEnabled'] = remoteCameraEnabled;
                            againSetUpStream(data['data']['room'], true);
                        } else {
                            video_list[data['data']['room']] = videoConnect(data['data']['user']['id'], data['data']['room'], remoteCameraEnabled);    
                        }                        
                        room = data['data']['room'];
                        break;                                            
                    case 'logout':
                        if (this['otherUserID'] == $('#otherUserID').val()) {
                            printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['user']['username']+' logout', data['timestamp']);               
                        }
                        toLastChatMessage();
                        chatID = true;
                        break;
                    case 'warning':
                        var id = $('#otherUserID').val();
                        if (this['otherUserID'] == id) {
                            printWarning(data['data']['user']['id'], data['data']['message']);
                            chatID = true;
                            toLastChatMessage();
                        } else {
                            if (chatWarnings[id] === undefined || chatWarnings[id] == null) {
                                chatWarnings[id] = [];
                            } else {
                                chatWarnings[id].push({'0': data['data']['user']['id'], '1': data['data']['message']});
                            }
                        }            
                        break;   
                    case 'close':
                        printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['user']['username']+' logout', data['timestamp']);                 
                        printWarning(data['data']['user']['id'], 'Chat was closed. Please refresh page to start chating again.');
                        toLastChatMessage();
                        chatID = data['chatID'];
                        this.close();
                }
            } catch (error) {
                console.error(error.message);
            }
            
        }

        function toLastChatMessage()
        {
            var toBottom = 0;
            $(".chat-message-container").each(function(index, value){              
                toBottom += $(value).height() + 30;
            });
            $('.chat-box-messages-wrapper .ss-content').animate({
                scrollTop: 2*toBottom
            });
        }

        // send a message
        function onSubmit() {
            var otherUserID = $('#otherUserID').val();
            if (client_list[otherUserID] != null) {
                var input = editor[0].emojioneArea.getText();
                if (input.trim() == '') {
                    return;
                } 
                var message = {
                    "type":"message",
                    "data": {
                        "user": {
                            "id": client_list[otherUserID]['otherUserID']
                        },
                        "message": input,    
                        "token": userToken
                    }            
                };
                client_list[otherUserID].send(JSON.stringify(message));
                setTimeout(function() {
                    editor[0].emojioneArea.editor.html('');
                }, 100);        
                $(".chat-message-container.interlocutor-message .message").removeClass("unread-message");
            } else {
                printWarning(userId, 'You are not connected to chat server. Please reload page');
            }
        }

        function moveCaretToStart(el) {
            if (typeof el.selectionStart == "number") {
                el.selectionStart = el.selectionEnd = 0;
            } else if (typeof el.createTextRange != "undefined") {
                el.focus();
                var range = el.createTextRange();
                range.collapse(true);
                range.select();
            }
        }

        function printMessageToChat(id, username, avatar, content, time, readed_at) {
            //var userAvatar = (avatar != null) ? apiServer + '/' + avatar : '/img/no_avatar_small.jpg';
            if (username != 'You') {
                $(".chat-box-messages-wrapper .ss-content").append('<div class="chat-message-container interlocutor-message">' +
                    '<div class="speaker">' +
                    '<span class="speaker-position">'+ username +'</span><span class="message-time">'+ transformTimestamp(time) +'</span>' +
                    '</div>' +
                    '<p class="message">' + messageFilter(content) + '</p></div>');
                if (readed_at == 0) {
                    $(".chat-message-container:last .message").addClass("unread-message");
                }
            } else {
                $(".chat-box-messages-wrapper .ss-content").append('<div class="chat-message-container your-message">' +
                    '<div class="speaker">' +
                    '<span class="speaker-position">'+ username +'</span><span class="message-time">'+ transformTimestamp(time) +'</span>' +
                    '</div>' +
                    '<p class="message">' + messageFilter(content) + '</p></div>');
            }
        }

        function printError(id, message) {
            if (id == $("#otherUserID").val()) {
                $(".sk-circle").remove();
                $(".chat-box-messages-wrapper .ss-content").append('<div class="chat-message-container interlocutor-message alert-danger"><div class="speaker"><span class="speaker-position"><strong>Error</strong></span></div><p class="message">' + message + '</p></div>');
                $('.btn-disabled').addClass('btn-disabled').prop('disabled', true);
                toLastChatMessage();
            }    
        }

        function printWarning(id, message) {
            $("sk-circle").remove();
            $(".chat-box-messages-wrapper .ss-content").append('<div class="chat-message-container interlocutor-message alert-warning"><div class="speaker"><span class="speaker-position"><strong>Warning</strong></span></div><p class="message">' + message + '</p></div>');
            $('.btn-disabled').addClass('btn-disabled').prop('disabled', true);
            toLastChatMessage();
        }

        window.onbeforeunload = function (e) {
            closeChats();
        };

        function closeChats()
        {
            chatID = true;
            videoChatID = true;
            var i;
            for (i in client_list) {
                if (client_list[i] != null) {
                    client_list[i].close();
                    client_list[i] = null;
                }
            }
            for (i in video_list) {
                if (video_list[i] != null) {
                    endCall("Call ended by remote", false);
                    handleCallTermination();
                    video_list[i].close();
                    video_list[i] = null;
                }
            }
        }

        $(document).ready(function() {   
            $(".end").on('click', function() {
                closeChats();
                $('.chat-box-messages-wrapper .sk-circle').parent().remove();
                printWarning($('#otherUserID').val(), 'Chat was closed. Please refresh page to start chating again.');
            });      
            var otherUserID = $('#otherUserID').val();
            if (otherUserID != null && otherUserID != '') {
                client_list[otherUserID] = createChat(otherUserID);
                chatConnectionTries[otherUserID] = 0;
            } else {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text('To start chat, please select user in sidebar');
                setTimeout(window['commonLibrary']['hideModal'], 2000);
            }             
            $('.print-message-form').on('submit', function(event){
                event.preventDefault();
                onSubmit();
                return false;
            });

            editor = $("#chat-message").emojioneArea({          
                pickerPosition: "top",
                filtersPosition: "top",          
                tonesStyle: "bullet",
                saveEmojisAs: "shortname",
                hidePickerOnBlur: false,
                events: {
                    // max number of allowed symbols
                    keypress: function (editor, event) { 
                        var length = this.getText().length;
                        if (length == 300 && event.keyCode != 8 && event.keyCode != 46) { 
                            event.preventDefault(); 
                        }
                    } 
                }
            });
            editor[0].emojioneArea.on("keyup", function(editor, event) {
                event = event || window.event;
                var keyCode = event.keyCode || event.which;            
                if (keyCode==13) {
                    setTimeout(function() {                    
                        editor.closest('form').submit();
                    }, 50); 
                }
            }); 
        });

        function transformTimestamp(timestamp)
        {
            try {
                var currentDateTime = new Date(timestamp);
                if (currentDateTime != 'Invalid Date') {
                    var currentDate = (new Date(currentDateTime-(new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, 10).replace(/[^0-9]/g, "-");
                    var currentTime = (new Date(currentDateTime-(new Date()).getTimezoneOffset() * 60000)).toISOString().slice(11, 19).replace(/[^0-9]/g, ":");
                    return currentDate + ' ' + currentTime;
                } else {
                    return timestamp;
                }            
            } catch (error) {
                return timestamp;
            }        
        }

        function messageFilter(text)
        {
            if (typeof emojione == 'undefined') {
              document.write('<script type="text/javascript" src="/js/node_modules/emojione/lib/js/emojione.min.js"><\/script>');        
            }
            emojione.ascii = true;
            return emojione.shortnameToImage(decodeEntities(text));      
        }

        $("#attach-image").change(function (e) {
            if (this.disabled) return alert('File upload not supported!');
            var F = this.files;
            if (F) {
                FilePreUpload(F);
            }
        });

        function decodeEntities(encodedString) {
            var textArea = document.createElement('textarea');
            textArea.innerHTML = encodedString;
            return textArea.value;
        }

        function FilePreUpload(files) {
            var fd = new FormData();    
            var otherUserID = $('#otherUserID').val();  
            fd.append('imageFile', files[0]);        
            fd.append('otherUserID', otherUserID);        
            uploadImage(fd);
        }

        function uploadImage(formData)
        {
            var uploadURL = apiServer + "/v1/photo/upload-image-from-chat"; //Upload URL
            var extraData = {}; //Extra Data.
            var jqXHR=$.ajax({
                xhr: function() {
                    var xhrobj = $.ajaxSettings.xhr();
                    if (xhrobj.upload) {
                        xhrobj.upload.addEventListener('progress', function(event) {
                            var percent = 0;
                            var position = event.loaded || event.position;
                            var total = event.total;
                            if (event.lengthComputable) {
                                percent = Math.ceil(position / total * 100);
                            }
                        }, false);
                    }
                    return xhrobj;
                },
                url: uploadURL,
                type: "POST",
                contentType: false,
                processData: false,
                data: formData,
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + userToken
                },            
                success: function(data) {
                    var otherUserID = $('#otherUserID').val();
                    console.log(data);
                    if (data['success']){
                        var message = {
                            "type":"image",
                            "data": {
                                "user": {
                                    "id": client_list[otherUserID]['otherUserID']
                                },
                                "image": data['medium_thumb'],    
                                "token": userToken
                            }            
                        };
                        console.log(JSON.stringify(message));
                        client_list[otherUserID].send(JSON.stringify(message));
                    } else {
                        $('#modalMessage').modal('show');
                        $('#modalMessage .modal-body span').text(data['message']);
                        setTimeout(window['commonLibrary']['hideModal'], 2000);
                    }
                }
            });
        }


        $('body').on('click', '.person-contact', function () {        
            $('.chat-box-messages-wrapper .ss-content').html('<div style="height: 500px;"></div>');
            loadingSpinner();
            var otherUserID = $(this).data('id');
            $('.contact-list .person-contact.active').removeClass('active');        
            $(this).addClass('active');
            $.ajax({
                url: apiServer + '/v1/user/user-in-chat/' + otherUserID,
                dataType: "json",
                type: "GET",
                headers: {
                    "Authorization": "Bearer " + userToken
                },
                success: function (response) {
                    if (response['success']) {
                        window.history.pushState("", "", '/account/chat/' + response['user']['id']);

                        $('#otherUserID').val(response['user']['id']);
                        var image = (response['user']['medium_avatar'] != null) ? apiServer + '/' + response['user']['medium_avatar'] : "/img/no_avatar_normal.jpg";
                        $('.active-person-image').css('backgroundImage', 'url(' + image + ')');
                        if (response['user']['online']) {
                            $('.active-person-image').html('<span class="status online">Online</span>');
                        } else {
                            $('.active-person-image').html('<span class="status offline">Offline</span>')
                        }
                        var personDescription = $('.active-person-description');
                        personDescription.html('<a href="/girls/' + response['user']['id'] + '" class="person-name">' + response['user']['first_name'] + '</a>');

                        personDescription.append('<span class="person-id">ID: <span>' + response['user']['id'] + '</span></span>');
                        if (response['user']['age'] != null && typeof response['user']['age'] == "number") {
                            personDescription.append('<span class="person-age">Age: <span>' + response['user']['age'] + '</span></span>');
                        }
                        if (response['user']['height'] != null && typeof response['user']['age'] == "number") {
                            var heightInFeet = response['user']['height']*0.03280839895013123;
                            var heightFeet = Math.floor(heightInFeet); // truncate the float to an integer
                            var heightInches = Math.round((heightInFeet-heightFeet)*12);
                            var heightFeetAndInches = heightFeet + "'" + ((heightInches != 0) ? ' ' + heightInches + "''" : '');
                            personDescription.append('<span class="person-height">Height: <span>' + heightFeetAndInches + '</span></span>');
                        }
                        if (response['user']['height'] != null && typeof response['user']['age'] == "number") {
                            var weight = Math.round(response['user']['weight']/0.45359237) + " lbs";
                            personDescription.append('<span class="person-weight">Weight: <span>' + weight + '</span></span>');
                        }                    
                        
                        $('.chat-active-person-box').removeClass('not-visible');
                        $('.btn-disabled').removeClass('btn-disabled').prop('disabled', false);

                        $('.new-message').attr('href', '/newmessage/' + response['user']['id']);
                        $('.video-gallery').attr('href', '/men/' + response['user']['id'] + '/video-gallery');
                        $('.wink-link').data('id', response['user']['id']);
                        $('.send-like').data('id', response['user']['id']);

                        if (response['user']['inFavourite'] == 1) {
                            $('.interlocutor-cta-list.list-inline li:last a').addClass('like liked');
                        } else {
                            $('.interlocutor-cta-list.list-inline li:last a').removeClass('like liked');
                        }                    
                        if (client_list[response['user']['id']] === undefined || client_list[response['user']['id']] === null) {
                            client_list[response['user']['id']] = createChat(response['user']['id']);
                            chatConnectionTries[response['user']['id']] = 0;
                            $('#stop-video, .stop-video').addClass('hidden');
                            $('.video-container')[0].src = "";
                        } else {
                            var message = {
                                "type": "loadhistory",
                                "data": {
                                    "user": {
                                        "id": client_list[response['user']['id']]['otherUserID']
                                    },
                                    "token": userToken
                                }
                            };
                            client_list[response['user']['id']].send(JSON.stringify(message));
                            if (video_list[response['user']['id']] !== undefined && video_list[response['user']['id']] !== null) {
                                $('.video-container')[0].src = video_list[response['user']['id']]['blobSrc'];
                                $('.play-video-chat').click();
                            }                        
                        }                    
                        setTimeout(function () {
                            toLastChatMessage();
                        }, 500);


    /*                    if (response['user']['cam_online'] == 1 && $('#video-' + response['user']['id']).length == 0) {
                            $('#start-video, .ready-to-videochat').removeClass('hidden');
                            $('#stop-video').addClass('hidden');
                        } else {
                            $('#start-video, .ready-to-videochat').addClass('hidden');
                        }
                        if (response['user']['cam_online'] == 1 && $('#video-' + response['user']['id']).length != 0) {
                            $('#stop-video').removeClass('hidden');
                        }*/
                    } else {
                        $('.chat-box-messages-wrapper .ss-content').html('<div style="400px;"></div>');
                        $('#modalMessage').modal('show');
                        $('#modalMessage .modal-body span').text(response['message']);
                        setTimeout(window['commonLibrary']['hideModal'], 2000);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });
        });
    } catch (error) {
        console.log(error.message);
        console.log(error);
    }
    

})(jQuery);