(function($) {
'use strict';

var userToken = window["commonAdminLibrary"]["userToken"];
var apiServer = window["commonAdminLibrary"]["apiServer"];
var agencyID = window["commonAdminLibrary"]["userID"];
var userType = 3;

$(document).ready(function() {
	$.ajax({
		url: apiServer+'/v1/admin/get-users-info-from-agency',
		type: "POST",
        headers: {
            "Authorization": "Bearer " + userToken
        },			
		dataType: "json",
		data: {
			'otherUserID': agencyID,
			'type': (userType != "") ? userType : null
		},
		success: function (response) {
			if(response.success && response.usersArray != null ){
				var otherUserID = $('#otherUserID').val();
				var selectHtml = '';
				for (var i=0; i < response.usersArray.length; i++) {
					var select = '';
					if (response.usersArray[i]['id'] == otherUserID) {
						select = 'selected';
					}
					var name = response.usersArray[i]['first_name'];
					if (response.usersArray[i]['last_name'] != null) {
						name += ' ' + response.usersArray[i]['last_name'];
					}
					selectHtml +='<option value="'+response.usersArray[i]['id']+'" '+select+'>'+name+'</option>';
				}
				$('select.my-users').html(selectHtml);
			}
		},
	});
});

$('select.my-users').change(function() {
	$('#otherUserID').val($(this).val());
});

})(jQuery);