(function ($) {
    'use strict';
    var userId = $('#userID').val();
    var userToken = window["commonLibrary"]["userToken"];
    var apiServer = window["commonLibrary"]["apiServer"];

    var webSocketServer = chatConfig['webSocketServer'];
    var chatPort = chatConfig['chatPort'];
    var videoChatPort = chatConfig['videoChatPort'];

    if (typeof console == "undefined") {
        this.console = {
            log: function (msg) {
            }
        };
    }
    if (typeof WEB_SOCKET_SWF_LOCATION != "undefined") {
        // if your brower not support websocket， we will use this flash automatic websocket modeling
        WEB_SOCKET_SWF_LOCATION = "/swf/WebSocketMain.swf";
        // enable flash websocket debug
        var WEB_SOCKET_DEBUG = true;
    }
    var send = false;
    var first = true;
    var second = false;
    var editor, loading = false;
    var stop, show = true, chatID, videoChatID, client_list = {}, video_list = {}, chatWarnings = {};
    var videoBuffer = [], bufferLength = 1;
    var chatConnectionTries = {}, videoConnectionTries = 0;
    var myCamera = document.getElementById('myVid');
    var myPC;
    var streamConstraints;
    var myMediaStream;
    var audioEnabled = true;
    var remoteCamera = document.getElementById('PeerVid');

    function byteToHex(byte) {
        return ('0' + byte.toString(16)).slice(-2);
    }

    // str randomString(int len);
    //   len - must be an even number (default: 40)
    function randomString(len) {
        var arr = new Uint8Array((len || 40) / 2);
        window.crypto.getRandomValues(arr);
        return [].map.call(arr, byteToHex).join("");
    }

    function getRoom() {
        return randomString(30);
    }

    if (window['commonLibrary']['storageAvailable']('localStorage')) {
        if (!localStorage.getItem('audioEnabled')) {
            saveSettings();
        } else {
            setup();
        }
    }

    function saveSettings() {
        if ($(".sound-trigger-wrapper.enabled").length > 0) {
            localStorage.setItem('audioEnabled', 'true');
        } else {
            localStorage.setItem('audioEnabled', 'false');
        }
        setup();
    }

    function setup() {
        if (localStorage.getItem('audioEnabled') == 'true') {
            audioEnabled = window['commonLibrary']['setup']();
            setTimeout(function () {
                $(".sound-trigger-wrapper").addClass('enabled sound-enabled').removeClass('sound-disabled');
            }, 500);
        } else {
            audioEnabled = window['commonLibrary']['setup']();
            setTimeout(function () {
                $(".sound-trigger-wrapper").removeClass('enabled sound-enabled').addClass('sound-disabled');
            }, 500);
        }
    }

    $(".sound-trigger-wrapper").on('click', function () {
        $(".sound-trigger-wrapper").toggleClass('enabled');
        saveSettings();
    });

    function createChat(otherUserID) {
        var ws;
        ws = new WebSocket("wss://" + webSocketServer + ":" + chatPort);
        ws['otherUserID'] = otherUserID;
        //console.log(chatWebSocket);
        // when socket connection is open, enter user name
        ws.onopen = onopen;
        // when new message available print different information base on message type
        ws.onmessage = onmessage;
        ws.onclose = function () {
            if (chatID == null) {
                chatConnectionTries[otherUserID]++;
                if (chatConnectionTries[otherUserID] > 10) {
                    printError(ws['otherUserID'], "Can't connect to chat server. Try to reload page or send email to site administration.");
                    console.log("Can't connect to chat server. Try to reload page or send email to site administration.");
                    client_list[ws['otherUserID']] = null;
                    toLastChatMessage();
                } else {
                    console.log("Connection is closing, trying to reconnect in 3 seconds");
                    setTimeout(function () {
                        if (chatID == null) {
                            client_list[ws['otherUserID']] = createChat(ws['otherUserID']);
                        }
                    }, 3000);
                }
            } else {
                if (video_list[otherUserID] != null && (video_list[otherUserID].readyState !== video_list[otherUserID].CLOSED && video_list[otherUserID].readyState !== video_list[otherUserID].CLOSING)) {
                    $("#stop-video").click();
                }
                client_list[ws['otherUserID']] = null;
                if (ws['otherUserID'] == $("#otherUserID").val()) {
                    $(".video-container")[0].src="";
                    $(".video-window .preview").css({'display': 'inherit'});
                    $("#start-video, .play-video-chat, .start").removeClass('hidden');
                    $(".start .play-video-chat").css({'display':'inherit'});
                    $("#stop-video, .stop-video").addClass('hidden');
                }
            }
        };
        ws.onerror = function () {
            loadingSpinner(20);
            console.log("Some error occured");
            $("video-" + otherUserID).remove();
        };
        return ws;
    }

    function loadingSpinner(height) {
        if (typeof height == "undefined" || height == null) {
            height = 400;
        }
        $(".chat-box-messages-wrapper .ss-content").html('<div style="height: ' + height +'px;"><div class="sk-circle">' +
            '<div class="sk-circle1 sk-child"></div>' +
            '<div class="sk-circle2 sk-child"></div>' +
            '<div class="sk-circle3 sk-child"></div>' +
            '<div class="sk-circle4 sk-child"></div>' +
            '<div class="sk-circle5 sk-child"></div>' +
            '<div class="sk-circle6 sk-child"></div>' +
            '<div class="sk-circle7 sk-child"></div>' +
            '<div class="sk-circle8 sk-child"></div>' +
            '<div class="sk-circle9 sk-child"></div>' +
            '<div class="sk-circle10 sk-child"></div>' +
            '<div class="sk-circle11 sk-child"></div>' +
            '<div class="sk-circle12 sk-child"></div>' +
            '</div></div>');
    }

    function onopen() {
        // login
        loadingSpinner();
        chatWarnings[this.otherUserID] = [];
        var login_data = '{"type":"auth","data":{"user":{"id":' + this['otherUserID'] + '}, "token":"' + userToken + '"}}';
        console.log("websocket successful handshake, sending login data:" + login_data);
        this.send(login_data);

        var lastScrollTop = 0;
        $(".chat-box-messages-wrapper .ss-content").on('scroll', function (event) {
            var otherUserID = $('#otherUserID').val();
            if (loading) {
                //loadingSpinner();
            } else {                
                if (client_list[otherUserID] != null) {
                    var st = $(this).scrollTop();
                    if (st <= lastScrollTop && st == 0) {
                        loading = true;
                        var message = {
                            "type": "loadhistory",
                            "data": {
                                "user": {
                                    "id": client_list[otherUserID]
                                },
                                "token": userToken,
                                "messages": $(this).find(".chat-message-container").length + 10 - 1
                            }
                        };
                        client_list[otherUserID].send(JSON.stringify(message));
                    }
                }
            }
            lastScrollTop = st;
        });

    }

    // when server sends a message
    function onmessage(e) {
        var data = JSON.parse(e.data);
        console.log(data);       
        switch (data['type']) {
            // ping from client
            case 'ping':
                this.send('{"type":"pong", "data":{"token":"' + userToken + '"}}');
                if (window['mytimer'] == null) {
                    window['mytimer'] = setInterval(function () {
                        window['accountLibrary']['chatPagination'](1);
                        window['accountLibrary']['contactPagination'](1);
                    }, 15000);
                }
                window['commonLibrary']['updateUserBalance']();
                break;
            // login to update user list
            case 'error':
                printError(data['data']['user']['id'], data['data']['message']);
                chatID = data['data']['chatID'];
                toLastChatMessage();
                break;
            case 'auth':
                console.log(data); 
                if (this['otherUserID'] == $('#otherUserID').val()) {
                    //loadingSpinner();
                    var history = data['data']['history'];
                    if (typeof history != "undefined" && history != null && history.length > 0) {
                        $(".chat-box-messages-wrapper .ss-content").html("");
                        for (var counter in history) {
                            printMessageToChat(history[counter]['id'], history[counter]['username'], history[counter]['avatar'], history[counter]['message'], history[counter]['created_at'], history[counter]['readed_at']);
                        }
                    } else {
                        if (!data['data']['join']) {
                            $(".chat-box-messages-wrapper .ss-content").html("");
                            $(".chat-box-messages-wrapper .ss-content").append('<div>You haven\'t any chat history with this user</div>');
                        }
                    }
                    if (data['data']['user']['id'] == this['otherUserID']) {
                        printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['user']['username'] + ' registered in chat', data['timestamp']);
                    } else {
                        printMessageToChat(data['data']['user']['id'], 'You', data['data']['user']['avatar'], 'You registered in chat', data['timestamp']);
                    }
                    //$(".messages").scrollTop($(".messages").height());                                           
                }
                if (typeof history != "undefined" && history != null && data['data']['unreadedMessages'] > 0) {
                    $(".person-online-image.user" + this['otherUserID']).html("").append('<div class="person-online-messages"><span>' + data['data']['unreadedMessages'] + '</span></div>');
                } else if (typeof history != "undefined" && history != null && data['data']['unreadedMessages'] == 0) {
                    $(".person-online-image.user" + this['otherUserID']).html("");
                }
                /*                $.each($(".chat-message-container.unread-message"), function(i, obj){
                 setTimeout(function() {
                 $(obj).removeClass("unread-message");
                 }, 1000+i*800);
                 });
                 setTimeout(function() {
                 $(".person-online-image.user"  + $("#otherUserID").val()).html("");
                 }, 1000+data['data']['unreadedMessages']*800); */
                toLastChatMessage();
                break;
            case 'history':
                console.log(data);
                var history = data['data']['history'];
                if (typeof history != "undefined" && history != null && history.length > 0) {
                    $(".chat-box-messages-wrapper .ss-content").html("");
                    for (var counter in history) {
                        printMessageToChat(history[counter]['id'], history[counter]['username'], history[counter]['avatar'], history[counter]['message'], history[counter]['created_at'], history[counter]['readed_at']);
                    }
                } else {
                    $(".chat-box-messages-wrapper .ss-content").html("");
                    $(".chat-box-messages-wrapper .ss-content").append('<div>You haven\'t any chat history with this user</div>');
                }
                if (chatWarnings[this['otherUserID']] != null && chatWarnings[this['otherUserID']].length > 0) {
                    for (var counter in chatWarnings[this['otherUserID']]) {
                        console.log(chatWarnings[this['otherUserID']]);
                        printWarning(chatWarnings[counter][0], chatWarnings[counter][1]);
                        chatID = true;
                        toLastChatMessage();
                    }
                }
                $(".chat-box-messages-wrapper .ss-content").scrollTop(1);
                /*                var mes =  $(".messages");
                 mes.scrollTop(500);*/
                loading = false;
                break;
            // sending information to client
            case 'message':
                if (this['otherUserID'] == $('#otherUserID').val()) {
                    if (data['data']['user']['id'] == this['otherUserID']) {
                        printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['message'], data['data']['created_at'], data['data']['readed_at']);
                    } else {
                        printMessageToChat(data['data']['user']['id'], 'You', data['data']['user']['avatar'], data['data']['message'], data['data']['created_at'], data['data']['readed_at']);
                    }
                }
                if (data['data']['unreadedMessages'] > 0) {
                    $(".person-online-image.user" + this['otherUserID']).html("").append('<div class="person-online-messages"><span>' + data['data']['unreadedMessages'] + '</span></div>');
                    window['accountLibrary']['getNewMessageCount']();
                } else {
                    $(".person-online-image.user" + this['otherUserID']).html("");
                }
                toLastChatMessage();
                break;
            case 'image':
                if (this['otherUserID'] == $('#otherUserID').val()) {
                    if (data['data']['user']['id'] == this['otherUserID']) {
                        printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['message'], data['data']['created_at'], data['data']['readed_at']);
                    } else {
                        printMessageToChat(data['data']['user']['id'], 'You', data['data']['user']['avatar'], data['data']['message'], data['data']['created_at'], data['data']['readed_at']);
                    }
                }
                if (data['data']['unreadedMessages'] > 0) {
                    $(".person-online-image.user" + this['otherUserID']).html("").append('<div class="person-online-messages"><span>' + data['data']['unreadedMessages'] + '</span></div>');
                    window['accountLibrary']['getNewMessageCount']();
                } else {
                    $(".person-online-image.user" + this['otherUserID']).html("");
                }
                toLastChatMessage();
                chatID = true;
                break;
            case 'videostart':
                $('.person-online[data-id=' + data['data']['user']['id'] + '] .person-online-webcam-status').addClass("status-online");
/*                if (data['data']['user']['id'] == $("#otherUserID").val()) {
                    $('#start-video, .start, .ready-to-videochat').removeClass('hidden');
                }*/                
                $("#stop-video").click();

                //video_list[data['data']['user']['id']] = data['data']['user'];

                break;
            case 'videostop':
                $('.person-online[data-id=' + data['data']['user']['id'] + '] .person-online-webcam-status').removeClass("status-online");
                if (data['data']['user']['id'] == $("#otherUserID").val()) {
                    $('#stop-video, .stop-video, #myVid, #PeerVid, .ready-to-videochat').addClass('hidden');
                }
                videoChatID = true;
                $('#video-' + data['data']['user']['id']).remove();
                stopMediaStream();
                if (video_list[data['data']['user']['id']] != null && typeof video_list[data['data']['user']['id']] != "undefined") {                    
                    video_list[data['data']['user']['id']] = null;
                }
                delete video_list[data['data']['user']['id']];

                break;
            // some user logout, update user list
            case 'logout':
                if (this['otherUserID'] == $('#otherUserID').val()) {
                    printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['user']['username'] + ' logout', data['timestamp']);
                }
                chatID = true;
                toLastChatMessage();
                break;
            case 'warning':
                var id = $('#otherUserID').val();
                if (this['otherUserID'] == id) {
                    printWarning(data['data']['user']['id'], data['data']['message']);
                    chatID = true;
                    toLastChatMessage();
                } else {
                    chatID = true;
                    if (chatWarnings[id] === undefined || chatWarnings[id] == null) {
                        chatWarnings[id] = [];
                    } else {
                        chatWarnings[id].push({'0': data['data']['user']['id'], '1': data['data']['message']});
                    }
                }
                break;
            case 'close':
                printMessageToChat(data['data']['user']['id'], data['data']['user']['username'], data['data']['user']['avatar'], data['data']['user']['username'] + ' logout', data['timestamp']);
                printWarning(data['data']['user']['id'], 'Chat was closed. Please refresh page to start chating again.');
                toLastChatMessage();
                chatID = data['chatID'];
                this.close();
        }
    }

    function toLastChatMessage() {
        var toBottom = 0;
        $(".chat-message-container").each(function (index, value) {
            toBottom += $(value).height() + 30;
        });
        $('.chat-box-messages-wrapper .ss-content').animate({
            scrollTop: 2 * toBottom
        });
    }

    function answerCall() {
        //check whether user can use webrtc and use that to determine the response to send
        if (checkUserMediaSupport) {
            //set media constraints based on the button clicked. Audio only should be initiated by default
            streamConstraints = {video: {facingMode: 'user'}, audio: false};

            //uncomment the lines below if you comment out the get request above
            startCall(true);
        }

        else {
            var otherUserID = $("#otherUserID").val();
            //inform caller and current user (i.e. receiver) that he cannot use webrtc, then dismiss modal after a while
            video_list[otherUserID].send(JSON.stringify({
                token: userToken,
                userReceiverId: video_list[otherUserID]['otherUserID'],
                data: {
                    action: 'callRejected',
                    msg: "Remote's device does not have the necessary requirements to make call",
                    room: video_list[otherUserID]['room']
                }
            }));

            console.log("Your browser/device does not meet the minimum requirements needed to make a call");

        }

    }

    function reconnectToRemoteUser(isCaller) {
        if (isCaller) {
            myPC.createOffer({iceRestart: true}).then(description, function (e) {
                console.log("Error creating offer", e.message);
            });

            //then notify callee to start call on his end
            video_list[otherUserID].send(JSON.stringify({
                token: userToken,
                userReceiverId: video_list[otherUserID]['otherUserID'],
                data: {
                    action: 'startCall',
                    room: video_list[otherUserID]['room']
                }
            }));
        }

        else {
            //myPC.createAnswer(description);
            myPC.createAnswer().then(description).catch(function (e) {
                console.log("Error creating answer", e);
            });

        }
    }

    function startCall(isCaller) {
        var otherUserID = $('#otherUserID').val();
        if (checkUserMediaSupport) {
            myPC = new RTCPeerConnection(servers);//RTCPeerconnection obj

            //When my ice candidates become available
            myPC.onicecandidate = function (e) {
                if (e.candidate) {
                    //send my candidate to peer
                    video_list[otherUserID].send(JSON.stringify({
                        token: userToken,
                        userReceiverId: video_list[otherUserID]['otherUserID'],
                        data: {
                            action: 'candidate',
                            candidate: e.candidate,
                            room: video_list[otherUserID]['room']
                        }
                    }));
                }
            };
            //When remote stream becomes available
            myPC.ontrack = function (e) {                
                console.log(e.streams[0]);
                $('.video-window').removeClass('no-video');
                video_list[otherUserID]['blobSrc'] = window.URL.createObjectURL(e.streams[0]);
                $('.video-container')[0].src = window.URL.createObjectURL(e.streams[0]);
            };

            //when remote connection state and ice agent is closed
            myPC.oniceconnectionstatechange = function () {
                if (myPC != null) {
                    switch (myPC.iceConnectionState) {
                        case 'disconnected':
                        case 'failed':
                            console.log("Ice connection state is failed/disconnected");
                            console.log("Call connection problem");
                            $("#stop-video").click();
                            break;

                        case 'closed':
                            console.log("Ice connection state is 'closed'");
                            console.log("Call connection closed");
                            $("#stop-video").click();
                            break;
                    }
                }
            };

            //WHEN REMOTE CLOSES CONNECTION
            myPC.onsignalingstatechange = function () {
                if (myPC != null) {
                    switch (myPC.signalingState) {
                        case 'closed':
                            console.log("Signalling state is 'closed'");
                            console.log("Signal lost");
                            break;
                    }
                }
            };
            //set local media
            $('#myVid').addClass('hidden');
            //console.log(streamConstraints);
            //setLocalMedia(streamConstraints, isCaller);
        } else {
            console.log("Your browser does not support video call");
        }
    }

    function startCallwithVideo(isCaller) {
        var otherUserID = $('#otherUserID').val();
        if (checkUserMediaSupport) {
            myPC = new RTCPeerConnection(servers);//RTCPeerconnection obj

            //When my ice candidates become available
            myPC.onicecandidate = function (e) {
                if (e.candidate) {
                    //send my candidate to peer
                    video_list[otherUserID].send(JSON.stringify({
                        token: userToken,
                        userReceiverId: video_list[otherUserID]['otherUserID'],
                        data: {
                            action: 'candidate',
                            candidate: e.candidate,
                            room: video_list[otherUserID]['room']
                        }
                    }));
                }
            };
            //When remote stream becomes available
            myPC.ontrack = function (e) {                
                console.log(e.streams[0]);
                document.getElementById('video-' + $("#otherUserID").val()).src = window.URL.createObjectURL(e.streams[0]);
            };

            //when remote connection state and ice agent is closed
            myPC.oniceconnectionstatechange = function () {
                if (myPC != null) {
                    switch (myPC.iceConnectionState) {
                        case 'disconnected':
                        case 'failed':
                            console.log("Ice connection state is failed/disconnected");
                            console.log("Call connection problem");
                            $("#stop-video").click();
                            break;

                        case 'closed':
                            console.log("Ice connection state is 'closed'");
                            console.log("Call connection closed");
                            $("#stop-video").click();
                            break;
                    }
                }
            };

            //WHEN REMOTE CLOSES CONNECTION
            myPC.onsignalingstatechange = function () {
                if (myPC != null) {
                    switch (myPC.signalingState) {
                        case 'closed':
                            console.log("Signalling state is 'closed'");
                            console.log("Signal lost");
                            break;
                    }
                }
            };
            //set local media
            $('#myVid').addClass('hidden');
            console.log(streamConstraints);
            setLocalMedia(streamConstraints, isCaller);
        } else {
            console.log("Your browser does not support video call");
        }
    }

    function checkUserMediaSupport() {
        return !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
    }

    function connectToRemoteUser(isCaller) {
        if (isCaller) {
            myPC.createOffer().then(description, function (e) {
                console.log("Error creating offer", e.message);
            });

            //then notify callee to start call on his end
            video_list[otherUserID].send(JSON.stringify({
                token: userToken,
                userReceiverId: video_list[otherUserID]['otherUserID'],
                data: {
                    action: 'startCall',
                    room: video_list[otherUserID]['room']
                }
            }));
        }

        else {
            //myPC.createAnswer(description);
            myPC.createAnswer().then(description).catch(function (e) {
                console.log("Error creating answer", e);
            });

        }
    }

    //get and set local media
    function setLocalMedia(streamConstraints, isCaller) {
        var otherUserID = $('#otherUserID').val();        
        navigator.mediaDevices.getUserMedia(
            streamConstraints
        ).then(function (myStream) {
            myCamera.src = window.URL.createObjectURL(myStream);
            $('#myVid').removeClass('hidden');
            myPC.addStream(myStream);//add my stream to RTCPeerConnection

            //set var myMediaStream as the stream gotten. Will be used to remove stream later on
            myMediaStream = myStream;

            if (isCaller) {
                myPC.createOffer().then(description, function (e) {
                    console.log("Error creating offer", e.message);
                });

                //then notify callee to start call on his end
                video_list[otherUserID].send(JSON.stringify({
                    token: userToken,
                    userReceiverId: video_list[otherUserID]['otherUserID'],
                    data: {
                        action: 'startCall',
                        room: video_list[otherUserID]['room']
                    }
                }));
            }

            else {
                //myPC.createAnswer(description);

                myPC.createAnswer().then(description).catch(function (e) {
                    console.log("Error creating answer", e);
                });

            }

        }).catch(function (e) {
            console.log('error');
            switch (e.name) {
                case 'SecurityError':
                    console.log(e.message);
                    break;

                case 'NotAllowedError':
                    console.log(e.message);
                    break;

                case 'NotFoundError':
                    console.log(e.message);
                    break;

                case 'NotReadableError':
                case 'AbortError':
                    console.log(e.message);
                    break;
            }
        });
    }

    function description(desc) {
        var otherUserID = $('#otherUserID').val();
        myPC.setLocalDescription(desc);

        //send sdp
        video_list[otherUserID].send(JSON.stringify({
            token: userToken,
            userReceiverId: video_list[otherUserID]['otherUserID'],
            data: {
                action: 'sdp',
                sdp: desc,
                room: video_list[otherUserID]['room']
            }
        }));
    }

    function endCall(msg, setTimeOut) {
        var otherUserID = $('#otherUserID').val();
        if (video_list[otherUserID] != null && (video_list[otherUserID].readyState !== video_list[otherUserID].CLOSED && video_list[otherUserID].readyState !== video_list[otherUserID].CLOSING)) {
            video_list[otherUserID].send(JSON.stringify({
                token: userToken,
                userReceiverId: video_list[otherUserID]['otherUserID'],
                data: {
                    action: 'endCall',
                    msg: msg,
                    room: video_list[otherUserID]['room']
                }
            }));
            $('#video-' + $("#otherUserID").val()).remove();
        }
    }

    function handleCallTermination() {
        myPC = myPC ? myPC.close() : null;//close connection as well

        //tell user that remote terminated call
        console.log("Call terminated by remote");

        //remove streams and free media devices
        stopMediaStream();
        myMediaStream = null;
        //remove video playback src
        if (myCamera != null) {
            myCamera.src = "";
        }        
        $('.video-window').addClass('no-video');
        $('.video-container')[0].src="";
        $("#start-video, .start").removeClass('hidden');
        $(".video-window .preview").css({'display': 'inherit'});
        //enable 'call' button and disable 'terminate call' btn        
        $("#stop-video, .stop-video, #myVid").addClass('hidden');

    }

    function stopMediaStream() {
        if (myMediaStream) {
            myMediaStream.getAudioTracks().forEach(function (track) {
                track.stop();
            });

            myMediaStream.getVideoTracks().forEach(function (track) {
                track.stop();
            });
        }
    }

    function videoConnect(otherUserID, room) {
        if (video_list[otherUserID] != null) {
            endCall("Call ended by remote", false);
            handleCallTermination();
            video_list[otherUserID].close();
        }
        var videoWebSocket = new WebSocket("wss://" + webSocketServer + ":" + videoChatPort);
        videoWebSocket['otherUserID'] = otherUserID;
        videoWebSocket['room'] = room;
        videoWebSocket['blobSrc'] = "";
        videoWebSocket.onopen = function () {
            //subscribe to room
            videoWebSocket.send(JSON.stringify({
                token: userToken,
                userReceiverId: videoWebSocket['otherUserID'],
                data: {
                    action: 'subscribe',
                    room: room
                }
            }));
            console.log("Connected to the video server!");
        };
        videoWebSocket.onclose = function () {
            if (videoChatID == null) {
                videoConnectionTries++;
                if (videoConnectionTries > 10) {
                    console.log("Can't connect to chat server. Try to reload page or send email to site administration.");
                    video_list[videoWebSocket['otherUserID']] = null;
                } else {
                    if (videoWebSocket['otherUserID'] != null && typeof videoWebSocket['otherUserID'] != "undefined") {
                        console.log("Connection is closing, trying to reconnect in 3 seconds");
                        setTimeout(function () {
                            endCall("Call ended by remote", false);
                            handleCallTermination();
                            var otherUserID = $('#otherUserID').val();
                            var room = getRoom();
                            if (myMediaStream != null) {
                                var localCameraEnabled = true;
                            } else {
                                var localCameraEnabled = false;
                            }
                            streamConstraints = {video: {facingMode: 'user'}, audio: false};
                            if (client_list[otherUserID] != null && (client_list[otherUserID].readyState !== client_list[otherUserID].CLOSED &&
                                                                     client_list[otherUserID].readyState !== client_list[otherUserID].CONNECTING &&
                                                                     client_list[otherUserID].readyState !== client_list[otherUserID].CLOSING)) {
                                client_list[otherUserID].send(JSON.stringify({
                                    "type": "startVideoCall",
                                    "data": {
                                        "user": {
                                            "id": client_list[otherUserID]['otherUserID']
                                        },
                                        "token": userToken,
                                        "room": room,
                                        "camera": (localCameraEnabled ? "on" : "off")
                                    }
                                }));
                            }
                            video_list[otherUserID] = videoConnect(videoWebSocket['otherUserID'], room);
                            $(".web-cam").remove('#video-' + $('#otherUserID').val());
                            $(".web-cam").append($("<video class='img-responsive' id='video-" + $('#otherUserID').val() + "' autoplay></video>"));
                            $("#start-video, #stop-video").removeClass('hidden');
                            $('.video-window .preview').css({'display': 'inherit'});
                        }, 3000);
                    }
                }
            }
        };
        videoWebSocket.onerror = function () {
            console.log("Unable to connect to the video server!");
        };
        videoWebSocket.onmessage = function (e) {
            var data = JSON.parse(e.data);
            console.log(data);
            if (data.room === room) {
                //above check is not necessary since all messages coming to this user are for the user's current room
                //but just to be on the safe side
                switch (data.action) {
                    case 'callRejected':
                        console.log(data.msg);
                        // video chat rejected by man                            
                        break;
                    case 'error':
                        printError(data['data']['user']['id'], data['data']['message']);
                        videoChatID = true;
                        toLastChatMessage();
                        break;
                    case 'endCall':
                        //i.e. when the caller ends the call from his end (after dialing and before recipient respond)
                        //End call                        
                        console.log(data.msg);
                        video_list[videoWebSocket['otherUserID']] = null;
                        videoWebSocket.close();
                        handleCallTermination();
                        break;

                    case 'startCall':
                        $('.video-container')[0].src = '';                        
                        if (typeof data.camera == "undefined" || data.camera == null || data.camera == "off") {
                            startCall(false);//to start call when callee gives the go ahead (i.e. answers call)
                        } else {
                            myMediaStream = true;                       
                            startCallwithVideo(false);
                        }                        

                        break;

                    case 'candidate':
                        //message is iceCandidate
                        myPC ? myPC.addIceCandidate(new RTCIceCandidate(data.candidate)) : "";

                        break;
                    case 'restartCall':
                        console.log('restart');
                        break;
                    case 'sdp':
                        //message is signal description
                        myPC ? myPC.setRemoteDescription(new RTCSessionDescription(data.sdp)) : "";
                        if (myMediaStream == null || typeof myMediaStream == "undefined") {
                            connectToRemoteUser(false);
                        }                        

                        break;
                    case 'terminateCall'://when remote terminates call (while call is ongoing)
                        handleCallTermination();
                        break;
                    case 'newSub':
                        //once the other user joined and current user has been notified, current user should also send a signal
                        //that he is online
                        videoWebSocket.send(JSON.stringify({
                            token: userToken,
                            userReceiverId: videoWebSocket['otherUserID'],
                            data: {
                                action: 'imOnline',
                                room: videoWebSocket['room']
                            }
                        }));
                        break;
                    case 'imOnline':
                        videoWebSocket.send(JSON.stringify({
                            token: userToken,
                            userReceiverId: videoWebSocket['otherUserID'],
                            data: {
                                action: 'imOnline',
                                room: videoWebSocket['room']
                            }
                        }));
                        break;
                    case 'imOffline':
                        break;
                }
            }

            else if (data.action === "subRejected") {
                //subscription on this device rejected cos user has subscribed on another device/browser
                console.log("Maximum of two users allowed in room. Communication disallowed");
            }
        };
        return videoWebSocket;
    }

    $("#start-video").on("click", function () {
        var otherUserID = $('#otherUserID').val();
        var room = getRoom();
        streamConstraints = {video: {facingMode: 'user'}, audio: false};
        video_list[otherUserID] = videoConnect(otherUserID, room);
        if (client_list[otherUserID] != null && (client_list[otherUserID].readyState !== client_list[otherUserID].CONNECTING && client_list[otherUserID].readyState !== client_list[otherUserID].CLOSED && client_list[otherUserID].readyState !== client_list[otherUserID].CLOSING)) {
            client_list[otherUserID].send(JSON.stringify({
                "type": "startVideoCall",
                "data": {
                    "user": {
                        "id": client_list[otherUserID]['otherUserID']
                    },
                    "token": userToken,
                    "room": room,
                    "camera": "off"
                }
            }));
        }
        first = true;
        $("#start-video, .start").addClass('hidden');
        $("#stop-video, .stop-video, #PeerVid").removeClass('hidden');
        $(".web-cam").remove('#video-' + $('#otherUserID').val());
        $(".web-cam").append($("<video class='img-responsive' id='video-" + $('#otherUserID').val() + "' autoplay></video>"));
    });
    $("#stop-video").on("click", function () {
        var otherUserID = $('#otherUserID').val();
        videoChatID = true;
        videoBuffer = [];
        $("#start-video, .play-video-chat, .ready-to-videochat").removeClass('hidden');
        $(".start .play-video-chat").css({'display':'inherit'});
        $("#stop-video, .stop-video, #myVid, #PeerVid").addClass('hidden');
        $("video-" + otherUserID).remove();
        endCall("Call ended by remote", false);
        handleCallTermination();        
        if (video_list[otherUserID] != null) {
            video_list[otherUserID].close();
            video_list[otherUserID] = null;
        }
        
    });

    $(".turn-web").on('click', function(){
        console.log(myMediaStream);
        if (myMediaStream == null || typeof myMediaStream == "undefined") {
            var otherUserID = $('#otherUserID').val();
            var room = getRoom();
            videoChatID = true;
            $("video-" + otherUserID).remove();
            endCall("Call ended by remote", false);
            handleCallTermination();
            if (video_list[otherUserID] != null && typeof video_list[otherUserID] != "undefined" && typeof video_list[otherUserID].close == "function") {
                video_list[otherUserID].close();
            }            
            videoChatID = false;
            streamConstraints = {video: {facingMode: 'user'}, audio: false};
            video_list[otherUserID] = videoConnect(otherUserID, room);
            if (client_list[otherUserID] != null && (client_list[otherUserID].readyState !== client_list[otherUserID].CONNECTING && client_list[otherUserID].readyState !== client_list[otherUserID].CLOSED && client_list[otherUserID].readyState !== client_list[otherUserID].CLOSING)) {                    
                client_list[otherUserID].send(JSON.stringify({
                    "type": "startVideoCall",
                    "data": {
                        "user": {
                            "id": client_list[otherUserID]['otherUserID']
                        },
                        "token": userToken,
                        "room": room,
                        "camera": "on"
                    }
                }));
            }
            $("#start-video").addClass('hidden');
            $("#stop-video, .stop-video, #PeerVid").removeClass('hidden');
            $(".web-cam").remove('#video-' + $('#otherUserID').val());
            $(".web-cam").append($("<video class='img-responsive' id='video-" + $('#otherUserID').val() + "' autoplay></video>"));
        } else {
            $('#modalMessage').modal('show');
            $('#modalMessage .modal-body span').text('Your web-cam already enabled.');            
        }
    });

    // send a message
    function onSubmit() {
        var otherUserID = $('#otherUserID').val();
        if (client_list[otherUserID] != null) {
            var otherUserID = $('#otherUserID').val();
            var input = editor[0].emojioneArea.getText();
            if (input.trim() == '') {
                return;
            }
            //console.log(input);
            var message = {
                "type": "message",
                "data": {
                    "user": {
                        "id": client_list[otherUserID]['otherUserID']
                    },
                    "message": input,
                    "token": userToken
                }
            };

            client_list[otherUserID].send(JSON.stringify(message));
            setTimeout(function () {
                editor[0].emojioneArea.editor.html('');
            }, 100);
            $(".chat-message-container.interlocutor-message .message").removeClass("unread-message");
        } else {
            printWarning(userId, 'You are not connected to chat server. Please reload page');
        }       
    }

    function moveCaretToStart(el) {
        if (typeof el.selectionStart == "number") {
            el.selectionStart = el.selectionEnd = 0;
        } else if (typeof el.createTextRange != "undefined") {
            el.focus();
            var range = el.createTextRange();
            range.collapse(true);
            range.select();
        }
    }

    function printMessageToChat(id, username, avatar, content, time, readed_at) {
        //var userAvatar = (avatar != null) ? apiServer + '/' + avatar : '/img/no_avatar_small.jpg';
        if (username != 'You') {
            $(".chat-box-messages-wrapper .ss-content").append('<div class="chat-message-container interlocutor-message">' +
                '<div class="speaker">' +
                '<span class="speaker-position">'+ username +'</span><span class="message-time">'+ transformTimestamp(time) +'</span>' +
                '</div>' +
                '<p class="message">' + messageFilter(content) + '</p></div>');
            if (readed_at == 0) {
                $(".chat-message-container:last .message").addClass("unread-message");
            }
        } else {
            $(".chat-box-messages-wrapper .ss-content").append('<div class="chat-message-container your-message">' +
                '<div class="speaker">' +
                '<span class="speaker-position">'+ username +'</span><span class="message-time">'+ transformTimestamp(time) +'</span>' +
                '</div>' +
                '<p class="message">' + messageFilter(content) + '</p></div>');
        }
    }

    function printError(id, message) {
        if (id == $("#otherUserID").val()) {
            $(".sk-circle").remove();
            $(".chat-box-messages-wrapper .ss-content").append('<div class="chat-message-container interlocutor-message alert-danger"><div class="speaker"><span class="speaker-position"><strong>Error</strong></span></div><p class="message">' + message + '</p></div>');
            $('.btn-disabled').addClass('btn-disabled').prop('disabled', true);
            toLastChatMessage();
        }    
    }

    function printWarning(id, message) {
        $("sk-circle").remove();
        $(".chat-box-messages-wrapper .ss-content").append('<div class="chat-message-container interlocutor-message alert-warning"><div class="speaker"><span class="speaker-position"><strong>Warning</strong></span></div><p class="message">' + message + '</p></div>');
        $('.btn-disabled').addClass('btn-disabled').prop('disabled', true);
        toLastChatMessage();
    }

    function buildVideoList(currentUser) {
        var camList = $('.web-cam');
        camList.empty();
        for (var p in video_list) {
            if (p != currentUser) {
                camList.append('<p>' + video_list[p]['username'] + ' Web Cam</p><div><img class="img-responsive" id="img' + p + '" /></div>');
            }
        }
    }    

    window.onbeforeunload = function (e) {
        closeChats();
    };

    function closeChats()
    {
        chatID = true;
        videoChatID = true;
        var i;
        for (i in client_list) {
            if (client_list[i] != null) {
                client_list[i].close();
                client_list[i] = null;
            }
        }
        for (i in video_list) {
            if (video_list[i] != null) {
                endCall("Call ended by remote", false);
                handleCallTermination();
                video_list[i].close();
                video_list[i] = null;
            }
        }
    }

    $(document).ready(function () {
        $(".end").on('click', function() {
            closeChats();
            $('.chat-box-messages-wrapper .sk-circle').parent().remove();
            printWarning($('#otherUserID').val(), 'Chat was closed. Please refresh page to start chating again.');            
        });

        var otherUserID = $('#otherUserID').val();
        if (otherUserID != null && otherUserID != '') {            
            client_list[otherUserID] = createChat(otherUserID);
            chatConnectionTries[otherUserID] = 0;
        } else {
            $('#modalMessage').modal('show');
            $('#modalMessage .modal-body span').text('To start chat, please select user in sidebar');
            setTimeout(window['commonLibrary']['hideModal'], 2000);
        }
        $('.print-message-form').on('submit', function (event) {
            event.preventDefault();
            onSubmit();
            return false;
        });
        
        if ($(".chat-message-container").length) {
            var lastMessage = $(".chat-message-container").last().offset().top;
            if ($('.chat-box-messages-wrapper .ss-content').length > 0) {
                $('.chat-box-messages-wrapper .ss-content').animate({
                    scrollTop: lastMessage
                });
            } else {
                setTimeout(function () {
                    $('.chat-box-messages-wrapper .ss-content').animate({
                        scrollTop: lastMessage
                    });
                }, 1000);
            }            
        }
        editor = $("#chat-message").emojioneArea({
            pickerPosition: "top",
            filtersPosition: "top",
            tonesStyle: "bullet",
            saveEmojisAs: "shortname",
            hidePickerOnBlur: false,
            events: {
                click: function (editor, event) {
                    $("#chat-message").data("emojioneArea").editor.focus();
                },
                // max number of allowed symbols
                keypress: function (editor, event) {
                    var length = this.getText().length;
                    if (length == 300 && event.keyCode != 8 && event.keyCode != 46) {
                        event.preventDefault();
                    }
                }
            }
        });
        editor[0].emojioneArea.on("keyup", function (editor1, event) {
            event = event || window.event;
            var keyCode = event.keyCode || event.which;
            if (keyCode == 13) {
                event.preventDefault();
                $('.print-message-form').submit();
            }
        });
    });

    function messageFilter(text) {
        if (typeof emojione == 'undefined') {
            document.write('<script type="text/javascript" src="/js/node_modules/emojione/lib/js/emojione.min.js"><\/script>');
        }
        emojione.ascii = true;
        return emojione.shortnameToImage(decodeEntities(text));
    }

    $("#attach-image").change(function (e) {
        if (this.disabled) return alert('File upload not supported!');
        var F = this.files;
        if (F) {
            FilePreUpload(F);
        }
    });

    function decodeEntities(encodedString) {
        var textArea = document.createElement('textarea');
        textArea.innerHTML = encodedString;
        return textArea.value;
    }

    function transformTimestamp(timestamp) {
        try {
            var currentDateTime = new Date(timestamp);
            if (currentDateTime != 'Invalid Date') {
                var currentDate = (new Date(currentDateTime - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, 10).replace(/[^0-9]/g, "-");
                var currentTime = (new Date(currentDateTime - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(11, 19).replace(/[^0-9]/g, ":");
                return currentDate + ' ' + currentTime;
            } else {
                return timestamp;
            }
        } catch (error) {
            return timestamp;
        }
    }

    function FilePreUpload(files) {
        var fd = new FormData();
        var otherUserID = $('#otherUserID').val();
        fd.append('imageFile', files[0]);
        fd.append('otherUserID', otherUserID);
        uploadImage(fd);
    }

    function uploadImage(formData) {
        var uploadURL = apiServer + "/v1/photo/upload-image-from-chat"; //Upload URL
        var extraData = {}; //Extra Data.
        var jqXHR = $.ajax({
            xhr: function () {
                var xhrobj = $.ajaxSettings.xhr();
                if (xhrobj.upload) {
                    xhrobj.upload.addEventListener('progress', function (event) {
                        var percent = 0;
                        var position = event.loaded || event.position;
                        var total = event.total;
                        if (event.lengthComputable) {
                            percent = Math.ceil(position / total * 100);
                        }
                    }, false);
                }
                return xhrobj;
            },
            url: uploadURL,
            type: "POST",
            contentType: false,
            processData: false,
            data: formData,
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + userToken
            },
            success: function (data) {
                var otherUserID = $('#otherUserID').val();                
                if (data['success']) {
                    var message = {
                        "type": "image",
                        "data": {
                            "user": {
                                "id": client_list[otherUserID]['otherUserID']
                            },
                            "image": data['medium_thumb'],
                            "token": userToken
                        }
                    };
                    client_list[otherUserID].send(JSON.stringify(message));
                } else {
                    $('#modalMessage').modal('show');
                    $('#modalMessage .modal-body span').text(data['message']);
                    setTimeout(window['commonLibrary']['hideModal'], 2000);
                }
            }
        });
    }

    $('body').on('click', '.person-contact', function () {        
        $('.chat-box-messages-wrapper .ss-content').html('<div style="height: 500px;"></div>');
        loadingSpinner();
        var otherUserID = $(this).data('id');
        $('.contact-list .person-contact.active').removeClass('active');        
        $(this).addClass('active');
        $.ajax({
            url: apiServer + '/v1/user/user-in-chat/' + otherUserID,
            dataType: "json",
            type: "GET",
            headers: {
                "Authorization": "Bearer " + userToken
            },
            success: function (response) {
                if (response['success']) {
                    window.history.pushState("", "", '/account/chat/' + response['user']['id']);

                    $('#otherUserID').val(response['user']['id']);
                    var image = (response['user']['medium_avatar'] != null) ? apiServer + '/' + response['user']['medium_avatar'] : apiServer + "/img/no_avatar_normal_girl.jpg";
                    $('.active-person-image').css('backgroundImage', 'url(' + image + ')');
                    if (response['user']['online']) {
                        $('.active-person-image').html('<span class="status online">Online</span>');
                    } else {
                        $('.active-person-image').html('<span class="status offline">Offline</span>')
                    }
                    var personDescription = $('.active-person-description');
                    personDescription.html('<a href="/girls/' + response['user']['id'] + '" class="person-name">' + response['user']['first_name'] + '</a>');

                    personDescription.append('<span class="person-id">ID: <span>' + response['user']['id'] + '</span></span>');
                    if (response['user']['age'] != null && typeof response['user']['age'] == "number") {
                        personDescription.append('<span class="person-age">Age: <span>' + response['user']['age'] + '</span></span>');
                    }
                    if (response['user']['height'] != null && typeof response['user']['age'] == "number") {
                        var heightInFeet = response['user']['height']*0.03280839895013123;
                        var heightFeet = Math.floor(heightInFeet); // truncate the float to an integer
                        var heightInches = Math.round((heightInFeet-heightFeet)*12);
                        var heightFeetAndInches = heightFeet + "'" + ((heightInches != 0) ? ' ' + heightInches + "''" : '');
                        personDescription.append('<span class="person-height">Height: <span>' + heightFeetAndInches + '</span></span>');
                    }
                    if (response['user']['height'] != null && typeof response['user']['age'] == "number") {
                        var weight = Math.round(response['user']['weight']/0.45359237) + " lbs";
                        personDescription.append('<span class="person-weight">Weight: <span>' + weight + '</span></span>');
                    }                    
                    
                    $('.chat-active-person-box').removeClass('not-visible');
                    $('.btn-disabled').removeClass('btn-disabled').prop('disabled', false);

                    $('.new-message').attr('href', '/newmessage/' + response['user']['id']);
                    $('.video-gallery').attr('href', '/girls/' + response['user']['id'] + '/video-gallery');
                    $('.present').attr('href', '/account/shop/' + response['user']['id']);
                    $('.send-like').data('id', response['user']['id']);

                    if (response['user']['inFavourite'] == 1) {
                        $('.interlocutor-cta-list.list-inline li:last a').addClass('like liked');
                    } else {
                        $('.interlocutor-cta-list.list-inline li:last a').removeClass('like liked');
                    }                    
                    if (client_list[response['user']['id']] === undefined || client_list[response['user']['id']] === null) {
                        client_list[response['user']['id']] = createChat(response['user']['id']);
                        chatConnectionTries[response['user']['id']] = 0;
                        $('#stop-video, .stop-video').addClass('hidden');
                        $('.video-container')[0].src = "";
                    } else {
                        var message = {
                            "type": "loadhistory",
                            "data": {
                                "user": {
                                    "id": client_list[response['user']['id']]['otherUserID']
                                },
                                "token": userToken
                            }
                        };
                        client_list[response['user']['id']].send(JSON.stringify(message));
                        console.log(video_list[response['user']['id']]);
                        if (video_list[response['user']['id']] !== undefined && video_list[response['user']['id']] !== null) {
                            $('.video-container')[0].src = video_list[response['user']['id']]['blobSrc'];
                            $(".video-window .preview, .start .play-video-chat").css({'display': 'none'});
                            $("#start-video, .play-video-chat").addClass('hidden');
                            $("#stop-video, .stop-video").removeClass('hidden');
                        } else {
                            $('.video-container')[0].src = "";
                            $(".video-window .preview, .start .play-video-chat").css({'display': 'inherit'});
                            $("#start-video, .play-video-chat, .start").removeClass('hidden');
                            $("#stop-video, .stop-video").addClass('hidden');
                        }                       
                    }                    
                    setTimeout(function () {
                        toLastChatMessage();
                    }, 500);


/*                    if (response['user']['cam_online'] == 1 && $('#video-' + response['user']['id']).length == 0) {
                        $('#start-video, .ready-to-videochat').removeClass('hidden');
                        $('#stop-video').addClass('hidden');
                    } else {
                        $('#start-video, .ready-to-videochat').addClass('hidden');
                    }
                    if (response['user']['cam_online'] == 1 && $('#video-' + response['user']['id']).length != 0) {
                        $('#stop-video').removeClass('hidden');
                    }*/
                } else {
                    $('.chat-box-messages-wrapper .ss-content').html('<div style="400px;"></div>');
                    $('#modalMessage').modal('show');
                    $('#modalMessage .modal-body span').text(response['message']);
                    setTimeout(window['commonLibrary']['hideModal'], 2000);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });

    $('body').on('click', '.btn-start', function () {
        $('#start-video').click();
        $(this).removeClass('btn-start').addClass('btn-stop').html('Turn off webcam');
    });
    $('body').on('click', '.btn-stop', function () {
        $('#stop-video').click();
        $(this).removeClass('btn-stop').addClass('btn-start').html('Turn on webcam');
    });


})(jQuery);