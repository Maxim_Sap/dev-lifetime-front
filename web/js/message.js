var messageLibrary = (function($) {
'use strict';

var userID = window["commonLibrary"]["userID"];
var userToken = window["commonLibrary"]["userToken"];
var apiServer = window["commonLibrary"]["apiServer"];
var page = getCurrentPage();
var pageName = $('.tab-panel.active span.page').attr('data-page-name');


function messagePagination(page, pageName) {
    var lettersID = '';
    var otherUserID = $('.messages-container .filter-line input[name="otherUserID"]').val();
    var createdAt = $('.messages-container .filter-line input[name="created_at"]').val();
    var isFavourite = $('.messages-container .filter-line select[name="is_favourite"]').val();
    var answered = $('.messages-container .filter-line select[name="answered"]').val();
    var searchParams ={
        'created_at': createdAt,
        'is_favourite': isFavourite,
        'answered': answered,
        'otherUserID': otherUserID,
    };  
    $.ajax({
        url: '/message/' + pageName,
        dataType: "json",
        type: "POST",
        data: {
            'page': page,
            'lettersID': lettersID,
            'searchParams': searchParams
        },
        success: function (response) {
            $('.tab-panel.'+ pageName +' .tab-panel-content').html(response);
            //$('.select-messages select').selectpicker('refresh');
            commonLibrary.checkSelect();
        },
    });
}

function addToBlacklist(label, otherUserID) {
    var blackListText = $('.blacklist.link_blacklist').find('span');
    if (blackListText.attr('data-atr') == "add") {
        var confirmMessage = "Are you sure you want add this user in blacklist?";
    } else {
        var confirmMessage = "Are you sure you want delete this user from blacklist?";
    }
    if (confirm(confirmMessage)) {              
        $.ajax({
            url: apiServer + '/v1/user/set-user-to-black-list',
            dataType: "json",
            type: "POST",
            headers: {
                "Authorization": "Bearer " + userToken
            },
            data: {
                'otherUserID': otherUserID,
            },
            success: function (response) {
                if (response.success == true) {
                    if ($('.message-detail').length == 0) {
                        var pageName = $('.tab-panel.active span.page').attr('data-page-name');
                        messagePagination(page, pageName);
                        updateTabs();
                    }                   
                    if (blackListText.attr('data-atr') == "add") {
                        blackListText.attr('data-atr','dell');
                        blackListText.text('Delete from blacklist');
                    } else {
                        blackListText.attr('data-atr','add');
                        blackListText.text('Add to blacklist');
                    }
                }
            },
        });           
    }
}

$(document).on('click','.control-list a.blacklist', function() {
    var messageIDArray = [];
    $('.messages-box li.check .checked').each(function() {
        var currentID = $(this).attr('data-other-user-id');
        messageIDArray.push(currentID);
    });
    if (messageIDArray.length > 0) {
        addToBlacklist($(this), messageIDArray);
    }
});

function updateTabs()
{
    location.reload();
/*    $.get( "/messages", function( data ) {
        var tabList = $(data).find(".list-inline.tab-list");        
        $(".list-inline.tab-list .inbox .item-count").html(tabList.find('.inbox .item-count').html());                        
        $(".list-inline.tab-list .outbox .item-count").html(tabList.find('.outbox .item-count').html());                          
        $(".list-inline.tab-list .trash .item-count").html(tabList.find('.trash .item-count').html());                        
        $(".list-inline.tab-list .notifications .item-count").html(tabList.find('.notifications .item-count').html());                        
    });
    var tabs = ['inbox', 'outbox', 'deleted', 'system'];
    for (var i = 0; i < tabs.length; i++) {
        if ($('.'+ tabs[i] + '.active').length == 0) {
            $.get( "/message/" + tabs[i], function( data ) {
                $(".tab-panel." + tabs[i]).html('123');
            });
        }       
    }*/
}

$(".tab-list li").on('click', function() {
    console.log($(this).hasClass('notifications'));
    if ($(this).hasClass('notifications')) {
        $(".filters-list-wrapper").addClass('hidden');
    } else {
        $(".filters-list-wrapper").removeClass('hidden');
    }
});

function deleteMessage(letterID) {
    if (confirm("Are you sure you want delete this letter?")) {     
        $.ajax({
            url: apiServer+'/v1/letter/delete-letter',
            dataType: "json",
            type: "POST",
            headers: {
                "Authorization": "Bearer " + userToken
            },          
            data: {             
                'letterID': letterID
            },
            success: function (response) {
                if (response.success == true) {
                    var pageName = $('.tab-panel.active span.page').attr('data-page-name');
                    messagePagination(page, pageName);  
                    updateTabs();
                } else {
                    $('#modalMessage').modal('show');
                    $('#modalMessage .modal-body span').text(response.message);
                }
            },
        });           
    }
}

function deleteSingleMessage(letterID) {
    if (confirm("Are you sure you want delete this letter?")) {
        $.ajax({
            url: apiServer+'/v1/letter/delete-letter',
            dataType: "json",
            type: "POST",
            headers: {
                "Authorization": "Bearer " + userToken
            },          
            data: {             
                'letterID': letterID
            },
            success: function (response) {
                if (response.success == true) {
                    location.href = "/message/new";
                } else {
                    $('#modalMessage').modal('show');
                    $('#modalMessage .modal-body span').text(response.message);
                }
            },
        });           
    }
}

function deleteSystemMessage(messageID){
    if (confirm("Are you sure you want delete this system message?")) {

        $.ajax({
            url: apiServer+'/v1/message/delete-message',
            dataType: "json",
            type: "POST",
            headers: {
                "Authorization": "Bearer " + userToken
            },
            data: {             
                'messageID': messageID
            },
            success: function (response) {
                if(response.success == true){
                    location.href = "/message/system";
                } else {
                    $('#modalMessage').modal('show');
                    $('#modalMessage .modal-body span').text(response.message);
                }
            },
        });           
    }
}

function restoreMessage(letterID) { 
    $.ajax({
        url: apiServer + '/v1/letter/restore-letter',
        dataType: "json",
        type: "POST",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {         
            'letterID': letterID,           
        },
        success: function (response) {
            if (response.success == true) {
                var pageName = $('.tab-panel.active span.page').attr('data-page-name');
                messagePagination(page, pageName);
                updateTabs();
            } else {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
            }
        },
    });           
}

$(document).on('click', '.control-list a.delete_link', function() {
    var messageIDArray = [];
    $('.tab-panel.active .messages-box li.check .checked').each(function() {
        var currentID = $(this).attr('id').substr(11);
        messageIDArray.push(currentID);
    });
    if (messageIDArray.length > 0) {
        if ($(this).hasClass('system')) {
            deleteSystemMessage(messageIDArray);
        } else {
            deleteMessage(messageIDArray);
        }
        
    }
});

$(document).on('click', '.control-list a.restore_link', function() {
    var messageIDArray = [];
    $('.tab-panel.active .messages-box li.check .checked').each(function() {
        var currentID = $(this).attr('id').substr(11);
        messageIDArray.push(currentID);
    });
    if (messageIDArray.length > 0) {
        if ($('#system-mmessage').length > 0) {
            deleteSystemMessage(messageIDArray);
        } else {
            restoreMessage(messageIDArray);
        }
        
    }
});

function readMessage(letterID) {    
    $.ajax({
        url: apiServer + '/v1/letter/read-letter',
        dataType: "json",
        type: "POST",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'letterID': letterID
        },
        success: function (response) {
            if (response.success == true) {
                //console.log(response);
            } else {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
            }
        },
    });           
}

function readSystemMessage(messageID) { 
    $.ajax({
        url: apiServer + '/v1/message/read-message',
        dataType: "json",
        type: "POST",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'messageID': messageID
        },
        success: function (response) {
            if (response.success == true) {
                //console.log(response);
            } else {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
            }
        },
    });           
}


function addMessageToFavorite(label, letterID) {
    var featureText = $('.add-featured.add_to_featured_link').find('span');
    $.ajax({
        url: apiServer+'/v1/letter/set-featured-letter',
        dataType: "json",
        type: "POST",
        headers: {
            "Authorization": "Bearer " + userToken
        },      
        data: {
            'letterID': letterID
        },
        success: function (response) {
            if (response.success == true) {             
                if (featureText.attr('data-atr') == "add") {
                    featureText.attr('data-atr','dell');
                    featureText.text('Delete from featured');
                } else {
                    if ($('.message-detail').length == 0) {
                        var pageName = $('.tab-panel.active span.page').attr('data-page-name');
                        messagePagination(page, pageName);
                    }
                    featureText.attr('data-atr','add');
                    featureText.text('Add to featured');
                }
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
            } else {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
            }
        },
    });           
}

$(document).on('click','.control-list a.add-featured', function() {
    var messageIDArray = [];
    $('.tab-panel.active .messages-box li.check .checked').each(function() {
        var currentID = $(this).attr('id').substr(11);
        messageIDArray.push(currentID);
    });
    if(messageIDArray.length >0){
        addMessageToFavorite($(this), messageIDArray);
    }
});

function getCurrentPage() {
    var page = $('.tab-panel.active span.page').data('page');
    var messageCount = $('#message .messages_block');
    if (messageCount.length == 1) {
        page = page - 1;
    }
    if (page < 1) {
        page = 1;
    }
    return page;
}

function getUserLocation(userID) {
    $.ajax({
        url: apiServer+'/user/get-user-locations',
        dataType: "json",
        type: "POST",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'skinID': 1
        },
        success: function (response) {
            if (response.length >0 && response[0].userID == userID) {
                $('.left_profile_info .user_location').text(response[0].city + ', ' + response[0].country);             
            }
        },
    });           
}

function sendMessageToEmail(letterID, path){
    $.ajax({
        url: path + letterID,
        dataType: "json",
        success: function (response) {
            $('#modalMessage').modal('show');
            $('#modalMessage .modal-body span').text(response.message);
        },
    });           
}

$(document).on('change','.select-messages select',function() {
    switch ($(this).val()) {
          case 'check_all': 
            $('[id^="check_mail_"]').prop('checked',true);
            break;
          case '': 
            $('[id^="check_mail_"]').prop('checked',false);
            break;          
          case 'new': 
            $('[id^="check_mail_"]').prop('checked',false);
            $('[id^="check_mail_"][data-type="new-msg"]').prop('checked',true);
            break;        
          case 'answered': 
            $('[id^="check_mail_"]').prop('checked',false);
            $('[id^="check_mail_"][data-type="responsewer-msg"]').prop('checked',true);
            break;        
          case 'read': 
            $('[id^="check_mail_"]').prop('checked',false);
            $('[id^="check_mail_"][data-type="read-msg"]').prop('checked',true);
            break;
        }
    $('.select-messages select [value="'+$(this).val()+'"]').attr("selected", "selected");
    //$('.select-messages select').selectpicker('refresh');
});

$(document).ready(function(){
    $('.datepicker').datepicker();   
    if($('.reply_message textarea').length > 0){
        tinymce.init({
            selector: ".reply_message textarea",
            plugins: "paste textcolor emoticons",
            paste_as_text: true,
            skin: "custom",
            height: "300",
            menubar: false,
            toolbar: "bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | forecolor backcolor | emoticons | undo redo | fullscreen",
            statusbar: true,
            language: "en",
        });
    }
    var charscount = $('#charscount').attr('data-count');
    tinyMceUpdateCharset();

    function tinyMceUpdateCharset() {
        console.log();
        if ($('.reply_message textarea').length > 0) {
            setTimeout(function() {
                if (typeof(tinymce) != 'undefined') {
                    tinymce.triggerSave();
                }
                var content = (typeof($('.reply_message textarea').val()) != 'undefined' && $('.reply_message textarea').val() != null) ? $('.reply_message textarea').val() : '';
                content=content.replace(/<\/?[^>]+(>|$)/g,' ');
                content=content.replace(/&(lt|gt);/g, function (strMatch, p1) {
                        return (p1 == "lt")? "<" : ">";
                      });
                content=content.replace(/ /g,' ');
                content=content.replace(/\n/," ");
                content=content.replace(/ +/g,' ');
                content=content.replace(/\s*((\S+\s*)*)/,'$1');
                content=content.replace(/((\s*\S+)*)\s*/,'$1');
                var charsws=content.length;
                if(charscount-charsws <=0){
                    $('#charscount').addClass('error');
                    $('.send_msg button').addClass('disabled');
                    $('.send_msg button').attr('disabled','disabled');
                }else{
                    $('#charscount').removeClass('error');
                    $('.send_msg button').removeClass('disabled');
                    $('.send_msg button').removeAttr('disabled');
                }
                $('#charscount').text(charscount-charsws);
                tinyMceUpdateCharset();
            }, 700);
        }
    }   
});



//send letters
$('.send_msg button').click(function() {
    var actionType = $('.send_msg button').attr('data-type');
    var themeID = $('.theme_id').val();
    var caption = $('#letters_caption').val();  
    var toUserID = $('.message-autor-info').data('user-id');
    var desc = $('.reply_message textarea').val();
    var previousLetterID = $('.reply_message .previous_letters_id').val();
    if(desc.trim() == ""){
        $('#modalMessage').modal('show');
        $('#modalMessage .modal-body span').text('Fill message content');
        return false;
    } else if (caption == "" && themeID == "") {
        $('#modalMessage').modal('show');
        $('#modalMessage .modal-body span').text('Fill message title');
        return false;
    }
    $.ajax({
        url: apiServer+actionType,
        dataType: "json",
        type: "POST",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'skinID': 1,
            'toUserID': toUserID,
            'title': caption,
            'themeID': themeID,
            'desc': desc,
            'previousLetterID': previousLetterID
        },
        success: function (response) {
            tinyMCE.activeEditor.setContent('');
            //updateUserBalance();
            setTimeout(function() {
                location.href = "/message/inbox";
            }, 1000);
            $('#modalMessage').modal('show');
            $('#modalMessage .modal-body span').text(response.message);
        },
    });
});

function getLettersAlbum() {
    $.ajax({
        url: '/message/get-letters-album',
        dataType: "json",
        success: function (response) {
            if(response.success && response.photos != null){
                $('#message-attach-photo').html(response.photos);
            }
        },
    });           
}

function removePhotoFromLetter(photoID){    
    $.ajax({
        url: apiServer+'/v1/photo/set-photo-status',
        dataType: "json",
        type: "POST",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {         
            'photoID': photoID,
            'status': 0
        },
        success: function (response) {
            if (response.success) {
                getLettersAlbum();
            }
        },
    });           
}

// add photo to text
$(document).on('click','#message-attach-photo img', function() {
    var href = $(this).attr('src');
    var originalHref = $(this).attr('data-original');
    var innerHtml = '<p style="text-align:center;" data-mce-style="text-align: center;">'
                +'<a target="_blank" href="'+originalHref+'" target="_blank" rel="group" data-mce-href="'+originalHref+'">'
                +'<img src="'+href+'" border="0" width="250" data-thumb="'+href+'" data-mce-src="'+href+'"></a></p>';
    var currentContent = tinyMCE.activeEditor.getContent();
    tinyMCE.activeEditor.setContent(currentContent+innerHtml);
});

return {
    "getLettersAlbum": getLettersAlbum,
    "removePhotoFromLetter": removePhotoFromLetter,
    "messagePagination": messagePagination,
    "readMessage": readMessage,
    "readSystemMessage": readSystemMessage,
    "deleteMessage": deleteMessage,
    "deleteSingleMessage": deleteSingleMessage,
    "deleteSystemMessage": deleteSystemMessage,
    "restoreMessage": restoreMessage,
    "addToBlacklist": addToBlacklist,
    "addMessageToFavorite": addMessageToFavorite,
    "sendMessageToEmail": sendMessageToEmail
}

})(jQuery);