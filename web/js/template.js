//jQuery.noConflict();
(function( $ ) {
	
	$(document).ready(function(){
		$(document).on('click','.slide-toggler', function() {
			$($(this).attr('href')).stop().slideToggle();
			$(this).toggleClass('opened');
			return false;
		});
	});
	
	$(window).resize(function(){
		
	});

	$(window).scroll(function(){
		
	});
	
	$(window).load(function(){
		
	});
	
})(jQuery);