var CropImageLibrary = (function () {
'use strict';

if (window["commonLibrary"] != null && window["commonLibrary"]["apiServer"] != null) {
    var apiServer = window["commonLibrary"]["apiServer"];
} else if (window["commonAdminLibrary"] != null && window["commonAdminLibrary"]['apiServer'] != null) {
    var apiServer = window["commonAdminLibrary"]['apiServer'];
} else {
    var apiServer = 'https://api.brachka.web-synthesis.ru';
}

if (window["commonLibrary"] != null && window["commonLibrary"]["userID"] != null) {
    var userID = window["commonLibrary"]["userID"];
} else if (window["commonAdminLibrary"] != null && window["commonAdminLibrary"]['userID'] != null) {
    var userID = window["commonAdminLibrary"]['userID'];
} else {
    var userID = $('#userID').val();
}

if (window["commonLibrary"] != null && window["commonLibrary"]["userToken"] != null) {
    var userToken = window["commonLibrary"]["userToken"];
} else if (window["commonAdminLibrary"] != null && window["commonAdminLibrary"]['userToken'] != null) {
    var userToken = window["commonAdminLibrary"]['userToken'];
} else {
    var userToken = $('#userToken').val();
}

var preview = function(image, selection) {
    if (!selection.width || !selection.height)
        return;
    $('#x1').val(selection.x1);
    $('#y1').val(selection.y1);
    $('#x2').val(selection.x2);
    $('#y2').val(selection.y2);
    $('#width').val(selection.width);
    $('#height').val(selection.height);
    $('#cropPhoto').hide();
    $('#applyCropPhoto').show();
    var originalSrc = $('#myPhoto').attr('data-original-src');
    $('#myPhoto').attr('src', originalSrc);
};

var cropPhoto = function() {
    var originalSrc = $('#myphoto').attr('data-original-src');
    $('#myphoto').attr('src', originalSrc);
    $('#myphoto').imgAreaSelect({
        aspectRatio: '1:1.13',
        x1:0,
        y1:0,
        x2:150,
        y2:169,
        minWidth: 150,
        minHeight: 169,
        handles: true,
        fadeSpeed: 200,
        onSelectChange: preview,
    });
    $('#crop_photo').hide();
    $('#apply_crop_photo').show();
};

var applyCrop = function() {
    var photoID = $('#photo-id').val();
    var x1 = $('#x1').val();
    var y1 = $('#y1').val();
    var x2 = $('#x2').val();
    var y2 = $('#y2').val();
    var imageCoordinates = [x1, y1, x2, y2];
    var imageSize = [$('#myphoto').width(), $('#myphoto').height()];

    $.ajax({
        url: apiServer + '/v1/photo/crop-photo',
        type: "POST",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'photoID': photoID,
            'skinID': 1,
            'imageCoordinates': imageCoordinates,
            'imageSize': imageSize
        },
        success: function (response) {
            if (response['success'] == true) {
                location.reload(true);
            } else {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response['message']);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }
    });
}

return {
    applyCrop: applyCrop,
    cropPhoto: cropPhoto,
    preview: preview
}

})(jQuery);