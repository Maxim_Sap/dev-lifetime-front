var accountLibrary = (function($) {
'use strict';

var userID = window["commonLibrary"]["userID"];
var userToken = window["commonLibrary"]["userToken"];
var apiServer = window["commonLibrary"]["apiServer"];
var userType = window["commonLibrary"]["userType"];
var USER_MALE = window["commonLibrary"]["USER_MALE"];
var USER_FEMALE = window["commonLibrary"]["USER_FEMALE"];
var previousLetterCount, previousMessageCount, previousChatsCount = 0;
var firstTime= true;

if (userType == USER_FEMALE) {
    var searchUserType = USER_MALE;
} else {
    var searchUserType = USER_FEMALE;
}

function searchChatGirlsByParam(params, label) {
    var userLabel = "";
    if (userType == USER_MALE) {
        userLabel = 'girls';
    } else {
        userLabel = 'men';
    }
    $.ajax({
        url: apiServer+'/user/get-girls-by-params',
        dataType: "json",
        type: "POST",
        data: params,
        success: function (response) {
            if (response.success == true) {
                if (response.womresponse.length > 0) {
                    generateChatGirlList(response.womresponse, label);
                    $('.search_girls_list_wrap h3').html(response.count + " " + userLabel + " found");
                } else {
                    $('.search_girls_list_wrap h3').html(userLabel + " not found");
                    $("#" + label + ' .girls_list').html("");
                }
            } 
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }
    });           
}

function generateChatGirlList(girlsArray, label) {
    if (girlsArray == null) return false;
    $.ajax({
        url: '/girls/generate-chat-girls-list',
        type: "POST",
        dataType: "json",
        data: {'girls_mass': girlsArray, 'label': label},
        success: function (response) {
            if (response) {             
                $("#"+label+' .girls_list').html(response);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }
    });
}

//search girls in chat
$('#home .search input').keyup(function() {
    var name = $(this).val().trim();
    var params = {'skin_id': 1, 'userID': userID, 'user_type': searchUserType, 'type': 'last_chat', 'params': {'name': name,}};
    searchChatGirlsByParam(params, 'home');
});
$('#active_chat_mess .search input').keyup(function() {
    var name = $(this).val().trim();
    var params = {'skin_id':1, 'userID': userID, 'user_type': searchUserType, 'type': 'active_chat', 'params': {'name':name,}};
    searchChatGirlsByParam(params,'active_chat_mess');
});
$('#profile .search input').keyup(function() {
    var name = $(this).val().trim();
    var params = {'skin_id': 1,'userID': userID, 'user_type': searchUserType,'type': 'favorite', 'params': {'name':name,}};
    searchChatGirlsByParam(params, 'profile');
});
$('#messages .search input').keyup(function() {
    var name = $(this).val().trim();
    var params = {'skin_id': 1,'userID': userID, 'user_type': searchUserType, 'type': 'winks', 'params': {'name':name,}};
    searchChatGirlsByParam(params, 'messages');
});
$('#settings .search-chat-girl').click(function() {
    var time = 500;
    var params = getChatSearchParams();
    searchChatGirlsByParam(params, 'settings');
    $('#settings .advanced_search_wrap').fadeOut(time);
    $('#settings .search_girls_list_wrap').fadeIn(time);  
    return false;
});
$('#settings #search_girl_by_name').click(function() {
    var time = 500;
    var name = $('#settings .advanced_search .search input[name]').val();
    var params = {'skin_id': 1, 'userID': userID, 'user_type': searchUserType, 'params': {'name':name,}};
    searchChatGirlsByParam(params, 'settings');
    $('#settings .advanced_search_wrap').fadeOut(time);
    $('#settings .search_girls_list_wrap').fadeIn(time);  
});
$('#settings .show-search-panel').click(function() {
    var time = 500;
    $('#settings .advanced_search_wrap').fadeIn(time);
    $('#settings .search_girls_list_wrap').fadeOut(time);
    return false;
});

function getChatSearchParams() {
    var name = $('#settings .advanced_search input[name="name"]').val();
    var cityID = $('#settings .advanced_search .select-region :selected').val();
    var ageFrom = $('#settings .advanced_search .age-from').val();
    var ageTo = $('#settings .advanced_search .age-to').val();
    var girlsOnline = $('#settings #girls-online').prop('checked');
    var camOnline = $('#settings #cam-online').prop('checked');
    var hair = $('#settings .advanced_search select.select-hair').val();
    var eyes = $('#settings .advanced_search select.eyes').val();
    var weight = $('#settings .advanced_search select.weight').val();
    var height = $('#settings .advanced_search select.height').val();

    var params = {
        'skin_id': 1,
        'userType': searchUserType,
        'params': {
            'name': name,
            'city_id': cityID,
            'age_from': ageFrom,
            'age_to': ageTo,
            'girls_online': girlsOnline,
            'cam_online': camOnline,
            'heir': heir,
            'eyes': eyes,
            'weight': weight,
            'height': height,
        }
    };
    return params;
}

function getUsersOnAccountPage() {
    $.ajax({
        url: apiServer+'/user/get-users-on-account-page',
        type: "POST",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {'skin_id': 1, 'limit': 4, 'userID': userID, 'user_type': searchUserType, 'token': userToken}, 
        success: function (response) {          
            if (response.success == true) {
                commonLibrary['generateGirlList'](response.girls.newGirls, 'account_girls_new');
                setTimeout(function() {
                    commonLibrary['generateGirlList'](response.girls.guestGirls, 'account_girls_guest');
                }, 300);
                setTimeout(function() {
                    commonLibrary['generateGirlList'](response.girls.birthdayGirls, 'account_girls_birthday');
                }, 700);
                setTimeout(function() {
                    commonLibrary['generateGirlList'](response.girls.recentGirls, 'account_girls_recent');
                }, 900);
                setTimeout(function() {
                    commonLibrary['generateGirlList'](response.girls.favoriteGirls, 'account_girls_favorite');                  
                }, 1100);
            } 
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }
    });           
}

//setLastActivity every 15 second
var timerID = setInterval(function() { 
                    setLastActivity();                  
                }, 15000);

function setLastActivity() {
    if (userToken != null && userToken !== undefined) {    
        $.ajax({
              url: apiServer+'/v1/user/set-last-activity',
              dataType: "json",
              type: "POST",
              headers: {
                  "Authorization": "Bearer " + userToken
              },
              data: {
                  
              },
              success: function (response) {
                  if (response.success == true) {
                      //
                  } else {
                      if(response.success == false && response.code == 2){
                          clearInterval(timerId);
                          $('#modalMessage').modal('show');
                          $('#modalMessage .modal-body span').text(response.message);
                      }
                  }
              },        
              error: function (xhr, ajaxOptions, thrownError) {        
              }
       }); 
    }          
}

//profile-edit
$('form.edit-profile-form').submit(function(event) {
    event.preventDefault();
    var firstName = $('form.edit-profile-form input[name="first_name"]').val();
    var lastName = $('form.edit-profile-form input[name="last_name"]').val();
    var email = $('form.edit-profile-form input[name="email"]').val();
    var region = $('form.edit-profile-form input[name="region"]').val();
    var birthday = $('form.edit-profile-form input[name="birthday"]').val();
    var weight = $('form.edit-profile-form input[name="weight"]').val();
    var height = $('form.edit-profile-form input[name="height"]').val();
    var occupation = $('form.edit-profile-form input[name="occupation"]').val();
    var kids = $('form.edit-profile-form input[name="kids"]').val();
    var phone = $('form.edit-profile-form input[name="phone"]').val();
    var passport = $('form.edit-profile-form input[name="passport"]').val();
    var hair = $('form.edit-profile-form select[name="hair"]').val();
    var eyes = $('form.edit-profile-form select[name="eyes"]').val();
    var physique = $('form.edit-profile-form select[name="physique"]').val();
    var country = $('form.edit-profile-form select[name="country"] :selected').val();
    var education = $('form.edit-profile-form select[name="education"] :selected').val();
    var religion = $('form.edit-profile-form select[name="religion"] :selected').val();
    var marital = $('form.edit-profile-form select[name="marital"] :selected').val();
    var smoking = $('form.edit-profile-form select[name="smoking"] :selected').val();
    var alcohol = $('form.edit-profile-form select[name="alcohol"] :selected').val();
    var lookingAgeFrom = $('form.edit-profile-form select[name="looking_age_from"] :selected').val();
    var lookingAgeTo = $('form.edit-profile-form select[name="looking_age_to"] :selected').val();
    var aboutMe = $('form.edit-profile-form textarea[name="about_me"]').val();
    var hobbies = $('form.edit-profile-form textarea[name="hobbies"]').val();
    var myIdeal = $('form.edit-profile-form textarea[name="my_ideal"]').val();
    var ethnos = $('form.edit-profile-form select[name="ethnos"]').val();
    var englishLevel = $('form.edit-profile-form select[name="english_level"]').val();
    var address = $('form.edit-profile-form input[name="address"]').val();
    var otherLanguage = $('form.edit-profile-form input[name="other_language"]').val();
    if (country == '' && region !='' || country != '' && region =='') {
        $('#modalMessage').modal('show');
        $('#modalMessage .modal-body span').text('Please select country and region');
        setTimeout(window['commonLibrary']['hideModal'], 2000);
        return false;
    }
    var birthdayArray = birthday.split('-');
    var userBirthDay = new Date();
    userBirthDay.setFullYear(+birthdayArray[2], +birthdayArray[1]-1, +birthdayArray[0]);
    var today = new Date();
    if (isNaN(userBirthDay) || userBirthDay == null) {
        $('#modalMessage').modal('show');
        $('#modalMessage .modal-body span').text('Enter your birthday');
        return false;
    }
    var years = today.getFullYear() - userBirthDay.getFullYear();
    userBirthDay.setFullYear(today.getFullYear());
    if (today < userBirthDay)
    {
        years--;
    }
    if (years < 18) {
        $('#modalMessage').modal('show');
        $('#modalMessage .modal-body span').text('Check your birthday. You have to be at least 18 years old');
        return false;
    }

    $.ajax({
        url: apiServer + '/user/edit-info',
        type: "POST",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'skin_id': 1,
            'userID': userID,
            'token': userToken,
            'first_name': firstName,
            'last_name': lastName,
            'phone': phone,
            'birthday': birthday,
            'kids': kids,
            'height': height,
            'weight': weight,
            'occupation': occupation,
            'looking_age_from': lookingAgeFrom,
            'looking_age_to': lookingAgeTo,
            'about_me': aboutMe,
            'hobbies': hobbies,
            'my_ideal': myIdeal,
            'religion': religion,
            'marital': marital,
            'eyes': eyes,
            'physique': (physique != null ? physique : null),
            'hair': hair,
            'smoking': smoking,
            'alcohol': alcohol,
            'education': education,
            'email': email,
            'city': region,
            'country': country,
            'ethnos': ethnos,
            'english_level': englishLevel,
            'address': address,
            'other_language': otherLanguage,
            'pasport': passport
        },
        success: function (response) {
            if (response.success) {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
                setTimeout(function() {location.reload(true)}, 1000);
            } else {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }
    });           
    return false;
});

//edit pass
$('form.edit-password').submit(function() {
    var oldPassword = $('form.edit-password input[name="oldPassword"]').val();
    var newPassword = $('form.edit-password input[name="newPassword"]').val();
    var confirmPassword = $('form.edit-password input[name="confirmPassword"]').val();
    if (newPassword != confirmPassword) {
        $('#modalMessage').modal('show');
        $('#modalMessage .modal-body span').text('Fields New Password and Confirm Password must contain same values. Please check.');
    }

    $.ajax({
        url: apiServer + '/user/change-password',
        type: "POST",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'skinID': 1,
            'userID': userID,           
            'oldPassword': oldPassword,
            'newPassword': newPassword
        },
        success: function (response) {
            $('#modalMessage').modal('show');
            $('#modalMessage .modal-body span').text(response['message']);
            if (response['success']) {
                setTimeout(function() {
                    location.href = "/account/" + userID;
                }, 1500);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }
    });
    return false;
});

$('#change_avatar').click(function() {
    $('#avatar_file').click();
});

$("#avatar_file").change(function() {
    if(this.disabled) return alert('File upload not supported!');
    var F = this.files;
    if (F) {
        AvatarFilePreUpload(F);
    }
});

function AvatarFilePreUpload(files)
{
    var fd = new FormData();
    var userID = $('#userID').val();
    var userToken = $('#userToken').val();
    fd.append('imageFile', files[0]);
    fd.append('userID', userID);
    fd.append('token', userToken);
    AvatarSendFileToServer(fd);
}
function AvatarSendFileToServer(formData){
    var uploadURL = apiServer + "/user/upload-and-set-avatar"; //Upload URL
    var extraData = {}; //Extra Data.
    var jqXHR = $.ajax({
        xhr: function() {
            var xhrobj = $.ajaxSettings.xhr();
            if (xhrobj.upload) {
                xhrobj.upload.addEventListener('progress', function(event) {
                    var percent = 0;
                    var position = event.loaded || event.position;
                    var total = event.total;
                    if (event.lengthComputable) {
                        percent = Math.ceil(position / total * 100);
                    }
                }, false);
            }
            return xhrobj;
        },
        url: uploadURL,
        type: "POST",
        contentType: false,
        processData: false,
        data: formData,
        headers: {
            "Authorization": "Bearer " + userToken
        },
        dataType: "json",
        success: function(response) {
            if (response.success) {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
                $('.edit_profile_avatar').attr('style', 'background-image: url(' + apiServer + '/' + response.avatar.medium_thumb+');');
                $('.operator-photo').attr('style', 'background-image: url(' + apiServer + '/' + response.avatar.small_thumb+');');
                UpdateSessionAvatar(apiServer + '/' + response.avatar.small_thumb);
            }
            else
            {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(data.message);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }        
    });
}

function UpdateSessionAvatar(avatar) {
    $.post( '/account/update-session-avatar', {avatar: avatar});
}

$('#set_photo_as_avatar').click(function() {
    var photoID = $('.my-gallery-container input[type="radio"]:checked').val();
    $.ajax({
        url: apiServer + '/user/upload-and-set-avatar',
        type: "POST",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {'photoID': photoID, 'params': 'onlySet'},
        success: function (response) {
            if (response.success) {
                $.post( '/account/update-session-avatar',{
                    'avatar': apiServer + '/' + response.avatar.thumb_small,
                    'avatar_photoID': photoID,
                });
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }       
    });
});

$('#set-album-cover').click(function() {
    var photoID = $(this).attr('data-id');
    var albumID = $(this).attr('data-album-title');
    $.ajax({
        url: apiServer + '/v1/album/set-album-cover',
        type: "POST",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'token': userToken,
            'photoID': photoID,
            'albumID': albumID
        },
        success: function (response) {
            $('#modalMessage').modal('show');
            $('#modalMessage .modal-body span').text(response.message);            
            setTimeout(window['commonLibrary']['hideModal'], 2000);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }       
    });           
    return false;
});

$('.photo-title-form').submit(function() {
    var photoID = $('#photo-id').val();
    var photoTitle = $('#photo-title').val();
    $.ajax({
        url: apiServer + '/v1/photo/update-photo-title',
        type: "POST",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'photoID': photoID,
            'photoTitle': photoTitle
        },
        success: function (response) {
            $('#modalMessage').modal('show');
            $('#modalMessage .modal-body span').text(response.message);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }       
    });           
    return false;
});

$('.add-new-album').submit(function() {
    var title = $('.add-new-album [name="title"]').val();
    $.ajax({
        url: apiServer + '/user/create-album',
        type: "POST",
        dataType: "json",
        data: {
            'userID': userID,
            'token': userToken,
            'title':title
        },
        success: function (response) {
            if (response.success) {
                location.reload();
            } else {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }       
    });
    return false;
});

$('.album-edit').submit(function() {
    var title = $('#album_title').val();
    var albumID = $('#album_id').val();
    var status = $('#album_status').prop('checked');
    
    $.ajax({
        url: apiServer+'/user/edit-album',
        type: "POST",
        dataType: "json",
        data: {
            'userID': userID,
            'token': userToken,
            'album_id': albumID,
            'title': title,
            'new_public': status
        },
        success: function (response) {
            $('#modalMessage').modal('show');
            $('#modalMessage .modal-body span').text(response.message);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }       
    });
    return false;
});

$('.blacklist-add, .blacklist-remove').on('click', function () {    
    if ($(this).hasClass('blacklist-add')) {
        var confirmMessage = "Are you sure you want add this user in blacklist?";
    } else {
        var confirmMessage = "Are you sure you want delete this user from blacklist?";
    }
    if (confirm(confirmMessage)) {              
        $.ajax({
            url: apiServer + '/v1/user/set-user-to-black-list',
            dataType: "json",
            type: "POST",
            headers: {
                "Authorization": "Bearer " + userToken
            },
            data: {
                'otherUserID': $(this).data('id'),
            },
            success: function (response) {
                if (response.success == true) {
                    location.reload(true);
                } else {
                    $('#modalMessage').modal('show');
                    $('#modalMessage .modal-body span').text(response.message);
                }
            },
        });           
    }
});


function setVisitor(otherUserID) {
    $.post(apiServer + '/user/set-visitor', {userID: userID, otherUserID: otherUserID});
}

function errorHandler(errorCode)
{
    if (errorCode == 401) {
        $('#modalMessage').modal('show');
        $('#modalMessage .modal-body span').text('Please login.');
        setTimeout(window['commonLibrary']['hideModal'], 2000);
        return;
    }
    if (errorCode == 500) {
        $('#modalMessage').modal('show');
        $('#modalMessage .modal-body span').text('Server error');
        setTimeout(window['commonLibrary']['hideModal'], 2000);
        return;             
    }
}

function chatInvites()
{   
    $.ajax({
            url: apiServer + '/v1/chat-invites',
            dataType: "json",
            type: "GET",
            headers: {
                "Authorization": "Bearer " + userToken
            },
            success: function(response, textStatus, xhr) {
                var html = '';
                if (response["success"]) {
                    for (var i = 0; i < response['chatInvites'].length; i++) {
                        html +=
                            '<div class="chat-person-item clearfix" data-id="'+ response['chatInvites'][i]['invite_id'] +'">' +
                                '<div class="item-person-image" style="background-image: url(\''+ ((response['chatInvites'][i].medium_thumb) ? apiServer + '/' + response['chatInvites'][i].medium_thumb : (userType == 1) ? apiServer + '/img/no_avatar_small_girl.jpg' : apiServer + '/img/no_avatar_small_man.jpg') +'\')">' +
                                    '<span class="status '+ ((response['chatInvites'][i].online) ? 'online' : 'offline') +'">' + ((response['chatInvites'][i].online) ? 'Online' : 'Offline') + '</span>' +
                                '</div>' +
                                '<div class="item-person-description">' +
                                    '<ul class="item-name-years list-inline">' +
                                        '<li><a href="/girls/'+ response['chatInvites'][i].id +'" class="person-name">'+ ((response['chatInvites'][i].first_name) ? response['chatInvites'][i].first_name : '') +'</a></li>' +
                                        '<li><span class="person-age">' + ((response['chatInvites'][i].age) ? response['chatInvites'][i].age + ' years' : '') + '</span></li>' +
                                    '</ul>' +
                                    '<div class="message-box">' +
                                        '<div class="content">' +
                                            '<p>'+ messageFilter(response['chatInvites'][i].message) +'</p>' +
                                        '</div>' +
                                    '</div>' +
                                    '<ul class="item-action-list list-inline">' +
                                        '<li><a href="/account/chat/' + response['chatInvites'][i].id +'" class="chat-now">Chat Now</a></li>' +
                                        '<li><a href="javascript:void(0);" class="no-chat">No Thanks</a></li>' +
                                    '</ul>' +
                                '</div>' +
                            '</div>';
                    }
                    $('.chat-persons-box .inner').html(html);
                    if (response['chatInvites'].length > 0) {
                        $(".chat-button").click();  
                    }                   
                }               
            },
            error: function(xhr, ajaxOptions, thrownError) {
                //errorHandler(xhr.status);
            }
    });
}

$("body").on('click', ".no-chat", function() {          
    var element = $(this).parents(".chat-person-item");
    var inviteID = element.data('id');
    $.ajax({
        url: apiServer + '/v1/chat-invite/hide',
        dataType: "json",
        type: "PATCH",
        data: {                     
            'inviteID': inviteID,           
        },
        headers: {
            "Authorization": "Bearer " + userToken
        },
        success: function (response, textStatus, xhr) {                         
            if (!response['success']) {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response['message']);
                setTimeout(window['commonLibrary']['hideModal'], 2000);
            } else {
                console.log(element);
                element.remove();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            errorHandler(xhr.status);
        }       
    });
});

var getNewMessageCount = function () {
    $.ajax({
        url: apiServer + '/v1/user/get-new-message-count',
        type: "POST",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'userID': userID,
            'token':userToken
        },
        success: function (response) {
            if (response.success) {
                var lettersCount = parseInt(response.lettersCount);
                var messageCount = parseInt(response.messageCount);
                var chatsCount = parseInt(response.chatsCount);
                if (window['commonLibrary']['storageAvailable']('localStorage')) {
                    if (!localStorage.getItem('previousLetterCount')) {
                        localStorage.setItem('previousLetterCount', lettersCount);
                        localStorage.setItem('previousMessageCount', messageCount);
                        localStorage.setItem('previousChatsCount', chatsCount);
                    } else {
                        previousLetterCount = parseInt(localStorage.getItem('previousLetterCount'));
                        previousMessageCount = parseInt(localStorage.getItem('previousMessageCount'));
                        previousChatsCount = parseInt(localStorage.getItem('previousChatsCount'));
                        localStorage.setItem('previousLetterCount', lettersCount);
                        localStorage.setItem('previousMessageCount', messageCount);
                        localStorage.setItem('previousChatsCount', chatsCount);
                    }
                } else {
                    previousLetterCount, previousMessageCount, previousChatsCount = 0;
                }

                if (chatsCount > 0) {
                    $('.side-menu .chat-link a .item-count').remove();                    
                    $('.side-menu .chat-link a').append('<span class="item-count">' + (chatsCount) +'</span>');                                       
                } else {
                    $('.side-menu .chat-link a .item-count').remove();    
                }               
                if (lettersCount + messageCount > 0) {
                    $('.side-menu .messages-link a .item-count').remove();
                    $('.side-menu .messages-link a').append('<span class="item-count">' + (lettersCount + messageCount) +'</span>');                    
                } else {
                    $('.side-menu .messages-link a .item-count').remove();
                }               
                if (lettersCount + messageCount + chatsCount > 0) {
                    $('a.notification-count').remove();
                    $('li.notification').append('<a href="javascript:void(0);" class="notification-count"><span class="plus">+</span>'+ (lettersCount + messageCount + chatsCount) +'</a>');
                } else {
                    $('a.notification-count').remove();
                }
                if (window["commonLibrary"]["storageAvailable"]('localStorage') && 
                    (lettersCount + messageCount + chatsCount > previousLetterCount + previousMessageCount + previousChatsCount)) {
                    // user enable sound
                    if (window['commonLibrary']['setup']()) {
                        if ($('.chat-content-wrapper #otherUserID').length == 0 || $('.chat-content-wrapper #otherUserID').val() == "") {
                            window["commonLibrary"]["playAudio"]();
                        } else if (chatsCount == previousChatsCount) {
                            window["commonLibrary"]["playAudio"]();
                        } else if (chatsCount > previousChatsCount) {
                            var playAudioFile = true;
/*                          for (var i in response.chatsUserName) {
                                if ($('.chat-content-wrapper #otherUserID').length != 0 && 
                                    $('.chat-content-wrapper #otherUserID').val() != "" && 
                                    response.chatsUserName[i]["active"] == "0" &&
                                    response.chatsUserName[i]["action_creator"] == $('.chat-content-wrapper #otherUserID').val()) {
                                    playAudioFile = false;
                                    console.log("in check");
                                    window["commonLibrary"]["playAudio"]();
                                    $('.global-content-wrapper > .person-online').remove();
                                    $('.global-content-wrapper').append($('<div>').addClass('person-online clearfix hidden').attr({'data-id': response.chatsUserName[i]["action_creator"]}));
                                    $(".global-content-wrapper > .person-online[data-id='"+ response.chatsUserName[i]["action_creator"] +"']").click();
                                    break;
                                }
                            }*/
                            if (playAudioFile) {
                                window["commonLibrary"]["playAudio"]();
                            }
                        }
                    }                   
                    if (chatsCount > previousChatsCount) {
                        $('.message-item.chat-message').remove();
                        $('#messages-feed').append('<div class="message-item chat-message" style="display:block;">' +
                                '<a href="javascript:void(0);" class="message-item-link"></a>' +
                                '<button type="button" class="close">' +
                                  '<span></span>' +
                                  '<span></span>' +
                                '</button>' +
                                '<span class="message-title"><a href="/account/chat">New chat message</a></span>' +
                                '<div class="message-body clearfix">' + 
                                  '<i class="fa fa-big fa-commenting-o"></i>' +                                               
                                  '<div class="text-container">' +                                                  
                                    '<a href="/account/chat"><span class="message-text">You have ' + chatsCount + 
                                    ((chatsCount > 1) ? ' unreaded chat messages' : ' unreaded chat message') + '</span></a>' +
                                  '</div>' +
                                '</div>' +
                              '</div>');
                        if (($('.chat-content-wrapper').length > 0) && ($('.contact-list-wrapper:hover').length == 0)) {
                            contactPagination(1);
                        }
                        chatInvites();
                    }
                    if (lettersCount > previousLetterCount) {
                        $('.message-item.letter-message').remove();
                        $('#messages-feed').append('<div class="message-item letter-message" style="display:block;">' +
                            '<a href="javascript:void(0);" class="message-item-link"></a>' +
                            '<button type="button" class="close">' +
                              '<span></span>' +
                              '<span></span>' +
                            '</button>' +
                            '<span class="message-title"><a href="/message/inbox">New letter</a></span>' +
                            '<div class="message-body clearfix">' + 
                              '<i class="fa fa-big fa-envelope"></i>' +                                               
                              '<div class="text-container">' +                                                  
                                '<a href="/message/inbox"><span class="message-text">You have ' + lettersCount + 
                                ((lettersCount > 1) ? ' unreaded letters' : ' unreaded letter') + '</span></a>' +
                              '</div>' +
                            '</div>' +
                          '</div>');    
                    }
                    if (messageCount > previousMessageCount) {
                        $('.message-item.system-message').remove();
                        $('#messages-feed').append('<div class="message-item system-message" style="display:block;">' +
                            '<a href="javascript:void(0);" class="message-item-link"></a>' +
                            '<button type="button" class="close">' +
                              '<span></span>' +
                              '<span></span>' +
                            '</button>' +
                            '<span class="message-title"><a href="/message/inbox">New letter</a></span>' +
                            '<div class="message-body clearfix">' + 
                              '<i class="fa fa-big fa-envelope"></i>' +                                               
                              '<div class="text-container">' +                                                  
                                '<a href="/message/inbox"><span class="message-text">You have ' + messageCount + 
                                ((messageCount > 1) ? ' unreaded system messages' : ' unreaded system message') + '</span></a>' +
                              '</div>' +
                            '</div>' +
                          '</div>');
                    }           
                    setTimeout(function() {
                        for(var i = 0, j = 0; i < $('#messages-feed .message-item').length; i++, j+= 800) {
                            $('#messages-feed .message-item').each(function() {
                                var $thisItem = $(this);
                                if($thisItem.index() === i) {
                                    setTimeout(function() {
                                        $thisItem.fadeOut(400);
                                        $thisItem.remove();
                                    }, j);
                                }
                            });
                        }
                    }, 8000);                       
                }               
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            errorHandler(xhr.status);
        }       
    });
}

// check for new letters and chat sms
if (typeof userToken != 'undefined' && userToken != null && userToken != "") {  
    getNewMessageCount();
}

function close_new_message_popup() {
    $('#modal-info-new-message').fadeOut(300);
    set_session_new_message('');
}

function new_message_link(place) {
    var href = place.attr('data-href');
    set_session_new_message(href);
}

function messageFilter(text) {
    if (typeof emojione == 'undefined') {
        document.write('<script type="text/javascript" src="/js/node_modules/emojione/lib/js/emojione.min.js"><\/script>');
    }
    emojione.ascii = true;
    return emojione.shortnameToImage(decodeEntities(text));
}

function decodeEntities(encodedString) {
    var textArea = document.createElement('textarea');
    textArea.innerHTML = encodedString;
    return textArea.value;
}

function set_session_new_message(href) {
    var htmlMessageCount = $('#modal-info-new-message').attr('data-message-count');
    var htmlLettersCount = $('#modal-info-new-message').attr('data-letters-count');
    var htmlChatSmsCount = $('#modal-info-new-message').attr('data-chat-sms-count');
    $.post('/account/set-session-new-message', {
        'htmlLettersCount': htmlLettersCount,
        'htmlMessageCount': htmlMessageCount,
        'htmlChatSmsCount': htmlChatSmsCount
    });
    $.ajax({
        url: apiServer + '/user/get-new-message-count',
        type: "POST",
        dataType: "json",
        data: {
            'userID': userID,
            'token': userToken
        },
        success: function (response) {
            if (href != '') {
                location.href = href;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            errorHandler(xhr.status);
        }       
    });
}

function addGiftToCart(giftID, price) {
    $.ajax({
        url: '/account/add-gift-to-cart',
        type: "POST",
        dataType: "json",
        data: {
            'gift_id': giftID,
            'price': price
        },
        success: function (response) {
            if (response.success) {
                var count = 0;
                var amount = 0;
                if (typeof(response.basket) == 'object') {
                    for (var gift in response.basket) {
                        count += response.basket[gift]['count'];
                        amount += response.basket[gift]['price'];
                    }
                }
                $('#gift_count').text(count);
                $('#gift_amount').text(amount.toFixed(2));
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text('Current gift add to you cart');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            errorHandler(xhr.status);
        }       
    }); 
}

// purchase in shop
$('.confirm_order').click(function(){
    var giftArray = [];
    var userTo = $('#other-user-id').attr('data-other-user-id');
    $(this).parents('.gifts-add-block').find('.gift-row').each(function(){
        var giftID = $(this).attr('data-gift-id');
        var count = $(this).find('.quantity-counter').text();
        giftArray.push({
            'giftID': giftID,
            'count': count
        });
    });
    $.ajax({
        url: apiServer + '/v1/gift/confirm-gift-payment',
        type: "POST",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'userTo': userTo,
            'giftArray': giftArray,
            'skinID': 1
        },
        success: function (response) {
            if (response.success == true) {
                location.href = '/account/clear-cart?last_girl_id=' + userTo;
            } else {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            errorHandler(xhr.status);
        }       
    }); 
    return false;
});

function giftPagination(page) {
    var otherUserID = $('#other-user-id').attr('data-other-user-id');
    var searchParams = getGiftSearchParams();
    $.ajax({
        url: '/account/shop/' + otherUserID,
        dataType: "json",
        type: "POST",
        data: {
            'page': page,
            'searchParams': searchParams
        },
        success: function (response) {
            if (response.success) {
                $('#gifts_content').html(response.giftsHtml);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            errorHandler(xhr.status);
        }        
    });
    return false;
}

function getGiftSearchParams() {
    var sort = $('#gifts_content .sort-price a.active').attr('data-value');
    var giftCategoryArray = [];
    var category = $('#gifts_content .gifts_categories li.active').attr('data-category');
    giftCategoryArray.push(category);
    return {
        'sort': sort,
        'giftCategoryArray': giftCategoryArray
    }
}

$(document).on('click', '#gifts_content .sort-price a', function () {
    $('#gifts_content .sort-price a').removeClass('active');
    $(this).addClass('active');
    giftPagination(1);
});

$(document).on('click', '.shop .gifts_categories span', function () {
    $('.shop .gifts_categories span[data-value="all"]').removeClass('active');
    if (!$(this).hasClass('active')) {
        if ($(this).attr('data-value') == 'all') {
            $('.shop .gifts_categories span').removeClass('active');
        }
        $(this).addClass('active');
    } else {
        $(this).removeClass('active');
    }
    giftPagination(1);
});

$(".like-icon").on('click', function() {
    var photoID = $(this).parent().data('photo-id');
    likeOrDislikePhoto('like', photoID, $(this));
});

$(".dislike-icon").on('click', function() {
    var photoID = $(this).parent().data('photo-id');
    likeOrDislikePhoto('dislike', photoID, $(this));
});

function likeOrDislikePhoto(type, photoID, element) {
    if (type == "dislike") {
        var path = apiServer + '/v1/photo/dislike';
    } else {
        var path = apiServer + '/v1/photo/like';
    }
    $.ajax({
        url: path,
        type: "POST",
        dataType: "json",
        data: {
            'photoID': photoID,
            'skin_id':1
        },
        headers: {
            "Authorization": "Bearer " + userToken
        },
        success: function (response) {
            console.log(response);
            if (response.success == true) {
                element.parent().find(".like-icon .counter").text(response.likes);
                element.parent().find(".dislike-icon .counter").text(response.dislikes);
            } else {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            errorHandler(xhr.status);
        }       
    });
}

var getAccess = function($this) {   
    var photoID = $this.data('photo-id');
    $.ajax({
        url: apiServer + '/v1/photo/update-access',
        type: "POST",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'photoID': photoID,
            'skinID': 1
        },
        success: function (response) {
            if (response['success'] == true) {
                location.reload();
            } else {                
                $('#modalMessage').modal('show').css({'z-index': 1000000000});
                $('#modalMessage .modal-body span').text(response['message']);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }       
    }); 
}; 

$('.get-access-video').on('click', function() {
    var videoID = $(this).data('video-id');
    $.ajax({
        url: apiServer + '/v1/video/update-access',
        type: "POST",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'videoID': videoID,
            'skinID': 1
        },
        success: function (response) {
            if (response['success'] == true) {
                location.reload();
            } else {                
                $('#modalMessage').modal('show').css({'z-index': 1000000000});
                $('#modalMessage .modal-body span').text(response['message']);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }       
    }); 
});

function getDocumentAlbum() {
    $.ajax({
        url: '/account/get-document-album',
        dataType: "json",
        success: function (response) {
            if(response.success && response.photos != null){
                $('#document-attach-photo').html(response.photos);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }       
    });
}

function deleteDocumentPhoto(photoID) { 
    $.ajax({
        url: apiServer+'/user/set-photo-status',
        dataType: "json",
        type: "POST",
        data: {
            'userID': userID,
            'token': userToken,
            'photoID': photoID,
            'status': 0
        },
        success: function (response) {
            if (response.success) {
                getDocumentAlbum();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }       
    });
}

$('.added-videos-container .added-video button').click(function() {
    var videoID = $(this).attr('data-video-id');
    var videoDescription = $(this).siblings('textarea').val();
    if (typeof(videoID) != 'undefined' && videoID !='') {
        $.ajax({
            url: apiServer + '/user/update-video-description',
            dataType: "json",
            type: "POST",
            headers: {
                "Authorization": "Bearer " + userToken
            },
            data: {
                'videoID': videoID,
                'videoDescription': videoDescription,
                'skinID': 1
            },
            success: function (response) {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
                setTimeout(window['commonLibrary']['hideModal'], 2000);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                
            }           
        });
    }
    return false;
});

$('#start_watch_video').click(function() {
    var videoID = $(this).attr('data-video-id');
    $.ajax({
        url: apiServer + '/user/get-other-user-video',
        dataType: "json",
        type: "POST",
        data: {
            'userID': userID,           
            'token': userToken,
            'video_id': videoID,
            'skin_id': 1,
        },
        success: function (response) {
            if (response.success) {
                var video = $('<div id="video_id_'+videoID+'">Loading the player...</div>');
                video.html('<script type="text/javascript">jwplayer("video_id_'+videoID+'").setup({'
                +'file: "' + apiServer + '/' + response.video_data.path + '",'
                + 'image: "' + response.video_data.poster + '",}); </script>');
                $('.watch-video-block').html(video);
            } else {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response.message);
                setTimeout(window['commonLibrary']['hideModal'], 2000);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }       
    });
});

function chatPagination(currentPage) {

}

function contactPagination(currentPage) {
    if ($(".chat-content-wrapper").length == 0) {
        return;
    }
    $.ajax({
        url: apiServer + '/v1/user/contacts',
        dataType: "json",
        type: "GET",
        data: {                     
            'page': currentPage,            
        },
        headers: {
            "Authorization": "Bearer " + userToken
        },
        success: function (response, textStatus, xhr) {
            if (response['success']) {
                var sidebar = $('.contact-list-box');               
                sidebar.find(".empty-contact-list").remove();
                sidebar.find('.contact-list-wrapper').remove();
                if (response['contact_list'].length == 0) {
                    sidebar.find(".divider").after('<div class="empty-contact-list">Your contact list is empty.</div>');
                } else {
                    var list = $('<ul>').addClass('contact-list').attr({'data-page': currentPage});
                    var $userLi;
                    for (var i in response['contact_list']) {
                        $userLi = $('<li>');
                        if (response['contact_list'][i]['id'] == $('#otherUserID').val()) {
                            var divClass='person-contact active';
                        } else {
                            var divClass='person-contact';
                        }                                       
                        $userLi.append($('<div>').addClass(divClass).attr({'data-id': response['contact_list'][i]['id']})
                                                 .append($('<a>').attr({'href': 'javascript:void(0);'}).addClass('person-contact-link')
                                                                 .append($('<span>').addClass('person-name').text(response['contact_list'][i]['first_name'] + ','))
                                                                 .append($('<span>').addClass('person-id').html(' ID: ')
                                                                                    .append($('<span>').text(response['contact_list'][i]['id']))
                                                                        )
                                                                        
                                                        )
                                      );

                        if (response['contact_list'][i]['unreaded_messages'] != null && response['contact_list'][i]['unreaded_messages'] > 0) {
                            $userLi.find('.person-contact-link').append('<span class="item-count">' + response['contact_list'][i]['unreaded_messages'] + '</span>');
                        }
                        
                        list.append($userLi);                   
                    }
                    sidebar.find(".divider").after($('<div>').addClass('contact-list-wrapper').append(list));
                    if (sidebar.find('.contact-list-wrapper').length > 0) {
                        SimpleScrollbar.initEl(sidebar.find('.contact-list-wrapper')[0]);
                    }
                    if (response['count'] > sidebar.find('.contact-list').length) {
                        var lastScrollTop = 0;
                        var enableEvent = true;
                        $(".contact-list-wrapper .ss-content").on('scroll', function (event) {
                            var st = $(this).scrollTop();                    
                            if ((st >= $(".contact-list-wrapper .contact-list").height() - $(".contact-list-wrapper .ss-content").height()) && enableEvent) {
                                var nextPage = sidebar.find('.contact-list').data('page');
                                sidebar.find('.contact-list').data({'page': ++nextPage});
                                enableEvent = false;
                                $.ajax({
                                    url: apiServer + '/v1/user/contacts',
                                    dataType: "json",
                                    type: "GET",
                                    data: {                     
                                        'page': nextPage,           
                                    },
                                    headers: {
                                        "Authorization": "Bearer " + userToken
                                    },
                                    success: function (response, textStatus, xhr) {
                                        if (response['success']) {                               
                                            for (var i in response['contact_list']) {
                                                var newUser = $('<li>');
                                                if (response['contact_list'][i]['id'] == $('#otherUserID').val()) {
                                                    var divClass='person-contact active';
                                                } else {
                                                    var divClass='person-contact';
                                                }                                       
                                                newUser.append($('<div>').addClass(divClass).attr({'data-id': response['contact_list'][i]['id']})
                                                                         .append($('<a>').attr({'href': 'javascript:void(0);'}).addClass('person-contact-link')
                                                                                         .append($('<span>').addClass('person-name').text(response['contact_list'][i]['first_name'] + ','))
                                                                                         .append($('<span>').addClass('person-id').html(' ID: ')
                                                                                                            .append($('<span>').text(response['contact_list'][i]['id']))
                                                                                                )
                                                                                                
                                                                                )
                                                              );

                                                if (response['contact_list'][i]['unreaded_messages'] != null && response['contact_list'][i]['unreaded_messages'] > 0) {
                                                    newUser.find('.person-contact-link').append('<span class="item-count">' + response['contact_list'][i]['unreaded_messages'] + '</span>');
                                                }
                                                
                                                sidebar.find('.contact-list').append(newUser);                   
                                            }
                                        } else {
                                        }
                                        enableEvent = true;                             
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        enableEvent = true;
                                    }
                                }); 
                            }
                            else if (st == lastScrollTop) {
                            }
                            lastScrollTop = st;
                        });
                    } else {
                        $(".contact-list-wrapper .ss-content").off('scroll', "**");
                    }  
                }                                                          
            } else {
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body span').text(response['message']);
                setTimeout(window['commonLibrary']['hideModal'], 2000);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            errorHandler(xhr.status);
        }       
    });
}

function getPagination(currentPage, totalPages, myfunction, range = 1, dots = "", paginationClass="") {
    if (dots == "") {
        dots = "<li><a href=\"javascript:void(0);\">...</a></li>";
    }
    var slider = range * 2;
    var $result = $('<ul>');
    var from, to;
    if (paginationClass=="") {
        $result.addClass("pagination list-inline");
    } else {
        $result.addClass(paginationClass);
    }
    if (currentPage > 1) {
        var prevpage = currentPage - 1;
        $result.append(buildLink(prevpage, "< Prev", myfunction, 'prev'));
    }
    if (totalPages <= 5 + range*2) {
        from = 1;
        to = totalPages;        
        $result.append(buildLinkList(currentPage, from, to, myfunction));               
    }
    // Example: 1 [2] 3 4 5 6 ... 23 24
    else if (currentPage <= slider + 2) {
        from = 1;
        to = slider + 2;
        $result.append(buildLinkList(currentPage, from, to, myfunction));
        $result.append(dots, buildLinkList(currentPage, totalPages-1, totalPages, myfunction));
    }
    // Example: 1 2 ... 32 33 34 35 [36] 37
    else if (currentPage >= totalPages - slider) {
        from = totalPages - slider - 1;
        to = totalPages;
        $result.append(buildLinkList(currentPage, 1, 2, myfunction), dots);
        $result.append(buildLinkList(currentPage, from, to, myfunction));
    } else if ((currentPage > slider) && (currentPage < totalPages - slider)) {
        // Example: 1 2 ... 23 24 25 [26] 27 28 29 ... 51 52
        $result.append(buildLinkList(currentPage, 1, 2, myfunction), dots);
        $result.append(buildLinkList(currentPage, currentPage - range, currentPage + range, myfunction));
        $result.append(dots, buildLinkList(currentPage, totalPages-1, totalPages, myfunction));
    } 
    if (currentPage != totalPages) {
        var nextpage = currentPage + 1;
        $result.append(buildLink(nextpage, "Next >", myfunction, 'next'));
    }
    return $result;
}

function buildLinkList(currentPage, from, to, myfunction) {
    var pages = [];
    var page;
    for (page = from; page <= to; page++) {
        if (page == currentPage) {         
            pages.push(buildLink(page, page, myfunction, 'active'));
        // if not current page...
        } else {
         // make it a link
            pages.push(buildLink(page, page, myfunction, ''));
        } // end else
    }
    return pages;
}

function buildLink(page, text, myfunction, linkClass = "") {
    var $link = $('<li>');
    if (linkClass != "") {
        $link.addClass(linkClass);
    }
    $link.append('<a>');
    $link.find('a').attr('href', "javascript:void(0);").on('click', function() {myfunction(page)}).text(text);
    return $link;
}

$(document).ready(function() {
    $(".contact-cta-list .remove").on('click', function() {        
        var contactID = $(".contact-list .person-contact.active").data('id');
        if (typeof contactID == "undefined" || contactID == null) {
            $('#modalMessage').modal('show');
            $('#modalMessage .modal-body span').text('Select contact in a contact list.');
            setTimeout(window['commonLibrary']['hideModal'], 2000);
            return;
        }
        if (confirm('Are you really want remove this contact from contact list?')) {
            $.ajax({
                url: apiServer+'/v1/user/contact/' + contactID,
                dataType: "json",
                type: "DELETE",
                headers: {
                    "Authorization": "Bearer " + userToken
                },
                success: function(response, textStatus, xhr) {
                    if (xhr.status == 204) {
                        window['accountLibrary']['contactPagination'](1);
                        $('#modalMessage').modal('show');
                        $('#modalMessage .modal-body span').text('Contact was removed from contact list.');
                        setTimeout(window['commonLibrary']['hideModal'], 2000); 
                    }
                }, 
                error: function (xhr, ajaxOptions, thrownError) {
                    errorHandler(xhr.status);
                }
            });  
        }
    }); 

    setLastActivity();
    $.ajax({
        url: apiServer + '/v1/user/get-new-message-count',
        type: "POST",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + userToken
        },
        data: {
            'userID': userID,
            'token':userToken
        },
        success: function (response) {
            if (response.success) {
                var lettersCount = parseInt(response.lettersCount);
                var messageCount = parseInt(response.messageCount);
                var chatsCount = parseInt(response.chatsCount);
                if (chatsCount > 0) {
                    $('.message-item.chat-message').remove();
                    $('#messages-feed').append('<div class="message-item chat-message" style="display:block;">' +
                            '<a href="javascript:void(0);" class="message-item-link"></a>' +
                            '<button type="button" class="close">' +
                              '<span></span>' +
                              '<span></span>' +
                            '</button>' +
                            '<span class="message-title"><a href="/account/chat">New chat message</a></span>' +
                            '<div class="message-body clearfix">' +                                               
                              '<i class="fa fa-big fa-commenting-o"></i>' + 
                              '<div class="text-container">' +                                                                          
                                '<a href="/account/chat"><span class="message-text">You have ' + chatsCount + 
                                ((chatsCount > 1) ? ' unreaded chat messages' : ' unreaded chat message') + '</span></a>' +
                              '</div>' +
                            '</div>' +
                          '</div>');    
                }   
                if (lettersCount > 0) {                                     
                    $('.message-item.letter-message').remove();
                    $('#messages-feed').append('<div class="message-item letter-message" style="display:block;">' +
                                                '<a href="javascript:void(0);" class="message-item-link"></a>' +
                                                '<button type="button" class="close">' +
                                                  '<span></span>' +
                                                  '<span></span>' +
                                                '</button>' +
                                                '<span class="message-title"><a href="/message/inbox">New letter</a></span>' +
                                                '<div class="message-body clearfix">' +                                               
                                                  '<i class="fa fa-big fa-envelope"></i>' +
                                                  '<div class="text-container">' +                                                                                                      
                                                    '<a href="/message/inbox"><span class="message-text">You have ' + lettersCount + 
                                                    ((lettersCount > 1) ? ' unreaded letters' : ' unreaded letter') + '</span></a>' +                                                   
                                                  '</div>' +
                                                '</div>' +
                                              '</div>');    
                }               
                if (messageCount > 0) {
                    $('.message-item.system-message').remove();
                    $('#messages-feed').append('<div class="message-item system-message" style="display:block;">' +
                            '<a href="javascript:void(0);" class="message-item-link"></a>' +
                            '<button type="button" class="close">' +
                              '<span></span>' +
                              '<span></span>' +
                            '</button>' +
                            '<span class="message-title"><a href="/message/system">New system message</a></span>' +
                            '<div class="message-body clearfix">' + 
                              '<i class="fa fa-big fa-envelope"></i>' +                                           
                              '<div class="text-container">' +                                                                                  
                                '<a href="/message/system"><span class="message-text">You have ' + messageCount +
                                ((lettersCount > 1) ? ' unreaded system messages' : ' unreaded system message') + '</span></a>' +                                
                              '</div>' +
                            '</div>' +
                          '</div>');
                }                               
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }       
    }); 
});

return {
    "getDocumentAlbum": getDocumentAlbum,
    "getNewMessageCount": getNewMessageCount,
    "getUsersOnAccountPage": getUsersOnAccountPage,
    "getPagination": getPagination,
    "chatPagination": chatPagination,
    "contactPagination": contactPagination,
    "getAccess": getAccess,
    "giftPagination": giftPagination,
    "addGiftToCart": addGiftToCart,
}

})(jQuery);

/*for cart page only start code*/
$(document).ready(function() {
    window['accountLibrary']['contactPagination'](1);
    window['commonLibrary']['updateUserBalance']();
    $('.gifts-add-container .gift-quantity .less').click(function () {
        var old_count = $(this).parent().find('.quantity-counter');
        var count = parseInt(old_count.text()) - 1;
        count = count < 1 ? 1 : count;
        old_count.text(count);
        reCalcTotalAmount($(this).parents('.gifts-add-block'));
        return false;
    });
    $('.gifts-add-container .gift-quantity .more').click(function () {
        var old_count = $(this).parent().find('.quantity-counter');
        var count = parseInt(old_count.text()) + 1;
        old_count.text(count);
        reCalcTotalAmount($(this).parents('.gifts-add-block'));
        return false;
    });
});
function reCalcTotalAmount(place){
    var count = 0,price = 0;
    $(place).find('.quantity-counter').each(function(){
        count += parseInt($(this).text());
        price += (parseFloat(($(this).siblings('.more').attr('data-price')))*parseInt($(this).text()));
        //умножаем цену одного подарка на их количество
    });
    $('.total-text .total-quantity').html(count);
    $('.total-text .total-credits').html(price.toFixed(2));
    $('.total-text .total-price').html(price.toFixed(2)+" USD");
}
/*for cart page only end code*/