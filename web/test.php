<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<script src="/js/chatConfig.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
	<script>


		try {
			// Create WebSocket connection.
			const socket = new WebSocket('wss://echo.websocket.org');

			// Connection opened
			socket.addEventListener('open', function (event) {
			    socket.send('Hello Server!');
			});

			// Listen for messages
			socket.addEventListener('message', function (event) {
			    document.write('Message from server ', event.data);
			});
		} catch (err) {
			alert(err.message);
		}
	</script>
</body>
</html>